import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Router } from '@angular/router';
import { AbstractService } from './abstract.service';
import { Utility } from '../util/Utility';
declare var $: any;
declare var JSOG: any;
declare var moment: any;

@Injectable()
export class DisabilityService extends AbstractService {

    constructor(router: Router, localStorageService: LocalStorageService, http: HttpClient, globalService: GlobalService) {
        super(router, localStorageService, http, globalService);
        this.setEntity('disability');
    }
    getAllDisabilityList(boardType) {
        let params: HttpParams = new HttpParams();
        let headers: HttpHeaders = new HttpHeaders();
        if (Utility.notBlank(boardType) && boardType != -1) {
            params = params.set("boardType", boardType);
        }
        return this.http.get(this.globalService.getBase() + "/api/disability/getAlldisabilityList", { params: params, headers: this.getHeaders() });
    }

}