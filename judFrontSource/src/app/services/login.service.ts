import { Injectable } from "@angular/core";
import { Observable, Subject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { GlobalService } from './global.service';
declare var $: any;

@Injectable()
export class LoginService {
    _isLoggedIn: boolean = false;
    public userLoggedInSubject = new Subject<boolean>();
    _localStorageService: LocalStorageService;

    constructor(localStorageService: LocalStorageService, public globalService: GlobalService) {
        this._localStorageService = localStorageService;
    }

    isLoggedIn() {
        if (!this._isLoggedIn) {
            this._isLoggedIn = this._localStorageService.get('is-logged-in');
            this.userLoggedInSubject.next(this._isLoggedIn);
        }
        return this._isLoggedIn;
    }

    setLoggedOut() {
        this._isLoggedIn = false;
        var preferenceObj = this._localStorageService.get('preferenceObject');
        this._localStorageService.deleteAll();
        this.globalService.clearAll();
        this._localStorageService.set('preferenceObject', preferenceObj);
        this.userLoggedInSubject.next(this._isLoggedIn);
    }

    setToken(auth) {
        this._localStorageService.set('is-logged-in', true);
        this._localStorageService.set('token', auth.access_token);
    }
    setLoggedIn() {
        this._isLoggedIn = true;
        this.userLoggedInSubject.next(this._isLoggedIn);
    }
    setPrefeance(pref) {
        this._localStorageService.set('preferance', pref);
    }
}