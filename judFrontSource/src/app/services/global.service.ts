import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LocalStorageService } from './local-storage.service';
import { Utility } from '../util/Utility';

declare var moment: any;
declare var $: any;

// Name Service
export interface shareData {
    data: any;
}

@Injectable()
export class GlobalService {
    mode: boolean = false;
    entity: string;
    protocol: string;
    host: string;
    appName: string;
    token: string;
    route: any;
    menu: any;
    user: any;
    role: any;
    rolePermission: any;
    accessibleMenus: any;
    accessibleMenuUrls: any;
    accessibleRoutes: any;
    accessibleRouteUrls: any;
    tenant: any;
    tenantName: any;
    tenantList: any;
    industry: any;
    dashboard: any;
    lastSelectedTenant: any;
    industryList: any;
    loggedInEmail: any;
    resetPasswordUserId: any;
    dynamicLabelMap: any;
    drilldownFromObj: any;
    serverRouteMap: shareData = { data: {} };
    accessibleIndustry: any;
    industryTenantMap: any;
    solrServerTimeZone: any;
    tenantRoleMap: any;
    currentBrowserURL: any;
    wistiaPwd: any;
    customDashboards = [];
    tenantMenuMap = {};
    baselineData: any;
    locationIds;
    currentMenuUrl;
    httpError;
    selectedFilters: any;
    tenantType: any; //IN - 664 (To determine tenant is virtual tenant or not)
    navigatedFromDashboard: any;
    reportUnits: any;
    isLandingPage: boolean = false;
    userAvatarByteData: string = "";
    myPerformanceDashboardData: any = null;
    preferences: any = {};
    buildVersion: any = null;
    sessionTimeOutDuration: any;
    lastInteractionTime: any;
    isUserIdle: boolean = false;
    defaultMinColumn: number;


    private constructor(private title: Title, private localStorageService: LocalStorageService) {
        this.protocol = window.location.protocol;
        this.host = window.location.hostname;
        this.appName = "jud";
        this.accessibleMenuUrls = [];
        this.accessibleRouteUrls = [];

    }
    // SOLR RELATED CONSTANTS
    public DATE_FORMAT: string = 'DD-MM-YYYY';
    public DATE_FORMAT2: string = 'DD-MMM-YYYY';
    public SOLR_DATE_FORMAT: string = 'YYYY-MM-DD';
    public DATE_FORMAT3: string = 'MMMM DD, YYYY';
    public SOLR_DATE_APPEND_23: string = 'T23:59:59Z-5HOUR-30MINUTES';
    public SOLR_DATE_APPEND_ZERO: string = 'T00:00:00Z-5HOUR-30MINUTES';
    public DATE_TIME_FORMAT: string = "DD-MMM-YYYY hh:mm:ss A";

    clearAll() {
        this.token = null;
        this.route = null;
        this.menu = null;
        this.user = null;
        this.role = null;
        this.rolePermission = null;
        this.tenant = null;
        this.tenantList = null;
        this.industry = null;
        this.industryList = null;
        this.loggedInEmail = null;
        this.resetPasswordUserId = null;
        this.dynamicLabelMap = null;
        this.drilldownFromObj = null;
        this.accessibleMenuUrls = [];
        this.userAvatarByteData = "";
        this.myPerformanceDashboardData = null;
        this.preferences = {};
        this.buildVersion = null;
    }


    getMonthMap() {
        var map = this.localStorageService.get('yearMonthArray');
        if (map == undefined || map == null || map.length == 0) {
            var monthMap = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            map = {};
            for (var i = 2001; i <= 2100; i++) {
                for (var j = 0; j < monthMap.length; j++) {
                    map[monthMap[j] + "-" + i] = i + "-" + (j < 9 ? ("0" + (j + 1)) : (j + 1));
                }
            }
            this.localStorageService.set('yearMonthArray', map);
        }
        return map;
    }

    getWistiaPwd() {
        return this.wistiaPwd;
    }
    setWistiaPwd(wistiaPwd) {
        this.wistiaPwd = wistiaPwd;
    }
    getBase(): string {
        return this.protocol + '//' + this.host + '/' + this.appName;
    }
    setCurrentBrowserURL(obj) {
        this.currentBrowserURL = obj;
    }
    getCurrentBrowserURL() {
        return this.currentBrowserURL;
    }
    setRoute(obj) {
        this.route = obj;
    }
    getRoute() {
        return this.route;
    }
    getRouteMap() {
        var routes = this.getRoute();
        var map = {};
        if (routes) {
            routes.forEach(element => {
                map[element.url] = "true";
            });
        }
        return map;
    }
    setUser(obj) {
        this.user = obj;
        this.localStorageService.set('user', this.user);
    }
    getUser() {
        if (this.user == undefined) {
            this.user = this.localStorageService.get('user');
        }
        return this.user;
    }
    setRole(role) {
        this.role = role;
        this.localStorageService.set('role', this.role);
        let permissionStr = [];
        if (this.role.rolePermissions) {
            this.role.rolePermissions.forEach(element => {
                permissionStr.push(element.name);
            });
        }
        this.setRolePermission(permissionStr);
        let accessibleMenusTmp = [];
        if (this.role.roleIndustryMenu) {
            this.role.roleIndustryMenu.forEach(element => {
                accessibleMenusTmp.push(element);
            });
        }
        let accessibleRoutesTmp = [];
        if (this.role.roleIndustryRoute) {
            this.role.roleIndustryRoute.forEach(element => {
                accessibleRoutesTmp.push(element);
            });
        }
        this.setAccessibleMenus(accessibleMenusTmp);
        this.setAccessibleRoutes(accessibleRoutesTmp);
    }
    getRole() {
        if (this.role == undefined) {
            this.role = this.localStorageService.get('role');
        }
        return this.role;
    }
    setIsSuperAdmin(isSuperAdmin) {
        this.localStorageService.set('isSuperAdmin', isSuperAdmin);
    }
    getIsSuperAdmin() {
        return this.localStorageService.get('isSuperAdmin');
    }
    setAccessibleMenus(menus) {
        this.accessibleMenus = menus;
        this.localStorageService.set('accessibleMenus', this.accessibleMenus);
        this.accessibleMenuUrls = [];
        this.accessibleMenus.forEach(element => {
            this.accessibleMenuUrls.push(element.url);
        });
        this.localStorageService.set('accessibleMenuUrls', this.accessibleMenuUrls);
    }
    setAccessibleRoutes(routes) {
        this.accessibleRoutes = routes;
        this.localStorageService.set('accessibleRoutes', this.accessibleRoutes);
        this.accessibleRouteUrls = [];
        this.accessibleRoutes.forEach(element => {
            this.accessibleRouteUrls.push(element.url);
        });
        this.localStorageService.set('accessibleRouteUrls', this.accessibleRouteUrls);
    }
    getAccessibleMenus() {
        if (this.accessibleMenus == undefined) {
            this.accessibleMenus = this.localStorageService.get('accessibleMenus');
        }
        return this.accessibleMenus;
    }
    setRolePermission(rolePermission) {
        if (rolePermission.indexOf('isSuperAdmin') > -1) {
            this.setIsSuperAdmin(true);
        } else {
            this.setIsSuperAdmin(false);
        }
        this.rolePermission = rolePermission;
        this.localStorageService.set('rolePermission', this.rolePermission);

    }
    getRolePermission() {
        if (this.rolePermission == undefined) {
            this.rolePermission = this.localStorageService.get('rolePermission');
        }
        return this.rolePermission;
    }
    checkRolePermission(permission: any) {
        var permissions = this.getRolePermission();
        if (Utility.notBlank(permission) && Utility.notBlank(permissions) && permissions.length > 0) {
            for (var i = 0; i < permissions.length; i++) {
                if (permissions[i] === permission)
                    return true;
            }

        }
        return false;
    }
    setAccessibleIndustry(accessibleIndustry) {
        this.accessibleIndustry = accessibleIndustry;
        //this.localStorageService.set('accessibleIndustry', this.accessibleIndustry);
    }
    getAccessibleIndustry() {
        /*if (this.accessibleIndustry == undefined) {
            this.accessibleIndustry = this.localStorageService.get('accessibleIndustry');
        }*/
        return this.accessibleIndustry;
    }
    setIndustryTenantMap(industryTenantMap) {
        this.industryTenantMap = industryTenantMap;
        //this.localStorageService.set('industryTenantMap', this.industryTenantMap);
    }
    getIndustryTenantMap() {
        /*if (this.industryTenantMap == undefined) {
            this.industryTenantMap = this.localStorageService.get('industryTenantMap');
        }*/
        return this.industryTenantMap;
    }
    setTenantRoleMap(tenantRoleMap) {
        this.tenantRoleMap = tenantRoleMap;
        //this.localStorageService.set('tenantRoleMap', this.tenantRoleMap);
    }
    getTenantRoleMap() {
        /*if (this.tenantRoleMap == undefined) {
           this.tenantRoleMap = this.localStorageService.get('tenantRoleMap');
       }*/
        return this.tenantRoleMap;
    }
    setTenantList(tenantList) {
        this.tenantList = tenantList;
    }
    getTenantList() {
        return this.tenantList;
    }
    setTenant(tenant) {
        this.tenant = tenant;
        this.localStorageService.set('tenant', this.tenant);
        if (Utility.blank(this.localStorageService.get('tenantTheme'))) {
            this.localStorageService.set('tenantTheme', this.tenant);
        }
    }
    getTenant() {
        if (this.tenant == undefined) {
            this.tenant = this.localStorageService.get('tenant');
        }
        return this.tenant;
    }
    //IN - 664 (To determine tenant is virtual tenant or not)
    setTenantType(tenantType) {
        this.tenantType = tenantType;
        this.localStorageService.set('tenantType', this.tenantType);
    }

    getTenantType() {
        if (this.tenantType == undefined) {
            this.tenantType = this.localStorageService.get('tenantType');
        }
        return this.tenantType;
    }
    isVirtualTenant() {
        if (this.getTenantType() == "Virtual") {
            return true;
        } else {
            return false;
        }
    }
    setTenantName(tenantName) {
        this.tenantName = tenantName;
    }
    getTenantName() {
        return this.tenantName;
    }
    getToken() {
        return this.localStorageService.get('token');
    }

    getIndustry() {
        if (this.industry == undefined) {
            this.industry = this.localStorageService.get('industry');
        }
        return this.industry;
    }

    setLastSelectedTenant(tenantId) {
        this.lastSelectedTenant = tenantId;
        this.localStorageService.set('lastSelectedTenant', this.lastSelectedTenant);
    }
    getLastSelectedTenant() {
        if (this.lastSelectedTenant == undefined) {
            this.lastSelectedTenant = this.localStorageService.get('lastSelectedTenant');
        }
        return this.lastSelectedTenant;
    }
    setIndustryList(industryList) {
        this.industryList = industryList;
    }
    getIndustryList() {
        return this.industryList;
    }
    getLineaMenu(menus) {
        var linearMenu = [];
        menus.forEach(menu => {
            linearMenu.push(menu);
            if (menu.children && menu.children.length > 0) {
                menu.children.forEach(submenu => {
                    linearMenu.push(submenu);
                    if (submenu.children && submenu.children.length > 0) {
                        submenu.children.forEach(submenu2 => {
                            linearMenu.push(submenu2);
                        });
                    }
                });
            }
        });
        return linearMenu;
    }
    getComponentTitle(url) {

    }
    getSearchForm(component) {
        let localStorageForm = this.localStorageService.get('searchForm');
        let searchForm = localStorageForm ? localStorageForm : {};
        return searchForm[component];
    }
    setSearchForm(component, form) {
        let localStorageForm = this.localStorageService.get('searchForm');
        let searchForm = localStorageForm ? localStorageForm : {};
        searchForm[component] = form;
        this.localStorageService.set('searchForm', searchForm);
    }
    getLoggedInEmail() {
        return this.loggedInEmail;
    }
    setLoggedInEmail(loggedInEmail) {
        return this.loggedInEmail = loggedInEmail;
    }
    getResetPasswordUserId() {
        return this.resetPasswordUserId;
    }
    setResetPasswordUserId(userId) {
        return this.resetPasswordUserId = userId;
    }
    getDynamicLabelMap() {
        return this.dynamicLabelMap;
    }
    setDynamicLabelMap(dynamicLabelMap) {
        return this.dynamicLabelMap = dynamicLabelMap;
    }
    setDrilldownFromObject(drilldownFromObj) {
        this.drilldownFromObj = drilldownFromObj;
    }
    getDrilldownFromObject() {
        return this.drilldownFromObj;
    }
    setServerRouteMap(obj) {
        this.serverRouteMap.data = obj;
    }
    getServerRouteMap() {
        return this.serverRouteMap.data;
    }
    setSolrServerTimeZone(obj) {
        this.solrServerTimeZone = obj;

    }
    getSolrServerTimeZone() {
        return this.solrServerTimeZone;
    }
    getSolrQueryStartDateAppender() {
        let chkServerTZ = this.getSolrServerTimeZone();
        chkServerTZ = chkServerTZ.replace(/--/g, "-");
        if (chkServerTZ.startsWith("-")) {
            chkServerTZ = chkServerTZ.replace(/-/g, "+");
        } else {
            chkServerTZ = "-" + chkServerTZ;
        }
        return 'T00:00:00Z' + chkServerTZ;
    }
    getSolrQueryEndDateAppender() {
        let chkServerTZ = this.getSolrServerTimeZone();
        chkServerTZ = chkServerTZ.replace(/--/g, "-");
        if (chkServerTZ.startsWith("-")) {
            chkServerTZ = chkServerTZ.replace(/-/g, "+");
        } else {
            chkServerTZ = "-" + chkServerTZ;
        }
        return 'T23:59:59Z' + chkServerTZ;
    }
    checkPermission(url, isRoute = false) {
        // IN 664 provide restricted access to super admin of virtul tenant
        if (this.isVirtualTenant() && this.getIsSuperAdmin()) {
            if ((Utility.notBlank(isRoute) && isRoute)) {
                if (!Utility.notBlank(this.accessibleRouteUrls) || this.accessibleRouteUrls.length == 0) {
                    this.accessibleRouteUrls = this.localStorageService.get('accessibleRouteUrls');
                }
                let routeUrls = this.accessibleRouteUrls;
                if (routeUrls && routeUrls.indexOf(url) > -1) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if (!Utility.notBlank(this.accessibleMenuUrls) || this.accessibleMenuUrls.length == 0) {
                    this.accessibleMenuUrls = this.localStorageService.get('accessibleMenuUrls');
                }
                let menuUrls = this.accessibleMenuUrls;
                if (menuUrls && menuUrls.indexOf(url) > -1) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        if ((Utility.notBlank(isRoute) && isRoute)) {
            if (this.getIsSuperAdmin()) {
                return false;
            }
            if (!Utility.notBlank(this.accessibleRouteUrls) || this.accessibleRouteUrls.length == 0) {
                this.accessibleRouteUrls = this.localStorageService.get('accessibleRouteUrls');
            }
            let routeUrls = this.accessibleRouteUrls;
            if (routeUrls && routeUrls.indexOf(url) > -1) {
                return false;
            } else {
                return true;
            }
        } else {
            if (this.getIsSuperAdmin()) {
                return false;
            }
            if (!Utility.notBlank(this.accessibleMenuUrls) || this.accessibleMenuUrls.length == 0) {
                this.accessibleMenuUrls = this.localStorageService.get('accessibleMenuUrls');
            }
            let menuUrls = this.accessibleMenuUrls;
            if (menuUrls && menuUrls.indexOf(url) > -1) {
                return false;
            } else {
                return true;
            }
        }
    }
    getCustomDashboards() {
        /*if (this.industryTenantMap == undefined) {
            this.industryTenantMap = this.localStorageService.get('industryTenantMap');
        }*/
        return this.customDashboards;
    }
    setCustomDashboards(customDashboards) {
        this.customDashboards = customDashboards;
        //this.localStorageService.set('tenantRoleMap', this.tenantRoleMap);
    }

    setTenantMenuMap(tenantMenuMap) {
        this.tenantMenuMap = Utility.notBlank(tenantMenuMap) ? tenantMenuMap : {};
    }
    getTenantMenuMap() {
        return this.tenantMenuMap;
    }

    getBaselineData() {
        return this.baselineData;
    }
    setBaselineData(baselineData) {
        this.baselineData = baselineData;
    }
    setAccessibleLocations(locationIds) {
        this.localStorageService.set('accessibleLocationIds', locationIds);
        this.locationIds = locationIds;
    }
    getAccessibleLocations() {
        if (this.locationIds == undefined) {
            this.locationIds = this.localStorageService.get('accessibleLocationIds');
        }
        return this.locationIds;
    }
    setCurrentMenuUrl(currentMenuUrl) {
        this.currentMenuUrl = currentMenuUrl;
    }
    getCurrentMenuUrl() {
        return this.currentMenuUrl;
    }
    toggleDeveloperMode() {
        this.mode = !this.mode;
    }
    chkDeveloperMode() {
        return this.mode;
    }
    getPrferance() {
        return this.localStorageService.get('preferance');
    }
    getHttpError() {
        return this.httpError;
    }
    setHttpError(httpError) {
        this.httpError = httpError;
    }
    setActiveMenu(obj) {
        this.localStorageService.set('activeMenu', obj);
    }
    getActiveMenu() {
        return this.localStorageService.get('activeMenu');
    }
    getSolrQueryDateAppenderForUTC(date) {
        if (Utility.notBlank(date)) {
            let utcTime = date.split(' ')[1];
            let solrTimeFormat = Utility.notBlank(utcTime) ? "T" + utcTime + "Z" : "";
            var solrDateFormat = date.split(' ')[0] + "" + solrTimeFormat;
        }
        return solrDateFormat;
    }
    setSelectedFilters(filters) {
        this.selectedFilters = filters;
        this.localStorageService.set('selectedFilters', this.selectedFilters);
    }
    getSelectedFilters() {
        if (this.selectedFilters == undefined) {
            this.selectedFilters = this.localStorageService.get('selectedFilters');
        }
        return this.selectedFilters;
    }
    getNavigatedFromDashboard() {
        return this.navigatedFromDashboard;
    }
    setNavigatedFromDashboard(navigatedFromDashboard) {
        this.navigatedFromDashboard = navigatedFromDashboard;
    }
    getTenantReportUnits() {
        if (this.reportUnits == undefined) {
            this.reportUnits = this.localStorageService.get('tenantReportUnits');
        }
        return this.reportUnits;
    }
    setTenantReportUnits(reportUnits) {
        this.localStorageService.set('tenantReportUnits', reportUnits);
        this.reportUnits = reportUnits;
    }

    getUserAvatarByteData() {
        return this.userAvatarByteData;
    }
    setUserAvatarByteData(data) {
        this.userAvatarByteData = data;
    }

    getMyPerformanceDashboardData() {
        return this.myPerformanceDashboardData;
    }
    setMyPerformanceDashboardData(data) {
        this.myPerformanceDashboardData = data;
    }

    getPreferences() {
        return this.preferences;
    }
    setPreferences(data) {
        this.preferences = data;
    }

    getBuildVersion() {
        return this.buildVersion;
    }
    setBuildVersion(data) {
        this.buildVersion = data;
    }

    getSessionTimeOutDuration() {
        if (this.sessionTimeOutDuration == undefined) {
            this.sessionTimeOutDuration = this.localStorageService.get('sessionTimeOutDuration');
        }
        return this.sessionTimeOutDuration;
    }
    setSessionTimeOutDuration(sessionTimeOutDuration) {
        this.sessionTimeOutDuration = sessionTimeOutDuration * 60 * 1000;
        this.localStorageService.set('sessionTimeOutDuration', this.sessionTimeOutDuration);
    }

    getLastInteractionTime() {
        if (this.lastInteractionTime == undefined) {
            this.lastInteractionTime = this.localStorageService.get('lastInteractionTime');
        }
        return this.lastInteractionTime;
    }
    setLastInteractionTime(lastInteractionTime) {
        this.lastInteractionTime = lastInteractionTime;
        this.localStorageService.set('lastInteractionTime', this.lastInteractionTime);
    }

    getDefaultMinColumn() {
        if (this.defaultMinColumn == undefined) {
            this.defaultMinColumn = this.localStorageService.get('defaultMinColumn');
        }
        return this.defaultMinColumn;
    }
    setDefaultMinColumn(defaultMinColumn) {
        this.defaultMinColumn = defaultMinColumn;
        this.localStorageService.set('defaultMinColumn', this.defaultMinColumn);
    }


    getIsUserIdle() {
        if ((moment().valueOf() - this.getLastInteractionTime()) > this.getSessionTimeOutDuration()) {
            this.isUserIdle = true;
        } else {
            this.isUserIdle = false;
        }
        return this.isUserIdle;
    }





}
