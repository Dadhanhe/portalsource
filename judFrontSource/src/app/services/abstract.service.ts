import { Injectable } from "@angular/core";

import { LocalStorageService } from './local-storage.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Router } from '@angular/router';
import { Utility } from '../util/Utility';
declare var $: any;
declare var JSOG: any;
@Injectable()
export class AbstractService {
    entity: string;
    baseEntityPath: string;
    constructor(public router: Router, public localStorageService: LocalStorageService, public http: HttpClient, public globalService: GlobalService) {

    }
    setEntity(string) {
        this.entity = string;
        this.baseEntityPath = this.globalService.getBase() + '/api/' + this.getEntity() + '/secure';
    }

    setBaseEntityPath(string) {
        this.baseEntityPath = string;
    }

    getEntity() {
        return this.entity;
    }
    getHeaders() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Authorization', 'bearer ' + this.globalService.getToken());
        headers = headers.append('IndustryId', '1');
        headers = headers.append('content-type', 'application/json');
        headers = headers.append('platform', '2');
        return headers;
    }

    getExportHeaders() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Authorization', 'bearer ' + this.globalService.getToken());
        headers = headers.append('IndustryId', this.globalService.getIndustry());
        headers = headers.append('content-type', 'application/json');
        headers = headers.append('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        return headers;
    }
    getDetails(id) {
        return this.http.get(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure/" + id, { headers: this.getHeaders() })
            ;
    }
    find(body) {
        let reqBody = JSON.stringify(body);
        return this.http.post(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure/find", reqBody, { headers: this.getHeaders() })
            ;
    }
    create(body) {
        let reqBody = JSON.stringify(body);
        return this.http.post(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure", reqBody, { headers: this.getHeaders() })
            ;
    }
    update(body) {
        let reqBody = JSON.stringify(body);
        return this.http.put(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure", reqBody, { headers: this.getHeaders() })
            ;
    }
    delete(id) {
        return this.http.delete(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure/" + id, { headers: this.getHeaders() })
            ;
    }
    findFromDB(tenantId, first, total, search, status, orderBy?: any, sort?: any) {
        let params: HttpParams = new HttpParams();
        if (Utility.notBlank(tenantId)) {
            params = params.set("tenantId", tenantId);
        }
        if (Utility.notBlank(first)) {
            params = params.set("first", first);
        }
        if (Utility.notBlank(total)) {
            params = params.set("total", total);
        }
        if (Utility.notBlank(search)) {
            params = params.set("search", search);
        }
        if (Utility.notBlank(status)) {
            params = params.set("status", status);
        }
        if (Utility.notBlank(orderBy)) {
            if (orderBy instanceof Array) {
                params = params.set("orderBy", orderBy.join());
            } else {
                params = params.set("orderBy", orderBy);
            }
        }
        if (Utility.notBlank(sort)) {
            if (sort instanceof Array) {
                params = params.set("sort", sort.join());
            } else {
                params = params.set("sort", sort);
            }
        }
        return this.http.get(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure", { params: params, headers: this.getHeaders() })
            ;
    }
    cancel(id) {
        return this.http.delete(this.globalService.getBase() + "/api/" + this.getEntity() + "/secure/cancel/" + id, { headers: this.getHeaders() })
            ;
    }

    addObjectValue(jsonData, key, value) {
        if (Utility.notBlank(jsonData) && Utility.notBlank(key) && Utility.notBlank(value)) {
            jsonData[key] = value;
        }
    }
}