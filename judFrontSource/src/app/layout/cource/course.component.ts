import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { Course } from "../../modal/course.modal";
import { CourseService } from "../../services/course.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-course',
    templateUrl: "./course.component.html",
    animations: []
})


export class CourseComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    course: Course = new Course();
    boardType;
    courseList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private courseService: CourseService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllcourses();
    }

    addCourse(content) {
        console.log("test");
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveCourse() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.course.mapTo = "COURSE";
                this.courseService.update(this.course).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllcourses();
                        console.log("test");
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "COURSE";
                this.courseService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllcourses();
                        console.log("test");
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllcourses();

    }
    getAllcourses() {
        this.courseService.getAllcourses(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.courseList = response.data;
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.course.id = education.id;
        this.course.name = education.name;
        this.course.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.course.activeStatus = "Active";
        } else {
            this.course.activeStatus = "Deactive";
        }
        this.course.mapTo = "COURSE";
        this.course.id = edu.id;
        this.course.name = edu.name;
        this.courseService.update(this.course).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllcourses();
            }
        });
    }
}

