import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { EducationBoard } from "../../modal/educationBoard.modal";
import { EducationBoardService } from "../../services/educationBoard.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-educationBoard',
    templateUrl: "./educationBoard.component.html",
    animations: []
})


export class EducationBoardComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    educationBoard: EducationBoard = new EducationBoard();
    boardType;
    educationBoardList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private educationBoardService: EducationBoardService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllEducationBoards();
    }

    addEducationBoard(content) {
        console.log("test");
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveEducationBoard() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.educationBoard.mapTo = "EDUCATIONBOARD";
                this.educationBoardService.update(this.educationBoard).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllEducationBoards();
                        console.log("test");
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "EDUCATIONBOARD";
                this.educationBoardService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllEducationBoards();
                        console.log("test");
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllEducationBoards();

    }
    getAllEducationBoards() {
        this.educationBoardService.getAllBoardList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.educationBoardList = response.data;
                console.log(this.educationBoardList)
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.educationBoard.id = education.id;
        this.educationBoard.name = education.name;
        this.educationBoard.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.educationBoard.activeStatus = "Active";
        } else {
            this.educationBoard.activeStatus = "Deactive";
        }
        this.educationBoard.mapTo = "EDUCATIONBOARD";
        this.educationBoard.id = edu.id;
        this.educationBoard.name = edu.name;
        this.educationBoardService.update(this.educationBoard).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllEducationBoards();
            }
        });
    }
}

