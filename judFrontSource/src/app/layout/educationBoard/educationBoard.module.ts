import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationBoardComponent } from './educationBoard.component';
import { EducationBoardRoutingModule } from './educationBoard-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationModule } from '../../validation/validation.module';

@NgModule({
    imports: [
        CommonModule,
        EducationBoardRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ValidationModule
    ],
    declarations: [EducationBoardComponent],

})
export class EducationBoardModule { }
