import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { Industry } from "../../modal/industry.modal";
import { Subindustry } from "../../modal/subindustry.modal";
import { IndustryService } from "../../services/industry.service";
import { SubindustryService } from "../../services/subindustry.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-subindustry',
    templateUrl: "./subindustry.component.html",
    animations: []
})


export class SubindustryComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    subindustry: Subindustry = new Subindustry();
    boardType;
    industryList = [];
    subindustryList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private industryService: IndustryService,
        private subindustryService: SubindustryService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]],
            "industryId": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllIndustry();
        this.getAllSubIndustry();
    }

    addIndustry(content) {
        console.log("test");
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveSubIndustry() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.subindustry.mapTo = "SUBINDUSTRY";
                this.subindustryService.update(this.subindustry).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllSubIndustry();
                        console.log("test");
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "SUBINDUSTRY";
                this.subindustryService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllSubIndustry();
                        console.log("test");
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllSubIndustry();

    }
    getAllIndustry() {
        this.industryService.getAllIndustryList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.industryList = response.data;
                console.log(this.industryList)
            } else {
                console.log("Test");
            }
        });
    }
    getAllSubIndustry() {
        this.subindustryService.getAllSubIndustryList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.subindustryList = response.data;
                console.log(this.subindustryList)
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.subindustry.id = education.id;
        this.subindustry.name = education.name;
        this.subindustry.industryId = education.industryId;
        this.subindustry.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.subindustry.activeStatus = "Active";
        } else {
            this.subindustry.activeStatus = "Deactive";
        }
        this.subindustry.mapTo = "INDUSTRY";
        this.subindustry.id = edu.id;
        this.subindustry.name = edu.name;
        this.subindustryService.update(this.subindustry).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllSubIndustry();
            }
        });
    }
}

