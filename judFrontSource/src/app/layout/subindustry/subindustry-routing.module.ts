import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubindustryComponent } from './subindustry.component';

const routes: Routes = [
    {
        path: '', component: SubindustryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubindustryRoutingModule {
}
