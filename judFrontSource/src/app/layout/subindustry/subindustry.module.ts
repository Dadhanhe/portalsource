import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubindustryComponent } from './subindustry.component';
import { SubindustryRoutingModule } from './subindustry-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationModule } from '../../validation/validation.module';

@NgModule({
    imports: [
        CommonModule,
        SubindustryRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ValidationModule
    ],
    declarations: [SubindustryComponent],

})
export class SubindustryModule { }
