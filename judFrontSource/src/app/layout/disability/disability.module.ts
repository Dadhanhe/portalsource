import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisabilityComponent } from './disability.component';
import { DisabilityRoutingModule } from './disability-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationModule } from '../../validation/validation.module';

@NgModule({
    imports: [
        CommonModule,
        DisabilityRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ValidationModule
    ],
    declarations: [DisabilityComponent],

})
export class DisabilityModule { }
