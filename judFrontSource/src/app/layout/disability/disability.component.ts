import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { Disability } from "../../modal/disability.modal";
import { DisabilityService } from "../../services/disability.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-disability',
    templateUrl: "./disability.component.html",
    animations: []
})


export class DisabilityComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    disability: Disability = new Disability();
    boardType;
    disabilityList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private disabilityService: DisabilityService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllDisabilities();
    }

    addDisability(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveDisability() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.disability.mapTo = "DISABILITY";
                this.disabilityService.update(this.disability).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllDisabilities();
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "DISABILITY";
                this.disabilityService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllDisabilities();
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllDisabilities();

    }
    getAllDisabilities() {
        this.disabilityService.getAllDisabilityList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.disabilityList = response.data;
                console.log(this.disabilityList)
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.disability.id = education.id;
        this.disability.name = education.name;
        this.disability.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.disability.activeStatus = "Active";
        } else {
            this.disability.activeStatus = "Deactive";
        }
        this.disability.mapTo = "DISABILITY";
        this.disability.id = edu.id;
        this.disability.name = edu.name;
        this.disabilityService.update(this.disability).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllDisabilities();
            }
        });
    }
}

