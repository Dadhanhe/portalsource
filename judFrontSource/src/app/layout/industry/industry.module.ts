import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndustryComponent } from './industry.component';
import { IndustryRoutingModule } from './industry-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationModule } from '../../validation/validation.module';

@NgModule({
    imports: [
        CommonModule,
        IndustryRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ValidationModule
    ],
    declarations: [IndustryComponent],

})
export class IndustryModule { }
