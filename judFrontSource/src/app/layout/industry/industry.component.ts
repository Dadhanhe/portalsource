import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { Industry } from "../../modal/industry.modal";
import { IndustryService } from "../../services/industry.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-industry',
    templateUrl: "./industry.component.html",
    animations: []
})


export class IndustryComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    industry: Industry = new Industry();
    boardType;
    industryList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private industryService: IndustryService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllIndustry();
    }

    addIndustry(content) {
        console.log("test");
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveIndustry() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.industry.mapTo = "INDUSTRY";
                this.industryService.update(this.industry).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllIndustry();
                        console.log("test");
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "INDUSTRY";
                this.industryService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllIndustry();
                        console.log("test");
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllIndustry();

    }
    getAllIndustry() {
        this.industryService.getAllIndustryList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.industryList = response.data;
                console.log(this.industryList)
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.industry.id = education.id;
        this.industry.name = education.name;
        this.industry.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.industry.activeStatus = "Active";
        } else {
            this.industry.activeStatus = "Deactive";
        }
        this.industry.mapTo = "INDUSTRY";
        this.industry.id = edu.id;
        this.industry.name = edu.name;
        this.industryService.update(this.industry).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllIndustry();
            }
        });
    }
}

