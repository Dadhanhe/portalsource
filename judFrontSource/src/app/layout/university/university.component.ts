import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from "../../validation/validation.service";
import { University } from "../../modal/university.modal";
import { UniversityService } from "../../services/university.service";
import { Utility } from '../../util/utility';
declare var $: any;
@Component({
    selector: 'app-university',
    templateUrl: "./university.component.html",
    animations: []
})


export class UniversityComponent {
    closeResult: string;
    form: FormGroup;
    errorMessage: boolean = false;
    university: University = new University();
    boardType;
    universityList = [];
    content: any;
    isEdit = false;
    constructor(private modalService: NgbModal, private fb: FormBuilder, private router: Router, private universityService: UniversityService) {

    }

    ngOnInit() {
        let _thisI = this;
        this.form = this.fb.group({
            "name": ["", [ValidationService.required]]
        });
        this.form.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
        this.getAllUniversities();
    }

    addUniversity(content) {
        console.log("test");
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }

    ngAfterViewInit() {
        let _thiss = this;


    }
    saveUniversity() {
        if (this.form.dirty && this.form.valid) {
            if (this.isEdit) {
                this.university.mapTo = "UNIVERSITY";
                this.universityService.update(this.university).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllUniversities();
                        console.log("test");
                    }
                });
            } else {
                let body = $.extend({}, this.form.value);
                body.activeStatus = "Active";
                body.mapTo = "UNIVERSITY";
                this.universityService.create(body).subscribe((response: any) => {
                    if (response && response.status == "SUCCESS") {
                        this.getAllUniversities();
                        console.log("test");
                    }
                });
            }
        }
    }
    onChange(newValue) {
        console.log(newValue);
        this.boardType = newValue;
        this.getAllUniversities();

    }
    getAllUniversities() {
        this.universityService.getAllUniversityList(null).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.universityList = response.data;
                console.log(this.universityList)
            } else {
                console.log("Test");
            }
        });
    }
    navigateEdit(event, education, content) {
        this.isEdit = true;
        this.university.id = education.id;
        this.university.name = education.name;
        this.university.activeStatus = education.activeStatus;
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
    deActiveRecord(edu) {
        if (edu.activeStatus == "Deactive") {
            this.university.activeStatus = "Active";
        } else {
            this.university.activeStatus = "Deactive";
        }
        this.university.mapTo = "UNIVERSITY";
        this.university.id = edu.id;
        this.university.name = edu.name;
        this.universityService.update(this.university).subscribe((response: any) => {
            if (response && response.status == "SUCCESS") {
                this.getAllUniversities();
            }
        });
    }
}

