export class Subindustry {
    name: string;
    mapTo: string;
    activeStatus: any;
    id: any
    industryId: any;
    industryName: string;
}