import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { FormComponentModule} from '../form-control/formComponent.module';
import { ControlMessages } from './control-messages.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    //  FormComponentModule
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ControlMessages],
  exports: [ControlMessages]
})
export class ValidationModule { }
