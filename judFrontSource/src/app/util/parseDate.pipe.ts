import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'parseDate', pure: false })
export class ParseDatePipe implements PipeTransform {
    transform(value: any, args: string = null): any {
        var date;
        if (args) {
            if (args == 'dateOnly') {
                var arrays = value.split("-");
                date = new Date(arrays[0], arrays[1] - 1, arrays[2]);
            } else if (args == 'dateTime') {
                var dateString = value;
                var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                var dateArray = reggie.exec(dateString);
                date = new Date(
                    (+dateArray[1]),
                    (+dateArray[2]) - 1, // Careful, month starts at 0!
                    (+dateArray[3]),
                    (+dateArray[4]),
                    (+dateArray[5]),
                    (+dateArray[6])
                );
            }
        }
        return date;
    }
}