import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'mapValues' })
export class MapPipe implements PipeTransform {
    transform(value: any): any[] {
        let returnArray = [];
        for (var i in value) {
            returnArray.push({
                key: i,
                val: value[i]
            });
        }
        return returnArray;
    }
}