import { Pipe, PipeTransform } from '@angular/core';
import { Utility } from './Utility';

@Pipe({ name: 'textLimit' })
export class TextLimitPipe implements PipeTransform {
    transform(value: any, displayCount: number = null): any {

        displayCount = Utility.notBlank(displayCount) ? displayCount : 25;
        if (value && displayCount) {
            let limit = displayCount;
            let trail = '...';

            return value.length > limit ? value.substring(0, limit) + trail : value;
        }
    }
} 