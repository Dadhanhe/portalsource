import { Utility } from './Utility';
declare var _: any;
declare var Date: any;
declare var moment: any;

export class ContestUtility {

    static manageRankingText(contestDetail, currencyPipe) {
        if (Utility.notBlank(contestDetail)) {
            contestDetail.amountText = '';
            contestDetail.amount = null;
            let displayRank = false;
            contestDetail.tenantLocationRegionCurrency = currencyPipe.transform(0, contestDetail.tenantLocationRegionCurrency, 'symbol', '1.0-0').substring(0, currencyPipe.transform(0, contestDetail.tenantLocationRegionCurrency, 'symbol', '1.0-0').length - 1);
            if (Utility.notBlank(contestDetail.contestReportRankings)
                && contestDetail.contestReportRankings.length > 0
            ) {
                _.each(contestDetail.contestReportRankings, function (contestReportRanking, index) {
                    if (Utility.notBlank(contestReportRanking.amount)) {
                        contestDetail.amount = contestReportRanking.amount;
                        if (Utility.notBlank(contestDetail.amountText) && index > 0) {
                            contestDetail.amountText += ', ';
                        }
                        contestDetail.amountText += (contestDetail.tenantLocationRegionCurrency + ' ' + contestReportRanking.amount);
                        if (displayRank) {
                            contestDetail.amountText += (' (Rank ' + contestReportRanking.rank + ') ');
                        }
                    }
                });
            }
            contestDetail.amountText = Utility.notBlank(contestDetail.amountText) ? contestDetail.amountText : '-';
        }
    }

}