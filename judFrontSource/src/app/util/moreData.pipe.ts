import { Pipe, PipeTransform } from '@angular/core';
import { Utility } from './Utility';

@Pipe({ name: 'moreData' })
export class MoreDataPipe implements PipeTransform {
    transform(value: any, displayCount: number = null): any {
        let returnValue: any = "";
        let toolTip: any = "";
        let cnt = 0;
        displayCount = Utility.notBlank(displayCount) ? displayCount : 1;
        if (value && displayCount) {
            if (value.indexOf(',') != -1) {
                let arr = value.split(',');
                arr.forEach(ar => {
                    if (cnt == 0) {
                        returnValue = ar;
                    } else if (cnt < displayCount && cnt != 0) {
                        returnValue = returnValue + ", " + ar;
                    } else {

                    }
                    cnt++;
                });
                /*for (cnt = cnt; cnt < arr.length; cnt++) {
                    toolTip = toolTip + arr[cnt] + ", ";
                    <span title='" + toolTip + "'></span>
                }*/
                if (arr.length > displayCount) {
                    returnValue = returnValue + " & " + (arr.length - displayCount) + " more";
                }
            } else {
                return value;
            }
        } else {
            return value;
        }
        return returnValue;
    }
}