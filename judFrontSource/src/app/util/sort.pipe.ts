import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sortPipe' })
export class SortPipe implements PipeTransform {
    transform(arr: any[], sortBy: string, sort: string): any[] {
        return arr == null ? null : arr.sort((a, b) => {
            if (sort && sort.toLowerCase() == "desc") {
                if (a[sortBy] < b[sortBy]) {
                    return 1;
                }
                if (a[sortBy] > b[sortBy]) {
                    return -1;
                }
            } else {
                if (a[sortBy] > b[sortBy]) {
                    return 1;
                }
                if (a[sortBy] < b[sortBy]) {
                    return -1;
                }
            }
            return 0;
        });
    }
}