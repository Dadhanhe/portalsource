import { Pipe, PipeTransform } from '@angular/core';
import { Utility } from './Utility';
declare var Date: any;
declare var moment: any;

@Pipe({ name: 'formatDate', pure: false })
export class FormatDatePipe implements PipeTransform {
    transform(value: any, args: string = null): any {
        var strDate;
        if (args && value) {
            if (args == 'dateOnly') {
                strDate = moment(value).format('YYYY-MM-DD');
            } else if (args == 'dateTime') {
                strDate = moment(value).format('YYYY-MM-DD HH:mm:ss');
            } else if (args == 'fullDateTime') {
                strDate = moment(value).format('MMM DD, YYYY hh:mm:ss a');
            } else {
                try {
                    strDate = moment(value).format(args);
                } catch (err) {
                    strDate = "INVALID FORMAT";
                    console.log("formatDate.pipe.1 - error :" + err);
                }
            }
        }
        return strDate;
    }
}