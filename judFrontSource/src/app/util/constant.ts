export const MAX_RECORD = 1000000;
export const DASHBOARD_HOME = "/dashboard/home";
export const GOOGLE_ANALYTICS_ID: string = 'app.google.analytics.id';
export const IS_LDAP_AUTHENTICATION: string = 'enable.ldap.authentication';
export const MASTER_LOCATION_TOOLTIP: string = 'system.masterLocation.info';
export const TEAM_LOCATION_TOOLTIP: string = 'system.teamLocation.info';
export const SESSION_EXPIRED: string = 'app.session.expired';
export const DEFAULT_MIN_COLUMN: string = 'default.min.column';

export const DEFAULT_INDUSTRY: string = 'user.default.industry';
