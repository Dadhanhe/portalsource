import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapToArrayPipe } from './mapToArray.pipe';
import { MapPipe } from './map.pipe';
import { MoreDataPipe } from './moreData.pipe';
import { TextLimitPipe } from './textLimit.pipe';
import { SafePipe } from './safe.pipe';
import { FilterAndGroupArrayPipe } from './filterAndGroupArray.pipe';
import { SearchPipe, SearchWithParameterPipe } from './search.pipe';
import { SafeHtmlPipe } from './safeHtml.pipe';
import { JConvensionPipe } from './jConvension.pipe';
import { FormatDatePipe } from './formatDate.pipe';
import { EnumToArrayPipe } from './enumToArray.pipe';
import { SortPipe } from './sort.pipe';
import { ParseDatePipe } from './parseDate.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MapToArrayPipe, MapPipe, MoreDataPipe, SafePipe, TextLimitPipe,
    FilterAndGroupArrayPipe, SearchPipe, SearchWithParameterPipe, SafeHtmlPipe, JConvensionPipe,
    FormatDatePipe, EnumToArrayPipe, SortPipe, ParseDatePipe],
  exports: [MapToArrayPipe, MapPipe, MoreDataPipe, SafePipe, TextLimitPipe,
    FilterAndGroupArrayPipe, SearchPipe, SearchWithParameterPipe, SafeHtmlPipe, JConvensionPipe,
    FormatDatePipe, EnumToArrayPipe, SortPipe, ParseDatePipe]
})
export class PipeModule { }
