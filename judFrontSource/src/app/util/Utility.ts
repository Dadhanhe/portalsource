
declare var _: any;
declare var Date: any; declare var moment: any;
// declare var Appcues;
declare var Highcharts: any;
export class Utility {
    static notBlank(val) {
        return val == undefined || val == null
            || (typeof val == "string" && val.trim() == "") || (val instanceof Array && val.length < 1) ? false : true;
    }
    public static blank(val) {
        return val == undefined || val == null
            || (typeof val == "string" && val.trim() == "") || (val instanceof Array && val.length < 1) ? true : false;
    }
}