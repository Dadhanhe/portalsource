import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'JConvension' })
export class JConvensionPipe implements PipeTransform {
    transform(value: any): any {
        let returnValue: any = "";
        let returnValueArr: any[] = [];
        let cnt = 0;
        if (value) {
            if (value.length > 1) {
                if (value.indexOf('_') != -1) {
                    let arr = value.split('_');
                    arr.forEach(ar => {
                        if (cnt == 0) {
                            returnValueArr.push(ar.charAt(0).toLowerCase() + ar.slice(1).toLowerCase());
                        } else {
                            returnValueArr.push(ar.charAt(0).toUpperCase() + ar.slice(1).toLowerCase());
                        }
                        cnt++;
                    });
                    returnValue = returnValueArr.join('');
                } else {
                    returnValue = value.charAt(0).toLowerCase() + value.slice(1);
                }
            } else {
                returnValue = value.toLowerCase();
            }
        } else {
            return value;
        }
        return returnValue;
    }
}