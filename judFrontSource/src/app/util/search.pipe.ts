import { Pipe, PipeTransform } from '@angular/core';
import { Utility } from './Utility';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {
    filterItem;

    transform(items: any[], args: any): any {
        if (Utility.notBlank(items) && Utility.notBlank(args) && Array.isArray(items)) {
            return items.filter(item => Utility.notBlank(item.name) && item.name.toLowerCase().indexOf(args.toLowerCase()) !== -1);
        } else {
            return items;
        }
    }
}
@Pipe({
    name: 'SearchWithParameter'
})
export class SearchWithParameterPipe implements PipeTransform {
    transform(items: any[], args: any): any {
        if (Utility.notBlank(items) && Array.isArray(items)
            && Utility.notBlank(args) && Utility.notBlank(args.col) && Utility.notBlank(args.value)) {
            return items.filter(item => Utility.notBlank(item[args.col.toString()]) && item[args.col.toString()].toString().indexOf(args.value.toString().toLowerCase()) !== -1);
        } else {
            return items;
        }
    }
}