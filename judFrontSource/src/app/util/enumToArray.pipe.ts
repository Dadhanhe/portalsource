import { PipeTransform, Pipe } from "@angular/core";
import { Utility } from "./Utility";

@Pipe({
    name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
    transform(data: Object) {
        let arr = [];
        if (Utility.notBlank(data)) {
            let keys = Object.keys(data);
            for (var i = 0; i < keys.length / 2; i++) {
                let obj = {
                    value: keys[i],
                    key: keys[i + keys.length / 2]
                }
                arr.push(obj);
            }
        }
        return arr;
    }


}