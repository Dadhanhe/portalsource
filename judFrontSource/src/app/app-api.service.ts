import { ValidationService } from './validation/validation.service';
import { GlobalService } from './services/global.service';
import { LocalStorageService } from './services/local-storage.service';
import { EducationBoardService } from './services/educationBoard.service';
import { UniversityService } from './services/university.service';
import { DisabilityService } from './services/disability.service';
import { UserService } from './services/user.service';
import { LoginService } from './services/login.service';
import { CourseService } from './services/course.service';
import { IndustryService } from './services/industry.service';
import { SubindustryService } from './services/subindustry.service';

export const ApiServices = [
    ValidationService,
    GlobalService,
    LocalStorageService,
    EducationBoardService,
    UserService,
    LoginService,
    UniversityService,
    DisabilityService,
    CourseService,
    IndustryService,
    SubindustryService
]