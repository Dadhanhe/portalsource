import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Utility } from '../util/Utility';
import { UserService } from '../services/user.service';
import { GlobalService } from '../services/global.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../validation/validation.service';
import { LoginService } from '../services/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    showValidationMsg: boolean = false;
    signInForm: any;
    userId: any;
    errorMessage: boolean = false;
    auth: any;
    constructor(public router: Router, private userService: UserService, public globalService: GlobalService, private formBuilder: FormBuilder, private loginService: LoginService) {
        this.signInForm = this.formBuilder.group({
            'email': ['', [ValidationService.required, ValidationService.emailValidator]],
            // 'password': ['', [ValidationService.required, ValidationService.passwordValidator]]
            'password': ['', [ValidationService.required]]
        });
        this.signInForm.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
    }

    ngOnInit() {

    }



    doLogin(token?: any) {
        // show validation only if form submitted once.
        if (!this.showValidationMsg) {
            this.showValidationMsg = true;
        }
        if (Utility.blank(token) && (this.signInForm.dirty && this.signInForm.valid)) {
            // get credentials
            // this.signInForm.value.email = this.signInForm.value.email.trim();
            var email = this.signInForm.value.email;
            if (Utility.notBlank(email)) {
                email = email.trim();
            }
            var password = this.signInForm.value.password;
            this.userService.authenticate(email, password).subscribe((response: any) => {
                if (response && response.status == "SUCCESS") {
                    this.userDetail(response.data);
                } else {
                    if (response.data) {
                        let errorFields = response.data[0];
                        if (this.signInForm.controls[errorFields.field]) {
                            this.signInForm.controls[errorFields.field].markAsTouched();
                            this.signInForm.controls[errorFields.field].setErrors([errorFields.message]);
                        } else {

                        }
                    }
                }
            });
        } else if (Utility.notBlank(token)) {
            this.userDetail({ access_token: token });
        }
        return false;
    }
    userDetail(auth) {
        let thiss = this;
        this.userService.getUser(auth).subscribe((res: any) => {
            thiss.globalService.setUser(res.data);
            thiss.userId = thiss.globalService.getUser().id;
        });
        this.auth = auth;
        // this.loginService.setToken(this.auth);
        localStorage.setItem('isLoggedin', 'true');
        this.loginService.setToken(auth);
        thiss.router.navigate(['/dashboard']);
    }
}
