/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.job.portal.pojo.enums.IndustryType;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class test.
 */
public class test {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(test.class);

	/**
	 * Main 1.
	 *
	 * @param args
	 *            the args
	 * @throws ParseException
	 *             the parse exception
	 */
	public static void main1(final String[] args) throws ParseException {

		final Calendar c = Calendar.getInstance();

		Date toDate = new Date();
		toDate = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-28");
		c.setTime(toDate);
		c.set(Calendar.DATE, 1);
		final Date fromDate = c.getTime();

		c.setTime(toDate);
		final int day = c.get(Calendar.DAY_OF_MONTH);
		if (day == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			c.add(Calendar.MONTH, -1);
			c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		} else {
			c.add(Calendar.MONTH, -1);
			if (day > c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				c.set(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
		}
		final Date previousToDate = c.getTime();
		c.set(Calendar.DATE, 1);
		final Date previousFromDate = c.getTime();

		System.out.println("fromDate :: " + fromDate);
		System.out.println("toDate :: " + toDate);
		System.out.println("previousFromDate :: " + previousFromDate);
		System.out.println("previousToDate :: " + previousToDate);

		final String pass = "123456";
		final String pwd1 = DigestUtils.md5Hex(pass);
		System.out.println(pwd1);
		System.out.println(
				DigestUtils.md5Hex(pwd1 + "53cf87290c90a8cdfdd97ae8ad589f41"));

		final String json = "json123";
		System.out.println(json.matches("^json[0-9]+$"));

		System.out
				.println(IndustryType.Hotel.getValueByIndustry("NissanSales"));
		final String[] s = "1,2".split(",");
		System.out.println(s[s.length - 1]);
		final String[] s1 = "1".split(",");
		System.out.println(s1[s1.length - 1]);

		final String selectedCompCode = null;
		List<String> selectedCompCodes = new ArrayList<String>();
		if (!Objects.isEmpty(selectedCompCode)) {
			selectedCompCodes = Arrays.asList(selectedCompCode.split(","));
		}
		System.out.println(selectedCompCodes.contains("S"));
		System.out.println("-----------------------------------------");
		System.out.println(
				NumberUtils.isNumber("-1") || NumberUtils.isDigits("-1"));
		System.out.println(
				NumberUtils.isNumber("-1.2") || NumberUtils.isDigits("-1.2"));
		System.out.println(
				NumberUtils.isNumber(".2") || NumberUtils.isDigits(".2"));
		System.out.println(
				NumberUtils.isNumber(".2.") || NumberUtils.isDigits(".2."));
		System.out.println(
				NumberUtils.isNumber("2.0") || NumberUtils.isDigits("2.0"));
		System.out.println(
				NumberUtils.isNumber("+2.0") || NumberUtils.isDigits("+2.0"));
		System.out.println(
				NumberUtils.isNumber("+2") || NumberUtils.isDigits("+2"));
		System.out.println(
				NumberUtils.isNumber("2") || NumberUtils.isDigits("2"));
		System.out.println(
				NumberUtils.isNumber("2.2") || NumberUtils.isDigits("2.2"));
	}

	/**
	 * Copy file using stream.
	 *
	 * @param source
	 *            the source
	 * @param dest
	 *            the dest
	 */
	private static void copyFileUsingStream(final File source,
			final File dest) {
		FileInputStream fis = null;
		try (FileOutputStream fos = new FileOutputStream(dest)) {
			fis = new FileInputStream(source);
			final byte[] buffer = new byte[1024];
			int length;
			while ((length = fis.read(buffer)) > 0) {
				fos.write(buffer, 0, length);
			}
		} catch (final Exception ex) {
			ObjectUtils.logException(logger, ex,
					"Unable to copy file:" + ex.getMessage());
		} finally {
			// Close the streams
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (final IOException e) {
				ObjectUtils.logException(logger, e, null);
			}
		}
	}

	public static void main(final String[] args) throws ParseException {
		final Map<String, String> a = new HashedMap();
		a.put("a", null);
		a.put("b", "b");
		System.out.println(a.get("a"));
		System.out.println(a.get("b"));
		System.out.println(a.get("c"));
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws ParseException
	 *             the parse exception
	 */
	public static void main3(final String[] args) throws ParseException {

		final String sourceFolder = "D:/Dhaval/D Drive/builds/PRODUCTION 12012018/Hospitality/processed_files_1701/pulse-home/processed-files";
		final String targetFolder = "D:/Dhaval/D Drive/builds/PRODUCTION 12012018/Hospitality/processed_files_1701/pulse-home/filtered";

		final File sFile = new File(sourceFolder);
		// Find files with specified extension
		final File[] sourceFiles = sFile.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(final File dir, final String name) {
				final List<String> listt = new ArrayList<String>();
				listt.add("da440873-caf6-484c-b503-c10e42ed3924.xlsx");
				listt.add("24ca457b-4717-4dde-9554-1517a960923b.xlsx");
				listt.add("ef5eb068-b5ea-4067-99a6-7ae0c0bd61b3.xlsx");
				listt.add("6eb9d5be-8078-47ee-a751-7878e55ce351.xlsx");
				listt.add("0f6f75a3-6ffd-4fb0-98cf-aebf9b3affe9.xlsx");
				listt.add("7e02796d-ea0c-4d20-9131-f458587202bd.xlsx");
				listt.add("349fe850-f9b6-4bba-89cc-c76969feaddc.xlsx");
				listt.add("6a843b30-4247-41af-9474-e87ef86f5ecb.xlsx");
				listt.add("8dd977fd-9fa3-4729-a174-7635662f4497.xlsx");
				listt.add("7fdf1bdf-b27d-4563-99e6-877191e3cc77.xlsx");
				listt.add("5c4d8f57-4287-4ad6-82c9-a0e11b5e8759.xlsx");
				listt.add("9be50f49-2fda-486a-9a06-1f8d15409a3e.xlsx");
				listt.add("56f052df-5185-4c37-9cb4-de5e3c1e98d7.xlsx");
				listt.add("30c81e6f-37e4-4f3b-8cc9-4b68913113c8.xlsx");
				listt.add("6c0eccca-0a61-42c8-b9dd-92dae3773190.xlsx");
				listt.add("2444ccea-722e-4cd8-a0e9-47fe54d9fa25.xlsx");
				listt.add("60f0ec60-a70b-425f-821e-c778848a126d.xlsx");
				listt.add("d52792f4-fd30-4bcc-909b-c7547947ca73.xlsx");
				listt.add("b8451c41-a5b9-4247-84d2-10e661948f5b.xlsx");
				listt.add("acbabc9a-8388-4f12-afdb-b7e14012ea46.xlsx");
				listt.add("abaa6842-b07d-471a-97c4-8a8e7d8c9ab2.xlsx");
				listt.add("13b04d6e-1585-41c4-ae8e-5fd2bc04133b.xlsx");
				listt.add("e4488d6d-62ca-4155-811e-e7667288c16f.xlsx");
				listt.add("c8eff9cf-39af-4948-b102-3ea0420736f9.xlsx");
				listt.add("17c49e2e-b41f-44a3-b0c9-2f3132fb7313.xlsx");
				listt.add("d38cc00a-e908-4f32-ad8b-d28b74bfe53b.xlsx");
				listt.add("b5c95637-f8a6-4d66-860b-53e8437d2856.xlsx");
				listt.add("26c86970-eedd-4d70-9a16-d954da9342c7.xlsx");
				listt.add("ab0d38a2-a001-4345-a679-fd7622abe9cb.xlsx");
				listt.add("bde1e02d-49d3-4c2e-8630-af57ce3fcbae.xlsx");
				listt.add("184c7cef-dc5e-46a5-b0b8-dae3c4f8c4d1.xlsx");
				listt.add("c40837d5-c821-4a4e-bc5c-960ca29e881f.xlsx");
				listt.add("4808ec77-b8ed-4b89-8230-d3d4667dbc98.xlsx");
				listt.add("1f366b7a-1517-4e25-9e03-066caa51c740.xlsx");
				listt.add("4870ff66-ee1a-41c2-856f-65f7d08fdb1c.xlsx");
				listt.add("e26ecc11-3871-48ab-a30a-dfb5cc5b43ed.xlsx");
				listt.add("22d4ad72-71d9-4758-a80a-9e874d42ccf8.xlsx");
				listt.add("aa966462-c408-46f6-bfff-e0868798266c.xlsx");
				listt.add("cfc03f0c-ad0a-41c0-8aae-5f4c9f33d6ff.xlsx");
				listt.add("e2e0df80-10ad-4b1a-8593-ca72825ea1ef.xlsx");
				listt.add("39219ee0-2de2-414f-bba5-585dbf983bce.xlsx");
				listt.add("afb3bda3-6da3-4bad-bb79-48b1f65f5c1c.xlsx");
				listt.add("ce92fbab-ed66-4807-8e46-59e8a58d0c54.xlsx");
				listt.add("8a80b541-2490-48ab-a25c-73b4509590d0.xlsx");
				listt.add("fd659e83-cc32-47dc-a92d-b0591601dec9.xlsx");
				listt.add("1f79b66b-c77d-43aa-85f5-311af962bc5b.xlsx");
				listt.add("75ff0006-213c-41b6-a44d-9b42908a63bf.xlsx");
				listt.add("1d860b29-cf42-4cbd-a2ef-9c4c9901a73b.xlsx");
				listt.add("fde52a5f-2390-40b1-b034-7446a4ba2b3a.xlsx");
				listt.add("9b6b1bc2-d611-41af-810b-64fc05002d8d.xlsx");
				listt.add("429718dc-1d63-4e90-8861-8811f475b0b3.xlsx");
				listt.add("423dcde3-011c-4066-9ee1-841897b271a6.xlsx");
				listt.add("a3e21b45-1aef-49c2-982e-231a91260778.xlsx");
				listt.add("d78c6002-b91d-4871-8a75-15af182e57a3.xlsx");
				listt.add("bddae607-1a11-42d7-8bda-1f486675367d.xlsx");
				listt.add("bd89f612-0e24-4ede-9626-665039b8f872.xlsx");
				listt.add("d65d0297-2bc3-4083-8783-4b23839e1a95.xlsx");
				listt.add("ae54640a-f2cc-4b37-bb09-4d934ae9d0aa.xlsx");
				listt.add("b152cd15-468f-4172-b6f9-dedb6fc0b0bf.xlsx");
				listt.add("7f6819f4-3425-4894-8b4c-49658afa4292.xlsx");
				listt.add("c01262cb-a645-4235-b99d-c25652ffbb7e.xlsx");
				listt.add("83ff7c72-2fea-4a80-a4c6-a72177896b63.xlsx");
				listt.add("11356419-4c22-4de3-b10e-60dcb1dc464d.xlsx");
				listt.add("a1923090-2bc9-4f69-846c-6c54a5171c2c.xlsx");
				listt.add("eced5269-a476-4bdc-ac61-b2ae3a46c651.xlsx");
				listt.add("bb719b07-c6dc-4af1-b95c-986b67a98fc6.xlsx");
				listt.add("194e1fa2-d133-4449-b071-f64248d3deba.xlsx");
				listt.add("586b412f-d036-45c5-906b-25bca303c132.xlsx");
				listt.add("093ddcbe-913c-4666-88a7-61e9c38963ca.xlsx");
				listt.add("1d9c7b1d-0649-409a-8531-2b6ac9f74d08.xlsx");
				listt.add("4279c8ad-8016-4213-b960-091a63661e13.xlsx");
				listt.add("b16a6a22-e46a-405a-8450-2df1315d2df7.xlsx");
				listt.add("4666054d-8f43-42ac-9586-2ed6bd0f049a.xlsx");
				listt.add("65e974e4-44b7-4932-9a03-789169a345c5.xlsx");
				listt.add("8fb99a2d-1272-4289-8719-5391da9c3994.xlsx");
				listt.add("c7b4fa07-046b-4b3f-bb51-6901528e111a.xlsx");
				listt.add("0fb46ec0-0cdb-4837-939f-38b584dff994.xlsx");
				listt.add("e2c0a8dd-3be1-4030-a1e7-d64ccfb7b177.xlsx");
				listt.add("758f73c2-9b8b-49ee-bc6f-909622c20609.xlsx");
				listt.add("7fb81d08-ecbf-48b3-bbeb-1157a5bacb4a.xlsx");
				listt.add("0a7945c9-b5f4-4d8c-88a9-a0357fc85a19.xlsx");
				listt.add("e63dbb81-8d81-4e56-81b8-ba1194be91fe.xlsx");
				listt.add("75d674c7-d737-47bc-a9de-ea89cc060a1e.xlsx");
				listt.add("54c9e941-8ff8-4829-96bc-18b8fb5384c7.xlsx");
				listt.add("553212bb-02cb-4cb7-b888-72827c493825.xlsx");
				listt.add("c82a5ffd-5b4b-4397-814d-5233a450e64d.xlsx");
				listt.add("0fd8d80e-c394-4478-bbff-af58462d9ba6.xlsx");
				listt.add("9ec2a206-7dd4-4d72-840c-3342a04f97d4.xlsx");
				listt.add("d2369d7d-9728-4455-b262-8ea4843a9c0f.xlsx");
				listt.add("ff03567f-5067-4e0a-ad5a-851770c1d785.xlsx");
				listt.add("e6cd2958-0e62-4586-96e2-da047623e512.xlsx");
				listt.add("a3181f03-ace6-49f2-afab-6c50d5ebc922.xlsx");
				listt.add("eba20996-b287-4bdb-9e2d-0e4904e61b8d.xlsx");
				listt.add("0c152995-9212-4679-a06d-f0e708ac3de4.xlsx");
				listt.add("7d528532-2a08-4a8d-ae6d-0e4eecc3a3b2.xlsx");
				listt.add("9a546535-1655-4e35-8dbf-1beabf8e9dd3.xlsx");
				listt.add("47755cdd-ba9b-4810-958d-e01bd90ae967.xlsx");
				listt.add("d1038d94-b1ba-4694-b4a7-b29c2bd4f2a0.xlsx");
				listt.add("b1091c9e-7a14-4c32-91ce-b59966c26bd4.xlsx");
				listt.add("b4a84f1b-cd58-4199-831a-7152f824e0bd.xlsx");
				listt.add("fea67968-2ea2-4580-8cf8-45748888e750.xlsx");
				listt.add("81079ee7-6b0d-4f87-83b5-a0a317d9c879.xlsx");
				listt.add("3c57c7f8-5dc0-4da9-9934-2732aee3fdc5.xlsx");
				listt.add("084e1c6d-f8a4-4164-b0dc-893a16c782ac.xlsx");
				listt.add("1edbacd5-efb3-42fa-b133-566cc14145ed.xlsx");
				listt.add("642b7b75-537a-4cbd-96c6-19f01a558b01.xlsx");
				listt.add("19a13949-5081-48f2-ac8d-c0d590dc1f8d.xlsx");
				listt.add("630661ed-b947-4d9a-b5ef-9157eea1413e.xlsx");
				listt.add("faefe6e1-58ca-4734-97d9-135b60d176a2.xlsx");
				listt.add("f5882b37-69cd-4ab8-9775-d62c63c8fe45.xlsx");
				listt.add("73a1331e-e00d-451a-b8b9-ea13508a520b.xlsx");
				listt.add("be3de20e-91ef-4d9c-8e61-a7d51c8f24e9.xlsx");
				listt.add("1e021d25-51c2-4f02-b1e0-8e6ce56c570e.xlsx");
				listt.add("5db8e25c-e842-446c-95fa-59b6b54b44b7.xlsx");
				listt.add("5b30a91a-9526-44ce-9742-0d932019764d.xlsx");
				listt.add("9a42c816-15dd-4f75-8869-1ef745b73b5f.xlsx");
				listt.add("f44d8769-a31f-4e93-aa7e-1911da91fe70.xlsx");
				listt.add("ac2236f0-2707-4265-8df6-474d0803d3f5.xlsx");
				listt.add("2ba779d6-8898-4183-bc22-6fa5a663fba3.xlsx");
				listt.add("40b8611a-ca9c-46a7-9cd8-eb6877d414f8.xlsx");
				listt.add("05a31c5c-c568-40bf-835f-9aad12e57b74.xlsx");
				listt.add("d5e9f03b-9e68-4c5c-9443-2972e1fb948d.xlsx");
				listt.add("cb1140c3-bb7e-40f2-8d80-733bd5a038eb.xlsx");
				listt.add("a6ce807d-d674-4a84-8512-b9ef9b894817.xlsx");
				listt.add("a1d206f9-1433-4f3d-b8cd-3c0012c74829.xlsx");
				listt.add("c84325c7-25b9-4c28-84a5-63da6c034f62.xlsx");
				listt.add("783b43b3-d4df-4807-8f7b-d90553ae6c09.xlsx");
				listt.add("0a4d2a37-57a3-43a2-b6c2-f407a4d0e9f2.xlsx");
				listt.add("db72b76e-24a0-4547-adf4-00bfb14873b6.xlsx");
				listt.add("e9ee7772-6c9c-4f55-845f-108a4854b080.xlsx");
				listt.add("796e509e-2d11-426f-a925-94b2524d557c.xlsx");
				listt.add("e5d046f7-c98e-446b-b2f0-91647dcb324a.xlsx");
				listt.add("0990821c-e444-401f-b494-496fe1a79557.xlsx");
				listt.add("0e3ac1f6-d4aa-42b2-8ca2-e60dda67aafe.xlsx");
				listt.add("d33ced90-3047-4d17-8ba8-b8b47bc7494a.xlsx");
				listt.add("72b6664a-441c-4b9e-9ea5-d2c4e2b1eaf2.xlsx");
				listt.add("9ff5cb1e-5f35-46a9-8f5b-53fc448ba791.xlsx");
				listt.add("fcdc6021-ba0c-469d-9b97-ce0032c5a902.xlsx");
				listt.add("4ab34b67-f990-42f2-882d-e3aed4b1abe1.xlsx");
				listt.add("d0fc1493-b2ae-4acd-b3b2-e327e03e10e5.xlsx");
				listt.add("204d4dee-c73f-4bb7-8f19-7b05eecbb4e8.xlsx");
				listt.add("27a50f24-9835-4517-8981-b1be1b34d205.xlsx");
				listt.add("eb2ab4dc-c8c9-4df8-bfc8-d74be78e214d.xlsx");
				listt.add("4178f236-be69-4387-a48b-769d955157d2.xlsx");
				listt.add("15616578-1f99-40e3-8fab-68a457d02db6.xlsx");
				listt.add("9eb45f96-b2d7-4589-8a0f-5290535c8a48.xlsx");
				listt.add("c0d1e460-662f-45a2-bf7b-c9a1c8f3a1d9.xlsx");
				listt.add("205925b9-32b9-4507-9337-15433617be28.xlsx");
				listt.add("c44bcea6-01ee-4e0d-8090-30cfa56a8b7c.xlsx");
				listt.add("59716c73-2345-4299-a3dc-790720af034f.xlsx");
				listt.add("940d570c-d89f-4cd5-bd6f-26ebbdd9c47f.xlsx");
				listt.add("ca3ae132-4883-441f-8fcd-2f46e93afe5c.xlsx");
				listt.add("731f57f3-91c7-433d-9c25-36a9e02d66d3.xlsx");
				listt.add("6e5d69cb-c968-46a7-a1d2-b155cb19167b.xlsx");
				listt.add("52fc5a9f-6830-47d7-a3bd-3555d6df5f72.xlsx");
				listt.add("cf173aa1-77a8-46a7-a6a7-afdd99909215.xlsx");
				listt.add("ecd0d71c-0853-4f59-8ddc-59368cfc5df2.xlsx");
				listt.add("bb58c14e-0bc6-4752-8523-6ae09e82f31f.xlsx");
				listt.add("3d7fe156-89f1-4816-909d-7fe09326aa96.xlsx");
				listt.add("53d9674a-2702-4751-9508-ed2afb396826.xlsx");
				listt.add("dfacf44a-acff-4bf7-8427-a11d015746fb.xlsx");
				listt.add("1d18979a-e1be-4e08-a051-454d6542b2d8.xlsx");
				listt.add("e837a17d-7fff-47b5-ad9c-c603b2d6c53d.xlsx");
				listt.add("27cdacdf-9fec-4323-a0da-ac2d8082cf61.xlsx");
				listt.add("353d8e6d-0149-4469-94fc-61b827573aec.xlsx");
				listt.add("37a34173-748c-46d9-be40-723af88ff1c8.xlsx");
				listt.add("17917cc5-98ae-42de-87c3-976273b825e6.xlsx");
				listt.add("e486e396-2717-4759-8532-758d7927b8fa.xlsx");
				listt.add("a566f13e-c8f4-44a6-9fbf-23f1b4444607.xlsx");
				listt.add("a9b8c457-bd67-425a-b1ed-0a30f2f67ecf.xlsx");
				listt.add("d146b296-68b8-4b6a-822a-148399716c74.xlsx");
				listt.add("3baccdbd-c357-4b2e-927d-411bc0d143d1.xlsx");
				listt.add("b7d27cb7-3491-4c88-b7d2-355df75f7e68.xlsx");
				listt.add("328ae752-6ca3-4bfa-b394-ff9086acac41.xlsx");
				listt.add("f91ae94f-810c-44b7-bcd7-6af61234d43c.xlsx");
				listt.add("43d64dbd-35fb-4c5f-9638-2f85a5a8e8d0.xlsx");
				listt.add("f6c488b4-6966-45b1-8a4a-7ae84e56e3f9.xlsx");
				listt.add("b8fbf67f-a937-47b9-ab1a-8797a2e417eb.xlsx");
				listt.add("bed54137-426f-49b2-b9d9-336ffc7ff89c.xlsx");
				listt.add("aea80369-84e1-4685-a943-5f65cb539939.xlsx");
				listt.add("88040d35-2cc9-48d8-8bbc-6c1971252bbe.xlsx");
				listt.add("1f129f22-2d8c-4df7-a0b7-e49c161fe2f9.xlsx");
				listt.add("ab3c2ca0-43fa-417d-989b-0539af1def71.xlsx");
				listt.add("56d20b90-a313-43db-8bef-e14b4a545928.xlsx");
				listt.add("0f5ebc7e-2788-449f-a4d5-9766489cdc38.xlsx");
				listt.add("6e671615-3ff5-4146-ab28-50676a87c377.xlsx");
				listt.add("be8222c0-cf0b-4c98-83fe-2f1d66a49eca.xlsx");
				listt.add("3489df9d-83d3-41b9-9a5c-f2ee339c4ade.xlsx");
				listt.add("d1c2e8bb-3cd8-41f5-8126-0d1d6effa3c2.xlsx");
				listt.add("9fcef7da-6ae3-428f-94b6-fdee026cabbb.xlsx");
				listt.add("81c039ea-b404-4120-93c9-5e611a92661e.xlsx");
				listt.add("dd9c1829-a733-40b1-8746-03ef383ba012.xlsx");
				listt.add("69f66396-7634-4d45-af12-a68a1efc9620.xlsx");
				listt.add("fb074af4-c9cb-4607-ac9d-398cebc84d60.xlsx");
				listt.add("c3110fc3-804b-43c9-b4e0-e63a5675e09c.xlsx");
				listt.add("2d54edff-b2f2-449b-ab78-66f49645b6fa.xlsx");
				Boolean bool = false;
				for (final String string : listt) {
					if (name.equalsIgnoreCase(string)) {// change this to your
														// extension
						bool = true;
					}
				}
				return bool;
			}
		});

		// let us copy each file to the target folder
		for (final File fSource : sourceFiles) {
			final File fTarget = new File(new File(targetFolder),
					fSource.getName());
			copyFileUsingStream(fSource, fTarget);
			// fSource.delete(); // Uncomment this line if you want source file
			// deleted
		}

		final ArrayList<File> files = new ArrayList<File>();
		test.listf("D:/FPG/pulse-new-framework/source/ng-pulse/src", files);
		for (final File f : files) {
			if (f.getName().endsWith("routes.ts")
					|| f.getName().endsWith("route.ts")) {
				System.out.println(f.getAbsolutePath());
				try {
					final FileReader fr = new FileReader(f);
					String s;
					String totalStr = "import { NgModule } from \"@angular/core\";\n";
					try (BufferedReader br = new BufferedReader(fr)) {
						while ((s = br.readLine()) != null) {
							if (s.contains("export default")) {
								final String temps = s;
								s = s.replace(
										"export default RouterModule.forChild(",
										"");
								s = s.replace(");", "");
								s = "@NgModule(\n\t{\n\t\timports: [RouterModule.forChild("
										+ s
										+ ")],\n\t\texports: [RouterModule]\n\t})\nexport class "
										+ s + "Module { }";
								s = s + "\n//" + temps;
							}
							s = s + "\n";
							totalStr += s;
						}
						// totalStr = totalStr.replaceAll(search, replace);
						final FileWriter fw = new FileWriter(f);
						fw.write(totalStr);
						fw.close();
						// String rename =
						// f.getAbsolutePath().replace(".routes",
						// "-routes.module");
						// rename = rename.replaceAll("\\\\", "\\\\\\\\");
						// System.out.println(f.getAbsolutePath());
						// System.out.println(rename);
						// final boolean success = f.renameTo(new File(rename));
						// if (success) {
						// System.out.println("Rename ==>" + f.getName());
						// } else {
						// System.out.println("Fail Rename ==>" + f.getName());
						// }
					}
				} catch (final Exception e) {
					ObjectUtils.logException(logger, e, null);
				}
			} else if (f.getName().endsWith("module.ts")
					|| f.getName().endsWith("modules.ts")) {
				System.out.println(f.getAbsolutePath());
				try {
					final FileReader fr = new FileReader(f);
					String s;
					String totalStr = "";
					try (BufferedReader br = new BufferedReader(fr)) {
						String routeModule = "";
						while ((s = br.readLine()) != null) {
							if (s.contains("export") && s.contains("default")) {
								s = s.replace("default", "");
							}
							if (s.contains("import") && (s.contains(".routes")
									|| s.contains(".route"))) {
								routeModule = s.substring(s.indexOf(" ") + 1,
										s.indexOf(" ") + (s
												.substring(s.indexOf(" ") + 1)
												.indexOf(" ")));
								s = s.replace(routeModule,
										"{ " + routeModule + "Module }");
								s = s.replace(".routes", "-routes.module");
							}
							s = !routeModule.equals("")
									&& s.contains(routeModule)
									&& !s.contains(routeModule + "Module")
											? s.replace(routeModule,
													routeModule + "Module")
											: s;
							s = s + "\n";
							totalStr += s;
						}
						// totalStr = totalStr.replaceAll(search, replace);
						FileWriter fw = null;
						try {
							fw = new FileWriter(f);
							fw.write(totalStr);
						} catch (final Exception ex) {
							ObjectUtils.logException(logger, ex, null);
						} finally {
							if (fw != null) {
								fw.close();
							}
						}
					}
				} catch (final Exception e) {
					ObjectUtils.logException(logger, e, null);
				}

			}
		}
	}

	/**
	 * Listf.
	 *
	 * @param directoryName
	 *            the directory name
	 * @param files
	 *            the files
	 */
	public static void listf(final String directoryName,
			final ArrayList<File> files) {
		final File directory = new File(directoryName);

		// get all the files from a directory
		final File[] fList = directory.listFiles();
		for (final File file : fList) {
			if (file.isFile()) {
				if (file.getName().endsWith("routes.ts")
						|| file.getName().endsWith("route.ts")
						|| file.getName().endsWith("module.ts")
						|| file.getName().endsWith("modules.ts")) {
					files.add(file);
				}
			} else if (file.isDirectory()) {
				listf(file.getAbsolutePath(), files);
			}
		}
	}
}