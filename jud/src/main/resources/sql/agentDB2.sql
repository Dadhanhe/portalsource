SELECT 
    @row:=@row + 1 AS id,
    salesAgentId AS userId,
    salesAgent AS userName,
    productId,
    productName AS reportDisplayName,
    incrementalRevenue AS mtdValue,
    ytdIncrementalRevenue AS ytdValue,
    mtdCount AS mtdCount,
    arpd AS mtdArpd,
    ytdArpd AS ytdArpd,
    roomConversion AS mtdRoomConversion,
    ytdRoomconversion AS ytdRoomConversion,
    conversion AS mtdConv,
    ytdConversion AS ytdConv 
FROM
    (SELECT @row:=0) AS row,
    (SELECT 
        main.salesAgentId,
            main.salesAgent,
            main.productId,
            main.productName,
            main.incrementalRevenue,
            main.ytdIncrementalRevenue,
            main.mtdCount,
            main.nights,
            main.arpd,
            main.ytdArpd,
            main.roomConversion,
            main.ytdRoomconversion,
            main.roomNights,
            main.nightsCheckedIn,
            main.conversion,
            main.ytdConversion
    FROM
        (SELECT 
        main1.salesAgentId,
            main1.salesAgent,
            main1.productId,
            main1.productName,
            IF(main1.incrementalRevenue IS NULL, 0, main1.incrementalRevenue) AS incrementalRevenue,
            IF(main3.incrementalRevenue IS NULL, 0, main3.incrementalRevenue) AS ytdIncrementalRevenue,
            IF(main1.mtdCount IS NULL, 0, main1.mtdCount) AS mtdCount,
            main2.nightsCheckedIn AS nights,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main2.nightsCheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.incrementalRevenue / main2.nightsCheckedIn), 0) AS arpd,
            IF(main3.incrementalRevenue IS NOT NULL
                AND main4.nightsCheckedIn IS NOT NULL
                AND main4.nightsCheckedIn != 0, (main3.incrementalRevenue / main4.nightsCheckedIn), 0) AS ytdArpd,
            IF(main1.roomConversion IS NULL, 0, main1.roomConversion) AS roomConversion,
            IF(main3.roomConversion IS NULL, 0, main3.roomConversion) AS ytdRoomConversion,
            main1.roomNights,
            main2.nightsCheckedIn,
            IF(main1.roomNights IS NOT NULL
                AND main2.nightsCheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.roomNights / main2.nightsCheckedIn) * 100, 0) AS conversion,
            IF(main3.roomNights IS NOT NULL
                AND main4.nightsCheckedIn IS NOT NULL
                AND main4.nightsCheckedIn != 0, (main3.roomNights / main4.nightsCheckedIn) * 100, 0) AS ytdConversion
    FROM
        (SELECT 
        pm.USER_ID AS salesAgentId,
        pm.PRODUCT_ID AS productId,
        SUM(@reportRevenueData) AS incrementalRevenue,
		@roomNightsQuery as roomNights,
        SUM(IF(pm.CallOrders > 0, pm.CallOrders, 0)) AS roomConversion
    FROM
        @productMetricTableName pm
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
           	AND month(@reportDate) <= @monthVar
			AND year(@reportDate) = @yearVar	
    GROUP BY pm.USER_ID , pm.PRODUCT_ID , pm.TENANT_LOCATION_ID  @locationGroupJoin) AS main3
    LEFT OUTER JOIN (SELECT 
        im.USER_ID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn
    FROM
        @agentMetricTableName im
    JOIN user u ON (im.USER_ID = u.ID)
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
           	AND month(im.metricDate) <= @monthVar
			AND year(im.metricDate) = @yearVar
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID) AS main4 ON main3.salesAgentId = main4.salesAgentId
	LEFT OUTER JOIN (SELECT 
        pm.USER_ID AS salesAgentId,
            pm.PRODUCT_ID as productId,
            p.name AS productName,
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS salesAgent,
            SUM(@reportRevenueData) AS incrementalRevenue,
            COUNT(pm.upsellCharge) AS mtdCount,
            @roomNightsQuery as roomNights,
            SUM(IF(pm.CallOrders > 0, pm.CallOrders, 0)) AS roomConversion
    FROM
        @productMetricTableName pm
    JOIN user u ON (pm.USER_ID = u.ID)
    JOIN user_profile up ON (u.ID = up.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @monthVar
			AND year(@reportDate) = @yearVar	
    GROUP BY pm.USER_ID , pm.PRODUCT_ID , pm.TENANT_LOCATION_ID @locationGroupJoin) AS main1 ON main1.salesAgentId = main3.salesAgentId
        AND main1.productId = main3.productId
     LEFT OUTER JOIN (SELECT 
        im.USER_ID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn
    FROM
        @agentMetricTableName im
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
            AND month(im.metricDate) = @monthVar
			AND year(im.metricDate) = @yearVar
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID) AS main2 ON main3.salesAgentId = main2.salesAgentId) AS main) mainFinal 
WHERE 
    mainFinal.salesAgentId = @userId 
ORDER BY mainFinal.productName ASC;	