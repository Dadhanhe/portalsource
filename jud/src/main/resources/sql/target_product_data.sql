SELECT 
    @row:=@row + 1 AS id,
    p2.id AS productId,
    p2.pname AS productName,
    p2.isRecurring AS isRecurring,
    p2.isMajor AS isProdMajor,
    p2.ACTIVE_STATUS AS prodStatus,
    p2.dname AS departmentName,
    p2.dId AS departmentId,
    p2.productCat AS prodCateName,
    p2.ACTIVE_STATUS AS departmentStatus,
    revenueTotalData.priorCurrentMonthProjectedRevenue1,
    revenueTotalData.priorCurrentMonthActualRevenue1,
    revenueTotalData.priorMonthRevenue1,
    revenueTotalData.priorMonthRevenue2,
    revenueTotalData.priorMonthRevenue3,
    revenueTotalData.priorLastYearSameMonthRevenue1,
    (revenueTotalData.priorMonthRevenue1 + revenueTotalData.priorMonthRevenue2 + revenueTotalData.priorMonthRevenue3 + revenueTotalData.priorLastYearSameMonthRevenue1) AS totalRevenue
	FROM
    (SELECT @row:=0) AS row,
    (SELECT 
        tp.name AS pname, lg.name AS dname, lg.id AS dId, tgp.*,tgp.productCategory AS productCat
    FROM
        location_group_product tgp
    JOIN tenant_product tp ON tp.id = tgp.TENANT_PRODUCT_ID AND tgp.isBasicRateProduct <> true
    JOIN location_group lg ON lg.id = tgp.LOCATION_GROUP_ID AND tgp.isBasicRateProduct <> true
        AND lg.TENANT_LOCATION_ID = @organisation_id
        AND PARENT_GROUP_ID IS NOT NULL
    WHERE
        tgp.ACTIVE_STATUS = 0
        ) AS p2
        LEFT OUTER JOIN
    (SELECT 
        revData.productID AS productID,
            ROUND(SUM(IF(revData.date BETWEEN DATE_SUB(@date, INTERVAL 1 MONTH) AND DATE_SUB(@date, INTERVAL 1 DAY), revData.revenue, 0)), 2) AS priorMonthRevenue1,
            ROUND(SUM(IF(revData.date BETWEEN DATE_SUB(@date, INTERVAL 2 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 2 MONTH)), revData.revenue, 0)), 2) AS priorMonthRevenue2,
            ROUND(SUM(IF(revData.date BETWEEN DATE_SUB(@date, INTERVAL 3 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 3 MONTH)), revData.revenue, 0)), 2) AS priorMonthRevenue3,
            ROUND(SUM(IF(revData.date BETWEEN DATE_SUB(@date, INTERVAL 12 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 12 MONTH)), revData.revenue, 0)), 2) AS priorLastYearSameMonthRevenue1,
            ROUND(SUM(IF(revData.date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND DATE_FORMAT(NOW(), '%Y-%m-%d'), revData.revenue, 0)), 2) AS priorCurrentMonthActualRevenue1,
            ROUND(SUM(IF(revData.date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND DATE_FORMAT(NOW(), '%Y-%m-%d'), ((revData.revenue / (DAY(NOW()) - 1)) * DAY(LAST_DAY(NOW()))), 0)), 2) AS priorCurrentMonthProjectedRevenue1
    FROM
        (SELECT 
        pm.product_ID AS productID,
            (CASE
                WHEN o.hotelMetricsDataType = '0' THEN pm.arrivalDate
                WHEN o.hotelMetricsDataType = '1' THEN pm.businessDate
                ELSE pm.departureDate
            END) AS date,
            SUM(CASE
                WHEN o.hotelMetricsDataType = '1' THEN IF(pm.upsellCharge IS NULL, 0, pm.upsellCharge)
                ELSE CASE
                    WHEN p.isRecurring = 0 THEN IF(pm.upsellCharge IS NULL, 0, pm.upsellCharge)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, (pm.roomNights * pm.upsellCharge))
                END
            END) AS revenue
    FROM
        product_metric pm
    JOIN location_group_product p ON (p.id = pm.product_ID AND p.isBasicRateProduct <> true)
    JOIN tenant_location o ON o.id = pm.TENANT_LOCATION_ID
    WHERE
        pm.TENANT_LOCATION_ID = @organisation_id
            AND ((CASE
            WHEN o.hotelMetricsDataType = '0' THEN pm.arrivalDate
            WHEN o.hotelMetricsDataType = '1' THEN pm.businessDate
            ELSE pm.departureDate
        END) BETWEEN DATE_SUB(@date, INTERVAL 3 MONTH) AND DATE_SUB(@date, INTERVAL 1 DAY)
            OR (CASE
            WHEN o.hotelMetricsDataType = '0' THEN pm.arrivalDate
            WHEN o.hotelMetricsDataType = '1' THEN pm.businessDate
            ELSE pm.departureDate
        END) BETWEEN DATE_SUB(@date, INTERVAL 12 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 12 MONTH))
            OR (CASE
            WHEN o.hotelMetricsDataType = '0' THEN pm.arrivalDate
            WHEN o.hotelMetricsDataType = '1' THEN pm.businessDate
            ELSE pm.departureDate
        END) BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND DATE_FORMAT(NOW(), '%Y-%m-%d'))
            AND pm.auditstatus NOT IN (2 , 4, 5)
        
    GROUP BY pm.product_ID , date) AS revData
    GROUP BY revData.productID) AS revenueTotalData ON (p2.id = revenueTotalData.productID)
ORDER BY totalRevenue DESC;