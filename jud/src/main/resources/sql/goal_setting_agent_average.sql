SELECT 
    @row:=@row + 1 AS id,
    totalData.employeeName AS userName,
    totalData.userID AS userId,
    SUM(totalData.salesRecords) AS mtdRoomUpsells,
    SUM(totalData.totalRevenue) AS incrementalRevenue,
    ROUND(IF(majorProd3.uniqueArrivals != 0,
                ((SUM(totalData.majorUpsells) / majorProd3.uniqueArrivals)),
                0),
            2) AS mtdMajorUpsells,
    IF(SUM(totalData.majorIncrementalRevenue) IS NULL,
        0,
        SUM(totalData.majorIncrementalRevenue)) AS majorIncrementalRevenue,
    IF(SUM(totalData.EstimatedArrivals) IS NULL,
        0,
        SUM(totalData.EstimatedArrivals)) AS mtdArrivals,
    IF(SUM(totalData.EstimatedNights) IS NULL,
        0,
        SUM(totalData.EstimatedNights)) AS mtdOccupiedRoomsChargeDays,
    ROUND(IF(majorProd3.uniqueArrivals != 0,
                (((majorDateProd.majorIncrementalRevenue) / majorProd3.uniqueArrivals)),
                0),
            2) AS averageUpsellRevenuePDPA,
    ROUND(IF(SUM(totalData.salesRecords) != 0,
                ((SUM(totalData.totalSaleDays) / SUM(totalData.salesRecords))),
                0),
            2) AS guestAverageStay,
    ROUND(IF(SUM(totalData.EstimatedArrivals) != 0,
                ((SUM(totalData.majorUpsells) / SUM(totalData.EstimatedArrivals)) * 100),
                0),
            2) AS roomUpsellConversion,
    ROUND(IF(SUM(totalData.EstimatedNights) != 0,
                ((SUM(totalData.majorIncrementalRevenue) / SUM(totalData.EstimatedNights)) * 100),
                0),
            2) AS 'IDR',
    IF(SUM(totalData.majorUpsells) IS NULL,
        0,
        SUM(totalData.majorUpsells)) AS majorUpsells,
    IF(SUM(totalData.EstimatedNights) IS NULL,
        0,
        SUM(totalData.EstimatedNights)) AS EstimatedNights,
    IF(SUM(totalData.EstimatedArrivals) IS NULL,
        0,
        SUM(totalData.EstimatedArrivals)) AS EstimatedArrivals,
    IF(SUM(totalData.salesRecords) IS NULL,
        0,
        SUM(totalData.salesRecords)) AS salesRecords,
    IF(SUM(totalData.totalSaleDays) IS NULL,
        0,
        SUM(totalData.totalSaleDays)) AS totalSaleDays,
    IF(majorProd3.uniqueArrivals IS NULL,
        0,
        majorProd3.uniqueArrivals) AS uniqueArrivals,
    ROUND(IF(majorProd3.uniqueArrivals != 0,
                (((majorDateProd.majorUpsells) / majorProd3.uniqueArrivals)),
                0),
            2) AS averageUpsellPDPA,
    IF(majorDateProd.majorUpsells IS NULL,
        0,
        majorDateProd.majorUpsells) AS majorDateBasedUpsells,
    IF(majorDateProd.majorIncrementalRevenue IS NULL,
        0,
        majorDateProd.majorIncrementalRevenue) AS majorDateBasedUpsellRevenue
  FROM
    (SELECT @row:=0) AS rowCnt,
    (SELECT 
        locationMetricData.metricDate,
            locationMetricData.roomsOccupied,
            locationMetricData.arrivals,
            productMetricData.userID,
            productMetricData.employeeName,
            productMetricData.organizationID,
            productMetricData.date,
            productMetricData.salesRecords,
            productMetricData.totalSaleDays,
            productMetricData.nights,
            productMetricData.totalRevenue,
            productMetricData.majorIncrementalRevenue,
            productMetricData.majorUpsells,
            productMetricData.percentageTotalSales,
            productMetricData.percentageTotalNights,
            productMetricData.totalSalesRecords,
            productMetricData.totalNightsRecords,
            ROUND(((productMetricData.percentageTotalSales * locationMetricData.arrivals) / 100), 0) AS EstimatedArrivals,
            ROUND(((productMetricData.percentageTotalNights * locationMetricData.roomsOccupied) / 100), 0) AS EstimatedNights 
    FROM
        (SELECT 
        TENANT_LOCATION_ID as organizationiD, metricDate, roomsOccupied, arrivals
    FROM
        location_metric
    WHERE
        TENANT_LOCATION_ID = @tenantLocationId
            @metricDateStr) AS locationMetricData
    JOIN (SELECT 
        finalVal.userID,
            finalVal.employeeName,
            finalVal.organizationID,
            finalVal.date,
            finalVal.totalSaleDays,
            finalVal.salesRecords,
            finalVal.nights,
            finalVal.totalRevenue,
            finalVal.majorIncrementalRevenue,
            finalVal.majorUpsells,
            ROUND(IF(SUM(majorProd2.sales) != 0, ((finalVal.salesRecords / SUM(majorProd2.sales)) * 100), 0), 2) AS percentageTotalSales,
            ROUND(IF(SUM(majorProd2.nights) != 0, ((finalVal.nights / SUM(majorProd2.nights)) * 100), 0), 2) AS percentageTotalNights,
            SUM(majorProd2.sales) AS totalSalesRecords,
            SUM(majorProd2.nights) AS totalNightsRecords
     FROM
        (SELECT 
        pm.USER_ID AS userID,
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS employeeName,
            pm.TENANT_LOCATION_ID AS organizationID,
            pm.arrivalDate AS date,
            SUM(DATEDIFF(pm.departureDate, pm.arrivalDate)) AS totalSaleDays,
            COUNT(DISTINCT (CONCAT(pm.PRODUCT_ID, pm.confirmationNumber))) AS salesRecords,
            SUM(roomNights) AS nights,
            SUM((CASE
                WHEN o.hotelMetricsDataType = '1' THEN IF(pm.upsellCharge IS NULL, 0, pm.upsellCharge)
                ELSE CASE
                    WHEN p.isRecurring = 0 THEN IF(pm.upsellCharge IS NULL, 0, pm.upsellCharge)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, (pm.roomNights * pm.upsellCharge))
                END
            END)) AS totalRevenue,
            majorProd.majorIncrementalRevenue AS majorIncrementalRevenue,
            majorProd.majorUpsells AS majorUpsells
      FROM
        product_metric pm
    LEFT OUTER JOIN (SELECT 
            pm2.USER_ID as userID,
            pm2.TENANT_LOCATION_ID as organizationID,
            pm2.arrivalDate,
            SUM((CASE
                WHEN o2.hotelMetricsDataType = '1' THEN IF(pm2.upsellCharge IS NULL, 0, pm2.upsellCharge)
                ELSE CASE
                    WHEN p2.isRecurring = 0 THEN IF(pm2.upsellCharge IS NULL, 0, pm2.upsellCharge)
                    WHEN
                        p2.isRecurring = 1
                    THEN
                        IF(pm2.roomNights IS NULL
                            OR pm2.roomNights = 0
                            OR pm2.upsellCharge IS NULL
                            OR pm2.upsellCharge = 0, 0, (pm2.roomNights * pm2.upsellCharge))
                END
            END)) AS majorIncrementalRevenue,
            COUNT(DISTINCT (CONCAT(pm2.PRODUCT_ID, pm2.confirmationNumber))) AS majorUpsells
     FROM
        product_metric pm2
    JOIN tenant_location o2 ON (pm2.TENANT_LOCATION_ID = o2.ID)
    JOIN location_group_product p2 ON (pm2.PRODUCT_ID = p2.id AND p2.isMajor = 1)
    WHERE
        pm2.auditstatus NOT IN (2 , 4, 5)
            AND pm2.TENANT_LOCATION_ID = @tenantLocationId
            @pm2_departmentStr
    GROUP BY pm2.USER_ID , pm2.TENANT_LOCATION_ID , pm2.arrivalDate) AS majorProd ON (pm.USER_ID = majorProd.userID
        AND pm.TENANT_LOCATION_ID = majorProd.organizationID
        AND pm.arrivalDate = majorProd.arrivalDate)
    JOIN tenant_location o ON (pm.TENANT_LOCATION_ID = o.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.id)
    JOIN user u ON (u.id = pm.USER_ID)
    JOIN user_profile up ON (u.id = up.id) 
    WHERE
        pm.auditstatus NOT IN (2 , 4, 5)
            AND pm.TENANT_LOCATION_ID = @tenantLocationId
            @pm_departmentStr
            @pm_arrivalDateStr 
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID , pm.arrivalDate, majorProd.majorIncrementalRevenue, majorProd.majorUpsells ) AS finalVal
    LEFT OUTER JOIN (SELECT 
        totalRecords.organizationID,
            totalRecords.arrivalDate,
            SUM(totalRecords.nights) AS nights,
            SUM(totalRecords.sales) AS sales
     FROM
        (SELECT 
            pm3.USER_ID as userID,
            pm3.TENANT_LOCATION_ID as organizationID,
            pm3.arrivalDate,
            SUM(roomNights) AS nights,
            COUNT(DISTINCT (CONCAT(pm3.PRODUCT_ID, pm3.confirmationNumber))) AS sales
     FROM
        product_metric pm3
    JOIN tenant_location o3 ON (pm3.TENANT_LOCATION_ID = o3.ID)
    WHERE
        pm3.auditstatus NOT IN (2 , 4, 5)
            AND pm3.TENANT_LOCATION_ID = @tenantLocationId
            @pm3_departmentStr
            @pm3_arrivalDateStr 
    GROUP BY pm3.USER_ID , pm3.TENANT_LOCATION_ID , pm3.arrivalDate) AS totalRecords
    GROUP BY totalRecords.organizationID, totalRecords.arrivalDate) AS majorProd2 ON (finalVal.organizationID = majorProd2.organizationID
        AND finalVal.date = majorProd2.arrivalDate)
    GROUP BY finalVal.userID, finalVal.employeeName, finalVal.organizationID, finalVal.date, finalVal.totalSaleDays, finalVal.salesRecords, finalVal.nights, finalVal.totalRevenue, finalVal.majorIncrementalRevenue, finalVal.majorUpsells) AS productMetricData ON (productMetricData.date = locationMetricData.metricDate)
	GROUP BY locationMetricData.metricDate, locationMetricData.roomsOccupied, locationMetricData.arrivals, productMetricData.userID, productMetricData.employeeName, productMetricData.organizationID, productMetricData.date, productMetricData.salesRecords, productMetricData.totalSaleDays, productMetricData.nights, productMetricData.totalRevenue, productMetricData.majorIncrementalRevenue, productMetricData.majorUpsells, productMetricData.percentageTotalSales, productMetricData.percentageTotalNights, productMetricData.totalSalesRecords, productMetricData.totalNightsRecords    
    ORDER BY locationMetricData.metricDate ASC) AS totalData
        LEFT OUTER JOIN
    (SELECT 
        totalRecords2.userID,
            totalRecords2.organizationID,
            totalRecords2.uniqueArrivals AS uniqueArrivals
     FROM
        (SELECT 
            pm4.USER_ID as userID,
            pm4.TENANT_LOCATION_ID as organizationID,
            COUNT(DISTINCT pm4.arrivalDate) AS uniqueArrivals
     FROM
        product_metric pm4
    WHERE
        pm4.auditstatus NOT IN (2 , 4, 5)
            AND pm4.TENANT_LOCATION_ID = @tenantLocationId
            @pm4_departmentStr 
            @pm4_arrivalDateStr 
    GROUP BY pm4.USER_ID, pm4.TENANT_LOCATION_ID) AS totalRecords2
    GROUP BY totalRecords2.userID, totalRecords2.organizationID, totalRecords2.uniqueArrivals) AS majorProd3 ON (totalData.organizationID = majorProd3.organizationID
        AND totalData.userID = majorProd3.userID)
        LEFT OUTER JOIN
    (SELECT 
        totalRecords2.userID,
            totalRecords2.organizationID,
            totalRecords2.majorIncrementalRevenue AS majorIncrementalRevenue,
            totalRecords2.majorUpsells AS majorUpsells
     FROM
        (SELECT 
            pm5.USER_ID as userID,
            pm5.TENANT_LOCATION_ID as organizationID,
            SUM((CASE
                WHEN o5.hotelMetricsDataType = '1' THEN IF(pm5.upsellCharge IS NULL, 0, pm5.upsellCharge)
                ELSE CASE
                    WHEN p5.isRecurring = 0 THEN IF(pm5.upsellCharge IS NULL, 0, pm5.upsellCharge)
                    WHEN
                        p5.isRecurring = 1
                    THEN
                        IF(pm5.roomNights IS NULL
                            OR pm5.roomNights = 0
                            OR pm5.upsellCharge IS NULL
                            OR pm5.upsellCharge = 0, 0, (pm5.roomNights * pm5.upsellCharge))
                END
            END)) AS majorIncrementalRevenue,
            COUNT(DISTINCT (CONCAT(pm5.PRODUCT_ID, pm5.confirmationNumber))) AS majorUpsells
     FROM
        product_metric pm5
    JOIN tenant_location o5 ON (pm5.TENANT_LOCATION_ID = o5.ID)
    JOIN location_group_product p5 ON (pm5.PRODUCT_ID = p5.id AND p5.isMajor = 1)
    WHERE
        pm5.auditstatus NOT IN (2 , 4, 5)
            AND pm5.TENANT_LOCATION_ID = @tenantLocationId
            @pm5_departmentStr
            @pm5_productMetricDateStr
    GROUP BY pm5.USER_ID , pm5.TENANT_LOCATION_ID) AS totalRecords2
    GROUP BY totalRecords2.userID , totalRecords2.organizationID) AS majorDateProd ON (totalData.userID = majorDateProd.userID
        AND totalData.organizationID = majorDateProd.organizationID)        
 GROUP BY totalData.userID, majorDateProd.majorUpsells, majorDateProd.majorIncrementalRevenue, majorProd3.uniqueArrivals
 ORDER BY id ASC