SELECT  
    * 
FROM
    (SELECT 
        @row:=@row + 1 AS id,
            mainFinal.tenantLocationId,
            mainFinal.locationGroupId,
            mainFinal.date AS date,
            rh.name AS reportName,
            CASE
                WHEN rh.name = 'totalUpsellAmount' THEN totalUpsellAmount
                WHEN rh.name = 'majorUpsellCharge' THEN majorUpsellCharge
                WHEN rh.name = 'noOfUpsellRoomNights' THEN noOfUpsellRoomNights
                WHEN rh.name = 'upsellRoomAverageRate' THEN upsellRoomAverageRate
                WHEN rh.name = 'idr' THEN idr
                WHEN rh.name = 'uoConversion' THEN uoConversion
                WHEN rh.name = 'conversion' THEN upsellConversion
                WHEN rh.name = 'checkedInNights' THEN checkedInNights
                WHEN rh.name = 'averageRevenuePerOrder' THEN averageRevenuePerOrder
                WHEN rh.name = 'totalIncentiveAmount' THEN totalIncentiveAmount
            END AS value,
            mainFinal.unit AS unit
    FROM
        (SELECT @row:=0) AS row, report_helper rh
    LEFT OUTER JOIN (SELECT 
        main.tenantLocationId,
            main.locationGroupId,
            main.date,
            main.unit,
            main.incrementalRevenue AS totalUpsellAmount,
            main.majorUpsellCharge,
            main.noOfUpsellRoomNights,
            main.upsellRoomAverageRate,
            main.idr,
            main.upsellConversion,
            main.totalIncentiveAmount,
            main.checkedInNights,
            main.averageRevenuePerOrder,
            main.uoConversion
    FROM
        (SELECT 
            main1.tenantLocationId,
            main1.locationGroupId,
            main1.date,
            main1.unit,
            IF(main1.incrementalRevenue IS NULL, 0, main1.incrementalRevenue) AS incrementalRevenue,
            main1.majorUpsellCharge,
            main1.noOfUpsellRoomNights,
            IF(main1.majorUpsellCharge IS NOT NULL
                AND main1.noOfUpsellRoomNights IS NOT NULL
                AND main1.noOfUpsellRoomNights != 0, (main1.majorUpsellCharge / main1.noOfUpsellRoomNights), 0) AS upsellRoomAverageRate,
            IF(main2.totalRevenue IS NOT NULL
                AND main2.roomsOccupied IS NOT NULL
                AND main2.roomsOccupied != 0, (main2.totalRevenue / main2.roomsOccupied), 0) AS idr,
            IF(main1.upsells IS NOT NULL
                AND main2.arrivals IS NOT NULL
                AND main2.arrivals != 0, (main1.upsells / main2.arrivals) * 100, 0) AS upsellConversion,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main1.orders IS NOT NULL
                AND main1.orders != 0, main1.incrementalRevenue / main1.orders, 0) AS averageRevenuePerOrder,
            IF(main1.orders IS NOT NULL
                AND main2.roomsOccupied IS NOT NULL
                AND main2.roomsOccupied != 0, (main1.orders / main2.roomsOccupied) * 100, 0) AS uoConversion,
            IF(main2.roomsOccupied IS NULL, 0, main2.roomsOccupied) AS checkedInNights,
            main1.userCount,
            IF(main1.userCount > 0
                AND main1.incrementalRevenue IS NOT NULL, main1.incrementalRevenue / main1.userCount, 0) AS average,
            main1.totalIncentiveAmount
    FROM
        (SELECT 
            pm.TENANT_LOCATION_ID as tenantLocationId,
            pm.LOCATION_GROUP_ID as locationGroupId,
           	(@reportDate) as date,
            r.currency AS unit,
            SUM(@reportRevenueData) AS incrementalRevenue,
            SUM(CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    IF(pm.upsellCharge IS NOT NULL
                        AND p.isMajor = 1, pm.upsellCharge, 0)
                ELSE (CASE
                    WHEN
                        p.isRecurring = 0
                    THEN
                        IF(pm.upsellCharge IS NOT NULL
                            AND p.isMajor = 1, pm.upsellCharge, 0)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
                END)
            END) AS majorUpsellCharge,
            CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    COUNT(DISTINCT (CASE
                        WHEN p.isMajor = '1' THEN pm.ID
                        ELSE NULL
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS noOfUpsellRoomNights,
            COUNT(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            @roomNightsQuery as roomNights,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,
            IF(COUNT(DISTINCT (pm.USER_ID)) IS NOT NULL, COUNT(DISTINCT (pm.USER_ID)), 0) AS userCount,
            SUM(IF(pm.commissionableRevenue IS NULL, 0, pm.commissionableRevenue)) AS totalIncentiveAmount
    FROM
        product_metric pm
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN region r ON (tl.REGION_ID = r.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND year(@reportDate) = @yearVar 
            @monthQry1
            AND (@reportDate) <= CURDATE()
    GROUP BY date , pm.TENANT_LOCATION_ID  @locationGroupJoin) AS main1
    LEFT OUTER JOIN (SELECT 
        m.TENANT_LOCATION_ID as tenantLocationId,
            m.metricDate AS date,
            SUM(IF(m.arrivals IS NULL, 0, m.arrivals)) AS arrivals,
            SUM(IF(m.roomsOccupied IS NULL, 0, m.roomsOccupied)) AS roomsOccupied,
            CASE
                WHEN tl.hotelMetricsDataType = '0' THEN SUM(IF(m.upsellMainRevenue IS NULL, 0, m.upsellMainRevenue) + IF(m.otherRevenue IS NULL, 0, m.otherRevenue))
                WHEN tl.hotelMetricsDataType = '1' THEN SUM(IF(m.dailyRoomUpsellRevenue IS NULL, 0, m.dailyRoomUpsellRevenue) + IF(m.dailyOtherRevenue IS NULL, 0, m.dailyOtherRevenue))
                WHEN tl.hotelMetricsDataType = '2' THEN SUM(IF(m.departureRoomUpsellRevenue IS NULL, 0, m.departureRoomUpsellRevenue) + IF(m.departureOtherRevenue IS NULL, 0, m.departureOtherRevenue))
                ELSE 0
            END AS totalRevenue
    FROM
        location_metric m
    JOIN tenant_location tl ON (m.TENANT_LOCATION_ID = tl.ID)
    WHERE
        m.TENANT_LOCATION_ID = @orgId
            AND YEAR(m.metricDate) = @yearVar 
            @monthQry2 
    GROUP BY m.metricDate , m.TENANT_LOCATION_ID) AS main2 ON main1.tenantLocationId = main2.tenantLocationId AND main1.date = main2.date) AS main) mainFinal ON (rh.ACTIVE_STATUS = 0)
    WHERE
        rh.ACTIVE_STATUS = 0 AND rh.INDUSTRY_ID = @industryID AND rh.type = 'Chart' @reportNames ) AS chartData 
WHERE
    chartData.value IS NOT NULL;	