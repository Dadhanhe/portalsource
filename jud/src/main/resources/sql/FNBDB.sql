SELECT 
    agentInfo.id as id,
	IF(agentInfo.userId is null,-1,agentInfo.userId) as userId,
	IF(agentInfo.userName is null,'-',agentInfo.userName) as userName,
	@monthVar as month,
	@yearVar as year,
	agentInfo.unit as unit,
	agentInfo.isMoney as isMoney,
	agentInfo.isDisplayInTable as isDisplayInTable,
	IF(agentInfo.reportName is null,'-',agentInfo.reportName) as reportName,
	IF(agentInfo.reportDisplayName is null,'-',agentInfo.reportDisplayName) as reportDisplayName,
	IF(agentInfo.mtdValue is null,0,agentInfo.mtdValue) as mtdValue,
	IF(agentInfo.ytdValue is null,0,agentInfo.ytdValue) as ytdValue,
	IF(agentInfo.varienceToPreMonth is null,0,agentInfo.varienceToPreMonth) as varienceToPreMonth,
	IF(agentInfo.varienceToPreMonthPer is null,0,agentInfo.varienceToPreMonthPer) as varienceToPreMonthPer,
	IF(agentInfo.projectedEOM is null,0,agentInfo.projectedEOM) as projectedEOM,
	agentInfo.isFNBReport as isFNBReport 
FROM
    (SELECT 
			rh.id AS id,
            mainFinal.salesAgentId AS userId,
            mainFinal.salesAgent AS userName,
            rh.NAME AS reportName,
            rh.DISPLAY_NAME AS reportDisplayName,
            rh.UNIT AS unit,
            rh.IS_MONEY AS isMoney,
            rh.IS_DISPLAY_IN_TABLE AS isDisplayInTable,
            rh.isFNBReport,
            CASE
                WHEN rh.name = 'covers' THEN covers
                WHEN rh.name = 'checkCount' THEN checkCount
                WHEN rh.name = 'duration' THEN duration
            END AS mtdValue,
            CASE
                WHEN rh.name = 'covers' THEN ytdCovers
                WHEN rh.name = 'checkCount' THEN ytdCheckCount
                WHEN rh.name = 'duration' THEN ytdDuration
            END AS ytdValue,
            CASE
                WHEN rh.name = 'covers' THEN varCovers
                WHEN rh.name = 'checkCount' THEN varCheckCount
                WHEN rh.name = 'duration' THEN varDuration
            END AS varienceToPreMonth,
            CASE
                WHEN rh.name = 'covers' THEN varCoversPer
                WHEN rh.name = 'checkCount' THEN varCheckCountPer
                WHEN rh.name = 'duration' THEN varDurationPer
            END AS varienceToPreMonthPer,
            CASE
                WHEN rh.name = 'covers' THEN coversEOM
                WHEN rh.name = 'checkCount' THEN checkCountEOM
                WHEN rh.name = 'duration' THEN durationEOM
            END AS projectedEOM 
    FROM
        report_helper rh
    LEFT OUTER JOIN (SELECT 
			main.salesAgentId,
            main.salesAgent,
            main.covers,
            main.ytdCovers,
            main.varCovers,
            main.varCoversPer,
            main.coversEOM,
            main.checkCount,
            main.ytdCheckCount,
            main.varCheckCount,
            main.varCheckCountPer,
            main.checkCountEOM,
             main.duration,
            main.ytdDuration,
            main.varDuration,
            main.varDurationPer,
            main.durationEOM 
    FROM
        (SELECT 
			main1.salesAgentId,
            main1.salesAgent,
            IF(main1.covers IS NOT NULL, main1.covers, 0) AS covers,
            IF(main3.covers IS NOT NULL, main3.covers, 0) AS ytdCovers,
            IF((main1.covers - main5.covers) IS NOT NULL
                AND main5.covers > 0, ((main1.covers - main5.covers) / main5.covers) * 100, 0) AS varCovers,
            IF((main1.covers - main5.covers) IS NOT NULL
                AND main5.covers > 0, ((main1.covers - main5.covers) / main5.covers) * 100, 0) AS varCoversPer,
            CASE
                WHEN (YEAR(NOW()) >= 2017 && MONTH(NOW()) != 2) THEN (main1.covers)
                WHEN
                    (YEAR(NOW()) > 2017
                        || (YEAR(NOW()) = 2017 && MONTH(NOW()) >= 2))
                THEN
                    (((main1.covers) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS coversEOM,
            IF(main1.checkCount IS NOT NULL, main1.checkCount, 0) AS checkCount,
            IF(main3.checkCount IS NOT NULL, main3.checkCount, 0) AS ytdCheckCount,
            IF((main1.checkCount - main5.checkCount) IS NOT NULL
                AND main5.checkCount > 0, ((main1.checkCount - main5.checkCount) / main5.checkCount) * 100, 0) AS varCheckCount,
            IF((main1.checkCount - main5.checkCount) IS NOT NULL
                AND main5.checkCount > 0, ((main1.checkCount - main5.checkCount) / main5.checkCount) * 100, 0) AS varCheckCountPer,
            CASE
                WHEN (YEAR(NOW()) >= 2017 && MONTH(NOW()) != 2) THEN (main1.checkCount)
                WHEN
                    (YEAR(NOW()) > 2017
                        || (YEAR(NOW()) = 2017 && MONTH(NOW()) >= 2))
                THEN
                    (((main1.checkCount) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS checkCountEOM, 
            IF(main1.duration IS NOT NULL, main1.duration, 0) AS duration,
            IF(main3.duration IS NOT NULL, main3.duration, 0) AS ytdDuration,
            IF((main1.duration - main5.duration) IS NOT NULL
                AND main5.duration > 0, ((main1.duration - main5.duration) / main5.duration) * 100, 0) AS varDuration,
            IF((main1.duration - main5.duration) IS NOT NULL
                AND main5.duration > 0, ((main1.duration - main5.duration) / main5.duration) * 100, 0) AS varDurationPer,
            CASE
                WHEN (YEAR(NOW()) >= 2017 && MONTH(NOW()) != 2) THEN (main1.duration)
                WHEN
                    (YEAR(NOW()) > 2017
                        || (YEAR(NOW()) = 2017 && MONTH(NOW()) >= 2))
                THEN
                    (((main1.duration) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS durationEOM 
    FROM
        (SELECT 
			salesAgentId,
            tenantLocationID,
            @locationGroupOuterStr
            SUM(covers) AS covers,
            sum(checkCount) as checkCount,
            sum(duration) as duration 
    FROM
        (SELECT 
			pm.USER_ID AS salesAgentId,
            pm.TENANT_LOCATION_ID AS tenantLocationID,
            @locationGroupInnerStr
            MAX((CASE
                WHEN pm.roomNights IS NULL THEN 0
                ELSE pm.roomNights
            END)) AS covers,
            MAX((CASE
                WHEN pm.checkCount IS NULL THEN 0
                ELSE pm.checkCount
            END)) AS checkCount,
            MAX((CASE
                WHEN pm.duration IS NULL THEN 0
                ELSE pm.duration
            END)) AS duration 
    FROM
        product_metric pm
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
			pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) <= @monthVar
			AND year(@reportDate) = @yearVar	
			AND (@reportDate) <= CURDATE()
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID , pm.confirmationNumber @locationGroupJoin) AS a1
    GROUP BY salesAgentId , tenantLocationID @locationGroupPlain) AS main3
    LEFT OUTER JOIN (SELECT 
        	salesAgentId AS salesAgentId,
            salesAgent,
            tenantLocationID,
            @locationGroupOuterStr
            SUM(covers) AS covers,
            sum(checkCount) as checkCount,
            sum(duration) as duration 
    FROM
        (SELECT 
        	pm.USER_ID AS salesAgentId,
            pm.TENANT_LOCATION_ID AS tenantLocationID,
            @locationGroupInnerStr
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS salesAgent,
            MAX((CASE
                WHEN pm.roomNights IS NULL THEN 0
                ELSE pm.roomNights
            END)) AS covers,
            MAX((CASE
                WHEN pm.checkCount IS NULL THEN 0
                ELSE pm.checkCount
            END)) AS checkCount,
            MAX((CASE
                WHEN pm.duration IS NULL THEN 0
                ELSE pm.duration
            END)) AS duration 
    FROM
        product_metric pm
    JOIN user u ON (pm.USER_ID = u.id)
    JOIN user_profile up ON (u.ID = up.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @monthVar
			AND year(@reportDate) = @yearVar	
			AND (@reportDate) <= CURDATE()
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID , pm.confirmationNumber @locationGroupJoin) AS a2
    GROUP BY salesAgentId , tenantLocationID @locationGroupPlain) AS main1 ON (main1.salesAgentId = main3.salesAgentId)
    LEFT OUTER JOIN (SELECT 
       	 	salesAgentId AS salesAgentId,
            tenantLocationID,
            @locationGroupOuterStr
            SUM(covers) AS covers,
            sum(checkCount) as checkCount,
            sum(duration) as duration 
    FROM
        (SELECT 
        pm.USER_ID AS salesAgentId,
            pm.TENANT_LOCATION_ID AS tenantLocationID,
            @locationGroupInnerStr
            MAX((CASE
                WHEN pm.roomNights IS NULL THEN 0
                ELSE pm.roomNights
            END)) AS covers,
            MAX((CASE
                WHEN pm.checkCount IS NULL THEN 0
                ELSE pm.checkCount
            END)) AS checkCount,
            MAX((CASE
                WHEN pm.duration IS NULL THEN 0
                ELSE pm.duration
            END)) AS duration 
    FROM
        product_metric pm
    JOIN user u ON (pm.USER_ID = u.id)
    JOIN user_profile up ON (u.ID = up.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @prevMonthVar
			AND year(@reportDate) = @yearVar	
			AND (@reportDate) <= CURDATE()
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID , pm.confirmationNumber @locationGroupJoin) AS a3
    GROUP BY salesAgentId , tenantLocationID @locationGroupPlain) AS main5 ON (main3.salesAgentId = main5.salesAgentId)) AS main) mainFinal ON (rh.ACTIVE_STATUS = 0)
    WHERE
        rh.ACTIVE_STATUS = 0 AND rh.type = 'Agent' AND rh.INDUSTRY_ID = @industryID
            AND rh.isFNBReport = 1
    ORDER BY rh.DISPLAY_RANK ASC) AS agentInfo;