 SELECT  
     tl.id AS tenantLocationId, 
     tl.NAME AS tenantLocationName,
     tl.REGION_ID AS tenantLocationRegionId,
     di.metricDate AS metricDate, 
     CASE 
         WHEN LM.TENANT_LOCATION_ID IS NOT NULL THEN 1 
         ELSE 0 
     END AS isLocationMetricsAvailable, 
     CASE 
         WHEN AM.TENANT_LOCATION_ID IS NOT NULL THEN 1 
         ELSE 0 
     END AS isAgentMetricsAvailable, 
     CASE 
         WHEN AP.TENANT_LOCATION_ID IS NOT NULL THEN 1 
         ELSE 0 
     END AS isProductMetricsAvailable,
     flStatus.fileStatus,
     flStatus.file_name AS fileName,
     tl.hotelMetricsDataType,
     tl.productMetricsValidationType
 FROM 
     tenant_location tl 
         JOIN 
     date_intervals di 
     	 LEFT OUTER JOIN 
     	 (select *,
			CASE 
				WHEN iflStatus = 0 THEN "Pending"
				WHEN iflStatus = 3 THEN "Error"
				WHEN iflStatus = 1 AND FIND_IN_SET('0',pflStatus) > 0 THEN "Pending"
				WHEN iflStatus = 1 AND FIND_IN_SET('0',pflStatus) = 0 AND FIND_IN_SET('4',pflStatus) > 0 THEN "Queued"
				WHEN iflStatus = 1 AND FIND_IN_SET('0',pflStatus) = 0 AND FIND_IN_SET('4',pflStatus) = 0 AND FIND_IN_SET('3',pflStatus) = 0 AND FIND_IN_SET('1',pflStatus) > 0 THEN "Processed"
				WHEN iflStatus = 1 AND FIND_IN_SET('3',pflStatus) > 0 THEN "Error"
			END AS fileStatus
		 FROM (
			SELECT 
				ifl.ID as importedFileLogId,
				ifl.tenantLocationId,
				ifl.metric_date, 
				ifl.file_name, 
				ifl.status as iflStatus,
				group_concat(pfl.status) pflStatus
			FROM imported_file_log ifl 
			LEFT OUTER JOIN processed_file_log pfl ON ifl.ID = pfl.importedFileLogId
			WHERE ifl.metric_date BETWEEN '@startDate' AND '@endDate' group by ifl.ID) as filelStatus) as flStatus ON flStatus.tenantLocationId = tl.ID AND flStatus.metric_date = di.metricDate
         LEFT OUTER JOIN 
     (SELECT  
         TENANT_LOCATION_ID, metricDate 
     FROM 
         location_metric lmTemp
    WHERE
        lmTemp.TENANT_LOCATION_ID IN (@locationIds)
            AND lmTemp.metricDate BETWEEN '@startDate' AND '@endDate'
    GROUP BY lmTemp.TENANT_LOCATION_ID , lmTemp.metricDate) AS LM ON (di.metricDate = LM.metricDate 
         AND tl.id = LM.TENANT_LOCATION_ID) 
         LEFT OUTER JOIN 
     (SELECT  
         TENANT_LOCATION_ID, metricDate 
     FROM 
         agent_metric amTemp
    WHERE
        amTemp.TENANT_LOCATION_ID IN (@locationIds)
            AND amTemp.metricDate BETWEEN '@startDate' AND '@endDate'
    GROUP BY amTemp.TENANT_LOCATION_ID , amTemp.metricDate) AS AM ON (di.metricDate = AM.metricDate 
         AND tl.id = AM.TENANT_LOCATION_ID) 
         LEFT OUTER JOIN 
     (SELECT  
         TENANT_LOCATION_ID, 
             (CASE 
                 WHEN o.productMetricsValidationType = 0 THEN pm.arrivalDate 
                 WHEN o.productMetricsValidationType = 1 THEN pm.businessDate 
                 ELSE pm.departureDate 
             END) AS metricDate 
     FROM 
         product_metric pm 
     INNER JOIN tenant_location o ON (pm.TENANT_LOCATION_ID = o.id) WHERE
        pm.TENANT_LOCATION_ID IN (@locationIds)
            AND (CASE
            WHEN o.productMetricsValidationType = 0 THEN pm.arrivalDate
            WHEN o.productMetricsValidationType = 1 THEN pm.businessDate
            ELSE pm.departureDate
        END) BETWEEN '@startDate' AND '@endDate'
    GROUP BY pm.TENANT_LOCATION_ID , (CASE
        WHEN o.productMetricsValidationType = 0 THEN pm.arrivalDate
        WHEN o.productMetricsValidationType = 1 THEN pm.businessDate
        ELSE pm.departureDate
    END)) AS AP ON (di.metricDate = AP.metricDate 
         AND tl.id = AP.TENANT_LOCATION_ID)  
 WHERE 
     tl.id IN (@locationIds) 
        AND di.metricDate BETWEEN '@startDate' AND '@endDate'