SELECT 
    agentInfo.id AS id,
    IF(agentInfo.userId IS NULL,-1,agentInfo.userId) AS userId,
    IF(agentInfo.userName IS NULL,'-',agentInfo.userName) AS userName,
    @monthVar AS month,
    @yearVar AS year,
    agentInfo.unit AS unit,
    agentInfo.isMoney AS isMoney,
    agentInfo.isDisplayInTable AS isDisplayInTable,
    IF(agentInfo.reportName IS NULL,'-',agentInfo.reportName) AS reportName,
    IF(agentInfo.reportDisplayName IS NULL,'-',agentInfo.reportDisplayName) AS reportDisplayName,
    IF(agentInfo.mtdValue IS NULL,0,ROUND(agentInfo.mtdValue, 2)) AS mtdValue,
    IF(agentInfo.ytdValue IS NULL,0,ROUND(agentInfo.ytdValue, 2)) AS ytdValue,
    IF(agentInfo.varienceToPreMonth IS NULL,0,ROUND(agentInfo.varienceToPreMonth, 2)) AS varienceToPreMonth,
    IF(agentInfo.varienceToPreMonthPer IS NULL,0,ROUND(agentInfo.varienceToPreMonthPer, 2)) AS varienceToPreMonthPer,
    IF(agentInfo.projectedEOM IS NULL,0,ROUND(agentInfo.projectedEOM, 2)) AS projectedEOM,
    agentInfo.isFNBReport as isFNBReport 
FROM
    (SELECT 
        rh.ID AS id,
            mainFinal.salesAgentId AS userId,
            mainFinal.salesAgent AS userName,
            rh.NAME AS reportName,
            rh.DISPLAY_NAME AS reportDisplayName,
            rh.UNIT,
            rh.IS_MONEY as isMoney,
            rh.IS_DISPLAY_IN_TABLE AS isDisplayInTable,
            rh.isFNBReport,
            CASE
                WHEN rh.NAME = 'totalUpsellAmount' THEN incrementalRevenue
                WHEN rh.NAME = 'majorUpsellCharge' THEN upsellRevenue
                WHEN rh.NAME = 'noOfUpsellRoomNights' THEN upsellNights
                WHEN rh.NAME = 'noOfRoomUpsell' THEN upsells
                WHEN rh.NAME = 'upsellRoomAverageRate' THEN averageUpsellRate
                WHEN rh.NAME = 'idr' THEN idr
                WHEN rh.NAME = 'uoConversion' THEN entertainmentUpsellConversion
                WHEN rh.NAME = 'conversion' THEN upsellConversion
                WHEN rh.NAME = 'checkedInNights' THEN nightsCheckedIn
                WHEN rh.name = 'checkedInNightsAvg' THEN nightsCheckedInAvg
                WHEN rh.NAME = 'averageRevenuePerOrder' THEN averageRevenuePerOrder
                WHEN rh.NAME = 'roomConversion' THEN roomConversion
                WHEN rh.NAME = 'upgradeConversion' THEN upgradeConversion
                WHEN rh.NAME = 'adr' THEN adr
                WHEN rh.NAME = 'admissionUnits' THEN admissionUnits
                WHEN rh.NAME = 'ancillaryUnits' THEN ancillaryUnits
                WHEN rh.NAME = 'upgradeAdmission' THEN upgradeAdmission
                WHEN rh.NAME = 'admissionYield' THEN admissionYield
                WHEN rh.NAME = 'rpau' THEN rpau
                WHEN rh.NAME = 'rpu' THEN rpu
                WHEN rh.NAME = 'rpt' THEN rpt
                WHEN rh.NAME = 'contributionMargin' THEN contributionMargin
                WHEN rh.NAME = 'transactions' THEN transactions
                WHEN rh.NAME = 'admissionRevenue' THEN admissionRevenue
                WHEN rh.NAME = 'ancillaryRevenue' THEN ancillaryRevenue
                WHEN rh.NAME = 'CallOrders' THEN orders
                WHEN rh.NAME = 'UpselFlag' THEN upsellFlag
                WHEN rh.name = 'ancillaryCommission' THEN ancillaryCommission
				WHEN rh.name = 'revenuePerCheck' THEN revenuePerCheck
				WHEN rh.name = 'revenuePerCover' THEN revenuePerCover
				WHEN rh.name = 'revenuePerHour' THEN revenuePerHour 
				WHEN rh.name = 'covers' THEN covers 
				WHEN rh.name = 'checkCount' THEN checkCount  
				WHEN rh.name = 'arrivals' THEN arrivals				
				WHEN rh.name = 'arrivalsAvg' THEN arrivalsAvg
				WHEN rh.name = 'policiesPerClient' THEN policiesPerClient	
            END AS mtdValue,
            CASE
                WHEN rh.NAME = 'totalUpsellAmount' THEN ytdIncrementalRevenue
                WHEN rh.NAME = 'majorUpsellCharge' THEN ytdUpsellRevenue
                WHEN rh.NAME = 'noOfUpsellRoomNights' THEN ytdUpsellNights
                WHEN rh.NAME = 'noOfRoomUpsell' THEN ytdUpsells
                WHEN rh.NAME = 'upsellRoomAverageRate' THEN ytdAverageUpsellRate
                WHEN rh.NAME = 'idr' THEN ytdIdr
                WHEN rh.NAME = 'uoConversion' THEN ytdEntertainmentUpsellConversion
                WHEN rh.NAME = 'conversion' THEN ytdUpsellConversion
                WHEN rh.NAME = 'checkedInNights' THEN ytdNightsCheckedIn
                WHEN rh.name = 'checkedInNightsAvg' THEN ytdNightsCheckedInAvg
                WHEN rh.NAME = 'averageRevenuePerOrder' THEN ytdAverageRevenuePerOrder
                WHEN rh.NAME = 'roomConversion' THEN ytdRoomConversion
                WHEN rh.NAME = 'upgradeConversion' THEN ytdUpgradeConversion
                WHEN rh.NAME = 'adr' THEN ytdAdr
                WHEN rh.NAME = 'admissionUnits' THEN ytdAdmissionUnits
                WHEN rh.NAME = 'ancillaryUnits' THEN ytdAncillaryUnits
                WHEN rh.NAME = 'upgradeAdmission' THEN ytdUpgradeAdmission
                WHEN rh.NAME = 'admissionYield' THEN ytdAdmissionYield
                WHEN rh.NAME = 'rpau' THEN ytdRpau
                WHEN rh.NAME = 'rpu' THEN ytdRpu
                WHEN rh.NAME = 'rpt' THEN ytdRpt
                WHEN rh.NAME = 'transactions' THEN ytdTransactions
                WHEN rh.NAME = 'admissionRevenue' THEN ytdAdmissionRevenue
                WHEN rh.NAME = 'ancillaryRevenue' THEN ytdAncillaryRevenue
                WHEN rh.NAME = 'CallOrders' THEN ytdOrders
                WHEN rh.NAME = 'UpselFlag' THEN ytdUpsellFlag
                WHEN rh.name = 'ancillaryCommission' THEN ytdAncillaryCommission
				WHEN rh.name = 'revenuePerCheck' THEN ytdRevenuePerCheck
				WHEN rh.name = 'revenuePerCover' THEN ytdRevenuePerCover
				WHEN rh.name = 'revenuePerHour' THEN ytdRevenuePerHour 
				WHEN rh.name = 'covers' THEN ytdCovers 
				WHEN rh.name = 'checkCount' THEN ytdCheckCount
				WHEN rh.name = 'arrivals' THEN ytdArrivals
				WHEN rh.name = 'arrivalsAvg' THEN ytdArrivalsAvg
				WHEN rh.name = 'policiesPerClient' THEN ytdPoliciesPerClient
            END AS ytdValue,
            CASE
                WHEN rh.NAME = 'totalUpsellAmount' THEN varIncrementalRevenue
                WHEN rh.NAME = 'majorUpsellCharge' THEN varUpsellRevenue
                WHEN rh.NAME = 'noOfUpsellRoomNights' THEN varUpsellNights
                WHEN rh.NAME = 'noOfRoomUpsell' THEN varUpsells
                WHEN rh.NAME = 'upsellRoomAverageRate' THEN varAverageUpsellRate
                WHEN rh.NAME = 'idr' THEN varIdr
                WHEN rh.NAME = 'uoConversion' THEN varEntertainmentUpsellConversion
                WHEN rh.NAME = 'conversion' THEN varUpsellConversion
                WHEN rh.NAME = 'checkedInNights' THEN varNightsCheckedIn
                WHEN rh.name = 'checkedInNightsAvg' THEN varNightsCheckedInAvg
                WHEN rh.NAME = 'averageRevenuePerOrder' THEN varAverageRevenuePerOrder
                WHEN rh.NAME = 'roomConversion' THEN varRoomConversion
                WHEN rh.NAME = 'upgradeConversion' THEN varUpgradeConversion
                WHEN rh.NAME = 'adr' THEN varAdr
                WHEN rh.NAME = 'admissionUnits' THEN varAdmissionUnits
                WHEN rh.NAME = 'ancillaryUnits' THEN varAncillaryUnits
                WHEN rh.NAME = 'upgradeAdmission' THEN varUpgradeAdmission
                WHEN rh.NAME = 'admissionYield' THEN varAdmissionYield
                WHEN rh.NAME = 'rpau' THEN varRpau
                WHEN rh.NAME = 'rpu' THEN varRpu
                WHEN rh.NAME = 'rpt' THEN varRpt
                WHEN rh.NAME = 'transactions' THEN varTransactions
                WHEN rh.NAME = 'admissionRevenue' THEN varAdmissionRevenue
                WHEN rh.NAME = 'ancillaryRevenue' THEN varAncillaryRevenue
                WHEN rh.NAME = 'CallOrders' THEN varOrders
                WHEN rh.NAME = 'UpselFlag' THEN varUpsellFlag
                WHEN rh.name = 'ancillaryCommission' THEN varAncillaryCommission
				WHEN rh.name = 'revenuePerCheck' THEN varRevenuePerCheck
				WHEN rh.name = 'revenuePerCover' THEN varRevenuePerCover
				WHEN rh.name = 'revenuePerHour' THEN varRevenuePerHour  
				WHEN rh.name = 'covers' THEN varCovers 
				WHEN rh.name = 'checkCount' THEN varCheckCount
				WHEN rh.name = 'arrivals' THEN varArrivals
				WHEN rh.name = 'arrivalsAvg' THEN varArrivalsAvg
				WHEN rh.name = 'policiesPerClient' THEN varPoliciesPerClient
            END AS varienceToPreMonth,
            CASE
                WHEN rh.NAME = 'totalUpsellAmount' THEN varIncrementalRevenuePer
                WHEN rh.NAME = 'majorUpsellCharge' THEN varUpsellRevenuePer
                WHEN rh.NAME = 'noOfUpsellRoomNights' THEN varUpsellNightsPer
                WHEN rh.NAME = 'noOfRoomUpsell' THEN varUpsellsPer
                WHEN rh.NAME = 'upsellRoomAverageRate' THEN varAverageUpsellRatePer
                WHEN rh.NAME = 'idr' THEN varIdrPer
                WHEN rh.NAME = 'uoConversion' THEN varEntertainmentUpsellConversionPer
                WHEN rh.NAME = 'conversion' THEN varUpsellConversionPer
                WHEN rh.NAME = 'checkedInNights' THEN varNightsCheckedInPer
                WHEN rh.name = 'checkedInNightsAvg' THEN varNightsCheckedInPerAvg
                WHEN rh.NAME = 'averageRevenuePerOrder' THEN varAverageRevenuePerOrderPer
                WHEN rh.NAME = 'roomConversion' THEN varRoomConversionPer
                WHEN rh.NAME = 'upgradeConversion' THEN varUpgradeConversionPer
                WHEN rh.NAME = 'adr' THEN varAdrPer
                WHEN rh.NAME = 'admissionUnits' THEN varAdmissionUnitsPer
                WHEN rh.NAME = 'ancillaryUnits' THEN varAncillaryUnitsPer
                WHEN rh.NAME = 'upgradeAdmission' THEN varUpgradeAdmissionPer
                WHEN rh.NAME = 'admissionYield' THEN varAdmissionYieldPer
                WHEN rh.NAME = 'rpau' THEN varRpauPer
                WHEN rh.NAME = 'rpu' THEN varRpuPer
                WHEN rh.NAME = 'rpt' THEN varRptPer
                WHEN rh.NAME = 'transactions' THEN varTransactionsPer
                WHEN rh.NAME = 'admissionRevenue' THEN varAdmissionRevenuePer
                WHEN rh.NAME = 'ancillaryRevenue' THEN varAncillaryRevenuePer
                WHEN rh.NAME = 'CallOrders' THEN varOrdersPer
                WHEN rh.NAME = 'UpselFlag' THEN varUpsellFlagPer
                WHEN rh.name = 'ancillaryCommission' THEN varAncillaryCommissionPer
				WHEN rh.name = 'revenuePerCheck' THEN varRevenuePerCheckPer
				WHEN rh.name = 'revenuePerCover' THEN varRevenuePerCoverPer
				WHEN rh.name = 'revenuePerHour' THEN varRevenuePerHourPer  
				WHEN rh.name = 'covers' THEN varCoversPer 
				WHEN rh.name = 'checkCount' THEN varCheckCountPer
				WHEN rh.name = 'arrivals' THEN varArrivalsPer
				WHEN rh.name = 'arrivalsAvg' THEN varArrivalsPerAvg
				WHEN rh.name = 'policiesPerClient' THEN varPoliciesPerClientPer
            END AS varienceToPreMonthPer,
            CASE
                WHEN rh.NAME = 'totalUpsellAmount' THEN incrementalRevenueEOM
                WHEN rh.NAME = 'majorUpsellCharge' THEN upsellRevenueEOM
                WHEN rh.NAME = 'noOfUpsellRoomNights' THEN upsellNightsEOM
                WHEN rh.NAME = 'noOfRoomUpsell' THEN upsellsEOM
                WHEN rh.NAME = 'upsellRoomAverageRate' THEN averageUpsellRateEOM
                WHEN rh.NAME = 'idr' THEN idrEOM
                WHEN rh.NAME = 'uoConversion' THEN entertainmentUpsellConversionEOM
                WHEN rh.NAME = 'conversion' THEN upsellConversionEOM
                WHEN rh.NAME = 'checkedInNights' THEN nightsCheckedInEOM
                WHEN rh.name = 'checkedInNightsAvg' THEN nightsCheckedInEOMAvg
                WHEN rh.NAME = 'averageRevenuePerOrder' THEN averageRevenuePerOrderEOM
                WHEN rh.NAME = 'roomConversion' THEN roomConversionEOM
                WHEN rh.NAME = 'upgradeConversion' THEN upgradeConversionEOM
                WHEN rh.NAME = 'adr' THEN adrEOM
                WHEN rh.NAME = 'admissionUnits' THEN admissionUnitsEOM
                WHEN rh.NAME = 'ancillaryUnits' THEN ancillaryUnitsEOM
                WHEN rh.NAME = 'upgradeAdmission' THEN upgradeAdmissionEOM
                WHEN rh.NAME = 'admissionYield' THEN admissionYieldEOM
                WHEN rh.NAME = 'rpau' THEN rpauEOM
                WHEN rh.NAME = 'rpu' THEN rpuEOM
                WHEN rh.NAME = 'rpt' THEN rptEOM
                WHEN rh.name = 'revenuePerCheck' THEN revenuePerCheckEOM
				WHEN rh.name = 'revenuePerCover' THEN revenuePerCoverEOM
				WHEN rh.name = 'revenuePerHour' THEN revenuePerHourEOM  
				WHEN rh.name = 'covers' THEN coversEOM 
				WHEN rh.name = 'checkCount' THEN checkCountEOM
				WHEN rh.name = 'arrivals' THEN arrivalsEOM
				WHEN rh.name = 'arrivalsAvg' THEN arrivalsEOMAvg
				WHEN rh.name = 'policiesPerClient' THEN policiesPerClientEOM
            END AS projectedEOM 
    FROM
        report_helper rh
    LEFT OUTER JOIN (SELECT 
        main.salesAgentId,
            main.salesAgent,
            main.incrementalRevenue,
            main.ytdIncrementalRevenue,
            main.varIncrementalRevenue,
            main.varIncrementalRevenuePer,
            main.incrementalRevenueEOM,
            main.upsellRevenue,
            main.ytdUpsellRevenue,
            main.varUpsellRevenue,
            main.varUpsellRevenuePer,
            main.upsellRevenueEOM,
            main.upsellNights,
            main.ytdUpsellNights,
            main.varUpsellNights,
            main.varUpsellNightsPer,
            main.upsellNightsEOM,
            main.upsells,
            main.ytdUpsells,
            main.varUpsells,
            main.varUpsellsPer,
            main.upsellsEOM,
            main.averageUpsellRate,
            main.ytdAverageUpsellRate,
            (main.averageUpsellRate - main.preAverageUpsellRate) AS varAverageUpsellRate,
            ((main.averageUpsellRate - main.preAverageUpsellRate) / main.preAverageUpsellRate) * 100 AS varAverageUpsellRatePer,
            0 AS averageUpsellRateEOM,
            main.idr,
            main.ytdIdr,
            (main.idr - main.preIdr) AS varIdr,
            IF(main.preIdr = 0, 0, ((main.idr - main.preIdr) / main.preIdr) * 100) AS varIdrPer,
            0 AS idrEOM,
            main.entertainmentUpsellConversion,
            main.ytdEntertainmentUpsellConversion,
            (main.entertainmentUpsellConversion - main.preEntertainmentUpsellConversion) AS varEntertainmentUpsellConversion,
            IF(main.preEntertainmentUpsellConversion = 0, 0, ((main.entertainmentUpsellConversion - main.preEntertainmentUpsellConversion) / main.preEntertainmentUpsellConversion) * 100) AS varEntertainmentUpsellConversionPer,
            0 AS entertainmentUpsellConversionEOM,
            main.upsellConversion,
            main.ytdUpsellConversion,
            (main.upsellConversion - main.preUpsellConversion) AS varUpsellConversion,
            IF(main.preUpsellConversion = 0, 0, ((main.upsellConversion - main.preUpsellConversion) / main.preUpsellConversion) * 100) AS varUpsellConversionPer,
            0 AS upsellConversionEOM,
            main.nightsCheckedIn,
            main.ytdNightsCheckedIn,
            main.varNightsCheckedIn,
            main.varNightsCheckedInPer,
            main.nightsCheckedInEOM,
            ROUND(main.nightsCheckedInAvg, 0) as nightsCheckedInAvg,
		    ROUND(main.ytdNightsCheckedInAvg, 0) as ytdNightsCheckedInAvg,
		    ROUND(main.varNightsCheckedInAvg, 0) as varNightsCheckedInAvg,
			main.varNightsCheckedInPerAvg,
			main.nightsCheckedInEOMAvg,
            main.averageRevenuePerOrder,
            main.ytdAverageRevenuePerOrder,
            main.varAverageRevenuePerOrder,
            main.varAverageRevenuePerOrderPer,
            0 AS averageRevenuePerOrderEOM,
            main.roomConversion,
            main.ytdRoomConversion,
            main.varRoomConversion,
            main.varRoomConversionPer,
            0 AS roomConversionEOM,
            main.upgradeConversion,
            main.ytdUpgradeConversion,
            main.varUpgradeConversion,
            main.varUpgradeConversionPer,
            0 AS upgradeConversionEOM,
            main.adr,
            main.ytdAdr,
            main.varAdr,
            main.varAdrPer,
            0 AS adrEOM,
            main.admissionUnits,
            main.ytdAdmissionUnits,
            main.varAdmissionUnits,
            main.varAdmissionUnitsPer,
            0 AS admissionUnitsEOM,
            main.ancillaryUnits,
            main.ytdAncillaryUnits,
            main.varAncillaryUnits,
            main.varAncillaryUnitsPer,
            0 AS ancillaryUnitsEOM,
            main.upgradeAdmission,
            main.ytdUpgradeAdmission,
            main.varUpgradeAdmission,
            main.varUpgradeAdmissionPer,
            0 AS upgradeAdmissionEOM,
            main.admissionYield,
            main.ytdAdmissionYield,
            main.varAdmissionYield,
            main.varAdmissionYieldPer,
            0 AS admissionYieldEOM,
            main.rpau,
            main.ytdRpau,
            main.varRpau,
            main.varRpauPer,
            0 AS rpauEOM,
            main.rpu,
            main.ytdRpu,
            main.varRpu,
            main.varRpuPer,
            0 AS rpuEOM,
            main.rpt,
            main.ytdRpt,
            main.varRpt,
            main.varRptPer,
            0 AS rptEOM,
            main.arrivals,
            main.ytdArrivals,
            0 as varArrivals,
		    0 as varArrivalsPer,
			main.arrivalsEOM,
			main.arrivalsAvg,
		    main.ytdArrivalsAvg,  
		    0 as varArrivalsAvg,
		    0 as varArrivalsPerAvg,
			main.arrivalsEOMAvg,
		    IF(main.nightsCheckedIn = 0 , 0 ,((main.arrivals)/main.nightsCheckedIn)) as policiesPerClient,
		    IF(main.ytdNightsCheckedIn = 0 , 0 ,((main.ytdArrivals)/main.ytdNightsCheckedIn)) as ytdPoliciesPerClient,
		    0 as varPoliciesPerClient,
		    0 as varPoliciesPerClientPer,
		    IF(main.nightsCheckedInEOM = 0 , 0 ,((main.arrivalsEOM)/main.nightsCheckedInEOM)) as policiesPerClientEOM,
            (CASE
                WHEN main.idr = 0 AND main.ytdIdr != 0 THEN - 100
                WHEN main.ytdIdr = 0 AND main.idr != 0 THEN 100
                WHEN
                    main.idr IS NOT NULL
                        AND main.ytdIdr IS NOT NULL
                        AND main.ytdIdr != 0
                THEN
                    ((main.idr - main.ytdIdr) / main.ytdIdr) * 100
                ELSE 0
            END) AS crTrend,
            (CASE
                WHEN
                    main.incrementalRevenue = 0
                        AND main.ytdIncrementalRevenue != 0
                THEN
                    - 100
                WHEN
                    main.ytdIncrementalRevenue = 0
                        AND main.incrementalRevenue != 0
                THEN
                    100
                WHEN
                    main.incrementalRevenue IS NOT NULL
                        AND main.ytdIncrementalRevenue IS NOT NULL
                        AND main.ytdIncrementalRevenue != 0
                THEN
                    ((main.incrementalRevenue - main.ytdIncrementalRevenue) / main.ytdIncrementalRevenue) * 100
                ELSE 0
            END) AS trend,
            main.upsellFlag,
            main.ytdUpsellFlag,
            main.varUpsellFlag,
            main.varUpsellFlagPer,
            main.admissionRevenue,
            main.ytdAdmissionRevenue,
            main.varAdmissionRevenue,
            main.varAdmissionRevenuePer,
            main.ancillaryRevenue,
            main.ytdAncillaryRevenue,
            main.varAncillaryRevenue,
            main.varAncillaryRevenuePer,
            main.transactions,
            main.ytdTransactions,
            main.varTransactions,
            main.varTransactionsPer,
            main.orders,
            main.ytdOrders,
            main.varOrders,
            main.varOrdersPer,
            main.excludedadmissionUnits,
            main.contributionMargin,
            main.ticketsSold,
            main.ancillaryCommission,
			main.ytdAncillaryCommission,
			main.varAncillaryCommission,
			main.varAncillaryCommissionPer,
			main.revenuePerCheck,
			main.ytdRevenuePerCheck,
			main.varRevenuePerCheck,
			main.varRevenuePerCheckPer,
			main.revenuePerCheckEOM,  
			main.revenuePerCover,
			main.ytdRevenuePerCover,
			main.varRevenuePerCover,
			main.varRevenuePerCoverPer,
			main.revenuePerCoverEOM,
			main.revenuePerHour,
			main.ytdRevenuePerHour,
			main.varRevenuePerHour,
			main.varRevenuePerHourPer,
			main.revenuePerHourEOM,
			main.covers,
			main.ytdCovers,
			main.varCovers,
			main.varCoversPer,
			main.coversEOM,
			main.checkCount,
			main.ytdCheckCount,
			main.varCheckCount,
			main.varCheckCountPer,
			main.checkCountEOM  
    FROM
        (SELECT 
        main1.salesAgentId,
            main1.salesAgent,
            main1.incrementalRevenue,
            IF(main3.incrementalRevenue IS NULL, 0, main3.incrementalRevenue) AS ytdIncrementalRevenue,
            (main1.incrementalRevenue - main5.incrementalRevenue) AS varIncrementalRevenue,
            ((main1.incrementalRevenue - main5.incrementalRevenue) / main5.incrementalRevenue) * 100 AS varIncrementalRevenuePer,
            CASE
                WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.incrementalRevenue)
                WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.incrementalRevenue) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS incrementalRevenueEOM,
            main1.upsellRevenue,
            main3.upsellRevenue AS ytdUpsellRevenue,
            (main1.upsellRevenue - main5.upsellRevenue) AS varUpsellRevenue,
            ((main1.upsellRevenue - main5.upsellRevenue) / main5.upsellRevenue) * 100 AS varUpsellRevenuePer,
            CASE
                WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.upsellRevenue)
                WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.upsellRevenue) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS upsellRevenueEOM,
            main1.upsellNights,
            main3.upsellNights AS ytdUpsellNights,
            (main1.upsellNights - main5.upsellNights) AS varUpsellNights,
            ((main1.upsellNights - main5.upsellNights) / main5.upsellNights) * 100 AS varUpsellNightsPer,
            CASE
                WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.upsellNights)
                WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.upsellNights) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS upsellNightsEOM,
            main1.upsells,
            main3.upsells AS ytdUpsells,
            (main1.upsells - main5.upsells) AS varUpsells,
            ((main1.upsells - main5.upsells) / main5.upsells) * 100 AS varUpsellsPer,
            CASE
                WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.upsells)
                WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.upsells) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS upsellsEOM,
            IF(main1.upsellRevenue IS NOT NULL
                AND main1.upsellNights IS NOT NULL
                AND main1.upsellNights != 0, (main1.upsellRevenue / main1.upsellNights), 0) AS averageUpsellRate,
            IF(main3.upsellRevenue IS NOT NULL
                AND main3.upsellNights IS NOT NULL
                AND main3.upsellNights != 0, (main3.upsellRevenue / main3.upsellNights), 0) AS ytdAverageUpsellRate,
            IF(main5.upsellRevenue IS NOT NULL
                AND main5.upsellNights IS NOT NULL
                AND main5.upsellNights != 0, (main5.upsellRevenue / main5.upsellNights), 0) AS preAverageUpsellRate,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main2.nightsCheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.incrementalRevenue / main2.nightsCheckedIn), 0) AS idr,
            IF(main3.incrementalRevenue IS NOT NULL
                AND main4.nightsCheckedIn IS NOT NULL
                AND main4.nightsCheckedIn != 0, (main3.incrementalRevenue / main4.nightsCheckedIn), 0) AS ytdIdr,
            IF(main5.incrementalRevenue IS NOT NULL
                AND main6.nightsCheckedIn IS NOT NULL
                AND main6.nightsCheckedIn != 0, (main5.incrementalRevenue / main6.nightsCheckedIn), 0) AS preIdr,
            IF(main1.orders IS NOT NULL
                AND main2.nightscheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.orders / main2.nightsCheckedIn) * 100, 0) AS entertainmentUpsellConversion,
            IF(main3.orders IS NOT NULL
                AND main4.nightscheckedIn IS NOT NULL
                AND main4.nightsCheckedIn != 0, (main3.orders / main4.nightsCheckedIn) * 100, 0) AS ytdEntertainmentUpsellConversion,
            IF(main5.orders IS NOT NULL
                AND main6.nightscheckedIn IS NOT NULL
                AND main6.nightsCheckedIn != 0, (main5.orders / main6.nightsCheckedIn) * 100, 0) AS preEntertainmentUpsellConversion,
            IF(main1.upsells IS NOT NULL
                AND main2.arrivals IS NOT NULL
                AND main2.arrivals != 0, (main1.upsells / main2.arrivals) * 100, 0) AS upsellConversion,
            IF(main3.upsells IS NOT NULL
                AND main4.arrivals IS NOT NULL
                AND main4.arrivals != 0, (main3.upsells / main4.arrivals) * 100, 0) AS ytdUpsellConversion,
            IF(main5.upsells IS NOT NULL
                AND main6.arrivals IS NOT NULL
                AND main6.arrivals != 0, (main5.upsells / main6.arrivals) * 100, 0) AS preUpsellConversion,
            IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn) AS nightsCheckedIn,
            IF(main4.nightsCheckedIn IS NULL, 0, main4.nightsCheckedIn) AS ytdNightsCheckedIn,
            CASE
                WHEN main2.nightsCheckedIn > 0 AND main6.nightsCheckedIn > 0 THEN (main2.nightsCheckedIn - main6.nightsCheckedIn)
                WHEN main2.nightsCheckedIn > 0 AND main6.nightsCheckedIn = 0 THEN main2.nightsCheckedIn
                WHEN main2.nightsCheckedIn = 0 AND main6.nightsCheckedIn > 0 THEN -main6.nightsCheckedIn
                ELSE 0
            END AS varNightsCheckedIn,
            IF(main2.nightsCheckedIn IS NOT NULL
                AND main6.nightsCheckedIn IS NOT NULL
                AND main6.nightsCheckedIn != 0, ((main2.nightsCheckedIn - main6.nightsCheckedIn) / main6.nightsCheckedIn) * 100, 0) AS varNightsCheckedInPer,
            CASE
                WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main2.nightsCheckedIn)
                WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main2.nightsCheckedIn) / (DAY(NOW()) - 1)) * (DAY(LAST_DAY(NOW()))))
                ELSE 0
            END AS nightsCheckedInEOM,
            If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0) AS nightsCheckedInAvg,
			If(main4.avgCount > 0,IF(main4.nightsCheckedIn IS NULL, 0, main4.nightsCheckedIn/main4.avgCount),0) AS ytdNightsCheckedInAvg,
			CASE WHEN main2.nightsCheckedIn > 0 and main6.nightsCheckedIn > 0 THEN (If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0) - If(main6.avgCount > 0,IF(main6.nightsCheckedIn IS NULL, 0, main6.nightsCheckedIn/main6.avgCount),0))
				WHEN (main2.nightsCheckedIn > 0 and (main6.nightsCheckedIn = 0 or main6.nightsCheckedIn is null)) THEN (If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0))
				WHEN ((main2.nightsCheckedIn = 0 or main2.nightsCheckedIn is null) and main6.nightsCheckedIn > 0) THEN -(If(main6.avgCount > 0,IF(main6.nightsCheckedIn IS NULL, 0, main6.nightsCheckedIn/main6.avgCount),0))
				ELSE 0 END AS varNightsCheckedInAvg,	
			IF(main2.nightsCheckedIn IS NOT NULL and main6.nightsCheckedIn IS NOT NULL and main6.nightsCheckedIn != 0,((If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0) - If(main6.avgCount > 0,IF(main6.nightsCheckedIn IS NULL, 0, main6.nightsCheckedIn/main6.avgCount),0))/If(main6.avgCount > 0,IF(main6.nightsCheckedIn IS NULL, 0, main6.nightsCheckedIn/main6.avgCount),0))*100,0) as varNightsCheckedInPerAvg,
			CASE
				WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0))
				WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((If(main2.avgCount > 0,IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn/main2.avgCount),0))/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
				ELSE 0
			END AS nightsCheckedInEOMAvg,
            IF(main1.incrementalRevenue IS NOT NULL AND main1.orders IS NOT NULL AND main1.orders != 0, main1.incrementalRevenue / main1.orders, 0) AS averageRevenuePerOrder,
            IF(main3.incrementalRevenue IS NOT NULL AND main3.orders IS NOT NULL AND main3.orders != 0, main3.incrementalRevenue / main3.orders, 0) AS ytdAverageRevenuePerOrder,
            CASE
                WHEN main1.orders > 0 AND main5.orders > 0 THEN (main1.incrementalRevenue / main1.orders) - (main5.incrementalRevenue / main5.orders)
                WHEN main1.orders > 0 AND main5.orders = 0 THEN (main1.incrementalRevenue / main1.orders)
                WHEN main1.orders = 0 AND main5.orders > 0 THEN -(main5.incrementalRevenue / main5.orders)
                ELSE 0
            END AS varAverageRevenuePerOrder,
            IF(main1.incrementalRevenue IS NOT NULL AND main1.orders IS NOT NULL AND main1.orders != 0 AND main5.incrementalRevenue IS NOT NULL AND main5.orders IS NOT NULL
                AND main5.orders != 0, (((main1.incrementalRevenue / main1.orders) - (main5.incrementalRevenue / main5.orders)) / (main5.incrementalRevenue / main5.orders)) * 100, 0) AS varAverageRevenuePerOrderPer,
            IF(main1.orders IS NOT NULL AND main2.nightsCheckedIn IS NOT NULL AND main2.nightsCheckedIn != 0, (main1.orders / main2.nightsCheckedIn) * 100, 0) AS roomConversion,
            IF(main3.orders IS NOT NULL AND main4.nightsCheckedIn IS NOT NULL AND main4.nightsCheckedIn != 0, (main3.orders / main4.nightsCheckedIn) * 100, 0) AS ytdRoomConversion,
            CASE
                WHEN main2.nightsCheckedIn > 0 AND main2.nightsCheckedIn > 0 THEN (main1.orders / main2.nightsCheckedIn) * 100 - (main5.orders / main6.nightsCheckedIn) * 100
                WHEN main2.nightsCheckedIn > 0 AND main2.nightsCheckedIn = 0 THEN (main1.orders / main2.nightsCheckedIn) * 100
                WHEN main2.nightsCheckedIn = 0 AND main2.nightsCheckedIn > 0 THEN -(main5.orders / main6.nightsCheckedIn) * 100
                ELSE 0
            END AS varRoomConversion,
            IF(main1.orders IS NOT NULL AND main2.nightsCheckedIn IS NOT NULL AND main2.nightsCheckedIn != 0 AND main5.orders IS NOT NULL AND main6.nightsCheckedIn IS NOT NULL
                AND main6.nightsCheckedIn != 0, ((((main1.orders / main2.nightsCheckedIn) * 100) - ((main5.orders / main6.nightsCheckedIn) * 100)) / ((main5.orders / main6.nightsCheckedIn) * 100)) * 100, 0) AS varRoomConversionPer,
            IF(main1.upsellFlag IS NOT NULL AND main1.uniqueConfirmationNumber IS NOT NULL AND main1.uniqueConfirmationNumber != 0, (main1.upsellFlag / main1.uniqueConfirmationNumber) * 100, 0) AS upgradeConversion,
            IF(main3.upsellFlag IS NOT NULL AND main3.uniqueConfirmationNumber IS NOT NULL AND main3.uniqueConfirmationNumber != 0, (main3.upsellFlag / main3.uniqueConfirmationNumber) * 100, 0) AS ytdUpgradeConversion,
            CASE
                WHEN main1.uniqueConfirmationNumber > 0 AND main5.uniqueConfirmationNumber > 0 THEN (main1.upsellFlag / main1.uniqueConfirmationNumber) * 100 - (main5.upsellFlag / main5.uniqueConfirmationNumber) * 100
                WHEN main1.uniqueConfirmationNumber > 0 AND main5.uniqueConfirmationNumber = 0 THEN (main1.upsellFlag / main1.uniqueConfirmationNumber) * 100
                WHEN main1.uniqueConfirmationNumber = 0 AND main5.uniqueConfirmationNumber > 0 THEN -(main5.upsellFlag / main5.uniqueConfirmationNumber) * 100
                ELSE 0
            END AS varUpgradeConversion,
            IF(main1.upsellFlag IS NOT NULL AND main1.uniqueConfirmationNumber IS NOT NULL AND main1.uniqueConfirmationNumber != 0 AND main5.upsellFlag IS NOT NULL AND main5.uniqueConfirmationNumber IS NOT NULL
                AND main5.uniqueConfirmationNumber != 0, ((((main1.upsellFlag / main1.uniqueConfirmationNumber) * 100) - ((main5.upsellFlag / main5.uniqueConfirmationNumber) * 100)) / ((main5.upsellFlag / main5.uniqueConfirmationNumber) * 100)) * 100, 0) AS varUpgradeConversionPer,
            IF(main1.incrementalRevenue IS NOT NULL AND main1.ticketsSold IS NOT NULL AND main1.ticketsSold != 0, (main1.incrementalRevenue / main1.ticketsSold), 0) AS adr,
            IF(main3.incrementalRevenue IS NOT NULL AND main3.ticketsSold IS NOT NULL AND main3.ticketsSold != 0, (main3.incrementalRevenue / main3.ticketsSold), 0) AS ytdAdr,
            CASE
                WHEN main1.ticketsSold > 0 AND main5.ticketsSold > 0 THEN (main1.incrementalRevenue / main1.ticketsSold) - (main5.incrementalRevenue / main5.ticketsSold)
                WHEN main1.ticketsSold > 0 AND main5.ticketsSold = 0 THEN (main1.incrementalRevenue / main1.ticketsSold)
                WHEN main1.ticketsSold = 0 AND main5.ticketsSold > 0 THEN -(main5.incrementalRevenue / main5.ticketsSold)
                ELSE 0
            END AS varAdr,
            IF(main1.incrementalRevenue IS NOT NULL AND main1.ticketsSold IS NOT NULL AND main1.ticketsSold != 0 AND main5.incrementalRevenue IS NOT NULL AND main5.ticketsSold IS NOT NULL
                AND main5.ticketsSold != 0, ((((main1.incrementalRevenue / main1.ticketsSold)) - ((main5.incrementalRevenue / main5.ticketsSold))) / ((main5.incrementalRevenue / main5.ticketsSold))) * 100, 0) AS varAdrPer,
            IF(main1.admissionUnits IS NULL, 0, main1.admissionUnits) AS admissionUnits,
            IF(main3.admissionUnits IS NULL, 0, main3.admissionUnits) AS ytdAdmissionUnits,
            CASE
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits > 0 THEN (main1.admissionUnits - main5.admissionUnits)
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits = 0 THEN main1.admissionUnits
                WHEN main1.admissionUnits = 0 AND main5.admissionUnits > 0 THEN -main5.admissionUnits
                ELSE 0
            END AS varAdmissionUnits,
            IF(main1.admissionUnits IS NOT NULL AND main5.admissionUnits IS NOT NULL AND main5.admissionUnits != 0, ((main1.admissionUnits - main5.admissionUnits) / main5.admissionUnits) * 100, 0) AS varAdmissionUnitsPer,
            IF(main1.ancillaryUnits IS NULL, 0, main1.ancillaryUnits) AS ancillaryUnits,
            IF(main3.ancillaryUnits IS NULL, 0, main3.ancillaryUnits) AS ytdAncillaryUnits,
            CASE
                WHEN main1.ancillaryUnits > 0 AND main5.ancillaryUnits > 0 THEN (main1.ancillaryUnits - main5.ancillaryUnits)
                WHEN main1.ancillaryUnits > 0 AND main5.ancillaryUnits = 0 THEN main1.ancillaryUnits
                WHEN main1.ancillaryUnits = 0 AND main5.ancillaryUnits > 0 THEN -main5.ancillaryUnits
                ELSE 0
            END AS varAncillaryUnits,
            IF(main1.ancillaryUnits IS NOT NULL AND main5.ancillaryUnits IS NOT NULL
                AND main5.ancillaryUnits != 0, ((main1.ancillaryUnits - main5.ancillaryUnits) / main5.ancillaryUnits) * 100, 0) AS varAncillaryUnitsPer,
            IF(main1.admissionRevenue IS NULL, 0, main1.admissionRevenue) AS admissionRevenue,
            IF(main3.admissionRevenue IS NULL, 0, main3.admissionRevenue) AS ytdAdmissionRevenue,
            IF((main1.admissionRevenue - main5.admissionRevenue) IS NOT NULL, (main1.admissionRevenue - main5.admissionRevenue), 0) AS varAdmissionRevenue,
            IF((main1.admissionRevenue - main5.admissionRevenue) IS NOT NULL
                AND main5.admissionRevenue > 0, ((main1.admissionRevenue - main5.admissionRevenue) / main5.admissionRevenue) * 100, 0) AS varAdmissionRevenuePer,
            IF(main1.ancillaryRevenue IS NULL, 0, main1.ancillaryRevenue) AS ancillaryRevenue,
            IF(main3.ancillaryRevenue IS NULL, 0, main3.ancillaryRevenue) AS ytdAncillaryRevenue,
            IF((main1.ancillaryRevenue - main5.ancillaryRevenue) IS NOT NULL, (main1.ancillaryRevenue - main5.ancillaryRevenue), 0) AS varAncillaryRevenue,
            IF((main1.ancillaryRevenue - main5.ancillaryRevenue) IS NOT NULL
                AND main5.ancillaryRevenue > 0, ((main1.ancillaryRevenue - main5.ancillaryRevenue) / main5.ancillaryRevenue) * 100, 0) AS varAncillaryRevenuePer,
            IF(main1.excludedadmissionUnits IS NULL, 0, main1.excludedadmissionUnits) AS excludedadmissionUnits,
            IF(main1.transactions IS NULL, 0, main1.transactions) AS transactions,
            IF(main3.transactions IS NULL, 0, main3.transactions) AS ytdTransactions,
            IF((main1.transactions - main5.transactions) IS NOT NULL, (main1.transactions - main5.transactions), 0) AS varTransactions,
            IF((main1.transactions - main5.transactions) IS NOT NULL
                AND main5.transactions > 0, ((main1.transactions - main5.transactions) / main5.transactions) * 100, 0) AS varTransactionsPer,
            IF(main1.admissionUnits > 0, (main1.excludedadmissionUnits / main1.admissionUnits) * 100, 0) AS upgradeAdmission,
            IF(main3.admissionUnits > 0, (main3.excludedadmissionUnits / main3.admissionUnits) * 100, 0) AS ytdUpgradeAdmission,
            CASE
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits > 0 THEN (main1.excludedadmissionUnits / main1.admissionUnits) * 100 - (main5.excludedadmissionUnits / main5.admissionUnits) * 100
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits = 0 THEN (main1.excludedadmissionUnits / main1.admissionUnits) * 100
                WHEN main1.admissionUnits = 0 AND main5.admissionUnits > 0 THEN -(main5.excludedadmissionUnits / main5.admissionUnits) * 100
                ELSE 0
            END AS varUpgradeAdmission,
            IF(main1.admissionUnits IS NOT NULL AND main5.admissionUnits IS NOT NULL AND main5.admissionUnits != 0, ((((main1.excludedadmissionUnits / main1.admissionUnits) * 100) - ((main5.excludedadmissionUnits / main5.admissionUnits) * 100)) / ((main5.excludedadmissionUnits / main5.admissionUnits) * 100)) * 100, 0) AS varUpgradeAdmissionPer,
            IF(main1.admissionUnits > 0, (main1.excludedadmissionRevenue / main1.admissionUnits), 0) AS admissionYield,
            IF(main3.admissionUnits > 0, (main3.excludedadmissionRevenue / main3.admissionUnits), 0) AS ytdAdmissionYield,
            CASE
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits > 0 THEN (main1.excludedadmissionRevenue / main1.admissionUnits) - (main5.excludedadmissionRevenue / main5.admissionUnits)
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits = 0 THEN (main1.excludedadmissionRevenue / main1.admissionUnits)
                WHEN main1.admissionUnits = 0 AND main5.admissionUnits > 0 THEN -(main5.excludedadmissionRevenue / main5.admissionUnits)
                ELSE 0
            END AS varAdmissionYield,
            IF(main1.admissionUnits IS NOT NULL AND main5.admissionUnits IS NOT NULL
                AND main5.admissionUnits != 0, ((((main1.excludedadmissionRevenue / main1.admissionUnits)) - ((main5.excludedadmissionRevenue / main5.admissionUnits))) / ((main5.excludedadmissionRevenue / main5.admissionUnits))), 0) AS varAdmissionYieldPer,
            IF(main1.roomNights IS NOT NULL, (main1.incrementalRevenue / main1.roomNights), 0) AS rpu,
            IF(main3.roomNights IS NOT NULL, (main3.incrementalRevenue / main3.roomNights), 0) AS ytdRpu,
            CASE
                WHEN main1.roomNights > 0 AND main5.roomNights > 0 THEN (main1.incrementalRevenue / main1.roomNights) - (main5.incrementalRevenue / main5.roomNights)
                WHEN main1.roomNights > 0 AND main5.roomNights = 0 THEN (main1.incrementalRevenue / main1.roomNights)
                WHEN main1.roomNights = 0 AND main5.roomNights > 0 THEN -(main5.incrementalRevenue / main5.roomNights)
                ELSE 0
            END AS varRpu,
            IF(main1.roomNights IS NOT NULL AND main5.roomNights IS NOT NULL
                AND main5.roomNights != 0, (((main1.incrementalRevenue / main1.roomNights) - (main5.incrementalRevenue / main5.roomNights)) / (main5.incrementalRevenue / main5.roomNights)) * 100, 0) AS varRpuPer,
            IF(main1.admissionUnits IS NOT NULL, (main1.admissionRevenue / main1.admissionUnits), 0) AS rpau,
            IF(main3.admissionUnits IS NOT NULL, (main3.admissionRevenue / main3.admissionUnits), 0) AS ytdRpau,
            CASE
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits > 0 THEN (main1.admissionRevenue / main1.admissionUnits) - (main5.admissionRevenue / main5.admissionUnits)
                WHEN main1.admissionUnits > 0 AND main5.admissionUnits = 0 THEN (main1.admissionRevenue / main1.admissionUnits)
                WHEN main1.admissionUnits = 0 AND main5.admissionUnits > 0 THEN -(main5.admissionRevenue / main5.admissionUnits)
                ELSE 0
            END AS varRpau,
            IF(main1.admissionUnits IS NOT NULL
                AND main5.admissionUnits IS NOT NULL
                AND main5.admissionUnits != 0, (((main1.admissionRevenue / main1.admissionUnits) - (main5.admissionRevenue / main5.admissionUnits)) / (main5.admissionRevenue / main5.admissionUnits)) * 100, 0) AS varRpauPer,
            IF(main1.transactions IS NOT NULL, (main1.incrementalRevenue / main1.transactions), 0) AS rpt,
            IF(main3.transactions IS NOT NULL, (main3.incrementalRevenue / main3.transactions), 0) AS ytdRpt,
            CASE
                WHEN main1.transactions > 0 AND main5.transactions > 0 THEN (main1.incrementalRevenue / main1.transactions) - (main5.incrementalRevenue / main5.transactions)
                WHEN main1.transactions > 0 AND main5.transactions = 0 THEN (main1.incrementalRevenue / main1.transactions)
                WHEN main1.transactions = 0 AND main5.transactions > 0 THEN -(main5.incrementalRevenue / main5.transactions)
                ELSE 0
            END AS varRpt,
            IF(main1.transactions IS NOT NULL
                AND main5.transactions IS NOT NULL
                AND main5.transactions != 0, (((main1.incrementalRevenue / main1.transactions) - (main5.incrementalRevenue / main5.transactions)) / (main5.incrementalRevenue / main5.transactions)) * 100, 0) AS varRptPer,
            IF(main2.arrivals IS NULL, 0, main2.arrivals) AS arrivals,
            IF(main4.arrivals IS NULL, 0, main4.arrivals) AS ytdArrivals,
            CASE
				WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main2.arrivals)
				WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main2.arrivals)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
				ELSE 0
			END AS arrivalsEOM,
			If(main2.avgCount > 0,IF(main2.arrivals IS NULL, 0, main2.arrivals/main2.avgCount),0) AS arrivalsAvg,
			If(main4.avgCount > 0,IF(main4.arrivals IS NULL, 0, main4.arrivals/main4.avgCount),0) AS ytdArrivalsAvg,
            CASE
				WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (If(main2.avgCount > 0,IF(main2.arrivals IS NULL, 0, main2.arrivals/main2.avgCount),0))
				WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((If(main2.avgCount > 0,IF(main2.arrivals IS NULL, 0, main2.arrivals/main2.avgCount),0))/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
				ELSE 0
			END AS arrivalsEOMAvg,
            IF(main1.upsellFlag IS NULL, 0, main1.upsellFlag) AS upsellFlag,
            IF(main3.upsellFlag IS NULL, 0, main3.upsellFlag) AS ytdUpsellFlag,
            IF((main1.upsellFlag - main5.upsellFlag) IS NOT NULL, (main1.upsellFlag - main5.upsellFlag), 0) AS varUpsellFlag,
            IF((main1.upsellFlag - main5.upsellFlag) IS NOT NULL
                AND main5.upsellFlag > 0, ((main1.upsellFlag - main5.upsellFlag) / main5.upsellFlag) * 100, 0) AS varUpsellFlagPer,
            IF(main1.uniqueConfirmationNumber IS NULL, 0, main1.uniqueConfirmationNumber) AS uniqueConfirmationNumber,
            main1.orders AS orders,
            main3.orders AS ytdOrders,
            IF((main1.orders - main5.orders) IS NOT NULL, (main1.orders - main5.orders), 0) AS varOrders,
            IF((main1.orders - main5.orders) IS NOT NULL
                AND main5.orders > 0, ((main1.orders - main5.orders) / main5.orders) * 100, 0) AS varOrdersPer,
            IF(main7.incrementalRevenue IS NOT NULL, (main1.incrementalRevenue / main7.incrementalRevenue) * 100, 0) AS contributionMargin,
            main1.ticketsSold,
             IF(main1.ancillaryRevenue IS NULL, 0, main1.ancillaryRevenue * 0.01) AS ancillaryCommission,
            IF(main3.ancillaryRevenue IS NULL, 0, main3.ancillaryRevenue * 0.01) AS ytdAncillaryCommission,
            IF((main1.ancillaryRevenue - main5.ancillaryRevenue) is not null,(main1.ancillaryRevenue - main5.ancillaryRevenue) * 0.01,0) as varAncillarycommission,
            IF((main1.ancillaryRevenue - main5.ancillaryRevenue) is not null and main5.ancillaryRevenue > 0,(((main1.ancillaryRevenue - main5.ancillaryRevenue)*0.01)/(main5.ancillaryRevenue*0.01))*100,0) as varAncillaryCommissionPer,
            IF(main1.checkCount IS NULL, 0, (main1.incrementalRevenue/main1.checkCount)) AS revenuePerCheck,
            IF(main3.checkCount IS NULL, 0, (main3.incrementalRevenue/main3.checkCount)) AS ytdRevenuePerCheck,
            CASE WHEN main1.checkCount > 0 and main5.checkCount > 0 THEN (main1.incrementalRevenue/main1.checkCount) - (main5.incrementalRevenue/main5.checkCount)
				WHEN main1.checkCount > 0 and main5.checkCount = 0 THEN (main1.incrementalRevenue/main1.checkCount) 
				WHEN main1.checkCount = 0 and main5.checkCount > 0 THEN -(main5.incrementalRevenue/main5.checkCount)
				ELSE 0 END AS varRevenuePerCheck,			
			IF(main1.checkCount IS NOT NULL and main5.checkCount IS NOT NULL and main5.checkCount != 0, (((main1.incrementalRevenue/main1.checkCount) - (main5.incrementalRevenue/main5.checkCount))/(main5.incrementalRevenue/main5.checkCount))*100 , 0) AS varRevenuePerCheckPer,
			CASE
			WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.incrementalRevenue/main1.checkCount)
			WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.incrementalRevenue/main1.checkCount)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
			ELSE 0
			END AS revenuePerCheckEOM,
			IF(main1.covers is not null , (main1.incrementalRevenue/main1.covers) , 0 ) as revenuePerCover,
			IF(main3.covers is not null , (main3.incrementalRevenue/main3.covers),0) AS ytdRevenuePerCover,
			CASE WHEN main1.covers > 0 and main5.covers > 0 THEN (main1.incrementalRevenue/main1.covers) - (main5.incrementalRevenue/main5.covers)
				WHEN main1.covers > 0 and main5.covers = 0 THEN (main1.incrementalRevenue/main1.covers) 
				WHEN main1.covers = 0 and main5.covers > 0 THEN -(main5.incrementalRevenue/main5.covers)
				ELSE 0 END AS varRevenuePerCover,			
			IF(main1.covers IS NOT NULL and main5.covers IS NOT NULL and main5.covers != 0, (((main1.incrementalRevenue/main1.covers) - (main5.incrementalRevenue/main5.covers))/(main5.incrementalRevenue/main5.covers))*100 , 0) AS varRevenuePerCoverPer,
			CASE
			WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.incrementalRevenue/main1.covers)
			WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.incrementalRevenue/main1.covers)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
			ELSE 0
			END AS revenuePerCoverEOM,
			IF(main1.duration is not null , ((main1.incrementalRevenue*60)/main1.duration) , 0 ) as revenuePerHour,
			IF(main3.duration is not null , ((main1.incrementalRevenue*60)/main3.duration),0) AS ytdRevenuePerHour,
			CASE WHEN main1.duration > 0 and main5.duration > 0 THEN ((main1.incrementalRevenue*60)/main1.duration) - ((main5.incrementalRevenue*60)/main5.duration)
				WHEN main1.duration > 0 and main5.duration = 0 THEN ((main1.incrementalRevenue*60)/main1.duration) 
				WHEN main1.duration = 0 and main5.duration > 0 THEN -((main5.incrementalRevenue*60)/main5.duration)
				ELSE 0 END AS varRevenuePerHour,			
			IF(main1.duration IS NOT NULL and main5.duration IS NOT NULL and main5.duration != 0, ((((main1.incrementalRevenue*60)/main1.duration) - ((main5.incrementalRevenue*60)/main5.duration))/((main5.incrementalRevenue*60)/main5.duration))*100 , 0) AS varRevenuePerHourPer,
			CASE
			WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN ((main1.incrementalRevenue*60)/main1.duration)
			WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN ((((main1.incrementalRevenue*60)/main1.duration)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
			ELSE 0
			END AS revenuePerHourEOM,
			IF(main1.covers is not null , main1.covers , 0 ) as covers,
			IF(main3.covers is not null , main3.covers,0) AS ytdCovers,
			IF((main1.covers - main5.covers) is not null and main5.covers > 0,((main1.covers - main5.covers)/main5.covers)*100,0) as varCovers,
			IF((main1.covers - main5.covers) is not null and main5.covers > 0,((main1.covers - main5.covers)/main5.covers)*100,0) as varCoversPer,
			CASE
			WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.covers)
			WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.covers)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
			ELSE 0
			END AS coversEOM,
			IF(main1.checkCount is not null , main1.checkCount , 0 ) as checkCount,
			IF(main3.checkCount is not null , main3.checkCount,0) AS ytdCheckCount,
			IF((main1.checkCount - main5.checkCount) is not null and main5.checkCount > 0,((main1.checkCount - main5.checkCount)/main5.checkCount)*100,0) as varCheckCount,
			IF((main1.checkCount - main5.checkCount) is not null and main5.checkCount > 0,((main1.checkCount - main5.checkCount)/main5.checkCount)*100,0) as varCheckCountPer,
			CASE
			WHEN (YEAR(NOW()) >= @yearVar && MONTH(NOW()) != @monthVar) THEN (main1.checkCount)
			WHEN (YEAR(NOW()) > @yearVar || (YEAR(NOW()) = @yearVar && MONTH(NOW()) >= @monthVar)) THEN (((main1.checkCount)/(DAY(NOW()) - 1)) *(DAY(LAST_DAY(NOW()))))
			ELSE 0
			END AS checkCountEOM  
    FROM
        (SELECT 
        	pm.USER_ID AS salesAgentId,
            pm.TENANT_LOCATION_ID AS tenantLocationID,
            @roomNightsQuery AS roomNights,
            COUNT(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    COUNT(DISTINCT (CASE
                        WHEN p.isMajor = '1' THEN pm.ID
                        ELSE NULL
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS upsellNights,
            SUM(CASE
                WHEN pm.upsellCharge IS NULL THEN 0
                ELSE pm.upsellCharge
            END) AS salesRevenue,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,
           	SUM(@reportRevenueData) AS incrementalRevenue,
            SUM(CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    IF(pm.upsellCharge IS NOT NULL
                        AND p.isMajor = 1, pm.upsellCharge, 0)
                ELSE (CASE
                    WHEN
                        p.isRecurring = 0
                    THEN
                        IF(pm.upsellCharge IS NOT NULL
                            AND p.isMajor = 1, pm.upsellCharge, 0)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
                END)
            END) AS upsellRevenue,
            SUM((CASE
                WHEN pm.upsellFlag IS NULL THEN 0
                ELSE pm.upsellFlag
            END)) AS upsellFlag,
            COUNT(DISTINCT (CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber))) AS uniqueConfirmationNumber,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS admissionUnits,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 0
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS ancillaryUnits,
            SUM(IF(p.isMajor = 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS admissionRevenue,
            SUM(IF(p.isMajor = 0, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS ancillaryRevenue,
            COUNT(DISTINCT pm.confirmationNumber) AS transactions,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isExcludedProduct <> 1
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS excludedadmissionUnits,
            SUM(IF(p.isMajor = 1
                AND p.isExcludedProduct <> 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS excludedAdmissionRevenue,
            SUM((CASE
                WHEN pm.TicketsSold IS NULL THEN 0
                ELSE pm.TicketsSold
            END)) AS ticketsSold,
            SUM((case when pm.checkCount is null then 0 else pm.checkCount end)) as checkCount,
			SUM((case when pm.duration is null then 0 else pm.duration end)) as duration,
			SUM((case when pm.roomNights is null then 0 else pm.roomNights end)) as covers 
    FROM
        product_metric pm
    JOIN tenant_location tl ON pm.TENANT_LOCATION_ID = tl.ID
    JOIN location_group_product p ON pm.PRODUCT_ID = p.ID
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND MONTH(@reportDate) <= @monthVar
            AND YEAR(@reportDate) = @yearVar
            AND (@reportDate) <= CURDATE() 
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID  @locationGroupJoin) AS main3
    LEFT OUTER JOIN (SELECT 
        im.USER_ID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn,
            COUNT(im.id) AS avgCount 
    FROM
        agent_metric im
    JOIN user u ON (im.USER_ID = u.ID)
    JOIN tenant_location tl ON (im.TENANT_LOCATION_ID = tl.ID)
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
            AND MONTH(im.metricDate) <= @monthVar 
            AND YEAR(im.metricDate) = @yearVar
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID) AS main4 ON main3.salesAgentId = main4.salesAgentId
    LEFT OUTER JOIN (SELECT 
        pm.USER_ID AS salesAgentId,
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS salesAgent,
            pm.TENANT_LOCATION_ID AS organizationId,
           	@roomNightsQuery AS roomNights,
            COUNT(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    COUNT(DISTINCT (CASE
                        WHEN p.isMajor = '1' THEN pm.ID
                        ELSE NULL
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS upsellNights,
            SUM(CASE
                WHEN pm.upsellCharge IS NULL THEN 0
                ELSE pm.upsellCharge
            END) AS salesRevenue,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,
            SUM(@reportRevenueData) AS incrementalRevenue,
            SUM(CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    IF(pm.upsellCharge IS NOT NULL
                        AND p.isMajor = 1, pm.upsellCharge, 0)
                ELSE (CASE
                    WHEN
                        p.isRecurring = 0
                    THEN
                        IF(pm.upsellCharge IS NOT NULL
                            AND p.isMajor = 1, pm.upsellCharge, 0)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
                END)
            END) AS upsellRevenue,
            SUM((CASE
                WHEN pm.upsellFlag IS NULL THEN 0
                ELSE pm.upsellFlag
            END)) AS upsellFlag,
            COUNT(DISTINCT (CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber))) AS uniqueConfirmationNumber,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS admissionUnits,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 0
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS ancillaryUnits,
            SUM(IF(p.isMajor = 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS admissionRevenue,
            SUM(IF(p.isMajor = 0, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS ancillaryRevenue,
            IF(COUNT(DISTINCT pm.confirmationNumber) IS NOT NULL, COUNT(DISTINCT pm.confirmationNumber), 0) AS transactions,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isExcludedProduct <> 1
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS excludedadmissionUnits,
            SUM(IF(p.isMajor = 1
                AND p.isExcludedProduct <> 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS excludedAdmissionRevenue,
            SUM((CASE
                WHEN pm.TicketsSold IS NULL THEN 0
                ELSE pm.TicketsSold
            END)) AS ticketsSold,
            SUM((case when pm.checkCount is null then 0 else pm.checkCount end)) as checkCount,
			SUM((case when pm.duration is null then 0 else pm.duration end)) as duration, 
			SUM((case when pm.roomNights is null then 0 else pm.roomNights end)) as covers 
    FROM
        product_metric pm
    JOIN user u ON (pm.USER_ID = u.ID)
    JOIN user_profile up ON (u.ID = up.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @monthVar
			AND year(@reportDate) = @yearVar
			AND (@reportDate) <= CURDATE() 
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID @locationGroupJoin) AS main1 ON main3.salesAgentId = main1.salesAgentId
    LEFT OUTER JOIN (SELECT 
        im.USER_ID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn,
            COUNT(im.id) AS avgCount 
    FROM
        agent_metric im
    JOIN user u ON (im.USER_ID = u.ID)
    JOIN tenant_location tl ON (im.TENANT_LOCATION_ID = tl.ID)
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
            AND month(im.metricDate) = @monthVar 
			AND year(im.metricDate) = @yearVar
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID) AS main2 ON main3.salesAgentId = main2.salesAgentId
    LEFT OUTER JOIN (SELECT 
        	pm.USER_ID AS salesAgentId,
            @roomNightsQuery as roomNights,
            COUNT(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    COUNT(DISTINCT (CASE
                        WHEN p.isMajor = '1' THEN pm.ID
                        ELSE NULL
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS upsellNights,
            SUM(CASE
                WHEN pm.upsellCharge IS NULL THEN 0
                ELSE pm.upsellCharge
            END) AS salesRevenue,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,
            SUM(@reportRevenueData) AS incrementalRevenue,
            SUM(CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    IF(pm.upsellCharge IS NOT NULL
                        AND p.isMajor = 1, pm.upsellCharge, 0)
                ELSE (CASE
                    WHEN
                        p.isRecurring = 0
                    THEN
                        IF(pm.upsellCharge IS NOT NULL
                            AND p.isMajor = 1, pm.upsellCharge, 0)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
                END)
            END) AS upsellRevenue,
            SUM((CASE
                WHEN pm.upsellFlag IS NULL THEN 0
                ELSE pm.upsellFlag
            END)) AS upsellFlag,
            COUNT(DISTINCT (CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber))) AS uniqueConfirmationNumber,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS admissionUnits,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 0
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS ancillaryUnits,
            SUM(IF(p.isMajor = 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS admissionRevenue,
            SUM(IF(p.isMajor = 0, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS ancillaryRevenue,
            COUNT(DISTINCT pm.confirmationNumber) AS transactions,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isExcludedProduct <> 1
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS excludedadmissionUnits,
            SUM(IF(p.isMajor = 1
                AND p.isExcludedProduct <> 1, GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))), IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS excludedAdmissionRevenue,
            SUM((CASE
                WHEN pm.TicketsSold IS NULL THEN 0
                ELSE pm.TicketsSold
            END)) AS ticketsSold,
            SUM((case when pm.checkCount is null then 0 else pm.checkCount end)) as checkCount,
			SUM((case when pm.duration is null then 0 else pm.duration end)) as duration,
			SUM((case when pm.roomNights is null then 0 else pm.roomNights end)) as covers 
    FROM
        product_metric pm
    JOIN user u ON (pm.USER_ID = u.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @prevMonthVar
			AND year(@reportDate) = @yearVar	
			AND (@reportDate) <= CURDATE() 
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID @locationGroupJoin) AS main5 ON main3.salesAgentId = main5.salesAgentId
    LEFT OUTER JOIN (SELECT 
        im.USER_ID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn,
            COUNT(im.id) AS avgCount 
    FROM
        agent_metric im
    JOIN user u ON (im.USER_ID = u.ID)
    JOIN tenant_location tl ON (im.TENANT_LOCATION_ID = tl.ID)
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
            AND month(im.metricDate) = @prevMonthVar 
			AND year(im.metricDate) = @yearVar
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID) AS main6 ON main3.salesAgentId = main6.salesAgentId
    LEFT OUTER JOIN (SELECT 
        pm.TENANT_LOCATION_ID AS tenantLocationID,
           SUM(@reportRevenueData) AS incrementalRevenue	
    FROM
        product_metric pm
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN location_group_product p ON (pm.PRODUCT_ID = p.ID)
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND month(@reportDate) = @monthVar
			AND year(@reportDate) = @yearVar	
    GROUP BY pm.TENANT_LOCATION_ID  @locationGroupJoin) AS main7 ON main3.tenantLocationID = main7.tenantLocationID) AS main) mainFinal ON (rh.ACTIVE_STATUS = 0)
    WHERE
        rh.ACTIVE_STATUS = 0 AND rh.TYPE = 'Agent' AND rh.INDUSTRY_ID = @industryId
    ORDER BY rh.DISPLAY_RANK ASC) AS agentInfo;