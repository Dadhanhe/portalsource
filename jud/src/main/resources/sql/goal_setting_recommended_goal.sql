SELECT 
	 @rowIndex:=@rowIndex + 1 AS id,
	 finalResult2.userID AS userId,
     finalResult2.estimatedDaysWorked AS estimatedDaysWorked,
     finalResult2.employeeName AS employeeName,finalResult2.orgName AS orgName,finalResult2.deemphasizeByTierOne AS deemphasizeByTierOne,
     finalResult2.orgId AS orgId,finalResult2.priorMonthCount AS priorMonthCount,finalResult2.priorMonthCount2 AS priorMonthCount2,
     finalResult2.priorMonthCount3 AS priorMonthCount3,finalResult2.priorYearCount AS priorYearCount,finalResult2.maxArrivals AS maxArrivals,
     finalResult2.priorMonthRevenue AS priorMonthRevenue,finalResult2.priorMonthRevenue2 AS priorMonthRevenue2,finalResult2.priorMonthRevenue3 AS priorMonthRevenue3,
     
     finalResult2.priorYearRevenue AS priorYearRevenue,finalResult2.revenuePerDay1 AS revenuePerDay1,finalResult2.revenuePerDay2 AS revenuePerDay2,
     finalResult2.revenuePerDay3 AS revenuePerDay3,finalResult2.revenuePerDay4 AS revenuePerDay4,finalResult2.threeMonthAverage AS threeMonthAverage,
     
     finalResult2.twoMonthAverage AS twoMonthAverage,finalResult2.estimatedRev AS estimatedRev,finalResult2.estimatedRevenueGoal AS estimatedRevenueGoal,
     finalResult2.total AS total,finalResult2.margin AS margin,finalResult2.deemphasized AS deemphasized,
     finalResult2.count AS count,finalResult2.sumConributionMargin AS sumConributionMargin,finalResult2.sumDeemphasized AS sumDeemphasized,
     
     @tier1 AS tier1,@tier2 AS tier2,@tier3 AS tier3,
     
     @contributionSpread:=ROUND((@sumConributionMargin - @sumDeemphasized),
            4) AS contributionSpread,
    @adjustedMargin:=ROUND(IF(finalResult2.deemphasized < @deemphasizeValueTierOne,
                IF(finalResult2.deemphasized > 0,
                    (finalResult2.deemphasized + (@contributionSpread / @tier1)),
                    0),
                finalResult2.deemphasized),
            4) AS adjustedMargin,
    @adjustedGoal:=(IF(@target > @sum,((@target - @sum) * @adjustedMargin),0)) AS adjustedGoal,
    FLOOR(((@adjustedGoal + finalResult2.estimatedRevenueGoal) + 99) / 100) * 100 AS recommendedGoal 
FROM 
(SELECT @rowIndex:=0) AS rowIndex,
    (SELECT 
        finalResult.*,
            @margin:=ROUND(estimatedRevenueGoal / @sum , 4) AS margin,
            @deemphasized:=IF(@margin > @deemphasizeValueTierOne, IF(@margin > @deemphasizeValueTierThree, @margin * (1 - (@deemphasizeByTierThree)), IF(@margin > @deemphasizeValueTierTwo, @margin * (1 - (@deemphasizeByTierTwo)), @margin * (1 - (@deemphasizeByTierOne)))), @margin) AS deemphasized,
            IF(@deemphasized > 0
                AND @deemphasized < @deemphasizeValueTierOne, @tier1:=@tier1 + 1, IF(@deemphasized > @deemphasizeValueTierOne
                AND @deemphasized < @deemphasizeValueTierTwo, @tier2:=@tier2 + 1, @tier3:=@tier3 + 1)) AS count,
            @sumConributionMargin:=@sumConributionMargin + @margin AS sumConributionMargin,
            @sumDeemphasized:=@sumDeemphasized + @deemphasized AS sumDeemphasized 
    FROM
    	 (SELECT @tier1:=0) AS tier1,(SELECT @tier2:=0) AS tier2,(SELECT @tier3:=0) AS tier3,(SELECT @sumDeemphasized:=0) AS sumDeemphasized,(SELECT @margin:=0) AS margin,(SELECT @deemphasized:=0) AS deemphasized,(SELECT @sumConributionMargin:=0) AS sumConributionMargin, 
    	 (SELECT 
        finalData.userID,
		finalData.employeeName,
            finalData.deemphasizeByTierOne,
            finalData.orgId,
            finalData.orgName,
            finalData.priorMonthCount AS priorMonthCount,
            finalData.priorMonthCount2 AS priorMonthCount2,
            finalData.priorMonthCount3 AS priorMonthCount3,
			finalData.priorYearCount AS priorYearCount,            
            finalData.priorMonthRevenue AS priorMonthRevenue,
            finalData.priorMonthRevenue2 AS priorMonthRevenue2,
            finalData.priorMonthRevenue3 AS priorMonthRevenue3,
            finalData.priorYearRevenue AS priorYearRevenue,
            finalData.maxArrivals AS maxArrivals,
            finalData.revenuePerDay1 AS revenuePerDay1,
            finalData.revenuePerDay2 AS revenuePerDay2,
            finalData.revenuePerDay3 AS revenuePerDay3,
			finalData.revenuePerDay4 AS revenuePerDay4,
            
            finalData.threeMonthAverage AS threeMonthAverage,
            finalData.twoMonthAverage AS twoMonthAverage,
            @estimatedRev:=ROUND(IF((finalData.weightLastMonth + finalData.weightLastTwoMonth + finalData.weightLastThreeMonth + finalData.weightPriorYear) != 0, 
            	((IFNULL(finalData.revenuePerDay1, 0) * finalData.weightLastMonth) + 
            	(IFNULL(finalData.twoMonthAverage, 0) * finalData.weightLastTwoMonth) + 
            	(IFNULL(finalData.threeMonthAverage, 0) * finalData.weightLastThreeMonth) + 
            	(IFNULL(finalData.revenuePerDay4, 0) * finalData.weightPriorYear)) / (finalData.weightLastMonth + finalData.weightLastTwoMonth + finalData.weightLastThreeMonth + finalData.weightPriorYear), 0), 2) AS estimatedRev,
            finalData.maxArrivals AS estimatedDaysWorked,
            IF((@estimatedRev * finalData.maxArrivals < 50
                AND @estimatedRev * finalData.maxArrivals > 0), 50, ROUND(IFNULL(@estimatedRev * finalData.maxArrivals, 0), 2)) AS estimatedRevenueGoal,
            @sum:=@sum + (IF((@estimatedRev * finalData.maxArrivals < 50
                AND @estimatedRev * finalData.maxArrivals > 0), 50, ROUND(IFNULL(@estimatedRev * finalData.maxArrivals, 0), 2))) AS total
    FROM
        (SELECT @sum:=0) AS sum, 
        (SELECT @estimatedRev:=0) AS estimatedRev,
        (SELECT 
        userData.userID,
        arrivalData.employeeName,
            pmData.orgId,
            pmData.orgName,
            pmData.weightLastMonth,
            pmData.weightLastTwoMonth,
            pmData.weightLastThreeMonth,
            pmData.weightPriorYear,
            pmData.deemphasizeByTierOne,
            pmData.deemphasizeByTierTwo,
            pmData.deemphasizeByTierThree,
            arrivalData.priorMonthCount AS priorMonthCount,
            arrivalData.priorMonthCount2 AS priorMonthCount2,
            arrivalData.priorMonthCount3 AS priorMonthCount3,
			arrivalData.priorYearCount AS priorYearCount,
            pmData.priorMonthRevenue AS priorMonthRevenue,
            pmData.priorMonthRevenue2 AS priorMonthRevenue2,
            pmData.priorMonthRevenue3 AS priorMonthRevenue3,
            pmData.priorYearRevenue AS priorYearRevenue,
            GREATEST( IFNULL(arrivalData.priorMonthCount,0), IFNULL(arrivalData.priorMonthCount2,0), IFNULL(arrivalData.priorMonthCount3,0)) AS maxArrivals,
            ROUND(IF(arrivalData.priorMonthCount != 0, (((pmData.priorMonthRevenue) / arrivalData.priorMonthCount)), 0), 2) AS revenuePerDay1,
            ROUND(IF(arrivalData.priorMonthCount2 != 0, (((pmData.priorMonthRevenue2) / arrivalData.priorMonthCount2)), 0), 2) AS revenuePerDay2,
            ROUND(IF(arrivalData.priorMonthCount3 != 0, (((pmData.priorMonthRevenue3) / arrivalData.priorMonthCount3)), 0), 2) AS revenuePerDay3,
			ROUND(IF(arrivalData.priorYearCount != 0, (((pmData.priorYearRevenue) / arrivalData.priorYearCount)), 0), 2) AS revenuePerDay4,
            
            ROUND(((IFNULL(ROUND(IF(arrivalData.priorMonthCount != 0, (((pmData.priorMonthRevenue) / arrivalData.priorMonthCount)), 0), 2), 0) + IFNULL(ROUND(IF(arrivalData.priorMonthCount2 != 0, (((pmData.priorMonthRevenue2) / arrivalData.priorMonthCount2)), 0), 2), 0) + IFNULL(ROUND(IF(arrivalData.priorMonthCount3 != 0, (((pmData.priorMonthRevenue3) / arrivalData.priorMonthCount3)), 0), 2), 0)) / 3), 4) AS threeMonthAverage,
            ROUND((ROUND(IF(arrivalData.priorMonthCount != 0, (((pmData.priorMonthRevenue) / arrivalData.priorMonthCount)), 0), 2) + ROUND(IF(arrivalData.priorMonthCount2 != 0, (((pmData.priorMonthRevenue2) / arrivalData.priorMonthCount2)), 0), 2)) / 2, 4) AS twoMonthAverage
    FROM
     
		(select uu.ID as userID FROM user uu  
        INNER JOIN user_profile up ON uu.id=up.id 
 INNER JOIN  (select DISTINCT(ulg.USER_ID) 
 from user_location_group ulg  
 INNER JOIN user u ON ulg.USER_ID = u.ID  
 AND u.active_status = 0 INNER JOIN location_group lg ON ulg.LOCATION_GROUP_ID = lg.ID  
 INNER JOIN tenant_location tl ON tl.ID = lg.TENANT_LOCATION_ID  AND ulg.IS_CONTRIBUTOR = true @userDepartment_departmentStr  AND tl.TENANT_ID = 1)as u_ids ON uu.ID = u_ids.USER_ID
 UNION
 select distinct user_id from product_metric pmUser where  pmUser.auditstatus NOT IN (2 , 4, 5) @pmUser_departmentStr 
 AND (pmUser.departureDate BETWEEN DATE_SUB(@date, INTERVAL 3 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 3 MONTH))
            OR pmUser.departureDate BETWEEN DATE_SUB(@date, INTERVAL 12 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 12 MONTH)))) AS userData
			
    LEFT OUTER JOIN (SELECT       
            pm.USER_ID as userID,
            concat(up.FIRST_NAME ,' ', up.LAST_NAME )as employeeName,
            COUNT(DISTINCT IF(arrivalDate BETWEEN DATE_SUB(@date, INTERVAL 1 MONTH) AND DATE_SUB(@date, INTERVAL 1 DAY), arrivalDate, NULL)) AS priorMonthCount,
            COUNT(DISTINCT IF(arrivalDate BETWEEN DATE_SUB(@date, INTERVAL 2 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 2 MONTH)), arrivalDate, NULL)) AS priorMonthCount2,
            COUNT(DISTINCT IF(arrivalDate BETWEEN DATE_SUB(@date, INTERVAL 3 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 3 MONTH)), arrivalDate, NULL)) AS priorMonthCount3,  
			COUNT(DISTINCT IF(arrivalDate BETWEEN DATE_SUB(@date, INTERVAL 12 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 12 MONTH)), arrivalDate, NULL)) AS priorYearCount
    FROM
        product_metric pm
    JOIN user u ON u.id = pm.USER_ID
    JOIN user_profile up ON u.id=up.id
    WHERE
        pm.TENANT_LOCATION_ID = @tenantLocationId
            @pm_departmentStr
            AND pm.auditstatus NOT IN (2 , 4, 5)
    GROUP BY pm.USER_ID) AS arrivalData ON arrivalData.userID = userData.userID
    LEFT OUTER JOIN (SELECT 
        revData.userID,
            revData.orgId,
            revData.orgName,
            revData.weightLastMonth,
            revData.weightLastTwoMonth,
            revData.weightLastThreeMonth,
            revData.weightPriorYear,
            revData.deemphasizeByTierOne,
            revData.deemphasizeByTierTwo,
            revData.deemphasizeByTierThree,
            SUM(IF(revData.selectedDate BETWEEN DATE_SUB(@date, INTERVAL 1 MONTH) AND DATE_SUB(@date, INTERVAL 1 DAY), revData.revenue, 0)) AS priorMonthRevenue,
            SUM(IF(revData.selectedDate BETWEEN DATE_SUB(@date, INTERVAL 2 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 2 MONTH)), revData.revenue, 0)) AS priorMonthRevenue2,
            SUM(IF(revData.selectedDate BETWEEN DATE_SUB(@date, INTERVAL 3 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 3 MONTH)), revData.revenue, 0)) AS priorMonthRevenue3,
            SUM(IF(revData.selectedDate BETWEEN DATE_SUB(@date, INTERVAL 12 MONTH) AND LAST_DAY(DATE_SUB(@date, INTERVAL 12 MONTH)), revData.revenue, 0)) AS priorYearRevenue
    FROM
        (SELECT 
        USER_ID as userID,
            @metricDateStr AS selectedDate,
            org.id AS orgId,
            org.name AS orgName,
            tlp.weightLastMonth AS weightLastMonth,
            tlp.weightLastTwoMonth AS weightLastTwoMonth,
            tlp.weightLastThreeMonth AS weightLastThreeMonth,
            tlp.weightPriorYear AS weightPriorYear,
            tlp.deemphasizeByTierOne AS deemphasizeByTierOne,
            tlp.deemphasizeByTierTwo AS deemphasizeByTierTwo,
            tlp.deemphasizeByTierThree AS deemphasizeByTierThree,
            (CASE
                WHEN org.hotelMetricsDataType = '1' THEN IF(revenueData.upsellCharge IS NULL, 0, revenueData.upsellCharge)
                ELSE CASE
                    WHEN p.isRecurring = 0 THEN IF(revenueData.upsellCharge IS NULL, 0, revenueData.upsellCharge)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(revenueData.roomNights IS NULL
                            OR revenueData.roomNights = 0
                            OR revenueData.upsellCharge IS NULL
                            OR revenueData.upsellCharge = 0, 0, (revenueData.roomNights * revenueData.upsellCharge))
                END
            END) AS revenue
    FROM
        product_metric revenueData
    JOIN tenant_location org ON org.id = revenueData.TENANT_LOCATION_ID
    JOIN tenant_location_profile tlp ON tlp.id = org.id 
    JOIN location_group_product p ON p.id = revenueData.PRODUCT_ID
    WHERE
        revenueData.TENANT_LOCATION_ID = @tenantLocationId 
             @revenueData_departmentStr 
             @product_isMajor 
            AND revenueData.auditstatus NOT IN (2 , 4, 5)) AS revData
    GROUP BY revData.userID) AS pmData ON pmData.userID = userData.userID) AS finalData) AS finalResult) AS finalResult2;