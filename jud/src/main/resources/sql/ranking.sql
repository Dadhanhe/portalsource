SELECT 
	@row:=@row + 1 as id,
    finalData.USER_ID as userId,
    finalData.userName,
    finalData.TENANT_LOCATION_ID as tenantLocationId,
    finalData.tenantLocationName,
    @region1
    @locationGroup4
    finalData.month,
    finalData.year,
    finalData.totalRevenue AS value,
    finalData.roomConversion AS roomConversion,
    finalData.ADR AS ADR,
    finalData.IDR AS IDR,
    finalData.UpgradeAdm AS UpgradeAdm,
    finalData.rank AS rank 
FROM (SELECT @row:=0) AS row,
    (SELECT 
            finalData2.USER_ID,
            finalData2.userName,
            finalData2.TENANT_LOCATION_ID,
            finalData2.tenantLocationName,
            @region2
            @locationGroup3
            finalData2.month AS month,
            finalData2.year AS year,
            finalData2.totalRevenue AS totalRevenue,
            finalData2.admissionUnits AS admissionUnits,
            finalData2.excludedAdmissionUnits AS excludedAdmissionUnits,
            IF(finalData2.CallOrders IS NOT NULL
                AND finalData2.checkedInNights IS NOT NULL
                AND finalData2.checkedInNights != 0, (finalData2.CallOrders / finalData2.checkedInNights) * 100, 0) AS roomConversion,
            IF(finalData2.totalRevenue IS NOT NULL
                AND finalData2.TicketsSold IS NOT NULL
                AND finalData2.TicketsSold != 0, (finalData2.totalRevenue / finalData2.TicketsSold), 0) AS ADR,
            IF(finalData2.totalRevenue IS NOT NULL
                AND finalData2.checkedInNights IS NOT NULL
                AND finalData2.checkedInNights != 0, (finalData2.totalRevenue / finalData2.checkedInNights), 0) AS IDR,
            IF(finalData2.excludedAdmissionUnits IS NOT NULL
                AND finalData2.admissionUnits IS NOT NULL
                AND finalData2.admissionUnits != 0, (finalData2.excludedAdmissionUnits / finalData2.admissionUnits) * 100, 0) AS UpgradeAdm,
            @rankDaily:=IF(@monthVarDaily = finalData2.month
                AND @yearVarDaily = finalData2.year
                @region3, @rankDaily + 1, 1) AS rank,
            @monthVarDaily:=finalData2.month AS monthHelper,
            @yearVarDaily:=finalData2.year AS yearHelper
			@region4
    FROM
        (SELECT 
            pm.USER_ID,
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS userName,
            MONTH(@reportDate) AS month,  
            YEAR(@reportDate) AS year,
            pm.TENANT_LOCATION_ID,
            tl.NAME AS tenantLocationName,
            @region5
            @locationGroup2
            SUM(@reportRevenueData) AS totalRevenue,
            SUM(CASE
                WHEN pm.CallOrders IS NULL THEN 0
                ELSE pm.CallOrders
            END) AS CallOrders,
            SUM(CASE
                WHEN pm.TicketsSold IS NULL THEN 0
                ELSE pm.TicketsSold
            END) AS TicketsSold,
            SUM(IF(p.isMajor = '1', (CASE
                WHEN pm.roomNights IS NULL THEN 0
                ELSE pm.roomNights
            END), 0)) AS admissionUnits,
            SUM(IF(p.isMajor = '1'
                AND p.isExcludedProduct <> '1', (CASE
                WHEN pm.roomNights IS NULL THEN 0
                ELSE pm.roomNights
            END), 0)) AS excludedAdmissionUnits,
            SUM(CASE
                WHEN imData.checkedInNights IS NULL THEN 0
                ELSE imData.checkedInNights
            END) AS checkedInNights
    FROM
        product_metric pm
    JOIN user u ON (u.ID = pm.USER_ID)
    JOIN user_profile up ON (up.ID = u.ID)
    JOIN tenant_location tl ON (pm.TENANT_LOCATION_ID = tl.ID)
    JOIN product p ON pm.PRODUCT_ID = p.ID
    @regionJoin
    LEFT OUTER JOIN (SELECT 
        im.USER_ID,
            im.TENANT_LOCATION_ID,
            MONTH(im.metricDate) AS month,
            YEAR(im.metricDate) AS year,
            SUM((CASE
                WHEN im.checkedInNights IS NULL THEN 0
                ELSE im.checkedInNights
            END)) AS checkedInNights
    FROM
        agent_metric im
    WHERE
        im.USER_ID NOT IN (0)
            AND im.auditStatus NOT IN (2 , 4, 5)
            @location1
            AND MONTH(im.metricDate) = 5
            AND YEAR(im.metricDate) = 2016
    GROUP BY im.USER_ID , im.TENANT_LOCATION_ID , MONTH(im.metricDate) , YEAR(im.metricDate)) AS imData ON (imData.USER_ID = pm.USER_ID
        AND imData.TENANT_LOCATION_ID = pm.TENANT_LOCATION_ID)
    WHERE
        pm.ID IS NOT NULL
            AND pm.USER_ID NOT IN (0)
            @location2
            @locationGroup1
            @region6
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND MONTH(@reportDate) = @monthVal  
            AND YEAR(@reportDate) = @yearVal 
    GROUP BY pm.USER_ID , pm.TENANT_LOCATION_ID  @locationGroupJoin, MONTH(@reportDate), YEAR(@reportDate)) AS finalData2, 
    (SELECT @rankDaily:=0) AS rankDailyCnt, (SELECT @dayVarDaily:=0) AS dayVarDailyCnt, (SELECT @monthVarDaily:=0) AS monthVarDailyCnt, (SELECT @yearVarDaily:=0) AS yearVarDailyCnt
        @region7
    ORDER BY month , year , 
	    @region8
	    totalRevenue DESC) AS finalData 
@userQuery 
LIMIT @startLimit,@endLimit;