 SELECT 
    count(errProductID) as errorCount,
    tl.name as name,
    tl.id as tenantLocationId,
    tl.TENANT_ID as tenantId
 FROM
    product_metric pm
        JOIN
    tenant_location tl on tl.id = pm.TENANT_LOCATION_ID
  WHERE
    	pm.TENANT_LOCATION_ID IN (@locationIds)
        AND pm.auditstatus = 4
        AND FIND_IN_SET('PRODUCT_NOT_FOUND', pm.errorCodes) > 0
        AND FIND_IN_SET('USER_NOT_CREATED', pm.errorCodes) <= 0
        AND tl.INDUSTRY_ID = @industryId
        AND tl.ACTIVE_STATUS = 0
        AND ((pm.departureDate BETWEEN '@startDate' AND '@endDate')
        OR (pm.CREATED_ON BETWEEN '@startDate' AND '@endDate'))
 GROUP BY TENANT_LOCATION_ID
 ORDER BY COUNT(errProductID) DESC limit 5;