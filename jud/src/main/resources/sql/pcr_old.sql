 SELECT 
    main.salesAgentId,
    main.salesAgent,
    main.isContributor,
    main.upsells,
    main.hrccUpsells,
    main.upsellNights,
    main.salesRevenue,
    main.orders,
    main.incrementalRevenue,
    main.upsellRevenue,
    main.otherProductsRevenue,
    main.nightsCheckedIn,
    main.entertainmentUpsellConversion,
    main.upsellConversion,
    main.averageUpsellRate,
    main.idr,
    main.avgRevenuePerOrder,
    main.country,
    (CASE
        WHEN main.idr = 0 AND main.previousIdr != 0 THEN - 100
        WHEN main.previousIdr = 0 AND main.idr != 0 THEN 100
        WHEN
            main.idr IS NOT NULL
                AND main.previousIdr IS NOT NULL
                AND main.previousIdr != 0
        THEN
            ((main.idr - main.previousIdr) / main.previousIdr) * 100
        ELSE 0
    END) AS crTrend,
    (CASE
        WHEN
            main.incrementalRevenue = 0
                AND main.previousIncrementalRevenue != 0
        THEN
            - 100
        WHEN
            main.previousIncrementalRevenue = 0
                AND main.incrementalRevenue != 0
        THEN
            100
        WHEN
            main.incrementalRevenue IS NOT NULL
                AND main.previousIncrementalRevenue IS NOT NULL
                AND main.previousIncrementalRevenue != 0
        THEN
            ((main.incrementalRevenue - main.previousIncrementalRevenue) / main.previousIncrementalRevenue) * 100
        ELSE 0
    END) AS trend,
    main.admissionUnit,
    main.ancillaryUnit,
    main.admissionRevenue,
    main.ancillaryRevenue,
    main.transactions,
    main.upgradeAdmission,
	
    main.ticketsSold,
    main.upsellFlags,
	main.admissionYield  
FROM
    (SELECT 
        main1.salesAgentId,
            main1.salesAgent,
            main1.isContributor,
            main1.isOperator,
            main1.upsells,
            main1.hrccUpsells,
            main1.upsellNights,
            main1.salesRevenue,
            main1.orders,
            main1.incrementalRevenue,
            main1.upsellRevenue,
            IF(main1.upsellRevenue IS NULL, main1.incrementalRevenue, main1.incrementalrevenue - main1.upsellRevenue) AS otherProductsRevenue,
            IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn) AS nightsCheckedIn,
            IF(main2.arrivals IS NULL, 0, main2.arrivals) AS arrivals,
            IF(main1.orders IS NOT NULL
                AND main2.nightscheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.orders / main2.nightsCheckedIn) * 100, 0) AS entertainmentUpsellConversion,
            IF(main1.upsells IS NOT NULL
                AND main2.arrivals IS NOT NULL
                AND main2.arrivals != 0, (main1.upsells / main2.arrivals) * 100, 0) AS upsellConversion,
            IF(main1.upsellRevenue IS NOT NULL
                AND main1.upsellNights IS NOT NULL
                AND main1.upsellNights != 0, (main1.upsellRevenue / main1.upsellNights), 0) AS averageUpsellRate,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main2.nightsCheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.incrementalRevenue / main2.nightsCheckedIn), 0) AS idr,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main1.orders IS NOT NULL
                AND main1.orders != 0, (main1.incrementalRevenue / main1.orders), 0) AS avgRevenuePerOrder,
            main1.country AS country,
            IF(main3.incrementalRevenue IS NULL, 0, main3.incrementalRevenue) AS previousIncrementalRevenue,
            IF(main4.nightsCheckedIn IS NULL, 0, main4.nightsCheckedIn) AS previousNightsCheckedIn,
            IF(main3.incrementalRevenue IS NOT NULL
                AND main4.nightsCheckedIn IS NOT NULL
                AND main4.nightsCheckedIn != 0, (main3.incrementalRevenue / main4.nightsCheckedIn), 0) AS previousIdr,
            IF(main1.admissionUnit IS NULL, 0, main1.admissionUnit) AS admissionUnit,
            IF(main1.ancillaryUnit IS NULL, 0, main1.ancillaryUnit) AS ancillaryUnit,
            IF(main1.admissionRevenue IS NULL, 0, main1.admissionRevenue) AS admissionRevenue,
            IF(main1.ancillaryRevenue IS NULL, 0, main1.ancillaryRevenue) AS ancillaryRevenue,
            IF(main1.transactions IS NULL, 0, main1.transactions) AS transactions,
            IF(main1.excludedAdmissionUnits IS NULL, 0, main1.excludedAdmissionUnits) AS excludedAdmissionUnits,
			IF(main1.excludedAdmissionRevenue IS NULL, 0, main1.excludedAdmissionRevenue) AS excludedAdmissionRevenue,
			IF(main1.admissionUnit > 0,(main1.excludedadmissionUnits/main1.admissionUnit)*100,0) as upgradeAdmission,
			IF(main1.admissionRevenue > 0, (main1.excludedAdmissionRevenue / main1.admissionUnit), 0) AS admissionYield,
            IF(main1.TicketsSold IS NULL, 0, main1.TicketsSold) AS ticketsSold,
            IF(main1.upsellFlags IS NULL, 0, main1.upsellFlags) AS upsellFlags
    FROM
        (SELECT 
        pm.userID AS salesAgentId,
            CONCAT(u.firstName, ' ', u.lastName) AS salesAgent,
            u.isContributor AS isContributor,
            u.isOperator AS isOperator,
            count(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.productID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            COUNT(DISTINCT (CONCAT(pm.productID, '$', pm.confirmationNumber))) AS hrccUpsells,
             CASE
                WHEN
                    o.hotelMetricsDataType = '1'
                THEN
                   COUNT(DISTINCT (CASE                        
                        WHEN p.isMajor = '1' THEN pm.id
						ELSE null
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS upsellNights,
            SUM(CASE
                WHEN pm.upsellCharge IS NULL THEN 0
                ELSE pm.upsellCharge
            END) AS salesRevenue,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,			
			@reportRevenueData AS incrementalRevenue,
            SUM(CASE
                WHEN o.hotelMetricsDataType = '1' THEN IF(pm.upsellCharge IS NOT NULL and p.isMajor = 1, pm.upsellCharge,0)
                ELSE 		
					(CASE
						WHEN
							p.isRecurring = 0
						THEN
							IF(pm.upsellCharge IS NOT NULL
								AND p.isMajor = 1, pm.upsellCharge, 0)
						WHEN
							p.isRecurring = 1
						THEN
							IF(pm.roomNights IS NULL
								OR pm.roomNights = 0
								OR pm.upsellCharge IS NULL
								OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
					END)
			END) AS upsellRevenue,
            c.name AS country,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS admissionUnit,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isMajor = 0
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS ancillaryUnit,
            SUM(IF(p.isMajor = 1, GREATEST(IFNULL(pm.upsellCharge,
						IF(pm.ezPayPrice = NULL,
							0,
							(IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))),
				IFNULL(pm.ezPayPrice,
						IF(pm.upsellCharge = NULL,
							0,
							(IF(pm.upsellCharge < 0,
								pm.upsellCharge - 1,
								0))))) , 0)) as admissionRevenue,
			SUM(IF(p.isMajor = 0, GREATEST(IFNULL(pm.upsellCharge,
						IF(pm.ezPayPrice = NULL,
							0,
							(IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))),
				IFNULL(pm.ezPayPrice,
						IF(pm.upsellCharge = NULL,
							0,
							(IF(pm.upsellCharge < 0,
								pm.upsellCharge - 1,
								0))))) , 0)) AS ancillaryRevenue,
            COUNT(DISTINCT pm.confirmationNumber) AS transactions,
            SUM((CASE
                WHEN
                    pm.roomNights IS NOT NULL
                        AND p.isExcludedProduct<>1
                        AND p.isMajor = 1
                THEN
                    pm.roomNights
                ELSE 0
            END)) AS excludedAdmissionUnits,
			SUM(IF(p.isMajor = 1 and p.isExcludedProduct<>1,
				GREATEST(IFNULL(pm.upsellCharge,
                        IF(pm.ezPayPrice = NULL,
                            0,
                            (IF(pm.ezPayPrice < 0,
                                pm.ezPayPrice - 1,
                                0)))),
                IFNULL(pm.ezPayPrice,
                        IF(pm.upsellCharge = NULL,
                            0,
                            (IF(pm.upsellCharge < 0,
                                pm.upsellCharge - 1,
                                0))))),
			0)) AS excludedAdmissionRevenue,
            SUM(IF(pm.TicketsSold IS NULL, 0, pm.TicketsSold)) AS ticketsSold,
            SUM(IF(pm.upsellFlag IS NULL, 0, pm.upsellFlag)) AS upsellFlags
    FROM
        product_metrics pm, organizations o, products p ,users u, country c     
    WHERE
        pm.organizationID = @orgId
		@departmentStr
			AND pm.userId = u.id
			AND pm.organizationId = o.ID
			AND o.countryID = c.id
			AND pm.productID = p.ID
            AND pm.auditstatus <> 2
            AND pm.auditstatus <> 4
            AND pm.auditstatus <> 5
            AND @reportDate BETWEEN '@startDate' AND '@endDate'
    GROUP BY pm.userId , pm.organizationId @departmentJoin) AS main1
    LEFT OUTER JOIN (SELECT 
        im.userID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn
    FROM
        individual_metrics im
    JOIN users u ON (im.userId = u.id)
    JOIN organizations o ON (im.organizationId = o.ID)
    WHERE
        im.organizationID = @orgId
            AND im.auditstatus <> 2
            AND im.auditstatus <> 4
            AND im.auditstatus <> 5
            AND im.metricDate BETWEEN '@startDate' AND '@endDate'
    GROUP BY im.userId , im.organizationId) AS main2 ON (main1.salesAgentId = main2.salesAgentId)
    LEFT OUTER JOIN (SELECT 
        pm.userID AS salesAgentId,
           @reportRevenueData AS incrementalRevenue
    FROM
        product_metrics pm
    JOIN organizations o ON (pm.organizationId = o.ID)
    JOIN products p ON (pm.productID = p.ID)
    WHERE
        pm.organizationID = @orgId
		@departmentStr
            AND pm.auditstatus <> 2
            AND pm.auditstatus <> 4
            AND pm.auditstatus <> 5
            AND @reportDate BETWEEN '@prevStartDate' AND '@prevEndDate'
    GROUP BY pm.userId , pm.organizationId  @departmentJoin) AS main3 ON (main1.salesAgentId = main3.salesAgentId)
    LEFT OUTER JOIN (SELECT 
        im.userID AS salesAgentId,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn
    FROM
        individual_metrics im, users u, organizations o 
    WHERE
        im.organizationID = @orgId
			AND im.userId = u.id
			AND im.organizationId = o.ID
            AND im.auditstatus <> 2
            AND im.auditstatus <> 4
            AND im.auditstatus <> 5
            AND im.metricDate BETWEEN '@prevStartDate' AND '@prevEndDate'
    GROUP BY im.userId , im.organizationId) AS main4 ON (main1.salesAgentId = main4.salesAgentId)) AS main