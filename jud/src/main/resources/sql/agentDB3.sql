SELECT  
    * 
FROM
    (SELECT 
        @row:=@row + 1 AS id,
            mainFinal.userId AS userId,
            mainFinal.userName AS userName,
            mainFinal.tenantLocationId,
            mainFinal.locationGroupId,
            mainFinal.date AS date,
            rh.name AS reportName,
            CASE
                WHEN rh.name = 'totalUpsellAmount' THEN totalUpsellAmount
                WHEN rh.name = 'majorUpsellCharge' THEN majorUpsellCharge
                WHEN rh.name = 'noOfUpsellRoomNights' THEN noOfUpsellRoomNights
                WHEN rh.name = 'upsellRoomAverageRate' THEN upsellRoomAverageRate
                WHEN rh.name = 'idr' THEN idr
                WHEN rh.name = 'uoConversion' THEN uoConversion
                WHEN rh.name = 'conversion' THEN upsellConversion
                WHEN rh.name = 'checkedInNights' THEN checkedInNights
                WHEN rh.name = 'averageRevenuePerOrder' THEN averageRevenuePerOrder
                WHEN rh.name = 'totalIncentiveAmount' THEN totalIncentiveAmount
                WHEN rh.name = 'admissionUnits' THEN admissionUnits
                WHEN rh.name = 'ancillaryUnits' THEN ancillaryUnits
                WHEN rh.name = 'excludedAdmissionUnits' THEN excludedAdmissionUnits        
                WHEN rh.name = 'excludedAdmissionRevenue' THEN excludedAdmissionRevenue   
				WHEN rh.name = 'admissionYield' THEN admissionYield
				WHEN rh.name = 'admissionRevenue' THEN admissionRevenue
                WHEN rh.name = 'rpau' THEN rpau
                WHEN rh.name = 'upgradeAdmission' THEN upgradeAdmission 
                WHEN rh.name = 'ancillaryRevenue' THEN ancillaryRevenue 
            END AS value,
            CASE
                WHEN rh.name = 'upsellRoomAverageRate' THEN 0
                WHEN rh.name = 'noOfUpsellRoomNights' THEN 0
                ELSE mainFinal.commissionableRevenue
            END AS commissionableRevenue,
            mainFinal.unit AS unit 
    FROM
        (SELECT @row:=0) AS row, report_helper rh
    LEFT OUTER JOIN (SELECT 
        main.userId,
            main.userName,
            main.tenantLocationId,
            main.locationGroupId,
            main.date,
            main.unit,
            main.incrementalRevenue AS totalUpsellAmount,
            main.commissionableRevenue,
            main.majorUpsellCharge,
            main.noOfUpsellRoomNights,
            main.upsellRoomAverageRate,
            main.idr,
            main.upsellConversion,
            main.totalIncentiveAmount,
            main.checkedInNights,
            main.averageRevenuePerOrder,
            main.uoConversion,
            main.admissionUnits,
		    main.ancillaryUnits,
		    main.excludedadmissionUnits,
		    main.excludedAdmissionRevenue,
		    main.admissionYield,
		    main.admissionRevenue,
		    main.rpau,
		    main.upgradeAdmission,
		    main.ancillaryRevenue 
    FROM
        (SELECT 
        main1.userId,
            main1.userName,
            main1.tenantLocationId,
            main1.locationGroupId,
            main1.date,
            main1.unit,
            IF(main1.incrementalRevenue IS NULL, 0, main1.incrementalRevenue) AS incrementalRevenue,
            IF(main1.commissionableRevenue IS NULL, 0, main1.commissionableRevenue) AS commissionableRevenue,
            main1.majorUpsellCharge,
            main1.noOfUpsellRoomNights,
            IF(main1.majorUpsellCharge IS NOT NULL
                AND main1.noOfUpsellRoomNights IS NOT NULL
                AND main1.noOfUpsellRoomNights != 0, (main1.majorUpsellCharge / main1.noOfUpsellRoomNights), 0) AS upsellRoomAverageRate,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main2.nightsCheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.incrementalRevenue / main2.nightsCheckedIn), 0) AS idr,
            IF(main1.upsells IS NOT NULL
                AND main2.arrivals IS NOT NULL
                AND main2.arrivals != 0, (main1.upsells / main2.arrivals) * 100, 0) AS upsellConversion,
            IF(main1.incrementalRevenue IS NOT NULL
                AND main1.orders IS NOT NULL
                AND main1.orders != 0, main1.incrementalRevenue / main1.orders, 0) AS averageRevenuePerOrder,
            IF(main1.orders IS NOT NULL
                AND main2.nightscheckedIn IS NOT NULL
                AND main2.nightsCheckedIn != 0, (main1.orders / main2.nightsCheckedIn) * 100, 0) AS uoConversion,
            IF(main2.nightsCheckedIn IS NULL, 0, main2.nightsCheckedIn) AS checkedInNights,
            main1.totalIncentiveAmount,
            IF(main1.admissionUnits IS NULL, 0, main1.admissionUnits) AS admissionUnits,
            IF(main1.ancillaryUnits IS NULL, 0, main1.ancillaryUnits) AS ancillaryUnits,
            main1.excludedAdmissionUnits,
            IF(main1.excludedAdmissionRevenue IS NULL, 0, main1.excludedAdmissionRevenue) AS excludedAdmissionRevenue,
            IF(main1.admissionUnits > 0,(main1.excludedadmissionRevenue/main1.admissionUnits),0) as admissionYield,
            IF(main1.admissionRevenue IS NULL, 0, main1.admissionRevenue) AS admissionRevenue,
            IF(main1.admissionUnits is not null , (main1.admissionRevenue/main1.admissionUnits) , 0 ) as rpau,
            IF(main1.admissionUnits > 0,(main1.excludedadmissionUnits/main1.admissionUnits)*100,0) as upgradeAdmission,
            IF(main1.ancillaryRevenue IS NULL, 0, main1.ancillaryRevenue) AS ancillaryRevenue  
    FROM
        (SELECT 
        pm.USER_ID AS userId,
            pm.TENANT_LOCATION_ID as tenantLocationId,
            pm.LOCATION_GROUP_ID as locationGroupId,
           	(@reportDate) as date,
            r.CURRENCY AS unit,
            CONCAT(up.FIRST_NAME, ' ', up.LAST_NAME) AS userName,
            SUM(@reportRevenueData) AS incrementalRevenue,
            SUM(IF(pm.incentiveDiscount is null, 0, pm.incentiveDiscount)) as incentiveDiscount,			
			SUM(IF(pm.incentiveDiscount > 0 AND @reportRevenueData > 0, (@reportRevenueData - ((@reportRevenueData * pm.incentiveDiscount) / 100)) , @reportRevenueData)) as commissionableRevenue,			
            SUM(CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    IF(pm.upsellCharge IS NOT NULL
                        AND p.isMajor = 1, pm.upsellCharge, 0)
                ELSE (CASE
                    WHEN
                        p.isRecurring = 0
                    THEN
                        IF(pm.upsellCharge IS NOT NULL
                            AND p.isMajor = 1, pm.upsellCharge, 0)
                    WHEN
                        p.isRecurring = 1
                    THEN
                        IF(pm.roomNights IS NULL
                            OR pm.roomNights = 0
                            OR pm.upsellCharge IS NULL
                            OR pm.upsellCharge = 0, 0, IF(p.isMajor = 1, (pm.roomNights * pm.upsellCharge), 0))
                END)
            END) AS majorUpsellCharge,
            CASE
                WHEN
                    tl.hotelMetricsDataType = '1'
                THEN
                    COUNT(DISTINCT (CASE
                        WHEN p.isMajor = '1' THEN pm.id
                        ELSE NULL
                    END))
                ELSE SUM(CASE
                    WHEN pm.roomNights IS NULL THEN 0
                    WHEN p.isMajor = '1' THEN pm.roomNights
                    ELSE 0
                END)
            END AS noOfUpsellRoomNights,
            COUNT(DISTINCT (CASE
                WHEN p.isMajor = 1 THEN CONCAT(pm.PRODUCT_ID, '$', pm.confirmationNumber)
                ELSE NULL
            END)) AS upsells,
            @roomNightsQuery as roomNights,
            SUM(IF(pm.callOrders IS NULL, 0, pm.callOrders)) AS orders,
            SUM(IF(pm.commissionableRevenue IS NULL, 0, pm.commissionableRevenue)) AS totalIncentiveAmount,
            SUM((case when pm.roomNights is not null and p.isMajor = 1 then pm.roomNights
				else 0 end)) as admissionUnits,
			SUM((case when pm.roomNights is not null and p.isMajor = 0 then pm.roomNights
				else 0 end)) as ancillaryUnits,
			SUM((case when pm.roomNights is not null and p.isExcludedProduct<>1 and p.isMajor = 1 then pm.roomNights
				else 0 end)) as excludedadmissionUnits,
            SUM(IF(p.isMajor = 1 and p.isExcludedProduct<>1,
				GREATEST(IFNULL(pm.upsellCharge, IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))),
                IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))), 0)) AS excludedAdmissionRevenue,
			 SUM(IF(p.isMajor = 1, GREATEST(IFNULL(pm.upsellCharge,
						IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))),
				IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))) , 0)) as admissionRevenue,
			SUM(IF(p.isMajor = 0, GREATEST(IFNULL(pm.upsellCharge,
						IF(pm.ezPayPrice = NULL, 0, (IF(pm.ezPayPrice < 0, pm.ezPayPrice - 1, 0)))),
				IFNULL(pm.ezPayPrice, IF(pm.upsellCharge = NULL, 0, (IF(pm.upsellCharge < 0, pm.upsellCharge - 1, 0))))) , 0)) AS ancillaryRevenue 
    FROM
        @productMetricTableName pm
    JOIN user u ON pm.USER_ID = u.ID
    JOIN user_profile up ON u.ID = up.ID
    JOIN tenant_location tl ON pm.TENANT_LOCATION_ID = tl.ID
    JOIN region r ON tl.REGION_ID = r.ID
    JOIN location_group_product p ON pm.PRODUCT_ID = p.ID
    WHERE
        pm.TENANT_LOCATION_ID = @orgId
            AND pm.USER_ID = @userId
            @locationGroupStr
            AND pm.auditStatus NOT IN (2 , 4, 5)
            AND year(@reportDate) = @yearVar 
            @monthQry1 
            AND (@reportDate) <= CURDATE() 
    GROUP BY pm.USER_ID , date , pm.TENANT_LOCATION_ID @locationGroupJoin) AS main1
    LEFT OUTER JOIN (SELECT 
        im.USER_ID AS userId,
            im.metricDate AS date,
            SUM(IF(im.arrivals IS NULL, 0, im.arrivals)) AS arrivals,
            SUM(IF(im.checkedInNights IS NULL, 0, im.checkedInNights)) AS nightsCheckedIn
    FROM
        @agentMetricTableName im
    JOIN user u ON (im.USER_ID = u.ID)
    JOIN tenant_location tl ON (im.TENANT_LOCATION_ID = tl.ID)
    WHERE
        im.TENANT_LOCATION_ID = @orgId
            AND im.USER_ID = @userId
            AND im.auditstatus NOT IN (2 , 4, 5)
            AND YEAR(im.metricDate) = @yearVar 
            @monthQry2 
    GROUP BY im.USER_ID , date , im.TENANT_LOCATION_ID) AS main2 ON main1.userId = main2.userId
        AND main1.date = main2.date) AS main
        WHERE
        main.userId = @userId) mainFinal ON (rh.ACTIVE_STATUS = 0)
    WHERE
        rh.ACTIVE_STATUS = 0 AND rh.type = 'Chart' @reportNames AND rh.INDUSTRY_ID = @industryID ) AS chartData 
WHERE
    chartData.value IS NOT NULL;