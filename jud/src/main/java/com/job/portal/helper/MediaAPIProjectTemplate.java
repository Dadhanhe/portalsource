/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.helper;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * The Class MediaAPIProjectTemplate.
 */
@Entity
@Cacheable(false)
public class MediaAPIProjectTemplate implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	private String id;

	/** The name. */
	private String name;

	/** The hashed id. */
	private String hashed_id;

	/**
	 * Instantiates a new media API project template.
	 */
	public MediaAPIProjectTemplate() {
		super();
	}

	/**
	 * Instantiates a new media API project template.
	 *
	 * @param id
	 *            the id
	 */
	public MediaAPIProjectTemplate(final String id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the hashed id.
	 *
	 * @return the hashed id
	 */
	public String getHashed_id() {
		return this.hashed_id;
	}

	/**
	 * Sets the hashed id.
	 *
	 * @param hashed_id
	 *            the new hashed id
	 */
	public void setHashed_id(final String hashed_id) {
		this.hashed_id = hashed_id;
	}

}