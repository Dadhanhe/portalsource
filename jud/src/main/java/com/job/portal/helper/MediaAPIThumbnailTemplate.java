/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.helper;

import java.io.Serializable;

import javax.persistence.Cacheable;

// TODO: Auto-generated Javadoc
/**
 * The Class MediaAPIThumbnailTemplate.
 */
@Cacheable(false)
public class MediaAPIThumbnailTemplate implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The url. */
	private String url;

	/** The width. */
	private Integer width;

	/** The height. */
	private Integer height;

	/**
	 * Instantiates a new media API thumbnail template.
	 */
	public MediaAPIThumbnailTemplate() {
		super();
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url
	 *            the new url
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public Integer getWidth() {
		return this.width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width
	 *            the new width
	 */
	public void setWidth(final Integer width) {
		this.width = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public Integer getHeight() {
		return this.height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height
	 *            the new height
	 */
	public void setHeight(final Integer height) {
		this.height = height;
	}
}
