/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import com.job.portal.aspect.EventLog;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.InvalidPasswordException;
import com.job.portal.exception.InvalidUserException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.UserPasswordAttemptDetail;
import com.job.portal.pojo.enums.EventAction;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class UserHelper.
 */
@Component
public class UserHelper {

	/**
	 * Authenticate user LDAP.
	 *
	 * @param user
	 *            the user
	 * @param password
	 *            the password
	 * @param apfvalue
	 *            the apfvalue
	 * @param userPwdAttDtl
	 *            the upad
	 * @param compareDate
	 *            the compare date
	 * @param enableAttamptFeature
	 *            the enable attampt feature
	 * @return the user
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	@EventLog(action = EventAction.Login)
	public User authenticateUserLDAP(final User user, final String password,
			final String apfvalue,
			final UserPasswordAttemptDetail userPwdAttDtl,
			final Date compareDate, final boolean enableAttamptFeature)
			throws InvalidPasswordException, RecordNotFoundException,
			InvalidUserException, InvalidInputException,
			DuplicateRecordException, SystemConfigurationException {
		final Integer maxPasswordAttempt = 0;
		if (enableAttamptFeature && !Objects.isEmpty(userPwdAttDtl)) {
			final Date lastAttempt = userPwdAttDtl.getLastAttempt();
			new Date().getTime();
			lastAttempt.getTime();
		}
		if (user != null) {
			if (user.getActiveStatus().equals(Status.Deactive)
					&& !(user.getActiveStatus().equals(Status.Active)
							|| user.getActiveStatus().equals(Status.Pending))) {
				throw new InvalidUserException("email",
						"Account is deactivated");
			} else if (user.getActiveStatus().equals(Status.Pending)) {
				throw new InvalidUserException("email",
						"Account is pending to approve");
			} else if (user.getActiveStatus().equals(Status.Blocked)) {
				throw new InvalidUserException("email", "Account is blocked");
			} else if (user.getActiveStatus().equals(Status.Deleted)) {
				throw new InvalidUserException("email", "Account is deleted");
			}
			final String pass = DigestUtils.md5Hex(password);
			final String encryptedPassword = DigestUtils
					.md5Hex(pass + user.getSalt());
			if (!user.getPassword().equals(encryptedPassword)) {
				if (enableAttamptFeature && !Objects.isEmpty(userPwdAttDtl)
						&& !userPwdAttDtl.getIsLocked()
						&& userPwdAttDtl != null) {
					final Date lastAttempt = userPwdAttDtl.getLastAttempt();
					final long secs = (new Date().getTime()
							- lastAttempt.getTime()) / 1000;
					final int hours1 = (int) (secs / 3600);
					if (hours1 < 24) {
						userPwdAttDtl.setLoginFailedCount(
								userPwdAttDtl.getLoginFailedCount() + 1);
					} else {
						userPwdAttDtl.setLoginFailedCount(1);
					}
					userPwdAttDtl.setLastAttempt(new Date());
					if (userPwdAttDtl
							.getLoginFailedCount() == maxPasswordAttempt) {
						userPwdAttDtl.setLastAttempt(new Date());
						userPwdAttDtl.setIsLocked(true);
					}

				} else if (enableAttamptFeature
						&& Objects.isEmpty(userPwdAttDtl)
						&& userPwdAttDtl == null) {
					final UserPasswordAttemptDetail newUpad = new UserPasswordAttemptDetail();
					newUpad.setUserId(user.getId());
					newUpad.setIsLocked(false);
					newUpad.setLastAttempt(new Date());
					newUpad.setLoginFailedCount(1);

				}
				final String messssge = "";
				throw new InvalidPasswordException("password", messssge);
			}

			/*
			 *
			 * GET USER'S ROLE. IF(USER'S ROLE ==NULL){ ATHROW NEW
			 * EXCEPTION(TAASDF); }
			 *
			 * ROLE = GET ROLE BY ID USERS.SETROLE(ROLE);
			 */

		} else {
			throw new InvalidUserException("email", "User has no account");
		}
		return user;
	}

	/**
	 * Checks if is converted.
	 *
	 * @param userId
	 *            the user id
	 * @param userToBeConvertedList
	 *            the user to be converted list
	 * @return true, if is converted
	 */
	public boolean isConverted(final Integer userId,
			final List<User> userToBeConvertedList) {
		if (!Objects.isEmptyCollection(userToBeConvertedList)) {
			for (final User user : Objects
					.collectionsIterator(userToBeConvertedList)) {
				if (user.getId().intValue() == userId.intValue()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets the location wise user id list.
	 *
	 * @param userList
	 *            the user list
	 * @return the location wise user id list
	 */
	public List<Integer> getLocationWiseUserIdList(final List<User> userList) {
		final List<Integer> ids = new ArrayList<>();
		if (!Objects.isEmptyCollection(userList)) {
			Objects.stream(userList).forEach(user -> {
				ids.add(user.getId());
			});
		}
		return ids;
	}

}
