/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.helper;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * The Class MediaAPITemplate.
 */
@Entity
@Cacheable(false)
public class MediaAPITemplate implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	private Integer id;

	/** The name. */
	private String name;

	/** The type. */
	private String type;

	/** The section. */
	private String section;

	/** The created. */
	private String created;

	/** The updated. */
	private String updated;

	/** The duration. */
	private Double duration;

	/** The hashed id. */
	private String hashed_id;

	/** The progress. */
	private Integer progress;

	/** The description. */
	private String description;

	/** The status. */
	private String status;

	/** The project. */
	private MediaAPIProjectTemplate project;

	/** The embed code. */
	private String embedCode;

	/** The assets. */
	private List<MediaAPIAssetsTemplate> assets;

	/** The thumbnail. */
	private MediaAPIThumbnailTemplate thumbnail;

	/** The play count. */
	private Integer play_count;

	/**
	 * Instantiates a new media API template.
	 */
	public MediaAPITemplate() {
		super();
	}

	/**
	 * Instantiates a new media API template.
	 *
	 * @param id
	 *            the id
	 */
	public MediaAPITemplate(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public Double getDuration() {
		return this.duration;
	}

	/**
	 * Sets the duration.
	 *
	 * @param duration
	 *            the new duration
	 */
	public void setDuration(final Double duration) {
		this.duration = duration;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *            the new type
	 */
	public void setType(final String type) {
		this.type = type;
	}

	/**
	 * Gets the hashed id.
	 *
	 * @return the hashed id
	 */
	public String getHashed_id() {
		return this.hashed_id;
	}

	/**
	 * Sets the hashed id.
	 *
	 * @param hashed_id
	 *            the new hashed id
	 */
	public void setHashed_id(final String hashed_id) {
		this.hashed_id = hashed_id;
	}

	/**
	 * Gets the progress.
	 *
	 * @return the progress
	 */
	public Integer getProgress() {
		return this.progress;
	}

	/**
	 * Sets the progress.
	 *
	 * @param progress
	 *            the new progress
	 */
	public void setProgress(final Integer progress) {
		this.progress = progress;
	}

	/**
	 * Gets the project.
	 *
	 * @return the project
	 */
	public MediaAPIProjectTemplate getProject() {
		return this.project;
	}

	/**
	 * Sets the project.
	 *
	 * @param project
	 *            the new project
	 */
	public void setProject(final MediaAPIProjectTemplate project) {
		this.project = project;
	}

	/**
	 * Gets the embed code.
	 *
	 * @return the embed code
	 */
	public String getEmbedCode() {
		return this.embedCode;
	}

	/**
	 * Sets the embed code.
	 *
	 * @param embedCode
	 *            the new embed code
	 */
	public void setEmbedCode(final String embedCode) {
		this.embedCode = embedCode;
	}

	/**
	 * Gets the assets.
	 *
	 * @return the assets
	 */
	public List<MediaAPIAssetsTemplate> getAssets() {
		return this.assets;
	}

	/**
	 * Sets the assets.
	 *
	 * @param assets
	 *            the new assets
	 */
	public void setAssets(final List<MediaAPIAssetsTemplate> assets) {
		this.assets = assets;
	}

	/**
	 * Gets the thumbnail.
	 *
	 * @return the thumbnail
	 */
	public MediaAPIThumbnailTemplate getThumbnail() {
		return this.thumbnail;
	}

	/**
	 * Sets the thumbnail.
	 *
	 * @param thumbnail
	 *            the new thumbnail
	 */
	public void setThumbnail(final MediaAPIThumbnailTemplate thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * Gets the created.
	 *
	 * @return the created
	 */
	public String getCreated() {
		return this.created;
	}

	/**
	 * Sets the created.
	 *
	 * @param created
	 *            the new created
	 */
	public void setCreated(final String created) {
		this.created = created;
	}

	/**
	 * Gets the updated.
	 *
	 * @return the updated
	 */
	public String getUpdated() {
		return this.updated;
	}

	/**
	 * Sets the updated.
	 *
	 * @param updated
	 *            the new updated
	 */
	public void setUpdated(final String updated) {
		this.updated = updated;
	}

	/**
	 * Gets the play count.
	 *
	 * @return the play count
	 */
	public Integer getPlay_count() {
		return this.play_count;
	}

	/**
	 * Sets the play count.
	 *
	 * @param play_count
	 *            the new play count
	 */
	public void setPlay_count(final Integer play_count) {
		this.play_count = play_count;
	}

	/**
	 * Gets the section.
	 *
	 * @return the section
	 */
	public String getSection() {
		return this.section;
	}

	/**
	 * Sets the section.
	 *
	 * @param section
	 *            the new section
	 */
	public void setSection(final String section) {
		this.section = section;
	}

}