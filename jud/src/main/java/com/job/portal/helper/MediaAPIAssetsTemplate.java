/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.helper;

import java.io.Serializable;

import javax.persistence.Cacheable;

// TODO: Auto-generated Javadoc
/**
 * The Class MediaAPIAssetsTemplate.
 */
@Cacheable(false)
public class MediaAPIAssetsTemplate implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The url. */
	private String url;

	/** The width. */
	private Integer width;

	/** The height. */
	private Integer height;

	/** The file size. */
	private Double fileSize;

	/** The content type. */
	private String contentType;

	/** The type. */
	private String type;

	/**
	 * Instantiates a new media API assets template.
	 */
	public MediaAPIAssetsTemplate() {
		super();
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url
	 *            the new url
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public Integer getWidth() {
		return this.width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width
	 *            the new width
	 */
	public void setWidth(final Integer width) {
		this.width = width;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public Integer getHeight() {
		return this.height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height
	 *            the new height
	 */
	public void setHeight(final Integer height) {
		this.height = height;
	}

	/**
	 * Gets the file size.
	 *
	 * @return the file size
	 */
	public Double getFileSize() {
		return this.fileSize;
	}

	/**
	 * Sets the file size.
	 *
	 * @param fileSize
	 *            the new file size
	 */
	public void setFileSize(final Double fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the content type.
	 *
	 * @return the content type
	 */
	public String getContentType() {
		return this.contentType;
	}

	/**
	 * Sets the content type.
	 *
	 * @param contentType
	 *            the new content type
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *            the new type
	 */
	public void setType(final String type) {
		this.type = type;
	}

}