/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.oauth;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomTokenService.
 */
public class CustomTokenService extends DefaultTokenServices {

	/*
	 * The context.
	 * 
	 * @Autowired private ApplicationContext context;
	 */

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.security.oauth2.provider.token.DefaultTokenServices#
	 * loadAuthentication(java.lang.String)
	 */
	@Override
	public OAuth2Authentication loadAuthentication(
			final String accessTokenValue) {
		final OAuth2Authentication auth = super.loadAuthentication(
				accessTokenValue);
		if (auth.getUserAuthentication().isAuthenticated()) {
			/*
			 * final String id = auth.getUserAuthentication().getPrincipal()
			 * .toString(); final User user = new User();
			 * user.setId(Integer.getInteger(id)); final GlobalAccess
			 * globalAccess = (GlobalAccess) this.context
			 * .getBean("globalAccess"); globalAccess.setUser(user);
			 */
		}
		return auth;
	}

}
