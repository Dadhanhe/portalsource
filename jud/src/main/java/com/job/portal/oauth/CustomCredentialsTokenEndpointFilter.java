/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.oauth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomCredentialsTokenEndpointFilter.
 */
public class CustomCredentialsTokenEndpointFilter
		extends ClientCredentialsTokenEndpointFilter {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(CustomCredentialsTokenEndpointFilter.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.oauth2.provider.client.
	 * ClientCredentialsTokenEndpointFilter#successfulAuthentication(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain,
	 * org.springframework.security.core.Authentication)
	 */
	@Override
	protected void successfulAuthentication(final HttpServletRequest request,
			final HttpServletResponse response, final FilterChain chain,
			final Authentication authResult)
			throws IOException, ServletException {
		// this method is useful when we need something from request
		if (logger.isDebugEnabled()) {
			logger.debug("In successful authentication method");
		}
		super.successfulAuthentication(request, response, chain, authResult);
	}
}