/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.oauth;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.job.portal.pojo.customer.User;
import com.job.portal.service.UserService;
import com.job.portal.utils.ObjectUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomAuthenticationProvider.
 */
@Component(value = "customAuthenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

	/** The Constant USER_NOT_FOUND. */
	private static final String USER_NOT_FOUND = "USER_NOT_FOUND";

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(CustomAuthenticationProvider.class);

	/** The user service. */
	@Autowired
	private UserService userService;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(final Authentication authentication) {
		final String name = authentication.getName();
		final String password = authentication.getCredentials().toString();
		if (name != null && password != null) {
			try {

				final ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes();
				Integer platform = null;
				if (attr.getRequest().getParameter("platform") != null) {
					platform = Integer.parseInt(
							attr.getRequest().getParameter("platform"));
				}

				final User user = this.userService.authenticateUser(name,
						password, platform);
				if (user != null) {
					final List<GrantedAuthority> grantedAuths = new ArrayList<>();
					return new UsernamePasswordAuthenticationToken(user.getId(),
							password, grantedAuths);
				}
			} catch (final Exception ex) {
				ObjectUtils.logException(logger, ex, USER_NOT_FOUND);
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * supports (java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
