/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.job;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class EventExecutor.
 */
/* @Component */
public class EventExecutor implements Runnable, IObservable {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(EventExecutor.class);

	/** The event manager. */
	private EventManager eventManager;

	/** The jobs executor. */
	private JobsExecutor jobsExecutor;

	/** The monitor. */
	// this is the object we will be synchronizing on ("the monitor")
	private final Object MONITOR = new Object();

	/** The m observers. */
	private Set<IObserver> mObservers;

	/**
	 * Gets the event manager.
	 *
	 * @return the event manager
	 */
	public EventManager getEventManager() {
		return this.eventManager;
	}

	/**
	 * Sets the event manager.
	 *
	 * @param eventManager
	 *            the new event manager
	 */
	public void setEventManager(final EventManager eventManager) {
		this.eventManager = eventManager;
	}

	/**
	 * Gets the jobs executor.
	 *
	 * @return the jobs executor
	 */
	public JobsExecutor getJobsExecutor() {
		return this.jobsExecutor;
	}

	/**
	 * Sets the jobs executor.
	 *
	 * @param jobsExecutor
	 *            the new jobs executor
	 */
	public void setJobsExecutor(final JobsExecutor jobsExecutor) {
		this.jobsExecutor = jobsExecutor;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.job.IObservable#register(com.job.portal.job.IObserver)
	 */
	@Override
	public void register(final IObserver observer) {
		if (observer == null) {
			return;
		}

		synchronized (this.MONITOR) {
			if (this.mObservers == null) {
				this.mObservers = new HashSet<>(1);
			}
			if (this.mObservers.add(observer) && this.mObservers.size() == 1) {
				// you can call a method here to perform initialization before
				// adding first observer
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.job.IObservable#unregister(com.job.portal.job.IObserver)
	 */
	@Override
	public void unregister(final IObserver observer) {
		if (observer == null) {
			return;
		}

		synchronized (this.MONITOR) {
			if (this.mObservers != null && this.mObservers.remove(observer)
					&& this.mObservers.isEmpty()) {
				// you can call some cleanup when last observer removed
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.job.IObservable#notifyObservers()
	 */
	@Override
	public void notifyObservers() {
		Set<IObserver> observersCopy;

		synchronized (this.MONITOR) {
			if (this.mObservers == null) {
				return;
			}
			observersCopy = new HashSet<>(this.mObservers);
		}

		for (final IObserver observer : observersCopy) {
			observer.update(this.eventManager);
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}
