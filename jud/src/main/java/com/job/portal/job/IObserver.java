/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.job;

// TODO: Auto-generated Javadoc
/**
 * An asynchronous update interface for receiving notifications about I
 * information as the I is constructed.
 */
public interface IObserver {

	/**
	 * This method is called when information about an I which was previously
	 * requested using an asynchronous interface becomes available.
	 *
	 * @param object
	 *            the object
	 */
	public void update(Object object);

}
