/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.job;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class JobsExecutor.
 */
public abstract class JobsExecutor {

	/** The start date. */
	private Date startDate;

	/** The end date. */
	private Date endDate;

	/** The location id. */
	private Integer locationId;

	/** The region id. */
	private Integer regionId;

	/** The thershold. */
	private Double thershold;

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate
	 *            the new start date
	 */
	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate
	 *            the new end date
	 */
	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the location id.
	 *
	 * @return the location id
	 */
	public Integer getLocationId() {
		return this.locationId;
	}

	/**
	 * Sets the location id.
	 *
	 * @param locationId
	 *            the new location id
	 */
	public void setLocationId(final Integer locationId) {
		this.locationId = locationId;
	}

	/**
	 * Gets the region id.
	 *
	 * @return the region id
	 */
	public Integer getRegionId() {
		return this.regionId;
	}

	/**
	 * Sets the region id.
	 *
	 * @param regionId
	 *            the new region id
	 */
	public void setRegionId(final Integer regionId) {
		this.regionId = regionId;
	}

	/**
	 * Gets the thershold.
	 *
	 * @return the thershold
	 */
	public Double getThershold() {
		return this.thershold;
	}

	/**
	 * Sets the thershold.
	 *
	 * @param thershold
	 *            the new thershold
	 */
	public void setThershold(final Double thershold) {
		this.thershold = thershold;
	}

}
