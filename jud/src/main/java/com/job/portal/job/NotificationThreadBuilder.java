/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.job;

import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class NotificationThreadBuilder.
 */
public class NotificationThreadBuilder {

	/** The name. */
	private String name;

	/** The pay load. */
	private Object payLoad;

	/** The rule. */
	private String rule;

	/** The extra options. */
	private Object[] extraOptions;

	/** The notification map. */
	private Map<Object, Object> notificationMap = new HashMap<>();

	/**
	 * Instantiates a new notification thread builder.
	 */
	public NotificationThreadBuilder() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the pay load.
	 *
	 * @return the pay load
	 */
	public Object getPayLoad() {
		return this.payLoad;
	}

	/**
	 * Sets the pay load.
	 *
	 * @param payLoad
	 *            the new pay load
	 */
	public void setPayLoad(final Object payLoad) {
		this.payLoad = payLoad;
	}

	/**
	 * Gets the rule.
	 *
	 * @return the rule
	 */
	public String getRule() {
		return this.rule;
	}

	/**
	 * Sets the rule.
	 *
	 * @param rule
	 *            the new rule
	 */
	public void setRule(final String rule) {
		this.rule = rule;
	}

	/**
	 * Gets the extra options.
	 *
	 * @return the extra options
	 */
	public Object[] getExtraOptions() {
		return this.extraOptions;
	}

	/**
	 * Sets the extra options.
	 *
	 * @param extraOptions
	 *            the new extra options
	 */
	public void setExtraOptions(final Object[] extraOptions) {
		this.extraOptions = extraOptions;
	}

	/**
	 * Gets the notification map.
	 *
	 * @return the notification map
	 */
	public Map<Object, Object> getNotificationMap() {
		return this.notificationMap;
	}

	/**
	 * Sets the notification map.
	 *
	 * @param notificationMap
	 *            the notification map
	 */
	public void setNotificationMap(final Map<Object, Object> notificationMap) {
		this.notificationMap = notificationMap;
	}
}
