/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.job;

// TODO: Auto-generated Javadoc
/**
 * The Interface IObservable.
 */
public interface IObservable {

	/**
	 * Register.
	 *
	 * @param obj
	 *            the obj
	 */
	public void register(IObserver obj);

	/**
	 * Unregister.
	 *
	 * @param obj
	 *            the obj
	 */
	public void unregister(IObserver obj);

	/**
	 * Notify observers.
	 */
	public void notifyObservers();

}
