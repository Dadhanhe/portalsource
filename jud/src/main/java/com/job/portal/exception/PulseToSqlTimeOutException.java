/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class PulseToSqlTimeOutException.
 */
public class PulseToSqlTimeOutException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The metric. */
	private AbstractValueObject metric;

	/** The tenant location ID. */
	private Integer tenantLocationID;

	/** The mode. */
	private String mode;

	/**
	 * Instantiates a new pulse to sql time out exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public PulseToSqlTimeOutException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new pulse to sql time out exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public PulseToSqlTimeOutException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Sets the meta data.
	 *
	 * @param metric
	 *            the metric
	 * @param tenantLocationID
	 *            the tenant location ID
	 * @param mode
	 *            the mode
	 */
	public void setMetaData(AbstractValueObject metric,
			Integer tenantLocationID, String mode) {
		this.metric = metric;
		this.tenantLocationID = tenantLocationID;
		this.mode = mode;
	}

	/**
	 * Gets the metric.
	 *
	 * @return the metric
	 */
	public AbstractValueObject getMetric() {
		return metric;
	}

	/**
	 * Sets the metric.
	 *
	 * @param metric
	 *            the new metric
	 */
	public void setMetric(AbstractValueObject metric) {
		this.metric = metric;
	}

	/**
	 * Gets the mode.
	 *
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Sets the mode.
	 *
	 * @param mode
	 *            the new mode
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * Gets the tenant location ID.
	 *
	 * @return the tenant location ID
	 */
	public Integer getTenantLocationID() {
		return tenantLocationID;
	}

	/**
	 * Sets the tenant location ID.
	 *
	 * @param tenantLocationID
	 *            the new tenant location ID
	 */
	public void setTenantLocationID(Integer tenantLocationID) {
		this.tenantLocationID = tenantLocationID;
	}

}