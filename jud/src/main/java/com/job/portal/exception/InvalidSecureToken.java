/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class InvalidSecureToken.
 */
public class InvalidSecureToken extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new invalid secure token.
	 */
	public InvalidSecureToken() {
		super(null);
	}

	/**
	 * Instantiates a new invalid secure token.
	 *
	 * @param msg
	 *            the msg
	 */
	public InvalidSecureToken(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new invalid secure token.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public InvalidSecureToken(final String msg, final Throwable cause) {
		super(msg, cause);
	}

}
