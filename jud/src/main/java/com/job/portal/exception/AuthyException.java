/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthyException.
 */
public class AuthyException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new authy exception.
	 */
	public AuthyException() {
		super(null);
	}

	/**
	 * Instantiates a new authy exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public AuthyException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new authy exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public AuthyException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

}
