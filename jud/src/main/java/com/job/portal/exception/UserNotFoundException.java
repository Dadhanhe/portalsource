/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// TODO: Auto-generated Javadoc
//Created by Sapan on 14/11/16.
/**
 * The Class UserNotFoundException.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RecordNotFoundException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new user not found exception.
	 */
	public UserNotFoundException() {
	}

	/**
	 * Instantiates a new user not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public UserNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new user not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public UserNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new user not found exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public UserNotFoundException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new user not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public UserNotFoundException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
