/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class MetricConflictException.
 */
public class MetricConflictException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The field scheme ID. */
	private Integer fieldSchemeID;

	/** The value. */
	private String value;

	/**
	 * Instantiates a new metric conflict exception.
	 */
	public MetricConflictException() {
		super(null);
	}

	/**
	 * Instantiates a new metric conflict exception.
	 *
	 * @param fieldSchemeID
	 *            the field scheme ID
	 * @param value
	 *            the value
	 */
	public MetricConflictException(Integer fieldSchemeID, String value) {
		super(null);
		this.fieldSchemeID = fieldSchemeID;
		this.value = value;
	}

	/**
	 * Instantiates a new metric conflict exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public MetricConflictException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new metric conflict exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public MetricConflictException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Gets the field scheme ID.
	 *
	 * @return the field scheme ID
	 */
	public Integer getFieldSchemeID() {
		return fieldSchemeID;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the field scheme ID.
	 *
	 * @param fieldSchemeID
	 *            the new field scheme ID
	 */
	public void setFieldSchemeID(Integer fieldSchemeID) {
		this.fieldSchemeID = fieldSchemeID;
	}

	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
