/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class InvalidPasswordException.
 */
public class InvalidPasswordException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new invalid password exception.
	 */
	public InvalidPasswordException() {

	}

	/**
	 * Instantiates a new invalid password exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public InvalidPasswordException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new invalid password exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public InvalidPasswordException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Instantiates a new invalid password exception.
	 *
	 * @param fieldName
	 *            the field name
	 * @param msg
	 *            the msg
	 */
	public InvalidPasswordException(final String fieldName, final String msg) {
		super(msg);
	}
}
