/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class InvalidMetricSpreadsheetException.
 */
public class InvalidMetricSpreadsheetException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The column index. */
	private Integer columnIndex;

	/** The row index. */
	private Integer rowIndex;

	/**
	 * Instantiates a new invalid metric spreadsheet exception.
	 */
	public InvalidMetricSpreadsheetException() {
		super(null);
	}

	/**
	 * Instantiates a new invalid metric spreadsheet exception.
	 *
	 * @param columnIndex
	 *            the column index
	 * @param rowIndex
	 *            the row index
	 */
	public InvalidMetricSpreadsheetException(Integer columnIndex,
			Integer rowIndex) {
		super(null);
		this.columnIndex = columnIndex;
		this.rowIndex = rowIndex;
	}

	/**
	 * Instantiates a new invalid metric spreadsheet exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public InvalidMetricSpreadsheetException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new invalid metric spreadsheet exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public InvalidMetricSpreadsheetException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Gets the column index.
	 *
	 * @return the column index
	 */
	public Integer getColumnIndex() {
		return columnIndex;
	}

	/**
	 * Gets the row index.
	 *
	 * @return the row index
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * Sets the column index.
	 *
	 * @param columnIndex
	 *            the new column index
	 */
	public void setColumnIndex(Integer columnIndex) {
		this.columnIndex = columnIndex;
	}

	/**
	 * Sets the row index.
	 *
	 * @param rowIndex
	 *            the new row index
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
