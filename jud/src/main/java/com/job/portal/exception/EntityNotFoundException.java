/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityNotFoundException.
 */
public class EntityNotFoundException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new entity not found exception.
	 */
	public EntityNotFoundException() {
		super(null);
	}

	/**
	 * Instantiates a new entity not found exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public EntityNotFoundException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new entity not found exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public EntityNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.NestedRuntimeException#getMessage()
	 */
	@Override
	public String getMessage() {
		return "Entity not found";
	}
}
