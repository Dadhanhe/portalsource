/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class DuplicateUserIdException.
 */
public class DuplicateUserIdException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The column index. */
	private Integer columnIndex;

	/** The row index. */
	private Integer rowIndex;

	/**
	 * Instantiates a new duplicate user id exception.
	 */
	public DuplicateUserIdException() {
		super(null);
	}

	/**
	 * Instantiates a new duplicate user id exception.
	 *
	 * @param columnIndex
	 *            the column index
	 * @param rowIndex
	 *            the row index
	 */
	public DuplicateUserIdException(final Integer columnIndex,
			final Integer rowIndex) {
		super(null);
		this.columnIndex = columnIndex;
		this.rowIndex = rowIndex;
	}

	/**
	 * Instantiates a new duplicate user id exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public DuplicateUserIdException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new duplicate user id exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public DuplicateUserIdException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Gets the column index.
	 *
	 * @return the column index
	 */
	public Integer getColumnIndex() {
		return this.columnIndex;
	}

	/**
	 * Gets the row index.
	 *
	 * @return the row index
	 */
	public Integer getRowIndex() {
		return this.rowIndex;
	}

	/**
	 * Sets the column index.
	 *
	 * @param columnIndex
	 *            the new column index
	 */
	public void setColumnIndex(final Integer columnIndex) {
		this.columnIndex = columnIndex;
	}

	/**
	 * Sets the row index.
	 *
	 * @param rowIndex
	 *            the new row index
	 */
	public void setRowIndex(final Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
