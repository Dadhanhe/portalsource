/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import java.security.PrivilegedActionException;

// TODO: Auto-generated Javadoc
/**
 * The Class RecordNotFoundException.
 */
public class RecordNotFoundException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2910876576081113590L;

	/**
	 * Instantiates a new record not found exception.
	 */
	public RecordNotFoundException() {
	}

	/**
	 * Instantiates a new record not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public RecordNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new record not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public RecordNotFoundException(final String message,
			final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new record not found exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public RecordNotFoundException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new record not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public RecordNotFoundException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
