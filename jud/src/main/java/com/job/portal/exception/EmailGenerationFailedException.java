/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailGenerationFailedException.
 */
public class EmailGenerationFailedException extends CodeBasedException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new email generation failed exception.
	 */
	public EmailGenerationFailedException() {
		super(null);
	}

	/**
	 * Instantiates a new email generation failed exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public EmailGenerationFailedException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new email generation failed exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public EmailGenerationFailedException(final String msg,
			final Throwable cause) {
		super(msg, cause);
	}
}
