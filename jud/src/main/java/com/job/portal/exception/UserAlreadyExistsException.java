/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// TODO: Auto-generated Javadoc
// Created by Sapan on 14/11/16.
/**
 * The Class UserAlreadyExistsException.
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Record already exists")
public class UserAlreadyExistsException extends DuplicateRecordException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new user already exists exception.
	 */
	public UserAlreadyExistsException() {
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param message
	 *            the message
	 */
	public UserAlreadyExistsException(final String message) {

		super("The user is already registered with the contact information: "
				+ message);
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public UserAlreadyExistsException(final String message,
			final Throwable cause) {
		super("The user is already registered with the contact information: "
				+ message, cause);
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public UserAlreadyExistsException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public UserAlreadyExistsException(final String message,
			final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super("The user is already registered with the contact information: "
				+ message, cause, enableSuppression, writableStackTrace);
	}
}
