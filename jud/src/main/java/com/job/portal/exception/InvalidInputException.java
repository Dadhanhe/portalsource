/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import java.io.Serializable;

import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

// TODO: Auto-generated Javadoc
/**
 * The Class InvalidInputException.
 */
public class InvalidInputException extends RuntimeException
		implements Serializable {

	/** The errors. */

	private transient Errors errors;

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param bindingResult
	 *            the binding result
	 */
	public InvalidInputException(final BindingResult bindingResult) {
		this.errors = bindingResult;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8194611661239926763L;

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public Errors getErrors() {
		return this.errors;
	}

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param message
	 *            the message
	 * @param errors
	 *            the errors
	 */
	public InvalidInputException(final String message, final Errors errors) {
		super(message);
		this.errors = errors;

	}

	/**
	 * Instantiates a new invalid input exception.
	 */
	public InvalidInputException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param arg0
	 *            the arg 0
	 */
	public InvalidInputException(final String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param arg0
	 *            the arg 0
	 */
	public InvalidInputException(final Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param arg0
	 *            the arg 0
	 * @param arg1
	 *            the arg 1
	 */
	public InvalidInputException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new invalid input exception.
	 *
	 * @param arg0
	 *            the arg 0
	 * @param arg1
	 *            the arg 1
	 * @param arg2
	 *            the arg 2
	 * @param arg3
	 *            the arg 3
	 */
	public InvalidInputException(final String arg0, final Throwable arg1,
			final boolean arg2, final boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}
}
