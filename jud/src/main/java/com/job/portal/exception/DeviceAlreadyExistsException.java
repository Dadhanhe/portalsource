/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
// Created by Sapan on 18/11/16.
/**
 * The Class DeviceAlreadyExistsException.
 */
public class DeviceAlreadyExistsException extends DuplicateRecordException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 370194113669662269L;

	/**
	 * Instantiates a new device already exists exception.
	 */
	public DeviceAlreadyExistsException() {
	}

	/**
	 * Instantiates a new device already exists exception.
	 *
	 * @param message
	 *            the message
	 */
	public DeviceAlreadyExistsException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new device already exists exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public DeviceAlreadyExistsException(final String message,
			final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new device already exists exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public DeviceAlreadyExistsException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new device already exists exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public DeviceAlreadyExistsException(final String message,
			final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
