/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// TODO: Auto-generated Javadoc
// Created by Sapan on 25/11/16.
/**
 * The Class SystemConfigurationException.
 */
@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "Something's missing in the system configuration.")
public class SystemConfigurationException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new system configuration exception.
	 */
	public SystemConfigurationException() {
	}

	/**
	 * Instantiates a new system configuration exception.
	 *
	 * @param message
	 *            the message
	 */
	public SystemConfigurationException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new system configuration exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public SystemConfigurationException(final String message,
			final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new system configuration exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public SystemConfigurationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new system configuration exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public SystemConfigurationException(final String message,
			final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
