/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class InvalidUserException.
 */
public class InvalidUserException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The field name. */
	private String fieldName;

	/** The msg. */
	private String msg;

	/**
	 * Instantiates a new invalid user exception.
	 */
	public InvalidUserException() {
	}

	/**
	 * Instantiates a new invalid user exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public InvalidUserException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new invalid user exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public InvalidUserException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Instantiates a new invalid user exception.
	 *
	 * @param fieldName
	 *            the field name
	 * @param msg
	 *            the msg
	 */
	public InvalidUserException(final String fieldName, final String msg) {
		super(msg);
		this.fieldName = fieldName;
		this.msg = msg;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return this.msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg
	 *            the new msg
	 */
	public void setMsg(final String msg) {
		this.msg = msg;
	}
}
