/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.dao.DataAccessException;

// TODO: Auto-generated Javadoc
/**
 * The Class CodeBasedException.
 */
public abstract class CodeBasedException extends DataAccessException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2598260267210676522L;

	/**
	 * Instantiates a new code based exception.
	 */
	public CodeBasedException() {
		super(null);
	}

	/**
	 * Instantiates a new code based exception.
	 *
	 * @param msg
	 *            the msg
	 */
	public CodeBasedException(final String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new code based exception.
	 *
	 * @param msg
	 *            the msg
	 * @param cause
	 *            the cause
	 */
	public CodeBasedException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		final String className = this.getClass().getName();
		return DigestUtils.md5Hex(className);
	}
}
