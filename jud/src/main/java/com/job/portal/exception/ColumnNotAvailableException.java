/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class ColumnNotAvailableException.
 */
public class ColumnNotAvailableException extends RecordNotFoundException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -775228236868460273L;

	/** The entity. */
	Class<?> entity;

	/**
	 * Instantiates a new column not available exception.
	 *
	 * @param entity
	 *            the entity
	 */
	public ColumnNotAvailableException(final Class<?> entity) {
		// TODO Auto-generated constructor stub
		this.entity = entity;
	}

	/** The entity name. */
	String entityName;

	/**
	 * Gets the entity name.
	 *
	 * @return the entity name
	 */
	public String getEntityName() {
		return this.entity.getSimpleName();
	}
}
