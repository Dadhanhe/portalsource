/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// TODO: Auto-generated Javadoc
// Exception thrown when any record that is being inserted is already existing
// through its unique key. Created by Sapan on 08/11/16.
/**
 * The Class DuplicateRecordException.
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Record already exists")
public class DuplicateRecordException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The field name. */
	private String fieldName;

	/**
	 * Instantiates a new duplicate record exception.
	 */
	public DuplicateRecordException() {
	}

	/**
	 * Instantiates a new duplicate record exception.
	 *
	 * @param message
	 *            the message
	 */
	public DuplicateRecordException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new duplicate record exception.
	 *
	 * @param fieldName
	 *            the field name
	 * @param message
	 *            the message
	 */
	public DuplicateRecordException(final String fieldName,
			final String message) {
		super(message);
		setFieldName(fieldName);
	}

	/**
	 * Instantiates a new duplicate record exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public DuplicateRecordException(final String message,
			final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new duplicate record exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public DuplicateRecordException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new duplicate record exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public DuplicateRecordException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

}
