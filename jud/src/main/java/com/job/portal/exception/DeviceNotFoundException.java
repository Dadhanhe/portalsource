/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

// TODO: Auto-generated Javadoc
// Created by Sapan on 18/11/16.
/**
 * The Class DeviceNotFoundException.
 */
public class DeviceNotFoundException extends RecordNotFoundException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 316528183758792474L;

	/**
	 * Instantiates a new device not found exception.
	 */
	public DeviceNotFoundException() {
	}

	/**
	 * Instantiates a new device not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public DeviceNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new device not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public DeviceNotFoundException(final String message,
			final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new device not found exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public DeviceNotFoundException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new device not found exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public DeviceNotFoundException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
