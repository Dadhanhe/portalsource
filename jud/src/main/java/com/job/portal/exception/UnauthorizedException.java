/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// TODO: Auto-generated Javadoc
// Created by Sapan on 25/11/16.
/**
 * The Class InternalException.
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Unauthorized")
public class UnauthorizedException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new internal exception.
	 */
	public UnauthorizedException() {
	}

	/**
	 * Instantiates a new internal exception.
	 *
	 * @param message
	 *            the message
	 */
	public UnauthorizedException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new internal exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public UnauthorizedException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new internal exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public UnauthorizedException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new internal exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public UnauthorizedException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
