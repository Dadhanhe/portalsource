/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.exception;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class DuplicateMetricException.
 */
public class DuplicateMetricException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The field name. */
	private String fieldName;

	/** The resource. */
	private Class<? extends AbstractValueObject> resource;

	/**
	 * Instantiates a new duplicate metric exception.
	 *
	 * @param resource
	 *            the resource
	 * @param fieldName
	 *            the field name
	 */
	public DuplicateMetricException(
			final Class<? extends AbstractValueObject> resource,
			final String fieldName) {
		this.fieldName = fieldName;
		this.resource = resource;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public Class<? extends AbstractValueObject> getResource() {
		return this.resource;
	}

	/**
	 * Sets the resource.
	 *
	 * @param resource
	 *            the new resource
	 */
	public void setResource(
			final Class<? extends AbstractValueObject> resource) {
		this.resource = resource;
	}

}
