/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.EducationBoard;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardDAO.
 */
@Component
public class EducationBoardDAO
		extends CachebleCommonHibernateDAO<EducationBoard> {

	/**
	 * Gets the all education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the all education board
	 */
	public List<EducationBoard> getAllEducationBoard() {
		final Criteria criteria = createCriteria(EducationBoard.class);
		final List<EducationBoard> list = criteria.list();
		if (!list.isEmpty()) {
			return list;
		} else {
			final List<EducationBoard> list1 = new ArrayList<>();
			return list1;
		}

	}

	public EducationBoard findBySpecific(final Integer id) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(EducationBoard.class);
		criteria.add(Restrictions.eq("id", id));
		final List<EducationBoard> list = criteria.list();
		final EducationBoard edu = list.get(0);
		return edu;
	}

}
