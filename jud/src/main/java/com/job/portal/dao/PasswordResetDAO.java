/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CommonHibernateDAO;
import com.job.portal.pojo.customer.PasswordReset;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class PasswordResetDAO.
 */
@SuppressWarnings("unchecked")
@Component
public class PasswordResetDAO extends CommonHibernateDAO<PasswordReset> {

	/**
	 * Instantiates a new password reset DAO.
	 */
	public PasswordResetDAO() {
		super();
		setEntityClass(PasswordReset.class);
	}

	/**
	 * Validate token.
	 *
	 * @param industryId
	 *            the industry id
	 * @param token
	 *            the token
	 * @return the password reset
	 */
	public PasswordReset validateToken(final Integer industryId,
			final String token) {
		final Criteria criteria = createCriteria(PasswordReset.class);
		if (!Objects.isEmpty(industryId)) {
			criteria.add(Restrictions.eq("industryId", industryId));
		}
		criteria.add(Restrictions.eq("token", token));
		criteria.add(Restrictions.eq("isUsed", false));
		final PasswordReset pr = (PasswordReset) criteria.uniqueResult();
		return pr;
	}

}