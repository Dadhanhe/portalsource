/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.common.AppPreference;

// TODO: Auto-generated Javadoc
/**
 * The Class AppPreferenceDAO.
 */
@Component
public class AppPreferenceDAO
		extends CachebleCommonHibernateDAO<AppPreference> {

	/**
	 * Instantiates a new app preference DAO.
	 */
	public AppPreferenceDAO() {
		super();
		setEntityClass(AppPreference.class);
	}

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(AppPreferenceDAO.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.CachebleCommonHibernateDAO#findByProperty(java.
	 * lang.Integer, java.lang.String, java.lang.Object)
	 */
	@Override
	public AppPreference findByProperty(final String propertyName,
			final String propertyValue) {
		final Criteria criteria = createCriteria(AppPreference.class);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		// criteria.setCacheable(true);
		return (AppPreference) criteria.uniqueResult();
	}
}