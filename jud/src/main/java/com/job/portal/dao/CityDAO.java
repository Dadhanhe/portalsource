/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.City;

/**
 * @author Vaibhav
 *
 */
@Component
public class CityDAO extends CachebleCommonHibernateDAO<City> {

	public CityDAO() {
		super();
		setEntityClass(City.class);
	}

	@SuppressWarnings("unchecked")
	public City getCityByStateAndCountry(final String cityName,
			final String stateName, final String countryName) {
		final Criteria criteria = createCriteria(City.class);
		criteria.createAlias("state", "s");
		criteria.add(Restrictions.eq("name", cityName))
				.add(Restrictions.eq("s.name", stateName));
		final List<City> cities = criteria.list();
		if (!cities.isEmpty()) {
			return cities.get(0);
		} else {
			return null;
		}
	}

}
