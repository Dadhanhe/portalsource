/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao.common;

import java.util.List;

import org.hibernate.criterion.Criterion;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Interface IHibernateDAO.
 *
 * @param <C>
 *            the generic type
 */
public interface IHibernateDAO<C extends AbstractValueObject> {

	/**
	 * Save.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	C save(C object);

	/**
	 * Update.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	C update(C object);

	/**
	 * Merge.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	C merge(C object);

	/**
	 * Save or update.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	C saveOrUpdate(C object);

	/**
	 * Delete.
	 *
	 * @param object
	 *            the object
	 */
	void delete(C object);

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the c
	 */
	C findById(Integer id);

	/**
	 * Find by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the c
	 */
	C findByProperty(String propertyName, String propertyValue);

	/**
	 * List by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the list
	 */
	List<C> listByProperty(String propertyName, String propertyValue);

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the list
	 */
	List<C> findAll(Integer first, Integer total, String search, Status status);

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param status
	 *            the status
	 * @return the list
	 */
	List<C> findAll(Integer first, Integer total, String search,
			String[] orderBy, Sort[] sort, Status status);

	/**
	 * Find total.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the long
	 */
	Long findTotal(String search, Status status);

	/**
	 * Gets the search restrictions.
	 *
	 * @param search
	 *            the search
	 * @return the search restrictions
	 */
	Criterion getSearchRestrictions(String search);

	/**
	 * Commit.
	 */
	void commit();

}
