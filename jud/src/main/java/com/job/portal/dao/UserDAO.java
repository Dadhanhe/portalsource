/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.UserPlain;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;
import com.job.portal.utils.SystemConfigs;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDAO.
 */
@SuppressWarnings("unchecked")
@Component
public class UserDAO extends CachebleCommonHibernateDAO<User> {

	/** The Constant ACTIVE_STATUS. */
	private static final String ACTIVE_STATUS = "activeStatus";

	/** The Constant EMAIL. */
	private static final String EMAIL = "email";

	/** The Constant LAST_NAME. */
	private static final String LAST_NAME = "lastName";

	/** The Constant FIRST_NAME. */
	private static final String FIRST_NAME = "firstName";

	/** The Constant BLANK_SPACE. */
	private static final String BLANK_SPACE = "";

	/**
	 * Instantiates a new user DAO.
	 */
	public UserDAO() {
		super();
		setEntityClass(User.class);
	}

	/**
	 * Gets the search restrictions.
	 *
	 * @param search
	 *            the search
	 * @return the search restrictions
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.CachebleCommonHibernateDAO#
	 * getSearchRestrictions (java.lang.String)
	 */
	@Override
	public Criterion getSearchRestrictions(final String search) {
		final Disjunction disjunction = Restrictions.disjunction();
		if (search != null && !"".equals(search.trim())) {
			final String keyword = search.trim();
			final String[] searchWords = ObjectUtils.getSearchWords(keyword);
			final MatchMode mode = ObjectUtils.isExactWord(keyword)
					? MatchMode.EXACT
					: MatchMode.ANYWHERE;
			for (final String word : searchWords) {
				disjunction.add(Restrictions.ilike(FIRST_NAME, word, mode));
				disjunction.add(Restrictions.ilike(LAST_NAME, word, mode));
				disjunction.add(Restrictions.ilike(EMAIL, word, mode));

				if (StringUtils.isNumeric(word)) {
					disjunction
							.add(Restrictions.eq("id", Integer.valueOf(word)));
				}
			}
		}
		return disjunction;
	}

	/**
	 * Find total.
	 *
	 * @param industryId
	 *            the industry id
	 * @param search
	 *            the search
	 * @param userIds
	 *            the user ids
	 * @param activeStatus
	 *            the active status
	 * @return the long
	 */
	public Long findTotal(final Integer industryId, final String search,
			final List<Integer> userIds, final Status activeStatus) {
		final Criteria criteria = createCriteria(User.class);
		criteria.setProjection(Projections.rowCount());
		if (!Objects.isEmpty(industryId)) {
			criteria.add(Restrictions.eq("industryId", industryId));
		}
		if (!Objects.isEmpty(userIds) && userIds.size() > 0) {
			criteria.add(Restrictions.in("id", userIds));
		}
		if (!Objects.isEmpty(activeStatus)) {
			criteria.add(Restrictions.eq(ACTIVE_STATUS, activeStatus));
		}
		if (search != null && !search.trim().equals(BLANK_SPACE)) {
			criteria.add(getSearchRestrictions(search));
		}
		return (Long) criteria.uniqueResult();
	}

	/**
	 * Gets the user pulse ID infos.
	 *
	 * @param emailAddress
	 *            the email address
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param birthMonth
	 *            the birth month
	 * @param birthDay
	 *            the birth day
	 * @param notUserID
	 *            the not user ID
	 * @param activeStatus
	 *            the active status
	 * @param queryPulseID
	 *            the query pulse ID
	 * @return the user pulse ID infos
	 */
	public List<User> getUserPulseIDInfos(final String emailAddress,
			final String firstName, final String lastName,
			final Integer birthMonth, final Integer birthDay,
			final Integer notUserID, final Status activeStatus,
			final String queryPulseID) {
		final Criteria criteria = createCriteria(User.class);
		if (!Objects.isEmpty(emailAddress)) {
			criteria.add(Restrictions.eq(EMAIL, emailAddress));
		}
		if (!Objects.isEmpty(firstName)) {
			criteria.add(Restrictions.eq(FIRST_NAME, firstName));
		}
		if (!Objects.isEmpty(lastName)) {
			criteria.add(Restrictions.eq(LAST_NAME, lastName));
		}
		if (!Objects.isEmpty(birthMonth)) {
			criteria.add(Restrictions.eq("birthMonth", birthMonth));
		}
		if (!Objects.isEmpty(birthDay)) {
			criteria.add(Restrictions.eq("birthDay", birthDay));
		}
		if (!Objects.isEmpty(notUserID)) {
			criteria.add(Restrictions.ne("id", notUserID));
		}
		if (!Objects.isEmpty(activeStatus)) {
			criteria.add(Restrictions.eq(ACTIVE_STATUS, activeStatus));
		}
		if (!Objects.isEmpty(queryPulseID)) {
			criteria.add(Restrictions.eq("pulseId", queryPulseID));
		}
		List<User> list = criteria.list();
		if (list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	/**
	 * Find by email.
	 *
	 * @param email
	 *            the email
	 * @return the user
	 */
	public User findByEmail(final String email) {
		final Criteria criteria = createCriteria(User.class);
		criteria.add(Restrictions.eq(EMAIL, email));
		final List<User> list = criteria.list();
		if ((list != null && list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * Find team members.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param contributor
	 *            the contributor
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @return the list
	 */
	public List<User> findTeamMembers(final Integer tenantLocationId,
			final boolean contributor, final Integer first,
			final Integer total) {
		// @formatter:off
		final String sqlString=
				"SELECT user_location_group.USER_ID"
				+ " FROM user_location_group "
				+ " JOIN "
				+ "location_group ON (user_location_group.LOCATION_GROUP_ID = location_group.ID) "
				+ " JOIN "
				+ "location_group_type ON (location_group_type.id = location_group.group_type_id AND is_default=true) "
				+ " JOIN "
				+ "user ON (user.id = user_location_group.USER_ID) "
				+ "WHERE "
				+ "location_group.TENANT_LOCATION_ID =:tenantLocationId "
				//+ "AND user_location_group.is_assigned=true "
				+ "AND user.ACTIVE_STATUS = 0 "
				+ "AND user_location_group.IS_CONTRIBUTOR = :contributor "
				+ "AND location_group.ACTIVE_STATUS = 0";
		// @formatter:on
		final SQLQuery query = getSession().createSQLQuery(sqlString);
		query.setParameter("tenantLocationId", tenantLocationId);
		query.setParameter("contributor", contributor);
		final List<Integer> userList = query.list();
		final Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.in("id", userList));
		criteria.addOrder(Order.asc(FIRST_NAME));
		if (first != null && total != null) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
		return criteria.list();
	}

	/**
	 * Find team members.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param contributor
	 *            the contributor
	 * @return the list
	 */
	public List<User> findTeamMembers(final Integer tenantLocationId,
			final boolean contributor) {
		return findTeamMembers(tenantLocationId, contributor, null, null);
	}

	/**
	 * Gets the available user list.
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the available user list
	 */
	public List<User> getAvailableUserList(final String sqlQuery) {
		final SQLQuery query = getSession().createSQLQuery(sqlQuery);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> users = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			users.add(ObjectUtils.mapToPojo(map, User.class));
		}

		return users;
	}

	/**
	 * Gets the users ids of role tenant.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param roleId
	 *            the role id
	 * @return the users ids of role tenant
	 */
	public List<Integer> getUsersIdsOfRoleTenant(final Integer tenantId,
			final Integer roleId) {
		String sqlString = "";
		/*
		 * final String sqlString =
		 * "select utr.USER_ID from user_tenant_role utr where " + (tenantId !=
		 * null ? (" utr.TENANT_ID = " + tenantId) : "") + (roleId != null &&
		 * tenantId != null ? " AND " : "") + (roleId != null ? (
		 * " utr.ROLE_ID = " + roleId) : "");
		 */

		if (tenantId != null && roleId != null) {
			sqlString = "select utr.USER_ID from user_tenant_role utr where "
					+ " utr.TENANT_ID =  :tenantId AND utr.ROLE_ID = :roleId ";
		} else if (tenantId != null) {
			sqlString = "select utr.USER_ID from user_tenant_role utr where "
					+ " utr.TENANT_ID =  :tenantId ";
		} else {
			sqlString = "select utr.USER_ID from user_tenant_role utr where "
					+ " utr.ROLE_ID = :roleId";
		}
		final SQLQuery query = getSession().createSQLQuery(sqlString);
		if (Objects.isNotNull(roleId)) {
			query.setParameter("roleId", roleId);
		}
		if (Objects.isNotNull(tenantId)) {
			query.setParameter("tenantId", tenantId);
		}
		final List<Integer> userIDs = query.list();
		return userIDs;

	}

	/**
	 * Gets the active users ids of role tenant.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param roleId
	 *            the role id
	 * @param status
	 *            the status
	 * @return the active users ids of role tenant
	 */
	public List<Integer> getActiveUsersIdsOfRoleTenant(final Integer tenantId,
			final Integer roleId, final Status status) {
		String sqlString = "";

		if (tenantId != null && roleId != null) {
			sqlString = "select utr.USER_ID from user_tenant_role utr inner join user u on utr.USER_ID = u.ID where "
					+ " utr.TENANT_ID =  :tenantId AND utr.ROLE_ID = :roleId AND u.ACTIVE_STATUS = :status";
		} else if (tenantId != null) {
			sqlString = "select utr.USER_ID from user_tenant_role utr inner join user u on utr.USER_ID = u.ID where "
					+ " utr.TENANT_ID =  :tenantId AND u.ACTIVE_STATUS = :status";
		} else {
			sqlString = "select utr.USER_ID from user_tenant_role utr inner join user on utr.USER_ID = u.ID u where "
					+ " utr.ROLE_ID = :roleId AND u.ACTIVE_STATUS = :status";
		}
		final SQLQuery query = getSession().createSQLQuery(sqlString);
		if (Objects.isNotNull(roleId)) {
			query.setParameter("roleId", roleId);
		}
		if (Objects.isNotNull(tenantId)) {
			query.setParameter("tenantId", tenantId);
		}
		if (Objects.isNotNull(status)) {
			query.setParameter("status", status.value());
		}
		final List<Integer> userIDs = query.list();
		return userIDs;
	}

	/**
	 * Gets the users ids of location group type group.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param locationGroupId
	 *            the location group id
	 * @param locationGroupTypeId
	 *            the location group type id
	 * @return the users ids of location group type group
	 */
	public List<Integer> getUsersIdsOfLocationGroupTypeGroup(
			final Integer tenantLocationId, final Integer locationGroupId,
			final Integer locationGroupTypeId) {
		final StringBuilder sqlString = new StringBuilder(
				"select ulg.USER_ID from user_location_group ulg ");
		sqlString.append(
				((locationGroupTypeId != null || tenantLocationId != null)
						? " join location_group lg ON ulg.LOCATION_GROUP_ID = lg.ID "
						: ""));
		sqlString.append((tenantLocationId != null
				? " join location_group_type lgt ON lg.GROUP_TYPE_ID = lgt.ID "
				: ""));
		sqlString.append(" where ");
		sqlString.append((tenantLocationId != null
				? " lgt.IS_DEFAULT = true AND lg.TENANT_LOCATION_ID = "
						+ tenantLocationId
				: ""));
		sqlString.append((locationGroupTypeId != null
				? " lg.GROUP_TYPE_ID = " + locationGroupTypeId
				: ""));
		sqlString.append((locationGroupId != null
				? " ulg.LOCATION_GROUP_ID = " + locationGroupId
				: ""));
		final SQLQuery query = getSession()
				.createSQLQuery(sqlString.toString());
		return query.list();
	}

	/**
	 * Gets the user ids.
	 *
	 * @param userId
	 *            the user id
	 * @return the user ids
	 */
	public List<Integer> getUserIds(final Integer userId) {
		final Criteria criteria = createCriteria(User.class);
		criteria.setProjection(Projections.property("id"));
		if (userId != null) {
			criteria.add(
					Restrictions.or(Restrictions.eq("createdBy.id", userId),
							Restrictions.eq("updatedBy.id", userId)));
		}
		return criteria.list();
	}

	/**
	 * Find all.
	 *
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param userIds
	 *            the user ids
	 * @param status
	 *            the status
	 * @param search
	 *            the search
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @return the list
	 */
	public List<User> findAll(final Integer first, final Integer total,
			final List<Integer> userIds, final Status status,
			final String search, final String orderBy, final Sort sort) {
		final Criteria criteria = createCriteria(User.class);
		List<User> list = new ArrayList<>();
		if (first != null && total != null) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
		if (userIds.size() > 0) {
			criteria.add(Restrictions.in("id", userIds));

			if (!Objects.isEmpty(status)) {
				criteria.add(Restrictions.eq(ACTIVE_STATUS, status));
			}
			if (!Objects.isEmpty(orderBy)) {
				if (sort != null && sort == Sort.DESC) {
					criteria.addOrder(Order.desc(orderBy));
				} else {
					criteria.addOrder(Order.asc(orderBy));
				}
			}
			list = criteria.list();
		}
		if (list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	/**
	 * Find all ids for all user.
	 *
	 * @param activeStatus
	 *            the active status
	 * @return the list
	 */
	public List<Integer> findAllIdsForAllUser(final Status activeStatus) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(User.class);
		if (!Objects.isEmpty(activeStatus)) {
			criteria.add(Restrictions.eq(ACTIVE_STATUS, activeStatus));
		}
		criteria.setProjection(Projections.property("id"));
		List<Integer> list = criteria.list();
		if (list == null) {
			list = new ArrayList<>();
		}

		return list;
	}

	/**
	 * Find users.
	 *
	 * @param keyword
	 *            the keyword
	 * @param activeStatus
	 *            the active status
	 * @return the list
	 */
	public List<User> findUsers(final String keyword,
			final Status activeStatus) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(User.class);
		if (!Objects.isEmpty(keyword)) {
			criteria.add(Restrictions.or(Restrictions.eq("email", keyword),
					Restrictions.eq("number", keyword),
					Restrictions.eq("pulseId", keyword)));
			if (!Objects.isEmpty(activeStatus)) {
				criteria.add(Restrictions.eq(ACTIVE_STATUS, activeStatus));
			}
			return criteria.list();
		}
		return null;
	}

	/**
	 * Gets the users for location team count.
	 *
	 * @param query
	 *            the query
	 * @return the users for location team count
	 */
	public BigInteger getUsersForLocationTeamCount(final String query) {
		final Query q = getSession().createSQLQuery(query);
		return (BigInteger) q.uniqueResult();
	}

	/**
	 * Gets the users for pulse ID names.
	 *
	 * @return the users for pulse ID names
	 */
	public HashSet<String> getUsersForPulseIDNames() {
		final HashSet<String> processedNameSet = new HashSet<>();
		final Criteria criteria = createCriteria(User.class);
		criteria.add(Restrictions.isNotNull("pulseId"));
		List<User> list = criteria.list();
		if (list == null) {
			list = new ArrayList<>();
		}

		return processedNameSet;
	}

	/**
	 * Find all users.
	 *
	 * @param userIds
	 *            the user ids
	 * @param status
	 *            the status
	 * @return the list
	 */
	@Transactional(readOnly = true)
	public List<User> findAllUsers(final List<Integer> userIds, Status status) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(User.class);
		if (Objects.isEmpty(status)) {
			status = Status.Active;
		}
		if (userIds.size() != 0) {
			criteria.add(Restrictions.in("id", userIds));
			criteria.add(Restrictions.eq(ACTIVE_STATUS, status));
			criteria.addOrder(Order.asc(FIRST_NAME));
		}
		List<User> list = criteria.list();
		if (list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	/**
	 * Gets the users of tenant.
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the users of tenant
	 */
	public List<User> getUsersOfTenant(final String sqlQuery) {
		final SQLQuery query = getSession().createSQLQuery(sqlQuery);
		query.setResultTransformer(Transformers.aliasToBean(User.class));
		// query.addEntity(UserDTO.class);
		final List<User> users = query.list();
		return users;
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the user
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.CachebleCommonHibernateDAO#findById(java.lang.
	 * Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public User findById(final Integer id) {
		final Criteria criteria = createCriteria(User.class);
		criteria.add(Restrictions.eq("id", id));
		criteria.setCacheable(true);
		return (User) criteria.uniqueResult();
	}

	/**
	 * Fetch non fpg members.
	 *
	 * @param locationId
	 *            the location id
	 * @return the list
	 */
	public List<User> fetchNonFpgMembers(final Integer locationId) {
		// TODO Auto-generated method stub
		final String sqlString = "SELECT user_location_group.USER_ID"
				+ " FROM user_location_group " + "JOIN "
				+ "location_group ON (user_location_group.LOCATION_GROUP_ID = location_group.ID) "
				+ "JOIN "
				+ "location_group_type ON (location_group_type.id = location_group.group_type_id AND is_default=true) "
				+ "JOIN " + "user ON (user.ID = user_location_group.USER_ID) "
				+ "JOIN " + "user_profile ON (user_profile.ID = user.ID) "
				+ "WHERE "
				+ "location_group.TENANT_LOCATION_ID =:tenantLocationId "
				// + "AND user_location_group.is_assigned=true "
				+ "AND user.ACTIVE_STATUS = 0 "
				+ "AND user_profile.IS_FPG_MEMBER = 0 "
				+ "AND location_group.ACTIVE_STATUS = 0";
		final SQLQuery query = getSession().createSQLQuery(sqlString);
		query.setParameter("tenantLocationId", locationId);
		final List<Integer> userList = query.list();
		final Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.in("id", userList));
		criteria.addOrder(Order.asc(FIRST_NAME));
		return criteria.list();
	}

	/**
	 * Find by user val.
	 *
	 * @param userVal
	 *            the user val
	 * @return the user
	 */
	public User findByUserVal(final String userVal) {
		final Criteria criteria = createCriteria(User.class);
		criteria.add(Restrictions.eq("userVal", userVal));
		final List<User> list = criteria.list();
		if ((list != null && list.size() > 0)) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * Gets the users.
	 *
	 * @param isSuperAdmin
	 *            the is super admin
	 * @param getAccessibleRoleUsersOnly
	 *            the get accessible role users only
	 * @param status
	 *            the status
	 * @param roleIds
	 *            the role ids
	 * @param tenantLocationIds
	 *            the tenant location ids
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param searchKeyword
	 *            the search keyword
	 * @param getRoleForTenantId
	 *            the get role for tenant id
	 * @param totalCountQuery
	 *            the total count query
	 * @param listQuery
	 *            the list query
	 * @return the users
	 */
	public HashMap<String, Object> getUsers(final Boolean isSuperAdmin,
			final Boolean getAccessibleRoleUsersOnly, final Status status,
			final List<Integer> roleIds, final List<Integer> tenantLocationIds,
			final Integer first, final Integer total, final String orderBy,
			final Sort sort, final String searchKeyword,
			final Integer getRoleForTenantId, final String totalCountQuery,
			final String listQuery) {

		final HashMap<String, Object> map = new HashMap<>();

		Double totalCount = 0d;
		List<UserPlain> users = null;
		if (!isSuperAdmin && (Objects.isEmptyCollection(tenantLocationIds)
				|| (getAccessibleRoleUsersOnly
						&& Objects.isEmptyCollection(roleIds)))) {
			users = new ArrayList<>();
			if (!Objects.isEmpty(listQuery)) {
				final Query query1 = getSession().createSQLQuery(listQuery)
						.addEntity(UserPlain.class);
				users = query1.list();
			}
		} else {
			if (!Objects.isEmpty(totalCountQuery)) {
				totalCount = getCount(totalCountQuery);
			}
			if (!Objects.isEmpty(listQuery)) {
				final Query query1 = getSession().createSQLQuery(listQuery)
						.addEntity(UserPlain.class);
				users = query1.list();
			}
		}
		map.put(SystemConfigs.DAO_SEARCH_RESULTS_TOTAL_COUNT, totalCount);
		map.put(SystemConfigs.DAO_SEARCH_RESULTS, users);
		return map;
	}

	/**
	 * Gets the count.
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the count
	 */
	public Double getCount(final String sqlQuery) {
		final SQLQuery query = getSession().createSQLQuery(sqlQuery);
		final Number result = ((Number) query.uniqueResult());
		if (result != null) {
			return result.doubleValue();
		} else {
			return 0d;
		}
	}

	/**
	 * Gets the user based on permission and location access.
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the user based on permission and location access
	 */
	public List<User> getUserBasedOnPermissionAndLocationAccess(
			final String sqlQuery) {
		final SQLQuery query = getSession().createSQLQuery(sqlQuery);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> list = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			list.add(ObjectUtils.mapToPojo(map, User.class));
		}
		return list;
	}

	/**
	 * Gets the user list to email for pwd expire.
	 *
	 * @param passwordExpireDays
	 *            the password expire days
	 * @return the user list to email for pwd expire
	 */
	public List<User> getUserListToEmailForPwdExpire(
			final String passwordExpireDays) {
		final StringBuilder sqlQuery = new StringBuilder(
				"select SQL_NO_CACHE u.id,u.email,up.first_name as firstName,up.last_name as lastName, "
						+ "u.DEFAULT_INDUSTRY as defaultIndustry from user u inner join user_profile up on up.id=u.id "
						+ " where "
						+ " DATE_ADD(lastPasswordChangeDate,INTERVAL "
						+ (Integer.parseInt(passwordExpireDays) + 1)
						+ " DAY) = curdate();");
		final SQLQuery query = getSession().createSQLQuery(sqlQuery.toString());
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> list = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			list.add(ObjectUtils.mapToPojo(map, User.class));
		}
		return list;
	}

	/**
	 * Gets the users to notify before password expire.
	 *
	 * @param daysForbeforeNotifym
	 *            the days forbefore notifym
	 * @param passwordExpireDays
	 *            the password expire days
	 * @return the users to notify before password expire
	 */
	public List<User> getUsersToNotifyBeforePasswordExpire(
			final String daysForbeforeNotifym,
			final String passwordExpireDays) {
		final String sqlQuery = "select u.id,u.email,up.first_name,up.last_name,u.lastPasswordChangeDate,u.DEFAULT_INDUSTRY as defaultIndustry from user u inner join user_profile up on up.id=u.id where "
				+ "DATE_SUB(DATE_ADD(lastPasswordChangeDate,INTERVAL "
				+ Integer.parseInt(passwordExpireDays) + " DAY), INTERVAL "
				+ Integer.parseInt(daysForbeforeNotifym)
				+ " DAY) <= curdate() and "
				+ "DATE_ADD(lastPasswordChangeDate,INTERVAL "
				+ Integer.parseInt(passwordExpireDays) + " DAY) >= curdate();";
		final SQLQuery query = getSession().createSQLQuery(sqlQuery);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> list = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			list.add(ObjectUtils.mapToPojo(map, User.class));
		}
		return list;
	}

	/**
	 * Find user count.
	 *
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the integer
	 */
	public Long findUserCount(final String propertyName,
			final Object propertyValue) {
		final Criteria criteria1 = createCriteria(User.class);
		criteria1.add(Restrictions.eq("betaTester", true))
				.setProjection(Projections.count("id"));
		return (Long) criteria1.uniqueResult();
	}

	/**
	 * Gets the users ids of onboard location.
	 *
	 * @param industryID
	 *            the industry ID
	 * @param tenantId
	 *            the tenant id
	 * @param tenantLocationId
	 *            the tenant location id
	 * @return the users ids of onboard location
	 */
	public List<User> getUsersIdsOfOnboardLocation(final Integer industryID,
			final Integer tenantId, final Integer tenantLocationId) {

		final String sqlString = "SELECT"
				+ " u.id,u.email,u.userVal as userVal,u.PASSWORD as password,u.CREATED_ON as createdOn,u.UPDATED_ON as updatedOn,u.CREATED_BY as createdBy,u.UPDATED_BY as updatedBy, up.NUMBER as number, up.FIRST_NAME as firstName, up.MIDDLE_NAME as middleName, up.LAST_NAME as lastName, up.PHONE_NUMBER as phoneNumber, up.MOBILE_NUMBER as mobileNumber, up.HIRED_DATE as hiredDate, up.FPG_LEVEL as fpgLevel, up.JOB_TITLE as jobTitle, up.LINKEDIN_ID as linkedinId, up.LINKEDIN_IMAGE as linkedinImage, up.BIRTH_MONTH as birthMonth, up.BIRTH_DAY as birthDay, up.PULSE_ID as pulseId, up.IS_FPG_MEMBER as isFpgMember, up.AUTHY_ID as authyId, up.IS_AUTHY_ATTEMPTED as isAuthyAttempted, up.COUNTRY_CODE as countryCode, up.DESIRED_LOCATION as desiredLocation, up.IOS_VERSION as iosVersion, up.ANDROID_VERSION as androidVersion, up.IS_FIRST_LOGIN as isFirstLogin, up.WORKINGTYPE as workingType, up.BLOCK_EVENT as blockEvent, up.LAST_ACTIVITY_DONE_ON as lastActivityDoneOn, up.DEPARTMENT as departMent, up.MASTER_LOCATION as masterLocation, up.SPEAKS as speaks, up.HOBBIES as hobbies, up.IS_BETA_TESTER as isBetaTester "
				+ "FROM " + "(SELECT DISTINCT " + "USER_ID " + "FROM "
				+ "onboarding_wizard_step_permission owsp "
				+ "INNER JOIN tenant_location tl ON tl.id = owsp.TENANT_LOCATION_ID "
				+ " AND tl.id = :tenantLocationId) x " + "LEFT JOIN"
				+ "(SELECT DISTINCT " + "USER_ID " + "FROM "
				+ "onboarding_wizard_step_permission owsp "
				+ "INNER JOIN tenant_location tl ON tl.id = owsp.TENANT_LOCATION_ID "
				+ " AND (tl.ACTIVE_STATUS = 8 or tl.ACTIVE_STATUS = 7) "
				+ " AND tl.id != :tenantLocationId) y ON x.user_id = y.user_id "
				+ "INNER JOIN "
				+ "user u ON u.id = x.user_id and u.ACTIVE_STATUS=7 "
				+ "INNER join user_profile  up on up.id = x.user_id " + "WHERE "
				+ "y.user_id IS NULL";

		final SQLQuery query = getSession().createSQLQuery(sqlString);

		if (Objects.isNotNull(tenantLocationId)) {
			query.setParameter("tenantLocationId", tenantLocationId);
		}
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> list = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			list.add(ObjectUtils.mapToPojo(map, User.class));
		}
		return list;

	}

	/**
	 * Gets the location wise users.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @return the location wise users
	 */
	public List<User> getLocationWiseUsers(final Integer tenantLocationId) {
		final String sqlString = "SELECT"
				+ " u.id,u.email,u.userVal as userVal,u.PASSWORD as password,u.CREATED_ON as createdOn,u.UPDATED_ON as updatedOn,u.CREATED_BY as createdBy,u.UPDATED_BY as updatedBy, up.NUMBER as number, up.FIRST_NAME as firstName, up.MIDDLE_NAME as middleName, up.LAST_NAME as lastName, up.PHONE_NUMBER as phoneNumber, up.MOBILE_NUMBER as mobileNumber, up.HIRED_DATE as hiredDate, up.FPG_LEVEL as fpgLevel, up.JOB_TITLE as jobTitle, up.LINKEDIN_ID as linkedinId, up.LINKEDIN_IMAGE as linkedinImage, up.BIRTH_MONTH as birthMonth, up.BIRTH_DAY as birthDay, up.PULSE_ID as pulseId, up.IS_FPG_MEMBER as isFpgMember, up.AUTHY_ID as authyId, up.IS_AUTHY_ATTEMPTED as isAuthyAttempted, up.COUNTRY_CODE as countryCode, up.DESIRED_LOCATION as desiredLocation, up.IOS_VERSION as iosVersion, up.ANDROID_VERSION as androidVersion, up.IS_FIRST_LOGIN as isFirstLogin, up.WORKINGTYPE as workingType, up.BLOCK_EVENT as blockEvent, up.LAST_ACTIVITY_DONE_ON as lastActivityDoneOn, up.DEPARTMENT as departMent, up.MASTER_LOCATION as masterLocation, up.SPEAKS as speaks, up.HOBBIES as hobbies, up.IS_BETA_TESTER as isBetaTester "
				+ "FROM " + "(SELECT DISTINCT " + "USER_ID " + "FROM "
				+ "onboarding_wizard_step_permission owsp "
				+ "INNER JOIN tenant_location tl ON tl.id = owsp.TENANT_LOCATION_ID "
				+ "AND tl.id = :tenantLocationId)x inner join user u on u.id = x.user_id inner join user_profile up on up.id = x.user_id;";

		final SQLQuery query = getSession().createSQLQuery(sqlString);

		if (Objects.isNotNull(tenantLocationId)) {
			query.setParameter("tenantLocationId", tenantLocationId);
		}
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		final List<Map<String, Object>> maps = query.list();
		final List<User> list = new ArrayList<>();
		for (final Map<String, Object> map : maps) {
			list.add(ObjectUtils.mapToPojo(map, User.class));
		}
		return list;
	}
}