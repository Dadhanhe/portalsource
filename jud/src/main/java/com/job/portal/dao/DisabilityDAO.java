/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.Disability;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardDAO.
 */
@Component
public class DisabilityDAO extends CachebleCommonHibernateDAO<Disability> {

	/**
	 * Gets the all education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the all education board
	 */
	public List<Disability> getAllDisability() {
		final Criteria criteria = createCriteria(Disability.class);
		final List<Disability> list = criteria.list();
		if (!list.isEmpty()) {
			return list;
		} else {
			final List<Disability> list1 = new ArrayList<>();
			return list1;
		}

	}

	public Disability findBySpecific(final Integer id) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(Disability.class);
		criteria.add(Restrictions.eq("id", id));
		final List<Disability> list = criteria.list();
		final Disability edu = list.get(0);
		return edu;
	}

}
