/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.Country;

/**
 * @author Vaibhav
 *
 */
@Component
public class CountryDAO extends CachebleCommonHibernateDAO<Country> {

	public CountryDAO() {
		super();
		setEntityClass(Country.class);
	}
}
