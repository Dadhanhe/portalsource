/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.State;

/**
 * @author Vaibhav
 *
 */
@Component
public class StateDAO extends CachebleCommonHibernateDAO<State> {

	public StateDAO() {
		super();
		setEntityClass(State.class);
	}

	@SuppressWarnings("unchecked")
	public State getStateNameByCountry(final String stateName,
			final String countryName) {
		final Criteria criteria = createCriteria(getEntityClass());
		criteria.createAlias("country", "co");
		criteria.add(Restrictions.eq("name", stateName));
		criteria.add(Restrictions.eq("co.name", countryName));
		final List<State> states = criteria.list();
		if (!states.isEmpty()) {
			return states.get(0);
		} else {
			return null;
		}
	}

}
