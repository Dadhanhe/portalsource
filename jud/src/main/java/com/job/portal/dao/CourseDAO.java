/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.Course;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardDAO.
 */
@Component
public class CourseDAO extends CachebleCommonHibernateDAO<Course> {

	/**
	 * Gets the all education board.
	 *
	 * @return the all education board
	 */
	public List<Course> getAllCourse() {
		final Criteria criteria = createCriteria(Course.class);
		final List<Course> list = criteria.list();
		if (!list.isEmpty()) {
			return list;
		} else {
			final List<Course> list1 = new ArrayList<>();
			return list1;
		}

	}

	/**
	 * Find by specific.
	 *
	 * @param id
	 *            the id
	 * @return the course
	 */
	public Course findBySpecific(final Integer id) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(Course.class);
		criteria.add(Restrictions.eq("id", id));
		final List<Course> list = criteria.list();
		final Course uni = list.get(0);
		return uni;
	}

}
