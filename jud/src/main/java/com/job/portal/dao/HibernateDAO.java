/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class HibernateDAO.
 *
 * @param <E>
 *            the element type
 * @param <K>
 *            the key type
 */
@SuppressWarnings("unchecked")
@Validated
public abstract class HibernateDAO<E, K extends Serializable>
		implements GenericDAO<E, K> {

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/** The dao type. */
	protected Class<? extends E> daoType;

	/**
	 * Instantiates a new hibernate DAO.
	 */
	public HibernateDAO() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		daoType = (Class) pt.getActualTypeArguments()[0];
	}

	/**
	 * Current session.
	 *
	 * @return the session
	 */
	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#add(java.lang.Object)
	 */
	public void add(E entity) {
		currentSession().save(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#saveOrUpdate(java.lang.Object)
	 */
	public void saveOrUpdate(E entity) {
		currentSession().saveOrUpdate(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#update(java.lang.Object)
	 */
	public void update(E entity) {
		currentSession().saveOrUpdate(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#remove(java.lang.Object)
	 */
	public void remove(E entity) {
		currentSession().delete(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#find(java.lang.Object)
	 */
	public E find(K key) {
		return (E) currentSession().get(daoType, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#getAll()
	 */
	public List<E> getAll() {
		return currentSession().createCriteria(daoType).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.dao.GenericDAO#listByProperty(java.lang.String,
	 * java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List listByProperty(final String propertyName,
			final Object propertyValue) {
		final Criteria criteria = this.createCriteria(daoType);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		return criteria.list();
	}

	/**
	 * Creates the criteria.
	 *
	 * @param daoType
	 *            the dao type
	 * @return the criteria
	 */
	protected Criteria createCriteria(Class<? extends E> daoType) {
		return currentSession().createCriteria(daoType);
	}
}
