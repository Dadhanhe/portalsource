/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */

package com.job.portal.dao.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;
import com.job.portal.utils.SystemConfigs;

// TODO: Auto-generated Javadoc
/**
 * The Class CommonHibernateDAO.
 *
 * @param <C>
 *            the generic type
 */
@SuppressWarnings("rawtypes")
public class CommonHibernateDAO<C extends AbstractValueObject>
		extends AbstractHibernateDAO implements IHibernateDAO<C> {

	/** The Constant BLANK_SPACE. */
	private static final String BLANK_SPACE = "";

	/** The entity class. */
	private Class entityClass;

	/**
	 * Gets the entity class.
	 *
	 * @return the entity class
	 */
	public Class getEntityClass() {
		return this.entityClass;
	}

	/**
	 * Sets the entity class.
	 *
	 * @param entityClass
	 *            the new entity class
	 */
	public void setEntityClass(final Class entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Save.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#save(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public C save(final C object) {
		getSession().save(object);
		return object;
	}

	/**
	 * Update.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#update(com.job.portal.pojo.
	 * common. AbstractValueObject)
	 */
	@Override
	public C update(final C object) {
		getSession().update(object);
		return object;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#merge(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public C merge(final C object) {
		getSession().merge(object);
		return object;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#saveOrUpdate(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public C saveOrUpdate(final C object) {
		getSession().saveOrUpdate(object);
		return object;
	}

	/**
	 * Delete.
	 *
	 * @param object
	 *            the object
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#delete(com.job.portal.pojo.
	 * common. AbstractValueObject)
	 */
	@Override
	public void delete(final C object) {
		object.setActiveStatus(Status.Archive);
		getSession().update(object);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findById(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public C findById(final Integer id) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq("id", id));
		return (C) criteria.uniqueResult();
	}

	/**
	 * Find by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#findByProperty(java.lang.String,
	 * java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public C findByProperty(final String propertyName,
			final String propertyValue) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		return (C) criteria.uniqueResult();
	}

	/**
	 * List by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the list
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#listByProperty(java.lang.String,
	 * java.lang.Object)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public List listByProperty(final String propertyName,
			final String propertyValue) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		return criteria.list();
	}

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the list
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public List findAll(final Integer first, final Integer total,
			final String search, final Status status) {
		return findAll(first, total, search, null, null, status);
	}

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param status
	 *            the status
	 * @return the list
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, java.lang.String,
	 * com.job.portal.pojo.enums.Sort)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public List findAll(final Integer first, final Integer total,
			final String search, final String[] orderBy, final Sort[] sort,
			final Status status) {
		final Criteria criteria = createCriteria(this.entityClass);
		final List aliases = new ArrayList();
		if (first != null && total != null) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
		if (!Objects.isEmpty(search)) {
			criteria.add(getSearchRestrictions(search));
		}
		if (orderBy != null && orderBy.length > 0) {
			for (int index = 0; index < orderBy.length; index++) {
				final String sortBy = orderBy[index];
				if (sortBy.contains(".")) {
					final String[] s = sortBy.split("\\.");
					if (!aliases.contains(s[0])) {
						aliases.add(s[0]);
						criteria.createAlias(s[0], s[0]);
					}
				}
				Sort order = Sort.ASC;
				if (sort != null && sort.length >= index + 1) {
					order = Sort.DESC == sort[index] ? Sort.DESC : Sort.ASC;
				}
				if (order == Sort.DESC) {
					criteria.addOrder(Order.desc(sortBy));
				} else {
					criteria.addOrder(Order.asc(sortBy));
				}
			}
		}
		if (!Objects.isEmpty(status)) {
			criteria.add(Restrictions.eq("activeStatus", status));
		}
		List list = criteria.list();
		if (list == null) {
			list = new ArrayList();
		}
		return list;
	}

	/**
	 * Gets the search restrictions.
	 *
	 * @param search
	 *            the search
	 * @return the search restrictions
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#getSearchRestrictions(java.lang.
	 * String)
	 */
	@Override
	public Criterion getSearchRestrictions(final String search) {
		final Disjunction disjunction = Restrictions.disjunction();
		if (!Objects.isEmpty(search)) {
			final String keyword = search.trim();
			final String[] searchWords = ObjectUtils.getSearchWords(keyword);
			final MatchMode mode = ObjectUtils.isExactWord(keyword)
					? MatchMode.EXACT : MatchMode.ANYWHERE;
			for (final String word : searchWords) {
				disjunction.add(Restrictions.ilike("name", word, mode));
				if (StringUtils.isNumeric(word)) {
					disjunction
							.add(Restrictions.eq("id", Integer.valueOf(word)));
				}
			}
		}
		return disjunction;
	}

	/**
	 * Find total.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the long
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findTotal(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long findTotal(final String search, final Status status) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.setProjection(Projections.rowCount());
		if (!Objects.isEmpty(status)) {
			criteria.add(Restrictions.eq("activeStatus", status));
		}
		if (search != null && !search.trim().equals(BLANK_SPACE)) {
			criteria.add(getSearchRestrictions(search));
		}
		return (Long) criteria.uniqueResult();
	}

	/**
	 * Commit.
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#commit()
	 */
	@Override
	public void commit() {
		getSession().getTransaction().commit();
	}

	/**
	 * Gets the query data.
	 *
	 * @param queryStr
	 *            the query str
	 * @return the query data
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getQueryData(final String queryStr) {
		final SQLQuery query = getSession().createSQLQuery(queryStr);
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return query.list();
	}

	/**
	 * Hard delete.
	 *
	 * @param object
	 *            the object
	 */
	public void hardDelete(final C object) {
		getSession().delete(object);
	}

	/**
	 * Find all data with count.
	 *
	 * @param criteria
	 *            the criteria
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param columnName
	 *            the column name
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findAllDataWithCount(final Criteria criteria,
			final Integer first, final Integer total, final String columnName) {
		// to get total count
		final Projection idCountProjection = Projections
				.countDistinct(columnName);
		criteria.setFirstResult(0);
		criteria.setMaxResults(0);
		criteria.setProjection(idCountProjection);
		final int totalResultCount = ((Long) criteria.uniqueResult())
				.intValue();
		// to fetch records of selected page
		criteria.setProjection(
				Projections.distinct(Projections.property(columnName)));
		if (Objects.isNotNull(first)) {
			criteria.setFirstResult(first);
		}
		if (Objects.isNotNull(total)) {
			criteria.setMaxResults(total);
		}
		final List<C> uniqueSubList = criteria.list();

		// To fetch actual records
		criteria.setProjection(null);
		criteria.setFirstResult(0);
		criteria.setMaxResults(Integer.MAX_VALUE);
		if (!Objects.isEmpty(uniqueSubList)) {
			criteria.add(Restrictions.in(columnName, uniqueSubList));
		}
		criteria.setResultTransformer(
				CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<C> list = criteria.list();
		if (list == null) {
			list = new ArrayList<>();
		}
		final Map<String, Object> searchResultsMap = new HashMap<>();
		searchResultsMap.put(SystemConfigs.DAO_SEARCH_RESULTS, list);
		searchResultsMap.put(SystemConfigs.DAO_SEARCH_RESULTS_TOTAL_COUNT,
				totalResultCount);
		return searchResultsMap;
	}

	/**
	 * Adds the equal criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param val
	 *            the val
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualCriteria(final String valText, final Integer val,
			final Criteria criteria) {
		if (!Objects.isEmpty(val)) {
			criteria.add(Restrictions.eq(valText, val));
		}
	}

	/**
	 * Adds the equal criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param val
	 *            the val
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualCriteria(final String valText, final String val,
			final Criteria criteria) {
		if (!Objects.isEmpty(val)) {
			criteria.add(Restrictions.eq(valText, val));
		}
	}

	/**
	 * Adds the equal criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param val
	 *            the val
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualCriteria(final String valText, final Boolean val,
			final Criteria criteria) {
		if (!Objects.isEmpty(val)) {
			criteria.add(Restrictions.eq(valText, val));
		}
	}

	/**
	 * Adds the equal criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param valueIds
	 *            the value ids
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualCriteria(final String valText,
			final List<Integer> valueIds, final Criteria criteria) {
		if (!Objects.isEmpty(valueIds)) {
			criteria.add(Restrictions.in(valText, valueIds));
		}
	}

	/**
	 * Adds the equal or less criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param toDate
	 *            the to date
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualOrLessCriteria(final String valText,
			final Date toDate, final Criteria criteria) {
		if (Objects.isNotNull(toDate)) {
			criteria.add(Restrictions.le(valText, toDate));
		}
	}

	/**
	 * Adds the equal or greater criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param toDate
	 *            the to date
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualOrGreaterCriteria(final String valText,
			final Date toDate, final Criteria criteria) {
		if (Objects.isNotNull(toDate)) {
			criteria.add(Restrictions.ge(valText, toDate));
		}
	}

	/**
	 * Adds the equal status criteria.
	 *
	 * @param valText
	 *            the val text
	 * @param status
	 *            the status
	 * @param criteria
	 *            the criteria
	 */
	protected void addEqualStatusCriteria(final String valText,
			final String status, final Criteria criteria) {
		if (!Objects.isEmpty(status)) {
			final List<String> list = Objects.convertToArrayList(status);
			final List<Status> statusList = new ArrayList<>();
			Objects.stream(list)
					.forEach(sData -> statusList.add(Status.valueOf(sData)));
			if (!Objects.isEmptyCollection(list)) {
				criteria.add(Restrictions.in(valText, statusList));
			}
		}
	}

	/**
	 * Adds the pagination criteria.
	 *
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param criteria
	 *            the criteria
	 */
	protected void addPaginationCriteria(final Integer first,
			final Integer total, final Criteria criteria) {
		if (first != null && total != null) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
	}

	/**
	 * Adds the sort order criteria.
	 *
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param criteria
	 *            the criteria
	 */
	protected void addSortOrderCriteria(final String orderBy, final Sort sort,
			final Criteria criteria) {
		if (!Objects.isEmpty(orderBy)) {
			if (sort != null && sort == Sort.DESC) {
				criteria.addOrder(Order.desc(orderBy));
			} else {
				criteria.addOrder(Order.asc(orderBy));
			}
		}
	}

	/**
	 * Gets the criteria row count.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the criteria row count
	 */
	protected Long getCriteriaRowCount(final Criteria criteria) {
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
}
