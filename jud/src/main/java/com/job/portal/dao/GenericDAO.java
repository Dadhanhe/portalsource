/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.dao;

import javax.validation.Valid;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Interface GenericDAO.
 *
 * @param <E>
 *            the element type
 * @param <K>
 *            the key type
 */
public interface GenericDAO<E, K> {

	/**
	 * Adds the.
	 *
	 * @param entity
	 *            the entity
	 */
	public void add(@Valid E entity);

	/**
	 * Save or update.
	 *
	 * @param entity
	 *            the entity
	 */
	public void saveOrUpdate(E entity);

	/**
	 * Update.
	 *
	 * @param entity
	 *            the entity
	 */
	public void update(E entity);

	/**
	 * Removes the.
	 *
	 * @param entity
	 *            the entity
	 */
	public void remove(E entity);

	/**
	 * Find.
	 *
	 * @param key
	 *            the key
	 * @return the e
	 */
	public E find(K key);

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<E> getAll();

	/**
	 * List by property.
	 *
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the list
	 */
	public List<E> listByProperty(String propertyName, Object propertyValue);
}
