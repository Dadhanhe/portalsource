/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */

package com.job.portal.dao.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class CachebleCommonHibernateDAO.
 *
 * @param <C>
 *            the generic type
 */
@SuppressWarnings("rawtypes")
public class CachebleCommonHibernateDAO<C extends AbstractValueObject>
		extends AbstractHibernateDAO implements IHibernateDAO<C> {

	/** The Constant BLANK_SPACE. */
	private static final String BLANK_SPACE = "";

	/** The entity class. */
	private Class entityClass;

	/**
	 * Gets the entity class.
	 *
	 * @return the entity class
	 */
	public Class getEntityClass() {
		return this.entityClass;
	}

	/**
	 * Sets the entity class.
	 *
	 * @param entityClass
	 *            the new entity class
	 */
	public void setEntityClass(final Class entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Save.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#save(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public C save(final C object) {
		getSession().save(object);
		return object;
	}

	/**
	 * Update.
	 *
	 * @param object
	 *            the object
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#update(com.job.portal.pojo.
	 * common. AbstractValueObject)
	 */
	@Override
	public C update(final C object) {
		getSession().update(object);
		return object;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#merge(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public C merge(final C object) {
		getSession().merge(object);
		return object;
	}

	/**
	 * Delete.
	 *
	 * @param object
	 *            the object
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#delete(com.job.portal.pojo.
	 * common. AbstractValueObject)
	 */
	@Override
	public void delete(final C object) {
		object.setActiveStatus(Status.Archive);
		getSession().update(object);
	}

	/**
	 * Hard delete.
	 *
	 * @param object
	 *            the object
	 */
	public void hardDelete(final C object) {
		getSession().delete(object);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findById(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public C findById(final Integer id) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq("id", id));
		criteria.setCacheable(true);
		return (C) criteria.uniqueResult();
	}

	/**
	 * Find by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the c
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#findByProperty(java.lang.String,
	 * java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public C findByProperty(final String propertyName,
			final String propertyValue) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		criteria.setCacheable(true);
		return (C) criteria.uniqueResult();
	}

	/**
	 * List by property.
	 *
	 * @param industryId
	 *            the industry id
	 * @param propertyName
	 *            the property name
	 * @param propertyValue
	 *            the property value
	 * @return the list
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#listByProperty(java.lang.String,
	 * java.lang.Object)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public List listByProperty(final String propertyName,
			final String propertyValue) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.add(Restrictions.eq(propertyName, propertyValue));
		criteria.setCacheable(true);
		return criteria.list();
	}

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the list
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public List findAll(final Integer first, final Integer total,
			final String search, final Status status) {
		return findAll(first, total, search, null, null, status);
	}

	/**
	 * Gets the search restrictions.
	 *
	 * @param search
	 *            the search
	 * @return the search restrictions
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#getSearchRestrictions(java.lang.
	 * String)
	 */
	@Override
	public Criterion getSearchRestrictions(final String search) {
		final Disjunction disjunction = Restrictions.disjunction();
		if (!Objects.isEmpty(search)) {
			final String keyword = search.trim();
			final String[] searchWords = ObjectUtils.getSearchWords(keyword);
			final MatchMode mode = ObjectUtils.isExactWord(keyword)
					? MatchMode.EXACT : MatchMode.ANYWHERE;
			for (final String word : searchWords) {
				disjunction.add(Restrictions.ilike("name", word, mode));
				if (StringUtils.isNumeric(word)) {
					disjunction
							.add(Restrictions.eq("id", Integer.valueOf(word)));
				}
			}
		}
		return disjunction;
	}

	/**
	 * Find total.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the long
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findTotal(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long findTotal(final String search, final Status status) {
		final Criteria criteria = createCriteria(this.entityClass);
		criteria.setProjection(Projections.rowCount());
		if (status != null) {
			criteria.add(Restrictions.eq("activeStatus", status));
		}
		if (search != null && !search.trim().equals(BLANK_SPACE)) {
			criteria.add(getSearchRestrictions(search));
		}
		criteria.setCacheable(true);
		return (Long) criteria.uniqueResult();
	}

	/**
	 * Commit.
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#commit()
	 */
	@Override
	public void commit() {
		getSession().getTransaction().commit();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.dao.common.IHibernateDAO#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String, java.lang.String, com.job.portal.pojo.enums.Sort,
	 * com.job.portal.pojo.enums.Status)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<C> findAll(final Integer first, final Integer total,
			final String search, final String[] orderBy, final Sort[] sort,
			final Status status) {
		final Criteria criteria = createCriteria(this.entityClass);
		final List aliases = new ArrayList();
		if (first != null && total != null) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
		if (!Objects.isEmpty(status)) {
			criteria.add(Restrictions.eq("activeStatus", status));
		}
		if (!Objects.isEmpty(search)) {
			criteria.add(getSearchRestrictions(search));
		}
		if (orderBy != null && orderBy.length > 0) {
			for (int index = 0; index < orderBy.length; index++) {
				final String sortBy = orderBy[index];
				if (sortBy.contains(".")) {
					final String[] s = sortBy.split("\\.");
					if (!aliases.contains(s[0])) {
						aliases.add(s[0]);
						criteria.createAlias(s[0], s[0]);
					}
				}
				Sort order = Sort.ASC;
				if (sort != null && sort.length >= index + 1) {
					order = Sort.DESC == sort[index] ? Sort.DESC : Sort.ASC;
				}
				if (order == Sort.DESC) {
					criteria.addOrder(Order.desc(sortBy));
				} else {
					criteria.addOrder(Order.asc(sortBy));
				}
			}
		}
		criteria.setCacheable(true);
		List<C> list = criteria.list();
		if (list == null) {
			list = new ArrayList<C>();
		}
		return list;
	}

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param status
	 *            the status
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<C> findAll(final Integer industryId, final Integer tenantId,
			final Integer tenantLocationId, final Integer first,
			final Integer total, final String search, final String orderBy,
			final Sort sort, final Status status, final Date startDate,
			final Date endDate) {
		// TODO Auto-generated method stub
		final Criteria criteria = createCriteria(this.entityClass);
		if (!Objects.isEmpty(first) && !Objects.isEmpty(total)) {
			criteria.setFirstResult(first);
			criteria.setMaxResults(total);
		}
		if (!Objects.isEmpty(startDate) && !Objects.isEmpty(endDate)) {
			criteria.add(
					Restrictions.between("metricDate", startDate, endDate));
		}
		if (!Objects.isEmpty(industryId)) {
			criteria.add(Restrictions.eq("industryId", industryId));
		}
		if (!Objects.isEmpty(tenantId)) {
			criteria.createAlias("tenant", "t");
			criteria.add(Restrictions.eq("t.id", tenantId));
		}
		if (!Objects.isEmpty(tenantLocationId)) {
			criteria.createAlias("tenantLocation", "tl");
			criteria.add(Restrictions.eq("tl.id", tenantLocationId));
		}
		if (!Objects.isEmpty(status)) {
			criteria.add(Restrictions.eq("activeStatus", status));
		}
		if (!Objects.isEmpty(search)) {
			criteria.add(getSearchRestrictions(search));
		}
		if (!Objects.isEmpty(orderBy)) {
			if (sort != null && sort == Sort.DESC) {
				criteria.addOrder(Order.desc(orderBy));
			} else {
				criteria.addOrder(Order.asc(orderBy));
			}
		}
		criteria.setCacheable(true);
		List<C> list = criteria.list();
		if (list == null) {
			list = new ArrayList<C>();
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.dao.common.IHibernateDAO#saveOrUpdate(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public C saveOrUpdate(final C object) {
		// TODO Auto-generated method stub
		return null;
	}
}
