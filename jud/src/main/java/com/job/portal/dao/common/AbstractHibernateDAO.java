/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.dao.common;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractHibernateDAO.
 */
public abstract class AbstractHibernateDAO {

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	protected Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	/**
	 * Creates the criteria.
	 *
	 * @param entityClass
	 *            the entity class
	 * @return the criteria
	 */
	protected Criteria createCriteria(
			final Class<? extends AbstractValueObject> entityClass) {
		return getSession().createCriteria(entityClass);
	}

	/**
	 * Creates the criteria.
	 *
	 * @param entityClass
	 *            the entity class
	 * @param alias
	 *            the alias
	 * @return the criteria
	 */
	protected Criteria createCriteria(
			final Class<? extends AbstractValueObject> entityClass,
			final String alias) {
		return getSession().createCriteria(entityClass, alias);
	}

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	@Autowired
	public void setSessionFactory(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List listByProperty(final String propertyName,
			final Object propertyValue) {
		// TODO Auto-generated method stub
		return null;
	}

}
