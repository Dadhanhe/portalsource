/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.Subindustry;

// TODO: Auto-generated Javadoc
/**
 * The Class IndustryDAO.
 */
@SuppressWarnings("unchecked")
@Component
public class SubindustryDAO extends CachebleCommonHibernateDAO<Subindustry> {

	/**
	 * Instantiates a new industry DAO.
	 */
	public SubindustryDAO() {
		super();
		setEntityClass(Subindustry.class);
	}

	/**
	 * Gets the all education board.
	 *
	 * @param industryId
	 *
	 * @return the all education board
	 */
	public List<Subindustry> getAllSubIndustry(final Integer industryId) {
		final Criteria criteria = createCriteria(Subindustry.class);
		criteria.add(Restrictions.eq("industry.id", industryId));
		final List<Subindustry> list = criteria.list();
		if (!list.isEmpty()) {
			return list;
		} else {
			final List<Subindustry> list1 = new ArrayList<Subindustry>();
			return list1;
		}

	}

	/**
	 * Find by specific.
	 *
	 * @param id
	 *            the id
	 * @return the industry
	 */
	public Subindustry findBySpecific(final Integer id) {
		final Criteria criteria = createCriteria(Subindustry.class);
		criteria.add(Restrictions.eq("id", id));
		final List<Subindustry> list = criteria.list();
		final Subindustry ini = list.get(0);
		return ini;
	}

}