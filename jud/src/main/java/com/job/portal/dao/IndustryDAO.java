/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.job.portal.dao.common.CachebleCommonHibernateDAO;
import com.job.portal.pojo.customer.Industry;

// TODO: Auto-generated Javadoc
/**
 * The Class IndustryDAO.
 */
@SuppressWarnings("unchecked")
@Component
public class IndustryDAO extends CachebleCommonHibernateDAO<Industry> {

	/**
	 * Instantiates a new industry DAO.
	 */
	public IndustryDAO() {
		super();
		setEntityClass(Industry.class);
	}

	/**
	 * Gets the all education board.
	 *
	 * @return the all education board
	 */
	public List<Industry> getAllIndustry() {
		final Criteria criteria = createCriteria(Industry.class);
		final List<Industry> list = criteria.list();
		if (!list.isEmpty()) {
			return list;
		} else {
			final List<Industry> list1 = new ArrayList<>();
			return list1;
		}

	}

	/**
	 * Find by specific.
	 *
	 * @param id
	 *            the id
	 * @return the industry
	 */
	public Industry findBySpecific(final Integer id) {
		final Criteria criteria = createCriteria(Industry.class);
		criteria.add(Restrictions.eq("id", id));
		final List<Industry> list = criteria.list();
		final Industry ini = list.get(0);
		return ini;
	}

}