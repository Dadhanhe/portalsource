/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

// TODO: Auto-generated Javadoc
/**
 * The Class UploadUtils.
 */
public class UploadUtils {

	/**
	 * Gets the number of records.
	 *
	 * @param workbook
	 *            the workbook
	 * @return the number of records
	 */
	public static int getNumberOfRecords(Workbook workbook) {
		int iRowNumber = 0;
		Sheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (DynamicEntityUtils.isRowBlank(row)) {
				continue;
			}
			iRowNumber++;
		}
		return ((iRowNumber > 1) ? (iRowNumber - 1) : 0);
	}
}
