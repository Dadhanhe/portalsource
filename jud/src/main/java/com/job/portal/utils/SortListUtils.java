/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
//通用泛型list排序
/**
 * The Class SortListUtils.
 *
 * @param <T>
 *            the generic type
 */
public class SortListUtils<T> {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(SortListUtils.class);

	/**
	 * Sort.
	 *
	 * @param list
	 *            the list
	 * @param method
	 *            the method
	 * @param sort
	 *            the sort
	 */
	public void Sort(final List<T> list, final String method,
			final String sort) {
		if (!StringUtils.isEmpty(method)) {
			Collections.sort(list, new Comparator<T>() {

				@Override
				public int compare(final T a, final T b) {
					int ret = 0;
					try {
						final Field f1 = a.getClass().getDeclaredField(method);
						f1.setAccessible(true);
						final Field f2 = b.getClass().getDeclaredField(method);
						f2.setAccessible(true);
						if ("desc".equalsIgnoreCase(sort) && (sort != null)) {
							if (ObjectUtils.isNumber(f2.get(b).toString())
									&& ObjectUtils
											.isNumber(f1.get(a).toString())) {
								ret = new BigDecimal(f2.get(b).toString())
										.compareTo(new BigDecimal(
												f1.get(a).toString()));
							} else {
								ret = (f2.get(b).toString())
										.compareTo((f1.get(a).toString()));
							}
						} else {
							if (ObjectUtils.isNumber(f2.get(b).toString())
									&& ObjectUtils
											.isNumber(f1.get(a).toString())) {
								ret = new BigDecimal(f1.get(a).toString())
										.compareTo(new BigDecimal(
												f2.get(b).toString()));
							} else {
								ret = (f1.get(a).toString())
										.compareTo((f2.get(b).toString()));
							}
						}
					} catch (final Exception ex) {
						ObjectUtils.logException(logger, ex, "Exception");
					}
					return ret;
				}
			});
		}
	}
}
