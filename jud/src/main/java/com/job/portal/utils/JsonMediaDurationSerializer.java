/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonMediaDurationSerializer.
 */
public class JsonMediaDurationSerializer extends JsonSerializer<Double> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
	 * com.fasterxml.jackson.core.JsonGenerator,
	 * com.fasterxml.jackson.databind.SerializerProvider)
	 */
	@Override
	public void serialize(final Double actualDuration,
			final JsonGenerator jGenerator,
			final SerializerProvider serProvider)
			throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub
		String duration = "-";
		if (!Objects.isEmpty(actualDuration)) {

			int h = 0, m = 0, s = 0;
			// String hour = null;
			// String minutes = null;
			// String seconds = null;
			double actDuration = actualDuration;
			if (actDuration >= 3600) {
				h = (int) (actDuration / 3600);
				actDuration = actDuration - (h * 3600);
				// hour = h + "";
			}
			if (actDuration >= 60) {
				m = (int) (actDuration / 60);
				actDuration = actDuration - (m * 60);
				// minutes = m + "";
			}
			if (actDuration >= 0) {
				s = (int) (actDuration);
				final int ms = (int) ((actDuration - s) * 1000);
				if (ms >= 500) {
					s += 1;
				}
				// seconds = s + "";
			}

			/*
			 * duration = (Objects.isEmpty(hour) ? "" : hour + ":") +
			 * (Objects.isEmpty(minutes) ? "0:" : minutes + ":") +
			 * (Objects.isEmpty(seconds) ? "00" : seconds);
			 */

			if (h != 0) {
				duration = String.format("%02d:%02d:%02d", h, m, s);
			} else {
				duration = String.format("%02d:%02d", m, s);
			}

		}

		jGenerator.writeString(duration);

	}

}
