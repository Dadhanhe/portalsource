/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class DynamicEntityUtils.
 */
@Component
public class DynamicEntityUtils {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(DynamicEntityUtils.class);

	/**
	 * Gets the cell value.
	 *
	 * @param cell
	 *            the cell
	 * @return the cell value
	 */
	public static String getCellValue(final Cell cell) {
		if (cell == null) {
			return "";
		}
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return "";
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue()).replace("\r", "");
		case Cell.CELL_TYPE_ERROR:
			return "";
		case Cell.CELL_TYPE_FORMULA:
			final Workbook wb = cell.getSheet().getWorkbook();
			final CreationHelper crateHelper = wb.getCreationHelper();
			final FormulaEvaluator evaluator = crateHelper
					.createFormulaEvaluator();
			return getCellValue(evaluator.evaluateInCell(cell)).replace("\r",
					"");
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				final Date theDate = cell.getDateCellValue();
				try {
					return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.format(theDate).replace("\r", "");
				} catch (final Exception e) {
					return new SimpleDateFormat("yyyy-MM-dd").format(theDate)
							.replace("\r", "");
				}
			} else {
				return NumberToTextConverter.toText(cell.getNumericCellValue())
						.replace("\r", "");
			}
		case Cell.CELL_TYPE_STRING:
			return cell.getRichStringCellValue().getString().replace("\r", "");
		default:
			return "";
		}
	}

	/**
	 * Checks if is valid date.
	 *
	 * @param stringVal
	 *            the string val
	 * @return true, if is valid date
	 */
	public static boolean isValidDate(final String stringVal) {
		boolean convertSuccess = true;
		final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		try {
			format.setLenient(false);
			format.parse(stringVal);
		} catch (final ParseException e) {
			convertSuccess = false;
		}
		return convertSuccess;
	}

	/**
	 * Checks if is row blank.
	 *
	 * @param row
	 *            the row
	 * @return the boolean
	 */
	public static Boolean isRowBlank(final Row row) {
		Boolean isBlank = row == null ? true : false;
		if (!isBlank) {
			isBlank = true;
			final Short firstCell = row.getFirstCellNum();
			final Short lastCell = row.getLastCellNum();
			for (Short i = firstCell; i <= lastCell; i++) {
				final Cell cell = row.getCell(i);
				final String value = getCellValue(cell);
				if (!Objects.isEmpty(value)) {
					isBlank = false;
					break;
				}
			}
		}
		return isBlank;
	}
}
