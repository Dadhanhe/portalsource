/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.job.portal.pojo.enums.EntityType;
import com.job.portal.pojo.enums.FieldType;

// TODO: Auto-generated Javadoc
/**
 * The Class ObjectUtils.
 */
public class ObjectUtils {

	/** The Constant constantFields. */
	private static final String[] constantFields = { "salt" };

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(ObjectUtils.class);

	/**
	 * Copy field values.
	 *
	 * @param fromObj
	 *            the from obj
	 * @param toObj
	 *            the to obj
	 */
	public static void copyFieldValues(final Object fromObj,
			final Object toObj) {
		copyFieldValues(fromObj, toObj, null, false);
	}

	/**
	 * Copy field values.
	 *
	 * @param fromObj
	 *            the from obj
	 * @param toObj
	 *            the to obj
	 * @param excludes
	 *            the excludes
	 */
	public static void copyFieldValues(final Object fromObj, final Object toObj,
			final String excludes) {
		copyFieldValues(fromObj, toObj, excludes, false);
	}

	/**
	 * Copy field values.
	 *
	 * @param fromObj
	 *            the from obj
	 * @param toObj
	 *            the to obj
	 * @param excludes
	 *            the excludes
	 * @param ignoreNull
	 *            the ignore null
	 */
	public static void copyFieldValues(final Object fromObj, final Object toObj,
			final String excludes, final boolean ignoreNull) {
		final Class<?> class1 = fromObj.getClass();
		final Class<?> class2 = toObj.getClass();
		if (class1.isAssignableFrom(class2)
				|| class2.isAssignableFrom(class1)) {
			final List<Field> fields = new ArrayList<>();
			fields.addAll(
					Arrays.asList(class1.getSuperclass().getDeclaredFields()));
			/*
			 * if (class1.getSuperclass().getSimpleName()
			 * .equals("AbstractJsonValueObject")) {
			 * fields.addAll(Arrays.asList(class1.getSuperclass()
			 * .getSuperclass().getDeclaredFields())); }
			 */
			fields.addAll(Arrays.asList(class1.getDeclaredFields()));

			final List<String> excluded = new ArrayList<>();
			excluded.addAll(Arrays.asList(constantFields));
			if (!StringUtils.isEmpty(excludes)) {
				excluded.addAll(Arrays
						.asList(excludes.split(SystemConfigs.DATA_SEPARATOR)));
			}
			for (final Field field : fields) {
				if (excluded.contains(field.getName())) {
					continue;
				}
				Object value = null;
				/*
				 * try { value = FieldUtils.readDeclaredField(fromObj,
				 * field.getName(), true); if (ignoreNull && (value == null)) {
				 * continue; } FieldUtils.writeDeclaredField(toObj,
				 * field.getName(), value, true); } catch (final
				 * IllegalAccessException e) { }
				 */
				try {
					field.setAccessible(true);
					value = field.get(fromObj);
					if (ignoreNull && (value == null)) {
						continue;
					}
					field.set(toObj, value);
				} catch (final IllegalAccessException e) {
				}
			}
		}
	}

	/**
	 * Checks if is number.
	 *
	 * @param str
	 *            the str
	 * @return true, if is number
	 */
	public static boolean isNumber(String str) {
		try {
			if (!Objects.isEmpty(str)) {
				str = str.replace(",", "");
			}
			// Double.parseDouble(str);
		} catch (final NumberFormatException e) {
			return false;
		} catch (final NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	/**
	 * Gets the date from string.
	 *
	 * @param dateString
	 *            the date string
	 * @param format
	 *            the format
	 * @return the date from string
	 */
	public static Date getDateFromString(final String dateString,
			final String format) {
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			return simpleDateFormat.parse(dateString);
		} catch (final ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	/**
	 * Gets the string from date.
	 *
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the string from date
	 */
	public static String getStringFromDate(final Date date,
			final String format) {
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);
	}

	/**
	 * Gets the entity type object.
	 *
	 * @return the entity type object
	 */
	public static HashMap<EntityType, Object> getEntityTypeObject() {
		final HashMap<EntityType, Object> map = new HashMap<>();

		return map;
	}

	/**
	 * Gets the JSON data table by entity type.
	 *
	 * @param entityType
	 *            the entity type
	 * @return the JSON data table by entity type
	 */
	public static String getJSONDataTableByEntityType(
			final EntityType entityType) {
		final HashMap<EntityType, String> map = new HashMap<>();
		map.put(EntityType.IncentivePlan, "incentive_plan");
		map.put(EntityType.Product, "location_group_product");
		map.put(EntityType.Tenant, "tenant_profile");
		map.put(EntityType.TenantLocation, "tenant_location_profile");
		map.put(EntityType.LocationGroup, "location_group_profile");
		map.put(EntityType.ProductMetric, "product_metric");
		map.put(EntityType.AgentMetric, "agent_metric");
		return map.get(entityType);
	}

	/**
	 * Gets the entity type class.
	 *
	 * @param entityType
	 *            the entity type
	 * @return the entity type class
	 */
	public static Class getEntityTypeClass(final EntityType entityType) {
		final HashMap<EntityType, Object> entityMap = ObjectUtils
				.getEntityTypeObject();
		return entityMap.get(entityType).getClass();
	}

	/**
	 * Gets the column value.
	 *
	 * @param columnName
	 *            the column name
	 * @param entity
	 *            the entity
	 * @param isFormat
	 *            the is format
	 * @param fieldType
	 *            the field type
	 * @return the column value
	 */
	@SuppressWarnings("unchecked")
	public static Object getColumnValue(String columnName, final Object entity,
			final Boolean isFormat, final FieldType fieldType) {
		if (entity != null) {
			Object value = new Object();
			Type type;
			if (!Objects.isEmpty(columnName)) {
				if (columnName.matches("^json[0-9]+$")) {
					try {
						value = getDeclaredMethod(entity.getClass(),
								"getJsonMap", null).invoke(entity, null);
						//
						// value = isMetric
						// ? entity.getClass().getSuperclass()
						// .getDeclaredMethod("getJsonMap",
						// new Class[] {})
						// .invoke(entity, new Object[] {})
						// : entity.getClass()
						// .getDeclaredMethod("getJsonMap",
						// new Class[] {})
						// .invoke(entity, new Object[] {});
						return value != null
								? ((String) ((Map<String, Object>) value)
										.get(columnName))
								: null;
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | SecurityException e) {
						// TODO Auto-generated catch block
						ObjectUtils.logException(logger, e, null);
					}
				}
				columnName = columnName.replace("_", "");
				columnName = "get" + WordUtils.capitalize(columnName);
				try {
					type = getDeclaredMethod(entity.getClass(), columnName,
							null).getGenericReturnType();
					value = getDeclaredMethod(entity.getClass(), columnName,
							null).invoke(entity, null);
					// value = isMetric
					// ? entity.getClass().getSuperclass()
					// .getDeclaredMethod(columnName,
					// new Class[] {})
					// .invoke(entity, new Object[] {})
					// : entity.getClass()
					// .getDeclaredMethod(columnName,
					// new Class[] {})
					// .invoke(entity, new Object[] {});
					if (type.toString().contains("Date")) {
						final Date date = value != null ? ((Date) value) : null;
						if (date != null && isFormat) {
							if (fieldType == FieldType.DateTime) {
								try {
									value = PulseDateUtils.format(date,
											SystemConfigs.DATE_TIME_FORMAT);
								} catch (final Exception e) {
									value = PulseDateUtils.format(date);
								}
							} else {
								value = PulseDateUtils.format(date);
							}
						} else {
							value = date;
						}
					} else if (type.toString().contains("Integer")) {
						value = value != null ? ((Integer) value) : null;
					} else if (type.toString().contains("Double")) {
						value = value != null ? ((Double) value) : null;
					} else if (type.toString().contains("Boolean")) {
						value = value != null ? ((Boolean) value) : null;
					} else {
						value = value != null ? ((String) value) : null;
					}
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | SecurityException e) {
					// TODO Auto-generated catch block
					ObjectUtils.logException(logger, e, null);
				}
			}
			return !Objects.isEmpty(value) ? value : "";
		} else {
			return null;
		}
	}

	/**
	 * Gets the month.
	 *
	 * @param date
	 *            the date
	 * @return the month
	 */
	public static int getMonth(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH) + 1;
	}

	/**
	 * Gets the year.
	 *
	 * @param date
	 *            the date
	 * @return the year
	 */
	public static int getYear(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	/**
	 * Gets the first date of month year.
	 *
	 * @param year
	 *            the year
	 * @param month
	 *            the month
	 * @return the first date of month year
	 */
	public static Date getFirstDateOfMonthYear(final int year,
			final int month) {
		final Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, 1);
		return cal.getTime();
	}

	/**
	 * Random number from range.
	 *
	 * @param min
	 *            the min
	 * @param max
	 *            the max
	 * @return the string
	 */
	public static String randomNumberFromRange(final Integer min,
			final Integer max) {
		Integer randomNumber = null;
		String str = "";
		if (!Objects.isEmpty(min) && !Objects.isEmpty(max)) {
			randomNumber = (int) Math
					.floor(Math.random() * (max.intValue() - min.intValue() + 1)
							+ min.intValue());
		}
		if (randomNumber != null) {
			str = randomNumber.toString();
		}
		return str;
	}

	/**
	 * Left padding.
	 *
	 * @param str
	 *            the str
	 * @param max
	 *            the max
	 * @return the string
	 */
	public static String leftPadding(String str, final Integer max) {
		if (!Objects.isEmpty(str) && !Objects.isEmpty(max)) {
			str = str.toString();
			str = str.length() < max ? leftPadding("0" + str, max) : str;
		}
		return str;
	}

	/**
	 * Gets the cell value.
	 *
	 * @param cell
	 *            the cell
	 * @return the cell value
	 */
	public static String getCellValue(final Cell cell) {
		if (cell == null) {
			return "";
		}
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BLANK:
			return "";
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue()).replace("\r", "");
		case Cell.CELL_TYPE_ERROR:
			return "";
		case Cell.CELL_TYPE_FORMULA:
			final Workbook wb = cell.getSheet().getWorkbook();
			final CreationHelper crateHelper = wb.getCreationHelper();
			final FormulaEvaluator evaluator = crateHelper
					.createFormulaEvaluator();
			return getCellValue(evaluator.evaluateInCell(cell)).replace("\r",
					"");
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				final Date theDate = cell.getDateCellValue();
				return new SimpleDateFormat("yyyy-MM-dd").format(theDate)
						.replace("\r", "");
			} else {
				return NumberToTextConverter.toText(cell.getNumericCellValue())
						.replace("\r", "");
			}
		case Cell.CELL_TYPE_STRING:
			return cell.getRichStringCellValue().getString().replace("\r", "");
		default:
			return "";
		}
	}

	/**
	 * Checks if is row blank.
	 *
	 * @param row
	 *            the row
	 * @return the boolean
	 */
	public static Boolean isRowBlank(final Row row) {
		Boolean isBlank = row == null ? true : false;
		if (!isBlank) {
			isBlank = true;
			final Short firstCell = row.getFirstCellNum();
			final Short lastCell = row.getLastCellNum();
			for (Short i = firstCell; i <= lastCell; i++) {
				final Cell cell = row.getCell(i);
				final String value = getCellValue(cell);
				if (!Objects.isEmpty(value)) {
					isBlank = false;
					break;
				}
			}
		}
		return isBlank;
	}

	/**
	 * Generate email contents.
	 *
	 * @param context
	 *            the context
	 * @param templateName
	 *            the template name
	 * @return the string
	 */
	public static String generateEmailContents(final VelocityContext context,
			final String templateName) {
		// final StringWriter sw = null;
		try {
			final Template template = Velocity
					.getTemplate("/email/" + templateName, "UTF-8");
			final StringWriter writer = new StringWriter();
			template.merge(context, writer);
			if (templateName.indexOf("export_report") >= 0
					|| templateName.indexOf("financial_report") >= 0
					|| templateName.indexOf("audit_summary_report") >= 0
					|| templateName.indexOf("agent_performance_report") >= 0
					|| templateName.indexOf("location_report") >= 0) {
				return writer.toString().replace("&amp;", "&");
			} else {
				return writer.toString();
			}
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e,
					"Failed to generate email contents :: ");
		}
		return null;
	}

	/**
	 * Gets the number of records.
	 *
	 * @param workbook
	 *            the workbook
	 * @return the number of records
	 */
	public static int getNumberOfRecords(final Workbook workbook) {
		int iRowNumber = 0;
		final Sheet sheet = workbook.getSheetAt(0);
		final Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			final Row row = rowIterator.next();
			if (isRowBlank(row)) {
				continue;
			}
			iRowNumber++;
		}
		return ((iRowNumber > 1) ? (iRowNumber - 1) : 0);
	}

	/**
	 * Map to pojo.
	 *
	 * @param <E>
	 *            the element type
	 * @param map
	 *            the map
	 * @param object
	 *            the object
	 * @return the e
	 */
	public static <E> E mapToPojo(final Map<String, Object> map,
			final Class<E> object) {
		/*
		 * final Gson gson = new Gson(); final JsonElement jsonElement =
		 * gson.toJsonTree(map); final E pojo = gson.fromJson(jsonElement,
		 * object);
		 */
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(
				DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.setDateFormat(
				new SimpleDateFormat(SystemConfigs.DATE_TIME_FORMAT));
		final E pojo = objectMapper.convertValue(map, object);
		return pojo;
	}

	/**
	 * Special copy field values.
	 *
	 * @param fromObj
	 *            the from obj
	 * @param toObj
	 *            the to obj
	 * @param excludes
	 *            the excludes
	 * @param ignoreNull
	 *            the ignore null
	 */
	public static void specialCopyFieldValues(final Object fromObj,
			final Object toObj, final String excludes,
			final boolean ignoreNull) {
		final Class<?> class1 = fromObj.getClass().getSuperclass();
		final Class<?> class2 = toObj.getClass().getSuperclass();
		if (class1.isAssignableFrom(class2)
				|| class2.isAssignableFrom(class1)) {
			final Field[] fields = class1.getDeclaredFields();
			final List<String> excluded = new ArrayList<>();
			excluded.addAll(Arrays.asList(constantFields));
			if (!StringUtils.isEmpty(excludes)) {
				excluded.addAll(Arrays
						.asList(excludes.split(SystemConfigs.DATA_SEPARATOR)));
			}
			for (final Field field : fields) {
				if (excluded.contains(field.getName())) {
					continue;
				}
				Object value = null;
				try {
					field.setAccessible(true);
					value = field.get(fromObj);

					/*
					 * value = FieldUtils.readDeclaredField(
					 * fromObj.getClass().getSuperclass().cast(fromObj),
					 * field.getName(), true);
					 */
					if (ignoreNull && (value == null)) {
						continue;
					}
					field.set(toObj, value);
					/*
					 * FieldUtils.writeDeclaredField(
					 * toObj.getClass().getSuperclass().cast(toObj),
					 * field.getName(), value, true);
					 */
				} catch (final IllegalAccessException e) {
				}
			}
		}
	}

	/**
	 * Gets the string from JSON map.
	 *
	 * @param jsonMap
	 *            the json map
	 * @return the string from JSON map
	 */
	public static String getStringFromJSONMap(
			final Map<String, Object> jsonMap) {
		String strJsonData = "";
		final ObjectMapper mapper = new ObjectMapper();
		try {
			strJsonData = mapper.writeValueAsString(jsonMap);
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
		return strJsonData;
	}

	/**
	 * Gets the string from JSON map.
	 *
	 * @param obj
	 *            the obj
	 * @return the string from JSON map
	 */
	public static String getJsonString(final Object obj) {
		String strJsonData = "";
		final ObjectMapper mapper = new ObjectMapper();
		try {
			strJsonData = mapper.writeValueAsString(obj);
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
		return strJsonData;
	}

	/**
	 * Sets the column value.
	 *
	 * @param columnName
	 *            the column name
	 * @param value
	 *            the value
	 * @param entity
	 *            the entity
	 * @param fieldType
	 *            the field type
	 * @return the object
	 */
	public static Object setColumnValue(String columnName, Object value,
			final Object entity, final FieldType fieldType) {
		if (!Objects.isEmpty(columnName)) {
			Type[] type = null;
			Object mapValue = null;
			final Class[] params = new Class[1];
			final String colName = columnName;
			columnName = columnName.replace("_", "");
			columnName = "set" + WordUtils.capitalize(columnName);
			final String lowerColumnName = columnName.toLowerCase();
			try {
				if (lowerColumnName.indexOf("json") != (-1)) {
					try {
						mapValue = getDeclaredMethod(entity.getClass(),
								"getJsonMap", null).invoke(entity, null);
						//
						// value = isMetric
						// ? entity.getClass().getSuperclass()
						// .getDeclaredMethod("getJsonMap",
						// new Class[] {})
						// .invoke(entity, new Object[] {})
						// : entity.getClass()
						// .getDeclaredMethod("getJsonMap",
						// new Class[] {})
						// .invoke(entity, new Object[] {});
						value = mapValue != null
								? ((String) ((Map<String, Object>) mapValue)
										.get(colName))
								: null;
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException | SecurityException e) {
						// TODO Auto-generated catch block
						ObjectUtils.logException(logger, e, null);
					}
				}

				final Method[] methods = entity.getClass().getMethods();
				for (final Method m : methods) {
					if (m.getName().equalsIgnoreCase(columnName)) {
						type = m.getParameterTypes();
						break;
					}
				}
				if (!Objects.isEmpty(type)) {
					if (type[0].toString().contains("Date")) {
						try {
							params[0] = Date.class;
							if (!Objects.isEmpty(value)) {
								if (colName.equals("openTime")
										|| colName.equals("closeTime")) {
									final DateFormat df = new SimpleDateFormat(
											SystemConfigs.DATE_TIME_FORMAT3);
									value = df.parse((String) value);
								} else {
									if (fieldType == FieldType.DateTime) {
										try {
											final DateFormat df = new SimpleDateFormat(
													SystemConfigs.DATE_TIME_FORMAT3);
											value = df.parse((String) value);
										} catch (final Exception e) {
											final DateFormat df = new SimpleDateFormat(
													"yyyy-MM-dd");
											value = df.parse((String) value);
										}
									} else {
										final DateFormat df = new SimpleDateFormat(
												"yyyy-MM-dd");
										value = df.parse((String) value);
									}
								}
							}
						} catch (final ParseException e) {
							ObjectUtils.logException(logger, e, null);
						}
					} else if (type[0].toString().contains("Integer")) {
						params[0] = Integer.class;
						if (!Objects.isEmpty(value)) {
							value = Objects.isDecimal(value)
									? Integer.parseInt((String) value)
									: null;
						}
					} else if (type[0].toString().contains("Double")) {
						params[0] = Double.class;
						if (!Objects.isEmpty(value)) {
							value = Objects.isDecimal(value)
									? Double.parseDouble((String) value)
									: null;
						}
					} else if (type[0].toString().contains("Boolean")) {
						params[0] = Boolean.class;
						if (!Objects.isEmpty(value)) {
							if ((value.toString().equalsIgnoreCase("true"))
									|| (value.toString()
											.equalsIgnoreCase("yes"))) {
								value = true;
							} else if ((value.toString()
									.equalsIgnoreCase("false"))
									|| (value.toString()
											.equalsIgnoreCase("no"))) {
								value = false;
							} else if ((Integer.parseInt(value.toString()) == 1
									|| value.toString()
											.equalsIgnoreCase("1"))) {
								value = true;
							} else {
								value = false;
							}
							value = Objects.isBoolean(value.toString())
									? Boolean.parseBoolean(value.toString())
									: null;
						}
					} else {
						params[0] = String.class;
					}
				} else {
					params[0] = String.class;
				}
				if (Objects.isEmpty(value)) {
					value = null;
				}
				if (lowerColumnName.indexOf("json") != (-1)
						&& !Objects.isEmpty(mapValue)) {
					try {
						final Map<String, Object> tempMap = ((Map<String, Object>) mapValue);
						final Set<String> keys = tempMap.keySet();
						if (keys.contains(colName)) {
							tempMap.put(colName, value);
						}
						final String jsonData = ObjectUtils
								.getStringFromJSONMap(tempMap);
						getDeclaredMethod(entity.getClass(), "setJsonData",
								params).invoke(entity, jsonData);
					} catch (IllegalArgumentException | SecurityException e) {
						// TODO Auto-generated catch block
						ObjectUtils.logException(logger, e, null);
					}
				} else {
					getDeclaredMethod(entity.getClass(), columnName, params)
							.invoke(entity, value);
				}
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | SecurityException e) {
				// TODO Auto-generated catch block
				ObjectUtils.logException(logger, e, null);
			}
		}
		return entity;
	}

	/**
	 * Gets the declared method.
	 *
	 * @param classObj
	 *            the class obj
	 * @param getterMethodName
	 *            the getter method name
	 * @param parameterTypes
	 *            the parameter types
	 * @return the declared method
	 */
	@SuppressWarnings("unchecked")
	public static Method getDeclaredMethod(final Class classObj,
			final String getterMethodName, final Class<?>... parameterTypes) {
		try {
			return classObj.getDeclaredMethod(getterMethodName, parameterTypes);
		} catch (final NoSuchMethodException e) {
			return getDeclaredMethod(classObj.getSuperclass(), getterMethodName,
					parameterTypes);
		} catch (final SecurityException e) {
			return getDeclaredMethod(classObj.getSuperclass(), getterMethodName,
					parameterTypes);
		}
	}

	/**
	 * Generate web view contents.
	 *
	 * @param context
	 *            the context
	 * @param templateName
	 *            the template name
	 * @return the string
	 */
	public static String generateWebViewContents(final VelocityContext context,
			final String templateName) {
		// final StringWriter sw = null;
		try {
			final Template template = Velocity
					.getTemplate("/report/" + templateName, "UTF-8");
			final StringWriter writer = new StringWriter();
			template.merge(context, writer);
			return writer.toString();
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e,
					"Failed to generate email contents :: ");
		}
		return null;
	}

	/**
	 * Gets the first day of quarter.
	 *
	 * @param date
	 *            the date
	 * @return the first day of quarter
	 */
	public static Date getFirstDayOfQuarter(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 3 * 3);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * Gets the last day of quarter.
	 *
	 * @param date
	 *            the date
	 * @return the last day of quarter
	 */
	public static Date getLastDayOfQuarter(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 3 * 3 + 2);
		cal.set(Calendar.DAY_OF_MONTH,
				cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}

	/**
	 * Gets the first day of week.
	 *
	 * @param date
	 *            the date
	 * @return the first day of week
	 */
	public static Date getFirstDayOfWeek(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();// Date of Monday of current week
	}

	/**
	 * Gets the last day of week.
	 *
	 * @param date
	 *            the date
	 * @return the last day of week
	 */
	public static Date getLastDayOfWeek(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.add(Calendar.DATE, 6);// Add 6 days to get Sunday of next week
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return cal.getTime();// Date of Sunday of current week
	}

	/**
	 * Gets the first day of month.
	 *
	 * @param date
	 *            the date
	 * @return the first day of month
	 */
	public static Date getFirstDayOfMonth(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();// Date of Sunday of current week
	}

	/**
	 * Gets the last day of month.
	 *
	 * @param date
	 *            the date
	 * @return the last day of month
	 */
	public static Date getLastDayOfMonth(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH,
				cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();// Date of Sunday of current week
	}

	/**
	 * Gets the first day of year.
	 *
	 * @param date
	 *            the date
	 * @return the first day of year
	 */
	public static Date getFirstDayOfYear(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();// Date of Sunday of current week
	}

	/**
	 * Gets the last day of year.
	 *
	 * @param date
	 *            the date
	 * @return the last day of year
	 */
	public static Date getLastDayOfYear(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH,
				cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();// Date of Sunday of current week
	}

	/**
	 * Gets the search words.
	 *
	 * @param searchKeyword
	 *            the search keyword
	 * @return the search words
	 */
	public static String[] getSearchWords(final String searchKeyword) {
		String keyword = searchKeyword.trim();
		String[] searchWords = keyword.split(" ");
		if (keyword.startsWith("\"") && keyword.endsWith("\"")) {
			keyword = keyword.replaceAll("\"", "");
			searchWords = new String[] { keyword };
		}
		return searchWords;
	}

	/**
	 * Checks if is exact word.
	 *
	 * @param searchKeyword
	 *            the search keyword
	 * @return the boolean
	 */
	public static Boolean isExactWord(final String searchKeyword) {
		final String keyword = searchKeyword.trim();
		if (keyword.startsWith("\"") && keyword.endsWith("\"")) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the day of month.
	 *
	 * @param date
	 *            the date
	 * @return the day of month
	 */
	public static Integer getDayOfMonth(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Gets the last month date.
	 *
	 * @param date
	 *            the date
	 * @return the last month date
	 */
	public static Date getLastMonthDate(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	/**
	 * Gets the month in string.
	 *
	 * @param index
	 *            the index
	 * @return the month in string
	 */
	public static String getMonthInString(final int index) {
		switch (index) {
		case 1:

			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";

		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";
		default:
			return "None";

		}

	}

	/**
	 * Gets the report date.
	 *
	 * @param date
	 *            the date
	 * @return the report date
	 */
	public static final String getReportDate(final Date date) {
		return getMonthInString(getMonth(getLastMonthDate(date)));
	}

	/**
	 * Log exception.
	 *
	 * @param logger
	 *            the logger
	 * @param ex
	 *            the ex
	 * @param message
	 *            the message
	 */
	public static void logException(final Logger logger, final Exception ex,
			final String message) {
		final StringBuilder strMessage = new StringBuilder("");
		if (Objects.isEmpty(message)) {
			strMessage.append(ex.getMessage());
		} else {
			strMessage.append(message);
			strMessage.append(ex.getMessage());
		}
		strMessage.append(" :: && Exception is :: ");
		final String finalMessage = strMessage.toString();
		logger.error(finalMessage, ex);
	}

	/**
	 * Log error.
	 *
	 * @param logger
	 *            the logger
	 * @param ex
	 *            the ex
	 * @param message
	 *            the message
	 */
	public static void logError(final Logger logger, final Error ex,
			final String message) {
		final StringBuilder strMessage = new StringBuilder("");
		if (Objects.isEmpty(message)) {
			strMessage.append(ex.getMessage());
		} else {
			strMessage.append(message);
			strMessage.append(ex.getMessage());
		}
		strMessage.append(" :: && Error is :: ");
		final String finalMessage = strMessage.toString();
		logger.error(finalMessage, ex);
	}

	/**
	 * Gets the user token.
	 *
	 * @return the user token
	 */
	public static Integer getUserToken() {
		try {
			return Integer.parseInt(String.valueOf(SecurityContextHolder
					.getContext().getAuthentication().getPrincipal()));
		} catch (final Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
