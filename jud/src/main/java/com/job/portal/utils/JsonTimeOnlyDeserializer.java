/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonTimeOnlyDeserializer.
 */
@Component
public class JsonTimeOnlyDeserializer extends JsonDeserializer<Date> {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(JsonTimeOnlyDeserializer.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml
	 * .jackson.core.JsonParser,
	 * com.fasterxml.jackson.databind.DeserializationContext)
	 */
	@Override
	public Date deserialize(final JsonParser dateStr,
			final DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				Constants.TIME_FORMAT);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr.getText());
		} catch (final ParseException e) {
			ObjectUtils.logException(logger, e, null);
		}
		return date;

	}

}
