/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class HttpClientPool.
 */
@Component
public class HttpClientPool {

	/**
	 * The Constant logger.
	 *
	 */

	private static final Logger logger = LoggerFactory
			.getLogger(HttpClientPool.class);

	/** The http client pool. */
	private static HttpClientPool httpClientPool = null;

	/** The thread safe client. */
	private final CloseableHttpClient threadSafeClient;

	/** The monitor. */
	private final IdleConnectionMonitorThread monitor;

	/** The default http client max per route. */
	private static Integer defaultHttpClientMaxPerRoute = 20;

	/** The max http client connections. */
	private static Integer maxHttpClientConnections = 200;

	/** The http client timeout millis. */
	private static Integer httpClientTimeoutMillis = 60000;

	/**
	 * Instantiates a new http client pool.
	 */
	private HttpClientPool() {
		final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		// Increase max total connection to 200
		cm.setMaxTotal(maxHttpClientConnections);
		// Increase default max connection per route to 20
		cm.setDefaultMaxPerRoute(defaultHttpClientMaxPerRoute);

		final ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
			@Override
			public long getKeepAliveDuration(final HttpResponse response,
					final HttpContext context) {
				final HeaderElementIterator it = new BasicHeaderElementIterator(
						response.headerIterator(HTTP.CONN_KEEP_ALIVE));
				while (it.hasNext()) {
					final HeaderElement he = it.nextElement();
					final String param = he.getName();
					final String value = he.getValue();
					if (value != null && param.equalsIgnoreCase("timeout")) {
						return Long.parseLong(value) * 1000;
					}
				}
				return 10 * 1000L;
			}
		};

		final RequestConfig requestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(httpClientTimeoutMillis)
				.setSocketTimeout(httpClientTimeoutMillis)
				.setConnectionRequestTimeout(httpClientTimeoutMillis).build();

		// Build the client.
		this.threadSafeClient = HttpClients.custom().setConnectionManager(cm)
				.useSystemProperties().disableConnectionState()
				.setDefaultRequestConfig(requestConfig)
				.setKeepAliveStrategy(myStrategy).build();

		this.monitor = new IdleConnectionMonitorThread(cm);

		this.monitor.setDaemon(true);
		this.monitor.start();
	}

	/**
	 * Gets the single instance of HttpClientPool.
	 *
	 * @return single instance of HttpClientPool
	 */
	static public HttpClientPool getInstance() {
		synchronized (HttpClientPool.class) {
			if (httpClientPool == null) {
				httpClientPool = new HttpClientPool();
			}
		}
		return httpClientPool;
	}

	/**
	 * Gets the http client.
	 *
	 * @return the http client
	 */
	public CloseableHttpClient getHttpClient() {
		return this.threadSafeClient;
	}

	/**
	 * Shutdown.
	 */
	public void shutdown() {
		try {
			this.monitor.shutdown();
		} catch (final InterruptedException ie) {
			logger.debug("Intertupted shutting down HttpConnection Pooling.");
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * The Class IdleConnectionMonitorThread.
	 */
	// Watches for stale connections and evicts them.
	private class IdleConnectionMonitorThread extends Thread {

		/** The cm. */
		// The manager to watch.
		private final PoolingHttpClientConnectionManager cm;

		/** The stop signal. */
		// Use a BlockingQueue to stop everything.
		private final BlockingQueue<Stop> stopSignal = new ArrayBlockingQueue<Stop>(
				1);

		/**
		 * The Class Stop.
		 */
		// Pushed up the queue.
		private class Stop {

			/** The stop. */
			// The return queue.
			private final BlockingQueue<Stop> stop = new ArrayBlockingQueue<Stop>(
					1);

			/**
			 * Stopped.
			 */
			// Called by the process that is being told to stop.
			public void stopped() {
				// Push me back up the queue to indicate we are now stopped.
				this.stop.add(this);
			}

			/**
			 * Wait for stopped.
			 *
			 * @throws InterruptedException
			 *             the interrupted exception
			 */
			// Called by the process requesting the stop.
			public void waitForStopped() throws InterruptedException {
				// Wait until the callee acknowledges that it has stopped.
				this.stop.poll(30, TimeUnit.SECONDS);
			}

		}

		/**
		 * Instantiates a new idle connection monitor thread.
		 *
		 * @param cm
		 *            the cm
		 */
		IdleConnectionMonitorThread(
				final PoolingHttpClientConnectionManager cm) {
			super();
			this.cm = cm;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			try {
				// Holds the stop request that stopped the process.
				Stop stopRequest;
				// Every 5 seconds.
				while ((stopRequest = this.stopSignal.poll(5,
						TimeUnit.SECONDS)) == null) {
					// Close expired connections
					this.cm.closeExpiredConnections();
					// Optionally, close connections that have been idle too
					// long.
					this.cm.closeIdleConnections(60, TimeUnit.SECONDS);
					// Look at pool stats.
					logger.debug("Stats: {}", this.cm.getTotalStats());
				}
				// Acknowledge the stop request.
				stopRequest.stopped();
			} catch (final InterruptedException ex) {
				// terminate
				Thread.currentThread().interrupt();
			}
		}

		/**
		 * Shutdown.
		 *
		 * @throws InterruptedException
		 *             the interrupted exception
		 */
		public void shutdown() throws InterruptedException {
			logger.trace("Shutting down client pool");
			// Signal the stop to the thread.
			final Stop stop = new Stop();
			this.stopSignal.add(stop);
			// Wait for the stop to complete.
			stop.waitForStopped();
			// Close the pool - Added
			try {
				HttpClientPool.this.threadSafeClient.close();
			} catch (final IOException ioe) {
				logger.debug(
						"IO Exception while closing HttpClient connecntions.");
			}
			// Close the connection manager.
			this.cm.close();
			logger.trace("Client pool shut down");
		}

	}

}
