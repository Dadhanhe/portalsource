/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

// TODO: Auto-generated Javadoc
/**
 * The Class SendEmailOffice365.
 */
public class SendEmailOffice365 {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getAnonymousLogger();

	/** The Constant SERVIDOR_SMTP. */
	private static final String SERVIDOR_SMTP = "smtp.office365.com";

	/** The Constant PORTA_SERVIDOR_SMTP. */
	private static final int PORTA_SERVIDOR_SMTP = 587;

	/** The Constant CONTA_PADRAO. */
	private static final String CONTA_PADRAO = "noreply@fpg-ingauge.com";

	/** The Constant SEN_HA_CONTA_PADRAO. */
	private static final String SEN_HA_CONTA_PADRAO = "%FPG2017";

	/** The from. */
	private final String from = "noreply@fpg-ingauge.com";

	/** The to. */
	private final String to = "nishith@fpgpulse.com";

	/** The subject. */
	private final String subject = "Teste";

	/** The message content. */
	private final String messageContent = "Teste de Mensagem";

	/**
	 * Send email.
	 */
	public void sendEmail() {
		final Session session = Session.getInstance(getEmailProperties(),
				new Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(CONTA_PADRAO,
								SEN_HA_CONTA_PADRAO);
					}

				});

		try {
			final Message message = new MimeMessage(session);
			message.setRecipient(Message.RecipientType.TO,
					new InternetAddress(this.to));
			message.setFrom(new InternetAddress(this.from));
			message.setSubject(this.subject);
			message.setText(this.messageContent);
			message.setSentDate(new Date());
			Transport.send(message);
		} catch (final MessagingException ex) {
			LOGGER.log(Level.WARNING,
					"Erro ao enviar mensagem: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Gets the email properties.
	 *
	 * @return the email properties
	 */
	public Properties getEmailProperties() {
		final Properties config = new Properties();
		config.put("mail.smtp.auth", "true");
		config.put("mail.smtp.starttls.enable", "true");
		config.put("mail.smtp.host", SERVIDOR_SMTP);
		config.put("mail.smtp.port", PORTA_SERVIDOR_SMTP);
		return config;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args) {
		new SendEmailOffice365().sendEmail();
	}

}
