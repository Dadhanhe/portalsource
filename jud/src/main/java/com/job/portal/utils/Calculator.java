/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Collections;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class Calculator.
 */
public class Calculator {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(Calculator.class);

	/**
	 * The postfix stack.
	 *
	 * @param expression
	 *            the expression
	 * @return the double
	 */
	// private static Stack<String> postfixStack = new Stack<String>();

	/** The op stack. */
	// private static Stack<Character> opStack = new Stack<Character>();

	/** The operat priority. */
	// private static int[] operatPriority = new int[] { 0, 3, 2, 1, -1, 1, 0, 2
	// };

	/**
	 * Calculate.
	 *
	 * @param expression
	 *            the expression
	 * @return the double
	 */
	public static double calculate(String expression) {
		final Stack<String> postfixStack = new Stack<String>();
		final Stack<Character> opStack = new Stack<Character>();
		final int[] operatPriority = new int[] { 0, 3, 2, 1, -1, 1, 0, 2 };
		try {
			// logger.debug("expression : " + expression);
			postfixStack.removeAllElements();
			opStack.removeAllElements();
			expression = expression.replaceAll("\\s", "");
			final Stack<String> resultStack = new Stack<String>();
			prepare(expression, opStack, postfixStack, operatPriority);
			Collections.reverse(postfixStack);
			String firstValue, secondValue, currentValue;
			while (!postfixStack.isEmpty()) {
				currentValue = postfixStack.pop();
				if (!isOperator(currentValue.charAt(0))) {
					resultStack.push(currentValue);
				} else {
					secondValue = resultStack.pop();
					firstValue = resultStack.pop();
					final String tempResult = calculate(firstValue, secondValue,
							currentValue.charAt(0));
					resultStack.push(tempResult);
				}
			}
			return Double.valueOf(resultStack.pop());
		} catch (final NumberFormatException e) {
			logger.debug(String.format(
					"Failed to calculate expression - NumberFormatException \"%s\"",
					expression));
		} catch (final ArithmeticException e) {
			logger.debug(String.format(
					"Failed to calculate expression - ArithmeticException \"%s\"",
					expression));
		} catch (final Exception e) {
			logger.debug(String.format(
					"Failed to calculate expression - Exception \"%s\"",
					expression));
		}
		return 0;
	}

	/**
	 * Calculate.
	 *
	 * @param firstValue
	 *            the first value
	 * @param secondValue
	 *            the second value
	 * @param currentOp
	 *            the current op
	 * @return the string
	 */
	private static String calculate(final String firstValue,
			final String secondValue, final char currentOp) {
		String result = "";
		switch (currentOp) {
		case '+':
			result = String.valueOf(ArithHelper.add(firstValue, secondValue));
			break;
		case '-':
			result = String.valueOf(ArithHelper.sub(firstValue, secondValue));
			break;
		case '*':
			result = String.valueOf(ArithHelper.mul(firstValue, secondValue));
			break;
		case '/':
			result = String.valueOf(ArithHelper.div(firstValue, secondValue));
			break;
		}
		return result;
	}

	/**
	 * Compare.
	 *
	 * @param cur
	 *            the cur
	 * @param peek
	 *            the peek
	 * @param operatPriority
	 *            the operat priority
	 * @return true, if successful
	 */
	public static boolean compare(final char cur, final char peek,
			final int[] operatPriority) {
		boolean result = false;
		if (operatPriority[(peek) - 40] >= operatPriority[(cur) - 40]) {
			result = true;
		}
		return result;
	}

	/**
	 * Checks if is operator.
	 *
	 * @param c
	 *            the c
	 * @return true, if is operator
	 */
	private static boolean isOperator(final char c) {
		return (c == '+') || (c == '-') || (c == '*') || (c == '/')
				|| (c == '(') || (c == ')');
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args) {
		String s = " (checkedInRevenue /availableRooms) * 1.1";
		final Pattern pattern = Pattern
				.compile("[^a-zA-Z]?([a-zA-Z]+)[a-zA-Z]?");
		final Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			final String g = matcher.group(1);
			if ("checkedInRevenue".equals(g)) {
				s = s.replace("checkedInRevenue", "80000");
			} else if ("availableRooms".equals(g)) {
				s = s.replace("availableRooms", "10");
			}
		}
		Calculator.calculate(s);
	}

	/**
	 * Prepare.
	 *
	 * @param expression
	 *            the expression
	 * @param opStack
	 *            the op stack
	 * @param postfixStack
	 *            the postfix stack
	 * @param operatPriority
	 *            the operat priority
	 */
	private static void prepare(final String expression,
			final Stack<Character> opStack, final Stack<String> postfixStack,
			final int[] operatPriority) {
		opStack.push(',');
		final char[] arr = expression.toCharArray();
		int currentIndex = 0;
		int count = 0;
		char currentOp, peekOp;
		for (int i = 0; i < arr.length; i++) {
			currentOp = arr[i];
			if (isOperator(currentOp)) {
				if (count > 0) {
					postfixStack.push(new String(arr, currentIndex, count));
				}
				peekOp = opStack.peek();
				if (currentOp == ')') {
					while (opStack.peek() != '(') {
						postfixStack.push(String.valueOf(opStack.pop()));
					}
					opStack.pop();
				} else {
					while ((currentOp != '(') && (peekOp != ',')
							&& compare(currentOp, peekOp, operatPriority)) {
						postfixStack.push(String.valueOf(opStack.pop()));
						peekOp = opStack.peek();
					}
					opStack.push(currentOp);
				}
				count = 0;
				currentIndex = i + 1;
			} else {
				count++;
			}
		}
		if ((count > 1) || ((count == 1) && !isOperator(arr[currentIndex]))) {
			postfixStack.push(new String(arr, currentIndex, count));
		}
		while (opStack.peek() != ',') {
			postfixStack.push(String.valueOf(opStack.pop()));
		}
	}
}
