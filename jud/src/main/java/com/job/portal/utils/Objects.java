/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.customer.User;

// TODO: Auto-generated Javadoc
/**
 * The Class Objects.
 */
public class Objects {

	/** The Constant _NO. */
	private static final String _NO = "No";

	/** The Constant YES. */
	private static final String YES = "Yes";

	/** The Constant logger. */

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(Objects.class);

	/**
	 * Checks if is null.
	 *
	 * @param object
	 *            the object
	 * @return true, if is null
	 */
	public synchronized static boolean isNull(final Object object) {
		return (object == null);
	}

	/**
	 * Gets the string value.
	 *
	 * @param object
	 *            the object
	 * @return the string value
	 */
	public synchronized static String getStringValue(final Boolean object) {
		return Objects.checkBoolean(object) ? "true" : "false";
	}

	/**
	 * Checks if is not null.
	 *
	 * @param object
	 *            the object
	 * @return true, if is not null
	 */
	public synchronized static boolean isNotNull(final Object object) {
		return (object != null);
	}

	/**
	 * Checks if is empty.
	 *
	 * @param object
	 *            the object
	 * @return true, if is empty
	 */
	public synchronized static boolean isEmpty(final Object object) {
		if (isNull(object)) {
			return true;
		}
		if (object instanceof Collection) {
			return ((Collection<?>) object).isEmpty();
		}
		if (object instanceof String) {
			return "".equals(((String) object).trim());
		}
		return false;
	}

	/**
	 * Prepare long array.
	 *
	 * @param stringArray
	 *            the string array
	 * @return the long[]
	 */
	public synchronized static Long[] prepareLongArray(
			final String[] stringArray) {
		final Long[] longArray = new Long[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			longArray[i] = Long.valueOf(stringArray[i]);
		}
		return longArray;
	}

	/**
	 * Prepare long array.
	 *
	 * @param object
	 *            the object
	 * @return the long[]
	 */
	public synchronized static Long[] prepareLongArray(final String object) {
		final String[] stringArray = convertToArray(object);
		Long[] longArray = null;
		if (stringArray != null) {
			longArray = new Long[stringArray.length];
			for (int i = 0; i < stringArray.length; i++) {
				longArray[i] = Long.valueOf(stringArray[i]);
			}
		}
		return longArray;
	}

	/**
	 * Checks if is empty collection.
	 *
	 * @param collection
	 *            the collection
	 * @return the boolean
	 */
	public static Boolean isEmptyCollection(final Collection<?> collection) {
		return (collection == null || collection.isEmpty()) ? true : false;
	}

	/**
	 * Checks if is empty map.
	 *
	 * @param map
	 *            the map
	 * @return the boolean
	 */
	public static Boolean isEmptyMap(final Map<?, ?> map) {
		return (map == null || map.isEmpty()) ? true : false;
	}

	/**
	 * Gets the map value.
	 *
	 * @param map
	 *            the map
	 * @param key
	 *            the key
	 * @param defaultReturnValue
	 *            the default return value
	 * @return the map value
	 */
	public static Object getMapValue(final Map<?, ?> map, final Object key,
			final Object defaultReturnValue) {
		if (!isEmptyMap(map) && map.containsKey(key)) {
			return map.get(key);
		}
		return defaultReturnValue;
	}

	/**
	 * Checks if is null collection.
	 *
	 * @param collection
	 *            the collection
	 * @param message
	 *            the message
	 * @return the boolean
	 */
	public static Boolean isNullCollection(final Collection<?> collection,
			final String message) {
		return (collection == null) ? true : false;
	}

	/**
	 * Convert to array.
	 *
	 * @param value
	 *            the value
	 * @return the string[]
	 */
	public static String[] convertToArray(final String value) {

		if (value != null && !value.trim().isEmpty()) {
			return value.split(",");
		} else {
			return null;
		}
	}

	/**
	 * Cast list.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param cList
	 *            the c list
	 * @return the list
	 */
	public static <T> List<T> castList(final Class<? extends T> clazz,
			final Collection<?> cList) {
		final List<T> rList = new ArrayList<T>(cList.size());
		for (final Object o : cList) {
			rList.add(clazz.cast(o));
		}
		return rList;
	}

	/**
	 * Convert to string.
	 *
	 * @param list
	 *            the list
	 * @return the string
	 */
	public static String convertToString(final List<?> list) {
		String returnString = null;
		if (!isEmptyCollection(list)) {
			returnString = list.toString().replace("[", "").replace("]", "")
					.replace(", ", ",");
		}
		return returnString;
	}

	/**
	 * Convert to SQL string.
	 *
	 * @param list
	 *            the list
	 * @return the string
	 */
	public static String convertToSQLString(final List<?> list) {
		String returnString = null;
		if (!isEmptyCollection(list)) {
			returnString = "'" + list.toString().replace("[", "")
					.replace("]", "").replace(", ", "','") + "'";
		}
		return returnString;
	}

	/**
	 * Convert to string.
	 *
	 * @param fieldSchemeMap
	 *            the field scheme map
	 * @return the string
	 */
	public static String convertToString(
			final HashMap<String, Integer> fieldSchemeMap) {
		String returnString = null;
		if (fieldSchemeMap != null && !fieldSchemeMap.isEmpty()) {
			returnString = fieldSchemeMap.toString().replace("=", ":");
		}
		return returnString;
	}

	/**
	 * Convert to criteria array list.
	 *
	 * @param valueList
	 *            the value list
	 * @return the list
	 */
	public static List<String> convertToCriteriaArrayList(
			final Long[] valueList) {
		List<String> preparedList = convertToArrayList(valueList);
		if (preparedList == null) {
			preparedList = new ArrayList<String>();
			preparedList.add("");
		}
		return preparedList;
	}

	/**
	 * Gets the valid string.
	 *
	 * @param object
	 *            the object
	 * @return the valid string
	 */
	public static String getValidString(final String object) {
		return (object == null) ? "" : object;
	}

	/**
	 * Gets the valid string.
	 *
	 * @param object
	 *            the object
	 * @return the valid string
	 */
	public static String getValidString(final Long object) {
		return (object == null) ? "" : object.toString();
	}

	/**
	 * Gets the valid string.
	 *
	 * @param object
	 *            the object
	 * @return the valid string
	 */
	public static String getValidString(final Integer object) {
		return (object == null) ? "" : object.toString();
	}

	/**
	 * Gets the valid string.
	 *
	 * @param object
	 *            the object
	 * @return the valid string
	 */
	public static String getValidString(final Boolean object) {

		return checkBoolean(object) ? YES : _NO;
	}

	/**
	 * Join strings.
	 *
	 * @param string1
	 *            the string 1
	 * @param string2
	 *            the string 2
	 * @param separator
	 *            the separator
	 * @return the string
	 */
	public static String joinStrings(final String string1, final String string2,
			final String separator) {
		return getValidString(string1) + separator + getValidString(string2);
	}

	/**
	 * Gets the numbers.
	 *
	 * @param value
	 *            the value
	 * @return the numbers
	 */
	public static String getNumbers(String value) {
		if (value != null) {
			value = value.replaceAll("\\D+", "");
		}
		return value;
	}

	/**
	 * Convert to array list.
	 *
	 * @param value
	 *            the value
	 * @return the list
	 */
	public static List<String> convertToArrayList(final String value) {
		String[] valueList = null;
		if (value != null && !value.trim().isEmpty()) {
			valueList = value.split(",");
		}
		List<String> preparedList = null;
		if (valueList != null && valueList.length > 0) {
			preparedList = new ArrayList<>();
			for (final String string : valueList) {
				preparedList.add(string);
			}
		}
		return preparedList;
	}

	/**
	 * Equals.
	 *
	 * @param val1
	 *            the val 1
	 * @param val2
	 *            the val 2
	 * @return true, if successful
	 */
	public static boolean equals(final String val1, final String val2) {
		return val1 != null && val1.equals(val2);
	}

	/**
	 * Convert to integer array list.
	 *
	 * @param value
	 *            the value
	 * @return the list
	 */
	public static List<Integer> convertToIntegerArrayList(final String value) {
		String[] valueList = null;
		if (value != null && !value.trim().isEmpty()) {
			valueList = value.split(",");
		}
		List<Integer> preparedList = null;
		if (valueList != null && valueList.length > 0) {
			preparedList = new ArrayList<Integer>();
			for (final String string : valueList) {
				preparedList.add(Integer.valueOf(string));
			}
		}
		return preparedList;
	}

	/**
	 * Convert to array list.
	 *
	 * @param valueList
	 *            the value list
	 * @return the list
	 */
	public static List<String> convertToArrayList(final String[] valueList) {
		List<String> preparedList = null;
		if (valueList != null && valueList.length > 0) {
			preparedList = new ArrayList<String>();
			for (final String string : valueList) {
				preparedList.add(string);
			}
		}
		return preparedList;
	}

	/**
	 * Adds the data to object.
	 *
	 * @param processObjects
	 *            the process objects
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public static void addDataToObject(
			final LinkedHashMap<String, Object> processObjects,
			final String key, final Object value) {
		if (processObjects != null && value != null) {
			processObjects.put(key, value);
		}
	}

	/**
	 * Join first last name.
	 *
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @return the string
	 */
	public static String joinFirstLastName(final String firstName,
			final String lastName) {
		return Objects.getValidString(firstName) + " "
				+ Objects.getValidString(lastName);
	}

	/**
	 * Gets the valid string.
	 *
	 * @param object
	 *            the object
	 * @return the valid string
	 */
	public static String getValidString(final Date object) {
		return object == null ? ""
				: new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(object);
	}

	/**
	 * Gets the string.
	 *
	 * @param queryStrings
	 *            the query strings
	 * @return the string
	 */
	public static String getString(final Long[] queryStrings) {
		return Objects
				.convertToString(Objects.convertToArrayList(queryStrings));
	}

	/**
	 * Gets the string.
	 *
	 * @param queryStrings
	 *            the query strings
	 * @return the string
	 */
	public static String getString(final String[] queryStrings) {
		return Objects
				.convertToString(Objects.convertToArrayList(queryStrings));
	}

	/**
	 * Convert to array list.
	 *
	 * @param valueList
	 *            the value list
	 * @return the list
	 */
	public static List<String> convertToArrayList(final Long[] valueList) {
		List<String> preparedList = null;
		if (valueList != null && valueList.length > 0) {
			preparedList = new ArrayList<String>();
			for (final Long string : valueList) {
				preparedList.add(string == null ? null : string.toString());
			}
		}
		return preparedList;
	}

	/**
	 * Gets the integer value.
	 *
	 * @param object
	 *            the object
	 * @return the integer value
	 */
	public static Integer getIntegerValue(final Object object) {
		return object != null ? Integer.valueOf(String.valueOf(object)) : null;
	}

	/**
	 * Gets the long value.
	 *
	 * @param object
	 *            the object
	 * @return the long value
	 */
	public static Long getLongValue(final Object object) {
		return object != null ? Long.valueOf(String.valueOf(object)) : null;
	}

	/**
	 * Gets the double value.
	 *
	 * @param object
	 *            the object
	 * @return the double value
	 */
	public static Double getDoubleValue(final Object object) {
		Double value = 0.00d;
		if (object != null) {
			try {
				value = Double.valueOf(String.valueOf(object));
			} catch (final Exception e) {
				ObjectUtils.logException(logger, e, null);
			}
		}
		return value;
	}

	/**
	 * Gets the collection size.
	 *
	 * @param collection
	 *            the collection
	 * @return the collection size
	 */
	public static Integer getCollectionSize(final Collection<?> collection) {
		return collection != null ? collection.size() : 0;
	}

	/**
	 * Collections iterator.
	 *
	 * @param <T>
	 *            the generic type
	 * @param iterable
	 *            the iterable
	 * @return the iterable
	 */
	public static <T> Iterable<T> collectionsIterator(
			final Iterable<T> iterable) {
		return iterable == null ? Collections.<T> emptyList() : iterable;
	}

	/**
	 * Join.
	 *
	 * @param str
	 *            the str
	 * @return the string
	 */
	public static String join(final List<? extends Serializable> str) {
		final StringBuilder stringBuilder = new StringBuilder();
		for (int ind = 0; ind < str.size(); ind++) {
			stringBuilder.append("'").append(str.get(ind).toString())
					.append("'");
			if (str.size() != ind + 1) {
				stringBuilder.append(",");
			}

		}
		return stringBuilder.toString();
	}

	/**
	 * Join str.
	 *
	 * @param str
	 *            the str
	 * @param seperator
	 *            the seperator
	 * @return the string
	 */
	public static String joinStr(final List<? extends Serializable> str,
			String seperator) {
		final StringBuilder stringBuilder = new StringBuilder();
		seperator = !Objects.isEmpty(seperator) ? seperator : ",";
		for (int ind = 0; ind < str.size(); ind++) {
			stringBuilder.append(str.get(ind).toString());
			if (str.size() != ind + 1) {
				stringBuilder.append(seperator);
			}

		}
		return stringBuilder.toString();
	}

	/**
	 * To hash map.
	 *
	 * @param <K>
	 *            the key type
	 * @param <V>
	 *            the value type
	 * @param dictionary
	 *            the dictionary
	 * @return the map
	 */
	public static <K, V> Map<K, V> toHashMap(
			final Dictionary<K, V> dictionary) {
		if (dictionary == null) {
			return null;
		}
		final Map<K, V> map = new HashMap<K, V>(dictionary.size());
		final Enumeration<K> keys = dictionary.keys();
		while (keys.hasMoreElements()) {
			final K key = keys.nextElement();
			map.put(key, dictionary.get(key));
		}
		return map;
	}

	/**
	 * To dictionary.
	 *
	 * @param <K>
	 *            the key type
	 * @param <V>
	 *            the value type
	 * @param hashMap
	 *            the hash map
	 * @return the dictionary
	 */
	public static <K, V> Dictionary<K, V> toDictionary(
			final HashMap<K, V> hashMap) {
		if (hashMap == null) {
			return null;
		}
		final Dictionary<K, V> dictionary = new Hashtable<K, V>(hashMap.size());
		final Iterator<Entry<K, V>> it = hashMap.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<K, V> entry = it.next();
			final K key = entry.getKey();
			dictionary.put(key, hashMap.get(key));
		}
		return dictionary;
	}

	/**
	 * Convert string keysto int.
	 *
	 * @param map
	 *            the map
	 * @return the hash map
	 */
	public static HashMap<Integer, String> convertStringKeystoInt(
			final HashMap<String, String> map) {
		final HashMap<Integer, String> returnMap = new HashMap<Integer, String>();
		final Iterator<Entry<String, String>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<String, String> entry = it.next();
			final String key = entry.getKey();
			returnMap.put(Integer.parseInt(key), entry.getValue());
		}
		return returnMap;
	}

	/**
	 * The Enum Months.
	 */
	public enum Months {

		/** The january. */
		JANUARY(0),

		/** The february. */
		FEBRUARY(1),

		/** The march. */
		MARCH(2),

		/** The april. */
		APRIL(3),

		/** The may. */
		MAY(4),

		/** The june. */
		JUNE(5),

		/** The july. */
		JULY(6),

		/** The august. */
		AUGUST(7),

		/** The september. */
		SEPTEMBER(8),

		/** The october. */
		OCTOBER(9),

		/** The november. */
		NOVEMBER(10),

		/** The december. */
		DECEMBER(11);

		/** The value. */
		private final int value;

		/**
		 * Instantiates a new months.
		 *
		 * @param value
		 *            the value
		 */
		private Months(final int value) {
			this.value = value;
		}

		/**
		 * Value.
		 *
		 * @return the int
		 */
		public int value() {
			return this.value;
		}

		/**
		 * From integer.
		 *
		 * @param value
		 *            the value
		 * @return the months
		 */
		public static Months fromInteger(final Integer value) {
			if (value == JANUARY.value) {
				return JANUARY;
			} else if (value == FEBRUARY.value) {
				return FEBRUARY;
			} else if (value == MARCH.value) {
				return MARCH;
			} else if (value == APRIL.value) {
				return APRIL;
			} else if (value == MAY.value) {
				return MAY;
			} else if (value == JUNE.value) {
				return JUNE;
			} else if (value == JULY.value) {
				return JULY;
			} else if (value == AUGUST.value) {
				return AUGUST;
			} else if (value == SEPTEMBER.value) {
				return SEPTEMBER;
			} else if (value == OCTOBER.value) {
				return OCTOBER;
			} else if (value == NOVEMBER.value) {
				return NOVEMBER;
			} else if (value == DECEMBER.value) {
				return DECEMBER;
			} else if (value == 12) {
				return DECEMBER;
			}
			return null;
		}

		/**
		 * Gets the enum name.
		 *
		 * @param value
		 *            the value
		 * @return the enum name
		 */
		public static String getEnumName(final Integer value) {
			if (value == JANUARY.value) {
				return "January";
			} else if (value == FEBRUARY.value) {
				return "February";
			} else if (value == MARCH.value) {
				return "March";
			} else if (value == APRIL.value) {
				return "April";
			} else if (value == MAY.value) {
				return "May";
			} else if (value == JUNE.value) {
				return "June";
			} else if (value == JULY.value) {
				return "July";
			} else if (value == AUGUST.value) {
				return "August";
			} else if (value == SEPTEMBER.value) {
				return "September";
			} else if (value == OCTOBER.value) {
				return "October";
			} else if (value == NOVEMBER.value) {
				return "November";
			} else if (value == DECEMBER.value) {
				return "December";
			} else if (value == 12) {
				return "December";
			}
			return null;
		}

	}

	/**
	 * Checks if is decimal.
	 *
	 * @param object
	 *            the object
	 * @return the boolean
	 */
	public static Boolean isDecimal(final Object object) {
		Boolean isDecimal = false;
		if (object != null) {
			try {
				Double.valueOf(String.valueOf(object));
				isDecimal = true;
			} catch (final NumberFormatException e) {
				isDecimal = false;
			}
		}
		return isDecimal;
	}

	/**
	 * The Enum Days.
	 */
	public enum Days {

		/** The monday. */
		MONDAY(0),

		/** The tuesday. */
		TUESDAY(1),

		/** The wednesday. */
		WEDNESDAY(2),

		/** The thursday. */
		THURSDAY(3),

		/** The friday. */
		FRIDAY(4),

		/** The saturday. */
		SATURDAY(5),

		/** The sunday. */
		SUNDAY(6);

		/** The value. */
		private final int value;

		/**
		 * Instantiates a new days.
		 *
		 * @param value
		 *            the value
		 */
		private Days(final int value) {
			this.value = value;
		}

		/**
		 * Value.
		 *
		 * @return the int
		 */
		public int value() {
			return this.value;
		}

		/**
		 * From integer.
		 *
		 * @param value
		 *            the value
		 * @return the days
		 */
		public static Days fromInteger(final Integer value) {
			if (value == MONDAY.value) {
				return MONDAY;
			} else if (value == TUESDAY.value) {
				return TUESDAY;
			} else if (value == WEDNESDAY.value) {
				return WEDNESDAY;
			} else if (value == THURSDAY.value) {
				return THURSDAY;
			} else if (value == FRIDAY.value) {
				return FRIDAY;
			} else if (value == SATURDAY.value) {
				return SATURDAY;
			} else if (value == SUNDAY.value) {
				return SUNDAY;
			}
			return null;
		}

		/**
		 * Gets the enum name.
		 *
		 * @param value
		 *            the value
		 * @return the enum name
		 */
		public static String getEnumName(final Integer value) {
			if (value == MONDAY.value) {
				return "Monday";
			} else if (value == TUESDAY.value) {
				return "Tuesday";
			} else if (value == WEDNESDAY.value) {
				return "Wednesday";
			} else if (value == THURSDAY.value) {
				return "Thursday";
			} else if (value == FRIDAY.value) {
				return "Friday";
			} else if (value == SATURDAY.value) {
				return "Saturday";
			} else if (value == SUNDAY.value) {
				return "Sunday";
			}
			return null;
		}

	}

	/**
	 * Validate email.
	 *
	 * @param email
	 *            the email
	 * @return true, if successful
	 */
	public static boolean validateEmail(final String email) {
		if (!isEmpty(email)) {
			final Pattern pattern = Pattern
					.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
							+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			final Matcher matcher = pattern.matcher(email);
			return matcher.matches();
		}
		return true;
	}

	/**
	 * Gets the industry id from header.
	 *
	 * @param request
	 *            the request
	 * @return the industry id from header
	 */
	public static Integer getIndustryIdFromHeader(
			final HttpServletRequest request) {
		final String industryIdStr = request
				.getHeader(SystemConfigs.HEADER_INDUSTRY_ID);
		Integer industryID = null;
		if (!Objects.isEmpty(industryIdStr)) {
			industryID = Integer.valueOf(industryIdStr);
		}
		return industryID;
	}

	/**
	 * Checks if is for mobile from header.
	 *
	 * @param request
	 *            the request
	 * @return the boolean
	 */
	public static Boolean isForMobileFromHeader(
			final HttpServletRequest request) {
		final String isMobileStr = request
				.getHeader(SystemConfigs.HEADER_IS_MOBILE);
		Boolean isMobile = null;
		if (!Objects.isEmpty(isMobileStr)) {
			isMobile = Boolean.valueOf(isMobileStr);
		}
		return isMobile;
	}

	/**
	 * Formate date time to str.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String formateDateTimeToStr(final Date date) {
		final SimpleDateFormat format = new SimpleDateFormat(
				SystemConfigs.DATE_TIME_FORMAT);
		return format.format(date);
	}

	/**
	 * Gets the ids from abstract value object.
	 *
	 * @param list
	 *            the list
	 * @return the ids from abstract value object
	 */
	public static List<Integer> getIdsFromAbstractValueObject(
			final List<? extends AbstractValueObject> list) {
		final List<Integer> idList = new ArrayList<Integer>();
		if (list != null) {
			for (final AbstractValueObject avo : list) {
				idList.add(avo.getId());
			}
		}
		return idList;
	}

	/**
	 * Gets the big decimal value.
	 *
	 * @param object
	 *            the object
	 * @return the big decimal value
	 */
	public static BigDecimal getBigDecimalValue(final Object object) {
		BigDecimal value = BigDecimal.ZERO;
		if (object != null) {
			try {
				value = new BigDecimal(String.valueOf(object));
			} catch (final Exception e) {
				ObjectUtils.logException(logger, e, null);
			}
		}
		return value;
	}

	/**
	 * Convert string to int array.
	 *
	 * @param str
	 *            the str
	 * @param extraSize
	 *            the extra size
	 * @return the integer[]
	 */
	public static Integer[] convertStringToIntArray(final String str,
			final Integer extraSize) {
		if (!Objects.isEmpty(str)) {
			final String[] strArray = str.split(",");
			final Integer[] ints = new Integer[strArray.length
					+ (extraSize != null ? extraSize : 0)];
			for (int i = 0; i < strArray.length; i++) {
				try {
					ints[i] = Integer.parseInt(strArray[i]);
				} catch (final NumberFormatException nfe) {
					// Not an integer, do some
				}
			}
			return ints;
		} else {
			final Integer[] ints = new Integer[(extraSize != null ? extraSize
					: 0)];
			return ints;
		}
	}

	/**
	 * Removes the special characters.
	 *
	 * @param fileName
	 *            the file name
	 * @return the string
	 */
	public static String removeSpecialCharacters(String fileName) {
		fileName = fileName.replaceAll("[^a-zA-Z0-9-'+_]+", "");
		return fileName.toString();
	}

	/**
	 * Checks if is boolean.
	 *
	 * @param object
	 *            the object
	 * @return the boolean
	 */
	public static Boolean isBoolean(final Object object) {
		Boolean isBoolean = false;
		if (object != null) {
			try {
				// Boolean.valueOf(String.valueOf(object));
				isBoolean = true;
			} catch (final NumberFormatException e) {
				isBoolean = false;
			}
		}
		return isBoolean;
	}

	/**
	 * Gets the time zone from header.
	 *
	 * @param request
	 *            the request
	 * @return the time zone from header
	 */
	public static String getTimeZoneFromHeader(
			final HttpServletRequest request) {
		final String timeZoneStr = request.getHeader(SystemConfigs.TIME_ZONE);
		String timeZone = null;
		if (!Objects.isEmpty(timeZoneStr)) {
			timeZone = timeZoneStr;
		}
		return timeZone;
	}

	/**
	 * Gets the ids from user objects.
	 *
	 * @param list
	 *            the list
	 * @return the ids from user objects
	 */
	public static List<Integer> getIdsFromUserObjects(final List<User> list) {
		final List<Integer> idList = new ArrayList<Integer>();
		for (final User d : list) {
			idList.add(d.getId());
		}
		return idList;
	}

	/**
	 * Check boolean.
	 *
	 * @param object
	 *            the object
	 * @return true, if successful
	 */
	public static boolean checkBoolean(final Boolean object) {
		if (isNull(object)) {
			return false;
		} else {
			return object.booleanValue();
		}
	}

	/**
	 * Copy file.
	 *
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyFile(final String from, final String to)
			throws IOException {
		final Path src = Paths.get(from);
		final Path dest = Paths.get(to);
		Files.copy(src.toFile(), dest.toFile());
	}

	/**
	 * Stream.
	 *
	 * @param <T>
	 *            the generic type
	 * @param collection
	 *            the collection
	 * @return the stream
	 */
	public static <T> Stream<T> stream(final Collection<T> collection) {
		if (collection == null) {
			return Stream.empty();
		}
		return collection.stream();
	}

	/**
	 * Prints the JSON object.
	 *
	 * @param obj
	 *            the obj
	 * @return the string
	 */
	public static String printJSONObject(final Object obj) {
		final ObjectMapper objMapper = new ObjectMapper();
		try {
			return objMapper.writeValueAsString(obj);
		} catch (final JsonProcessingException e) {
			ObjectUtils.logException(logger, e, null);
			return null;
		}
	}

	/**
	 * Gets the ids from abstract DTO.
	 *
	 * @param list
	 *            the list
	 * @return the ids from abstract DTO
	 */
	public static List<Integer> getIdsFromAbstractDTO(
			final List<? extends AbstractDTO> list) {
		final List<Integer> idList = new ArrayList<>();
		if (list != null) {
			for (final AbstractDTO avo : list) {
				idList.add(avo.getId());
			}
		}
		return idList;
	}
}