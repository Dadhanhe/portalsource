/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonDateTimeDeserializer.
 */
@Component
public class JsonDateTimeDeserializer extends JsonDeserializer<Date> {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger
			.getLogger(JsonDateTimeDeserializer.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml
	 * .jackson.core.JsonParser,
	 * com.fasterxml.jackson.databind.DeserializationContext)
	 */
	@Override
	public Date deserialize(final JsonParser dateStr,
			final DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		// TODO Auto-generated method stub
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				Constants.DATE_TIME_FORMAT);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr.getText());
		} catch (final ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage(), e);
		}
		return date;
	}

}
