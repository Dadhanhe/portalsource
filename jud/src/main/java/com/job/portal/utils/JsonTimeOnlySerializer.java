/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonTimeOnlySerializer.
 */
@Component
public class JsonTimeOnlySerializer extends JsonSerializer<Date> {

	/**
	 * The Constant dateFormat.
	 *
	 * @param date
	 *            the date
	 * @param gen
	 *            the gen
	 * @param provider
	 *            the provider
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException
	 *             the json processing exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
	 * com.fasterxml.jackson.core.JsonGenerator,
	 * com.fasterxml.jackson.databind.SerializerProvider)
	 */
	@Override
	public void serialize(final Date date, final JsonGenerator gen,
			final SerializerProvider provider)
			throws IOException, JsonProcessingException {

		final String formattedDate = new SimpleDateFormat(Constants.TIME_FORMAT)
				.format(date);

		gen.writeString(formattedDate);
	}

}
