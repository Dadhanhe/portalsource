/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Date;
import java.util.Hashtable;

// TODO: Auto-generated Javadoc
/**
 * The Class UserSession.
 */
public class UserSession {

	/**
	 * just adding a private constructor to hide public.
	 */
	private UserSession() {
		// just adding a private constructor to hide public
	}

	/** The Constant hashtableForSession. */
	protected static final Hashtable<Integer, Date> hashtableForSession = new Hashtable<>();

	/**
	 * Sets the value in map.
	 *
	 * @param userId
	 *            the user id
	 * @return the date
	 */
	public static void setValueInMap(final Integer userId) {
		UserSession.hashtableForSession.put(userId, new Date());
	}

	/**
	 * Checks if is exist id in map.
	 *
	 * @param id
	 *            the id
	 * @return true, if is exist id in map
	 */
	public static boolean isExistIdInMap(final Integer id) {
		return UserSession.hashtableForSession.containsKey(id);
	}

	/**
	 * Gets the values from map.
	 *
	 * @param id
	 *            the id
	 * @return the values from map
	 */
	public static Date getValuesFromMap(final Integer id) {
		return UserSession.hashtableForSession.get(id);
	}

	/**
	 * Removes the session from map.
	 *
	 * @param id
	 *            the id
	 */
	public static void removeSessionFromMap(final Integer id) {
		UserSession.hashtableForSession.remove(id);

	}

}
