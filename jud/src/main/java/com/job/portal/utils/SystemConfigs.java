/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.job.portal.pojo.enums.FeedType;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemConfigs.
 */
public class SystemConfigs {

	/** The Constant ADMIN_EMAIL. */
	public static final String ADMIN_EMAIL = "system.admin.email";

	/** The Constant BASE_URL. */
	public static final String BASE_URL = "system.base.url";

	/** The Constant JSON_COLUMN_PREFIX. */
	public static final String JSON_COLUMN_PREFIX = "json";

	/** The Constant DATE_TIME_FORMAT. */
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/** The Constant DATE_TIME_FORMAT3. */
	public static final String DATE_TIME_FORMAT3 = "yyyy-MM-dd HH:mm:ss";

	/** The Constant DATE_TIME_FORMAT4. */
	public static final String DATE_TIME_FORMAT4 = "dd-MMM-yyyy hh:mm:ss a";

	/** The Constant DATE_TIME_12HR_FORMAT. */
	public static final String DATE_TIME_12HR_FORMAT = "yyyy-MM-dd hh:mm a";

	/** The Constant DATE_TIME_12HR_FORMAT_2. */
	public static final String DATE_TIME_12HR_FORMAT_2 = "MMM dd, yyyy hh:mm:ss a";

	/** The Constant DATE_TIME_12HR_FORMAT_3. */
	public static final String DATE_TIME_12HR_FORMAT_3 = "EEE, MMM d, yyyy @ hh:mm a";

	/** The Constant DISPLAY_DATE_FORMAT_2. */
	public static final String DISPLAY_DATE_FORMAT_2 = "dd-MMM-yy";

	/** The Constant DISPLAY_DATE_FORMAT_3. */
	public static final String DISPLAY_DATE_FORMAT_3 = "dd-MMM-yyyy";

	/** The Constant DISPLAY_DATE_FORMAT_4. */
	public static final String DISPLAY_DATE_FORMAT_4 = "dd-MM-yyyy";

	/** The Constant DISPLAY_DATE_FORMAT. */
	public static final String DISPLAY_DATE_FORMAT = "MMM dd";

	/** The Constant REPORT_DATE_FORMAT. */
	public static final String REPORT_DATE_FORMAT = "MMM dd, yyyy";

	/** The Constant FULL_MONTH_FORMAT. */
	public static final String FULL_MONTH_FORMAT = "MMMM";

	/** The Constant YEAR_FORMAT. */
	public static final String YEAR_FORMAT = "yyyy";

	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** The Constant TIME_FORMAT. */
	public static final String TIME_FORMAT = "HH:mm:ss";

	/** The Constant TIME_AM_PM_FORMAT. */
	public static final String TIME_AM_PM_FORMAT = "hh:mm a";

	/** The Constant YEAR_MONTH_FORMAT. */
	public static final String YEAR_MONTH_FORMAT = "yyyy-MM";

	/** The Constant BIRTH_DATE_FORMAT. */
	public static final String BIRTH_DATE_FORMAT = "dd-MMM";

	/** The Constant MONTH_FORMAT. */
	public static final String MONTH_FORMAT = "MM";

	/** The Constant DATA_SEPARATOR. */
	public static final String DATA_SEPARATOR = ",";

	/** The Constant ARRIVALS_DATE_FIELD. */
	public static final String ARRIVALS_DATE_FIELD = "arrivalDate";

	/** The Constant DEPARTURE_DATE_FIELD. */
	public static final String DEPARTURE_DATE_FIELD = "departureDate";

	/** The Constant BUSINESS_DATE_FIELD. */
	public static final String BUSINESS_DATE_FIELD = "businessDate";

	/** The Constant METRIC_DATE_FIELD. */
	public static final String METRIC_DATE_FIELD = "metricDate";

	/** The Constant seconds. */
	public static final int seconds = 1000;

	/** The Constant minutes. */
	public static final int minutes = 1000 * 60;

	/** The Constant hours. */
	public static final int hours = 1000 * 60 * 60;

	/** The Constant newEmailCheckInterval. */
	public static final int newEmailCheckInterval = 20 * seconds;

	/** The Constant queueProcessInterval. */
	public static final int queueProcessInterval = 5 * seconds;

	/** The Constant rankingProcessInterval. */
	public static final int rankingProcessInterval = 2 * hours;

	/** The Constant notifyBluePrintNotUpdatedInterval. */
	public static final int notifyBluePrintNotUpdatedInterval = 6 * hours;

	/** The Constant reminderProcessInterval. */
	public static final int reminderProcessInterval = 20 * seconds;

	/** The Constant timerTaskProcessInterval. */
	public static final int timerTaskProcessInterval = 20 * seconds;

	/** The Constant uploadQueueCheckInterval. */
	public static final int uploadQueueCheckInterval = 20 * seconds;

	/** The Constant FS. */
	public static final String FS = "FS";

	/** The Constant FL. */
	public static final String FL = "FL";

	/** The Constant FA. */
	public static final String FA = "FA";

	/** The Constant FSB. */
	public static final String FSB = "FSB";

	/** The Constant MIXED. */
	public static final String MIXED = "Mixed";

	/** The Constant UNDEFINED. */
	public static final String UNDEFINED = "Undefined";

	/** The Constant DT. */
	public static final String DT = "DT";

	/** The Constant DB. */
	public static final String DB = "DB";

	/** The Constant DEFAULT_PASS. */
	public static final String DEFAULT_PASS = "welcome123";

	/** The Constant DEFAULT_PASS_STRING. */
	public static final String DEFAULT_PASS_STRING = "5858ea228cc2edf88721699b2c8638e5";

	/** The Constant TEMPLATE_START_DATE. */
	public static final String TEMPLATE_START_DATE = "Start Date";

	/** The Constant TEMPLATE_COMPLETE_DATE. */
	public static final String TEMPLATE_COMPLETE_DATE = "Complete Date";

	/** The Constant LOCATION_NAME. */
	public static final String LOCATION_NAME = "Location Name";

	/** The Constant MULTIPLE_LOCATIONS. */
	public static final String MULTIPLE_LOCATIONS = "Multiple Locations";

	/** The Constant TEMPLATE_FILE_NAME. */
	public static final String TEMPLATE_FILE_NAME = "File Name";

	/** The Constant TEMPLATE_FILE_UPLOADER. */
	public static final String TEMPLATE_FILE_UPLOADER = "File Uploader";

	/** The Constant TEMPLATE_FILE_TOTAL_RECORDS. */
	public static final String TEMPLATE_FILE_TOTAL_RECORDS = "Total Number of Records";

	/** The Constant TEMPLATE_FILE_SUCCESS_RECORDS. */
	public static final String TEMPLATE_FILE_SUCCESS_RECORDS = "Number of success Records";

	/** The Constant TEMPLATE_FILE_EXISTS_RECORDS. */
	public static final String TEMPLATE_FILE_EXISTS_RECORDS = "Number of existing Records";

	/** The Constant TEMPLATE_FILE_FAIL_RECORDS. */
	public static final String TEMPLATE_FILE_FAIL_RECORDS = "Number of failed Records";

	/** The Constant DAILY. */
	public static final String DAILY = "Daily";

	/** The Constant DEPARTURE. */
	public static final String DEPARTURE = "Departure";

	/** The Constant DEPARTURE_CR. */
	public static final String DEPARTURE_CR = "Check in";

	/** The Constant ARRIVALS. */
	public static final String ARRIVALS = "Arrivals";

	/** The Constant REPORT_CONVERSION. */
	public static final String REPORT_CONVERSION = "RUC";

	/** The Constant REPORT_UPGRADE_CONVERSION_CODE. */
	public static final String REPORT_UPGRADE_CONVERSION_CODE = "UC";

	/** The Constant PRODUCT_THUMBNAIL_HEIGHT. */
	public static final Integer PRODUCT_THUMBNAIL_HEIGHT = 300;

	/** The Constant PRODUCT_THUMBNAIL_WIDTH. */
	public static final Integer PRODUCT_THUMBNAIL_WIDTH = 300;

	/** The Constant PRODUCT_COST. */
	public static final String PRODUCT_COST = "productCost";

	/** The Constant TIME_ZONE. */
	public static final String TIME_ZONE = "TimeZone";

	/** The Constant GOAL_START_DATE. */
	public static final String GOAL_START_DATE = "Start_Date";

	/** The Constant GOAL_END_DATE. */
	public static final String GOAL_END_DATE = "END_Date";

	/** The Constant GOAL_YEAR_START_DATE. */
	public static final String GOAL_YEAR_START_DATE = "YEAR_Start_Date";

	/** The Constant GOAL_YEAR_END_DATE. */
	public static final String GOAL_YEAR_END_DATE = "YEAR_END_Date";

	/** The Constant CURRENT_YEAR_START_DATE. */
	public static final String CURRENT_YEAR_START_DATE = "Current_Year_Start_Date";

	/** The Constant CURRENT_YEAR_END_DATE. */
	public static final String CURRENT_YEAR_END_DATE = "Current_Year_End_Date";

	/** The Constant PREVIOUS_YEAR_START_DATE. */
	public static final String PREVIOUS_YEAR_START_DATE = "Previous_Year_Start_Date";

	/** The Constant PREVIOUS_YEAR_END_DATE. */
	public static final String PREVIOUS_YEAR_END_DATE = "Previous_Year_End_Date";

	/** The Constant GOAL_PREVIOUS_MONTH_START_DATE. */
	public static final String GOAL_PREVIOUS_MONTH_START_DATE = "Previous_Month_Start_Date";

	/** The Constant GOAL_PREVIOUS_MONTH_END_DATE. */
	public static final String GOAL_PREVIOUS_MONTH_END_DATE = "Previous_Month_End_Date";

	/** The Constant RULE_GOAL_CAST. */
	public static final String RULE_GOAL_CAST = "RULE_GOAL_CAST";

	/** The Constant RULE_GOAL_ACCEPTED. */
	public static final String RULE_GOAL_ACCEPTED = "RULE_GOAL_ACCEPTED";

	/** The Constant RULE_GOAL_UPDATE. */
	public static final String RULE_GOAL_UPDATE = "RULE_GOAL_UPDATE";

	/** The Constant RULE_GOAL_ACHEIVED. */
	public static final String RULE_GOAL_ACHEIVED = "RULE_GOAL_ACHEIVED";

	/** The Constant RULE_GOAL_ACCEPTANCE_REMINDER. */
	public static final String RULE_GOAL_ACCEPTANCE_REMINDER = "RULE_GOAL_ACCEPTANCE_REMINDER";

	/** The Constant RULE_FPG_GOAL_UPDATE. */
	public static final String RULE_FPG_GOAL_UPDATE = "RULE_FPG_GOAL_UPDATE";

	/** The Constant RULE_GOAL_WEEKLY_MONTHLY_NOTIFICATIO_USER. */
	public static final String RULE_GOAL_WEEKLY_MONTHLY_NOTIFICATIO_USER = "RULE_GOAL_WEEKLY_MONTHLY_NOTIFICATION_USER";

	/** The Constant RULE_LOCATION_TARGET_CREATE. */
	public static final String RULE_LOCATION_TARGET_CREATE = "RULE_LOCATION_TARGET_CREATE";

	/** The Constant RULE_LOCATION_TARGET_UPDATE. */
	public static final String RULE_LOCATION_TARGET_UPDATE = "RULE_LOCATION_TARGET_UPDATE";

	/** The Constant RULE_SOCIAL_INTERACTION. */
	public static final String RULE_SOCIAL_INTERACTION = "RULE_SOCIAL_INTERACTION";

	/** The Constant RULE_FEED_SOCIAL_INTERACTION. */
	public static final String RULE_FEED_SOCIAL_INTERACTION = "RULE_FEED_SOCIAL_INTERACTION";

	/** The Constant RULE_FEED_SOCIAL_INTERACTION_LIKE. */
	public static final String RULE_FEED_SOCIAL_INTERACTION_LIKE = "RULE_FEED_SOCIAL_INTERACTION_LIKE";

	/** The Constant RULE_FEED_SOCIAL_INTERACTION_COMMENT. */
	public static final String RULE_FEED_SOCIAL_INTERACTION_COMMENT = "RULE_FEED_SOCIAL_INTERACTION_COMMENT";

	/** The Constant RULE_TRAINING_COMPLETE. */
	public static final String RULE_TRAINING_COMPLETE = "RULE_TRAINING_COMPLETE";

	/** The Constant RULE_TRAINING_ASSIGNE. */
	public static final String RULE_TRAINING_ASSIGNE = "RULE_TRAINING_ASSIGNE";

	/** The Constant RULE_COMMANDCENTER_ASSIGN_VIDEO. */
	public static final String RULE_COMMANDCENTER_ASSIGN_VIDEO = "RULE_COMMANDCENTER_ASSIGN_VIDEO";

	/** The Constant RULE_COMMANDCENTER_ONE2ONE. */
	public static final String RULE_COMMANDCENTER_ONE2ONE = "RULE_COMMANDCENTER_ONE2ONE";

	/** The Constant RULE_COMMANDCENTER_COACHING. */
	public static final String RULE_COMMANDCENTER_COACHING = "RULE_COMMANDCENTER_COACHING";

	/** The Constant RULE_ONE2ONE_SCHEDULE. */
	public static final String RULE_ONE2ONE_SCHEDULE = "RULE_ONE2ONE_SCHEDULE";

	/** The Constant RULE_ONE2ONE_UPDATE_SCHEDULE. */
	public static final String RULE_ONE2ONE_UPDATE_SCHEDULE = "RULE_ONE2ONE_UPDATE_SCHEDULE";

	/** The Constant RULE_ONE2ONE_TAKEN. */
	public static final String RULE_ONE2ONE_TAKEN = "RULE_ONE2ONE_TAKEN";

	/** The Constant RULE_ONE2ONE_CANCEL. */
	public static final String RULE_ONE2ONE_CANCEL = "RULE_ONE2ONE_CANCEL";

	/** The Constant RULE_ONE2ONE_REMOVE_USER. */
	public static final String RULE_ONE2ONE_REMOVE_USER = "RULE_ONE2ONE_REMOVE_USER";

	/** The Constant RULE_OBSERVATION_SCHEDULE. */
	public static final String RULE_OBSERVATION_SCHEDULE = "RULE_OBSERVATION_SCHEDULE";

	/** The Constant RULE_OBSERVATION_UPDATE_SCHEDULE. */
	public static final String RULE_OBSERVATION_UPDATE_SCHEDULE = "RULE_OBSERVATION_UPDATE_SCHEDULE";

	/** The Constant RULE_COACHING_SCHEDULE. */
	public static final String RULE_COACHING_SCHEDULE = "RULE_COACHING_SCHEDULE";

	/** The Constant RULE_COACHING_UPADTE_SCHEDULE. */
	public static final String RULE_COACHING_UPADTE_SCHEDULE = "RULE_COACHING_UPADTE_SCHEDULE";

	/** The Constant RULE_OBSERVATION_TAKEN. */
	public static final String RULE_OBSERVATION_TAKEN = "RULE_OBSERVATION_TAKEN";

	/** The Constant RULE_COACHING_TAKEN. */
	public static final String RULE_COACHING_TAKEN = "RULE_COACHING_TAKEN";

	/** The Constant RULE_COACHING_CANCEL. */
	public static final String RULE_COACHING_CANCEL = "RULE_COACHING_CANCEL";

	/** The Constant RULE_OBSERVATION_CANCEL. */
	public static final String RULE_OBSERVATION_CANCEL = "RULE_OBSERVATION_CANCEL";

	/** The Constant RULE_ASSIGN_TEAM_VIA_IMPORT. */
	public static final String RULE_ASSIGN_TEAM_VIA_IMPORT = "RULE_ASSIGN_TEAM_VIA_IMPORT";

	/** The Constant RULE_POST_FEED. */
	public static final String RULE_POST_FEED = "RULE_POST_FEED";

	/** The Constant RULE_POST_FEED_COMMENT. */
	public static final String RULE_POST_FEED_COMMENT = "RULE_POST_FEED_COMMENT";

	/** The Constant RULE_NOT_ASSIGN_TEAM_VIA_IMPORT_MULTIPLE_GROUP. */
	public static final String RULE_NOT_ASSIGN_TEAM_VIA_IMPORT_MULTIPLE_GROUP = "RULE_NOT_ASSIGN_TEAM_VIA_IMPORT_MULTIPLE_GROUP";

	/** The Constant RULE_MEETING_SCHEDULE. */
	public static final String RULE_MEETING_SCHEDULE = "RULE_MEETING_SCHEDULE";

	/** The Constant RULE_MEETING_SCHEDULE_UPDATE. */
	public static final String RULE_MEETING_SCHEDULE_UPDATE = "RULE_MEETING_SCHEDULE_UPDATE";

	/** The Constant RULE_MEETING_CANCEL. */
	public static final String RULE_MEETING_CANCEL = "RULE_MEETING_CANCEL";

	/** The Constant RULE_MEETING_REMOVE_USER. */
	public static final String RULE_MEETING_REMOVE_USER = "RULE_MEETING_REMOVE_USER";

	/** The Constant RULE_CHANGE_ERROR_RECORDS_TO_APPROVED. */
	public static final String RULE_CHANGE_ERROR_RECORDS_TO_APPROVED = "RULE_CHANGE_ERROR_RECORDS_TO_APPROVED";

	/** The Constant RULE_MACHINE_LEARNING_RECOMMENDATION_VIDEO. */
	public static final String RULE_MACHINE_LEARNING_RECOMMENDATION_VIDEO = "RULE_MACHINE_LEARNING_RECOMMENDATION_VIDEO";

	/** The Constan RULE_MACHINE_LEARNING_RECOMMENDATION_ONE2ONE. */
	public static final String RULE_MACHINE_LEARNING_RECOMMENDATION_ONE2ONE = "RULE_MACHINE_LEARNING_RECOMMENDATION_ONE2ONE";

	/** The Constant RULE_BLUEPRINT_SCORE_CHANGED. */
	public static final String RULE_BLUEPRINT_SCORE_CHANGED = "RULE_BLUEPRINT_SCORE_CHANGED";

	/** The Constant RULE_BLUEPRINT_NOT_UPDATED. */
	public static final String RULE_BLUEPRINT_NOT_UPDATED = "RULE_BLUEPRINT_NOT_UPDATED";

	/** The Constant RULE_FPG_GOAL_UPDATE. */
	public static final String RULE_IMPORT_TENANT_PRODUCT = "RULE_IMPORT_TENANT_PRODUCT";

	/** The Constant AUTH_EXPIRED_EMAIL. */
	public static final String AUTH_EXPIRED_EMAIL = "AUTH_EXPIRED_EMAIL";

	/** The Constant AUTH_EXPIRED_NOTIFICATION. */
	public static final String AUTH_EXPIRED_NOTIFICATION = "AUTH_EXPIRED_NOTIFICATION";

	/** The Constant PASSWORD_VALIDATION_MESSAGE. */
	public static final String AUTHENTICATION_VALIDATION_MESSAGE = "Use 7 to 20 characters with at least one number or special character and one alpha character.";

	/** The Constant PASSWORD_PATTERN. */
	public static final String AUTHENTICATION_PATTERN = "((?=.*[a-zA-Z])((?=.*\\d)|(?=.*[\\\\!\"#$%&()*+,./:`';<=>?@\\[\\]^_{|}~])).{7,20})";

	/** The Constant PASSWORD_LENGTH_VALIDATION. */
	public static final String AUTHENTICATION_LENGTH_VALIDATION = "A password must be between 7 and 20 characters long.";

	/** The Constant ONBOARDING_INVITE. */
	public static final String ONBOARDING_INVITE = "ONBOARDING_INVITE";

	/** The Constant ONBOARD. */
	public static final String ONBOARD = "ONBOARD";

	/** The Constant ONBOARDING_USER_STATUS_UPDATE. */
	public static final String ONBOARDING_USER_STATUS_UPDATE = "ONBOARDING_USER_STATUS_UPDATE";

	/** The Constant RULE_VIDEO_RECOMMEND. */
	public static final String RULE_VIDEO_RECOMMEND = "RULE_VIDEO_RECOMMEND";

	/** The Constant RECOMMEND_VIDEO. */
	public static final String RECOMMEND_VIDEO = "RULE_TRAINING_COMPLETE";

	/**
	 * Sort by value.
	 *
	 * @param <K>
	 *            the key type
	 * @param <V>
	 *            the value type
	 * @param map
	 *            the map
	 * @return the map
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(
			final Map<K, V> map) {
		final List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(final Map.Entry<K, V> o1,
					final Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		final Map<K, V> result = new LinkedHashMap<>();
		for (final Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	/** The Constant HEADER_INDUSTRY_ID. */
	public static final String HEADER_INDUSTRY_ID = "IndustryId";

	/** The Constant HEADER_IS_MOBILE. */
	public static final String HEADER_IS_MOBILE = "isMobile";

	/** The Constant IS_SUPER_ADMIN. */
	public static final String IS_SUPER_ADMIN = "isSuperAdmin";

	/** The Constant IS_PERFORMANCE_MANAGER. */
	public static final String IS_PERFORMANCE_MANAGER = "isPerformanceManager";

	/** The Constant IS_DEFAULT_TENANT_ROLE. */
	public static final String IS_DEFAULT_TENANT_ROLE = "isDefaultTenantRole";

	/** The Constant IS_FRONTLINE_ASSOCIATE_ROLE. */
	public static final String IS_FRONTLINE_ASSOCIATE_ROLE = "isFrontLineAssociate";

	/** The Constant REGION. */
	public static final String REGION = "region";

	/** The Constant DIM_CALENDAR. */
	public static final String DIM_CALENDAR = "dimCalendar";

	/** The Constant DIM_TENANT_LOCATION. */
	public static final String DIM_TENANT_LOCATION = "dimTenantLocation";

	/** The Constant TENANT_LOCATION. */
	public static final String TENANT_LOCATION = "tenantLocationId";

	/** The Constant DIM_LOCATION_GROUP. */
	public static final String DIM_LOCATION_GROUP = "dimLocationGroup";

	/** The Constant LOCATION_GROUP. */
	public static final String LOCATION_GROUP = "locationGroupId";

	/** The Constant DIM_PRODUCT. */
	public static final String DIM_PRODUCT = "dimProduct";

	/** The Constant PRODUCT. */
	public static final String PRODUCT = "productId";

	/** The Constant DIM_USER. */
	public static final String DIM_USER = "dimUser";

	/** The Constant USER. */
	public static final String USER = "userId";

	/** The Constant MARKET_SEGMENT_1. */
	public static final String MARKET_SEGMENT_1 = "marketSegment1";

	/** The Constant MARKET_SEGMENT_2. */
	public static final String MARKET_SEGMENT_2 = "marketSegment2";

	/** The Constant RESERVATION_TYPE. */
	public static final String RESERVATION_TYPE = "reservationType";

	/** The Constant TENANT_REPORT. */
	public static final String TENANT_REPORT = "tenantReport";

	/** The Constant OTHER_REVENUE_FIELD_Daily. */
	public static final String OTHER_REVENUE_FIELD_Daily = "dailyOtherRevenue";

	/** The Constant OTHER_REVENUE_FIELD_Departure. */
	public static final String OTHER_REVENUE_FIELD_Departure = "'departureOtherRevenue'";

	/** The Constant OTHER_REVENUE_FIELD. */
	public static final String OTHER_REVENUE_FIELD = "departureOtherRevenue";

	/** The Constant UPSELL_REVENUE_FIELD_Daily. */
	public static final String UPSELL_REVENUE_FIELD_Daily = "otherRevenue";

	/** The Constant UPSELL_REVENUE_FIELD_Departure. */
	public static final String UPSELL_REVENUE_FIELD_Departure = "departureRoomUpsellRevenue";

	/** The Constant UPSELL_REVENUE_FIELD. */
	public static final String UPSELL_REVENUE_FIELD = "dailyRoomUpsellRevenue";

	/** The Constant IDR_CONVERSION_FD_COMPONENT_CODE. */
	public static final Object IDR_CONVERSION_FD_COMPONENT_CODE = "IDR";

	/** The Constant CONTEST_REPORT_OWNER_ASSIGN. */
	public static final String CONTEST_REPORT_OWNER_ASSIGN = "CONTEST_REPORT_OWNER_ASSIGN";

	/** The Constant CONTEST_REPORT_OWNER_REMOVAL. */
	public static final String CONTEST_REPORT_OWNER_REMOVAL = "CONTEST_REPORT_OWNER_REMOVAL";

	/** The Constant CONTEST_REPORT_USER_ASSIGN. */
	public static final String CONTEST_REPORT_USER_ASSIGN = "CONTEST_REPORT_USER_ASSIGN";

	/** The Constant CONTEST_REPORT_USER_REMOVAL. */
	public static final String CONTEST_REPORT_USER_REMOVAL = "CONTEST_REPORT_USER_REMOVAL";

	/** The Constant TOP. */
	public static final String TOP = "TOP";

	/** The Constant LEAST. */
	public static final String LEAST = "LEAST";

	/** The Constant MY. */
	public static final String MY = "MY";

	/** The Constant uploadDataCheckInterval. */
	public static final int uploadDataCheckInterval = 1800 * seconds;

	/** The Constant uploadDataFromWebCheckInterval. */
	public static final int uploadDataFromWebCheckInterval = 600 * seconds;

	/** The Constant uploadFileQueueCheckInterval. */
	public static final int uploadFileQueueCheckInterval = 20 * seconds;

	/** The Constant updateMediaInterval. */
	public static final int updateMediaInterval = 7200 * seconds;

	/** The Constant updateSolrDataInterval. */
	public static final int updateSolrDataInterval = 7200 * seconds;

	/** The Constant CUSTOM_REPORT_WIDGET_TYPE_BLOCK. */
	public static final String CUSTOM_REPORT_WIDGET_TYPE_BLOCK = "block";

	/** The Constant CUSTOM_REPORT_WIDGET_TYPE_TABLE. */
	public static final String CUSTOM_REPORT_WIDGET_TYPE_TABLE = "table";

	/** The Constant CONTEST_REPORT_WIDGET_TYPE_TABLE. */
	public static final String CONTEST_REPORT_WIDGET_TYPE_TABLE = "table";

	/** The Constant CAL_EVENT_TYPE_OBSERVATION. */
	public static final String CAL_EVENT_TYPE_OBSERVATION = "OBSERVATION";

	/** The Constant CAL_EVENT_TYPE_COUNTER_COACHING. */
	public static final String CAL_EVENT_TYPE_COUNTER_COACHING = "COUNTER_COACHING";

	/** The Constant CAL_EVENT_TYPE_ONE_2_ONE. */
	public static final String CAL_EVENT_TYPE_ONE_2_ONE = "ONE_TO_ONE";

	/** The Constant CAL_EVENT_TYPE_TRAINING. */
	public static final String CAL_EVENT_TYPE_TRAINING = "TRAINING";

	/** The Constant SURVEYOR. */
	public static final String SURVEYOR = "SURVEYOR";

	/** The Constant RESPONDENT. */
	public static final String RESPONDENT = "RESPONDENT";

	// User export headers

	/** The Constant PULSE_ID. */
	public static final String PULSE_ID = "Pulse ID";

	/** The Constant EMPLOYEE_ID. */
	public static final String EMPLOYEE_ID = "Employee ID";

	/** The Constant EMAIL. */
	public static final String EMAIL = "Email";

	/** The Constant FIRST_NAME. */
	public static final String FIRST_NAME = "FirstName";

	/** The Constant MIDDLE_NAME. */
	public static final String MIDDLE_NAME = "MiddleName";

	/** The Constant LAST_NAME. */
	public static final String LAST_NAME = "LastName";

	/** The Constant GENDER. */
	public static final String GENDER = "Gender";

	/** The Constant PHONE_NUMBER. */
	public static final String PHONE_NUMBER = "PhoneNumber";

	/** The Constant HIRED_DATE. */
	public static final String HIRED_DATE = "HiredDate";

	/** The Constant BIRTH_DATE. */
	public static final String BIRTH_DATE = "BirthDate";

	/** The Constant FPG_LEVEL. */
	public static final String FPG_LEVEL = "FPGLevel";

	/** The Constant STATUS. */
	public static final String STATUS = "Status";

	/** The Constant TIME_STAMP. */
	public static final String TIME_STAMP = "TimeStamp";

	/** The Constant UPDATED_TIME. */
	public static final String UPDATED_TIME = "UpdateTime";

	/** The Constant CONTACT_EMAIL. */
	public static final String CONTACT_EMAIL = "Contact Email";

	/** The Constant USER_VAL. */
	public static final String USER_VAL = "User ID";

	/** The Constant SIGNED_IN_TIME. */
	public static final String SIGNED_IN_TIME = "SignedInTime";

	/** The Constant ROLES. */
	public static final String ROLES = "Roles";

	/** The Constant GOAL_CURRENT_DATE. */
	public static final String GOAL_CURRENT_DATE = "Current_Date";

	/** The Constant TOTAL_UPSELL_AMOUNT_FIELD. */
	public static final String TOTAL_UPSELL_AMOUNT_FIELD = "totalUpsellAmount";

	/** The Constant MAJOR_UPSELL_CHARGE. */
	public static final String MAJOR_UPSELL_CHARGE = "majorUpsellCharge";

	/** The Constant NO_OF_UPSELL_ROOM_NIGHTS. */
	public static final String NO_OF_UPSELL_ROOM_NIGHTS = "noOfUpsellRoomNights";

	/** The Constant NO_OF_ROOM_UPSELL. */
	public static final String NO_OF_ROOM_UPSELL = "noOfRoomUpsell";

	/** The Constant UPSELL_ROOM_AVERAGE_RATE. */
	public static final String UPSELL_ROOM_AVERAGE_RATE = "upsellRoomAverageRate";

	/** The Constant IDR_FIELD. */
	public static final String IDR_FIELD = "idr";

	/** The Constant CONVERSION. */
	public static final String CONVERSION = "conversion";

	/** The Constant PROFIT_CONTRIBUTION_FIELD. */
	public static final String PROFIT_CONTRIBUTION_FIELD = "profitContribution";

	/** The Constant CONTRIBUTION_MARGIN. */
	public static final String CONTRIBUTION_MARGIN = "contributionMargin";

	/** The Constant CHECKED_IN_NIGHTS_FIELD. */
	public static final String CHECKED_IN_NIGHTS_FIELD = "checkedInNights";

	/** The Constant AVERAGE_REVENUE_PER_ORDER. */
	public static final String AVERAGE_REVENUE_PER_ORDER = "averageRevenuePerOrder";

	/** The Constant CALL_ORDERS_FIELD. */
	public static final String CALL_ORDERS_FIELD = "CallOrders";

	/** The Constant ADR_CONVERSION. */
	public static final String ADR_CONVERSION = "adr";

	/** The Constant UPSELL_FLAG_FIELD. */
	public static final String UPSELL_FLAG_FIELD = "UpselFlag";

	/** The Constant ROOM_CONVERSION. */
	public static final String ROOM_CONVERSION = "roomConversion";

	/** The Constant RPC_FIELD. */
	public static final String RPC_FIELD = "idr";

	/** The Constant UO_CONVERSION. */
	public static final String UO_CONVERSION = "uoConversion";

	// SonarBug: added final to every field from here
	/** The Constant UPGRADE_ADMISSION. */
	public static final String UPGRADE_ADMISSION = "upgradeAdmission";

	/** The Constant TRANSACTIONS. */
	public static final String TRANSACTIONS = "transactions";

	/** The Constant ADMISSION_UNIT. */
	public static final String ADMISSION_UNIT = "admissionUnits";

	/** The Constant ADMISSION_REVENUE. */
	public static final String ADMISSION_REVENUE = "admissionRevenue";

	/** The Constant ANCILLARY_UNIT. */
	public static final String ANCILLARY_UNIT = "ancillaryUnits";

	/** The Constant ANCILLARY_REVENUE. */
	public static final String ANCILLARY_REVENUE = "ancillaryRevenue";

	/** The Constant DAO_SEARCH_RESULTS. */
	public static final String DAO_SEARCH_RESULTS = "searchResults";

	/** The Constant DAO_SEARCH_RESULTS_TOTAL_COUNT. */
	public static final String DAO_SEARCH_RESULTS_TOTAL_COUNT = "totalResultCount";

	/** The Constant GMT_TIME_ZONE_TEXT. */
	public static final String GMT_TIME_ZONE_TEXT = "GMT";

	/** The Constant DAO_SEARCH_RESULTS_MAX. */
	public static final Long DAO_SEARCH_RESULTS_MAX = 1000000L;

	/** The Constant UPSELL_PRODUCTS_LABLE. */
	public static final String UPSELL_PRODUCTS_LABLE = "Upsell Products";

	/** The Constant MINCOLVALUE. */
	public static final Integer MINCOLVALUE = 6;

	/** The Constant MAXCOLVALUE. */
	public static final Integer MAXCOLVALUE = 10;

	/** The Constant DATA_CHECK_INTERVAL. */
	public static final int DATA_CHECK_INTERVAL = 3600 * seconds;

	/** The Constant INDUSTRY_HOTEL. */
	public static final String INDUSTRY_HOTEL = "Hotel";

	/** The Constant EMAIL_SUBJECT. */
	public static final String EMAIL_SUBJECT = "email_subject";

	/** The Constant TEMPLATE_FILE_NO_OF_PROCESSED_RECORDS. */
	public static final String TEMPLATE_FILE_NO_OF_PROCESSED_RECORDS = "process_records";

	/** The Constant TEMPLATE_FILE_NO_OF_FAILED_RECORDS. */
	public static final String TEMPLATE_FILE_NO_OF_FAILED_RECORDS = "failed_records";

	/** The Constant AUTO_LOCK_INTERVAL. */
	public static final int AUTO_LOCK_INTERVAL = 900 * seconds;

	/** The Constant UPDATE_INCENTIVE_INTERVAL. */
	public static final int UPDATE_INCENTIVE_INTERVAL = 1 * hours;

	/** The Constant UPDATE_INCENTIVE_INTERVAL. */
	public static final int UPDATE_FULL_INCENTIVE_INTERVAL = 30 * minutes;

	/** The Constant checkForReportTransformationInterval. */
	public static final int checkForReportTransformationInterval = 1 * hours;

	/** The Constant checkForDataUploadUtilityInterval. */
	public static final int checkForDataUploadUtilityInterval = 1 * hours;

	/** The Constant solrUpdationInterval. */
	public static final int solrUpdationInterval = 1 * hours;

	/** The Constant solrEmailLogUpdationInterval. */
	public static final int solrEmailLogUpdationInterval = 120 * seconds;

	/** The Constant ASSIGNED_TENANT. */
	public static final String ASSIGNED_TENANT = "Assigned Tenants";

	/** The Constant INCENTIVE_PLAN_FROM_DATE. */
	public static final String INCENTIVE_PLAN_FROM_DATE = "incentive_plan_from_date";

	/** The Constant INCENTIVE_PLAN_TO_DATE. */
	public static final String INCENTIVE_PLAN_TO_DATE = "incentive_plan_to_date";

	/** The Constant ASSIGNED_LOCATION. */
	public static final String ASSIGNED_LOCATION = "Assigned Locations";

	/** The Constant recommendationsMailSendInterval. */
	public static final int recommendationsMailSendInterval = 720 * hours; // 30
																			// days
	/** The Constant WRITE_PERMISSION. */
	public static final String WRITE_PERMISSION = "Write";

	/** The Constant CREATED_TIME. */
	public static final String CREATED_TIME = "CreateTime";

	/** The Constant CREATED_ON. */
	public static final String CREATED_BY = "Created By";

	/** The Constant UPDATED_ON. */
	public static final String UPDATED_BY = "Updated By";

	/** The Constant RESULT. */
	public static final String RESULT = "result";

	/** The Constant ISMultiple. */
	public static final String ISMultiple = "isMultiple";

	/** The Constant IS_AVAIL_FOR_ONETOONE. */
	public static final String IS_AVAIL_FOR_ONETOONE = "isAvailableForTakeOneToOne";

	/** The Constant IS_AVAIL_FOR_TAKE_OBSERVATION. */
	public static final String IS_AVAIL_FOR_TAKE_OBSERVATION = "isAvailableForTakeObservation";

	/** The Constant IS_AVAIL_FOR_TAKE_COACHING. */
	public static final String IS_AVAIL_FOR_TAKE_COACHING = "isAvailableForTakeCoaching";

	/** The Constant IS_BETA_TESTER. */
	public static final String IS_BETA_TESTER = "isBetaTester";

	public static final String IS_OPERATOR_LOCATIONLIST = "isOperatorLocationList";
	public static final String IS_CONTRIBUTOR_LOCATIONLIST = "isContributorLocationList";

	/** The Constant dashboardTypes. */
	protected static final HashMap<Integer, String> dashboardTypes;
	static {
		dashboardTypes = new HashMap<Integer, String>();
		dashboardTypes.put(1, "custom-dashboard");
		dashboardTypes.put(2, "howamidoing-dashboard");
		dashboardTypes.put(3, "howdoiimprove-dashboard");
	}

	/** The feed type unique key map. */
	public static HashMap<String, String[]> feedTypeUniqueKeyMap;
	static {
		feedTypeUniqueKeyMap = new HashMap<>();
		feedTypeUniqueKeyMap.put(FeedType.TakeBlueprint.name(),
				new String[] { "tenantLocationId", "month", "year" });
	}
}
