/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.job.portal.pojo.enums.SolrEntityType;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants.
 */
public class Constants {

	/** The Constant USER. */
	public static final String USER = "user";

	/** The Constant USER_LOCATION_GROUP. */
	public static final String USER_LOCATION_GROUP = "user_location_group";

	/** The Constant TIMERTASK. */
	public static final String TIMERTASK = "timertask";

	/** The Constant LOCATIONMETRIC. */
	// public static final String LOCATIONMETRIC = "location_metric";

	/** The Constant USER_TENANT_ROLE. */
	public static final String USER_TENANT_ROLE = "user_tenant_role";

	/** The Constant GROUPMETRIC. */
	public static final String GROUPMETRIC = "location_group_metric";

	/** The Constant LOCATION_GROUP_TYPE. */
	public static final String LOCATION_GROUP_TYPE = "location_group_type";

	/** The Constant REGION_TYPE. */
	public static final String REGION_TYPE = "region_type";

	/** The Constant LOCATION_GROUP_PRODUCT. */
	public static final String LOCATION_GROUP_PRODUCT = "location_group_product";

	/** The Constant PERMISSION. */
	public static final String PERMISSION = "permission";

	/** The Constant TENANT_PRODUCT. */
	public static final String TENANT_PRODUCT = "tenant_product";

	/** The Constant TENANT_PRODUCT_GROUP. */
	public static final String TENANT_PRODUCT_GROUP = "tenant_product_group";

	/** The Constant TENANT_REPORT. */
	public static final String TENANT_REPORT = "tenant_report";

	/** The Constant TENANT_COMPONENT. */
	public static final String TENANT_COMPONENT = "tenant_component";

	/** The Constant LOCATION_GROUP. */
	public static final String LOCATION_GROUP = "location_group";

	/** The Constant REGION. */
	public static final String REGION = "region";

	/** The Constant INDUSTRY. */
	public static final String INDUSTRY = "industry";

	/** The Constant ROLE. */
	public static final String ROLE = "role";

	/** The Constant TENANT. */
	public static final String TENANT = "tenant";

	/** The Constant LOCATIONMETRICAUDITTRAIL. */
	public static final String LOCATIONMETRICAUDITTRAIL = "locationMetricAuditTrail";

	/** The Constant LOCATION. */
	public static final String LOCATION = "location";

	/** The Constant BASELINE. */
	public static final String BASELINE = "baseline";

	/** The Constant TARGET. */
	public static final String TARGET = "target";

	/** The Constant SURVEY. */
	public static final String SURVEY = "survey";

	/** The Constant MACHINELEARNING. */
	public static final String MACHINELEARNING = "machineLearning";

	/** The Constant SURVEY_CATEGORY. */
	public static final String SURVEY_CATEGORY = "survey_category";

	/** The Constant FIELDSCHEME. */
	public static final String FIELDSCHEME = "fieldscheme";

	/** The Constant REMINDER. */
	public static final String REMINDER = "reminder";

	/** The Constant QUESTIONNAIRE. */
	public static final String QUESTIONNAIRE = "questionnaire";

	/** The Constant QUESTION. */
	public static final String QUESTION = "question";

	/** The Constant INCENTIVE_PLAN. */
	public static final String INCENTIVE_PLAN = "incentive_plan";

	/** The Constant QUESTION_ANSWER. */
	public static final String QUESTION_ANSWER = "question_answer";

	/** The Constant TENANT_LOCATION_PRODUCT_CATEGORY. */
	public static final String TENANT_LOCATION_PRODUCT_CATEGORY = "tenant_location_product_category";

	/** The Constant TENANT_PRODUCT_CATEGORY. */
	public static final String TENANT_PRODUCT_CATEGORY = "tenant_product_category";

	/** The Constant BLUEPRINT_LOCATION_SUMMARY. */
	public static final String BLUEPRINT_LOCATION_SUMMARY = "blueprint_location_summary";

	/** The Constant EVENTLOG. */
	public static final String EVENTLOG = "eventlog";

	/** The Constant RANKING. */
	public static final String RANKING = "ranking";

	/** The Constant ONETOONE. */
	public static final String ONETOONE = "one_to_one";

	/** The Constant ONETOONETYPE. */
	public static final String ONETOONETYPE = "oneToOneType";

	/** The Constant ONETOONEUSER. */
	public static final String ONETOONEUSER = "one_to_one_user";

	/** The Constant VERDICT. */
	public static final String VERDICT = "verdict";

	/** The Constant GOAL. */
	public static final String GOAL = "goal";

	/** The Constant LANGUAGES. */
	public static final String LANGUAGES = "languages";

	/** The Constant MEDIATYPE. */
	public static final String MEDIATYPE = "mediaType";

	/** The Constant LOCATIONMARKETSEGMENT. */
	public static final String LOCATIONMARKETSEGMENT = "locationMarketSegment";

	/** The Constant DATE_TIME_FORMAT. */
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	/** The Constant DATE_TIME_12HR_FORMAT. */
	public static final String DATE_TIME_12HR_FORMAT = "yyyy-MM-dd hh:mm a";

	/** The Constant DISPLAY_DATE_FORMAT. */
	public static final String DISPLAY_DATE_FORMAT = "MMM dd";

	/** The Constant REPORT_DATE_FORMAT. */
	public static final String REPORT_DATE_FORMAT = "MMM dd, yyyy";

	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** The Constant TIME_FORMAT. */
	public static final String TIME_FORMAT = "HH:mm:ss";

	/** The Constant TIME_AM_PM_FORMAT. */
	public static final String TIME_AM_PM_FORMAT = "hh:mm a";

	/** The Constant YEAR_MONTH_FORMAT. */
	public static final String YEAR_MONTH_FORMAT = "yyyy-MM";

	/** The Constant BIRTH_DATE_FORMAT. */
	public static final String BIRTH_DATE_FORMAT = "dd-MMM";

	/** The Constant FAIL. */
	public static final String FAIL = "FAIL";

	/** The Constant SUCCESS. */
	public static final String SUCCESS = "SUCCESS";

	/** The Constant ENTITY_ID_SOLR_PARAM. */
	public static final String ENTITY_ID_SOLR_PARAM = "entity_id";

	/** The Constant NORMAL. */
	public static final String NORMAL = "NORMAL";

	/** The Constant RANGE_POINTS. */
	public static final String RANGE_POINTS = "RANGE_POINTS";

	/** The Constant RANGE_DATE. */
	public static final String RANGE_DATE = "RANGE_DATE";

	/** The Constant EMAIL_LOG. */
	public static final String EMAIL_LOG = "emaillog";

	/** The Constant PREFERENCE. */
	public static final String PREFERENCE = "preferences";

	/** The Constant INVITATION. */
	public static final String INVITATION = "invitation";

	/** The Constant FEEDBACK. */
	public static final String FEEDBACK = "feedback";

	/** The Constant MEDIA. */
	public static final String MEDIA = "media";

	/** The Constant USER_MEDIA_ACCESS. */
	public static final String USER_MEDIA_ACCESS = "userMediaAccess";

	/** The Constant MEDIA_FAVOURITE. */
	public static final String MEDIA_FAVOURITE = "mediaFavourite";

	/** The Constant DATE_TIME_HOURS_MINUTES_FORMAT. */
	public static final String DATE_TIME_HOURS_MINUTES_FORMAT = "dd-MM-yyyy hh:mm a";

	/** The Constant INDUSTRY_MENU. */
	public static final String INDUSTRY_MENU = "industry_menu";

	/** The Constant TENANT_MENU. */
	public static final String TENANT_MENU = "tenant_menu";

	/** The Constant objectMapper. */
	public static final ObjectMapper objectMapper = new ObjectMapper();

	/** The Constant USER_SESSION. */
	public static final String USER_SESSION = "user_session";

	/** The Constant REVENUE_MANAGEMENT. */
	public static final String REVENUE_MANAGEMENT = "revenue_management";

	/** The Constant solrCoreClassMapping. */
	@SuppressWarnings("rawtypes")
	public static final Map<String, Class> solrCoreClassMapping = new HashMap<>();

	/** The Constant solrCoreEntityMapping. */
	protected static final Map<String, SolrEntityType> solrCoreEntityMapping = new HashMap<>();

	/** The Constant classToSolrCoreMapping. */
	@SuppressWarnings("rawtypes")
	protected static final Map<Class, String> classToSolrCoreMapping = new HashMap<>();

	/** The Constant classToEntityMapping. */
	@SuppressWarnings("rawtypes")
	protected static final Map<Class, String> classToEntityMapping = new HashMap<>();

	/** The Constant INDUSTRY_ROUTE. */
	public static final String INDUSTRY_ROUTE = "industry_route";

	/** The Constant INTACCTCUSOMER. */
	public static final String INTACCTCUSOMER = "intacct_customer";

	/** The Constant USERMESSAGE. */
	public static final String USERMESSAGE = "userMessage";

	/** The Constant CALENDAR_EVENT. */
	public static final String CALENDAR_EVENT = "calendarEvent";

	/** The Constant CALENDAR. */
	public static final String CALENDAR = "calendar";

	/** The Constant MEETING. */
	public static final String MEETING = "meeting";

	/** The Constant TRAINING_COURSE. */
	public static final String TRAINING_COURSE = "training_course";

	/** The Constant TRAINING_CATEGORY. */
	public static final String TRAINING_CATEGORY = "training_category";

	/** The Constant TRAINING_CHAPTER. */
	public static final String TRAINING_CHAPTER = "trainingChapter";

	/** The Constant TRAININGWATCHDOGS. */
	public static final String TRAININGWATCHDOGS = "trainingWatchdogs";

	/** The Constant TRAINING_RECOMMEND. */
	public static final String TRAINING_RECOMMEND = "training_recommend";

	/** The Constant TRAINING_FAVOURITE. */
	public static final String TRAINING_FAVOURITE = "training_favourite";

	/** The Constant TRAINING_VIDEO_NOTES. */
	public static final String TRAINING_VIDEO_NOTES = "training_video_notes";

	/** The Constant TRAININGQUESTIONNAIRE. */
	public static final String TRAININGQUESTIONNAIRE = "training_questionnaire";

	/** The Constant TRAININGUSERCERTIFICATES. */
	public static final String TRAININGUSERCERTIFICATES = "training_user_certificates";

	/** The Constant CONDUCT_TRAINING_SUMMARY. */
	public static final String CONDUCT_TRAINING_SUMMARY = "conduct_training_summary";

	/** The Constant DASHBOARD_WEIGHT. */
	public static final String DASHBOARD_WEIGHT = "dashboard_weight";

	/** The Constant VPLACTIVITY. */
	public static final String VPLACTIVITY = "vplActivity";

	/** The Constant VPLACTIVITYTYPE. */
	public static final String VPLACTIVITYTYPE = "vplActivityType";

	/** The Constant COMMANDCENTERACTION. */
	public static final String COMMANDCENTERACTION = "commandCenterAction";

	/** The Constant CONFIGURE_SESSION. */
	public static final String CONFIGURE_SESSION = "app.enabled.session";

	/** The Constant SESSION_TIME. */
	public static final String SESSION_TIME = "app.session.expired";

	/** The Constant BYPASS_SECURE_PASSWORD_RANDDOM_STRING_TO_STRENTHEN. */
	public static final String BYPASS_SECURE_PASSWORD_RANDDOM_STRING_TO_STRENTHEN = "BYPASS_SECURE_PASSWORD_RANDDOM_STRING_TO_STRENTHEN";

	static {

	}

	/**
	 * Instantiates a new constants.
	 */
	private Constants() {

	}

	/**
	 * Gets the class for solr core.
	 *
	 * @param core
	 *            the core
	 * @return the class for solr core
	 */
	@SuppressWarnings("rawtypes")
	public static Class getClassForSolrCore(final String core) {
		return solrCoreClassMapping.get(core);
	}

	/**
	 * Gets the solr entity type for solr core.
	 *
	 * @param core
	 *            the core
	 * @return the solr entity type for solr core
	 */
	public static SolrEntityType getSolrEntityTypeForSolrCore(
			final String core) {
		return solrCoreEntityMapping.get(core);
	}

	/**
	 * Gets the solr core for class.
	 *
	 * @param classIns
	 *            the class ins
	 * @return the solr core for class
	 */
	public static String getSolrCoreForClass(
			@SuppressWarnings("rawtypes") final Class classIns) {
		return classToSolrCoreMapping.get(classIns);
	}

	/**
	 * Gets the entity name for class.
	 *
	 * @param classIns
	 *            the class ins
	 * @return the entity name for class
	 */
	public static String getEntityNameForClass(
			@SuppressWarnings("rawtypes") final Class classIns) {
		return classToEntityMapping.get(classIns);
	}

	/**
	 * Gets the class for core.
	 *
	 * @param coreName
	 *            the core name
	 * @return the class for core
	 */
	@SuppressWarnings("rawtypes")
	public static Class getClassForCore(final String coreName) {
		for (final Map.Entry<Class, String> entry : classToSolrCoreMapping
				.entrySet()) {
			final Class key = entry.getKey();
			final String value = entry.getValue();
			if (value.equals(coreName)) {
				return key;
			}
		}
		return null;
	}
}
