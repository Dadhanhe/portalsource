/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
//Created by Sapan on 29/11/16.
/**
 * The Class UnInitializedPropertyStripper.
 */
@Component
public class UnInitializedPropertyStripper {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(UnInitializedPropertyStripper.class);

}
