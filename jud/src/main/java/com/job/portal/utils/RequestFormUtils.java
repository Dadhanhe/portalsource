/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class RequestFormUtils.
 */
public class RequestFormUtils {

	/**
	 * Save url file.
	 *
	 * @param fileUrl
	 *            the file url
	 * @param fileDes
	 *            the file des
	 * @throws Exception
	 *             the exception
	 */
	public static void saveUrlFile(final String fileUrl, final String fileDes)
			throws Exception {
		final File toFile = new File(fileDes);
		if (toFile.exists()) {
			// throw new Exception("file exist");
			return;
		}
		// toFile.createNewFile();
		final FileOutputStream outImgStream = new FileOutputStream(toFile);
		try {
			outImgStream.write(getUrlFileData(fileUrl));
		} catch (final Exception e) {
			throw e;
		} finally {
			outImgStream.close();
		}
	}

	/**
	 * Gets the url file data.
	 *
	 * @param fileUrl
	 *            the file url
	 * @return the url file data
	 * @throws Exception
	 *             the exception
	 */
	public static byte[] getUrlFileData(final String fileUrl) throws Exception {
		final URL url = new URL(fileUrl);
		final HttpURLConnection httpConn = (HttpURLConnection) url
				.openConnection();
		httpConn.connect();
		final InputStream cin = httpConn.getInputStream();
		final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		final byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = cin.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		cin.close();
		final byte[] fileData = outStream.toByteArray();
		outStream.close();
		return fileData;
	}

	/**
	 * Checks if is decimal.
	 *
	 * @param str
	 *            the str
	 * @return true, if is decimal
	 */
	public static boolean isDecimal(final String str) {
		if (Objects.isEmpty(str)) {
			return false;
		}
		return Pattern.compile("^[\\-]?[0-9]+\\.{0,1}[0-9]{0,2}$").matcher(str)
				.matches();
	}
}
