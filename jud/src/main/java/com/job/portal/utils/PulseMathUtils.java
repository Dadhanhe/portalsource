/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Calendar;

// TODO: Auto-generated Javadoc
/**
 * The Class PulseMathUtils.
 */
public class PulseMathUtils {

	/**
	 * Average.
	 *
	 * @param data
	 *            the data
	 * @return the double
	 */
	public static double average(final double[] data) {
		if (data.length == 0) {
			return 0;
		}
		final double sum = sum(data);
		final double average = sum / data.length;
		return average;
	}

	/**
	 * Sum.
	 *
	 * @param data
	 *            the data
	 * @return the double
	 */
	public static double sum(final double[] data) {
		double sum = 0.0;
		for (final double d : data) {
			sum += d;
		}
		return sum;
	}

	/**
	 * Variance.
	 *
	 * @param data
	 *            the data
	 * @return the double
	 */
	public static double variance(final double[] data) {
		final double average = average(data);
		double totalVariance = 0d;
		for (final double d : data) {
			totalVariance += Math.pow((average - d), 2d);
		}
		return totalVariance / data.length;
	}

	/**
	 * Standard deviation.
	 *
	 * @param data
	 *            the data
	 * @return the double
	 */
	public static double standard_deviation(final double[] data) {
		final double variance = variance(data);
		return Math.sqrt(variance);
	}

	/**
	 * Round.
	 *
	 * @param d
	 *            the d
	 * @param floats
	 *            the floats
	 * @return the double
	 */
	public static double round(final double d, final int floats) {
		final double floatsTens = Math.pow(10d, floats);
		return (Math.round(d * floatsTens)) / floatsTens;
	}

	/**
	 * End of month value.
	 *
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @param value
	 *            the value
	 * @return the double
	 */
	public static double endOfMonthValue(final Integer month,
			final Integer year, final double value) {
		final double finalVal = 0;
		final Calendar cal = Calendar.getInstance();
		final double maxDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		final double yesterday = cal.get(Calendar.DATE) - 1d;
		final Integer curMonth = cal.get(Calendar.MONTH);
		final Integer curYear = cal.get(Calendar.YEAR);
		if (curYear.intValue() >= (year.intValue())
				&& curMonth.intValue() != (month.intValue() - 1)) {
			return value;
		} else if ((curYear.intValue() > year.intValue())
				|| (curYear.intValue() == year.intValue()
						&& curMonth.intValue() >= (month.intValue() - 1))) {
			if (yesterday == 0) {
				return (((value) / 1) * maxDays);
			} else {
				return (((value) / yesterday) * maxDays);
			}
		}
		return finalVal;
	}

}
