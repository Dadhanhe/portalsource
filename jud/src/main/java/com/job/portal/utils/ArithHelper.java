/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ArithHelper.
 */
public class ArithHelper {

	// 默认除法运算精度
	/** The Constant DEF_DIV_SCALE. */
	private static final int DEF_DIV_SCALE = 16;

	/**
	 * Adds the.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double add(final double v1, final double v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(
				Double.toString(v1));
		final java.math.BigDecimal b2 = new java.math.BigDecimal(
				Double.toString(v2));
		return b1.add(b2).doubleValue();
	}

	/**
	 * Adds the.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double add(final String v1, final String v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(v1);
		final java.math.BigDecimal b2 = new java.math.BigDecimal(v2);
		return b1.add(b2).doubleValue();
	}

	/**
	 * Div.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double div(final double v1, final double v2) {
		return div(v1, v2, DEF_DIV_SCALE);
	}

	/**
	 * Div.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @param scale
	 *            the scale
	 * @return the double
	 */
	public static double div(final double v1, final double v2,
			final int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The   scale   must   be   a   positive   integer   or   zero");
		}
		final java.math.BigDecimal b1 = new java.math.BigDecimal(
				Double.toString(v1));
		final java.math.BigDecimal b2 = new java.math.BigDecimal(
				Double.toString(v2));
		return b1.divide(b2, scale, java.math.BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	/**
	 * Div.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double div(final String v1, final String v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(v1);
		final java.math.BigDecimal b2 = new java.math.BigDecimal(v2);
		return b1.divide(b2, DEF_DIV_SCALE, java.math.BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	/**
	 * Mul.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double mul(final double v1, final double v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(
				Double.toString(v1));
		final java.math.BigDecimal b2 = new java.math.BigDecimal(
				Double.toString(v2));
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * Mul.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double mul(final String v1, final String v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(v1);
		final java.math.BigDecimal b2 = new java.math.BigDecimal(v2);
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * Round.
	 *
	 * @param v
	 *            the v
	 * @param scale
	 *            the scale
	 * @return the double
	 */
	public static double round(final double v, final int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The   scale   must   be   a   positive   integer   or   zero");
		}
		final java.math.BigDecimal b = new java.math.BigDecimal(
				Double.toString(v));
		final java.math.BigDecimal one = new java.math.BigDecimal("1");
		return b.divide(one, scale, java.math.BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	/**
	 * Round.
	 *
	 * @param v
	 *            the v
	 * @param scale
	 *            the scale
	 * @return the double
	 */
	public static double round(final String v, final int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The   scale   must   be   a   positive   integer   or   zero");
		}
		final java.math.BigDecimal b = new java.math.BigDecimal(v);
		final java.math.BigDecimal one = new java.math.BigDecimal("1");
		return b.divide(one, scale, java.math.BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	/**
	 * Sub.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double sub(final double v1, final double v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(
				Double.toString(v1));
		final java.math.BigDecimal b2 = new java.math.BigDecimal(
				Double.toString(v2));
		return b1.subtract(b2).doubleValue();
	}

	/**
	 * Sub.
	 *
	 * @param v1
	 *            the v 1
	 * @param v2
	 *            the v 2
	 * @return the double
	 */
	public static double sub(final String v1, final String v2) {
		final java.math.BigDecimal b1 = new java.math.BigDecimal(v1);
		final java.math.BigDecimal b2 = new java.math.BigDecimal(v2);
		return b1.subtract(b2).doubleValue();
	}

	// 这个类不能实例化
	/**
	 * Instantiates a new arith helper.
	 */
	private ArithHelper() {
	}
}
