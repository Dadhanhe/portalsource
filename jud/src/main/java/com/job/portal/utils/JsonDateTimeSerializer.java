/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonDateTimeSerializer.
 */
@Component
public class JsonDateTimeSerializer extends JsonSerializer<Date> {

	/**
	 * The Constant dateFormat.
	 *
	 * @param date
	 *            the date
	 * @param gen
	 *            the gen
	 * @param provider
	 *            the provider
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException
	 *             the json processing exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
	 * com.fasterxml.jackson.core.JsonGenerator,
	 * com.fasterxml.jackson.databind.SerializerProvider)
	 */
	@Override
	public void serialize(final Date date, final JsonGenerator gen,
			final SerializerProvider provider)
			throws IOException, JsonProcessingException {
		/*
		 * HttpServletRequest request = ((ServletRequestAttributes)
		 * RequestContextHolder .getRequestAttributes()).getRequest(); String
		 * timeZone = Objects.getTimeZoneFromHeader(request);
		 */
		final SimpleDateFormat sdf = new SimpleDateFormat(
				Constants.DATE_TIME_FORMAT);
		/*
		 * SimpleDateFormat sdfLocal = new SimpleDateFormat(
		 * Constants.DATE_TIME_FORMAT);
		 * sdf.setTimeZone(TimeZone.getTimeZone(SystemConfigs.GMT_TIME_ZONE_TEXT
		 * )); String formattedDate1 = sdf.format(date); Date date1 = date; try
		 * { date1 = sdfLocal.parse(formattedDate1); } catch (ParseException e)
		 * { // TODO Auto-generated catch block ObjectUtils.logException(logger,
		 * e, null); } sdf.setTimeZone(TimeZone
		 * .getTimeZone(Objects.isNotNull(timeZone) ? timeZone :
		 * SystemConfigs.GMT_TIME_ZONE_TEXT));
		 */
		final String formattedDate = sdf.format(date);
		gen.writeString(formattedDate);
	}

}
