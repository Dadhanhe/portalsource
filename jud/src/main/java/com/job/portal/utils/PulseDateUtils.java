/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.job.portal.utils.Objects.Months;

// TODO: Auto-generated Javadoc
/**
 * The Class PulseDateUtils.
 */
public class PulseDateUtils {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(PulseDateUtils.class);

	/**
	 * Gets the UTC date from time.
	 *
	 * @param date
	 *            the date
	 * @return the UTC date from time
	 */
	public static Date getUTCDateFromTime(final String date) {
		if (date != null) {
			return new Date(Long.valueOf(date));
		}
		return null;
	}

	/**
	 * Parses the date.
	 *
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the date
	 */
	public static Date parseDate(final String date, final String format) {
		Date dateObj = null;
		try {
			dateObj = (Objects.isEmpty(date)) ? null
					: new SimpleDateFormat(format).parse(date);
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, "Failed to parse date");
		}
		return dateObj;
	}

	/**
	 * Parses the date.
	 *
	 * @param date
	 *            the date
	 * @return the date
	 */
	public static Date parseDate(final String date) {
		return parseDate(date, SystemConfigs.DATE_FORMAT);
	}

	/**
	 * Format.
	 *
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return the string
	 */
	public static String format(final Date date, final String format) {
		return (date == null) ? "" : new SimpleDateFormat(format).format(date);
	}

	/**
	 * Format.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String format(final Date date) {
		return (date == null) ? "" : format(date, SystemConfigs.DATE_FORMAT);
	}

	/**
	 * Checks if is valid format.
	 *
	 * @param format
	 *            the format
	 * @param value
	 *            the value
	 * @return true, if is valid format
	 */
	public static boolean isValidFormat(final String format,
			final String value) {
		Date date = null;
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (final ParseException ex) {
		}
		return date != null;
	}

	/**
	 * Difference in months.
	 *
	 * @param beginningDate
	 *            the beginning date
	 * @param endingDate
	 *            the ending date
	 * @return the integer
	 */
	public static Integer differenceInMonths(final Date beginningDate,
			final Date endingDate) {
		if (beginningDate == null || endingDate == null) {
			return 0;
		}
		final Calendar cal1 = new GregorianCalendar();
		cal1.setTime(beginningDate);
		final Calendar cal2 = new GregorianCalendar();
		cal2.setTime(endingDate);
		return differenceInMonths(cal1, cal2);
	}

	/**
	 * Adds the days to date.
	 *
	 * @param date
	 *            the date
	 * @param days
	 *            the days
	 * @return the date
	 */
	public static Date addDaysToDate(final Date date, final int days) {
		final Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	/**
	 * Difference in months.
	 *
	 * @param beginningDate
	 *            the beginning date
	 * @param endingDate
	 *            the ending date
	 * @return the integer
	 */
	private static Integer differenceInMonths(final Calendar beginningDate,
			final Calendar endingDate) {
		if (beginningDate == null || endingDate == null) {
			return 0;
		}
		final int m1 = beginningDate.get(Calendar.YEAR) * 12
				+ beginningDate.get(Calendar.MONTH);
		final int m2 = endingDate.get(Calendar.YEAR) * 12
				+ endingDate.get(Calendar.MONTH);
		return m2 - m1 + 1;
	}

	/**
	 * Checks if is valid date range.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param equalOK
	 *            the equal OK
	 * @return true, if is valid date range
	 */
	public static boolean isValidDateRange(final Date startDate,
			final Date endDate, final boolean equalOK) {
		// false if either value is null
		if (startDate == null || endDate == null) {
			return false;
		}

		if (equalOK) {
			// true if they are equal
			if (startDate.equals(endDate)) {
				return true;
			}
		}

		// true if endDate after startDate
		if (endDate.after(startDate)) {
			return true;
		}

		return false;
	}

	/**
	 * Days between.
	 *
	 * @param d1
	 *            the d 1
	 * @param d2
	 *            the d 2
	 * @return the int
	 */
	public static int daysBetween(final Date d1, final Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public static Date getCurrentDate() {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Gets the current date time.
	 *
	 * @return the current date time
	 */
	public static Date getCurrentDateTime() {
		final Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}

	/**
	 * Gets the server current UTC date time.
	 *
	 * @return the server current UTC date time
	 */
	public static String getServerCurrentUTCDateTime() {
		final Calendar calendar = Calendar.getInstance();
		final SimpleDateFormat sdf = new SimpleDateFormat(
				SystemConfigs.DATE_TIME_FORMAT);
		String UTCDate = "";
		if (!Objects.isEmpty(calendar.getTime())) {
			sdf.setTimeZone(
					TimeZone.getTimeZone(SystemConfigs.GMT_TIME_ZONE_TEXT));
			UTCDate = sdf.format(calendar.getTime());
		}
		return UTCDate;
	}

	/**
	 * Gets the server current date time.
	 *
	 * @return the server current date time
	 */
	public static String getServerCurrentDateTime() {
		final Calendar calendar = Calendar.getInstance();
		final SimpleDateFormat sdf = new SimpleDateFormat(
				SystemConfigs.DATE_TIME_FORMAT);
		String UTCDate = "";
		if (!Objects.isEmpty(calendar.getTime())) {
			UTCDate = sdf.format(calendar.getTime());
		}
		return UTCDate;
	}

	/**
	 * Find working days.
	 *
	 * @param startDateCal
	 *            the start date cal
	 * @param endDateCal
	 *            the end date cal
	 * @return the integer
	 */
	public static Integer findWorkingDays(final Calendar startDateCal,
			final Calendar endDateCal) {
		Integer workingDays = 0;
		while (!startDateCal.after(endDateCal)) {
			final int day = startDateCal.get(Calendar.DAY_OF_WEEK);
			if ((day != Calendar.SATURDAY) && (day != Calendar.SUNDAY)) {
				workingDays++;
			}
			startDateCal.add(Calendar.DATE, 1);
		}
		return workingDays;
	}

	/**
	 * Gets the month year pair array.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param years
	 *            the years
	 * @return the month year pair array
	 */
	public static List<List<Integer>> getMonthYearPairArray(
			final Date startDate, final Date endDate,
			final List<Integer> years) {
		final List<List<Integer>> monthYearArr = new ArrayList<>();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		Integer startDateMonth = cal.get(Calendar.MONTH) + 1;
		cal.setTime(endDate);
		Integer endDateMonth = cal.get(Calendar.MONTH) + 1;
		List<Integer> monthYearPair = null;
		if (years != null && years.size() > 0) {
			if (years.size() == 1) {
				while (startDateMonth <= endDateMonth) {
					monthYearPair = new ArrayList<>();
					monthYearPair.add(0, startDateMonth);
					monthYearPair.add(1, years.get(0));
					monthYearArr.add(monthYearPair);
					startDateMonth++;
				}
			} else if (years.size() == 2) {
				while (startDateMonth <= 12) {
					monthYearPair = new ArrayList<>();
					monthYearPair.add(0, startDateMonth);
					monthYearPair.add(1, years.get(0));
					monthYearArr.add(monthYearPair);
					startDateMonth++;
				}
				while (1 <= endDateMonth) {
					monthYearPair = new ArrayList<>();
					monthYearPair.add(0, endDateMonth);
					monthYearPair.add(1, years.get(1));
					monthYearArr.add(monthYearPair);
					endDateMonth--;
				}
			} else {
				int count = 1;
				for (final Integer year : years) {
					if (count == 1) {
						while (startDateMonth <= 12) {
							monthYearPair = new ArrayList<>();
							monthYearPair.add(0, startDateMonth);
							monthYearPair.add(1, year);
							monthYearArr.add(monthYearPair);
							startDateMonth++;
						}
					} else if (count == years.size()) {
						while (1 <= endDateMonth) {
							monthYearPair = new ArrayList<>();
							monthYearPair.add(0, endDateMonth);
							monthYearPair.add(1, year);
							monthYearArr.add(monthYearPair);
							endDateMonth--;
						}
					} else {
						int iterator = 1;
						while (iterator <= 12) {
							monthYearPair = new ArrayList<>();
							monthYearPair.add(0, iterator);
							monthYearPair.add(1, year);
							monthYearArr.add(monthYearPair);
							iterator++;
						}
					}
					count++;
				}
			}
		}
		return monthYearArr;
	}

	/**
	 * Gets the month list.
	 *
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @return the month list
	 */
	public static List<Integer> getMonthList(final Integer month,
			final Integer year) {
		final List<Integer> months = new ArrayList<>();
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat(
					SystemConfigs.DATE_FORMAT);
			final Date startDate = sdf.parse(year + "-" + month + "-1");
			Date endDate = DateUtils.addMonths(startDate, 1);
			endDate = DateUtils.addDays(endDate, -1);

			final Calendar calendarStart = Calendar.getInstance();
			if (!Objects.isEmpty(startDate)) {
				calendarStart.setTime(startDate);
			}
			calendarStart.set(Calendar.YEAR, year);
			calendarStart.set(Calendar.MONTH, 0);
			calendarStart.set(Calendar.DAY_OF_MONTH, 1);

			final Calendar calendarEnd = Calendar.getInstance();
			calendarEnd.setTime(endDate);

			while (calendarStart.getTimeInMillis() < calendarEnd
					.getTimeInMillis()) {
				months.add(calendarStart.get(Calendar.MONTH) + 1);
				calendarStart.add(Calendar.MONTH, 1);
			}
		} catch (final Exception ex) {
			ObjectUtils.logException(logger, ex, null);
		}
		return months;
	}

	/**
	 * Gets the same previous date range.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the same previous date range
	 */
	public static List<Date> getSamePreviousDateRange(final Date startDate,
			final Date endDate) {
		final List<Date> startEndDates = new ArrayList<>();
		final int days = PulseDateUtils.daysBetween(startDate, endDate);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.DATE, -1);
		final Date previousEndDate = new Date(cal.getTimeInMillis());
		cal.add(Calendar.DATE, days - (days * 2));
		final Date previousStartDate = new Date(cal.getTimeInMillis());
		startEndDates.add(previousStartDate);
		startEndDates.add(previousEndDate);
		return startEndDates;
	}

	/**
	 * Parses the year range from dates.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the list
	 */
	public static List<Integer> parseYearRangeFromDates(final Date startDate,
			final Date endDate) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		final Integer startDateYear = cal.get(Calendar.YEAR);

		cal.setTime(endDate);
		final Integer endDateYear = cal.get(Calendar.YEAR);

		final List<Integer> years = new ArrayList<>();

		Integer startDateYear1 = startDateYear + 1;
		if (endDateYear.equals(startDateYear)) {
			years.add(endDateYear);
		} else if (endDateYear.equals(startDateYear1)) {
			years.add(startDateYear);
			years.add(endDateYear);
		} else {
			startDateYear1 = startDateYear;
			while (startDateYear1 < endDateYear) {
				years.add(startDateYear1);
				startDateYear1++;
			}
			years.add(endDateYear);
		}
		return years;
	}

	/**
	 * Parses the months from dates.
	 *
	 * @param dates
	 *            the dates
	 * @return the list
	 */
	public List<Integer> parseMonthsFromDates(final Date... dates) {
		final List<Integer> months = new ArrayList<>();
		final Calendar cal = Calendar.getInstance();
		for (final Date date : dates) {
			cal.setTime(date);
			final Integer month = cal.get(Calendar.MONTH);
			if (!months.contains(month)) {
				months.add(month);
			}
		}
		return months;
	}

	/**
	 * Gets the same previous month date range.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the same previous month date range
	 */
	public static List<Date> getSamePreviousMonthDateRange(final Date startDate,
			final Date endDate) {
		final List<Date> startEndDates = new ArrayList<>();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.MONTH, -1);
		final Date previousStartDate = new Date(cal.getTimeInMillis());
		cal.setTime(endDate);
		cal.add(Calendar.MONTH, -1);
		final Date previousEndDate = new Date(cal.getTimeInMillis());
		startEndDates.add(previousStartDate);
		startEndDates.add(previousEndDate);
		return startEndDates;
	}

	/**
	 * Gets the previous month date range.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the previous month date range
	 */
	public static List<Date> getPreviousMonthDateRange(final Date startDate,
			final Date endDate) {
		final List<Date> startEndDates = new ArrayList<>();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		final Calendar cal1 = Calendar.getInstance();
		cal1.setTime(endDate);
		cal.add(Calendar.MONTH, -1);
		final Date previousStartDate = new Date(cal.getTimeInMillis());

		cal1.add(Calendar.MONTH, -1);
		final Date previousEndDate = new Date(cal1.getTimeInMillis());
		startEndDates.add(previousStartDate);
		startEndDates.add(previousEndDate);
		return startEndDates;
	}

	/**
	 * Gets the current month date range.
	 *
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @return the current month date range
	 */
	public static List<Date> getCurrentMonthDateRange(final Integer month,
			final Integer year) {
		final List<Date> startEndDate = new ArrayList<>();
		final Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		final Date startDate = calendar.getTime();
		startEndDate.add(startDate);
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		final Date endDate = calendar.getTime();
		startEndDate.add(endDate);
		return startEndDate;
	}

	/**
	 * Gets the year date range.
	 *
	 * @param year
	 *            the year
	 * @return the year date range
	 */
	public static List<Date> getYearDateRange(final Integer year) {
		final List<Date> startEndDate = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		final Integer currentYear = calendar.get(Calendar.YEAR);
		calendar.clear();
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		final Date startDate = calendar.getTime();
		startEndDate.add(startDate);
		if (currentYear.equals(year)) {
			calendar.clear();
			calendar = Calendar.getInstance();
		} else {
			calendar.set(Calendar.MONTH, 11);
			calendar.set(Calendar.DAY_OF_MONTH,
					calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			calendar.set(Calendar.HOUR, 23);
			calendar.set(Calendar.MINUTE, 59);
		}
		final Date endDate = calendar.getTime();
		startEndDate.add(endDate);
		return startEndDate;
	}

	/**
	 * Gets the dates from string.
	 *
	 * @param startDateStr
	 *            the start date str
	 * @param endDateStr
	 *            the end date str
	 * @param dateFormat
	 *            the date format
	 * @return the dates from string
	 */
	public static List<Date> getDatesFromString(final String startDateStr,
			final String endDateStr, final String dateFormat) {
		final List<Date> startEndDate = new ArrayList<>();
		Date startDate = null;
		Date endDate = null;
		if (!Objects.isEmpty(startDateStr) && !Objects.isEmpty(endDateStr)) {
			startDate = PulseDateUtils.parseDate(startDateStr, dateFormat);
			if (startDate != null) {
				endDate = PulseDateUtils.parseDate(endDateStr, dateFormat);
				if (endDate == null) {
					startDate = null;
				}
			}
		}
		startEndDate.add(startDate);
		startEndDate.add(endDate);
		return startEndDate;
	}

	/**
	 * Gets the UTC date into server time.
	 *
	 * @param dateString
	 *            the date string
	 * @return the UTC date into server time
	 */
	public static Date getUTCDateIntoServerTime(final String dateString) {
		// String dateString = "2016-06-09T18:09:07+00:00";
		final String pattern = "yyyy-MM-dd'T'HH:mm:ss";
		final SimpleDateFormat sdf1 = new SimpleDateFormat(pattern);
		sdf1.setTimeZone(
				TimeZone.getTimeZone(SystemConfigs.GMT_TIME_ZONE_TEXT));
		Date date = null;
		try {
			date = sdf1.parse(dateString);
		} catch (final ParseException e) {
			ObjectUtils.logException(logger, e,
					"Failed to get UTC  date into server time");
		}
		return date;
	}

	/**
	 * Gets the start time of date.
	 *
	 * @param date
	 *            the date
	 * @return the start time of date
	 */
	public static Date getStartTimeOfDate(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	/**
	 * Gets the end time of date.
	 *
	 * @param date
	 *            the date
	 * @return the end time of date
	 */
	public static Date getEndTimeOfDate(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	/**
	 * Gets the compare date list.
	 *
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @return the compare date list
	 */
	public static List<Date> getCompareDateList(final Date startDate,
			final Date endDate) {
		final long days = (endDate.getTime() - startDate.getTime()) / 1000 / 60
				/ 60 / 24;
		final Date compareEndDate = DateUtils.addDays(startDate, -1);
		final Date compareStartDate = DateUtils.addDays(compareEndDate,
				-(int) days);
		final List<Date> dateList = new ArrayList<>();
		dateList.add(compareStartDate);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(compareEndDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		dateList.add(cal.getTime());
		return dateList;
	}

	/**
	 * Gets the dates for goal.
	 *
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @return the dates for goal
	 */
	public static HashMap<String, Date> getDatesForGoal(final Integer month,
			final Integer year) {
		// new
		final HashMap<String, Date> dateMap = new HashMap<>();
		// create date from month
		final Calendar cal = Calendar.getInstance();
		final Date currentDate = cal.getTime();
		dateMap.put(SystemConfigs.GOAL_CURRENT_DATE, currentDate);
		cal.set(Calendar.MONTH, Months.fromInteger(month).value());
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 00);
		cal.set(Calendar.MINUTE, 00);
		cal.set(Calendar.SECOND, 00);
		final Date startDate = cal.getTime();
		dateMap.put(SystemConfigs.GOAL_START_DATE, startDate);
		// get previous month startDate
		cal.add(Calendar.MONTH, -1);
		final Date prevMonthStartDate = cal.getTime();
		dateMap.put(SystemConfigs.GOAL_PREVIOUS_MONTH_START_DATE,
				prevMonthStartDate);
		cal.add(Calendar.MONTH, 1); // back to same date
		// get previous month endDate
		cal.add(Calendar.DATE, -1);
		final Date prevMonthEndDate = cal.getTime();
		dateMap.put(SystemConfigs.GOAL_PREVIOUS_MONTH_END_DATE,
				prevMonthEndDate);
		cal.add(Calendar.DATE, 1); // back to same date

		// to find end of month
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DATE, -1);
		final Date endDate = cal.getTime();
		dateMap.put(SystemConfigs.GOAL_END_DATE, endDate);

		// Year details
		final Calendar calendarStart = Calendar.getInstance();
		calendarStart.set(Calendar.YEAR, year);
		calendarStart.set(Calendar.MONTH, 0);
		calendarStart.set(Calendar.DAY_OF_MONTH, 1);
		// returning the first date
		final Date yearStartDate = calendarStart.getTime();
		dateMap.put(SystemConfigs.GOAL_YEAR_START_DATE, yearStartDate);
		final Calendar calendarEnd = Calendar.getInstance();
		if (!Objects.isEmpty(currentDate)) {
			calendarEnd.setTime(currentDate);
		}
		calendarEnd.set(Calendar.YEAR, year);
		calendarEnd.set(Calendar.MONTH, 11);
		calendarEnd.set(Calendar.DAY_OF_MONTH, 31);
		// returning the last date
		final Date yearEndDate = calendarEnd.getTime();
		dateMap.put(SystemConfigs.GOAL_YEAR_END_DATE, yearEndDate);
		return dateMap;
		// end
	}

	/**
	 * Gets the server timezone date time.
	 *
	 * @param timeZone
	 *            the time zone
	 * @param sdf
	 *            the sdf
	 * @return the server timezone date time
	 */
	public static SimpleDateFormat getServerTimezoneDateTime(
			final String timeZone, final SimpleDateFormat sdf) {
		final String timeZoneTemp = !Objects.isEmpty(timeZone) ? timeZone
				: SystemConfigs.GMT_TIME_ZONE_TEXT;
		sdf.setTimeZone(TimeZone.getTimeZone(timeZoneTemp));
		return sdf;
	}

	/**
	 * Gets the solr timezone.
	 *
	 * @return the solr timezone
	 */
	public static String getSolrTimezone() {
		String hms = "";
		try {
			final Calendar cal = Calendar.getInstance();
			final TimeZone timeZone = cal.getTimeZone();
			final long millis = timeZone.getRawOffset();
			hms = String.format("%dHOUR-%dMINUTES",
					TimeUnit.MILLISECONDS.toHours(millis),
					TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS
							.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
					TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(
									TimeUnit.MILLISECONDS.toMinutes(millis)));

		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
		return hms;
	}

	/**
	 * Gets the last months dates.
	 *
	 * @param howManyMonths
	 *            the how many months
	 * @return the last months dates
	 */
	public static List<Date> getLastMonthsDates(final Integer howManyMonths) {
		final List<Date> dateList = new ArrayList<>();
		for (int ik = howManyMonths; ik > (-1); ik--) {
			final Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.add(Calendar.MONTH, ik * (-1));
			dateList.add(calendar.getTime());
		}
		return dateList;
	}

	/**
	 * Gets the date from day of month.
	 *
	 * @param dayOfMonth
	 *            the day of month
	 * @return the date from day of month
	 */
	public static Date getDateFromDayOfMonth(final Integer dayOfMonth) {
		final Calendar calendar = Calendar.getInstance();
		final Integer month = calendar.get(Calendar.MONTH);
		final Integer year = calendar.get(Calendar.YEAR);
		final String strDate = dayOfMonth.toString() + "-" + (month + 1) + "-"
				+ year.toString();
		return parseDate(strDate, SystemConfigs.DISPLAY_DATE_FORMAT_4);
	}

	/**
	 * Difference in hours.
	 *
	 * @param beginningDate
	 *            the beginning date
	 * @param endingDate
	 *            the ending date
	 * @return the long
	 */
	public static Long differenceInHours(final Date beginningDate,
			final Date endingDate) {
		if (beginningDate == null || endingDate == null) {
			return 0L;
		}
		return (endingDate.getTime() - beginningDate.getTime())
				/ (60 * 60 * 1000);
	}

}
