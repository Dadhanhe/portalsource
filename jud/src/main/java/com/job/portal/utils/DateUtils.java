/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Calendar;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class DateUtils.
 *
 * @author vadherak
 */
public class DateUtils {

	/**
	 * Creating a private constructor to hide public.
	 */
	private DateUtils() {
		// Creating a private constructor to hide public
	}

	/**
	 * Gets the date ago.
	 *
	 * @param minutes
	 *            the minutes
	 * @return the date ago
	 */
	public static Date getDateAgo(final Integer minutes) {
		final Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, -1 * minutes);
		return now.getTime();
	}

}
