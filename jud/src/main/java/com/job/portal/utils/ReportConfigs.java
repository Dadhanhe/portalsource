/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ReportConfigs.
 */
public class ReportConfigs {

	/** The Constant REPORT_UNIQUE_CODE_INCREMENTAL_REVENUE. */
	public static final String REPORT_UNIQUE_CODE_INCREMENTAL_REVENUE = "IR";

	/** The Constant REPORT_UNIQUE_CODE_IDR. */
	public static final String REPORT_UNIQUE_CODE_IDR = "IDR";

	/** The Constant REPORT_UNIQUE_CODE_REVENUE. */
	public static final String REPORT_UNIQUE_CODE_REVENUE = "IHR";

	/** The Constant REPORT_UNIQUE_CODE_INCREMENTAL_REVENUE_2. */
	public static final String REPORT_UNIQUE_CODE_INCREMENTAL_REVENUE_2 = "IR2";

	/** The Constant REPORT_UNIQUE_CODE_IDR_INDEX. */
	public static final String REPORT_UNIQUE_CODE_IDR_INDEX = "IDR-INDEX";

	/** The Constant REPORT_UNIQUE_CODE_IDR_REV_PAR_INDEX. */
	public static final String REPORT_UNIQUE_CODE_IDR_REV_PAR_INDEX = "IDR-REV-PAR-INDEX";

	/** The Constant REPORT_UNIQUE_CODE_ADR. */
	public static final String REPORT_UNIQUE_CODE_ADR = "ADR";

	/** The Constant REPORT_UNIQUE_CODE_REV_PAR. */
	public static final String REPORT_UNIQUE_CODE_REV_PAR = "REVPAR";

	/** The Constant REPORT_UNIQUE_CODE_ROOMS_OCCUPIED. */
	public static final String REPORT_UNIQUE_CODE_ROOMS_OCCUPIED = "OCCR";

	/** The Constant REPORT_UNIQUE_CODE_ROOM_UPSELL_RATE. */
	public static final String REPORT_UNIQUE_CODE_ROOM_UPSELL_RATE = "ARUR";

	/** The Constant REPORT_UNIQUE_CODE_LENGTH_STAY. */
	public static final String REPORT_UNIQUE_CODE_LENGTH_STAY = "LOS";

	/** The Constant REPORT_UNIQUE_CODE_ROOM_UPSELL_CONVERSION. */
	public static final String REPORT_UNIQUE_CODE_ROOM_UPSELL_CONVERSION = "RUC";

	/** The Constant REPORT_UNIQUE_CODE_OCCUPANCY. */
	public static final String REPORT_UNIQUE_CODE_OCCUPANCY = "OCC";

	/** The Constant REPORT_UNIQUE_CODE_ARR. */
	public static final String REPORT_UNIQUE_CODE_ARR = "ARR";

	/** The Constant REPORT_UNIQUE_CODE_UPS. */
	public static final String REPORT_UNIQUE_CODE_UPS = "UPS";

	/** The Constant REPORT_UNIQUE_CODE_ROOM_UPSELL_REVENUE. */
	public static final String REPORT_UNIQUE_CODE_ROOM_UPSELL_REVENUE = "UMR";

	/** The Constant REPORT_UNIQUE_CODE_ROOM_UPSELL_NIGHTS. */
	public static final String REPORT_UNIQUE_CODE_ROOM_UPSELL_NIGHTS = "UPSN";

	/** The Constant REPORT_UNIQUE_CODE_AGENT_ROOM_UPSELL_CONVERSION. */
	public static final String REPORT_UNIQUE_CODE_AGENT_ROOM_UPSELL_CONVERSION = "AGENT_RUC";

	/** The Constant REPORT_UNIQUE_CODE_ROOM_UPSELLS. */
	public static final String REPORT_UNIQUE_CODE_ROOM_UPSELLS = "UPS";

	/** The Constant REPORT_UNIQUE_CODE_AGENT_IDR. */
	public static final String REPORT_UNIQUE_CODE_AGENT_IDR = "AGENT_IDR";

	/** The Constant REPORT_UNIQUE_CODE_AGENT_OTHER_REVENUE. */
	public static final String REPORT_UNIQUE_CODE_AGENT_OTHER_REVENUE = "AGENT_OR";

}
