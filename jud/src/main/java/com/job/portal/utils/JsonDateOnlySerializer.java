/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonDateOnlySerializer.
 */
@Component
public class JsonDateOnlySerializer extends JsonSerializer<Date> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(janew
	 * SimpleDateFormat(Constants.DATEONLY)re.JsonGenerator,
	 * com.fasterxml.jackson.databind.SerializerProvider)
	 */
	@Override
	public void serialize(final Date date, final JsonGenerator gen,
			final SerializerProvider provider)
			throws IOException, JsonProcessingException {

		final String formattedDate = new SimpleDateFormat(Constants.DATE_FORMAT)
				.format(date);

		gen.writeString(formattedDate);
	}

}
