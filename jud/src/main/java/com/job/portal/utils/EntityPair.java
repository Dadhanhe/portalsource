/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityPair.
 *
 * @param <K>
 *            the key type
 * @param <V>
 *            the value type
 */
public final class EntityPair<K, V> implements Map.Entry<K, V> {

	/** The key. */
	private final K key;

	/** The value. */
	private V value;

	/**
	 * Instantiates a new entity pair.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public EntityPair(final K key, final V value) {
		this.key = key;
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Map.Entry#getKey()
	 */
	@Override
	public K getKey() {
		return this.key;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Map.Entry#getValue()
	 */
	@Override
	public V getValue() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Map.Entry#setValue(java.lang.Object)
	 */
	@Override
	public V setValue(final V value) {
		final V old = this.value;
		this.value = value;
		return old;
	}
}