/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class ValidationUtils.
 */
public class ValidationUtils {

	/** The Constant EmailRegex. */
	public static final String EmailRegex = "^[_A-Za-z0-9-\\'\\+]+(\\.[_A-Za-z0-9-\\'\\+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/** The Constant pulseIdRegex. */
	public static final String pulseIdRegex = "[\\w\\`\\'\\.]{1,20}";

	/** The Constant userNameRegEx. */
	public static final String userNameRegEx = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{1,50}";

	/**
	 * Validate.
	 *
	 * @param regEx
	 *            the reg ex
	 * @param hex
	 *            the hex
	 * @return true, if successful
	 */
	public static boolean validate(final String regEx, final String hex) {
		final Pattern pattern = Pattern.compile(regEx);
		final Matcher matcher = pattern.matcher(hex);
		return matcher.matches();
	}
}
