/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * The Class XmlUtil.
 */
/**
 * @author makwanvi
 *
 */
public final class XmlUtil {

	/** The Constant YES. */
	private static final String YES = "yes";

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(XmlUtil.class);

	/**
	 * Instantiates a new xml util.
	 */
	private XmlUtil() {
	}

	/**
	 * The Class NodeListWrapper.
	 */
	static final class NodeListWrapper extends AbstractList<Node>
			implements RandomAccess {

		/** The list. */
		private final NodeList list;

		/**
		 * Instantiates a new node list wrapper.
		 *
		 * @param l
		 *            the l
		 */
		NodeListWrapper(final NodeList l) {
			this.list = l;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.util.AbstractList#get(int)
		 */
		@Override
		public Node get(final int index) {
			return this.list.item(index);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.util.AbstractCollection#size()
		 */
		@Override
		public int size() {
			return this.list.getLength();
		}
	}

	/**
	 * As list.
	 *
	 * @param n
	 *            the n
	 * @return the list
	 */
	public static List<Node> asList(final NodeList n) {
		return n.getLength() == 0 ? Collections.<Node> emptyList()
				: new NodeListWrapper(n);
	}

	/**
	 * Gets the tag value.
	 *
	 * @param eElement
	 *            the e element
	 * @param tag
	 *            the tag
	 * @return the tag value
	 */
	public static String getTagValue(final Element eElement, final String tag) {
		final Node nValue = eElement.getElementsByTagName(tag).item(0)
				.getChildNodes().item(0);
		return (nValue == null) ? null : nValue.getNodeValue();
	}

	/**
	 * Gets the text content.
	 *
	 * @param node
	 *            the node
	 * @return the text content
	 */
	public static String getTextContent(final Node node) {
		return node != null && node.getChildNodes() != null
				&& node.getChildNodes().item(0) != null
						? node.getChildNodes().item(0).getNodeValue()
						: null;
	}

	/**
	 * Creates the XML document.
	 *
	 * @param xmlRecords
	 *            the xml records
	 * @return the document
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Document createXMLDocument(final String xmlRecords)
			throws ParserConfigurationException, SAXException, IOException {
		return DocumentBuilderFactory.newInstance().newDocumentBuilder()
				.parse(new InputSource(new StringReader(xmlRecords)));
	}

	/**
	 * Display document.
	 *
	 * @param document
	 *            the document
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	public static void displayDocument(final Document document)
			throws TransformerConfigurationException {
		try {
			final Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, YES);
			transformer.transform(new DOMSource(document),
					new StreamResult(System.out));
		} catch (final TransformerException ex) {
			ObjectUtils.logException(logger, ex, "Exception");
		}
	}

	/**
	 * Gets the document element.
	 *
	 * @param element
	 *            the element
	 * @return the document element
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	public static String getDocumentElement(final Element element)
			throws TransformerConfigurationException {
		try {
			final StreamResult result = new StreamResult(new StringWriter());
			TransformerFactory.newInstance().newTransformer()
					.transform(new DOMSource(element), result);
			return result.getWriter().toString();
		} catch (final TransformerException ex) {
			ObjectUtils.logException(logger, ex, "Exception");
		}
		return null;
	}

	/**
	 * Gets the document element.
	 *
	 * @param node
	 *            the node
	 * @return the document element
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	public static String getDocumentElement(final Node node)
			throws TransformerConfigurationException {
		try {
			final StreamResult result = new StreamResult(new StringWriter());
			TransformerFactory.newInstance().newTransformer()
					.transform(new DOMSource(node), result);
			return result.getWriter().toString();
		} catch (final TransformerException ex) {
			ObjectUtils.logException(logger, ex, "Exception");
		}
		return null;
	}

}