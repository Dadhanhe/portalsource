/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.job.portal.pojo.enums.EventAction;

// TODO: Auto-generated Javadoc
/**
 * The Interface EventLog.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventLog {

	/**
	 * Action.
	 *
	 * @return the event action
	 */
	EventAction action();
}
