
/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.aspect;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.job.portal.service.UserService;

// TODO: Auto-generated Javadoc
/**
 * The Class GlobalLogger.
 */
@Aspect
@Component
public class GlobalLogger {

	/** The request. */
	@Autowired
	HttpServletRequest request;

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(GlobalLogger.class);

	/** The user service. */
	@Autowired
	UserService userService;

	/**
	 * Log around.
	 *
	 * @param joinPoint
	 *            the join point
	 * @return the object
	 * @throws Throwable
	 *             the throwable
	 */
	@Around("@annotation(com.job.portal.aspect.Loggable)")
	public Object logAround(final ProceedingJoinPoint joinPoint)
			throws Throwable {
		final String fullName = joinPoint.getSignature().toLongString();
		final String arguments = Arrays.toString(joinPoint.getArgs());
		final long startTime = System.currentTimeMillis();
		Object retValue;
		retValue = joinPoint.proceed();
		final long endTime = System.currentTimeMillis();
		final long totalTimeTaken = endTime - startTime;
		final String url = this.request != null ? makeUrl(this.request) : "";
		logger.debug("Requested URL:`" + url + "` for method `" + fullName
				+ "` with arguments [" + arguments + "] took " + totalTimeTaken
				+ " ms by "
				+ (SecurityContextHolder.getContext()
						.getAuthentication() != null
								? SecurityContextHolder.getContext()
										.getAuthentication().getPrincipal()
								: ""));
		return retValue;
	}

	/**
	 * Make url.
	 *
	 * @param request
	 *            the request
	 * @return the string
	 */
	public static String makeUrl(final HttpServletRequest request) {
		return request != null ? request.getQueryString() != null
				? request.getRequestURL().toString() + "?"
						+ request.getQueryString()
				: request.getRequestURL().toString() : null;
	}
}
