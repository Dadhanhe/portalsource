/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.IndustryDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.Industry;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class IndustryService.
 */
@Service
public class IndustryService implements JudService<Industry> {

	/** The industry DAO. */
	@Autowired
	private IndustryDAO industryDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Industry create(final Industry input)
			throws DuplicateRecordException, RecordNotFoundException,
			SystemConfigurationException {
		return this.industryDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Industry update(final Industry input)
			throws RecordNotFoundException, DuplicateRecordException {
		final Industry ini = this.industryDAO.findBySpecific(input.getId());

		ini.setName(input.getName());
		ini.setActiveStatus(input.getActiveStatus());
		ini.setUpdatedOn(new Date());
		return this.industryDAO.update(ini);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#delete(java.lang.Integer)
	 */
	@Override
	public Industry delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#getDetails(java.lang.Integer)
	 */
	@Override
	public Industry getDetails(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public Industry find(final Industry input) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status,
	 * java.lang.String[], com.job.portal.pojo.enums.Sort[])
	 */
	@Override
	public List<Industry> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	/**
	 * Gets the all industry.
	 *
	 * @return the all industry
	 */
	@Transactional(readOnly = true)
	public List<Industry> getAllIndustry() {
		final List<Industry> industryies = this.industryDAO.getAllIndustry();
		if (!Objects.isEmpty(industryies)) {
			for (final Industry ini : industryies) {
				Hibernate.initialize(ini.getCreatedBy());
				Hibernate.initialize(ini.getUpdatedBy());
			}
		}
		return this.industryDAO.getAllIndustry();

	}
}
