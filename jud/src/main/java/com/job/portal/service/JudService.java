/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.List;

import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Interface PulseService.
 *
 * @param <E>
 *            the element type
 */
public interface JudService<E extends AbstractValueObject> {

	/**
	 * Creates the.
	 *
	 * @param input
	 *            the input
	 * @return the e
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	E create(E input) throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException;

	/**
	 * Update.
	 *
	 * @param input
	 *            the input
	 * @return the e
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 */
	E update(E input) throws RecordNotFoundException, InvalidInputException,
			DuplicateRecordException;

	/**
	 * Delete.
	 *
	 * @param id
	 *            the id
	 * @return the e
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	E delete(Integer id) throws RecordNotFoundException;

	/**
	 * Gets the details.
	 *
	 * @param id
	 *            the id
	 * @return the details
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	E getDetails(Integer id) throws RecordNotFoundException;

	/**
	 * Find.
	 *
	 * @param input
	 *            the input
	 * @return the e
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	E find(E input) throws RecordNotFoundException;

	/**
	 * Find all.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @return the list
	 */
	List<E> findAll(final Integer first, final Integer total,
			final String search, final Status status, String[] orderBy,
			Sort[] sort);

	/**
	 * Find total.
	 *
	 * @param industryId
	 *            the industry id
	 * @param tenantId
	 *            the tenant id
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @return the long
	 */
	Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status);

}
