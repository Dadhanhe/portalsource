/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.DisabilityDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.Disability;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

@Service
public class DisabilityManagementService implements JudService<Disability> {

	@Autowired
	private DisabilityDAO disabilityDAO;

	@Override
	@Transactional
	public Disability create(final Disability input)
			throws DuplicateRecordException, RecordNotFoundException,
			SystemConfigurationException {
		return this.disabilityDAO.save(input);
	}

	@Override
	@Transactional
	public Disability update(final Disability input)
			throws RecordNotFoundException, DuplicateRecordException {
		final Disability edu = this.disabilityDAO.findBySpecific(input.getId());
		edu.setName(input.getName());
		edu.setActiveStatus(input.getActiveStatus());
		return this.disabilityDAO.update(edu);
	}

	@Override
	public Disability delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	@Override
	public Disability getDetails(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	@Override
	public Disability find(final Disability input)
			throws RecordNotFoundException {
		return null;
	}

	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	@Transactional(readOnly = true)
	public List<Disability> getAlldisabilityList() {
		final List<Disability> disability = this.disabilityDAO
				.getAllDisability();
		for (final Disability dis : disability) {
			Hibernate.initialize(dis.getCreatedBy());
			Hibernate.initialize(dis.getUpdatedBy());
		}
		return disability;
	}

	@Override
	public List<Disability> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

}
