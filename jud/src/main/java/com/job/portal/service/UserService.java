/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.lang.reflect.Field;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.job.portal.aspect.EventLog;
import com.job.portal.dao.AppPreferenceDAO;
import com.job.portal.dao.PasswordResetDAO;
import com.job.portal.dao.UserDAO;
import com.job.portal.exception.DuplicateEmailException;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.InvalidPasswordException;
import com.job.portal.exception.InvalidSecureToken;
import com.job.portal.exception.InvalidUserException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.AppPreference;
import com.job.portal.pojo.common.Converter;
import com.job.portal.pojo.common.UserAuthentication;
import com.job.portal.pojo.customer.PasswordReset;
import com.job.portal.pojo.customer.PasswordValidation;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.UserPasswordAttemptDetail;
import com.job.portal.pojo.customer.dto.UserDTO;
import com.job.portal.pojo.enums.AuthyAction;
import com.job.portal.pojo.enums.EventAction;
import com.job.portal.pojo.enums.Gender;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;
import com.job.portal.utils.SystemConfigs;

import net.minidev.json.JSONObject;

// TODO: Auto-generated Javadoc
/**
 * The Class UserService.
 */
@Service
public class UserService implements JudService<User> {

	/** The user DAO. */
	@Autowired
	private UserDAO userDAO;

	/** The password reset DAO. */
	@Autowired
	private PasswordResetDAO passwordResetDAO;

	/** The authy service. */
	@Autowired
	private AuthyService authyService;

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);

	/** The app preference DAO. */
	@Autowired
	private AppPreferenceDAO appPreferenceDAO;

	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The converter. */
	@Autowired
	Converter converter;

	/**
	 * Authenticate user.
	 *
	 * @param strUserId
	 *            the str user id
	 * @param password
	 *            the password
	 * @param platform
	 *            the platform
	 * @return the user
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 */
	@Transactional
	@EventLog(action = EventAction.Login)
	public User authenticateUser(final String strUserId, final String password,
			final Integer platform)
			throws NoSuchAlgorithmException, InvalidPasswordException,
			RecordNotFoundException, InvalidUserException {
		User user = null;
		if (strUserId != null) {
			if (StringUtils.isNumeric(strUserId)) {
				final Integer userId = Integer.parseInt(strUserId);
				user = this.userDAO.findById(userId);
			} else {
				user = this.userDAO.findByProperty("email", strUserId);
			}
			if (user != null) {
				if (user.getActiveStatus().equals(Status.Deactive) && !(user
						.getActiveStatus().equals(Status.Active)
						|| user.getActiveStatus().equals(Status.Pending))) {
					throw new InvalidUserException("email",
							"Account is deactivated");
				} else if (user.getActiveStatus().equals(Status.Pending)) {
					throw new InvalidUserException("email",
							"Account is pending to approve");
				} else if (user.getActiveStatus().equals(Status.Blocked)) {
					throw new InvalidUserException("email",
							"Account is blocked");
				} else if (user.getActiveStatus().equals(Status.Deleted)) {
					throw new InvalidUserException("email",
							"Account is deleted");
				}

				/*
				 *
				 * GET USER'S ROLE. IF(USER'S ROLE ==NULL){ ATHROW NEW
				 * EXCEPTION(TAASDF); }
				 *
				 * ROLE = GET ROLE BY ID USERS.SETROLE(ROLE);
				 */

			} else {
				throw new InvalidUserException("email", "User has no account");
			}
		}
		return user;
	}

	/**
	 * Reset password.
	 *
	 * @param password
	 *            the password
	 * @param token
	 *            the token
	 * @return the string
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws GeneralSecurityException
	 *             the general security exception
	 */
	@Transactional
	public String resetPassword(final String password, final String token)
			throws RecordNotFoundException, GeneralSecurityException {
		String response = null;
		final PasswordReset reset = this.passwordResetDAO
				.findByProperty("token", token);
		if (reset == null) {
			throw new RecordNotFoundException();
		}
		if (reset.isValid()) {
			reset.setIsUsed(true);
			reset.setAccessedTime(new Date());
			final User user = this.userDAO.findById(reset.getUser().getId());
			final String cipheredPwd = generateMD5HexPassword(password,
					user.getSalt());
			user.setPassword(cipheredPwd);
			user.setUpdatedOn(new Date());
			this.userDAO.update(user);
			this.passwordResetDAO.update(reset);
			response = user.getEmail();
		}
		return response;
	}

	/**
	 * Gets the by token.
	 *
	 * @return the by token
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Transactional(readOnly = true)
	public User getByToken() throws RecordNotFoundException {
		final Integer id = Integer.parseInt(String.valueOf(SecurityContextHolder
				.getContext().getAuthentication().getPrincipal()));
		final User user = this.userDAO.findById(id);
		return user;
	}

	/**
	 * Gets the by token id.
	 *
	 * @return the by token id
	 * @throws RecordNotFoundException
	 *             the record not found exception To avoid call of UserDAO in
	 *             case where just userID information is needed
	 */
	@Transactional(readOnly = true)
	public Integer getByTokenId() throws RecordNotFoundException {
		return Integer.parseInt(String.valueOf(SecurityContextHolder
				.getContext().getAuthentication().getPrincipal()));
	}

	/**
	 * Gets the by token or system admin.
	 *
	 * @return the by token or system admin
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Transactional(readOnly = true)
	public User getByTokenOrSystemAdmin() throws RecordNotFoundException {
		final String token = !Objects
				.isEmpty(SecurityContextHolder.getContext().getAuthentication())
						? String.valueOf(SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
						: "1";
		final Integer id = Integer.parseInt(token);
		return this.userDAO.findById(id);
	}

	/**
	 * Resend SMS.
	 *
	 * @param userId
	 *            the user id
	 */
	@Transactional
	public void resendSMS(final Integer userId) {
		final User user = this.userDAO.findById(userId);
		if (Objects.isNotNull(user) && Objects.isNotNull(user.getAuthyId())) {
			this.authyService.activateAuthyAction(AuthyAction.REQUESTSMS, user,
					null);
		}
	}

	/**
	 * Verify secure 2 FA token.
	 *
	 * @param userId
	 *            the user id
	 * @param token
	 *            the token
	 * @return the boolean
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Transactional
	public Boolean verifySecure2FAToken(final Integer userId,
			final String token) throws RecordNotFoundException {
		Boolean resp = false;
		final User user = getDetails(userId);
		if (StringUtils.isEmpty(token)) {
			throw new InvalidSecureToken("Invalid secure token.");
		}

		final JSONObject response = this.authyService
				.activateAuthyAction(AuthyAction.VERIFYTOKEN, user, token);
		if (Objects.isNotNull(response)) {
			logger.debug("Authy Response :: " + response.toJSONString());
		} else {
			logger.debug("Authy Response :: " + response);
		}
		resp = new Boolean(response.get("success").toString());
		if (Objects.checkBoolean(resp)) {
			user.setIsAuthyAttempted(true);
			this.userDAO.save(user);
		} else {
			throw new InvalidSecureToken("Invalid secure token.");
		}
		return resp;
	}

	/**
	 * Generate random pulse id.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@Transactional
	public String generateRandomPulseId(final User user) {
		String pulseId = "";
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		if (firstName != null && lastName != null && firstName.length() > 0
				&& lastName.length() > 0) {
			final String randomNumber = ObjectUtils
					.leftPadding(ObjectUtils.randomNumberFromRange(0, 9999), 4);
			firstName = firstName.trim();
			lastName = lastName.trim();
			final String initialFirstName = firstName.substring(0, 1)
					.toUpperCase();
			final String initialLastName = lastName.substring(0, 1)
					.toUpperCase();
			pulseId = initialFirstName + initialLastName + randomNumber;
		}
		return pulseId;
	}

	/**
	 * Gets the user pulse ID infos.
	 *
	 * @param emailAddress
	 *            the email address
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param birthMonth
	 *            the birth month
	 * @param birthDay
	 *            the birth day
	 * @param notUserID
	 *            the not user ID
	 * @param status
	 *            the status
	 * @param queryPulseID
	 *            the query pulse ID
	 * @return the user pulse ID infos
	 */
	@Transactional
	public List<User> getUserPulseIDInfos(final String emailAddress,
			final String firstName, final String lastName,
			final Integer birthMonth, final Integer birthDay,
			final Integer notUserID, final Status status,
			final String queryPulseID) {
		final List<User> userInfos = this.userDAO.getUserPulseIDInfos(
				emailAddress, firstName, lastName, birthMonth, birthDay,
				notUserID, status, queryPulseID);
		return userInfos;
	}

	/**
	 * Gets the binding error string.
	 *
	 * @param br
	 *            the br
	 * @return the binding error string
	 */
	@Transactional
	public String getBindingErrorString(final BindingResult br) {
		final List<FieldError> fieldErrorList = br.getFieldErrors();
		String strError = "";
		for (final FieldError fieldError : fieldErrorList) {
			strError += "Field error in object " + fieldError.getObjectName()
					+ " \n  Field Name :: " + fieldError.getField()
					+ " \n  Default Message :: "
					+ fieldError.getDefaultMessage();
			strError += " -------------------------- \n ";
		}
		// strError = strError.replaceAll("\n", " ");
		return strError;
	}

	/**
	 * Gets the cell value.
	 *
	 * @param field
	 *            the field
	 * @param cell
	 *            the cell
	 * @param user
	 *            the user
	 * @return the cell value
	 * @throws Exception
	 *             the exception
	 */
	@Transactional
	public void getCellValue(final Field field, final Cell cell,
			final User user) throws Exception {
		if (field.getType().equals(String.class)) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
			field.set(user, cell.toString());
		} else if (field.getType().equals(boolean.class)
				|| field.getType().equals(Boolean.class)) {
			if (cell.toString().equalsIgnoreCase("YES")) {
				cell.setCellType(Cell.CELL_TYPE_BOOLEAN);
				field.set(user, true);
			} else if (cell.toString().equalsIgnoreCase("NO")) {
				cell.setCellType(Cell.CELL_TYPE_BOOLEAN);
				field.set(user, false);
			} else {
				cell.setCellType(Cell.CELL_TYPE_BOOLEAN);
				field.set(user, cell.getBooleanCellValue());
			}
		} else if (field.getType().equals(Gender.class)) {
			cell.setCellType(Cell.CELL_TYPE_STRING);
			if (cell.toString().equalsIgnoreCase("Man")
					|| cell.toString().equalsIgnoreCase("Male")) {
				field.set(user, Gender.Male);
			}
			/*
			 * else if (cell.toString().equals("") ||
			 * cell.toString().equals(null)) { field.set(user, Gender.Male); }
			 */
			else if (cell.toString().equals("") || cell == null) {
				field.set(user, Gender.Male);
			} else {
				field.set(user, Gender.Female);
			}
		} else if (field.getType().equals(double.class)) {
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			field.set(user, new Double(cell.getNumericCellValue()));
		} else if (field.getType().equals(Date.class)) {
			final String cellValue = ObjectUtils.getCellValue(cell);
			final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			final String strdate = cellValue;
			Date date = null;
			try {
				date = df.parse(strdate);
			} catch (final ParseException e) {
				// TODO Auto-generated catch block
				final SimpleDateFormat df2 = new SimpleDateFormat("MM/dd");
				final String strdate2 = cellValue;
				date = df2.parse(strdate2);
			}
			field.set(user, date);
		} else if (field.getType().equals(Integer.class)) {
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			field.set(user, Integer.parseInt(cell.toString()));
		} else {
			field.set(user, null);
		}
	}

	/**
	 * Find user by email.
	 *
	 * @param industryId
	 *            the industry id
	 * @param email
	 *            the email
	 * @return the user
	 */
	@Transactional
	public User findUserByEmail(final Integer industryId, final String email) {
		return this.userDAO.findByProperty("email", email);
	}

	/**
	 * List user by employee no.
	 *
	 * @param industryId
	 *            the industry id
	 * @param number
	 *            the number
	 * @return the list
	 */
	@Transactional
	public List<User> listUserByEmployeeNo(final Integer industryId,
			final String number) {
		return this.userDAO.listByProperty("number", number);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */

	@Override
	@Transactional
	@EventLog(action = EventAction.Created)
	public User create(final User input)
			throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException {

		return null;

	}

	/**
	 * Generate MD 5 hex password.
	 *
	 * @param pwd
	 *            the pwd
	 * @param salt
	 *            the salt
	 * @return the string
	 */
	public String generateMD5HexPassword(final String pwd, final String salt) {
		return DigestUtils.md5Hex(pwd + salt);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */

	@Override
	@Transactional
	@EventLog(action = EventAction.Updated)
	public User update(final User input) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#delete(java.lang.Integer)
	 */

	@Override
	@Transactional
	@EventLog(action = EventAction.Deleted)
	public User delete(final Integer id) throws RecordNotFoundException {
		final User dbObject = getDetails(id);
		this.userDAO.delete(dbObject);
		return dbObject;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#getDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public User getDetails(final Integer id) throws RecordNotFoundException {
		final User user = this.userDAO.findById(id);
		if (user == null) {
			throw new RecordNotFoundException();
		}

		return user;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public User find(final User input) throws RecordNotFoundException {
		return this.userDAO.findById(input.getId());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public List<User> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the users ids of role tenant.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param roleId
	 *            the role id
	 * @return the users ids of role tenant
	 */
	@Transactional(readOnly = true)
	public List<Integer> getUsersIdsOfRoleTenant(final Integer tenantId,
			final Integer roleId) {
		return this.userDAO.getUsersIdsOfRoleTenant(tenantId, roleId);
	}

	/**
	 * Gets the active users ids of role tenant.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param roleId
	 *            the role id
	 * @param status
	 *            the status
	 * @return the active users ids of role tenant
	 */
	@Transactional(readOnly = true)
	public List<Integer> getActiveUsersIdsOfRoleTenant(final Integer tenantId,
			final Integer roleId, final Status status) {
		return this.userDAO.getActiveUsersIdsOfRoleTenant(tenantId, roleId,
				status);
	}

	/**
	 * Gets the users ids of location group type group.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param locationGroupId
	 *            the location group id
	 * @param locationGroupTypeId
	 *            the location group type id
	 * @return the users ids of location group type group
	 */
	@Transactional(readOnly = true)
	public List<Integer> getUsersIdsOfLocationGroupTypeGroup(
			final Integer tenantLocationId, final Integer locationGroupId,
			final Integer locationGroupTypeId) {
		return this.userDAO.getUsersIdsOfLocationGroupTypeGroup(
				tenantLocationId, locationGroupId, locationGroupTypeId);
	}

	/**
	 * Gets the user ids.
	 *
	 * @param userId
	 *            the user id
	 * @return the user ids
	 */
	@Transactional(readOnly = true)
	public List<Integer> getUserIds(final Integer userId) {
		return this.userDAO.getUserIds(userId);
	}

	/**
	 * Gets the user by id.
	 *
	 * @param id
	 *            the id
	 * @return the user by id
	 */
	@Transactional(readOnly = true)
	public User getUserById(final Integer id) {
		// TODO Auto-generated method stub
		return this.userDAO.findById(id);
	}

	/**
	 * Logout user.
	 *
	 * @param userId
	 *            the user id
	 * @param industryId
	 *            the industry id
	 * @return the user
	 */
	@EventLog(action = EventAction.Logout)
	@Transactional(readOnly = true)
	public User logoutUser(final Integer userId, final Integer industryId) {
		final User user = this.userDAO.findById(userId);
		return user;
	}

	/**
	 * Find all.
	 *
	 * @param activeStatus
	 *            the active status
	 * @return the list
	 */
	@Transactional(readOnly = true)
	public List<Integer> findAll(final Status activeStatus) {
		// TODO Auto-generated method stub
		return this.userDAO.findAllIdsForAllUser(activeStatus);
	}

	/**
	 * Find users.
	 *
	 * @param keyword
	 *            the keyword
	 * @param activeStatus
	 *            the active status
	 * @return the list
	 */
	@Transactional(readOnly = true)
	public List<User> findUsers(final String keyword,
			final Status activeStatus) {
		return this.userDAO.findUsers(keyword, activeStatus);
	}

	/**
	 * Find team members.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param contributor
	 *            the contributor
	 * @return the list
	 */
	// public List<User> getUsersOfMasterLocations(final Integer industryID,
	// final Integer tenantId, final List<Integer> tenantLocationIds,
	// final List<Integer> roleIds) {
	// final QueryHelper q = this.queryHelperFactory
	// .getInstance(IndustryType.fromInteger(industryID));
	// final String query = q.getUsersOfMasterLocations(tenantId,
	// tenantLocationIds, roleIds);
	// return this.userDAO.getAvailableUsers(query);
	// }

	/**
	 * Find team members.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param contributor
	 *            the contributor
	 * @return the list
	 */
	public List<User> findTeamMembers(final Integer tenantLocationId,
			final boolean contributor) {
		return this.userDAO.findTeamMembers(tenantLocationId, contributor);
	}

	/**
	 * Find team member ids.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @param contributor
	 *            the contributor
	 * @return the list
	 */
	public List<Integer> findTeamMemberIds(final Integer tenantLocationId,
			final boolean contributor) {
		final List<User> users = this.userDAO.findTeamMembers(tenantLocationId,
				contributor);
		final List<Integer> userIds = new ArrayList<>();
		for (final User user : users) {
			userIds.add(user.getId());
		}
		// TODO Auto-generated method stub
		return userIds;
	}

	/**
	 * Find all onboard users.
	 *
	 * @param ids
	 *            the ids
	 * @return the list
	 */
	@Transactional(readOnly = true)
	public List<User> findAllOnboardUsers(final List<Integer> ids) {

		List<User> users = new ArrayList<>();
		if (ids != null) {
			users = this.userDAO.findAllUsers(ids, Status.Ready_to_onboard);
		}

		return users;
	}

	/**
	 * Fetch non fpg members.
	 *
	 * @param locationId
	 *            the location id
	 * @return the list
	 */
	@Transactional(readOnly = true)
	public List<User> fetchNonFpgMembers(final Integer locationId) {
		// TODO Auto-generated method stub
		return this.userDAO.fetchNonFpgMembers(locationId);
	}

	/**
	 * Verify user in LDAP.
	 *
	 * @param body
	 *            the body
	 * @param userdetail
	 *            the userdetail
	 * @param upad
	 *            the upad
	 * @param maxPasswordAttempt
	 *            the max password attempt
	 * @param enableAttamptFeature
	 *            the enable attampt feature
	 * @throws Exception
	 *             the exception
	 */
	@Transactional
	public void verifyUserInLDAP(final UserAuthentication body,
			final User userdetail, final UserPasswordAttemptDetail upad,
			final Integer maxPasswordAttempt,
			final boolean enableAttamptFeature) throws Exception {
		String base = null;
		final AppPreference appPreference = this.appPreferenceDAO
				.findByProperty("name", "ldap.server.baseDn");
		if (!Objects.isEmpty(appPreference)) {
			base = appPreference.getValue();
		}
		final AppPreference appPreferenceForUrl = this.appPreferenceDAO
				.findByProperty("name", "ldap.server.url");
		final String url = "ldap://" + appPreferenceForUrl.getValue();

		final String userToVerify = "CN=SVC_InGauge_Acct,OU=UO Service Accounts,DC=use,DC=ucdp,DC=net";
		final String pass_word = "rR5nwGBJ4H";
		final Hashtable<String, String> env = new Hashtable<>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, userToVerify);
		env.put(Context.SECURITY_CREDENTIALS, pass_word);
		try {
			final DirContext authContext = new InitialDirContext(env);
			logger.debug("Success To Authenticate Server.");
			final SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			final String searchFilter = "sAMAccountName=" + body.getUsername();
			final NamingEnumeration<SearchResult> answer = authContext
					.search(base, searchFilter, constraints);
			if (answer.hasMore()) {
				final SearchResult result = answer.next();
				final String distinguishedName = result.getNameInNamespace();
				final Hashtable<String, String> env1 = new Hashtable<>();
				env1.put(Context.INITIAL_CONTEXT_FACTORY,
						"com.sun.jndi.ldap.LdapCtxFactory");
				env1.put(Context.PROVIDER_URL, url);
				env1.put(Context.SECURITY_AUTHENTICATION, "simple");
				env1.put(Context.SECURITY_PRINCIPAL, distinguishedName);
				env1.put(Context.SECURITY_CREDENTIALS, body.getPassword());
				try {
					final DirContext authContext1 = new InitialDirContext(env1);
					logger.debug("Success To Login");
					final String convertedPassword = DigestUtils
							.md5Hex(body.getPassword());
					final String passwordToUpdate = new String(
							convertedPassword);
					userdetail.setPassword(passwordToUpdate);
					updateUserPassword(userdetail);
					authContext1.close();
				} catch (final AuthenticationException ex) {
					if (enableAttamptFeature && !Objects.isEmpty(upad)
							&& !upad.getIsLocked() && upad != null) {
						final Date lastAttempt = upad.getLastAttempt();
						final long secs = (new Date().getTime()
								- lastAttempt.getTime()) / 1000;
						final int hours = (int) (secs / 3600);
						if (hours < 24) {
							upad.setLoginFailedCount(
									upad.getLoginFailedCount() + 1);
						} else {
							upad.setLoginFailedCount(1);
						}

						upad.setLastAttempt(new Date());
						if (upad.getLoginFailedCount() == maxPasswordAttempt) {
							upad.setLastAttempt(new Date());
							upad.setIsLocked(true);
						}

					} else if (enableAttamptFeature && Objects.isEmpty(upad)) {
						final UserPasswordAttemptDetail newUpad = new UserPasswordAttemptDetail();
						newUpad.setUserId(userdetail.getId());
						newUpad.setIsLocked(false);
						newUpad.setLastAttempt(new Date());
						newUpad.setLoginFailedCount(1);

					}
					throw new InvalidPasswordException();
				} catch (final NamingException e) {
					throw new InvalidUserException("email",
							"User has no account");
				}
			} else {
				throw new InvalidUserException("email", "User has no account");
			}
			authContext.close();
		} catch (final AuthenticationException ex) {
			throw new InvalidPasswordException("Invalid LDAP Credentials.");
		} catch (final NamingException e) {
			throw new InvalidUserException("email",
					"Could not connect to LDAP Server");
		}
	}

	/**
	 * Update user password.
	 *
	 * @param userdetail
	 *            the userdetail
	 * @throws DuplicateEmailException
	 *             the duplicate email exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	private void updateUserPassword(final User userdetail)
			throws DuplicateEmailException, InvalidInputException,
			RecordNotFoundException {
		// TODO Auto-generated method stub
		update(userdetail);
	}

	/**
	 * Check user available in db.
	 *
	 * @param body
	 *            the body
	 * @return the user
	 * @throws InvalidUserException
	 *             the invalid user exception
	 */
	@Transactional(readOnly = true)
	public User checkUserAvailableInDb(final UserAuthentication body)
			throws InvalidUserException {
		final User user = this.userDAO.findByProperty("userVal",
				body.getUsername());
		if (user == null) {
			throw new InvalidUserException("email", "User has no account");
		}
		return user;
	}

	/**
	 * Find user by user val.
	 *
	 * @param industryId
	 *            the industry id
	 * @param username
	 *            the username
	 * @return the user
	 */
	@Transactional(readOnly = true)
	public User findUserByUserVal(final Integer industryId,
			final String username) {
		// TODO Auto-generated method stub
		return this.userDAO.findByProperty("userVal", username);
	}

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the user
	 */
	@Transactional(readOnly = true)
	public User findById(final Integer id) {
		// TODO Auto-generated method stub
		final User user = this.userDAO.findById(id);
		return user;
	}

	/**
	 * Delete super admin user location groups.
	 *
	 * @param input
	 *            the input
	 * @param industryId
	 *            the industry id
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */

	public void deleteSuperAdminUserLocationGroups(final UserDTO input,
			final Integer industryId) throws RecordNotFoundException {

	}

	/**
	 * Delete user from all location groups.
	 *
	 * @param input
	 *            the input
	 * @param industryId
	 *            the industry id
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	public void deleteUserFromAllLocationGroups(final UserDTO input,
			final Integer industryId) throws RecordNotFoundException {

	}

	/**
	 * Removes the linked in image URL.
	 *
	 * @param user
	 *            the user
	 */
	@Transactional
	public void removeLinkedInImageURL(final User user) {

		this.userDAO.update(user);
	}

	/**
	 * Gets the user based on permission and location access.
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the user based on permission and location access
	 */
	@Transactional(readOnly = true)
	public List<User> getUserBasedOnPermissionAndLocationAccess(
			final String sqlQuery) {
		return this.userDAO.getUserBasedOnPermissionAndLocationAccess(sqlQuery);
	}

	/**
	 * Gets the user list to send email.
	 *
	 * @param passwordExpireDays
	 *            the password expire days
	 * @return the user list to send email
	 */
	// Get users whose password are expired.
	@Transactional(readOnly = true)
	public List<User> getUserListToSendEmail(final String passwordExpireDays) {
		return this.userDAO.getUserListToEmailForPwdExpire(passwordExpireDays);
	}

	/**
	 * Gets the user list to send notification.
	 *
	 * @param daysForbeforeNotify
	 *            the days forbefore notify
	 * @param passwordExpireDays
	 *            the password expire days
	 * @return the user list to send notification
	 */
	// Get users whose password are going to be expired.
	@Transactional(readOnly = true)
	public List<User> getUserListToSendNotification(
			final String daysForbeforeNotify, final String passwordExpireDays) {
		return this.userDAO.getUsersToNotifyBeforePasswordExpire(
				daysForbeforeNotify, passwordExpireDays);
	}

	/**
	 * Gets the password validation msg.
	 *
	 * @param passwordValidationInput
	 *            the password validation input
	 * @return the password validation msg
	 * @throws InvalidUserException
	 *             the invalid user exception
	 */
	// Get environment specific password validation message.
	public String getPasswordValidationMsg(
			final PasswordValidation passwordValidationInput)
			throws InvalidUserException {
		final String PATTERN = ".{7,20}";
		final Pattern pattern = Pattern.compile(PATTERN);
		final Matcher matcher = pattern
				.matcher(passwordValidationInput.getPassword());
		if (!matcher.matches()) {
			throw new InvalidUserException("password",
					SystemConfigs.AUTHENTICATION_LENGTH_VALIDATION);
		}
		return "";
	}

	/**
	 * Gets the checks if is become UI tester visible.
	 *
	 * @return the checks if is become UI tester visible
	 */
	@Transactional(readOnly = true)
	public Boolean getIsBetaUITesterVisible() {
		boolean isBetaUITesterVisible = false;
		final Long total = this.userDAO.findUserCount("betaTester", true);
		Integer betaTesterMaxLimit = 0;
		final AppPreference appPreference = this.appPreferenceDAO
				.findByProperty("name", "app.isBetaTester.maxLimit");
		if (Objects.isNotNull(appPreference)
				&& Objects.isNotNull(appPreference.getValue())) {
			betaTesterMaxLimit = Integer.parseInt(appPreference.getValue());
		}
		if (total.intValue() <= betaTesterMaxLimit.intValue()) {
			isBetaUITesterVisible = true;
		}
		return isBetaUITesterVisible;
	}

	/**
	 * Gets the onboard user.
	 *
	 * @param industryID
	 *            the industry ID
	 * @param tenantId
	 *            the tenant id
	 * @param tenantLocationId
	 *            the tenant location id
	 * @return the onboard user
	 */
	@Transactional(readOnly = true)
	public List<User> getOnboardUser(final Integer industryID,
			final Integer tenantId, final Integer tenantLocationId) {

		return this.userDAO.getUsersIdsOfOnboardLocation(industryID, tenantId,
				tenantLocationId);
	}

	/**
	 * Generate random employee number.
	 *
	 * @param user
	 *            the user
	 * @return the string
	 */
	@Transactional
	public String generateRandomEmployeeNumber(final User user) {
		String pulseId = "";
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEmail();
		if (firstName != null && lastName != null && email != null
				&& firstName.length() > 0 && lastName.length() > 0
				&& email.length() > 0) {
			final String randomNumber = ObjectUtils
					.leftPadding(ObjectUtils.randomNumberFromRange(0, 9999), 2);
			firstName = firstName.trim();
			lastName = lastName.trim();
			email = email.trim();
			final String initialFirstName = firstName.substring(0, 1)
					.toUpperCase();
			final String initialLastName = lastName.substring(0, 1)
					.toUpperCase();
			final String initialEmail = email.substring(0, 1).toUpperCase();
			pulseId = initialFirstName + initialLastName + initialEmail
					+ randomNumber;
		}
		return pulseId;
	}

	/**
	 * Gets the location wise users.
	 *
	 * @param tenantLocationId
	 *            the tenant location id
	 * @return the location wise users
	 */
	@Transactional(readOnly = true)
	public List<User> getLocationWiseUsers(final Integer tenantLocationId) {
		return this.userDAO.getLocationWiseUsers(tenantLocationId);
	}

}
