/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.PasswordResetDAO;
import com.job.portal.exception.DeviceAlreadyExistsException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.InvalidUserException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.pojo.customer.PasswordReset;
import com.job.portal.pojo.customer.User;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.ObjectUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class PasswordResetService.
 */
@Service
public class PasswordResetService {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(PasswordResetService.class);

	/** The password reset DAO. */
	@Autowired
	private PasswordResetDAO passwordResetDAO;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The json date deserializer. */
	@Autowired
	JsonDateTimeDeserializer jsonDateDeserializer;

	/**
	 * Gets the details.
	 *
	 * @param id
	 *            the id
	 * @return the details
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Transactional(readOnly = true)
	public PasswordReset getDetails(final Integer id)
			throws RecordNotFoundException {
		final PasswordReset passwordReset = this.passwordResetDAO.findById(id);
		if (passwordReset == null) {
			throw new RecordNotFoundException();
		}
		return passwordReset;
	}

	/**
	 * Update.
	 *
	 * @param input
	 *            the input
	 * @return the password reset
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 */
	@Transactional
	public PasswordReset update(final PasswordReset input)
			throws RecordNotFoundException, InvalidInputException {
		final PasswordReset dbObject = getDetails(input.getId());
		final String excludes = "id,token,timestamp,user,accessedTime,oldId,industryId";
		ObjectUtils.copyFieldValues(input, dbObject, excludes);
		return this.passwordResetDAO.update(dbObject);
	}

	/**
	 * Validate token.
	 *
	 * @param token
	 *            the token
	 * @param tokenFlag
	 *            the token flag
	 * @return the user
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws DeviceAlreadyExistsException
	 *             the device already exists exception
	 */
	@Transactional
	public User validateToken(final String token, final Boolean tokenFlag)
			throws InvalidInputException, RecordNotFoundException,
			DeviceAlreadyExistsException {
		final PasswordReset reset = this.passwordResetDAO
				.findByProperty("token", token);
		if (reset != null) {
			reset.setAccessedTime(new Date());
			if (!(reset.isIsUsed())) {
				if (tokenFlag) {
					reset.setIsUsed(true);
					update(reset);
					return null;
				} else {
					if (reset.getUser().getId() != null) {
						return this.userService
								.getDetails(reset.getUser().getId());
					} else {
						throw new InvalidInputException();
					}
				}
			} else {
				throw new DeviceAlreadyExistsException();
			}
		} else {
			throw new RecordNotFoundException();
		}
	}

	/**
	 * Update user password.
	 *
	 * @param input
	 *            the input
	 * @return the user
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 */
	@Transactional
	public User updateUserPassword(final User input)
			throws RecordNotFoundException {
		input.setUpdatedBy(input);
		input.setUpdatedOn(new Date());
		this.userService.getDetails(input.getId());
		return this.userService.update(input);
	}

	/**
	 * Verify old password.
	 *
	 * @param oldPassword
	 *            the old password
	 * @param userId
	 *            the user id
	 * @return the string
	 * @throws InvalidUserException
	 *             the invalid user exception
	 */
	// Verify that current or old password is match or not.
	@Transactional(readOnly = true)
	public String verifyOldPassword(final String oldPassword,
			final Integer userId) throws InvalidUserException {
		final User user = this.userService.findById(userId);
		final String encryptedPassword = DigestUtils
				.md5Hex(oldPassword + user.getSalt());
		if (encryptedPassword.equals(user.getPassword())) {
			return "";
		} else {
			throw new InvalidUserException("oldPassword", "InValid Password");
		}
	}

}
