/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.StateDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.State;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseService.
 */
@Service
public class StateService implements JudService<State> {

	/** The course DAO. */
	@Autowired
	private StateDAO stateDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public State create(final State input) throws DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		return this.stateDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public State update(final State input)
			throws RecordNotFoundException, DuplicateRecordException {

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#delete(java.lang.Integer)
	 */
	@Override
	public State delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#getDetails(java.lang.Integer)
	 */
	@Override
	public State getDetails(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public State find(final State input) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	@Override
	public List<State> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public State getStateNameByCountry(final String stateName,
			final String countryName) {
		final State state = this.stateDAO.getStateNameByCountry(stateName,
				countryName);
		return state;
	}

}
