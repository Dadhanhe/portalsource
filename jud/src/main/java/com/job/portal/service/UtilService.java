/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.Constants;
import com.job.portal.utils.ObjectUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilService.
 */
@Service
public class UtilService implements JudService<AbstractValueObject> {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(UtilService.class);

	/** The env. */
	@Autowired
	private Environment env;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public AbstractValueObject create(final AbstractValueObject input)
			throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public AbstractValueObject update(final AbstractValueObject input)
			throws RecordNotFoundException, InvalidInputException,
			DuplicateRecordException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#delete(java.lang.Integer)
	 */
	@Override
	public AbstractValueObject delete(final Integer id)
			throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#getDetails(java.lang.Integer)
	 */
	@Override
	public AbstractValueObject getDetails(final Integer id)
			throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public AbstractValueObject find(final AbstractValueObject input)
			throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 * java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public List findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Index deleted data.
	 *
	 * @param core
	 *            the core
	 * @param withIndexFullCore
	 *            the with index full core
	 * @param solrCorePrimaryKey
	 *            the solr core primary key
	 * @param value
	 *            the value
	 * @return the pulse response body
	 * @throws Exception
	 *             the exception
	 */
	public PulseResponseBody<String> indexDeletedData(final String core,
			final boolean withIndexFullCore, final String solrCorePrimaryKey,
			final Integer[] value) throws Exception {
		// Need to add all cores except Product Metrics and Product Metrics
		// Audit Trail
		final StringBuilder ids = new StringBuilder();
		for (int i = 0; i < value.length; i++) {
			ids.append(String.valueOf(value[i]));
			if ((i + 1) < value.length) {
				ids.append(" OR ");
			}
		}
		SolrClient solr = null;
		try {
			solr = new HttpSolrClient.Builder(
					this.env.getProperty("solr.path") + core).build();
			solr.deleteByQuery("id:" + ids);
			solr.commit();
			if (withIndexFullCore) {
				indexFullCoreData(core);
			}
		} catch (final Exception e) {
		} finally {
			// Close the solr client
			try {
				if (solr != null) {
					solr.close();
				}
			} catch (final IOException e) {
				ObjectUtils.logException(logger, e, null);
			}
		}
		return new PulseResponseBody<>("Success", Constants.SUCCESS);
	}

	/**
	 * Index full core data.
	 *
	 * @param core
	 *            the core
	 * @return the pulse response body
	 * @throws Exception
	 *             the exception
	 */
	public PulseResponseBody<String> indexFullCoreData(final String core)
			throws Exception {
		final StringBuffer solrResponseStr = new StringBuffer();
		// Need to add all cores except Product Metrics and Product Metrics
		// Audit Trail

		final String url = this.env.getProperty("solr.path") + core
				+ "/dataimport?command=full-import&jdbcDB="
				+ this.env.getProperty("db.source.jdbcDB") + "&userName="
				+ this.env.getProperty("db.source.userName") + "&password="
				+ this.env.getProperty("db.source.password");
		try {
			final URL obj = new URL(url);
			final HttpURLConnection con = (HttpURLConnection) obj
					.openConnection();
			// optional default is GET
			con.setRequestMethod("GET");

			final BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			in.close();

		} catch (final Exception e) {
		}

		return new PulseResponseBody<>(solrResponseStr.toString(),
				Constants.SUCCESS);
	}

	/**
	 * Index solr data.
	 *
	 * @param cores
	 *            the cores
	 * @param isDeltaImport
	 *            the is delta import
	 * @return the pulse response body
	 */
	public PulseResponseBody<String> indexSolrData(final String[] cores,
			Boolean isDeltaImport) {
		String importType = "delta-import";
		if (Objects.isNull(isDeltaImport) || !isDeltaImport) {
			isDeltaImport = false;
			importType = "full-import";
		}
		List<String> urlList;

		final StringBuffer solrResponseStr = new StringBuffer();
		for (final String core : cores) {
			urlList = new ArrayList<>();
			if (!isDeltaImport) {
				urlList.add(this.env.getProperty("solr.path") + core
						+ "/update?stream.body=<delete><query>*:*</query></delete>&commit=true&jdbcDB="
						+ this.env.getProperty("db.source.jdbcDB")
						+ "&userName="
						+ this.env.getProperty("db.source.userName")
						+ "&password="
						+ this.env.getProperty("db.source.password"));
			}

			urlList.add(this.env.getProperty("solr.path") + core
					+ "/dataimport?command=" + importType + "&jdbcDB="
					+ this.env.getProperty("db.source.jdbcDB") + "&userName="
					+ this.env.getProperty("db.source.userName") + "&password="
					+ this.env.getProperty("db.source.password") + "&dbHost="
					+ this.env.getProperty("db.host"));

			// for solr optimization passed new String[0] as parameter
			final String[] urls = urlList.toArray(new String[0]);

			logger.debug("Core :: " + core);
			for (final String url : urls) {
				try {
					logger.debug("url :: " + url);
					final URL obj = new URL(url);
					final HttpURLConnection con = (HttpURLConnection) obj
							.openConnection();
					// optional default is GET
					con.setRequestMethod("GET");
					final int responseCode = con.getResponseCode();
					logger.debug("\nSending 'GET' request to URL : " + url);
					logger.debug("Response Code : " + responseCode);

					final BufferedReader in = new BufferedReader(
							new InputStreamReader(con.getInputStream()));
					String inputLine;
					final StringBuffer response = new StringBuffer();
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
						response.append("\n");
					}
					in.close();

					// print result
					solrResponseStr.append("\n");
					solrResponseStr.append("Url=" + url);
					solrResponseStr.append(response.toString());
					logger.debug("Final Solr Call :: " + response.toString());
					logger.debug("---- ---- ---- ---- ");
				} catch (final Exception e) {
					ObjectUtils.logException(logger, e, null);
				}
			}
			logger.debug(
					"===============================================================");
		}
		return new PulseResponseBody<>(solrResponseStr.toString(),
				Constants.SUCCESS);
	}

}
