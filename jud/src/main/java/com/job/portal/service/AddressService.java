/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.maps.GeoApiContext;
import com.job.portal.dao.CityDAO;
import com.job.portal.dao.CountryDAO;
import com.job.portal.dao.StateDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.Address;
import com.job.portal.pojo.customer.City;
import com.job.portal.pojo.customer.Country;
import com.job.portal.pojo.customer.State;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

@Service
public class AddressService implements JudService<Address> {

	@Autowired
	private CityDAO cityDao;

	@Autowired
	private StateDAO stateDao;

	@Autowired
	private CountryDAO countryDao;

	@Autowired
	StateService stateService;

	@Override
	public Address create(final Address input)
			throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address update(final Address input) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address delete(final Integer id) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getDetails(final Integer id) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address find(final Address input) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the secret.
	 *
	 * @return the secret
	 */
	public String getSecret() {
		return "AIzaSyCCUEX1CU9iFGsoSlGfTOqq8Q9K_J4BBpI";
	}

	/**
	 * Gets the google context.
	 *
	 * @return the google context
	 */
	public GeoApiContext getGoogleContext() {
		return new GeoApiContext().setApiKey(getSecret());
	}

	/**
	 * Gets the from state.
	 *
	 * @param stateName
	 *            the state name
	 * @return the from state
	 * @throws SystemConfigurationException
	 * @throws RecordNotFoundException
	 * @throws DuplicateRecordException
	 */
	@Transactional
	public City getFromState(final String cityName, final String stateName,
			final String countryName) throws DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		final City city = this.cityDao.getCityByStateAndCountry(cityName,
				stateName, countryName);
		if (city == null) {
			final City newCity = new City();
			final State state = this.stateDao.getStateNameByCountry(stateName,
					countryName);
			if (state == null) {
				final State newState = new State();
				final Country country = this.countryDao.findByProperty("name",
						countryName);
				if (country == null) {
					final Country newCountry = new Country();
					newCountry.setName(countryName);
					newCountry.setActiveStatus(Status.Active);
					final Country country1 = this.countryDao.save(newCountry);
					newState.setCountry(country1);
				} else {
					newState.setCountry(country);
				}
				newState.setName(stateName);
				final State state1 = this.stateService.create(newState);
				newCity.setState(state1);
			} else {
				newCity.setState(state);
			}
			newCity.setName(cityName);
			// return this.cityDao.save(newCity);
		}
		return city;
	}

}
