/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.CourseDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.Course;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseService.
 */
@Service
public class CourseService implements JudService<Course> {

	/** The course DAO. */
	@Autowired
	private CourseDAO courseDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Course create(final Course input) throws DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		return this.courseDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Course update(final Course input)
			throws RecordNotFoundException, DuplicateRecordException {
		final Course edu = this.courseDAO.findBySpecific(input.getId());
		edu.setName(input.getName());
		edu.setActiveStatus(input.getActiveStatus());
		edu.setUpdatedOn(new Date());
		return this.courseDAO.update(edu);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#delete(java.lang.Integer)
	 */
	@Override
	public Course delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#getDetails(java.lang.Integer)
	 */
	@Override
	public Course getDetails(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public Course find(final Course input) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	/**
	 * Gets the all university.
	 *
	 * @return the all university
	 */
	@Transactional(readOnly = true)
	public List<Course> getAllCourses() {
		final List<Course> coureses = this.courseDAO.getAllCourse();
		for (final Course uni : coureses) {
			Hibernate.initialize(uni.getCreatedBy());
			Hibernate.initialize(uni.getUpdatedBy());
		}
		return coureses;
	}

	@Override
	public List<Course> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

}
