/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.EducationBoardDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.EducationBoard;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

@Service
public class EducationBoardService implements JudService<EducationBoard> {

	@Autowired
	private EducationBoardDAO educationBoardDAO;

	@Override
	@Transactional
	public EducationBoard create(final EducationBoard input)
			throws DuplicateRecordException, RecordNotFoundException,
			SystemConfigurationException {
		return this.educationBoardDAO.save(input);
	}

	@Override
	@Transactional
	public EducationBoard update(final EducationBoard input)
			throws RecordNotFoundException, DuplicateRecordException {
		final EducationBoard edu = this.educationBoardDAO
				.findBySpecific(input.getId());

		edu.setName(input.getName());
		edu.setActiveStatus(input.getActiveStatus());
		edu.setUpdatedOn(new Date());
		return this.educationBoardDAO.update(edu);
	}

	@Override
	public EducationBoard delete(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	@Override
	public EducationBoard getDetails(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	@Override
	public EducationBoard find(final EducationBoard input)
			throws RecordNotFoundException {
		return null;
	}

	@Override
	public List<EducationBoard> findAll(final Integer first,
			final Integer total, final String search, final Status status,
			final String[] orderBy, final Sort[] sort) {
		return Collections.emptyList();
	}

	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	@Transactional(readOnly = true)
	public List<EducationBoard> getAllEducationBoard() {
		final List<EducationBoard> educationBoards = this.educationBoardDAO
				.getAllEducationBoard();
		for (final EducationBoard edu : educationBoards) {
			Hibernate.initialize(edu.getCreatedBy());
			Hibernate.initialize(edu.getUpdatedBy());
		}
		return educationBoards;
	}

}
