/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.SubindustryDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.Subindustry;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class IndustryService.
 */
@Service
public class SubindustryService implements JudService<Subindustry> {

	/** The industry DAO. */
	@Autowired
	private SubindustryDAO subindustryDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Subindustry create(final Subindustry input)
			throws DuplicateRecordException, RecordNotFoundException,
			SystemConfigurationException {
		return this.subindustryDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public Subindustry update(final Subindustry input)
			throws RecordNotFoundException, DuplicateRecordException {
		final Subindustry ini = this.subindustryDAO
				.findBySpecific(input.getId());

		ini.setName(input.getName());
		ini.setActiveStatus(input.getActiveStatus());
		ini.setUpdatedOn(new Date());
		ini.setIndustry(input.getIndustry());
		return this.subindustryDAO.update(ini);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#delete(java.lang.Integer)
	 */
	@Override
	public Subindustry delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#getDetails(java.lang.Integer)
	 */
	@Override
	public Subindustry getDetails(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public Subindustry find(final Subindustry input)
			throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status,
	 * java.lang.String[], com.job.portal.pojo.enums.Sort[])
	 */
	@Override
	public List<Subindustry> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.JudService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	/**
	 * Gets the all industry.
	 *
	 * @param industryId
	 *
	 * @return the all industry
	 */
	@Transactional(readOnly = true)
	public List<Subindustry> getAllSubIndustry(final Integer industryId) {
		return this.subindustryDAO.getAllSubIndustry(industryId);

	}
}
