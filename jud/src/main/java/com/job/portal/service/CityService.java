/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.CityDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.City;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class CourseService.
 */
@Service
public class CityService implements JudService<City> {

	/** The course DAO. */
	@Autowired
	private CityDAO cityDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public City create(final City input) throws DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		return this.cityDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.service.PulseService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public City update(final City input)
			throws RecordNotFoundException, DuplicateRecordException {

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#delete(java.lang.Integer)
	 */
	@Override
	public City delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#getDetails(java.lang.Integer)
	 */
	@Override
	public City getDetails(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public City find(final City input) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.service.PulseService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	@Override
	public List<City> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public City getCityByStateAndCountry(final String cityName,
			final String stateName, final String countryName) {
		// TODO Auto-generated method stub
		return this.cityDAO.getCityByStateAndCountry(cityName, stateName,
				countryName);
	}

}
