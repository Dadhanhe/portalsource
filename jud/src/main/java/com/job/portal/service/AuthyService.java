/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.job.portal.config.IgnoreCertificate;
import com.job.portal.exception.AuthyException;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.enums.AuthyAction;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthyService.
 */
@Service
public class AuthyService {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(AuthyService.class);

	/** The authy API key. */
	@Value("${authy.apiKey}")
	private String authyAPIKey;

	/** The authy service end point. */
	@Value("${authy.serviceEndPoint}")
	private String authyServiceEndPoint;

	/** The authy channel. */
	@Value("${authy.channel}")
	private String authyChannel;

	/** The authy format. */
	@Value("${authy.format}")
	private String authyFormat;

	/** The authy user API. */
	@Value("${authy.userAPI}")
	private String authyUserAPI;

	/** The authy user register. */
	@Value("${authy.userRegister}")
	private String authyUserRegister;

	/** The authy user delete. */
	@Value("${authy.deleteUser}")
	private String authyUserDelete;

	/** The authy register user activity. */
	@Value("${authy.registerUserActivity}")
	private String authyRegisterUserActivity;

	/** The authy user status. */
	@Value("${authy.userStatus}")
	private String authyUserStatus;

	/** The authy request SMS. */
	@Value("${authy.requestSMS}")
	private String authyRequestSMS;

	/** The authy verify token. */
	@Value("${authy.verifyToken}")
	private String authyVerifyToken;

	/**
	 * Instantiates a new authy service.
	 */
	protected AuthyService() {
		super();
	}

	/**
	 * Builds the URL.
	 *
	 * @param useCase
	 *            the use case
	 * @return the string
	 */
	private String buildURL(final String useCase) {
		String url = "";
		switch (useCase) {
		case "REGISTER_USER":
			url = this.authyServiceEndPoint.concat(this.authyChannel)
					.concat(this.authyFormat).concat(this.authyUserAPI)
					.concat(this.authyUserRegister);
			break;
		case "DELETE_USER":
			url = this.authyServiceEndPoint.concat(this.authyChannel)
					.concat(this.authyFormat).concat(this.authyUserAPI)
					.concat(this.authyUserDelete);
			break;
		case "VERIFY_TOKEN":
			url = this.authyServiceEndPoint.concat(this.authyChannel)
					.concat(this.authyFormat).concat(this.authyVerifyToken);
			break;
		case "REQUEST_SMS":
			url = this.authyServiceEndPoint.concat(this.authyChannel)
					.concat(this.authyFormat).concat(this.authyRequestSMS);

			break;
		default:
			break;
		}
		url = url.concat("?api_key=").concat(this.authyAPIKey);
		return url;

	}

	/**
	 * Gets the rest template.
	 *
	 * @return the rest template
	 */
	private RestTemplate getRestTemplate() {
		final SimpleClientHttpRequestFactory factory = new IgnoreCertificate();
		factory.setConnectTimeout(1000 * 20);// 20 s
		factory.setReadTimeout(1000 * 60 * 3);// 3 min
		try {
			IgnoreCertificate.trustAllHttpsCertificates();
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e,
					"Failed to get Rest Template :: ");
		}
		final RestTemplate restTemplate = new RestTemplate(factory);
		restTemplate.getMessageConverters()
				.add(new StringHttpMessageConverter());

		final FormHttpMessageConverter fmc = new FormHttpMessageConverter();
		restTemplate.getMessageConverters().add(fmc);
		return restTemplate;
	}

	/**
	 * Activate authy action.
	 *
	 * @param action
	 *            the action
	 * @param user
	 *            the user
	 * @param token
	 *            the token
	 * @return the JSON object
	 */
	public JSONObject activateAuthyAction(final AuthyAction action,
			final User user, final String token) {
		JSONObject response = null;
		if (action.equals(AuthyAction.REGISTERUSER)) {
			response = registerAuthyUser(action, user);
		} else if (action.equals(AuthyAction.DELETEUSER)) {
			response = deleteAuthyUser(action, user.getAuthyId());
		} else if (action.equals(AuthyAction.VERIFYTOKEN)) {
			response = verifyToken(action, user.getAuthyId(), token);
		} else if (action.equals(AuthyAction.REQUESTSMS)) {
			response = requestSMS(action, user.getAuthyId());
		}
		return response;
	}

	/**
	 * Verify token.
	 *
	 * @param action
	 *            the action
	 * @param authyID
	 *            the authy ID
	 * @param token
	 *            the token
	 * @return the JSON object
	 */
	private JSONObject verifyToken(final AuthyAction action,
			final String authyID, final String token) {
		if (Objects.isNull(authyID)) {
			return null;
		}
		String url = buildURL(AuthyAction.getEnumName(action.value()));
		final JSONObject bodyJson = new JSONObject();
		url = url.replace("{userAuthyID}", authyID);
		url = url.replace("{Token}", token);
		url = url.concat("&force=true");
		final HashMap<String, String> paramMap = new HashMap<>();
		final JSONObject response = sendRequest(url, bodyJson, JSONObject.class,
				"GET", paramMap);
		return response;
	}

	/**
	 * Request SMS.
	 *
	 * @param action
	 *            the action
	 * @param authyID
	 *            the authy ID
	 * @return the JSON object
	 */
	private JSONObject requestSMS(final AuthyAction action,
			final String authyID) {
		if (Objects.isNull(authyID)) {
			return null;
		}
		String url = buildURL(AuthyAction.getEnumName(action.value()));
		final JSONObject bodyJson = new JSONObject();
		url = url.replace("{userAuthyID}", authyID);
		final HashMap<String, String> paramMap = new HashMap<>();
		paramMap.put("force", "true");
		JSONObject response = null;
		try {
			response = sendRequest(url, bodyJson, JSONObject.class, "GET",
					paramMap);
		} catch (final Exception e) {
			throw new AuthyException(
					"Error occured on authy. Please try again later");
		}
		if (response != null) {
			logger.error("Authy call::" + response.toString());
		}
		return response;
	}

	/**
	 * Delete authy user.
	 *
	 * @param action
	 *            the action
	 * @param authyID
	 *            the authy ID
	 * @return the JSON object
	 */
	private JSONObject deleteAuthyUser(final AuthyAction action,
			final String authyID) {
		if (Objects.isNull(authyID)) {
			return null;
		}
		String url = buildURL(AuthyAction.getEnumName(action.value()));
		final JSONObject bodyJson = new JSONObject();
		url = url.replace("{userAuthyID}", authyID);
		final JSONObject response = sendRequest(url, bodyJson, JSONObject.class,
				"POST", null);
		if (response != null) {
			logger.error("Authy call::" + response.toString());
		}
		return response;
	}

	/**
	 * Register authy user.
	 *
	 * @param action
	 *            the action
	 * @param user
	 *            the user
	 * @return the JSON object
	 */
	private JSONObject registerAuthyUser(final AuthyAction action,
			final User user) {
		JSONObject response = null;
		if (Objects.isNotNull(user)
				&& !Objects.isEmpty(user.getPhoneNumber())) {
			final String url = buildURL(
					AuthyAction.getEnumName(action.value()));
			final JSONObject bodyJson = new JSONObject();
			final JSONObject userJson = new JSONObject();
			userJson.put("email", user.getEmail());
			userJson.put("cellphone", user.getPhoneNumber());
			bodyJson.put("user", userJson);
			response = sendRequest(url, bodyJson, JSONObject.class, "POST",
					null);
			if (response != null) {
				logger.error("Authy call::" + response.toString());
			}
		}
		return response;
	}

	/**
	 * Send request.
	 *
	 * @param <T>
	 *            the generic type
	 * @param url
	 *            the url
	 * @param bodyJson
	 *            the body json
	 * @param responseType
	 *            the response type
	 * @param methodType
	 *            the method type
	 * @param paramMap
	 *            the param map
	 * @return the t
	 */
	@SuppressWarnings("unchecked")
	private <T> T sendRequest(final String url, final JSONObject bodyJson,
			final Class<T> responseType, final String methodType,
			final Map<String, String> paramMap) {
		T response = null;
		final RestTemplate restTemplate = getRestTemplate();
		if (methodType.equalsIgnoreCase("POST")) {
			response = restTemplate.postForObject(url, bodyJson, responseType);
		} else if (methodType.equalsIgnoreCase("GET")) {
			try {
				response = restTemplate.getForObject(url, responseType,
						paramMap);
			} catch (final HttpClientErrorException e) {
				final JSONParser parser = new JSONParser(
						JSONParser.ACCEPT_SIMPLE_QUOTE);
				try {
					response = (T) parser.parse(e.getResponseBodyAsString());
				} catch (final ParseException e1) {
					ObjectUtils.logException(logger, e,
							"Failed to send request :: ");
				}
			}
		}
		return response;
	}
}
