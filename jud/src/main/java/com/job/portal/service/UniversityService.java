/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.job.portal.dao.UniversityDAO;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.customer.University;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class UniversityService.
 */
@Service
public class UniversityService implements JudService<University> {

	/** The university DAO. */
	@Autowired
	private UniversityDAO universityDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#create(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public University create(final University input)
			throws DuplicateRecordException, RecordNotFoundException,
			SystemConfigurationException {
		return this.universityDAO.save(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#update(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	@Transactional
	public University update(final University input)
			throws RecordNotFoundException, DuplicateRecordException {
		final University edu = this.universityDAO.findBySpecific(input.getId());

		edu.setName(input.getName());
		edu.setActiveStatus(input.getActiveStatus());
		edu.setUpdatedOn(new Date());
		return this.universityDAO.update(edu);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#delete(java.lang.Integer)
	 */
	@Override
	public University delete(final Integer id) throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#getDetails(java.lang.Integer)
	 */
	@Override
	public University getDetails(final Integer id)
			throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#find(com.job.portal.pojo.common.
	 * AbstractValueObject)
	 */
	@Override
	public University find(final University input)
			throws RecordNotFoundException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#findAll(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status,
	 * java.lang.String[], com.job.portal.pojo.enums.Sort[])
	 */
	@Override
	public List<University> findAll(final Integer first, final Integer total,
			final String search, final Status status, final String[] orderBy,
			final Sort[] sort) {
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.service.JudService#findTotal(java.lang.Integer,
	 * java.lang.Integer, java.lang.String, com.job.portal.pojo.enums.Status)
	 */
	@Override
	public Long findTotal(final Integer industryId, final Integer tenantId,
			final String search, final Status status) {
		return null;
	}

	/**
	 * Gets the all university.
	 *
	 * @return the all university
	 */
	@Transactional(readOnly = true)
	public List<University> getAllUniversity() {
		final List<University> university = this.universityDAO
				.getAllUniversity();
		for (final University uni : university) {
			Hibernate.initialize(uni.getCreatedBy());
			Hibernate.initialize(uni.getUpdatedBy());
		}
		return university;
	}

}
