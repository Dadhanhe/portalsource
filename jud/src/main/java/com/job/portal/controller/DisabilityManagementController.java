/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.aspect.Loggable;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Disability;
import com.job.portal.pojo.customer.dto.DisabilityDTO;
import com.job.portal.pojo.customer.mapper.IDisabilityManagementMapper;
import com.job.portal.pojo.enums.BoardType;
import com.job.portal.service.DisabilityManagementService;
import com.job.portal.service.JudService;
import com.job.portal.utils.Constants;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class DisabilityManagementController.
 */
@RestController
@RequestMapping("/api/disability")
public class DisabilityManagementController
		extends AbstractController<Disability, DisabilityDTO> {

	/** The disability management service. */
	@Autowired
	private DisabilityManagementService disabilityManagementService;

	/** The i disability management mapper. */
	@Autowired
	private IDisabilityManagementMapper iDisabilityManagementMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		return this.disabilityManagementService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper getMapper() {
		return this.iDisabilityManagementMapper;
	}

	/**
	 * Gets the education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the education board
	 */
	@Loggable
	@RequestMapping(value = "/getAlldisabilityList", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<Disability>> getEducationBoard(
			@RequestParam(value = "boardType", required = false) final BoardType boardType) {
		final List<Disability> disbilities = this.disabilityManagementService
				.getAlldisabilityList();
		if (!Objects.isEmpty(disbilities)) {
			for (final Disability dis : disbilities) {
				Hibernate.initialize(dis.getCreatedBy());
				Hibernate.initialize(dis.getUpdatedBy());
			}
		}
		return new PulseResponseBody<>(disbilities, Constants.SUCCESS);
	}
}
