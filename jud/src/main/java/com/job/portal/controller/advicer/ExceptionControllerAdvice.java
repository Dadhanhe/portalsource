/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.controller.advicer;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import com.job.portal.exception.ColumnNotAvailableException;
import com.job.portal.exception.DuplicateEmailException;
import com.job.portal.exception.DuplicateFieldException;
import com.job.portal.exception.DuplicateMetricException;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.DuplicateTaskException;
import com.job.portal.exception.DuplicateUserIdException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.InvalidPasswordException;
import com.job.portal.exception.InvalidUserException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.pojo.common.FieldErrorResource;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.utils.Constants;
import com.job.portal.utils.ObjectUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class ExceptionControllerAdvice.
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(ExceptionControllerAdvice.class);

	/** The env. */
	@Autowired
	private Environment env;

	/**
	 * Handle run time exception.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the pulse response body
	 */
	@ExceptionHandler({ RuntimeException.class })
	@ResponseBody
	@ResponseStatus(code = HttpStatus.OK)
	protected PulseResponseBody<Object> handleRunTimeException(
			final RuntimeException e, final WebRequest request) {
		ObjectUtils.logException(logger, e, "RuntimeException :: ");
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final List<FieldErrorResource> fieldErrorResources = new ArrayList<FieldErrorResource>();
		String responseMessage = "";
		if (e instanceof ProviderNotFoundException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("Login");
			fieldErrorResource.setField("email");
			fieldErrorResource.setCode("NO_USER");
			fieldErrorResource.setMessage("User not registered");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = this.env.getProperty("ERROR.PROVIDERNOTFOUND");
		} else if (e instanceof ConstraintViolationException) {
			final javax.validation.ConstraintViolationException ire = (javax.validation.ConstraintViolationException) e;
			final Set<ConstraintViolation<?>> violationExceptions = ire
					.getConstraintViolations();
			for (final ConstraintViolation<?> violation : violationExceptions) {
				final FieldErrorResource fieldErrorResource = new FieldErrorResource();
				fieldErrorResource
						.setResource(violation.getLeafBean().toString());
				fieldErrorResource
						.setField(violation.getPropertyPath().toString());
				fieldErrorResource.setMessage(violation.getMessage());
				fieldErrorResources.add(fieldErrorResource);

			}
			responseMessage = this.env
					.getProperty("ERROR.CONSTRAINTVIOLATIONEXCEPTION");
		} else if (e instanceof InvalidInputException) {
			final InvalidInputException ire = (InvalidInputException) e;
			final List<FieldError> fieldErrors = ire.getErrors()
					.getFieldErrors();
			for (final FieldError fieldError : fieldErrors) {
				final FieldErrorResource fieldErrorResource = new FieldErrorResource();
				fieldErrorResource.setResource(fieldError.getObjectName());
				fieldErrorResource.setField(fieldError.getField());
				fieldErrorResource.setCode(fieldError.getCode());
				fieldErrorResource.setMessage(fieldError.getDefaultMessage());
				fieldErrorResources.add(fieldErrorResource);
			}
			responseMessage = this.env
					.getProperty("ERROR.INVALIDINPUTEXCEPTION");
		} else if (e instanceof DuplicateFieldException) {
			final DuplicateFieldException ire = (DuplicateFieldException) e;
			final String fieldErrors = ire.getFieldName();
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource(ire.getResource().getSimpleName());
			fieldErrorResource.setField(fieldErrors);
			fieldErrorResource.setCode("duplicate");
			fieldErrorResource.setMessage("must be unique");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = fieldErrors
					+ this.env.getProperty("ERROR.DUPLICATEFIELDEXCEPTION");
		} else if (e instanceof DuplicateTaskException) {
			final DuplicateTaskException ire = (DuplicateTaskException) e;
			final String fieldErrors = ire.getFieldName();
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource(ire.getResource().getSimpleName());
			fieldErrorResource.setField(fieldErrors);
			fieldErrorResource.setCode("duplicate");
			fieldErrorResource.setMessage("Task Already Exist.");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = fieldErrors
					+ this.env.getProperty("ERROR.DUPLICATETASKEXCEPTION");
		} else if (e instanceof InvalidParameterException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField("Error");
			fieldErrorResource.setCode("Invalid");
			fieldErrorResource.setMessage("Parameter is invalid");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = this.env
					.getProperty("ERROR.INVALIDPARAMETEREXCEPTION");
		} else if (e instanceof DuplicateEmailException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField("email");
			fieldErrorResource.setCode("duplicateEmail");
			fieldErrorResource.setMessage("Duplicate Email Address");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = this.env
					.getProperty("ERROR.DUPLICATEEMAILEXCEPTION");
		} else if (e instanceof DuplicateMetricException) {
			final DuplicateMetricException ire = (DuplicateMetricException) e;
			final String fieldErrors = ire.getFieldName();
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource(ire.getResource().getSimpleName());
			fieldErrorResource.setField(fieldErrors);
			fieldErrorResource.setCode("duplicate");
			fieldErrorResource.setMessage("duplicated with existing metrics.");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = this.env
					.getProperty("ERROR.DUPLICATEMETRICEXCEPTION");
		} else if (e instanceof DuplicateUserIdException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField("userVal");
			fieldErrorResource.setCode("duplicateUserId");
			fieldErrorResource.setMessage("Duplicate User Id.");
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = "Found Duplicate User Id.";
		} else {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField("error");
			fieldErrorResource.setCode(e.getMessage().toString());
			fieldErrorResource.setMessage(e.getMessage().toString());
			fieldErrorResources.add(fieldErrorResource);
			responseMessage = this.env.getProperty("ERROR.INTERNAL");
		}
		return new PulseResponseBody<Object>(fieldErrorResources,
				Constants.FAIL, responseMessage);
	}

	/**
	 * Handle exceptions.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the pulse response body
	 */
	@ExceptionHandler({ Exception.class })
	@ResponseBody
	@ResponseStatus(code = HttpStatus.OK)
	protected PulseResponseBody<Object> handleExceptions(final Exception e,
			final WebRequest request) {
		ObjectUtils.logException(logger, e, "RuntimeException :: ");
		final List<FieldErrorResource> fieldErrorResources = new ArrayList<FieldErrorResource>();
		if (e instanceof InvalidPasswordException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			final InvalidPasswordException iue = (InvalidPasswordException) e;
			fieldErrorResource.setResource("Login");
			fieldErrorResource.setField("password");
			fieldErrorResource.setCode("INVALID_PWD");
			fieldErrorResource.setMessage(iue.getMessage());
			fieldErrorResources.add(fieldErrorResource);
		} else if (e instanceof InvalidUserException) {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			final InvalidUserException iue = (InvalidUserException) e;
			fieldErrorResource.setResource("Login");
			fieldErrorResource.setField(
					iue.getFieldName() != null ? iue.getFieldName() : "");
			fieldErrorResource.setCode("INVALID_USER");
			fieldErrorResource.setMessage(iue.getMessage());
			fieldErrorResources.add(fieldErrorResource);
		} else if (e instanceof RecordNotFoundException) {
			final RecordNotFoundException ire = (RecordNotFoundException) e;
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			// fieldErrorResource.setResource(ire.getEntityName());
			fieldErrorResource.setMessage(ire.getMessage());
			fieldErrorResources.add(fieldErrorResource);
		} else if (e instanceof DuplicateRecordException) {
			final DuplicateRecordException ire = (DuplicateRecordException) e;
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField(
					ire.getFieldName() != null ? ire.getFieldName() : "");
			fieldErrorResource.setCode("duplicateName");
			fieldErrorResource.setMessage(ire.getMessage());
			fieldErrorResources.add(fieldErrorResource);
		} else if (e instanceof ColumnNotAvailableException) {
			final ColumnNotAvailableException ire = (ColumnNotAvailableException) e;
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource(ire.getEntityName());
			fieldErrorResource.setMessage(ire.getEntityName());
			fieldErrorResources.add(fieldErrorResource);
		} else {
			final FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource("");
			fieldErrorResource.setField("error");
			fieldErrorResource.setCode(e.getMessage().toString());
			fieldErrorResources.add(fieldErrorResource);
		}
		return new PulseResponseBody<Object>(fieldErrorResources,
				Constants.FAIL);
	}

}