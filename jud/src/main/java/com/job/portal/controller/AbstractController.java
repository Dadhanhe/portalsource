/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.job.portal.aspect.Loggable;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.Converter;
import com.job.portal.pojo.common.ListWrapper;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.service.JudService;
import com.job.portal.service.UserService;
import com.job.portal.utils.Constants;
import com.job.portal.utils.Objects;
import com.job.portal.utils.UnInitializedPropertyStripper;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractController.
 *
 * @param <D>
 *            the generic type
 * @param <E>
 *            the element type
 */

public abstract class AbstractController<D extends AbstractValueObject, E extends AbstractDTO>
		implements Controller<E> {
	// ------------------------------ FIELDS ------------------------------

	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The converter. */
	@Autowired
	Converter converter;

	/** The un initialized property stripper. */
	@Autowired
	UnInitializedPropertyStripper unInitializedPropertyStripper;

	/** The user service. */
	@Autowired
	UserService userService;

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	// --------------------- GETTER / SETTER METHODS ---------------------

	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public Logger getLogger() {
		return this.logger;
	}

	// ------------------------ INTERFACE METHODS ------------------------

	// --------------------- Interface Controller ---------------------

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.Controller#create(com.job.portal.pojo.common.
	 * dto. AbstractDTO, org.springframework.validation.BindingResult)
	 */

	@Override
	@RequestMapping(value = "/secure", method = RequestMethod.POST)
	public PulseResponseBody<E> create(
			@RequestBody @Validated(E.CreateObjectValidationGroup.class) final E input,
			final BindingResult bindingResults,
			final HttpServletRequest request)
			throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException {
		final D retValue = (D) createObj(input, bindingResults, request);
		final E eObj = convertToDTO(retValue, input);
		return new PulseResponseBody<E>(eObj, Constants.SUCCESS);
	}

	/**
	 * Creates the obj.
	 *
	 * @param input
	 *            the input
	 * @param bindingResults
	 *            the binding results
	 * @param request
	 *            the request
	 * @return the abstract value object
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public AbstractValueObject createObj(final E input,
			final BindingResult bindingResults,
			final HttpServletRequest request) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException {
		final User user = this.userService.getByToken();
		getLogger().debug("Creating record " + input);
		if (bindingResults.hasErrors()) {
			throw new InvalidInputException(
					"Invalid Inputs while creating a record", bindingResults);
		}

		getLogger().debug("The mapto attribute is " + input.getMapTo());
		final Class<? extends AbstractValueObject> classMapped = this.converter
				.getModelMapping(input.getMapTo());
		getLogger().debug("The mapped to class is " + input.getMapTo());
		getLogger().debug("The class mapped is " + classMapped);
		// final AbstractValueObject inputVO = this.modelMapper.map(input,
		// classMapped);
		final AbstractValueObject inputVO = this.getMapper().toEntity(input);
		inputVO.setCreatedBy(user);
		inputVO.setCreatedOn(Calendar.getInstance().getTime());
		inputVO.setUpdatedBy(user);
		inputVO.setUpdatedOn(Calendar.getInstance().getTime());

		final JudService service = getService();
		getLogger().debug("Calling " + service);
		AbstractValueObject retValue = null;
		try {
			retValue = service.create(inputVO);
		} catch (final SystemConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		retValue = strip(retValue);
		return retValue;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.Controller#update(com.job.portal.pojo.common.
	 * dto. AbstractDTO, org.springframework.validation.BindingResult)
	 */

	@Override
	@RequestMapping(value = "/secure", method = RequestMethod.PUT)
	public PulseResponseBody<E> update(
			@Validated(E.UpdateObjectValidationGroup.class) @RequestBody final E input,
			final BindingResult bindingResults,
			final HttpServletRequest request) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException {
		final D retValue = (D) updateObj(input, bindingResults, request);
		final E eObj = convertToDTO(retValue, input);
		return new PulseResponseBody<E>(eObj, Constants.SUCCESS);
	}

	/**
	 * Update obj.
	 *
	 * @param input
	 *            the input
	 * @param bindingResults
	 *            the binding results
	 * @param request
	 *            the request
	 * @return the abstract value object
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public AbstractValueObject updateObj(final E input,
			final BindingResult bindingResults,
			final HttpServletRequest request) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException {
		getLogger()
				.debug("Updating record " + input.getMapTo() + " : " + input);
		if (bindingResults.hasErrors()) {
			throw new InvalidInputException(
					"Invalid Inputs while updating a record", bindingResults);
		}
		final Class<? extends AbstractValueObject> classMapped = this.converter
				.getModelMapping(input.getMapTo());
		final User user = this.userService.getByToken();
		getLogger().debug("The mapped to class is " + input.getMapTo());
		getLogger().debug("The class mapped is " + classMapped);
		// final AbstractValueObject inputVO = this.modelMapper.map(input,
		// classMapped);
		final AbstractValueObject inputVO = this.getMapper().toEntity(input);
		inputVO.setUpdatedBy(user);
		inputVO.setUpdatedOn(Calendar.getInstance().getTime());
		final JudService service = getService();
		AbstractValueObject retValue = service.update(inputVO);
		retValue = strip(retValue);
		return retValue;
	}

	/**
	 * Convert to DTO.
	 *
	 * @param retValue
	 *            the ret value
	 * @param input
	 *            the input
	 * @return the e
	 */
	@SuppressWarnings("unchecked")
	public E convertToDTO(final D retValue, final E input) {
		// final AbstractDTO output = this.modelMapper.map(retValue,
		// (Class<? extends AbstractDTO>) this.converter
		// .getModelMapping(input.getMapTo() + "DTO"));
		final AbstractDTO output = this.getMapper().toDto(retValue);
		getLogger().debug("The output is " + output);
		return (E) output;
	}

	/**
	 * Gets the core name.
	 *
	 * @return the core name
	 */
	public abstract String getCoreName();

	/**
	 * Gets the details.
	 *
	 * @param id
	 *            the id
	 * @return the details
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/secure/{id}", method = RequestMethod.GET)
	public PulseResponseBody<E> getDetails(@PathVariable("id") final Integer id)
			throws RecordNotFoundException, InvalidInputException {
		final JudService service = getService();
		final D foundDetails = (D) service.getDetails(id);
		strip(foundDetails);
		getLogger()
				.debug(foundDetails.getClass().getSimpleName().toUpperCase());
		// final AbstractDTO dto = this.modelMapper.map(foundDetails,
		// (Class<? extends AbstractDTO>) this.converter.getModelMapping(
		// foundDetails.getClass().getSimpleName().toUpperCase()
		// + "DTO"));
		final AbstractDTO dto = this.getMapper().toDto(foundDetails);
		return new PulseResponseBody(dto, Constants.SUCCESS);
	}

	/**
	 * Gets the list.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param first
	 *            the first
	 * @param total
	 *            the total
	 * @param search
	 *            the search
	 * @param status
	 *            the status
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param request
	 *            the request
	 * @return the list
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = { "/secure",
			"/secure/" }, method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<ListWrapper<AbstractDTO>> getList(
			@RequestParam(value = "tenantId", required = false) final Integer tenantId,
			@RequestParam(value = "first", required = false) final Integer first,
			@RequestParam(value = "total", required = false) final Integer total,
			@RequestParam(value = "search", required = false) final String search,
			@RequestParam(value = "status", required = false) final String status,
			@RequestParam(value = "orderBy", required = false) final String[] orderBy,
			@RequestParam(value = "sort", required = false) final Sort[] sort,
			final HttpServletRequest request) throws RecordNotFoundException {

		final Integer industryId = Objects.getIndustryIdFromHeader(request);
		final JudService service = getService();
		final List<D> vo = service.findAll(first, total, search,
				Status.getEnum(status), orderBy, sort);
		final List<AbstractDTO> abstractDTOs = new ArrayList<>();
		for (final D abstractValueObject : vo) {
			addTransientData(abstractValueObject);
			// final AbstractDTO dto = this.modelMapper.map(abstractValueObject,
			// (Class<? extends AbstractDTO>) this.converter
			// .getModelMapping(abstractValueObject.getClass()
			// .getSimpleName().toUpperCase() + "DTO"));
			final AbstractDTO dto = this.getMapper().toDto(abstractValueObject);
			abstractDTOs.add(dto);
		}

		final ListWrapper<AbstractDTO> dtoWrapper = new ListWrapper<>();
		dtoWrapper.setData(abstractDTOs);
		dtoWrapper.setFirst(first == null ? 0 : first);
		dtoWrapper.setTotalInList(abstractDTOs.size());
		dtoWrapper.setTotal(service.findTotal(industryId, tenantId, search,
				Status.getEnum(status)));
		return new PulseResponseBody(dtoWrapper, Constants.SUCCESS);
	}

	/**
	 * Gets the service.
	 *
	 * @return the service
	 */
	@SuppressWarnings("rawtypes")
	public abstract JudService getService();

	/**
	 * Strip.
	 *
	 * @param input
	 *            the input
	 * @return the abstract value object
	 */
	public abstract AbstractValueObject strip(AbstractValueObject input);

	/**
	 * Gets the list type.
	 *
	 * @return the list type
	 */
	protected abstract Type getListType();

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.Controller#archiveDelete(java.lang.Integer)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@RequestMapping(value = "/secure/{inputId}", method = RequestMethod.DELETE)
	public PulseResponseBody<E> archiveDelete(
			@PathVariable final Integer inputId) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException {
		final JudService service = getService();
		service.delete(inputId);
		return new PulseResponseBody(true, Constants.SUCCESS);
	}

	/**
	 * Verify header.
	 *
	 * @param request
	 *            the request
	 * @param searchKeyWord
	 *            the search key word
	 * @return the boolean
	 */
	public Boolean verifyHeader(final HttpServletRequest request,
			final String searchKeyWord) {
		Boolean containsKey = false;
		final Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			final String key = headerNames.nextElement();
			if (key.equalsIgnoreCase(searchKeyWord)) {
				containsKey = true;
				break;
			}
		}
		return containsKey;
	}

	/**
	 * Adds the transient data.
	 *
	 * @param abstractValueObject
	 *            the abstract value object
	 */
	public void addTransientData(
			final AbstractValueObject abstractValueObject) {

	}

	/**
	 * Gets the mapper.
	 *
	 * @return the mapper
	 */
	public abstract IAbstractMapper<D, E> getMapper();
}
