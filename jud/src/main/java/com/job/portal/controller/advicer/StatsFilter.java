/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.controller.advicer;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.job.portal.utils.Objects;
import com.job.portal.utils.SystemConfigs;

// TODO: Auto-generated Javadoc
/**
 * The Class StatsFilter.
 */
@Component
@WebFilter("/*")
public class StatsFilter implements Filter {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(StatsFilter.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		// empty
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(final ServletRequest req, final ServletResponse resp,
			final FilterChain chain) throws IOException, ServletException {
		long time = System.currentTimeMillis();
		String isMobileStr = "false";
		String token = "-1";
		final Double randomNumber = Math.random();
		try {
			if (req instanceof HttpServletRequest) {
				final HttpServletRequest httpRequest = (HttpServletRequest) req;
				isMobileStr = httpRequest
						.getHeader(SystemConfigs.HEADER_IS_MOBILE);
			}
			token = !Objects.isEmpty(
					SecurityContextHolder.getContext().getAuthentication())
							? String.valueOf(SecurityContextHolder.getContext()
									.getAuthentication().getPrincipal())
							: "-1";
		} catch (final Exception e) {
			// TODO: handle exception
		}
		logger.debug("{} - {} - {} - {}",
				((HttpServletRequest) req).getRequestURI(), token, isMobileStr,
				randomNumber);
		try {
			chain.doFilter(req, resp);
		} finally {
			time = System.currentTimeMillis() - time;
			logger.debug("{} - {} - {} - {}: {} ms",
					((HttpServletRequest) req).getRequestURI(), token,
					isMobileStr, randomNumber, time);

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// empty
	}
}