/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.Files;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.job.portal.aspect.Loggable;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.InvalidPasswordException;
import com.job.portal.exception.InvalidUserException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.helper.UserHelper;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.UserAuthentication;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.PasswordReset;
import com.job.portal.pojo.customer.Token;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.UserPasswordAttemptDetail;
import com.job.portal.pojo.customer.dto.UserDTO;
import com.job.portal.pojo.customer.mapper.IUserMapper;
import com.job.portal.pojo.enums.Sort;
import com.job.portal.pojo.enums.Status;
import com.job.portal.service.JudService;
import com.job.portal.service.UserService;
import com.job.portal.utils.Constants;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;
import com.job.portal.utils.UserSession;

// TODO: Auto-generated Javadoc
/**
 * The Class UserController.
 */
@RestController
@RequestMapping("/api/user")
public class UserController extends AbstractController<User, UserDTO> {

	/** The user service. */
	@Autowired
	private UserHelper userHelper;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	/** The env. */
	@Autowired
	private Environment env;

	// /** The user session helper. */
	// @Autowired
	// private UserSessionHelper userSessionHelper;

	/** The user mapper. */
	@Autowired
	private IUserMapper userMapper;

	/**
	 * Gets the by token.
	 *
	 * @return the by token
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@RequestMapping(value = {
			"/secure/getByToken" }, method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<User> getByToken() throws RecordNotFoundException {
		return new PulseResponseBody<>(this.userService.getByToken(),
				Constants.SUCCESS);
	}

	/**
	 * Authenticate.
	 *
	 * @param body
	 *            the body
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	@ResponseBody
	@RequestMapping(value = "/authenticateUser", method = RequestMethod.POST)
	public PulseResponseBody<Token> authenticate(
			@RequestBody final UserAuthentication body)
			throws RecordNotFoundException, InvalidUserException,
			NoSuchAlgorithmException, InvalidPasswordException,
			InvalidInputException, DuplicateRecordException,
			SystemConfigurationException {
		final String token = authenticationPostHttp(body);
		// final String feedOptions = feed.getOptions();
		final JsonParser parser = new JsonParser();
		final JsonElement json = parser.parse(token);
		final JsonObject jsonObject = json.getAsJsonObject();
		final String access_token = jsonObject.get("access_token")
				.getAsString();
		final Double expires_in = jsonObject.get("expires_in").getAsDouble();
		final String refresh_token = jsonObject.get("refresh_token")
				.getAsString();
		final String scope = jsonObject.get("scope").getAsString();
		final String token_type = jsonObject.get("token_type").getAsString();
		final Token t = new Token();
		t.setAccess_token(access_token);
		t.setExpires_in(expires_in);
		t.setRefresh_token(refresh_token);
		t.setScope(scope);
		t.setToken_type(token_type);

		if (!Objects.isEmpty(token)) {
			final User user = this.userService.findUserByEmail(null,
					body.getUsername());

			if (user.getId() != null && !Objects.isEmpty(user.getId())) {
				// this sets everything in memory - and eventually be removed
				UserSession.setValueInMap(user.getId());
				// this sets everything in DB and memory both
				// this.userSessionHelper.setUserActive(user.getId());
			}

			return new PulseResponseBody<>(t, Constants.SUCCESS);
		} else {
			return new PulseResponseBody<>(null, Constants.FAIL);
		}
	}

	/**
	 * Authentication post http.
	 *
	 * @param body
	 *            the body
	 * @return the string
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 */
	public String authenticationPostHttp(final UserAuthentication body)
			throws NoSuchAlgorithmException, InvalidPasswordException,
			RecordNotFoundException, InvalidUserException {
		String token = "";
		final String baseURL = this.env.getProperty("pulse.system.baseurl");
		int statusCode = 0;
		try {
			final URL url = new URL(baseURL + "/oauth/token");
			final HttpURLConnection httpCon = (HttpURLConnection) url
					.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("POST");
			httpCon.setRequestProperty("content-type",
					"application/x-www-form-urlencoded");
			final DataOutputStream wr = new DataOutputStream(
					httpCon.getOutputStream());
			wr.writeBytes("client_id=" + body.getClient_id() + "&client_secret="
					+ body.getClient_secret() + "&grant_type="
					+ body.getGrant_type() + "&username=" + body.getUsername()
					+ "&password=" + body.getPassword() + "&platform="
					+ body.getPlatform());
			statusCode = httpCon.getResponseCode();
			if (statusCode == HttpsURLConnection.HTTP_OK) {
				String line;
				final BufferedReader br = new BufferedReader(
						new InputStreamReader(httpCon.getInputStream()));
				while ((line = br.readLine()) != null) {
					token += line;
				}
				br.close();
			}
			wr.flush();
			wr.close();
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
		if (statusCode != 200) {
			this.userService.authenticateUser(body.getUsername(),
					body.getPassword(), body.getPlatform());
		}
		return token;
	}

	/**
	 * Reset password.
	 *
	 * @param passwordReset
	 *            the password reset
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws GeneralSecurityException
	 *             the general security exception
	 */
	@Loggable
	@RequestMapping(value = { "/reset_password" }, method = RequestMethod.POST)
	@ResponseBody
	public PulseResponseBody<String> resetPassword(
			@RequestBody final PasswordReset passwordReset,
			final HttpServletRequest request)
			throws RecordNotFoundException, GeneralSecurityException {
		final String response = this.userService.resetPassword(
				passwordReset.getPassword(), passwordReset.getToken());
		return new PulseResponseBody<>(response, Constants.SUCCESS);
	}

	/**
	 * Verify secure 2 FA token.
	 *
	 * @param userId
	 *            the user id
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@RequestMapping(value = {
			"/verifySecure2FAToken" }, method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<HashMap<String, Boolean>> verifySecure2FAToken(
			@RequestParam("userId") final Integer userId,
			final HttpServletRequest request) throws RecordNotFoundException {
		final String secureToken = request.getHeader("secureToken");
		final Boolean response = this.userService.verifySecure2FAToken(userId,
				secureToken);
		final HashMap<String, Boolean> responseObj = new HashMap<>();
		responseObj.put("is2FATokenVerified", response);
		return new PulseResponseBody<>(responseObj, Constants.SUCCESS);
	}

	/**
	 * Update.
	 *
	 * @param input
	 *            the input
	 * @param bindingResult
	 *            the binding result
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#update(com.job.portal.pojo.
	 * common.dto.AbstractDTO, org.springframework.validation.BindingResult)
	 */
	@Loggable
	@RequestMapping(value = { "/secure" }, method = RequestMethod.PUT)
	@ResponseBody
	@Override
	public PulseResponseBody<UserDTO> update(
			@RequestBody @Valid final UserDTO input,
			final BindingResult bindingResult, final HttpServletRequest request)
			throws RecordNotFoundException, DuplicateRecordException {

		final PulseResponseBody<UserDTO> userDTO = super.update(input,
				bindingResult, request);
		// final User inputVO = this.modelMapper.map(userDTO.getData(),
		// User.class);
		return new PulseResponseBody<>(userDTO.getData(), Constants.SUCCESS);
	}

	/**
	 * Resend SMS.
	 *
	 * @param userID
	 *            the user ID
	 * @param user
	 *            the user
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws InvalidInputException
	 *             the invalid input exception
	 */
	@RequestMapping(value = {
			"/resendSMS/{userID}" }, method = RequestMethod.PUT)
	@ResponseBody
	public PulseResponseBody<String> resendSMS(
			@PathVariable final Integer userID, @RequestBody final User user,
			final HttpServletRequest request) {
		this.userService.resendSMS(userID);
		return new PulseResponseBody<>("", Constants.SUCCESS);
	}

	/**
	 * Gets the default avatar.
	 *
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the default avatar
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/defaultAvatar", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public void getDefaultAvatar(final HttpServletResponse response,
			final HttpSession session) throws Exception {

		final String path = session.getServletContext()
				.getRealPath("/WEB-INF/classes/images/default_avatar.jpg");
		final File file = new File(path);
		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		final String headerKey = "Content-Disposition";
		final String headerValue = String.format("attachment; filename=\"%s\"",
				"test");
		response.setHeader(headerKey, headerValue);
		// final byte[] data = FileUtils.readFileToByteArray(file);
		// FileCopyUtils.copy(data, response.getOutputStream());
		Files.copy(file, response.getOutputStream());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#create(com.job.portal.pojo.
	 * common.dto.AbstractDTO, org.springframework.validation.BindingResult)
	 */
	/*
	 * @Loggable
	 *
	 * @RequestMapping(value = { "/secure", "/secure/" }, method =
	 * RequestMethod.GET)
	 *
	 * @ResponseBody public PulseResponseBody<ListWrapper<UserDTO>> searchUsers(
	 *
	 * @RequestParam(value = "tenantId", required = false) final Integer
	 * tenantId,
	 *
	 * @RequestParam(value = "first", required = false) final Integer first,
	 *
	 * @RequestParam(value = "total", required = false) final Integer total,
	 *
	 * @RequestParam(value = "roleId", required = false) final Integer roleId,
	 *
	 * @RequestParam(value = "status", required = false) final Status status,
	 *
	 * @RequestParam(value = "search", required = false) final String search,
	 * final HttpServletRequest request) throws RecordNotFoundException { //
	 * final ListWrapper<UserDTO> users = this.userService.search(tenantId, //
	 * first, total, roleId, null, status, search); final ListWrapper<UserDTO>
	 * users = this.userService.search(tenantId, first, total, roleId, null,
	 * status, search); return new
	 * PulseResponseBody<ListWrapper<UserDTO>>(users, Constants.SUCCESS); }
	 */

	/**
	 * Save.
	 *
	 * @param input
	 *            the input
	 * @param bindingResult
	 *            the binding result
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	@Loggable
	@RequestMapping(value = { "/secure" }, method = RequestMethod.POST)
	@ResponseBody
	@Override
	public PulseResponseBody<UserDTO> create(
			@RequestBody @Valid final UserDTO input,
			final BindingResult bindingResult, final HttpServletRequest request)
			throws InvalidInputException, DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		final PulseResponseBody<UserDTO> userDTO = super.create(input,
				bindingResult, request);
		return new PulseResponseBody<>(userDTO.getData(), Constants.SUCCESS);
	}

	/**
	 * Check for pulse id.
	 *
	 * @param firstName
	 *            the first name
	 * @param lastName
	 *            the last name
	 * @param birthMonth
	 *            the birth month
	 * @param birthDay
	 *            the birth day
	 * @param notUserID
	 *            the not user ID
	 * @param activeStatus
	 *            the active status
	 * @param queryPulseID
	 *            the query pulse ID
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@RequestMapping(value = "/secure/checkForPulseId", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<User>> checkForPulseId(
			@RequestParam(value = "firstName", required = false) final String firstName,
			@RequestParam(value = "lastName", required = false) final String lastName,
			@RequestParam(value = "birthMonth", required = false) final Integer birthMonth,
			@RequestParam(value = "birthDay", required = false) final Integer birthDay,
			@RequestParam(value = "notUserID", required = false) final Integer notUserID,
			@RequestParam(value = "activeStatus", required = false) final Status activeStatus,
			@RequestParam(value = "queryPulseID", required = false) final String queryPulseID)
			throws RecordNotFoundException {
		this.userService.getByToken();
		final List<User> userInfos = this.userService.getUserPulseIDInfos(null,
				firstName, lastName, birthMonth, birthDay, notUserID,
				activeStatus, queryPulseID);
		return new PulseResponseBody<>(userInfos, Constants.SUCCESS);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return Constants.USER;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@Override
	public JudService<?> getService() {
		return this.userService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		input.setCreatedBy(null);
		input.setUpdatedBy(null);
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		getLogger().debug("Sending the list type back");
		final Type listType = new TypeToken<ArrayList<UserDTO>>() {
		}.getType();
		return listType;
	}

	/**
	 * Gets the non fpg members.
	 *
	 * @param locationId
	 *            the location id
	 * @param request
	 *            the request
	 * @return the non fpg members
	 */
	@Loggable
	@ResponseBody
	@RequestMapping(value = "/secure/getNonFpgMembers/{locationId}", method = RequestMethod.GET, produces = {
			"application/json; charset=utf-8" })
	public PulseResponseBody<List<User>> getNonFpgMembers(
			@PathVariable("locationId") final Integer locationId,
			final HttpServletRequest request) {
		List<User> users = this.userService.fetchNonFpgMembers(locationId);
		if (Objects.isEmpty(users)) {
			users = new ArrayList<>();
		}
		return new PulseResponseBody<>(users, Constants.SUCCESS);
	}

	/**
	 * Authenticate user.
	 *
	 * @param body
	 *            the body
	 * @return the pulse response body
	 * @throws Exception
	 *             the exception
	 */
	@ResponseBody
	@RequestMapping(value = "/authenticateUserUsingLDAP", method = RequestMethod.POST)
	public PulseResponseBody<Token> authenticateUser(
			@RequestBody final UserAuthentication body) throws Exception {
		final boolean enableAttamptFeature = false;
		final String apfValue = "0";
		final User userdetail = this.userService.findUserByEmail(null,
				body.getUsername());
		return authnticateUser(body, apfValue, userdetail,
				enableAttamptFeature);

	}

	/**
	 * Authnticate user.
	 *
	 * @param body
	 *            the body
	 * @param apfvalue
	 *            the apfvalue
	 * @param userdetail
	 *            the userdetail
	 * @param enableAttamptFeature
	 *            the enable attampt feature
	 * @return the pulse response body
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	private PulseResponseBody<Token> authnticateUser(
			final UserAuthentication body, final String apfvalue,
			final User userdetail, final boolean enableAttamptFeature)
			throws NoSuchAlgorithmException, InvalidPasswordException,
			RecordNotFoundException, InvalidUserException,
			InvalidInputException, DuplicateRecordException,
			SystemConfigurationException {
		final String token = authenticationPostHttpForLDAP(body, apfvalue,
				userdetail, enableAttamptFeature);
		final JsonParser parser = new JsonParser();
		final JsonElement json = parser.parse(token);
		final JsonObject jsonObject = json.getAsJsonObject();
		final String access_token = jsonObject.get("access_token")
				.getAsString();
		final Double expires_in = jsonObject.get("expires_in").getAsDouble();
		final String refresh_token = jsonObject.get("refresh_token")
				.getAsString();
		final String scope = jsonObject.get("scope").getAsString();
		final String token_type = jsonObject.get("token_type").getAsString();
		final Token t = new Token();
		t.setAccess_token(access_token);
		t.setExpires_in(expires_in);
		t.setRefresh_token(refresh_token);
		t.setScope(scope);
		t.setToken_type(token_type);
		if (!Objects.isEmpty(token)) {
			if (apfvalue.equals("1")) {
				this.userService.findUserByUserVal(null, body.getUsername());
			} else {
				this.userService.findUserByEmail(null, body.getUsername());
			}
			return new PulseResponseBody<>(t, Constants.SUCCESS);
		} else {
			return new PulseResponseBody<>(null, Constants.FAIL);
		}
	}

	/**
	 * Authentication post http for LDAP.
	 *
	 * @param body
	 *            the body
	 * @param ap
	 *            the ap
	 * @param userdetail
	 *            the userdetail
	 * @param enableAttamptFeature
	 *            the enable attampt feature
	 * @return the string
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidPasswordException
	 *             the invalid password exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidUserException
	 *             the invalid user exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	public String authenticationPostHttpForLDAP(final UserAuthentication body,
			final String ap, final User userdetail,
			final boolean enableAttamptFeature) throws NoSuchAlgorithmException,
			InvalidPasswordException, RecordNotFoundException,
			InvalidUserException, InvalidInputException,
			DuplicateRecordException, SystemConfigurationException {
		User user = null;
		final StringBuilder token = new StringBuilder("");
		final String baseURL = this.env.getProperty("pulse.system.baseurl");
		int statusCode = 0;
		try {
			// Added By sandip for SSO
			String convertedPassword = null;
			if (body.getPassword().split(":")[0].equals(
					Constants.BYPASS_SECURE_PASSWORD_RANDDOM_STRING_TO_STRENTHEN)) {
				convertedPassword = body.getPassword();
			} else {
				convertedPassword = DigestUtils.md5Hex(body.getPassword());
			}
			// Added By Sandip For SSO
			/*
			 * final String convertedPassword = DigestUtils
			 * .md5Hex(body.getPassword());
			 */
			final URL url = new URL(baseURL + "/oauth/token");
			final HttpURLConnection httpCon = (HttpURLConnection) url
					.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("POST");
			httpCon.setRequestProperty("content-type",
					"application/x-www-form-urlencoded");
			final DataOutputStream wr = new DataOutputStream(
					httpCon.getOutputStream());
			wr.writeBytes("client_id=" + body.getClient_id() + "&client_secret="
					+ body.getClient_secret() + "&grant_type="
					+ body.getGrant_type() + "&username="
					+ userdetail.getEmail() + "&password=" + convertedPassword
					+ "&platform=" + body.getPlatform());
			statusCode = httpCon.getResponseCode();
			if (statusCode == HttpsURLConnection.HTTP_OK) {
				String line;
				final BufferedReader br = new BufferedReader(
						new InputStreamReader(httpCon.getInputStream()));
				while ((line = br.readLine()) != null) {
					token.append(line);
				}
				br.close();
			}
			wr.flush();
			wr.close();
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
		if (body.getUsername() != null) {
			if (StringUtils.isNumeric(body.getUsername())) {
				final Integer userId = Integer.parseInt(body.getUsername());
				user = this.userService.findById(userId);
			} else {
				if (ap.equals("1")) {
					user = this.userService.findUserByUserVal(null,
							body.getUsername());
				} else {
					user = this.userService.findUserByEmail(null,
							body.getUsername());
				}
			}
		}
		final UserPasswordAttemptDetail userPasswordAttemptDetail = null;
		if (statusCode != 200) {
			this.userHelper.authenticateUserLDAP(user, body.getPassword(), ap,
					userPasswordAttemptDetail, null, enableAttamptFeature);
		}
		return token.toString();

	}

	/**
	 * Export user.
	 *
	 * @param tenantId
	 *            the tenant id
	 * @param status
	 *            the status
	 * @param roleId
	 *            the role id
	 * @param getRoleForTenantId
	 *            the get role for tenant id
	 * @param allowSameWeightRoleUsers
	 *            the allow same weight role users
	 * @param getAllUser
	 *            the get all user
	 * @param getAccessibleRoleUsersOnly
	 *            the get accessible role users only
	 * @param orderBy
	 *            the order by
	 * @param sort
	 *            the sort
	 * @param searchKeyword
	 *            the search keyword
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@RequestMapping(value = "/secure/export/{tenantId}", method = RequestMethod.GET)
	@ResponseBody
	public void exportUser(@PathVariable("tenantId") final Integer tenantId,
			@RequestParam(value = "status", required = false) final Status status,
			@RequestParam(value = "roleId", required = false) final Integer roleId,
			@RequestParam(value = "getRoleForTenantId", required = false) final Integer getRoleForTenantId,
			@RequestParam(value = "allowSameWeightUser", required = false) Boolean allowSameWeightRoleUsers,
			@RequestParam(value = "getAllUser", required = false) Boolean getAllUser,
			@RequestParam(value = "getAccessibleRoleUsersOnly", required = false) Boolean getAccessibleRoleUsersOnly,
			@RequestParam(value = "sortBy", required = false) final String orderBy,
			@RequestParam(value = "sortOrder", required = false) final Sort sort,
			@RequestParam(value = "searchKeyword", required = false) final String searchKeyword,
			final HttpServletRequest request,
			final HttpServletResponse response) throws RecordNotFoundException {

		Objects.getIndustryIdFromHeader(request);

		getAllUser = Objects.isEmpty(getAllUser) ? false : getAllUser;
		getAccessibleRoleUsersOnly = Objects.isEmpty(getAccessibleRoleUsersOnly)
				? true
				: getAccessibleRoleUsersOnly;
		allowSameWeightRoleUsers = Objects.isEmpty(allowSameWeightRoleUsers)
				? true
				: allowSameWeightRoleUsers;
		// status = Objects.isEmpty(status) ? Status.Normal : status;

		verifyHeader(request, "TimeZone");
		request.getHeader("TimeZone");
		try {

			response.setHeader("Content-Disposition",
					String.format("attachment;filename=%s.xlsx", "Users"));
			// response.setContentType(
			// "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setContentType("application/octet-stream");

			response.getOutputStream().flush();
		} catch (final IOException e) {
			ObjectUtils.logException(logger, e, "Failed to export Users.");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

	/**
	 * Gets the password validation msg.
	 *
	 * @param email the email
	 * @param request the request
	 * @return the password validation msg
	 * @throws RecordNotFoundException the record not found exception
	 */
	// Environment specific password validation message

	/**
	 * Gets the user by email.
	 *
	 * @param email
	 *            the email
	 * @param request
	 *            the request
	 * @return the user by email
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 */
	@Loggable
	@RequestMapping(value = {
			"/secure/getUserByEmail" }, method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<User> getUserByEmail(
			@RequestParam(value = "email", required = false) final String email,
			final HttpServletRequest request) throws RecordNotFoundException {
		return new PulseResponseBody<>(
				this.userService.findUserByEmail(null, email),
				Constants.SUCCESS);
	}

	/**
	 * Gets the checks if is become UI tester allow.
	 *
	 * IN-1657 : We've set max limit for allowing UI tester in app preference
	 * table this controller will returns true if button should visible else
	 * returns false
	 *
	 * @return the checks if is become UI tester allow
	 */
	@Loggable
	@RequestMapping(value = {
			"/getIsBetaUITesterVisible" }, method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<Boolean> getIsBetaUITesterVisible() {
		return new PulseResponseBody<>(
				this.userService.getIsBetaUITesterVisible(), Constants.SUCCESS);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper<User, UserDTO> getMapper() {
		return this.userMapper;
	}

}
