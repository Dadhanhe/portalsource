/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.aspect.Loggable;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Subindustry;
import com.job.portal.pojo.customer.dto.SubindustryDTO;
import com.job.portal.pojo.customer.mapper.ISubindustryMapper;
import com.job.portal.service.JudService;
import com.job.portal.service.SubindustryService;
import com.job.portal.utils.Constants;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardController.
 */
@RestController
@RequestMapping("/api/subindustry")
public class SubindustryController
		extends AbstractController<Subindustry, SubindustryDTO> {

	/** The education board service. */
	@Autowired
	private SubindustryService subindustryService;

	/** The i education board mapper. */
	@Autowired
	private ISubindustryMapper isubindustryMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		return this.subindustryService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper getMapper() {
		return this.isubindustryMapper;
	}

	/**
	 * Gets the education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the education board
	 */
	@Loggable
	@RequestMapping(value = "/getAllSubIndustry", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<SubindustryDTO>> getAllIndustry(
			@RequestParam(value = "industryId", required = false) final Integer industryId) {
		final List<Subindustry> industryies = this.subindustryService
				.getAllSubIndustry(industryId);
		final List<SubindustryDTO> subindustryDTOs = new ArrayList<SubindustryDTO>();
		if (!industryies.isEmpty()) {
			for (final Subindustry sub : industryies) {
				SubindustryDTO subDTO = new SubindustryDTO();
				subDTO = this.isubindustryMapper.toDto(sub);
				subindustryDTOs.add(subDTO);
			}
		}
		return new PulseResponseBody<>(subindustryDTOs, Constants.SUCCESS);
	}

}
