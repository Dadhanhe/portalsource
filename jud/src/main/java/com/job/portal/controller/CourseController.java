/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.aspect.Loggable;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Course;
import com.job.portal.pojo.customer.dto.CourseDTO;
import com.job.portal.pojo.customer.mapper.ICourseMapper;
import com.job.portal.pojo.enums.BoardType;
import com.job.portal.service.CourseService;
import com.job.portal.service.JudService;
import com.job.portal.utils.Constants;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardController.
 */
@RestController
@RequestMapping("/api/course")
public class CourseController extends AbstractController<Course, CourseDTO> {

	/** The education board service. */
	@Autowired
	private CourseService courseService;

	/** The i education board mapper. */
	@Autowired
	private ICourseMapper iCourseMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		return this.courseService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper getMapper() {
		return this.iCourseMapper;
	}

	/**
	 * Gets the education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the education board
	 */
	@Loggable
	@RequestMapping(value = "/getAllCourselist", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<Course>> getAllUniversity(
			@RequestParam(value = "boardType", required = false) final BoardType boardType) {
		final List<Course> courses = this.courseService.getAllCourses();
		if (!Objects.isEmpty(courses)) {
			for (final Course uni : courses) {
				Hibernate.initialize(uni.getCreatedBy());
				Hibernate.initialize(uni.getUpdatedBy());
			}
		}
		return new PulseResponseBody<>(courses, Constants.SUCCESS);
	}
}
