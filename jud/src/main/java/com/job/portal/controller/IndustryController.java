/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.aspect.Loggable;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Industry;
import com.job.portal.pojo.customer.dto.IndustryDTO;
import com.job.portal.pojo.customer.mapper.IIndustryMapper;
import com.job.portal.pojo.enums.BoardType;
import com.job.portal.service.IndustryService;
import com.job.portal.service.JudService;
import com.job.portal.utils.Constants;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardController.
 */
@RestController
@RequestMapping("/api/industry")
public class IndustryController
		extends AbstractController<Industry, IndustryDTO> {

	/** The education board service. */
	@Autowired
	private IndustryService industryService;

	/** The i education board mapper. */
	@Autowired
	private IIndustryMapper iIndustryMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		// TODO Auto-generated method stub
		return this.industryService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper getMapper() {
		return this.iIndustryMapper;
	}

	/**
	 * Gets the education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the education board
	 */
	@Loggable
	@RequestMapping(value = "/getAllIndustry", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<Industry>> getAllIndustry(
			@RequestParam(value = "boardType", required = false) final BoardType boardType) {
		final List<Industry> industryies = this.industryService
				.getAllIndustry();
		return new PulseResponseBody<>(industryies, Constants.SUCCESS);
	}

}
