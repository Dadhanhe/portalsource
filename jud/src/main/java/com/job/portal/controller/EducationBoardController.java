/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.aspect.Loggable;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.EducationBoard;
import com.job.portal.pojo.customer.dto.EducationBoardDTO;
import com.job.portal.pojo.customer.mapper.IEducationBoardMapper;
import com.job.portal.pojo.enums.BoardType;
import com.job.portal.service.EducationBoardService;
import com.job.portal.service.JudService;
import com.job.portal.utils.Constants;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationBoardController.
 */
@RestController
@RequestMapping("/api/educationBoard")
public class EducationBoardController
		extends AbstractController<EducationBoard, EducationBoardDTO> {

	/** The education board service. */
	@Autowired
	private EducationBoardService educationBoardService;

	/** The i education board mapper. */
	@Autowired
	private IEducationBoardMapper iEducationBoardMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		return this.educationBoardService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.
	 * common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return input;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper getMapper() {
		return this.iEducationBoardMapper;
	}

	/**
	 * Gets the education board.
	 *
	 * @param boardType
	 *            the board type
	 * @return the education board
	 */
	@Loggable
	@RequestMapping(value = "/getEducationBoardForTest", method = RequestMethod.GET)
	@ResponseBody
	public PulseResponseBody<List<EducationBoard>> getEducationBoard(
			@RequestParam(value = "boardType", required = false) final BoardType boardType) {
		final List<EducationBoard> educationBoards = this.educationBoardService
				.getAllEducationBoard();
		for (final EducationBoard edu : educationBoards) {
			Hibernate.initialize(edu.getCreatedBy());
			Hibernate.initialize(edu.getUpdatedBy());
		}
		return new PulseResponseBody<>(educationBoards, Constants.SUCCESS);
	}
}
