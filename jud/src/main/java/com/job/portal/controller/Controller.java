/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindingResult;

import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.InvalidInputException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.dto.AbstractDTO;

// TODO: Auto-generated Javadoc
//Created by Sapan on 28/11/16.
/**
 * The Interface Controller.
 *
 * @param <E>
 *            the element type
 */
public interface Controller<E extends AbstractDTO> {

	/**
	 * Creates the.
	 *
	 * @param input
	 *            the input
	 * @param bindingResult
	 *            the binding result
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws SystemConfigurationException
	 *             the system configuration exception
	 */
	PulseResponseBody<E> create(E input, BindingResult bindingResult,
			final HttpServletRequest request)
			throws DuplicateRecordException, InvalidInputException,
			RecordNotFoundException, SystemConfigurationException;

	/**
	 * Update.
	 *
	 * @param input
	 *            the input
	 * @param bindingResult
	 *            the binding result
	 * @param request
	 *            the request
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 */
	PulseResponseBody<E> update(E input, BindingResult bindingResult,
			final HttpServletRequest request) throws RecordNotFoundException,
			InvalidInputException, DuplicateRecordException;

	/**
	 * Archive delete.
	 *
	 * @param inputId
	 *            the input id
	 * @return the pulse response body
	 * @throws RecordNotFoundException
	 *             the record not found exception
	 * @throws InvalidInputException
	 *             the invalid input exception
	 * @throws DuplicateRecordException
	 *             the duplicate record exception
	 */
	PulseResponseBody<E> archiveDelete(Integer inputId)
			throws RecordNotFoundException, InvalidInputException,
			DuplicateRecordException;
}
