/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.controller;

import java.lang.reflect.Type;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.job.portal.exception.DuplicateRecordException;
import com.job.portal.exception.RecordNotFoundException;
import com.job.portal.exception.SystemConfigurationException;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.PulseResponseBody;
import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Address;
import com.job.portal.pojo.customer.City;
import com.job.portal.pojo.customer.Country;
import com.job.portal.pojo.customer.State;
import com.job.portal.pojo.customer.dto.AddressDTO;
import com.job.portal.pojo.customer.mapper.IAddressMapper;
import com.job.portal.pojo.enums.Status;
import com.job.portal.service.CityService;
import com.job.portal.service.CountryService;
import com.job.portal.service.JudService;
import com.job.portal.service.StateService;
import com.job.portal.utils.Constants;

// TODO: Auto-generated Javadoc
/**
 * The Class AddressController.
 *
 * @author Vaibhav
 */
@RestController
@RequestMapping("/api/address")
public class AddressController extends AbstractController<Address, AddressDTO> {

	/** The address mapper. */
	@Autowired
	private IAddressMapper addressMapper;

	/** The country service. */
	@Autowired
	private CountryService countryService;

	/** The city service. */
	@Autowired
	private CityService cityService;

	/** The state service. */
	@Autowired
	private StateService stateService;

	/**
	 * Gets the address components.
	 *
	 * @param request the request
	 * @return the address components
	 */
	@RequestMapping(value = "/get-address-component", method = RequestMethod.POST)
	@ResponseBody
	public PulseResponseBody<Address> getAddressComponents(
			@RequestBody final Map<String, Object> request) {
		return new PulseResponseBody<>(getAddressComponents1(request),
				Constants.SUCCESS);
		// return null;
	}

	/* (non-Javadoc)
	 * @see com.job.portal.controller.AbstractController#getCoreName()
	 */
	@Override
	public String getCoreName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.job.portal.controller.AbstractController#getService()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public JudService getService() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.job.portal.controller.AbstractController#strip(com.job.portal.pojo.common.AbstractValueObject)
	 */
	@Override
	public AbstractValueObject strip(final AbstractValueObject input) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.job.portal.controller.AbstractController#getListType()
	 */
	@Override
	protected Type getListType() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.job.portal.controller.AbstractController#getMapper()
	 */
	@Override
	public IAbstractMapper<Address, AddressDTO> getMapper() {
		return this.addressMapper;
	}

	/**
	 * Gets the address components 1.
	 *
	 * @param request the request
	 * @return the address components 1
	 */
	public Address getAddressComponents1(final Map<String, Object> request) {
		try {
			final Object addressString = request.get("address");
			final Object lat = request.get("lat");
			final Object lng = request.get("lng");
			GeocodingApiRequest geocodingApirequest = null;
			if (addressString != null) {
				geocodingApirequest = GeocodingApi.geocode(getGoogleContext(),
						String.valueOf(addressString));
			} else if (lat != null && lng != null) {
				geocodingApirequest = GeocodingApi.reverseGeocode(
						getGoogleContext(),
						new LatLng(Double.valueOf(lat.toString()),
								Double.valueOf(lng.toString())));
			} else {
				return null;
			}
			final GeocodingResult[] results = geocodingApirequest.await();
			return extractAddress(results,
					addressString != null ? String.valueOf(addressString)
							: " ");
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the google context.
	 *
	 * @return the google context
	 */
	public GeoApiContext getGoogleContext() {
		return new GeoApiContext().setApiKey(getSecret());
	}

	/**
	 * Gets the secret.
	 *
	 * @return the secret
	 */
	public String getSecret() {
		return "AIzaSyCCUEX1CU9iFGsoSlGfTOqq8Q9K_J4BBpI";
	}

	/**
	 * Extract address.
	 *
	 * @param results the results
	 * @param addressStringAsInput the address string as input
	 * @return the address
	 */
	private Address extractAddress(final GeocodingResult[] results,
			final String addressStringAsInput) {
		try {
			final Address add = new Address();
			final GeocodingResult firstResult = results[0];
			add.setLat(String.valueOf(firstResult.geometry.location.lat));
			add.setLng(String.valueOf(firstResult.geometry.location.lng));
			String countryName = null;
			String stateName = null;
			String cityName = null;
			for (final AddressComponent addComponent : firstResult.addressComponents) {
				if (addComponent.types[0] == AddressComponentType.POSTAL_CODE) {
					add.setPincode(addComponent.longName);
				} else if (addComponent.types[0] == AddressComponentType.COUNTRY) {
					countryName = addComponent.longName;
				} else if (addComponent.types[0] == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1) {
					stateName = addComponent.longName;
				} else if (addComponent.types[0] == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2) {
					add.setDistrict(addComponent.longName);
				} else if (addComponent.types[0] == AddressComponentType.LOCALITY) {
					cityName = addComponent.longName;
				}
			}
			// set the name of district if city is null
			if (cityName == null) {
				cityName = add.getDistrict();
			}
			// if the district is null too then set state name e.x. Delhi
			if (cityName == null && stateName != null) {
				cityName = stateName;
			}
			if (cityName != null && countryName != null && stateName != null) {
				final City city = getFromState(cityName, stateName,
						countryName);
				add.setCity(city);
			}

			add.setLandmark(addressStringAsInput != null
					&& !addressStringAsInput.trim().equals(' ')
							? addressStringAsInput
							: firstResult.formattedAddress);
			add.setGooglePlaceId(firstResult.placeId);
			return add;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the from state.
	 *
	 * @param cityName the city name
	 * @param stateName            the state name
	 * @param countryName the country name
	 * @return the from state
	 * @throws DuplicateRecordException the duplicate record exception
	 * @throws RecordNotFoundException the record not found exception
	 * @throws SystemConfigurationException the system configuration exception
	 */
	public City getFromState(final String cityName, final String stateName,
			final String countryName) throws DuplicateRecordException,
			RecordNotFoundException, SystemConfigurationException {
		final City city = this.cityService.getCityByStateAndCountry(cityName,
				stateName, countryName);
		if (city == null) {
			final City newCity = new City();
			final State state = this.stateService
					.getStateNameByCountry(stateName, countryName);
			if (state == null) {
				final State newState = new State();
				final Country country = this.countryService
						.findByProperty("name", countryName);

				if (country == null) {
					final Country newCountry = new Country();
					newCountry.setName(countryName);
					newCountry.setActiveStatus(Status.Active);
					final Country countryNew = this.countryService
							.create(newCountry);
					newState.setCountry(countryNew);
					newState.setName(stateName);
					final State stateNew = this.stateService.create(newState);
					newCity.setState(stateNew);
				} else {
					newState.setName(stateName);
					newState.setCountry(country);
					final State newCrState = this.stateService.create(newState);
					newCity.setState(newCrState);
				}
			} else {
				newCity.setState(state);
			}
			newCity.setName(cityName);
			return this.cityService.create(newCity);
		}
		return city;
	}

}
