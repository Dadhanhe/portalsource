/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.interceptor;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.job.portal.utils.Objects;
import com.job.portal.utils.UserSession;

// TODO: Auto-generated Javadoc
/**
 * The Class AllRouteInterceptor.
 */
@Component
public class AllRouteInterceptor implements HandlerInterceptor {

	/** The Constant logger. */
	private static final Logger logger = Logger
			.getLogger(AllRouteInterceptor.class);

	/**
	 * The session expiration time. What is the time duration, we should expire
	 * the user session
	 */
	private int sessionExpirationTimeMin = Integer.MAX_VALUE;

	/** The is session comparision enabled. */
	private boolean isSessionComparisionEnabled = false;

	/**
	 * Gets the the session time for timeouts and store in session.
	 *
	 * @return the the session time
	 */
	@PostConstruct
	public void getTheSessionTime() {

	}

	/**
	 * Pre-fetch all the session in memory so that we don't need to go to DB
	 * every time to check session.
	 *
	 * @return the all sessions
	 */
	@PostConstruct
	public void getAllSessions() {
		if (isDbSessionStorageAvailable()) {
			// this.userSessionHelper.loadAllActive(this.sessionExpirationTimeMin);
		}
	}

	/**
	 * Gets the if session comparison enabled.
	 *
	 * @return the if session comparison enabled
	 */
	@PostConstruct
	public void getIfSessionComparisionEnabled() {

	}

	/**
	 * Checks if is db session storage available.
	 *
	 * @return the boolean
	 */
	private Boolean isDbSessionStorageAvailable() {

		return false;
	}

	/**
	 * Any request will first come here as we have registered the component in
	 * the route intercepter.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param handler
	 *            the handler
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public boolean preHandle(final HttpServletRequest request,
			final HttpServletResponse response, final Object handler)
			throws Exception {
		boolean returnValue = true;
		logger.debug("Service URL :: " + request.getRequestURI());
		if (request.getRequestURI().contains("secure")) {
			if (Objects.isEmpty(request.getHeader("Authorization"))) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				returnValue = false;
			}
			if (Objects.isEmpty(request.getHeader("IndustryId"))) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				returnValue = false;
			}

			final boolean isRequestFromMobile = !Objects
					.isEmpty(request.getHeader("ismobile"));
			if (!this.isSessionComparisionEnabled || isRequestFromMobile) {
				// we need to bypass the check session if mobile version is not
				// available so mobile app does not support session expiration
				returnValue = true;
			} else if (this.isSessionComparisionEnabled) {
				// returnValue = checkSession(response);
				boolean isUserIdle = false;
				if (!Objects.isEmpty(request.getHeader("isUserIdle"))) {
					isUserIdle = Boolean
							.parseBoolean(request.getHeader("isUserIdle"));
				}
				returnValue = checkSession(response, isUserIdle);
			}
		}
		return returnValue;
	}

	/**
	 * Check session.
	 *
	 * @param response
	 *            the response
	 * @param isUserIdle
	 *            the is user idle
	 * @return true, if successful
	 */
	public boolean checkSession(final HttpServletResponse response,
			final boolean isUserIdle) {
		final Integer userId = Integer
				.parseInt(String.valueOf(SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()));
		return isDbSessionStorageAvailable()
				? checkSessionDB(userId, response, isUserIdle)
				: checkSessionInMemory(userId, response, isUserIdle);
	}

	/**
	 * Check session DB.
	 *
	 * @param userId
	 *            the user id
	 * @param response
	 *            the response
	 * @param isUserIdle
	 *            the is user idle
	 * @return true, if successful
	 */
	public boolean checkSessionDB(final Integer userId,
			final HttpServletResponse response, final boolean isUserIdle) {
		final Long start = System.currentTimeMillis();
		// final boolean userSessionExist = this.userSessionHelper
		// .isActiveSessionExist(userId, this.sessionExpirationTimeMin);
		final boolean userSessionExist = false;
		if (!isUserIdle || userSessionExist) {
			// this.userSessionHelper.extendUserSession(userId);
			final Long end = System.currentTimeMillis();
			if (logger.isDebugEnabled()) {
				logger.debug("Checking session took " + (end - start) + " ms");
			}
			return true;
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return false;
		}
	}

	/**
	 * Check session.
	 *
	 * @param userId
	 *            the user id
	 * @param response
	 *            the response
	 * @param isUserIdle
	 *            the is user idle
	 * @return true, if successful
	 */
	public boolean checkSessionInMemory(final Integer userId,
			final HttpServletResponse response, final boolean isUserIdle) {
		final boolean userSessionExist = UserSession.isExistIdInMap(userId);
		if (userSessionExist) {
			final Date lastSeesion = UserSession.getValuesFromMap(userId);
			final long diff = new Date().getTime() - lastSeesion.getTime();
			// final long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
			if (diff > (this.sessionExpirationTimeMin * 60 * 1000)
					&& isUserIdle) {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				if (UserSession.isExistIdInMap(userId)) {
					UserSession.removeSessionFromMap(userId);
				}
				return false;
			} else {
				UserSession.setValueInMap(userId);
				return true;
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(final HttpServletRequest request,
			final HttpServletResponse response, final Object handler,
			final ModelAndView modelAndView) throws Exception {
		// anything to be handled after the service call finishes to be written
		// here
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(final HttpServletRequest request,
			final HttpServletResponse response, final Object handler,
			final Exception ex) throws Exception {
		// this will be getting called even after exception occurs

	}
}
