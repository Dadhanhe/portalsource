/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.queue;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessQueue.
 */
public abstract class ProcessQueue {

	/** The is finished. */
	private boolean isFinished = false;

	/**
	 * Checks if is finished.
	 *
	 * @return true, if is finished
	 */
	public boolean isFinished() {
		return this.isFinished;
	}

	/**
	 * Sets the finished.
	 *
	 * @param isFinished
	 *            the new finished
	 */
	public void setFinished(final boolean isFinished) {
		this.isFinished = isFinished;
	}

	/**
	 * Run.
	 */
	public abstract void run();
}
