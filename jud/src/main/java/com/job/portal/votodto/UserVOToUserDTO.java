/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.votodto;

import org.modelmapper.PropertyMap;

import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.dto.UserDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class UserVOToUserDTO.
 */
public class UserVOToUserDTO extends PropertyMap<User, UserDTO> {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.modelmapper.PropertyMap#configure()
	 */
	@Override
	protected void configure() {

	}
}
