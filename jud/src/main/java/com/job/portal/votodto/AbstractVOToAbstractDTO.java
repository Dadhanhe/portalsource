/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.votodto;

import org.modelmapper.PropertyMap;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.dto.AbstractDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractVOToAbstractDTO.
 */
public class AbstractVOToAbstractDTO
		extends PropertyMap<AbstractValueObject, AbstractDTO> {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.modelmapper.PropertyMap#configure()
	 */
	@Override
	protected void configure() {
		map().setCreatedById(this.source.getCreatedBy().getId());
		// map().setCreatedByFirstName(this.source.getCreatedBy().getFirstName());
		// map().setCreatedByLastName(this.source.getCreatedBy().getLastName());

		map().setUpdatedById(this.source.getUpdatedBy().getId());
		// map().setUpdatedByFirstName(this.source.getUpdatedBy().getFirstName());
		// map().setUpdatedByLastName(this.source.getUpdatedBy().getLastName());
	}
}
