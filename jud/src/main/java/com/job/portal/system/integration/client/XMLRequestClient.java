/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.system.integration.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

// TODO: Auto-generated Javadoc
/**
 * The Class XMLRequestClient.
 */
/**
 * @author makwanvi
 *
 */
public class XMLRequestClient {

	/** The Constant APPLICATION_XML. */
	private static final String APPLICATION_XML = "application/xml";

	/** The Constant POST. */
	private static final String POST = "POST";

	/** The endpoint url. */
	public final String ENDPOINT_URL = "https://api.intacct.com/ia/xml/xmlgw.phtml";

	/** The timeout. */
	public final int TIMEOUT = 30;

	/** The Constant UTF_8. */
	private static final String UTF_8 = "UTF-8";

	/** The Constant TEXT_XML_CHARSET_UTF_8. */
	private static final String TEXT_XML_CHARSET_UTF_8 = "text/xml;charset=UTF-8";

	/** The Constant CONTENT_TYPE. */
	private static final String CONTENT_TYPE = "content-type";

	/**
	 * Post.
	 *
	 * @param request
	 *            the request
	 * @return the document
	 * @throws Exception
	 *             the exception
	 */
	public Document post(final Document request) throws Exception {
		request.getDocumentElement().normalize();
		Document response = null;

		final String END_POINT = this.ENDPOINT_URL;
		final URL obj = new URL(END_POINT);

		// Set up the connection
		final HttpsURLConnection con = (HttpsURLConnection) obj
				.openConnection();
		con.setRequestMethod(POST);
		con.setRequestProperty(CONTENT_TYPE, APPLICATION_XML);
		con.setDoOutput(true);
		con.setDoInput(true);

		// Set up output stream for the POST content
		final StringWriter writer = new StringWriter();
		final StreamResult requestString = new StreamResult(writer);
		final TransformerFactory tf = TransformerFactory.newInstance();
		final Transformer transformer = tf.newTransformer();
		transformer.transform(new DOMSource(request), requestString);

		// Write to the output stream
		final OutputStreamWriter wr = new OutputStreamWriter(
				con.getOutputStream());
		wr.write(writer.toString());
		wr.flush();
		wr.close();

		// Get the response
		final InputStream rd = con.getInputStream();
		final DocumentBuilderFactory factory = DocumentBuilderFactory
				.newInstance();
		final DocumentBuilder parser = factory.newDocumentBuilder();
		response = parser.parse(rd);
		rd.close();

		// Check the HTTP code is 200-OK
		final int responseCode = con.getResponseCode();
		if (responseCode != 200) {
			throw new Exception("Received HTTP status code, " + responseCode);
		}

		return response;
	}

	/**
	 * Http request post.
	 *
	 * @param endpoint_url
	 *            the endpoint url
	 * @param request
	 *            the request
	 * @return the string
	 * @throws MalformedURLException
	 *             the malformed URL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ProtocolException
	 *             the protocol exception
	 * @throws Exception
	 *             the exception
	 */
	public String httpRequestPost(final String endpoint_url,
			final String request) throws MalformedURLException, IOException,
			ProtocolException, Exception {
		// Make a URL connection to the Adaptive Planning web service URL
		final URL urlRequest = new URL(endpoint_url);
		final HttpURLConnection conn = (HttpURLConnection) urlRequest
				.openConnection();
		// Set the content type, the HTTP method, and tell the connection we
		// expect output
		conn.setRequestProperty(CONTENT_TYPE, TEXT_XML_CHARSET_UTF_8);
		conn.setRequestMethod(POST);
		conn.setDoOutput(true);
		// Send the request
		writeRequest(conn, request);
		// Read the response
		final String response = readResponse(conn);
		return response;
	}

	/**
	 * Write request.
	 *
	 * @param conn
	 *            the conn
	 * @param request
	 *            the request
	 * @throws Exception
	 *             the exception
	 */
	private void writeRequest(final HttpURLConnection conn,
			final String request) throws Exception {
		conn.getOutputStream().write(request.getBytes(UTF_8));
	}

	/**
	 * Read response.
	 *
	 * @param conn
	 *            the conn
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	private String readResponse(final HttpURLConnection conn) throws Exception {
		final byte[] buffer = new byte[4096];
		final StringBuilder sb = new StringBuilder();
		int amt;
		while ((amt = conn.getInputStream().read(buffer)) != -1) {
			final String s = new String(buffer, 0, amt, UTF_8);
			sb.append(s);
		}
		return sb.toString();
	}

}
