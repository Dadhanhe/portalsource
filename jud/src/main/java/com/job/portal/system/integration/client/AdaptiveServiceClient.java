/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.system.integration.client;

/**
 * @author makwanvi
 *
 */
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class AdaptiveServiceClient.
 */
@Component
public class AdaptiveServiceClient {

	/** The Constant PASS_WORD. */
	private static final String PASS_WORD = "password";

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Export adaptive info.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public void exportAdaptiveInfo() throws Exception {
		// -- exportSheets
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportSheets\" callerName=\"test program\" >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\">\n" +
		// "</credentials>\n" +
		// "</call>";
		// -- exportModeledSheet
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportModeledSheet\" callerName=\"test program\" >\n"
		// +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<exportModel sheetToExport=\"Accounts Overview\" />\n" +
		// "</call>";
		// -- exportSheetDefinition
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportSheetDefinition\" callerName=\"test program\"
		// >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<modeled-sheet id=\"302\" />\n" +
		// "</call>";
		// -- exportAccounts
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportAccounts\" callerName=\"test program\" >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<modeled-sheet id=\"302\" />\n" +
		// "</call>";
		// -- exportUsers
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportUsers\" callerName=\"test program\" >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<modeled-sheet id=\"302\" />\n" +
		// "</call>";
		// -- exportLevels
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportLevels\" callerName=\"test program\" >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<modeled-sheet id=\"302\" />\n" +
		// "</call>";
		// -- exportAttributes
		// String request = "<?xml version='1.0' encoding='UTF-8'?>\n" +
		// "<call method=\"exportAttributes\" callerName=\"test program\" >\n" +
		// " <credentials login=\"KTilekar@frontlinepg.com\"
		// password=\"welcome123\" />\n" +
		// "<modeled-sheet id=\"302\" />\n" +
		// "</call>";
		// -- exportDimensions
		final String request = "<?xml version='1.0' encoding='UTF-8'?>\n"
				+ "<call method=\"exportDimensions\"  callerName=\"test program\" >\n"
				+ " <credentials login=\"KTilekar@frontlinepg.com\" "
				+ PASS_WORD + "=\"welcome123\" />\n" + "</call>";
		// Make a URL connection to the Adaptive Planning web service URL
		final URL url = new URL("https://api.adaptiveinsights.com/api/v14");
		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		// Set the content type, the HTTP method, and tell the connection we
		// expect output
		conn.setRequestProperty("content-type", "text/xml;charset=UTF-8");
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);

		// Send the request
		writeRequest(conn, request);

		// Read the response
		final String response = readResponse(conn);

		// Print it out
		this.logger.debug(response);
	}

	/**
	 * Write request.
	 *
	 * @param conn
	 *            the conn
	 * @param request
	 *            the request
	 * @throws Exception
	 *             the exception
	 */
	private void writeRequest(final HttpURLConnection conn,
			final String request) throws Exception {
		conn.getOutputStream().write(request.getBytes("UTF-8"));
	}

	/**
	 * Read response.
	 *
	 * @param conn
	 *            the conn
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	private String readResponse(final HttpURLConnection conn) throws Exception {
		final byte[] buffer = new byte[4096];
		final StringBuilder sb = new StringBuilder();
		int amt;
		while ((amt = conn.getInputStream().read(buffer)) != -1) {
			final String s = new String(buffer, 0, amt, "UTF-8");
			sb.append(s);
		}
		return sb.toString();
	}
}