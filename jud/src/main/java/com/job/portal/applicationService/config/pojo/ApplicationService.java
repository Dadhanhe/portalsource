/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.applicationService.config.pojo;

// TODO: Auto-generated Javadoc
/**
 * The Class ApplicationService.
 *
 * @author vadherak
 */
public enum ApplicationService {

	/** The General. */
	GENERAL,
	/** The Metric import. */
	METRIC_IMPORT,
	/** The scheduler. */
	SCHEDULER

}
