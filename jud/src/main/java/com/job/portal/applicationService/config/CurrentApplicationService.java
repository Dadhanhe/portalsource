/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.applicationService.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

import com.job.portal.applicationService.config.pojo.ApplicationService;

// TODO: Auto-generated Javadoc
/**
 * The Class CurrentApplicationService.
 *
 * @author vadherak
 */
@Configuration
@PropertySources(@PropertySource("classpath:webenv.properties"))
public class CurrentApplicationService {

	/** The env. */
	@Autowired
	private Environment env;

	/** The selected application service. */
	private ApplicationService selectedApplicationService;

	/** The distributed infro enabled. */
	private Boolean distributedInfroEnabled;

	/**
	 * Select application service.
	 *
	 * @return the application service
	 */
	@Bean
	public ApplicationService selectApplicationService() {
		final String applicationServiceString = this.env
				.getProperty("project.service", "GENERAL");
		this.selectedApplicationService = ApplicationService
				.valueOf(applicationServiceString);
		return this.selectedApplicationService;
	}

	/**
	 * Gets the.
	 *
	 * @return the application service
	 */
	public ApplicationService get() {
		return this.selectedApplicationService;
	}

	/**
	 * Distributed deployed.
	 *
	 * @return true, if successful
	 */
	public boolean distributedDeployed() {
		if (this.distributedInfroEnabled == null) {
			this.distributedInfroEnabled = Boolean.valueOf(this.env
					.getProperty("project.distributed.service", "false"));
		}
		return this.distributedInfroEnabled;
	}

	/**
	 * Checks if is scheduler service enabled.
	 *
	 * @return true, if is scheduler service enabled
	 */
	public boolean isSchedulerServiceEnabled() {
		return !distributedDeployed() || (distributedDeployed()
				&& get() == ApplicationService.SCHEDULER);
	}

}
