/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

// TODO: Auto-generated Javadoc
/**
 * The Class ApplicationContextProvider.
 */
public class ApplicationContextProvider implements ApplicationContextAware {

	/** The ctx. */
	private ApplicationContext ctx;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.context.ApplicationContextAware#setApplicationContext
	 * (org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(final ApplicationContext context)
			throws BeansException {
		this.ctx = context;

	}

	/**
	 * Gets the application context.
	 *
	 * @return the application context
	 * @throws BeansException
	 *             the beans exception
	 */
	public ApplicationContext getApplicationContext() throws BeansException {
		return this.ctx;
	}

}
