/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

//import java.util.LinkedList;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.BeanFactoryAware;
//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.config.BeanPostProcessor;
//import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class DestroyPrototypeBeansPostProcessor.
 * 
 * @author vadherak
 */
//@Component
public class DestroyPrototypeBeansPostProcessor
//		implements BeanPostProcessor, BeanFactoryAware, DisposableBean 
		{

//	/** The Constant logger. */
//	private static final Logger logger = LoggerFactory
//			.getLogger(DestroyPrototypeBeansPostProcessor.class);
//
//	/** The bean factory. */
//	private BeanFactory beanFactory;
//
//	/** The prototype beans. */
//	private final List<Object> prototypeBeans = new LinkedList<>();
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.springframework.beans.factory.config.BeanPostProcessor#
//	 * postProcessBeforeInitialization(java.lang.Object, java.lang.String)
//	 */
//	@Override
//	public Object postProcessBeforeInitialization(Object bean, String beanName)
//			throws BeansException {
//		return bean;
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.springframework.beans.factory.config.BeanPostProcessor#
//	 * postProcessAfterInitialization(java.lang.Object, java.lang.String)
//	 */
//	@Override
//	public Object postProcessAfterInitialization(Object bean, String beanName)
//			throws BeansException {
//		if (beanFactory.isPrototype(beanName)) {
//			synchronized (prototypeBeans) {
//				prototypeBeans.add(bean);
//			}
//		}
//		return bean;
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * org.springframework.beans.factory.BeanFactoryAware#setBeanFactory(org.
//	 * springframework.beans.factory.BeanFactory)
//	 */
//	@Override
//	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
//		this.beanFactory = beanFactory;
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.springframework.beans.factory.DisposableBean#destroy()
//	 */
//	@Override
//	public void destroy() throws Exception {
//		synchronized (prototypeBeans) {
//			for (Object bean : prototypeBeans) {
//				if (bean instanceof DisposableBean) {
//					DisposableBean disposable = (DisposableBean) bean;
//					try {
//						disposable.destroy();
//					} catch (Exception e) {
//						logger.error("Beans closing error", e);
//					}
//				}
//			}
//			prototypeBeans.clear();
//		}
//	}
}
