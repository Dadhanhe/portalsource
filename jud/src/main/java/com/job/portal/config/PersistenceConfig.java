/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

// TODO: Auto-generated Javadoc
/**
 * The Class PersistenceConfig.
 */
@Configuration
@EnableTransactionManagement
@PropertySources({ @PropertySource("classpath:webenv.properties"),
		@PropertySource("classpath:common.properties"),
		@PropertySource("classpath:authy_meta.properties") })
public class PersistenceConfig {

	/** The env. */
	@Autowired
	private Environment env;

	/** The application context. */
	@Autowired
	private ApplicationContext applicationContext;

	/** The system application context. */
	public static ApplicationContext SYSTEM_APPLICATION_CONTEXT;

	/**
	 * Session factory.
	 *
	 * @return the local session factory bean
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(restDataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.job.portal.pojo" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		SYSTEM_APPLICATION_CONTEXT = this.applicationContext;
		return sessionFactory;
	}

	/**
	 * Rest data source.
	 *
	 * @return the data source
	 */
	@Bean
	public DataSource restDataSource() {
		final BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(
				this.env.getProperty("db.source.driver.class"));
		dataSource.setUrl(this.env.getProperty("db.source.jdbcURL"));
		dataSource.setUsername(this.env.getProperty("db.source.userName"));
		dataSource.setPassword(this.env.getProperty("db.source.password"));
		return dataSource;
	}

	/**
	 * Transaction manager.
	 *
	 * @param sessionFactory
	 *            the session factory
	 * @return the hibernate transaction manager
	 */
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(
			final SessionFactory sessionFactory) {
		final HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);

		return txManager;
	}

	/**
	 * Exception translation.
	 *
	 * @return the persistence exception translation post processor
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	/**
	 * Hibernate properties.
	 *
	 * @return the properties
	 */
	Properties hibernateProperties() {
		return new Properties() {
			/**
			 *
			 */
			private static final long serialVersionUID = -2418448082526107564L;

			{
				setProperty("hibernate.show_sql", PersistenceConfig.this.env
						.getProperty("hibernate.show_sql"));
				setProperty("hibernate.dialect", PersistenceConfig.this.env
						.getProperty("hibernate.dialect"));
				setProperty("hibernate.connection.CharSet",
						PersistenceConfig.this.env
								.getProperty("hibernate.connection.CharSet"));
				setProperty("hibernate.generate_statistics", "true");
				setProperty("hibernate.connection.characterEncoding",
						PersistenceConfig.this.env.getProperty(
								"hibernate.connection.characterEncoding"));
				setProperty("hibernate.connection.useUnicode",
						PersistenceConfig.this.env.getProperty(
								"hibernate.connection.useUnicode"));
				setProperty("hibernate.id.new_generator_mappings",
						PersistenceConfig.this.env.getProperty(
								"hibernate.id.new_generator_mappings"));
				setProperty("hibernate.dbcp.initialSize",
						PersistenceConfig.this.env
								.getProperty("hibernate.dbcp.initialSize"));
				setProperty("hibernate.dbcp.maxActive",
						PersistenceConfig.this.env
								.getProperty("hibernate.dbcp.maxActive"));
				setProperty("hibernate.dbcp.maxIdle", PersistenceConfig.this.env
						.getProperty("hibernate.dbcp.maxIdle"));
				setProperty("hibernate.dbcp.minIdle", PersistenceConfig.this.env
						.getProperty("hibernate.dbcp.minIdle"));
				// Hibernate Second level Cache
				setProperty("hibernate.cache.use_second_level_cache", "true");
				setProperty("hibernate.cache.use_query_cache", "true");
				setProperty("net.sf.ehcache.configurationResourceName",
						"/myehcache.xml");
				setProperty("hibernate.cache.region.factory_class",
						"org.hibernate.cache.ehcache.EhCacheRegionFactory");
			}
		};
	}

	/**
	 * Gets the message source.
	 *
	 * @return the message source
	 */
	@Bean(name = "messageSource")
	public ResourceBundleMessageSource getMessageSource() {
		final ResourceBundleMessageSource ms = new ResourceBundleMessageSource();
		ms.setBasenames("messages", "validation");
		ms.setDefaultEncoding("UTF-8");
		ms.setCacheSeconds(0);
		ms.setFallbackToSystemLocale(false);
		ms.setUseCodeAsDefaultMessage(true);
		return ms;
	}

}