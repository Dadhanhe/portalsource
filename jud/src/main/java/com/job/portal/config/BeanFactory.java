/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.security.core.context.SecurityContextHolder;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Bean objects.
 */
@Configuration
public class BeanFactory implements BeanFactoryAware {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(BeanFactory.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.beans.factory.BeanFactoryAware#setBeanFactory(org.
	 * springframework.beans.factory.BeanFactory)
	 */
	@Override
	public void setBeanFactory(
			final org.springframework.beans.factory.BeanFactory beanFactory)
			throws BeansException {
		if (beanFactory instanceof ConfigurableBeanFactory) {
			logger.debug("MainConfig is backed by a ConfigurableBeanFactory");
			final ConfigurableBeanFactory cbf = (ConfigurableBeanFactory) beanFactory;

			/*
			 * Notice: org.springframework.beans.factory.config.Scope !=
			 * org.springframework.context.annotation.Scope
			 */
			final org.springframework.beans.factory.config.Scope simpleThreadScope = new SimpleThreadScope();
			cbf.registerScope("simpleThreadScope", simpleThreadScope);

			/*
			 * why the following? Because "Spring Social" gets the HTTP
			 * request's username from
			 * SecurityContextHolder.getContext().getAuthentication() ... and
			 * this by default only has a ThreadLocal strategy... also see
			 * http://stackoverflow.com/a/3468965/923560
			 */
			SecurityContextHolder.setStrategyName(
					SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
		} else {
			logger.debug(
					"MainConfig is not backed by a ConfigurableBeanFactory");
		}
	}
}