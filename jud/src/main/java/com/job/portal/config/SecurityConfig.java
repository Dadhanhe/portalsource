/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * The Class SecurityConfig.
 */
@Configuration
@ImportResource("classpath:webSecurityConfig.xml")
public class SecurityConfig {

}
