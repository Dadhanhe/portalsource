/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.client.SimpleClientHttpRequestFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class IgnoreCertificate.
 */
public class IgnoreCertificate extends SimpleClientHttpRequestFactory {

	/**
	 * Trust all https certificates.
	 *
	 * @throws Exception
	 *             the exception
	 */
	public static void trustAllHttpsCertificates() throws Exception {
		final TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		final TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		final javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext
				.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(hv);

	}

	/** The hv. */
	static HostnameVerifier hv = new HostnameVerifier() {
		@Override
		public boolean verify(final String hostname, final SSLSession session) {
			return true;
		}
	};

	/**
	 * The Class miTM.
	 */
	public static class miTM implements TrustManager, X509TrustManager {

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.net.ssl.X509TrustManager#getAcceptedIssuers()
		 */
		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		/**
		 * Checks if is server trusted.
		 *
		 * @param certs
		 *            the certs
		 * @return true, if is server trusted
		 */
		public boolean isServerTrusted(final X509Certificate[] certs) {
			return true;
		}

		/**
		 * Checks if is client trusted.
		 *
		 * @param certs
		 *            the certs
		 * @return true, if is client trusted
		 */
		public boolean isClientTrusted(final X509Certificate[] certs) {
			return true;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.net.ssl.X509TrustManager#checkServerTrusted(java.security.cert
		 * .X509Certificate[], java.lang.String)
		 */
		@Override
		public void checkServerTrusted(final X509Certificate[] certs,
				final String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.net.ssl.X509TrustManager#checkClientTrusted(java.security.cert
		 * .X509Certificate[], java.lang.String)
		 */
		@Override
		public void checkClientTrusted(final X509Certificate[] certs,
				final String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}
}
