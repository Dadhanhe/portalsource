/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

// TODO: Auto-generated Javadoc
// Created by Sapan on 09/11/16.
/**
 * The Class MiscConfiguration.
 */
@Configuration
public class MiscConfiguration {

	/**
	 * Model mapper.
	 *
	 * @return the model mapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		final ModelMapper mapper = new ModelMapper();

		return mapper;
	}

	/**
	 * Application context provider.
	 *
	 * @return the application context provider
	 */
	@Bean
	public ApplicationContextProvider applicationContextProvider() {
		return new ApplicationContextProvider();
	}

	/**
	 * Task executor.
	 *
	 * @return the thread pool task executor
	 */
	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		final ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(5);
		pool.setMaxPoolSize(30);
		pool.setWaitForTasksToCompleteOnShutdown(true);
		return pool;
	}
}
