/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.config;

import java.util.List;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.job.portal.interceptor.AllRouteInterceptor;
import com.job.portal.utils.ObjectUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class WebConfig.
 */
@Configuration
@ComponentScan("com.job.portal")
@EnableWebMvc
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class WebConfig extends WebMvcConfigurerAdapter {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(WebConfig.class);

	/** The all route interceptor. */
	@Autowired
	private AllRouteInterceptor allRouteInterceptor;

	static {
		try {
			final String templatePath = ResourceUtils
					.getURL("classpath:templates").getPath();
			Velocity.setProperty("runtime.log",
					"/var/lib/tomcat7/logs/velocity.log");
			Velocity.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH,
					templatePath);
			Velocity.setProperty("eventhandler.referenceinsertion.class",
					"org.apache.velocity.app.event.implement.EscapeHtmlReference");
			Velocity.setProperty("eventhandler.escape.html.match", "/.*/");
		} catch (final Exception e) {
			ObjectUtils.logException(logger, e, null);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #addInterceptors(org.springframework.web.servlet.config.annotation.
	 * InterceptorRegistry)
	 */
	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(this.allRouteInterceptor);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #configureContentNegotiation(org.springframework.web.servlet.config.
	 * annotation.ContentNegotiationConfigurer)
	 */
	@Override
	public void configureContentNegotiation(
			final ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false).favorParameter(true)
				.parameterName("mediaType").ignoreAcceptHeader(true)
				.useJaf(false)
				.defaultContentType(MediaType.APPLICATION_JSON_UTF8)
				.mediaType("xml", MediaType.APPLICATION_XML)
				.mediaType("json", MediaType.APPLICATION_JSON_UTF8)
				.mediaType("video", MediaType.ALL);
	}

	/**
	 * Multipart resolver.
	 *
	 * @return the commons multipart resolver
	 */
	@Bean(name = "multipartResolver")
	protected CommonsMultipartResolver multipartResolver() {
		final CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		/* multipartResolver.setMaxUploadSize(MAX_UPLOAD_SIZE); */
		multipartResolver.setDefaultEncoding("UTF-8");
		return multipartResolver;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #configureMessageConverters(java.util.List)
	 */
	@Override
	public void configureMessageConverters(
			final List<HttpMessageConverter<?>> converters) {
		final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		final ObjectMapper objectMapper = new ObjectMapper();
		// Registering Hibernate4Module to support lazy objects
		final Hibernate5Module module = new Hibernate5Module();
		module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
		objectMapper.registerModule(module);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		objectMapper.configure(
				DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		converter.setObjectMapper(objectMapper);
		converters.add(converter);
		super.configureMessageConverters(converters);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #addResourceHandlers(org.springframework.web.servlet.config.annotation.
	 * ResourceHandlerRegistry)
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/doc/**").addResourceLocations("/doc/");
		registry.addResourceHandler("/assets/images/swagger/**")
				.addResourceLocations("/assets/images/swagger/");
		registry.addResourceHandler("/assets/css/swagger/**")
				.addResourceLocations("/assets/css/swagger/");
		registry.addResourceHandler("/assets/fonts/**")
				.addResourceLocations("/assets/fonts/");
	}
}
