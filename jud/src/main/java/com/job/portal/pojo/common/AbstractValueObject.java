/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.common;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import org.apache.solr.client.solrj.beans.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.enums.MethodType;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.JsonDateTimeSerializer;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractValueObject.
 */
/**
 * @author shahhard
 *
 */
public abstract class AbstractValueObject implements ValueObject {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(AbstractValueObject.class);

	/**
	 * The Interface CreateObjectValidationGroup.
	 */
	public interface CreateObjectValidationGroup {
	}

	/**
	 * The Interface UpdateObjectValidationGroup.
	 */
	public interface UpdateObjectValidationGroup {
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Field
	protected Integer id;

	/** The created by. */
	@Field
	protected User createdBy;

	/** The updated by. */
	@Field
	protected User updatedBy;

	/** The active status. */
	@Field
	protected Status activeStatus;

	/** The created on. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	@Field
	protected Date createdOn;

	/** The updated on. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	@Field
	protected Date updatedOn;

	/** The json data. */
	@Field
	protected String jsonData;

	/** The index related only. */
	protected Boolean indexRelatedOnly = false;

	/** The index main only. */
	protected Boolean indexMainOnly = false;

	/** The method type. */
	protected MethodType methodType;

	/** The fire notification. */
	private Boolean fireNotification = false;

	/**
	 * The Interface SaveGroup.
	 */
	public interface SaveGroup {
	};

	/**
	 * The Interface UpdateGroup.
	 */
	public interface UpdateGroup {
	};

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public abstract Integer getId();

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public abstract void setId(final Integer id);

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public User getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	public void setCreatedBy(final User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the updated by
	 */
	public User getUpdatedBy() {
		return this.updatedBy;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updatedBy
	 *            the new updated by
	 */
	public void setUpdatedBy(final User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn
	 *            the new created on
	 */
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the updated on.
	 *
	 * @return the updated on
	 */
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/**
	 * Sets the updated on.
	 *
	 * @param updatedOn
	 *            the new updated on
	 */
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * Gets the active status.
	 *
	 * @return the active status
	 */
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/**
	 * Sets the active status.
	 *
	 * @param status
	 *            the new active status
	 */
	public void setActiveStatus(final Status status) {
		this.activeStatus = status;
	}

	/**
	 * Gets the json data.
	 *
	 * @return the json data
	 */
	public String getJsonData() {
		return this.jsonData;
	}

	/**
	 * Sets the json data.
	 *
	 * @param jsonData
	 *            the new json data
	 */
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

	/**
	 * Gets the json map.
	 *
	 * @return the json map
	 */
	@Transient
	public Map<String, Object> getJsonMap() {
		final ObjectMapper mapper = new ObjectMapper();
		final String json = this.jsonData;
		Map<String, Object> map = null;
		if (!Objects.isEmpty(json)) {
			map = new HashMap<String, Object>();
			// convert JSON string to Map
			try {
				map = mapper.readValue(json,
						new TypeReference<Map<String, String>>() {
						});
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				ObjectUtils.logException(logger, e, null);
			}
		}
		return map;
	}

	/**
	 * Gets the index related only.
	 *
	 * @return the index related only
	 */
	@Transient
	public Boolean getIndexRelatedOnly() {
		return this.indexRelatedOnly;
	}

	/**
	 * Sets the index related only.
	 *
	 * @param indexRelatedOnly
	 *            the new index related only
	 */
	public void setIndexRelatedOnly(final Boolean indexRelatedOnly) {
		this.indexRelatedOnly = indexRelatedOnly;
	}

	/**
	 * Gets the index main only.
	 *
	 * @return the index main only
	 */
	public Boolean getIndexMainOnly() {
		return this.indexMainOnly;
	}

	/**
	 * Sets the index main only.
	 *
	 * @param indexMainOnly
	 *            the new index main only
	 */
	public void setIndexMainOnly(final Boolean indexMainOnly) {
		this.indexMainOnly = indexMainOnly;
	}

	/**
	 * Gets the method type.
	 *
	 * @return the method type
	 */
	public MethodType getMethodType() {
		return this.methodType;
	}

	/**
	 * Sets the method type.
	 *
	 * @param methodType
	 *            the new method type
	 */
	public void setMethodType(final MethodType methodType) {
		this.methodType = methodType;
	}

	/**
	 * Gets the fire notification.
	 *
	 * @return the fire notification
	 */
	@Transient
	public Boolean getFireNotification() {
		return this.fireNotification;
	}

	/**
	 * Sets the fire notification.
	 *
	 * @param fireNotification
	 *            the new fire notification
	 */
	public void setFireNotification(final Boolean fireNotification) {
		this.fireNotification = fireNotification;
	}

}
