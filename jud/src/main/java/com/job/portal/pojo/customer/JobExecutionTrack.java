/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class JobExecutionTrack.
 */
@Entity
@Table(name = "job_execution_track")
public class JobExecutionTrack extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4636485389118586461L;

	/** The name. */
	private String name;

	/** The processed entity ids. */
	private String processedEntityIds;

	/** The tenant location id. */
	private Integer tenantLocationId;

	/** The is error. */
	private boolean isError;

	/** The error message. */
	private String errorMessage;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(name = "NAME", nullable = false)
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the processed entity ids.
	 *
	 * @return the processed entity ids
	 */
	@Column(name = "PROCESSED_ENTITY_IDS")
	public String getProcessedEntityIds() {
		return this.processedEntityIds;
	}

	/**
	 * Sets the processed entity ids.
	 *
	 * @param processedEntityIds
	 *            the new processed entity ids
	 */
	public void setProcessedEntityIds(final String processedEntityIds) {
		this.processedEntityIds = processedEntityIds;
	}

	/**
	 * Gets the tenant location id.
	 *
	 * @return the tenant location id
	 */
	@Column(name = "TENANT_LOCATION_ID")
	public Integer getTenantLocationId() {
		return this.tenantLocationId;
	}

	/**
	 * Sets the tenant location id.
	 *
	 * @param tenantLocationId
	 *            the new tenant location id
	 */
	public void setTenantLocationId(final Integer tenantLocationId) {
		this.tenantLocationId = tenantLocationId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedOn()
	 */
	@Override
	@Column(name = "EXECUTION_START")
	public Date getCreatedOn() {
		// TODO Auto-generated method stub
		return super.getCreatedOn();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		// TODO Auto-generated method stub
		super.setCreatedOn(createdOn);
	}

	/**
	 * Checks if is error.
	 *
	 * @return true, if is error
	 */
	@Column(name = "IS_ERROR")
	public boolean isError() {
		return this.isError;
	}

	/**
	 * Sets the error.
	 *
	 * @param isError
	 *            the new error
	 */
	public void setError(final boolean isError) {
		this.isError = isError;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	@Column(name = "ERROR_MESSAGE")
	public String getErrorMessage() {
		return this.errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage
	 *            the new error message
	 */
	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedOn()
	 */
	@Override
	@Column(name = "EXECUTION_END")
	public Date getUpdatedOn() {
		// TODO Auto-generated method stub
		return super.getUpdatedOn();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		// TODO Auto-generated method stub
		super.setUpdatedOn(updatedOn);
	}

}
