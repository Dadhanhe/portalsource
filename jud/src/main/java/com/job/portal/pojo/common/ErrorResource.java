/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ErrorResource.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResource {

	/** The code. */
	private String code;

	/** The message. */
	private String message;

	/** The field errors. */
	private List<FieldErrorResource> fieldErrors;

	/**
	 * Instantiates a new error resource.
	 */
	public ErrorResource() {
	}

	/**
	 * Instantiates a new error resource.
	 *
	 * @param code
	 *            the code
	 * @param message
	 *            the message
	 */
	public ErrorResource(String code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the field errors.
	 *
	 * @return the field errors
	 */
	public List<FieldErrorResource> getFieldErrors() {
		return fieldErrors;
	}

	/**
	 * Sets the field errors.
	 *
	 * @param fieldErrors
	 *            the new field errors
	 */
	public void setFieldErrors(List<FieldErrorResource> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}
