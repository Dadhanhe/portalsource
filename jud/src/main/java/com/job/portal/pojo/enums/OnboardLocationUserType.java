/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum OnboardLocationUserType.
 */
public enum OnboardLocationUserType {

	/** The Champion. */
	Champion(0),

	/** The Asignee. */
	Asignee(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new onboard location user type.
	 *
	 * @param value
	 *            the value
	 */
	private OnboardLocationUserType(final int value) {
		this.value = value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the status
	 */
	public static OnboardLocationUserType fromInteger(final Integer value) {
		if (value == Champion.value) {
			return Champion;
		} else if (value == Asignee.value) {
			return Asignee;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Champion.value) {
			return "Champion";
		} else if (value == Asignee.value) {
			return "Asignee";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param onboardLocationUserType
	 *            the onboard location user type
	 * @return the enum
	 */
	public static OnboardLocationUserType getEnum(
			final String onboardLocationUserType) {
		if (onboardLocationUserType != null) {
			if (onboardLocationUserType.equals("Champion")) {
				return OnboardLocationUserType.Champion;
			} else if (onboardLocationUserType.equals("Asignee")) {
				return OnboardLocationUserType.Asignee;
			}
		}
		return null;
	}

}
