/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum TimerTaskReportType.
 */
public enum TimerTaskReportType {

	/** The Audit trail report. */
	Audit_trail_report(0),

	/** The Financial report. */
	Financial_report(1),

	/** The Location report. */
	Location_report(2),

	/** The Agent performance report. */
	Agent_Performance_report(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the timer task report type
	 */
	public static TimerTaskReportType fromInteger(Integer value) {
		if (value == Audit_trail_report.value) {
			return Audit_trail_report;
		} else if (value == Financial_report.value) {
			return Financial_report;
		} else if (value == Location_report.value) {
			return Location_report;
		} else if (value == Agent_Performance_report.value) {
			return Agent_Performance_report;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new timer task report type.
	 *
	 * @param value
	 *            the value
	 */
	private TimerTaskReportType(int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return value;
	}
}
