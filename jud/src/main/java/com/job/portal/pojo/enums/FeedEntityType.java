/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum FeedEntityType.
 */
public enum FeedEntityType implements Serializable {

	/** The Tenant. */
	Tenant(0),

	/** The Tenant location. */
	TenantLocation(1),

	/** The Location group. */
	LocationGroup(2),

	/** The User. */
	User(3),

	/** The Product. */
	Product(4),

	/** The Region. */
	Region(5);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new feed entity type.
	 *
	 * @param value
	 *            the value
	 */
	private FeedEntityType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the feed entity type
	 */
	public static FeedEntityType fromInteger(final Integer value) {
		if (value == Tenant.value) {
			return Tenant;
		} else if (value == TenantLocation.value) {
			return TenantLocation;
		} else if (value == LocationGroup.value) {
			return LocationGroup;
		} else if (value == Product.value) {
			return Product;
		} else if (value == User.value) {
			return User;
		} else if (value == Region.value) {
			return Region;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Tenant.value) {
			return "Tenant";
		} else if (value == TenantLocation.value) {
			return "Tenant Location";
		} else if (value == LocationGroup.value) {
			return "Location Group";
		} else if (value == Product.value) {
			return "Product";
		} else if (value == User.value) {
			return "User";
		} else if (value == Region.value) {
			return "Region";
		}
		return null;
	}

}
