/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.common.dto;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.pojo.common.ValueObject;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.JsonDateTimeSerializer;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;
import com.job.portal.utils.PulseDateUtils;
import com.job.portal.utils.SystemConfigs;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractDTO implements ValueObject {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(AbstractDTO.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -843464619687954048L;

	/**
	 * The Interface CreateObjectValidationGroup.
	 */
	public interface CreateObjectValidationGroup {
	}

	/**
	 * The Interface UpdateObjectValidationGroup.
	 */
	public interface UpdateObjectValidationGroup {
	}

	/** The map to. */
	@NotNull
	@NotBlank
	private String mapTo;

	/** The id. */
	@Field
	protected Integer id;

	/** The active status. */
	@Field
	protected String activeStatus;

	/** The reviewer by id. */
	@Field
	protected Integer reviewerById;

	/** The r by identity. */
	@Field
	protected String rByIdentity;

	/** The reviewed on. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	protected Date reviewedOn;

	/** The created by id. */
	@Field
	protected Integer createdById;

	/** The updated by id. */
	@Field
	protected Integer updatedById;

	/** The c by identity. */
	@Field
	protected String cByIdentity;

	/** The u by identity. */
	@Field
	protected String uByIdentity;

	/** The created on. */
	@Field
	protected Date createdOn;

	/** The updated on. */
	@Field
	protected Date updatedOn;

	/** The json data. */
	@Field
	protected String jsonData;

	/** The json map. */
	protected transient Map<String, Object> jsonMap;

	/** The created by id. */
	@Field
	protected String createdByFirstName;

	/** The created by id. */
	@Field
	protected String createdByLastName;

	@Field
	protected String updatedByFirstName;

	/** The created by id. */
	@Field
	protected String updatedByLastName;

	/**
	 * Gets the map to.
	 *
	 * @return the map to
	 */
	public String getMapTo() {
		return this.mapTo;
	}

	/**
	 * Sets the map to.
	 *
	 * @param mapTo
	 *            the new map to
	 */
	public void setMapTo(final String mapTo) {
		this.mapTo = mapTo;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}

	/**
	 * Gets the created by id.
	 *
	 * @return the created by id
	 */
	public Integer getCreatedById() {
		return this.createdById;
	}

	/**
	 * Sets the created by id.
	 *
	 * @param createdById
	 *            the new created by id
	 */
	public void setCreatedById(final Integer createdById) {
		this.createdById = createdById;
	}

	/**
	 * Gets the updated by id.
	 *
	 * @return the updated by id
	 */
	public Integer getUpdatedById() {
		return this.updatedById;
	}

	/**
	 * Sets the updated by id.
	 *
	 * @param updatedById
	 *            the new updated by id
	 */
	public void setUpdatedById(final Integer updatedById) {
		this.updatedById = updatedById;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn
	 *            the new created on
	 */
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the updated on.
	 *
	 * @return the updated on
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/**
	 * Sets the updated on.
	 *
	 * @param updatedOn
	 *            the new updated on
	 */
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * Gets the c by identity.
	 *
	 * @return the c by identity
	 */
	public String getcByIdentity() {
		// if (Objects.isEmpty(this.cByIdentity)) {
		// String name = "";
		// if (!Objects.isEmpty(this.createdByFirstName)) {
		// name += this.createdByFirstName + " ";
		// }
		// if (!Objects.isEmpty(this.createdByLastName)) {
		// name += this.createdByLastName;
		// }
		// this.cByIdentity = name;
		// }
		return this.cByIdentity;
	}

	/**
	 * Sets the c by identity.
	 *
	 * @param cByIdentity
	 *            the new c by identity
	 */
	public void setcByIdentity(final String cByIdentity) {
		this.cByIdentity = cByIdentity;
	}

	/**
	 * Gets the u by identity.
	 *
	 * @return the u by identity
	 */
	public String getuByIdentity() {
		// if (Objects.isEmpty(this.uByIdentity)) {
		// String name = "";
		// if (!Objects.isEmpty(this.updatedByFirstName)) {
		// name += this.updatedByFirstName + " ";
		// }
		// if (!Objects.isEmpty(this.updatedByLastName)) {
		// name += this.updatedByLastName;
		// }
		// this.uByIdentity = name;
		// }
		return this.uByIdentity;
	}

	/**
	 * Sets the u by identity.
	 *
	 * @param uByIdentity
	 *            the new u by identity
	 */
	public void setuByIdentity(final String uByIdentity) {
		this.uByIdentity = uByIdentity;
	}

	/**
	 * Gets the active status.
	 *
	 * @return the active status
	 */
	public String getActiveStatus() {
		return this.activeStatus;
	}

	/**
	 * Sets the active status.
	 *
	 * @param status
	 *            the new active status
	 */
	public void setActiveStatus(final String status) {
		this.activeStatus = status;
	}

	/**
	 * Gets the json map.
	 *
	 * @return the json map
	 */
	public Map<String, Object> getJsonMap() {
		if (this.jsonMap == null && this.jsonData != null) {
			final ObjectMapper mapper = new ObjectMapper();
			final String json = this.jsonData;
			Map<String, Object> map = null;
			if (!Objects.isEmpty(json)) {
				map = new HashMap<String, Object>();
				// convert JSON string to Map
				try {
					map = mapper.readValue(json,
							new TypeReference<Map<String, String>>() {
							});
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					ObjectUtils.logException(logger, e, null);
				}
			}
			return map;
		}
		return this.jsonMap;
	}

	/**
	 * Sets the json map.
	 *
	 * @param jsonMap
	 *            the json map
	 */
	public void setJsonMap(final Map<String, Object> jsonMap) {
		this.jsonMap = jsonMap;
	}

	/**
	 * Gets the json data.
	 *
	 * @return the json data
	 */
	public String getJsonData() {
		return this.jsonData;
	}

	/**
	 * Sets the json data.
	 *
	 * @param jsonData
	 *            the new json data
	 */
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

	/**
	 * Gets the cname time.
	 *
	 * @return the cname time
	 */
	public String getCnameTime() {
		try {
			if (getCreatedOn() != null && getcByIdentity() != null) {
				return getcByIdentity() + " " + PulseDateUtils.format(
						getCreatedOn(), SystemConfigs.DATE_TIME_12HR_FORMAT);
			} else if (getCreatedOn() != null && getcByIdentity() == null) {
				return PulseDateUtils.format(getCreatedOn(),
						SystemConfigs.DATE_TIME_12HR_FORMAT);
			} else if (getCreatedOn() == null && getcByIdentity() != null) {
				return getcByIdentity();
			} else {
				return "-";
			}
		} catch (final Exception e) {
		}
		return "-";
	}

	/**
	 * Gets the uname time.
	 *
	 * @return the uname time
	 */
	public String getUnameTime() {
		try {
			if (getUpdatedOn() != null && getuByIdentity() != null) {
				return getuByIdentity() + " " + PulseDateUtils.format(
						getUpdatedOn(), SystemConfigs.DATE_TIME_12HR_FORMAT);
			} else if (getUpdatedOn() != null && getuByIdentity() == null) {
				return PulseDateUtils.format(getUpdatedOn(),
						SystemConfigs.DATE_TIME_12HR_FORMAT);
			} else if (getUpdatedOn() == null && getuByIdentity() != null) {
				return getuByIdentity();
			} else {
				return "-";
			}
		} catch (final Exception e) {
		}
		return "-";
	}

	/**
	 * Gets the created by with date time.
	 *
	 * @return the created by with date time
	 */
	public String getCreatedByWithDateTime() {
		try {
			if (getCreatedOn() != null && getcByIdentity() != null) {
				return getcByIdentity() + "  " + PulseDateUtils.format(
						getCreatedOn(), SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getCreatedOn() != null && getcByIdentity() == null) {
				return " - " + PulseDateUtils.format(getCreatedOn(),
						SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getCreatedOn() == null && getcByIdentity() != null) {
				return getcByIdentity() + " - ";
			} else {
				return " - ";
			}
		} catch (final Exception e) {
		}
		return " - ";
	}

	/**
	 * Gets the updated by with date time.
	 *
	 * @return the updated by with date time
	 */
	public String getUpdatedByWithDateTime() {
		try {
			if (getUpdatedOn() != null && getuByIdentity() != null) {
				return getuByIdentity() + "  " + PulseDateUtils.format(
						getUpdatedOn(), SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getUpdatedOn() != null && getuByIdentity() == null) {
				return " - " + PulseDateUtils.format(getUpdatedOn(),
						SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getUpdatedOn() == null && getuByIdentity() != null) {
				return getuByIdentity() + " - ";
			} else {
				return " - ";
			}
		} catch (final Exception e) {
		}
		return " - ";
	}

	/**
	 * Gets the reviewer by id.
	 *
	 * @return the reviewer by id
	 */
	public Integer getReviewerById() {
		return this.reviewerById;
	}

	/**
	 * Sets the reviewer by id.
	 *
	 * @param reviewerById
	 *            the new reviewer by id
	 */
	public void setReviewerById(final Integer reviewerById) {
		this.reviewerById = reviewerById;
	}

	/**
	 * Gets the r by identity.
	 *
	 * @return the r by identity
	 */
	public String getrByIdentity() {
		// if (Objects.isEmpty(this.rByIdentity)) {
		// String name = "";
		// if (!Objects.isEmpty(this.reviewerByFirstName)) {
		// name += this.reviewerByFirstName + " ";
		// }
		// if (!Objects.isEmpty(this.reviewerByLastName)) {
		// name += this.reviewerByLastName;
		// }
		// this.rByIdentity = name;
		// }
		return this.rByIdentity;
	}

	/**
	 * Sets the r by identity.
	 *
	 * @param rByIdentity
	 *            the new r by identity
	 */
	public void setrByIdentity(final String rByIdentity) {
		this.rByIdentity = rByIdentity;
	}

	/**
	 * Gets the reviewed on.
	 *
	 * @return the reviewed on
	 */
	public Date getReviewedOn() {
		return this.reviewedOn;
	}

	/**
	 * Sets the reviewed on.
	 *
	 * @param reviewedOn
	 *            the new reviewed on
	 */
	public void setReviewedOn(final Date reviewedOn) {
		this.reviewedOn = reviewedOn;
	}

	/**
	 * Gets the reviewed by with date time.
	 *
	 * @return the reviewed by with date time
	 */
	public String getReviewedByWithDateTime() {
		try {
			if (getReviewedOn() != null && getrByIdentity() != null) {
				return getrByIdentity() + "  " + PulseDateUtils.format(
						getReviewedOn(), SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getReviewedOn() != null && getrByIdentity() == null) {
				return " - " + PulseDateUtils.format(getReviewedOn(),
						SystemConfigs.DATE_TIME_12HR_FORMAT_2);
			} else if (getReviewedOn() == null && getrByIdentity() != null) {
				return getrByIdentity() + " - ";
			} else {
				return " - ";
			}
		} catch (final Exception e) {
		}
		return " - ";
	}

	public String getCreatedByFirstName() {
		return this.createdByFirstName;
	}

	public void setCreatedByFirstName(final String createdByFirstName) {
		this.createdByFirstName = createdByFirstName;
	}

	public String getCreatedByLastName() {
		return this.createdByLastName;
	}

	public void setCreatedByLastName(final String createdByLastName) {
		this.createdByLastName = createdByLastName;
	}

	public String getUpdatedByFirstName() {
		return this.updatedByFirstName;
	}

	public void setUpdatedByFirstName(final String updatedByFirstName) {
		this.updatedByFirstName = updatedByFirstName;
	}

	public String getUpdatedByLastName() {
		return this.updatedByLastName;
	}

	public void setUpdatedByLastName(final String updatedByLastName) {
		this.updatedByLastName = updatedByLastName;
	}

}
