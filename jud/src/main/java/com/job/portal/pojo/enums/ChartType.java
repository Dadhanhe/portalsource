/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// TODO: Auto-generated Javadoc
/**
 * The Enum ChartType.
 */
public enum ChartType {

	/** The Observation by location. */
	ObservationByLocation("ObservationByLocation"),

	/** The Observation average scores monthly by location. */
	ObservationAverageScoresMonthlyByLocation(
			"ObservationAverageScoresMonthlyByLocation"),

	/** The Observation average scores monthly sub data by location. */
	ObservationAverageScoresMonthlySubDataByLocation(
			"ObservationAverageScoresMonthlySubDataByLocation"),

	/** The Counter coaching average scores monthly by location. */
	CounterCoachingAverageScoresMonthlyByLocation(
			"CounterCoachingAverageScoresMonthlyByLocation"),

	/** The Counter coaching average scores monthly sub data by location. */
	CounterCoachingAverageScoresMonthlySubDataByLocation(
			"CounterCoachingAverageScoresMonthlySubDataByLocation"),

	/** The Observation types by location. */
	ObservationTypesByLocation("ObservationTypesByLocation"),

	/** The Counter coaching types by location. */
	CounterCoachingTypesByLocation("CounterCoachingTypesByLocation"),

	/** The One to one category sessions by location. */
	OneToOneCategorySessionsByLocation("OneToOneCategorySessionsByLocation"),

	/** The Counter coaching by location. */
	CounterCoachingByLocation("CounterCoachingByLocation"),

	/** The One to one by location. */
	OneToOneByLocation("OneToOneByLocation"),

	/** The Most improved location. */
	MostImprovedLocation("MostImprovedLocation"),

	/** The Observation user monthly IR. */
	ObservationUserMonthlyIR("ObservationUserMonthlyIR"),

	/** The Counter coaching user monthly IR. */
	CounterCoachingUserMonthlyIR("CounterCoachingUserMonthlyIR"),

	/** The One to one user monthly IR. */
	OneToOneUserMonthlyIR("OneToOneUserMonthlyIR"),

	/** The User monthly IR. */
	UserMonthlyIR("UserMonthlyIR"),

	/** The Observation organization monthly IR. */
	ObservationOrganizationMonthlyIR("ObservationOrganizationMonthlyIR"),

	/** The Counter coaching organization monthly IR. */
	CounterCoachingOrganizationMonthlyIR(
			"CounterCoachingOrganizationMonthlyIR"),

	/** The One to one organization monthly IR. */
	OneToOneOrganizationMonthlyIR("OneToOneOrganizationMonthlyIR"),

	/** The Organization monthly IR. */
	OrganizationMonthlyIR("OrganizationMonthlyIR"),

	/*
	 * Drildown data of above charts
	 */
	/** The User monthly IR sub data. */
	UserMonthlyIRSubData("UserMonthlyIRSubData"),

	/** The Organization monthly IR sub data. */
	OrganizationMonthlyIRSubData("OrganizationMonthlyIRSubData"),

	/** The Organization counter coaching monthly IR sub data. */
	OrganizationCounterCoachingMonthlyIRSubData(
			"OrganizationCounterCoachingMonthlyIRSubData"),

	/** The One to one impact IDR. */
	OneToOneImpactIDR("OneToOneImpactIDR"),

	/** The Tenure wise chart 1. */
	TenureWiseChart1("TenureWiseChart1"),

	/** The Tenure wise chart 2. */
	TenureWiseChart2("TenureWiseChart2");

	/** The Constant stringToEnum. */
	private static final ConcurrentMap<String, ChartType> stringToEnum = new ConcurrentHashMap<String, ChartType>();

	static {
		for (final ChartType u : values()) {
			stringToEnum.put(u.msgTypeCode, u);
		}
	}

	/** The msg type code. */
	private final String msgTypeCode;

	/**
	 * Instantiates a new chart type.
	 *
	 * @param msgTypeCode
	 *            the msg type code
	 */
	private ChartType(final String msgTypeCode) {
		this.msgTypeCode = msgTypeCode;
	}

	/**
	 * Gets the msg type code.
	 *
	 * @return the msg type code
	 */
	public String getMsgTypeCode() {
		return this.msgTypeCode;
	}

	/**
	 * From string.
	 *
	 * @param code
	 *            the code
	 * @return the chart type
	 */
	public static ChartType fromString(final String code) {
		return stringToEnum.get(code);
	}

	/**
	 * Gets the types.
	 *
	 * @param types
	 *            the types
	 * @return the types
	 */
	public static ArrayList<Object> getTypes(final String[] types) {
		final ArrayList<Object> typesFinal = new ArrayList<Object>();
		for (final String code : types) {
			typesFinal.add(stringToEnum.get(code));
		}
		return typesFinal;
	}
}
