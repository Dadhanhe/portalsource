/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum StackholderType.
 */
public enum StackholderType {

	/** The Arrival. */
	AUDITTRAIL(0),

	/** The blueprint. */
	BLUEPRINT(1),

	/** The data. */
	DATA(2);
	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the metrics date type
	 */
	public static StackholderType fromInteger(final Integer value) {
		if (value == AUDITTRAIL.value) {
			return AUDITTRAIL;
		} else if (value == BLUEPRINT.value) {
			return BLUEPRINT;
		} else if (value == DATA.value) {
			return DATA;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new metrics date type.
	 *
	 * @param value
	 *            the value
	 */
	private StackholderType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer to string.
	 *
	 * @param value
	 *            the value
	 * @return the string
	 */
	public static String fromIntegerToString(final Integer value) {
		if (value == AUDITTRAIL.value) {
			return "AUDITTRAIL";
		} else if (value == BLUEPRINT.value) {
			return "BLUEPRINT";
		} else if (value == DATA.value) {
			return "DATA";
		}
		return null;
	}
}
