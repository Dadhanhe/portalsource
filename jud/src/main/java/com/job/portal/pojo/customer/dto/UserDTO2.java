/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDTO2.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class UserDTO2 extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1786792803583942892L;

	/** The first name. */
	private String firstName;

	/** The last name. */
	private String lastName;

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		String name = "";
		if (!Objects.isEmpty(this.firstName)) {
			name += this.firstName + " ";
		}
		if (!Objects.isEmpty(this.lastName)) {
			name += this.lastName;
		}
		return name;
	}

}
