/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.mapper;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Interface AbstractMapper.
 *
 * @param <E>
 *            the element type
 * @param <D>
 *            the generic type
 */
public interface IAbstractMapper<E extends AbstractValueObject, D extends AbstractDTO> {

	/**
	 * To entity.
	 *
	 * @param d
	 *            the d
	 * @return the abstract value object
	 */
	E toEntity(D d);

	/**
	 * To dto.
	 *
	 * @param e
	 *            the e
	 * @return the abstract DTO
	 */
	D toDto(E e);

	/**
	 * Check created by updated by.
	 *
	 * @param d
	 *            the d
	 * @param e
	 *            the e
	 */
	default void checkCreatedByUpdatedBy(final D d, final E e) {
		if (Objects.isEmpty(d.getCreatedById())) {
			e.setCreatedBy(null);
		}
		if (Objects.isEmpty(d.getUpdatedById())) {
			e.setUpdatedBy(null);
		}
	}

}
