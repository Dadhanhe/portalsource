/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Tier.
 */
public enum Tier {

	/** The Tier 1. */
	Tier1(0),

	/** The Tier 2. */
	Tier2(1),

	/** The Tier 3. */
	Tier3(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the tier
	 */
	public static Tier fromInteger(final Integer value) {
		if (value == Tier1.value) {
			return Tier1;
		} else if (value == Tier2.value) {
			return Tier2;
		} else if (value == Tier3.value) {
			return Tier3;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new tier.
	 *
	 * @param value
	 *            the value
	 */
	private Tier(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("tier.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("tier.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Tier1.value) {
			return "Tier1";
		} else if (value == Tier2.value) {
			return "Tier2";
		} else if (value == Tier3.value) {
			return "Tier3";
		}
		return null;
	}
}
