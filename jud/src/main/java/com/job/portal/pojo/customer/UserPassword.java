/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class UserPassword.
 */
@Entity
@Table(name = "user_password")
public class UserPassword extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user id. */
	private Integer userId;

	/** The password. */
	private String password;

	/** The password change date. */
	private Date passwordChangeDate;

	/**
	 * Instantiates a new user password.
	 */
	public UserPassword() {
	}

	/**
	 * Instantiates a new user password.
	 *
	 * @param userId
	 *            the user id
	 * @param password
	 *            the password
	 * @param passwordChangeDate
	 *            the password change date
	 */
	public UserPassword(final Integer userId, final String password,
			final Date passwordChangeDate) {
		this.userId = userId;
		this.password = password;
		this.passwordChangeDate = passwordChangeDate;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name = "USER_ID", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(final Integer userId) {
		this.userId = userId;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	@Column(name = "PASSWORD", nullable = false, length = 100)
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Gets the password change date.
	 *
	 * @return the password change date
	 */
	@Column(name = "PASSWORD_CHANGE_DATE", nullable = false, length = 100)
	public Date getPasswordChangeDate() {
		return this.passwordChangeDate;
	}

	/**
	 * Sets the password change date.
	 *
	 * @param passwordChangeDate
	 *            the new password change date
	 */
	public void setPasswordChangeDate(final Date passwordChangeDate) {
		this.passwordChangeDate = passwordChangeDate;
	}

}
