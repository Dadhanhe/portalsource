/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class UserLoginDetail.
 */
@Entity
@Table(name = "user_login_detail")
public class UserLoginDetail extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8343472298710220652L;

	/** The user id. */
	Integer userId;

	/** The platform. */
	Integer platform;

	/** The device id. */
	String deviceId;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;

	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name = "USER_ID", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(final Integer userId) {
		this.userId = userId;
	}

	/**
	 * Gets the platform.
	 *
	 * @return the platform
	 */
	@Column(name = "PLATFORM", nullable = false)
	public Integer getPlatform() {
		return this.platform;
	}

	/**
	 * Sets the platform.
	 *
	 * @param platform
	 *            the new platform
	 */
	public void setPlatform(final Integer platform) {
		this.platform = platform;
	}

	/**
	 * Gets the device id.
	 *
	 * @return the device id
	 */
	@Column(name = "DEVICE_ID", nullable = false)
	public String getDeviceId() {
		return this.deviceId;
	}

	/**
	 * Sets the device id.
	 *
	 * @param deviceId
	 *            the new device id
	 */
	public void setDeviceId(final String deviceId) {
		this.deviceId = deviceId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", nullable = false, length = 19)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
