/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum ContestReportStatus.
 */
public enum ContestReportStatus implements Serializable {

	/** The Pending. */
	Pending(0),

	/** The Launched. */
	Launched(1),

	/** The Completed. */
	Completed(2),

	/** The Cancelled. */
	Cancelled(3);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new contest report status.
	 *
	 * @param value
	 *            the value
	 */
	private ContestReportStatus(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the contest report status
	 */
	public static ContestReportStatus fromInteger(final Integer value) {
		if (value == Pending.value) {
			return Pending;
		} else if (value == Launched.value) {
			return Launched;
		} else if (value == Completed.value) {
			return Completed;
		} else if (value == Cancelled.value) {
			return Cancelled;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Pending.value) {
			return "Pending";
		} else if (value == Launched.value) {
			return "Launched";
		} else if (value == Completed.value) {
			return "Completed";
		} else if (value == Cancelled.value) {
			return "Cancelled";
		}
		return null;
	}

}
