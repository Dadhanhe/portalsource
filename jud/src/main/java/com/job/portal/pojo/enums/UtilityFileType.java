/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum UtilityFileType.
 */
public enum UtilityFileType {

	/** The Europ car. */
	EuropCar(0),

	/** The Hilton. */
	Hilton(1),

	/** The France. */
	France(2),

	/** The Itel bpo. */
	ItelBpo(3),

	/** The Delware north. */
	DelwareNorth(4),

	/** The Portugal. */
	Portugal(5),

	/** The Cyprus. */
	Cyprus(6);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the utility file type
	 */
	public static UtilityFileType fromInteger(final Integer value) {
		if (value == EuropCar.value) {
			return EuropCar;
		} else if (value == Hilton.value) {
			return Hilton;
		} else if (value == France.value) {
			return France;
		} else if (value == ItelBpo.value) {
			return ItelBpo;
		} else if (value == DelwareNorth.value) {
			return DelwareNorth;
		} else if (value == Portugal.value) {
			return Portugal;
		} else if (value == Cyprus.value) {
			return Cyprus;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new utility file type.
	 *
	 * @param value
	 *            the value
	 */
	private UtilityFileType(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == EuropCar.value) {
			return "EuropCar";
		} else if (value == Hilton.value) {
			return "Hilton";
		} else if (value == France.value) {
			return "France";
		} else if (value == ItelBpo.value) {
			return "ItelBpo";
		} else if (value == DelwareNorth.value) {
			return "DelwareNorth";
		} else if (value == Portugal.value) {
			return "Portugal";
		} else if (value == Cyprus.value) {
			return "Cyprus";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param utilityFileType
	 *            the utility file type
	 * @return the enum
	 */
	public static UtilityFileType getEnum(final String utilityFileType) {
		if (utilityFileType != null) {
			if (utilityFileType.equals("EuropCar")) {
				return UtilityFileType.EuropCar;
			} else if (utilityFileType.equals("Hilton")) {
				return UtilityFileType.Hilton;
			} else if (utilityFileType.equals("France")) {
				return UtilityFileType.France;
			} else if (utilityFileType.equals("ItelBpo")) {
				return UtilityFileType.ItelBpo;
			} else if (utilityFileType.equals("DelwareNorth")) {
				return UtilityFileType.DelwareNorth;
			} else if (utilityFileType.equals("Portugal")) {
				return UtilityFileType.Portugal;
			} else if (utilityFileType.equals("Cyprus")) {
				return UtilityFileType.Cyprus;
			}
		}
		return null;
	}
}
