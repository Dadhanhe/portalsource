/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;

/**
 * @author Vaibhav
 *
 */
@JsonDeserialize(using = DTOSerializer.class)
public class CountryDTO extends AbstractDTO {

	private String name;

	private String code;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

}
