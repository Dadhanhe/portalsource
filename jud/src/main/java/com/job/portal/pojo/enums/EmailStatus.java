/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum EmailStatus.
 */
public enum EmailStatus {

	/** The Pending. */
	Pending(0),

	/** The Sent. */
	Sent(1),

	/** The Failed. */
	Failed(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the email status
	 */
	public static EmailStatus fromInteger(final Integer value) {
		if (value == Pending.value) {
			return Pending;
		} else if (value == Sent.value) {
			return Sent;
		} else if (value == Failed.value) {
			return Failed;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new email status.
	 *
	 * @param value
	 *            the value
	 */
	private EmailStatus(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("email.status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
