/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonValue;

// TODO: Auto-generated Javadoc
/**
 * The Enum RoleType.
 */
public enum RoleType implements Serializable {

	/** The super admin. */
	SUPER_ADMIN(1),

	/** The facilitator. */
	FACILITATOR(2),

	/** The customer admin. */
	CUSTOMER_ADMIN(3),

	/** The driver. */
	DRIVER(4);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new role type.
	 *
	 * @param val
	 *            the val
	 */
	private RoleType(int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	@JsonValue
	public int getValue() {
		return this.value;
	}

}
