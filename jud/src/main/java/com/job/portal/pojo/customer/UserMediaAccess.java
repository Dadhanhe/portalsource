/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.CommandCenterActionSource;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.JsonDateTimeSerializer;

// TODO: Auto-generated Javadoc
/**
 * The Class UserMediaAccess.
 */
@Entity
@Table(name = "user_media_access")
public class UserMediaAccess extends AbstractValueObject implements Cloneable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user id. */
	private User userId;

	/** The media id. */
	private Integer mediaId;

	/** The note. */
	private String note;

	/** The is watched. */
	private Boolean isWatched;

	/** The command center action source. */
	private CommandCenterActionSource commandCenterActionSource;

	/** The tenant location id. */
	private Integer tenantLocationId;

	/** The first watched time. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date firstWatchedTime;

	/** The expire date. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date expireDate;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	public User getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param user
	 *            the new user id
	 */
	public void setUserId(final User user) {
		this.userId = user;
	}

	/**
	 * Gets the media id.
	 *
	 * @return the media id
	 */
	@Column(name = "MEDIA_ID", nullable = false, length = 45)
	public Integer getMediaId() {
		return this.mediaId;
	}

	/**
	 * Sets the media id.
	 *
	 * @param mediaId
	 *            the new media id
	 */
	public void setMediaId(final Integer mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * Gets the note.
	 *
	 * @return the note
	 */
	@Column(name = "note")
	public String getNote() {
		return this.note;
	}

	/**
	 * Sets the note.
	 *
	 * @param note
	 *            the new note
	 */
	public void setNote(final String note) {
		this.note = note;
	}

	/**
	 * Gets the checks if is watched.
	 *
	 * @return the checks if is watched
	 */
	@Column(name = "IS_WATCHED", nullable = false)
	public Boolean getIsWatched() {
		return this.isWatched;
	}

	/**
	 * Sets the checks if is watched.
	 *
	 * @param isWatched
	 *            the new checks if is watched
	 */
	public void setIsWatched(final Boolean isWatched) {
		this.isWatched = isWatched;
	}

	/**
	 * Gets the first watched time.
	 *
	 * @return the first watched time
	 */
	@Column(name = "FIRST_WATCHED_TIME")
	public Date getFirstWatchedTime() {
		return this.firstWatchedTime;
	}

	/**
	 * Sets the first watched time.
	 *
	 * @param firstWatchedTime
	 *            the new first watched time
	 */
	public void setFirstWatchedTime(final Date firstWatchedTime) {
		this.firstWatchedTime = firstWatchedTime;
	}

	/**
	 * Gets the expire date.
	 *
	 * @return the expire date
	 */
	@Column(name = "EXPIRE_DATE", nullable = false)
	public Date getExpireDate() {
		return this.expireDate;
	}

	/**
	 * Sets the expire date.
	 *
	 * @param expireDate
	 *            the new expire date
	 */
	public void setExpireDate(final Date expireDate) {
		this.expireDate = expireDate;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedBy()
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CREATED_BY")
	@Override
	public User getCreatedBy() {
		return this.createdBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#setCreatedBy(com.job.
	 * portal. pojo.customer.User)
	 */
	@Override
	public void setCreatedBy(final User createdBy) {
		this.createdBy = createdBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedOn()
	 */
	@Override
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getActiveStatus()
	 */
	@Override
	@Column(name = "ACTIVE_STATUS", nullable = false)
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setActiveStatus(com.fpg.
	 * pulse.pojo.enums.Status)
	 */
	@Override
	public void setActiveStatus(final Status activeStatus) {
		this.activeStatus = activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedBy()
	 */
	@Override
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "UPDATED_BY", nullable = false)
	public User getUpdatedBy() {
		return this.updatedBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#setUpdatedBy(com.job.
	 * portal. pojo.customer.User)
	 */
	@Override
	public void setUpdatedBy(final User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the command center action source.
	 *
	 * @return the command center action source
	 */
	@Transient
	public CommandCenterActionSource getCommandCenterActionSource() {
		return this.commandCenterActionSource;
	}

	/**
	 * Sets the command center action source.
	 *
	 * @param commandCenterActionSource
	 *            the new command center action source
	 */
	public void setCommandCenterActionSource(
			final CommandCenterActionSource commandCenterActionSource) {
		this.commandCenterActionSource = commandCenterActionSource;
	}

	/**
	 * Gets the tenant location id.
	 *
	 * @return the tenant location id
	 */
	@Transient
	public Integer getTenantLocationId() {
		return this.tenantLocationId;
	}

	/**
	 * Sets the tenant location id.
	 *
	 * @param tenantLocationId
	 *            the new tenant location id
	 */
	public void setTenantLocationId(final Integer tenantLocationId) {
		this.tenantLocationId = tenantLocationId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#clone()
	 */
	@Override
	public UserMediaAccess clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (UserMediaAccess) super.clone();
	}

}
