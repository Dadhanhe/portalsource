/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum ReportDataMetric.
 */
public enum ReportDataMetric {

	/** The Daily. */
	Daily(0),

	/** The Weekly. */
	Weekly(1),

	/** The Monthly. */
	Monthly(2),

	/** The Yearly. */
	Yearly(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the report data metric
	 */
	public static ReportDataMetric fromInteger(final Integer value) {
		if (value == Daily.value) {
			return Daily;
		} else if (value == Weekly.value) {
			return Weekly;
		} else if (value == Monthly.value) {
			return Monthly;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new report data metric.
	 *
	 * @param value
	 *            the value
	 */
	private ReportDataMetric(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("report.data.metric.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}