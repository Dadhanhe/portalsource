/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.Preference;
import com.job.portal.pojo.common.dto.PreferenceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface IPreferenceMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IPreferenceMapper
		extends IAbstractMapper<Preference, PreferenceDTO> {

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.mapper.IAbstractMapper#toEntity(com.job.portal.
	 * pojo.common.dto.AbstractDTO)
	 */
	@Override
	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	Preference toEntity(PreferenceDTO dto);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.mapper.IAbstractMapper#toDto(com.job.portal.pojo
	 * .common.AbstractValueObject)
	 */
	@Override
	@InheritInverseConfiguration
	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	PreferenceDTO toDto(Preference entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final PreferenceDTO dto,
			@MappingTarget final Preference entity) {
		checkCreatedByUpdatedBy(dto, entity);
	}
}
