/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc

/**
 * The Enum SourceModule.
 */
public enum SourceModule {

	/** The Counter coaching. */
	CounterCoaching(1),

	/** The Command center. */
	CommandCenter(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the source module
	 */
	public static SourceModule fromInteger(final Integer value) {
		if (value == CounterCoaching.value) {
			return CounterCoaching;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new source module.
	 *
	 * @param value
	 *            the value
	 */
	private SourceModule(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("SourceModule.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
