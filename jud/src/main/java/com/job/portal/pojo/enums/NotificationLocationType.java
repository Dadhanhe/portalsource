/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum NotificationLocationType.
 */
public enum NotificationLocationType {

	/** The All locations. */
	AllLocations(0),

	/** The Master locations. */
	MasterLocations(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new notification location type.
	 *
	 * @param value
	 *            the value
	 */
	private NotificationLocationType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the notification location type
	 */
	public static NotificationLocationType fromInteger(final Integer value) {
		if (value == AllLocations.value) {
			return AllLocations;
		} else if (value == MasterLocations.value) {
			return MasterLocations;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == AllLocations.value) {
			return "AllLocations";
		} else if (value == MasterLocations.value) {
			return "MasterLocations";
		}
		return null;
	}
}
