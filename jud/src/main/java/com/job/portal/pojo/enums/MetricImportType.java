/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum MetricImportType.
 */
public enum MetricImportType {

	/** The web add. */
	WEB_ADD(0),

	/** The web import. */
	WEB_IMPORT(1),

	/** The utility. */
	UTILITY(2),

	/** The ecommerce. */
	ECOMMERCE(3),

	/** The ios add. */
	IOS_ADD(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the metric import type
	 */
	public static MetricImportType fromInteger(final Integer value) {
		if (value == WEB_ADD.value) {
			return WEB_ADD;
		} else if (value == WEB_IMPORT.value) {
			return WEB_IMPORT;
		} else if (value == UTILITY.value) {
			return UTILITY;
		} else if (value == ECOMMERCE.value) {
			return ECOMMERCE;
		} else if (value == IOS_ADD.value) {
			return IOS_ADD;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new metric import type.
	 *
	 * @param value
	 *            the value
	 */
	private MetricImportType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("entity.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

}
