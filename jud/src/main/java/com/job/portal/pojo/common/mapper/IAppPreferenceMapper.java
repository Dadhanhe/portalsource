/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.AppPreference;
import com.job.portal.pojo.common.dto.AppPreferenceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface IAppPreferenceMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IAppPreferenceMapper
		extends IAbstractMapper<AppPreference, AppPreferenceDTO> {

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.mapper.IAbstractMapper#toEntity(com.job.portal.
	 * pojo.common.dto.AbstractDTO)
	 */
	@Override
	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	AppPreference toEntity(AppPreferenceDTO dto);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.mapper.IAbstractMapper#toDto(com.job.portal.pojo
	 * .common.AbstractValueObject)
	 */
	@Override
	@InheritInverseConfiguration
	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	AppPreferenceDTO toDto(AppPreference entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final AppPreferenceDTO dto,
			@MappingTarget final AppPreference entity) {
		checkCreatedByUpdatedBy(dto, entity);
	}
}
