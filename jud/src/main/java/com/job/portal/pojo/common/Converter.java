/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.common;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.job.portal.pojo.common.dto.FieldSchemeDTO;
import com.job.portal.pojo.common.dto.PreferenceDTO;
import com.job.portal.pojo.customer.Course;
import com.job.portal.pojo.customer.Disability;
import com.job.portal.pojo.customer.EducationBoard;
import com.job.portal.pojo.customer.Industry;
import com.job.portal.pojo.customer.Subindustry;
import com.job.portal.pojo.customer.University;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.dto.CourseDTO;
import com.job.portal.pojo.customer.dto.DisabilityDTO;
import com.job.portal.pojo.customer.dto.EducationBoardDTO;
import com.job.portal.pojo.customer.dto.IndustryDTO;
import com.job.portal.pojo.customer.dto.SubindustryDTO;
import com.job.portal.pojo.customer.dto.UniversityDTO;
import com.job.portal.pojo.customer.dto.UserDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class Converter.
 */
@Component
public class Converter {

	/** The derserializer mappings. */
	@SuppressWarnings("rawtypes")
	static HashMap<String, Class> derserializerMappings = new HashMap<>();

	/** The mappings. */
	static HashMap<String, Class> mappings = new HashMap<>();

	static {
		derserializerMappings.put("USER", UserDTO.class);
		derserializerMappings.put("EDUCATIONBOARD", EducationBoardDTO.class);
		derserializerMappings.put("UNIVERSITY", UniversityDTO.class);
		derserializerMappings.put("DISABILITY", DisabilityDTO.class);
		derserializerMappings.put("COURSE", CourseDTO.class);
		mappings.put("COURSE", Course.class);
		derserializerMappings.put("INDUSTRY", IndustryDTO.class);
		mappings.put("INDUSTRY", Industry.class);
		derserializerMappings.put("SUBINDUSTRY", SubindustryDTO.class);
		mappings.put("SUBINDUSTRY", Subindustry.class);
		mappings.put("DISABILITY", Disability.class);
		mappings.put("EDUCATIONBOARD", EducationBoard.class);
		mappings.put("UNIVERSITY", University.class);
		mappings.put("EducationBoardDTO", EducationBoardDTO.class);
		derserializerMappings.put("FIELDSCHEME", FieldSchemeDTO.class);
		mappings.put("USER", User.class);
		mappings.put("USERDTO", UserDTO.class);
		mappings.put("FIELDSCHEMEDTO", FieldSchemeDTO.class);
		mappings.put("PREFERENCE", Preference.class);
		mappings.put("PREFERENCEDTO", PreferenceDTO.class);

	}

	/**
	 * Hex to ASCII.
	 *
	 * @param hexValue
	 *            the hex value
	 * @return the string
	 */
	public static String hexToASCII(final String hexValue) {
		final StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexValue.length(); i += 2) {
			final String str = hexValue.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return output.toString();
	}

	/**
	 * Ascii to hex.
	 *
	 * @param asciiValue
	 *            the ascii value
	 * @return the string
	 */
	public static String asciiToHex(final String asciiValue) {
		final char[] chars = asciiValue.toCharArray();
		final StringBuilder hex = new StringBuilder();
		for (final char c : chars) {
			hex.append(Integer.toHexString(c));
		}
		return hex.toString();
	}

	/**
	 * Gets the gson.
	 *
	 * @return the gson
	 */
	@SuppressWarnings("rawtypes")
	public static Gson getGson() {
		final GsonBuilder builder = new GsonBuilder();
		final JsonSerializer<Date> ser = new JsonSerializer<Date>() {
			@Override
			public JsonElement serialize(final Date src, final Type typeOfSrc,
					final JsonSerializationContext context) {
				return src == null ? null : new JsonPrimitive(src.getTime());
			}
		};
		// Register an adapter to manage the date types as long values
		builder.registerTypeAdapter(Date.class, new JsonDeserializer() {
			@Override
			public Date deserialize(final JsonElement json, final Type typeOfT,
					final JsonDeserializationContext context)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}
		}).setDateFormat(DateFormat.LONG).registerTypeAdapter(Date.class, ser);
		final Gson gson = builder.create();
		return gson;
	}

	/**
	 * Gets the deserializer mapping.
	 *
	 * @param key
	 *            the key
	 * @return the deserializer mapping
	 */
	@SuppressWarnings("rawtypes")
	public Class getDeserializerMapping(final String key) {
		return derserializerMappings.get(key);
	}

	/**
	 * Gets the model mapping.
	 *
	 * @param key
	 *            the key
	 * @return the model mapping
	 */
	@SuppressWarnings("rawtypes")
	public Class getModelMapping(final String key) {
		return mappings.get(key);
	}

}
