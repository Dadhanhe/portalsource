/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum LevelType.
 */
public enum LevelType {

	/** The Media. */
	Media(0),

	/** The Questionnarie. */
	Questionnarie(1);

	/** The value. */
	private final int value;

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the level type
	 */
	public static LevelType fromInteger(final Integer value) {
		if (value == Media.value) {
			return Media;
		} else if (value == Questionnarie.value) {
			return Questionnarie;
		}
		return null;
	}

	/**
	 * Instantiates a new level type.
	 *
	 * @param value
	 *            the value
	 */
	private LevelType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

}
