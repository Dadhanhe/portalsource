/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum SolrIndexStatus.
 */
public enum SolrIndexStatus {

	/** The indexed. */
	INDEXED(0),

	/** The not indexed. */
	NOT_INDEXED(1);

	/**
	 * Instantiates a new solr index status.
	 *
	 * @param value
	 *            the value
	 */
	private SolrIndexStatus(final int value) {
		this.value = value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the solr index status
	 */
	public static SolrIndexStatus fromInteger(final Integer value) {
		if (value == INDEXED.value) {
			return INDEXED;
		} else if (value == NOT_INDEXED.value) {
			return NOT_INDEXED;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == INDEXED.value) {
			return "INDEXED";
		} else if (value == NOT_INDEXED.value) {
			return "NOT_INDEXED";
		}
		return null;
	}
}
