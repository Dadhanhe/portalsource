/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

// Generated Oct 5, 2016 2:02:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class PasswordReset.
 */
@Entity
@Table(name = "password_reset")
public class PasswordReset extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3995667037077680282L;

	/** The timestamp. */

	/** The timestamp. */
	private Date timestamp;

	/** The user. */
	private User user;

	/** The token. */
	private String token;

	/** The accessed time. */
	private Date accessedTime;

	/** The is used. */
	private boolean isUsed;

	/** The old id. */
	private Integer oldId;

	/** The password. */

	/** The password. */
	private String password;

	/**
	 * Instantiates a new password reset.
	 */
	public PasswordReset() {
		this.timestamp = new Date();
	}

	/**
	 * Instantiates a new password reset.
	 *
	 * @param user
	 *            the user
	 * @param token
	 *            the token
	 * @param isUsed
	 *            the is used
	 */
	public PasswordReset(final User user, final String token,
			final boolean isUsed) {
		this.user = user;
		this.token = token;
		this.isUsed = isUsed;
	}

	/**
	 * Instantiates a new password reset.
	 *
	 * @param user
	 *            the user
	 * @param token
	 *            the token
	 * @param accessedTime
	 *            the accessed time
	 * @param isUsed
	 *            the is used
	 * @param oldId
	 *            the old id
	 */
	public PasswordReset(final User user, final String token,
			final Date accessedTime, final boolean isUsed,
			final Integer oldId) {
		this.user = user;
		this.token = token;
		this.accessedTime = accessedTime;
		this.isUsed = isUsed;
		this.oldId = oldId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	@Version
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TIMESTAMP", nullable = false, length = 19)
	public Date getTimestamp() {
		return this.timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp
	 *            the new timestamp
	 */
	public void setTimestamp(final Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", nullable = false)
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user
	 *            the new user
	 */
	public void setUser(final User user) {
		this.user = user;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	@Column(name = "TOKEN", nullable = false, length = 45)
	public String getToken() {
		return this.token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token
	 *            the new token
	 */
	public void setToken(final String token) {
		this.token = token;
	}

	/**
	 * Gets the accessed time.
	 *
	 * @return the accessed time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ACCESSED_TIME", length = 19)
	public Date getAccessedTime() {
		return this.accessedTime;
	}

	/**
	 * Sets the accessed time.
	 *
	 * @param accessedTime
	 *            the new accessed time
	 */
	public void setAccessedTime(final Date accessedTime) {
		this.accessedTime = accessedTime;
	}

	/**
	 * Checks if is checks if is used.
	 *
	 * @return true, if is checks if is used
	 */
	@Column(name = "IS_USED", nullable = false)
	public boolean isIsUsed() {
		return this.isUsed;
	}

	/**
	 * Sets the checks if is used.
	 *
	 * @param isUsed
	 *            the new checks if is used
	 */
	public void setIsUsed(final boolean isUsed) {
		this.isUsed = isUsed;
	}

	/**
	 * Gets the old id.
	 *
	 * @return the old id
	 */
	@Column(name = "OLD_ID")
	public Integer getOldId() {
		return this.oldId;
	}

	/**
	 * Sets the old id.
	 *
	 * @param oldId
	 *            the new old id
	 */
	public void setOldId(final Integer oldId) {
		this.oldId = oldId;
	}

	/**
	 * Checks if is valid.
	 *
	 * @return true, if is valid
	 */
	@Transient
	public boolean isValid() {
		final Date now = new Date();
		float timeSpan = (float) now.getTime() - this.timestamp.getTime();
		timeSpan = timeSpan / 1000 / 60 / 60;// hours
		if (timeSpan > 12) {// 超过12小时，则过期
			return false;
		}
		if (this.isUsed) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	@Transient
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getJsonData()
	 */
	@Override
	@Column(name = "json_data")
	public String getJsonData() {
		return this.jsonData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setJsonData(java.lang.
	 * String)
	 */
	@Override
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

}
