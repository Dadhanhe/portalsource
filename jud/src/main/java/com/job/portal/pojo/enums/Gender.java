/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Gender.
 */
public enum Gender {

	/** The Male. */
	Male(0),

	/** The Female. */
	Female(1);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the gender
	 */
	public static Gender fromInteger(Integer value) {
		if (value == Male.value) {
			return Male;
		} else if (value == Female.value) {
			return Female;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new gender.
	 *
	 * @param value
	 *            the value
	 */
	private Gender(int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("gender.%d", value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return value;
	}
}
