/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum OnboardingCategoryType.
 */
public enum OnboardingCategoryType implements Serializable {

	/** The Normal. */
	General(0),

	/** The Question answer. */
	QuestionAnswer(1),

	/** The Product mapping. */
	ProductMapping(2);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new onboarding category type.
	 *
	 * @param value
	 *            the value
	 */
	private OnboardingCategoryType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}
}
