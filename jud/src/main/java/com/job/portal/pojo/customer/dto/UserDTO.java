/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;
import com.job.portal.utils.JsonDateOnlySerializer;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.JsonDateTimeSerializer;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class UserDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1786792803583942892L;

	/** The access token. */
	@Field
	private String accessToken;

	/** The android version. */
	@Field
	private String androidVersion;

	/** The authy id. */
	@Field
	private String authyId;

	/** The birth date. */
	@Field
	private Date birthDate;

	/** The birth day. */
	@Field
	private Integer birthDay;

	/** The birth month. */
	@Field
	private Integer birthMonth;

	/** The country code. */
	@Field
	private String countryCode;

	/** The default industry. */
	@Field
	private Integer defaultIndustry;

	/** Dashboard Id. */
	@Field
	private String dashboardUrl;

	/** The desired location. */
	@Field
	private String desiredLocation;

	/** The device id. */
	@Field
	private String deviceId;

	/** The last selected tenant. */
	private Integer lastSelectedTenant;

	/** The email. */
	@Field
	@NotBlank(groups = { AbstractDTO.CreateObjectValidationGroup.class,
			AbstractDTO.UpdateObjectValidationGroup.class }, message = "{NotBlank.Email}")
	private String email;

	/** The user full name. */
	@Field
	private String userFullName;

	/** The speaks. */
	private String speaks;

	/** The hobbies. */
	private String hobbies;

	/** The first name. */
	@Field
	private String firstName;

	/** The fpg level. */
	@Field
	private String fpgLevel;

	/** The gender. */
	@Field
	private String gender;

	/** The hired date. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date hiredDate;

	/** The ios version. */
	@Field
	private String iosVersion;

	/** The is authy attempted. */
	@Field
	private Boolean isAuthyAttempted;

	/** The is first login. */
	@Field
	private Boolean isFirstLogin;

	/** The is fpg member. */
	@Field
	private Boolean isFpgMember;

	/** The is operator. */
	@Field
	private Boolean isOperator;

	/** The department. */
	@Field
	private String department;

	/** The master location. */
	@Field
	private String masterLocation;

	/** The job title. */
	@Field
	private String jobTitle;

	/** The last name. */
	@Field
	private String lastName;

	/** The linkedin id. */
	@Field
	private String linkedinId;

	/** The linkedin image. */
	@Field
	private String linkedinImage;

	/** The middle name. */
	@Field
	private String middleName;

	/** The mobile number. */
	@Field
	private String mobileNumber;

	/** The number. */
	@Field
	private String number;

	/** The password. */
	@Field
	private String password;

	/** The phone number. */
	@Field
	private String phoneNumber;

	/** The platform. */
	@Field
	private Integer platform;

	/** The pulse id. */
	@Field
	private String pulseId;

	/** The role id. */
	@Field
	private Integer roleId;

	/** The role name. */
	@Field
	private String roleName;

	/** The is super admin. */
	@Field
	private Boolean isSuperAdmin;

	/** The is performance manager. */
	@Field
	private Boolean isPerformanceManager;

	/** The is default tenant role. */
	@Field
	private Boolean isDefaultTenantRole;

	/** The salt. */
	@Field
	private String salt;

	/** The signed in time. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date signedInTime;

	/** The tenant id. */
	@Field
	private Integer tenantId;

	/** The tenant name. */
	@Field
	private String tenantName;

	/** The token expiry. */
	@Field
	private Date tokenExpiry;

	/** The user location name. */
	@Field
	private String userLocationName;

	/** The work age. */
	@Field
	private String workAge;

	/** The working type. */
	@Field
	private String workingType;

	/** The is contributor. */
	@Field
	private Boolean isContributor;

	/** The location group. */
	@Field
	private String locationGroup;

	/** The block event. */
	private String blockEvent;

	/** The user val. */
	@Field
	private String userVal;

	/** The assigned to tenant. */
	@Field
	private String assignedToTenant;

	/** The get last activity done on. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date getLastActivityDoneOn;

	/** The remove master location. */
	private Boolean removeMasterLocation;

	/** The remove role assignment. */
	private Boolean removeRoleAssignment;

	/** The last password change date. */
	@Field
	@Temporal(TemporalType.DATE)
	@JsonSerialize(using = JsonDateOnlySerializer.class)
	private Date lastPasswordChangeDate;

	/** The is password changed. */
	private boolean passwordChanged;

	/** The is beta tester. */
	private Boolean isBetaTester;

	/** The contact email. */
	private String contactEmail;

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	public String getAccessToken() {
		return this.accessToken;
	}

	/**
	 * Gets the android version.
	 *
	 * @return the android version
	 */
	public String getAndroidVersion() {
		return this.androidVersion;
	}

	/**
	 * Gets the authy id.
	 *
	 * @return the authy id
	 */
	public String getAuthyId() {
		return this.authyId;
	}

	/**
	 * Gets the birth date.
	 *
	 * @return the birth date
	 */
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 * Gets the birth day.
	 *
	 * @return the birth day
	 */
	public Integer getBirthDay() {
		return this.birthDay;
	}

	/**
	 * Gets the birth month.
	 *
	 * @return the birth month
	 */
	public Integer getBirthMonth() {
		return this.birthMonth;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountryCode() {
		return this.countryCode;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getCreatedOn()
	 */
	@Override
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/**
	 * Gets the default industry.
	 *
	 * @return the default industry
	 */
	public Integer getDefaultIndustry() {
		return this.defaultIndustry;
	}

	/**
	 * Gets the desired location.
	 *
	 * @return the desired location
	 */
	public String getDesiredLocation() {
		return this.desiredLocation;
	}

	/**
	 * Gets the device id.
	 *
	 * @return the device id
	 */
	public String getDeviceId() {
		return this.deviceId;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Gets the user full name.
	 *
	 * @return the user full name
	 */
	public String getUserFullName() {
		return this.userFullName;
	}

	/**
	 * Sets the user full name.
	 *
	 * @param userFullName
	 *            the new user full name
	 */
	public void setUserFullName(final String userFullName) {
		this.userFullName = userFullName;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Gets the fpg level.
	 *
	 * @return the fpg level
	 */
	public String getFpgLevel() {
		return this.fpgLevel;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		return this.gender;
	}

	/**
	 * Gets the hired date.
	 *
	 * @return the hired date
	 */
	public Date getHiredDate() {
		return this.hiredDate;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	public Integer getId() {
		return this.id;
	}

	/**
	 * Gets the ios version.
	 *
	 * @return the ios version
	 */
	public String getIosVersion() {
		return this.iosVersion;
	}

	/**
	 * Gets the checks if is authy attempted.
	 *
	 * @return the checks if is authy attempted
	 */
	public Boolean getIsAuthyAttempted() {
		return this.isAuthyAttempted;
	}

	/**
	 * Gets the checks if is first login.
	 *
	 * @return the checks if is first login
	 */
	public Boolean getIsFirstLogin() {
		return this.isFirstLogin;
	}

	/**
	 * Gets the checks if is fpg member.
	 *
	 * @return the checks if is fpg member
	 */
	public Boolean getIsFpgMember() {
		return this.isFpgMember;
	}

	/**
	 * Gets the checks if is operator.
	 *
	 * @return the checks if is operator
	 */
	public Boolean getIsOperator() {
		return this.isOperator;
	}

	/**
	 * Gets the job title.
	 *
	 * @return the job title
	 */
	public String getJobTitle() {
		return this.jobTitle;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Gets the linkedin id.
	 *
	 * @return the linkedin id
	 */
	public String getLinkedinId() {
		return this.linkedinId;
	}

	/**
	 * Gets the linkedin image.
	 *
	 * @return the linkedin image
	 */
	public String getLinkedinImage() {
		return this.linkedinImage;
	}

	/**
	 * Gets the middle name.
	 *
	 * @return the middle name
	 */
	public String getMiddleName() {
		return this.middleName;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public String getNumber() {
		return this.number;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Gets the platform.
	 *
	 * @return the platform
	 */
	public Integer getPlatform() {
		return this.platform;
	}

	/**
	 * Gets the pulse id.
	 *
	 * @return the pulse id
	 */
	public String getPulseId() {
		return this.pulseId;
	}

	/**
	 * Gets the role id.
	 *
	 * @return the role id
	 */
	public Integer getRoleId() {
		return this.roleId;
	}

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	public String getRoleName() {
		return this.roleName;
	}

	/**
	 * Gets the checks if is super admin.
	 *
	 * @return the checks if is super admin
	 */
	public Boolean getIsSuperAdmin() {
		return this.isSuperAdmin;
	}

	/**
	 * Sets the checks if is super admin.
	 *
	 * @param isSuperAdmin
	 *            the new checks if is super admin
	 */
	public void setIsSuperAdmin(final Boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	/**
	 * Gets the checks if is performance manager.
	 *
	 * @return the checks if is performance manager
	 */
	public Boolean getIsPerformanceManager() {
		return this.isPerformanceManager;
	}

	/**
	 * Sets the checks if is performance manager.
	 *
	 * @param isPerformanceManager
	 *            the new checks if is performance manager
	 */
	public void setIsPerformanceManager(final Boolean isPerformanceManager) {
		this.isPerformanceManager = isPerformanceManager;
	}

	/**
	 * Gets the checks if is default tenant role.
	 *
	 * @return the checks if is default tenant role
	 */
	public Boolean getIsDefaultTenantRole() {
		return this.isDefaultTenantRole;
	}

	/**
	 * Sets the checks if is default tenant role.
	 *
	 * @param isDefaultTenantRole
	 *            the new checks if is default tenant role
	 */
	public void setIsDefaultTenantRole(final Boolean isDefaultTenantRole) {
		this.isDefaultTenantRole = isDefaultTenantRole;
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	public String getSalt() {
		return this.salt;
	}

	/**
	 * Gets the signed in time.
	 *
	 * @return the signed in time
	 */
	public Date getSignedInTime() {
		return this.signedInTime;
	}

	/**
	 * Gets the tenant id.
	 *
	 * @return the tenant id
	 */
	public Integer getTenantId() {
		return this.tenantId;
	}

	/**
	 * Gets the tenant name.
	 *
	 * @return the tenant name
	 */
	public String getTenantName() {
		return this.tenantName;
	}

	/**
	 * Gets the token expiry.
	 *
	 * @return the token expiry
	 */
	public Date getTokenExpiry() {
		return this.tokenExpiry;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getUpdatedOn()
	 */
	@Override
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/**
	 * Gets the user location name.
	 *
	 * @return the user location name
	 */
	public String getUserLocationName() {
		return this.userLocationName;
	}

	/**
	 * Gets the work age.
	 *
	 * @return the work age
	 */
	public String getWorkAge() {
		return this.workAge;
	}

	/**
	 * Sets the access token.
	 *
	 * @param accessToken
	 *            the new access token
	 */
	public void setAccessToken(final String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * Sets the android version.
	 *
	 * @param androidVersion
	 *            the new android version
	 */
	public void setAndroidVersion(final String androidVersion) {
		this.androidVersion = androidVersion;
	}

	/**
	 * Sets the authy id.
	 *
	 * @param authyId
	 *            the new authy id
	 */
	public void setAuthyId(final String authyId) {
		this.authyId = authyId;
	}

	/**
	 * Sets the birth date.
	 *
	 * @param birthDate
	 *            the new birth date
	 */
	public void setBirthDate(final Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Sets the birth day.
	 *
	 * @param birthDay
	 *            the new birth day
	 */
	public void setBirthDay(final Integer birthDay) {
		this.birthDay = birthDay;
	}

	/**
	 * Sets the birth month.
	 *
	 * @param birthMonth
	 *            the new birth month
	 */
	public void setBirthMonth(final Integer birthMonth) {
		this.birthMonth = birthMonth;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode
	 *            the new country code
	 */
	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setCreatedOn(java.util.Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Sets the default industry.
	 *
	 * @param defaultIndustry
	 *            the new default industry
	 */
	public void setDefaultIndustry(final Integer defaultIndustry) {
		this.defaultIndustry = defaultIndustry;
	}

	/**
	 * Sets the desired location.
	 *
	 * @param desiredLocation
	 *            the new desired location
	 */
	public void setDesiredLocation(final String desiredLocation) {
		this.desiredLocation = desiredLocation;
	}

	/**
	 * Sets the device id.
	 *
	 * @param deviceId
	 *            the new device id
	 */
	public void setDeviceId(final String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Sets the fpg level.
	 *
	 * @param fpgLevel
	 *            the new fpg level
	 */
	public void setFpgLevel(final String fpgLevel) {
		this.fpgLevel = fpgLevel;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the new gender
	 */
	public void setGender(final String gender) {
		this.gender = gender;
	}

	/**
	 * Sets the hired date.
	 *
	 * @param hiredDate
	 *            the new hired date
	 */
	public void setHiredDate(final Date hiredDate) {
		this.hiredDate = hiredDate;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Sets the ios version.
	 *
	 * @param iosVersion
	 *            the new ios version
	 */
	public void setIosVersion(final String iosVersion) {
		this.iosVersion = iosVersion;
	}

	/**
	 * Sets the checks if is authy attempted.
	 *
	 * @param isAuthyAttempted
	 *            the new checks if is authy attempted
	 */
	public void setIsAuthyAttempted(final Boolean isAuthyAttempted) {
		this.isAuthyAttempted = isAuthyAttempted;
	}

	/**
	 * Sets the checks if is first login.
	 *
	 * @param isFirstLogin
	 *            the new checks if is first login
	 */
	public void setIsFirstLogin(final Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	/**
	 * Sets the checks if is fpg member.
	 *
	 * @param isFpgMember
	 *            the new checks if is fpg member
	 */
	public void setIsFpgMember(final Boolean isFpgMember) {
		this.isFpgMember = isFpgMember;
	}

	/**
	 * Sets the checks if is operator.
	 *
	 * @param isOperator
	 *            the new checks if is operator
	 */
	public void setIsOperator(final Boolean isOperator) {
		this.isOperator = isOperator;
	}

	/**
	 * Sets the job title.
	 *
	 * @param jobTitle
	 *            the new job title
	 */
	public void setJobTitle(final String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Sets the linkedin id.
	 *
	 * @param linkedinId
	 *            the new linkedin id
	 */
	public void setLinkedinId(final String linkedinId) {
		this.linkedinId = linkedinId;
	}

	/**
	 * Sets the linkedin image.
	 *
	 * @param linkedinImage
	 *            the new linkedin image
	 */
	public void setLinkedinImage(final String linkedinImage) {
		this.linkedinImage = linkedinImage;
	}

	/**
	 * Sets the middle name.
	 *
	 * @param middleName
	 *            the new middle name
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber
	 *            the new mobile number
	 */
	public void setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Sets the number.
	 *
	 * @param number
	 *            the new number
	 */
	public void setNumber(final String number) {
		this.number = number;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber
	 *            the new phone number
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Sets the platform.
	 *
	 * @param platform
	 *            the new platform
	 */
	public void setPlatform(final Integer platform) {
		this.platform = platform;
	}

	/**
	 * Sets the pulse id.
	 *
	 * @param pulseId
	 *            the new pulse id
	 */
	public void setPulseId(final String pulseId) {
		this.pulseId = pulseId;
	}

	/**
	 * Sets the role id.
	 *
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(final Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName
	 *            the new role name
	 */
	public void setRoleName(final String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Sets the salt.
	 *
	 * @param salt
	 *            the new salt
	 */
	public void setSalt(final String salt) {
		this.salt = salt;
	}

	/**
	 * Sets the signed in time.
	 *
	 * @param signedInTime
	 *            the new signed in time
	 */
	public void setSignedInTime(final Date signedInTime) {
		this.signedInTime = signedInTime;
	}

	/**
	 * Sets the tenant id.
	 *
	 * @param tenantId
	 *            the new tenant id
	 */
	public void setTenantId(final Integer tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * Sets the tenant name.
	 *
	 * @param tenantName
	 *            the new tenant name
	 */
	public void setTenantName(final String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 * Sets the token expiry.
	 *
	 * @param tokenExpiry
	 *            the new token expiry
	 */
	public void setTokenExpiry(final Date tokenExpiry) {
		this.tokenExpiry = tokenExpiry;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setUpdatedOn(java.util.Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * Sets the user location name.
	 *
	 * @param userLocationName
	 *            the new user location name
	 */
	public void setUserLocationName(final String userLocationName) {
		this.userLocationName = userLocationName;
	}

	/**
	 * Sets the work age.
	 *
	 * @param workAge
	 *            the new work age
	 */
	public void setWorkAge(final String workAge) {
		this.workAge = workAge;
	}

	public String getName() {
		String name = "";
		if (!Objects.isEmpty(this.firstName)) {
			name += this.firstName + " ";
		}
		if (!Objects.isEmpty(this.lastName)) {
			name += this.lastName;
		}
		return name;
	}

	/**
	 * Gets the working type.
	 *
	 * @return the working type
	 */
	public String getWorkingType() {
		return this.workingType;
	}

	/**
	 * Sets the working type.
	 *
	 * @param workingType
	 *            the new working type
	 */
	public void setWorkingType(final String workingType) {
		this.workingType = workingType;
	}

	/**
	 * Gets the last selected tenant.
	 *
	 * @return the last selected tenant
	 */
	public Integer getLastSelectedTenant() {
		return this.lastSelectedTenant;
	}

	/**
	 * Sets the last selected tenant.
	 *
	 * @param lastSelectedTenant
	 *            the new last selected tenant
	 */
	public void setLastSelectedTenant(final Integer lastSelectedTenant) {
		this.lastSelectedTenant = lastSelectedTenant;
	}

	/**
	 * Gets the checks if is contributor.
	 *
	 * @return the checks if is contributor
	 */
	public Boolean getIsContributor() {
		return this.isContributor;
	}

	/**
	 * Sets the checks if is contributor.
	 *
	 * @param isContributor
	 *            the new checks if is contributor
	 */
	public void setIsContributor(final Boolean isContributor) {
		this.isContributor = isContributor;
	}

	/**
	 * Gets the block event.
	 *
	 * @return the block event
	 */
	public String getBlockEvent() {
		return this.blockEvent;
	}

	/**
	 * Sets the block event.
	 *
	 * @param blockEvent
	 *            the new block event
	 */
	public void setBlockEvent(final String blockEvent) {
		this.blockEvent = blockEvent;
	}

	/**
	 * Gets the user val.
	 *
	 * @return the user val
	 */
	public String getUserVal() {
		return this.userVal;
	}

	/**
	 * Sets the user val.
	 *
	 * @param userVal
	 *            the new user val
	 */
	public void setUserVal(final String userVal) {
		this.userVal = userVal;
	}

	/**
	 * Gets the location group.
	 *
	 * @return the location group
	 */
	public String getLocationGroup() {
		return this.locationGroup;
	}

	/**
	 * Sets the location group.
	 *
	 * @param locationGroup
	 *            the new location group
	 */
	public void setLocationGroup(final String locationGroup) {
		this.locationGroup = locationGroup;
	}

	/**
	 * Gets the assigned to tenant.
	 *
	 * @return the assigned to tenant
	 */
	public String getAssignedToTenant() {
		return this.assignedToTenant;
	}

	/**
	 * Sets the assigned to tenant.
	 *
	 * @param assignedToTenant
	 *            the new assigned to tenant
	 */
	public void setAssignedToTenant(final String assignedToTenant) {
		this.assignedToTenant = assignedToTenant;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return this.department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department
	 *            the new department
	 */
	public void setDepartment(final String department) {
		this.department = department;
	}

	/**
	 * Gets the master location.
	 *
	 * @return the master location
	 */
	public String getMasterLocation() {
		return this.masterLocation;
	}

	/**
	 * Sets the master location.
	 *
	 * @param masterLocation
	 *            the new master location
	 */
	public void setMasterLocation(final String masterLocation) {
		this.masterLocation = masterLocation;
	}

	/**
	 * Gets the gets the last activity done on.
	 *
	 * @return the gets the last activity done on
	 */
	public Date getGetLastActivityDoneOn() {
		return this.getLastActivityDoneOn;
	}

	/**
	 * Sets the gets the last activity done on.
	 *
	 * @param getLastActivityDoneOn
	 *            the new gets the last activity done on
	 */
	public void setGetLastActivityDoneOn(final Date getLastActivityDoneOn) {
		this.getLastActivityDoneOn = getLastActivityDoneOn;
	}

	/**
	 * Gets the speaks.
	 *
	 * @return the speaks
	 */
	public String getSpeaks() {
		return this.speaks;
	}

	/**
	 * Sets the speaks.
	 *
	 * @param speaks
	 *            the new speaks
	 */
	public void setSpeaks(final String speaks) {
		this.speaks = speaks;
	}

	/**
	 * Gets the hobbies.
	 *
	 * @return the hobbies
	 */
	public String getHobbies() {
		return this.hobbies;
	}

	/**
	 * Sets the hobbies.
	 *
	 * @param hobbies
	 *            the new hobbies
	 */
	public void setHobbies(final String hobbies) {
		this.hobbies = hobbies;
	}

	/**
	 * Gets the removes the master location.
	 *
	 * @return the removes the master location
	 */
	public Boolean getRemoveMasterLocation() {
		return this.removeMasterLocation;
	}

	/**
	 * Sets the removes the master location.
	 *
	 * @param removeMasterLocation
	 *            the new removes the master location
	 */
	public void setRemoveMasterLocation(final Boolean removeMasterLocation) {
		this.removeMasterLocation = removeMasterLocation;
	}

	/**
	 * Gets the removes the role assignment.
	 *
	 * @return the removeRoleAssignment
	 */
	public Boolean getRemoveRoleAssignment() {
		return this.removeRoleAssignment;
	}

	/**
	 * Sets the removes the role assignment.
	 *
	 * @param removeRoleAssignment
	 *            the new removes the role assignment
	 */
	public void setRemoveRoleAssignment(final Boolean removeRoleAssignment) {
		this.removeRoleAssignment = removeRoleAssignment;
	}

	/**
	 * Gets the last password change date.
	 *
	 * @return the last password change date
	 */
	public Date getLastPasswordChangeDate() {
		return this.lastPasswordChangeDate;
	}

	/**
	 * Sets the last password change date.
	 *
	 * @param lastPasswordChangeDate
	 *            the new last password change date
	 */
	public void setLastPasswordChangeDate(final Date lastPasswordChangeDate) {
		this.lastPasswordChangeDate = lastPasswordChangeDate;
	}

	/**
	 * Checks if is password changed.
	 *
	 * @return true, if is password changed
	 */
	public boolean getPasswordChanged() {
		return this.passwordChanged;
	}

	/**
	 * Sets the password changed.
	 *
	 * @param passwordChanged
	 *            the new password changed
	 */
	public void setPasswordChanged(final boolean passwordChanged) {
		this.passwordChanged = passwordChanged;
	}

	/**
	 * Gets the dashboard url.
	 *
	 * @return the dashboard url
	 */
	public String getdashboardUrl() {
		return this.dashboardUrl;
	}

	/**
	 * Sets the password changed.
	 *
	 * @param dashboardUrl
	 *            the new password changed
	 */
	public void setPasswordChanged(final String dashboardUrl) {
		this.dashboardUrl = dashboardUrl;
	}

	/**
	 * Checks if is beta tester.
	 *
	 * @return true, if is beta tester
	 */
	public Boolean isBetaTester() {
		return this.isBetaTester;
	}

	/**
	 * Sets the beta tester.
	 *
	 * @param isBetaTester
	 *            the new beta tester
	 */
	public void setBetaTester(final Boolean isBetaTester) {
		this.isBetaTester = isBetaTester;
	}

	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	public String getContactEmail() {
		return this.contactEmail;
	}

	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail
	 *            the new contact email
	 */
	public void setContactEmail(final String contactEmail) {
		this.contactEmail = contactEmail;
	}

}
