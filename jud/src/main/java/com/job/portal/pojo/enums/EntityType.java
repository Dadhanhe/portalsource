/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum EntityType.
 */
public enum EntityType implements Serializable {

	/** The Tenant. */
	Tenant(0),

	/** The Tenant location. */
	TenantLocation(1),

	/** The Location metric. */
	LocationMetric(2),

	/** The Agent metric. */
	AgentMetric(3),

	/** The Product. */
	Product(4),

	/** The Product metric. */
	ProductMetric(5),

	/** The Location group. */
	LocationGroup(6),

	/** The Incentive plan. */
	IncentivePlan(7),

	/** The Group product category. */
	GroupProductCategory(8),

	/** The Discovery master. */
	DiscoveryMaster(9),

	/** The Country. */
	Country(10),

	/** The Region. */
	Region(11),

	/** The Ecommerce customer. */
	EcommerceCustomer(12),

	/** The Ecommerce event. */
	EcommerceEvent(13),

	/** The Product promo code. */
	ProductPromoCode(14),

	/** The Department metric. */
	DepartmentMetric(15),

	/** The Rental car. */
	RentalCar(16),

	/** The User. */
	User(17),

	/** The Training expectations. */
	TrainingExpectations(18),

	/** The Training certifications. */
	TrainingCertifications(19),

	/** The Progress report. */
	ProgressReport(20),

	/** The Base line. */
	BaseLine(21),

	/** The One 2 one. */
	One2One(22),

	/** The Observation. */
	Observation(23),

	/** The Coaching. */
	Coaching(24),

	/** The Bulk export. */
	BulkExport(25),

	/** The Agent metric audit trail. */
	AgentMetricAuditTrail(26),

	/** The Product metric audit trail. */
	ProductMetricAuditTrail(27),

	/** The Virtual group. */
	VirtualGroup(28),

	/** The Sales basic. */
	SalesBasic(29),

	/** The Comments feed. */
	CommentsFeed(30),

	/** The Tenant product. */
	TenantProduct(31),

	/** The Tenant product category. */
	TenantProductCategory(32);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new entity type.
	 *
	 * @param value
	 *            the value
	 */
	private EntityType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the entity type
	 */
	public static EntityType fromInteger(final Integer value) {
		if (value == Tenant.value) {
			return Tenant;
		} else if (value == TenantLocation.value) {
			return TenantLocation;
		} else if (value == LocationMetric.value) {
			return LocationMetric;
		} else if (value == AgentMetric.value) {
			return AgentMetric;
		} else if (value == Product.value) {
			return Product;
		} else if (value == ProductMetric.value) {
			return ProductMetric;
		} else if (value == LocationGroup.value) {
			return LocationGroup;
		} else if (value == IncentivePlan.value) {
			return IncentivePlan;
		} else if (value == GroupProductCategory.value) {
			return GroupProductCategory;
		} else if (value == DiscoveryMaster.value) {
			return DiscoveryMaster;
		} else if (value == Country.value) {
			return Country;
		} else if (value == Region.value) {
			return Region;
		} else if (value == EcommerceCustomer.value) {
			return EcommerceCustomer;
		} else if (value == EcommerceEvent.value) {
			return EcommerceEvent;
		} else if (value == DepartmentMetric.value) {
			return DepartmentMetric;
		} else if (value == ProductPromoCode.value) {
			return ProductPromoCode;
		} else if (value == RentalCar.value) {
			return RentalCar;
		} else if (value == User.value) {
			return User;
		} else if (value == TrainingExpectations.value) {
			return TrainingExpectations;
		} else if (value == TrainingCertifications.value) {
			return TrainingCertifications;
		} else if (value == ProgressReport.value) {
			return ProgressReport;
		} else if (value == BaseLine.value) {
			return BaseLine;
		} else if (value == One2One.value) {
			return One2One;
		} else if (value == Observation.value) {
			return Observation;
		} else if (value == Coaching.value) {
			return Coaching;
		} else if (value == BulkExport.value) {
			return BulkExport;
		} else if (value == AgentMetricAuditTrail.value) {
			return AgentMetricAuditTrail;
		} else if (value == ProductMetricAuditTrail.value) {
			return ProductMetricAuditTrail;
		} else if (value == SalesBasic.value) {
			return SalesBasic;
		} else if (value == VirtualGroup.value) {
			return VirtualGroup;
		} else if (value == CommentsFeed.value) {
			return CommentsFeed;
		} else if (value == TenantProduct.value) {
			return TenantProduct;
		} else if (value == TenantProductCategory.value) {
			return TenantProductCategory;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Tenant.value) {
			return "Tenant";
		} else if (value == TenantLocation.value) {
			return "Tenant Location";
		} else if (value == LocationMetric.value) {
			return "Location Metric";
		} else if (value == AgentMetric.value) {
			return "Agent Metric";
		} else if (value == Product.value) {
			return "Product";
		} else if (value == ProductMetric.value) {
			return "Product Metric";
		} else if (value == LocationGroup.value) {
			return "Location Group";
		} else if (value == IncentivePlan.value) {
			return "Incentive Plan";
		} else if (value == GroupProductCategory.value) {
			return "Group Product Category";
		} else if (value == DiscoveryMaster.value) {
			return "Discovery Master";
		} else if (value == Country.value) {
			return "Country";
		} else if (value == Region.value) {
			return "Region";
		} else if (value == EcommerceCustomer.value) {
			return "Ecommerce Customer";
		} else if (value == EcommerceEvent.value) {
			return "Ecommerce Event";
		} else if (value == DepartmentMetric.value) {
			return "Department Metric";
		} else if (value == ProductPromoCode.value) {
			return "Product Promo Code";
		} else if (value == RentalCar.value) {
			return "Rental Car";
		} else if (value == User.value) {
			return "User";
		} else if (value == TrainingExpectations.value) {
			return "Training Expectations";
		} else if (value == TrainingCertifications.value) {
			return "Training Certifications";
		} else if (value == ProgressReport.value) {
			return "Progress Report";
		} else if (value == BaseLine.value) {
			return "BaseLine";
		} else if (value == One2One.value) {
			return "One2One";
		} else if (value == Observation.value) {
			return "Observation";
		} else if (value == Coaching.value) {
			return "Coaching";
		} else if (value == BulkExport.value) {
			return "Bulk Export";
		} else if (value == AgentMetricAuditTrail.value) {
			return "AgentMetric AuditTrail";
		} else if (value == ProductMetricAuditTrail.value) {
			return "ProductMetric AuditTrail";
		} else if (value == SalesBasic.value) {
			return "Sales Basic";
		} else if (value == VirtualGroup.value) {
			return "Virtual Group";
		} else if (value == CommentsFeed.value) {
			return "Comments Feed";
		} else if (value == TenantProduct.value) {
			return "Tenant Product";
		} else if (value == TenantProductCategory.value) {
			return "Tenant Product Category";
		}
		return null;
	}

}
