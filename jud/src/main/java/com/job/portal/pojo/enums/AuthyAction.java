/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum AuthyAction.
 */
public enum AuthyAction {

	/** The registeruser. */
	REGISTERUSER(0),

	/** The deleteuser. */
	DELETEUSER(1),

	/** The verifytoken. */
	VERIFYTOKEN(2),

	/** The requestsms. */
	REQUESTSMS(3);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new authy action.
	 *
	 * @param value
	 *            the value
	 */
	private AuthyAction(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the authy action
	 */
	public static AuthyAction fromInteger(final Integer value) {
		if (value == REGISTERUSER.value) {
			return REGISTERUSER;
		} else if (value == DELETEUSER.value) {
			return DELETEUSER;
		} else if (value == VERIFYTOKEN.value) {
			return VERIFYTOKEN;
		} else if (value == REQUESTSMS.value) {
			return REQUESTSMS;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == REGISTERUSER.value) {
			return "REGISTER_USER";
		} else if (value == DELETEUSER.value) {
			return "DELETE_USER";
		} else if (value == REQUESTSMS.value) {
			return "REQUEST_SMS";
		} else if (value == VERIFYTOKEN.value) {
			return "VERIFY_TOKEN";
		}
		return null;
	}

}
