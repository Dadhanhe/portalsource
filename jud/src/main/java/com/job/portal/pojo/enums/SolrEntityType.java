/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum SolrEntityType.
 */
/**
 * @author bhagtabh
 *
 */
public enum SolrEntityType implements Serializable {

	/** The Tenant. */
	Tenant(0),

	/** The Tenant location. */
	TenantLocation(1),

	/** The Location metric. */
	LocationMetric(2),

	/** The Agent metric. */
	AgentMetric(3),

	/** The Product. */
	Product(4),

	/** The Product metric. */
	ProductMetric(5),

	/** The Location group. */
	LocationGroup(6),

	/** The Incentive plan. */
	IncentivePlan(7),

	/** The Group product category. */
	GroupProductCategory(8),

	/** The Discovery master. */
	DiscoveryMaster(9),

	/** The Country. */
	Country(10),

	/** The Region. */
	Region(11),

	/** The Ecommerce customer. */
	EcommerceCustomer(12),

	/** The Ecommerce event. */
	EcommerceEvent(13),

	/** The Product promo code. */
	ProductPromoCode(14),

	/** The Department metric. */
	DepartmentMetric(15),

	/** The user. */
	USER(16),

	/** The role. */
	ROLE(17),

	/** The Field scheme. */
	FieldScheme(18),

	/** The Reminder. */
	Reminder(19),

	/** The Timer task. */
	TimerTask(20),

	/** The Survey. */
	Survey(21),

	/** The Questionnaire. */
	Questionnaire(22),

	/** The Tenant report. */
	TenantReport(23),

	/** The Question. */
	Question(24),

	/** The Question answer. */
	QuestionAnswer(25),

	/** The Baseline. */
	Baseline(26),

	/** The Target. */
	Target(27),

	/** The Survey category. */
	SurveyCategory(28),

	/** The Email log. */
	EmailLog(29),

	/** The Preference. */
	Preference(30),

	/** The Invitation. */
	Invitation(31),

	/** The Location group product. */
	LocationGroupProduct(32),

	/** The Verdict. */
	Verdict(33),

	/** The Tenant id. */
	TenantId(34),

	/** The Media. */
	Media(35),

	/** The Agent metric audit trail. */
	AgentMetricAuditTrail(36),

	/** The Location group type. */
	LocationGroupType(37),

	/** The Tenant product. */
	TenantProduct(38),

	/** The Tenant product category. */
	TenantProductCategory(39),

	/** The Region type. */
	RegionType(40),

	/** The One to one type. */
	OneToOneType(41),

	/** The One to one. */
	OneToOne(42),

	/** The One to one user. */
	OneToOneUser(43),

	/** The Media favourite. */
	MediaFavourite(44),

	/** The User media access. */
	UserMediaAccess(45),

	/** The Goal. */
	Goal(46),

	/** The Industry menu. */
	IndustryMenu(47),

	/** The Industry route. */
	IndustryRoute(48),

	/** The Feedback. */
	Feedback(50),

	/** The Product metric audit trail. */
	ProductMetricAuditTrail(51),

	/** The Event log. */
	EventLog(52),

	/** The Calendar event. */
	CalendarEvent(53),

	/** The User message. */
	UserMessage(54),

	/** The Training course. */
	TrainingCourse(55),

	/** The Training category. */
	TrainingCategory(56),

	/** The Training chapter. */
	TrainingChapter(57),

	/** The Training watchdogs. */
	TrainingWatchdogs(58),

	/** The Training recommend. */
	TrainingRecommend(59),

	/** The Training favourite. */
	TrainingFavourite(60),

	/** The Training video notes. */
	TrainingVideoNotes(61),

	/** The Training questionnaire. */
	TrainingQuestionnaire(62),

	/** The Conduct training summary. */
	ConductTrainingSummary(63),

	/** The Conduct training answer. */
	ConductTrainingAnswer(64),

	/** The Training user certificates. */
	TrainingUserCertificates(65);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new solr entity type.
	 *
	 * @param val
	 *            the val
	 */
	private SolrEntityType(final int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

}
