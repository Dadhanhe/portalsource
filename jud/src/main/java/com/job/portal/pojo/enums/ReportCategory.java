/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum ReportCategory.
 */
public enum ReportCategory {

	/** The General. */
	General(0),

	/** The Target. */
	Target(1),

	/** The Forecasting. */
	Forecasting(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the report category
	 */
	public static ReportCategory fromInteger(Integer value) {
		if (value == General.value) {
			return General;
		} else if (value == Target.value) {
			return Target;
		} else if (value == Forecasting.value) {
			return Forecasting;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new report category.
	 *
	 * @param value
	 *            the value
	 */
	private ReportCategory(int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("report.category.%d", value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return value;
	}
}
