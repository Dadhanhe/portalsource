/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum RecommendedGoalAutomationCycle.
 */
public enum RecommendedGoalAutomationCycle {

	/** The Day 1. */
	Day1(1),

	/** The Day 2. */
	Day2(2),

	/** The Day 3. */
	Day3(3),

	/** The Day 4. */
	Day4(4),

	/** The Day 5. */
	Day5(5),

	/** The Day 6. */
	Day6(6),

	/** The Day 7. */
	Day7(7),

	/** The Day 8. */
	Day8(8),

	/** The Day 9. */
	Day9(9),

	/** The Day 10. */
	Day10(10),

	/** The Day 11. */
	Day11(11),

	/** The Day 12. */
	Day12(12),

	/** The Day 13. */
	Day13(13),

	/** The Day 14. */
	Day14(14),

	/** The Day 15. */
	Day15(15),

	/** The Day 16. */
	Day16(16),

	/** The Day 17. */
	Day17(17),

	/** The Day 18. */
	Day18(18),

	/** The Day 19. */
	Day19(19),

	/** The Day 20. */
	Day20(20),

	/** The Day 21. */
	Day21(21),

	/** The Day 22. */
	Day22(22),

	/** The Day 23. */
	Day23(23),

	/** The Day 24. */
	Day24(24),

	/** The Day 25. */
	Day25(25),

	/** The Day 26. */
	Day26(26),

	/** The Day 27. */
	Day27(27),

	/** The Last day of month. */
	LastDayOfMonth(28);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new recommended goal automation cycle.
	 *
	 * @param value
	 *            the value
	 */
	private RecommendedGoalAutomationCycle(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * Actual name.
	 *
	 * @return the string
	 */
	public String actualName() {
		if (this.value == Day1.value) {
			return "1";
		} else if (this.value == Day2.value) {
			return "2";
		} else if (this.value == Day3.value) {
			return "3";
		} else if (this.value == Day4.value) {
			return "4";
		} else if (this.value == Day5.value) {
			return "5";
		} else if (this.value == Day6.value) {
			return "6";
		} else if (this.value == Day7.value) {
			return "7";
		} else if (this.value == Day8.value) {
			return "8";
		} else if (this.value == Day9.value) {
			return "9";
		} else if (this.value == Day10.value) {
			return "10";
		} else if (this.value == Day11.value) {
			return "11";
		} else if (this.value == Day12.value) {
			return "12";
		} else if (this.value == Day13.value) {
			return "13";
		} else if (this.value == Day14.value) {
			return "14";
		} else if (this.value == Day15.value) {
			return "15";
		} else if (this.value == Day16.value) {
			return "16";
		} else if (this.value == Day17.value) {
			return "17";
		} else if (this.value == Day18.value) {
			return "18";
		} else if (this.value == Day19.value) {
			return "19";
		} else if (this.value == Day20.value) {
			return "20";
		} else if (this.value == Day21.value) {
			return "21";
		} else if (this.value == Day22.value) {
			return "22";
		} else if (this.value == Day23.value) {
			return "23";
		} else if (this.value == Day24.value) {
			return "24";
		} else if (this.value == Day25.value) {
			return "25";
		} else if (this.value == Day26.value) {
			return "26";
		} else if (this.value == Day27.value) {
			return "27";
		} else if (this.value == LastDayOfMonth.value) {
			return "Last day of month";
		} else {
			return "None";
		}
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the recommended goal automation cycle
	 */
	public static RecommendedGoalAutomationCycle fromInteger(final int value) {
		if (value == Day1.value) {
			return Day1;
		} else if (value == Day2.value) {
			return Day2;
		} else if (value == Day3.value) {
			return Day3;
		} else if (value == Day4.value) {
			return Day4;
		} else if (value == Day5.value) {
			return Day5;
		} else if (value == Day6.value) {
			return Day6;
		} else if (value == Day7.value) {
			return Day7;
		} else if (value == Day8.value) {
			return Day8;
		} else if (value == Day9.value) {
			return Day9;
		} else if (value == Day10.value) {
			return Day10;
		} else if (value == Day11.value) {
			return Day11;
		} else if (value == Day12.value) {
			return Day12;
		} else if (value == Day13.value) {
			return Day13;
		} else if (value == Day14.value) {
			return Day14;
		} else if (value == Day15.value) {
			return Day15;
		} else if (value == Day16.value) {
			return Day16;
		} else if (value == Day17.value) {
			return Day17;
		} else if (value == Day18.value) {
			return Day18;
		} else if (value == Day19.value) {
			return Day19;
		} else if (value == Day20.value) {
			return Day20;
		} else if (value == Day21.value) {
			return Day21;
		} else if (value == Day22.value) {
			return Day22;
		} else if (value == Day23.value) {
			return Day23;
		} else if (value == Day24.value) {
			return Day24;
		} else if (value == Day25.value) {
			return Day25;
		} else if (value == Day26.value) {
			return Day26;
		} else if (value == Day27.value) {
			return Day27;
		} else if (value == LastDayOfMonth.value) {
			return LastDayOfMonth;
		}
		return null;
	}
}
