/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum GoalProductType.
 */
public enum GoalProductType {

	/** The product. */
	PRODUCT(0),

	/** The ir. */
	IR(1),

	/** The idr. */
	IDR(2), // Not used in CR

	/** The conversionrate. */
	CONVERSIONRATE(3),

	/** The metric. */
	METRIC(4);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new goal product type.
	 *
	 * @param value
	 *            the value
	 */
	private GoalProductType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the goal product type
	 */
	public static GoalProductType fromInteger(final Integer value) {
		if (value == PRODUCT.value) {
			return PRODUCT;
		} else if (value == IR.value) {
			return IR;
		} else if (value == IDR.value) {
			return IDR;
		} else if (value == CONVERSIONRATE.value) {
			return CONVERSIONRATE;
		} else if (value == METRIC.value) {
			return METRIC;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == PRODUCT.value) {
			return "Product";
		} else if (value == IR.value) {
			return "IR";
		} else if (value == CONVERSIONRATE.value) {
			return "ConversionRate";
		} else if (value == IDR.value) {
			return "IDR";
		} else if (value == METRIC.value) {
			return "Metric";
		}
		return null;
	}

	/**
	 * Gets the value.
	 *
	 * @param code
	 *            the code
	 * @return the value
	 */
	public static int getValue(final String code) {
		for (int i = 0; i < GoalProductType.values().length; i++) {
			if (code.equalsIgnoreCase(GoalProductType.values()[i].name())) {
				return GoalProductType.values()[i].value();
			}
		}
		return -1;
	}

}
