/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Sort.
 */
public enum Sort {

	/** The asc. */
	ASC(0),

	/** The desc. */
	DESC(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new sort.
	 *
	 * @param value
	 *            the value
	 */
	private Sort(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static Sort getEnumName(final String value) {
		if (value.equalsIgnoreCase("ASC")) {
			return Sort.ASC;
		} else if (value.equalsIgnoreCase("DESC")) {
			return Sort.DESC;
		}
		return null;
	}

}