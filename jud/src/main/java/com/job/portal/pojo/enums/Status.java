/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Status.
 */
public enum Status {

	/** The Normal. */
	Active(0),

	/** The Blocked. */
	Blocked(1),

	/** The Deleted. */
	Deleted(2),

	/** The Deactivated. */
	Deactive(3),

	/** The Pending. */
	Pending(4),

	/** The Approved. */
	Approved(5),

	/** The Archive. */
	Archive(6),

	/** The Onboarding. */
	Ready_to_onboard(7),

	/** The Start onboarding. */
	Onboarding(8);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the status
	 */
	public static Status fromInteger(final Integer value) {
		if (value == Active.value) {
			return Active;
		} else if (value == Blocked.value) {
			return Blocked;
		} else if (value == Deleted.value) {
			return Deleted;
		} else if (value == Deactive.value) {
			return Deactive;
		} else if (value == Pending.value) {
			return Pending;
		} else if (value == Approved.value) {
			return Approved;
		} else if (value == Archive.value) {
			return Archive;
		} else if (value == Ready_to_onboard.value) {
			return Ready_to_onboard;
		} else if (value == Onboarding.value) {
			return Onboarding;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new status.
	 *
	 * @param value
	 *            the value
	 */
	private Status(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Active.value) {
			return "Active";
		} else if (value == Blocked.value) {
			return "Blocked";
		} else if (value == Deleted.value) {
			return "Deleted";
		} else if (value == Deactive.value) {
			return "Deactive";
		} else if (value == Pending.value) {
			return "Pending";
		} else if (value == Approved.value) {
			return "Approved";
		} else if (value == Archive.value) {
			return "Archive";
		} else if (value == Ready_to_onboard.value) {
			return "Ready_to_onboard";
		} else if (value == Onboarding.value) {
			return "Onboarding";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static Status getEnum(final String status) {
		if (status != null) {
			if (status.equals("Active")) {
				return Status.Active;
			} else if (status.equals("Blocked")) {
				return Status.Blocked;
			} else if (status.equals("Deleted")) {
				return Status.Deleted;
			} else if (status.equals("Deactive")) {
				return Status.Deactive;
			} else if (status.equals("Pending")) {
				return Status.Pending;
			} else if (status.equals("Approved")) {
				return Status.Approved;
			} else if (status.equals("Archive")) {
				return Status.Archive;
			} else if (status.equals("Ready_to_onboard")) {
				return Status.Ready_to_onboard;
			} else if (status.equals("Onboarding")) {
				return Status.Onboarding;
			}
		}
		return null;
	}
}
