/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

// TODO: Auto-generated Javadoc
/**
 * The Class Token.
 */
public class Token {

	/** The access token. */
	String access_token;

	/** The expires in. */
	Double expires_in;

	/** The refresh token. */
	String refresh_token;

	/** The scope. */
	String scope;

	/** The token type. */
	String token_type;

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	public String getAccess_token() {
		return this.access_token;
	}

	/**
	 * Sets the access token.
	 *
	 * @param access_token
	 *            the new access token
	 */
	public void setAccess_token(final String access_token) {
		this.access_token = access_token;
	}

	/**
	 * Gets the expires in.
	 *
	 * @return the expires in
	 */
	public Double getExpires_in() {
		return this.expires_in;
	}

	/**
	 * Sets the expires in.
	 *
	 * @param expires_in
	 *            the new expires in
	 */
	public void setExpires_in(final Double expires_in) {
		this.expires_in = expires_in;
	}

	/**
	 * Gets the refresh token.
	 *
	 * @return the refresh token
	 */
	public String getRefresh_token() {
		return this.refresh_token;
	}

	/**
	 * Sets the refresh token.
	 *
	 * @param refresh_token
	 *            the new refresh token
	 */
	public void setRefresh_token(final String refresh_token) {
		this.refresh_token = refresh_token;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return this.scope;
	}

	/**
	 * Sets the scope.
	 *
	 * @param scope
	 *            the new scope
	 */
	public void setScope(final String scope) {
		this.scope = scope;
	}

	/**
	 * Gets the token type.
	 *
	 * @return the token type
	 */
	public String getToken_type() {
		return this.token_type;
	}

	/**
	 * Sets the token type.
	 *
	 * @param token_type
	 *            the new token type
	 */
	public void setToken_type(final String token_type) {
		this.token_type = token_type;
	}

}
