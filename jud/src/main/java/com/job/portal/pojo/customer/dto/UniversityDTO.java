/*
 * Copyright 2018  Frontline Performance Group  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Frontline Performance Group. The intellectual
 * and technical concepts contained herein are proprietary to Frontline Performance Group
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Frontline Performance Group.
 */
package com.job.portal.pojo.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;

// TODO: Auto-generated Javadoc
/**
 * The Class UniversityDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class UniversityDTO extends AbstractDTO {

	/** The name. */
	private String name;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

}
