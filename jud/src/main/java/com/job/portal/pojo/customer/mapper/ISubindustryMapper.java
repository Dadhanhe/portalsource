/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Subindustry;
import com.job.portal.pojo.customer.dto.SubindustryDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserDTOMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ISubindustryMapper
		extends IAbstractMapper<Subindustry, SubindustryDTO> {

	/**
	 * To entity.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Override

	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	@Mapping(target = "industry.id", source = "dto.industryId")
	@Mapping(target = "industry.name", source = "dto.industryName")
	Subindustry toEntity(SubindustryDTO dto);

	/**
	 * To dto.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO
	 */
	@Override
	@Mapping(target = "createdById", source = "entity.createdBy.id")
	@Mapping(target = "updatedById", source = "entity.updatedBy.id")
	@Mapping(target = "industryId", source = "entity.industry.id")
	@Mapping(target = "industryName", source = "entity.industry.name")
	SubindustryDTO toDto(Subindustry entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final SubindustryDTO dto,
			@MappingTarget final Subindustry entity) {
		checkCreatedByUpdatedBy(dto, entity);
	}

	/**
	 * Map plain.
	 *
	 * @param dto
	 *            the dto
	 * @return the user plain
	 */
	Subindustry toEntityPlain(SubindustryDTO dto);

	/**
	 * Map 2.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	Subindustry toEntity2(SubindustryDTO dto);

	/**
	 * Map 2.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO 2
	 */
	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	SubindustryDTO toDto2(Subindustry entity);

}
