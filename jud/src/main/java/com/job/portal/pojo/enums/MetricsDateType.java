/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum MetricsDateType.
 */
public enum MetricsDateType {

	/** The Arrival. */
	Arrival(0),

	/** The Daily. */
	Daily(1),

	/** The Departure. */
	Departure(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the metrics date type
	 */
	public static MetricsDateType fromInteger(final Integer value) {
		if (value == Arrival.value) {
			return Arrival;
		} else if (value == Daily.value) {
			return Daily;
		} else if (value == Departure.value) {
			return Departure;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new metrics date type.
	 *
	 * @param value
	 *            the value
	 */
	private MetricsDateType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("report.hotelMetricsDataType.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer to string.
	 *
	 * @param value
	 *            the value
	 * @return the string
	 */
	public static String fromIntegerToString(final Integer value) {
		if (value == Arrival.value) {
			return "Arrival";
		} else if (value == Daily.value) {
			return "Daily";
		} else if (value == Departure.value) {
			return "Departure";
		}
		return null;
	}

}
