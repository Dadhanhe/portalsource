/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import com.job.portal.pojo.common.dto.AbstractDTO;

public class DisabilityDTO extends AbstractDTO {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The name. */
	String name;

	/** The board type. */
	String boardType;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the board type.
	 *
	 * @return the board type
	 */
	public String getBoardType() {
		return this.boardType;
	}

	/**
	 * Sets the board type.
	 *
	 * @param boardType
	 *            the new board type
	 */
	public void setBoardType(final String boardType) {
		this.boardType = boardType;
	}

}
