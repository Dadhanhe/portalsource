/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum ProductMetricsValidationType.
 */
public enum ProductMetricsValidationType {

	/** The arrival date. */
	ARRIVAL_DATE(0),

	/** The business date. */
	BUSINESS_DATE(1),

	/** The departure date. */
	DEPARTURE_DATE(2);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new product metrics validation type.
	 *
	 * @param value
	 *            the value
	 */
	private ProductMetricsValidationType(int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the product metrics validation type
	 */
	public static ProductMetricsValidationType fromInteger(Integer value) {
		if (value == BUSINESS_DATE.value) {
			return BUSINESS_DATE;
		} else if (value == ARRIVAL_DATE.value) {
			return ARRIVAL_DATE;
		} else if (value == DEPARTURE_DATE.value) {
			return DEPARTURE_DATE;
		}
		return null;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("report.productMetricsValidationType.%d", value);
	}
}
