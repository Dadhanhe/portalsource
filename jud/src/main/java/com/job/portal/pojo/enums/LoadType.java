/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonValue;

// TODO: Auto-generated Javadoc
/**
 * The Enum LoadType.
 */
public enum LoadType implements Serializable {

	/** The load. */
	LOAD(0);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new load type.
	 *
	 * @param val
	 *            the val
	 */
	private LoadType(int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	@JsonValue
	public int getValue() {
		return this.value;
	}
}
