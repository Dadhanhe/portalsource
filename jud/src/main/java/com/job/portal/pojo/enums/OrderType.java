/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum OrderType.
 */
public enum OrderType implements Serializable {

	/** The type. */
	TYPE(0);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new order type.
	 *
	 * @param val
	 *            the val
	 */
	private OrderType(int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}
}
