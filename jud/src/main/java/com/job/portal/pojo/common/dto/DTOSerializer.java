/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.job.portal.config.PersistenceConfig;
import com.job.portal.pojo.common.Converter;

// TODO: Auto-generated Javadoc
// Created by Sapan on 28/11/16.
/**
 * The Class DTOSerializer.
 */

public class DTOSerializer extends JsonDeserializer<AbstractDTO> {

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(DTOSerializer.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml
	 * .jackson.core.JsonParser,
	 * com.fasterxml.jackson.databind.DeserializationContext)
	 */
	@SuppressWarnings("static-access")
	@Override
	public AbstractDTO deserialize(final JsonParser jsonParser,
			final DeserializationContext deserializationContext)
			throws IOException, JsonProcessingException {
		final Converter converter = PersistenceConfig.SYSTEM_APPLICATION_CONTEXT
				.getBean(Converter.class);
		this.logger.debug("Into serializer ");
		final JsonNode node = jsonParser.getCodec().readTree(jsonParser);
		node.toString();
		final Boolean hasType = node.has("mapTo");
		if (!hasType) {
			throw new IOException(
					"Incorrect input. attribute 'mapTo' is mandatory");
		}
		final String c = node.get("mapTo").asText();
		@SuppressWarnings("rawtypes")
		final Class classType = converter.getDeserializerMapping(c);
		final Gson gson = converter.getGson();
		@SuppressWarnings("unchecked")
		final AbstractDTO dto = (AbstractDTO) gson.fromJson(node.toString(),
				classType);
		dto.setMapTo(c);
		this.logger.debug("Returning " + dto);
		return dto;
	}

}
