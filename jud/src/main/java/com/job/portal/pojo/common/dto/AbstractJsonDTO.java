/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import java.util.Map;

import org.apache.solr.client.solrj.beans.Field;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractJsonDTO.
 */

public abstract class AbstractJsonDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -843464619687954048L;

	/** The json data. */
	@Field
	protected String jsonData;

	/** The json map. */
	protected transient Map<String, Object> jsonMap;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getJsonMap()
	 */
	@Override
	public Map<String, Object> getJsonMap() {
		return this.jsonMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#setJsonMap(java.util.Map)
	 */
	@Override
	public void setJsonMap(final Map<String, Object> jsonMap) {
		this.jsonMap = jsonMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getJsonData()
	 */
	@Override
	public String getJsonData() {
		return this.jsonData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setJsonData(java.lang.String)
	 */
	@Override
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}
}
