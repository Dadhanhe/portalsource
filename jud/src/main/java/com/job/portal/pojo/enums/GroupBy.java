/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum GroupBy.
 */
public enum GroupBy {

	/** The Date range. */
	DateRange(1),

	/** The Year. */
	Year(2),

	/** The Month. */
	Month(3),

	/** The Quarter. */
	Quarter(4),

	/** The Date. */
	Date(5),

	/** The Week end. */
	WeekEnd(6),

	/** The Day of week. */
	DayOfWeek(7),

	/** The Week of year. */
	WeekOfYear(8),

	/** The Other. */
	Other(9),

	/** The mtd. */
	MTD(10),

	/** The ytd. */
	YTD(11);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new group by.
	 *
	 * @param value
	 *            the value
	 */
	private GroupBy(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

}
