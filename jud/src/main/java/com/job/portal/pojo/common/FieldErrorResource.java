/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */

package com.job.portal.pojo.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
//Created by Sapan on 30/03/16.
/**
 * The Class FieldErrorResource.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldErrorResource {

	/** The resource. */
	private String resource;

	/** The field. */
	private String field;

	/** The code. */
	private String code;

	/** The message. */
	private String message;

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public String getResource() {
		return this.resource;
	}

	/**
	 * Sets the resource.
	 *
	 * @param resource
	 *            the new resource
	 */
	public void setResource(final String resource) {
		this.resource = resource;
	}

	/**
	 * Gets the field.
	 *
	 * @return the field
	 */
	public String getField() {
		return this.field;
	}

	/**
	 * Sets the field.
	 *
	 * @param field
	 *            the new field
	 */
	public void setField(final String field) {
		this.field = field;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the new code
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}
}
