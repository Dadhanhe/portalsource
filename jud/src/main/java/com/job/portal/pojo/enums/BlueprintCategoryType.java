/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum BlueprintCategoryType.
 */
public enum BlueprintCategoryType {

	/** The Status. */
	Status(0),

	/** The Question answer. */
	QuestionAnswer(1);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the blueprint category type
	 */
	public static BlueprintCategoryType fromInteger(final Integer value) {
		if (value == Status.value) {
			return Status;
		} else if (value == QuestionAnswer.value) {
			return QuestionAnswer;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new blueprint category type.
	 *
	 * @param value
	 *            the value
	 */
	private BlueprintCategoryType(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("blueprintCategoryType.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("blueprintCategoryType.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Status.value) {
			return "Status";
		} else if (value == QuestionAnswer.value) {
			return "QuestionAnswer";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param categoryType
	 *            the category type
	 * @return the enum
	 */
	public static BlueprintCategoryType getEnum(final String categoryType) {
		if (categoryType != null) {
			if (categoryType.equals("Status")) {
				return BlueprintCategoryType.Status;
			} else if (categoryType.equals("QuestionAnswer")) {
				return BlueprintCategoryType.QuestionAnswer;
			}
		}
		return null;
	}

}
