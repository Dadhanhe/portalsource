/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.solr.client.solrj.beans.Field;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.utils.JsonDateTimeSerializer;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailLogDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class EmailLogDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1786792803583942892L;

	/** The attach file name. */
	@Field
	private String attachFileName;

	/** The attempts. */
	@Field
	private Integer attempts;

	/** The bcc recipients. */
	@Field
	private String bccRecipients;

	/** The cc recipients. */
	@Field
	private String ccRecipients;

	/** The contents. */
	@Field
	private String contents;

	/** The errors. */
	@Field
	private String errors;

	/** The recipients. */
	@Field
	private String recipients;

	/** The sent time. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date sentTime;

	/** The subject. */
	@Field
	private String subject;

	/** The time stamp. */
	@Field
	@Temporal(TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	private Date timeStamp;

	/** The email of. */
	@Field
	private String emailOf;

	/** The email status. */
	@Field
	private String emailStatus;

	/** The actual file name. */
	@Field
	private String actualFileName;

	/** The attachment ID. */
	@Field
	private Integer attachmentID;

	/** The token. */
	@Field
	private String token;

	/** The upload attach directory path. */
	private String uploadAttachDirectoryPath;

	/**
	 * Gets the email status.
	 *
	 * @return the email status
	 */
	public String getEmailStatus() {
		return this.emailStatus;
	}

	/**
	 * Sets the email status.
	 *
	 * @param emailStatus
	 *            the new email status
	 */
	public void setEmailStatus(final String emailStatus) {
		this.emailStatus = emailStatus;
	}

	/**
	 * Gets the attach file name.
	 *
	 * @return the attach file name
	 */
	public String getAttachFileName() {
		return this.attachFileName;
	}

	/**
	 * Gets the attempts.
	 *
	 * @return the attempts
	 */
	public Integer getAttempts() {
		return this.attempts;
	}

	/**
	 * Gets the bcc recipients.
	 *
	 * @return the bcc recipients
	 */
	public String getBccRecipients() {
		return this.bccRecipients;
	}

	/**
	 * Gets the cc recipients.
	 *
	 * @return the cc recipients
	 */
	public String getCcRecipients() {
		return this.ccRecipients;
	}

	/**
	 * Gets the contents.
	 *
	 * @return the contents
	 */
	public String getContents() {
		return this.contents;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getCreatedOn()
	 */
	@Override
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public String getErrors() {
		return this.errors;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	public Integer getId() {
		return this.id;
	}

	/**
	 * Gets the recipients.
	 *
	 * @return the recipients
	 */
	public String getRecipients() {
		return this.recipients;
	}

	/**
	 * Gets the sent time.
	 *
	 * @return the sent time
	 */
	public Date getSentTime() {
		return this.sentTime;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public Date getTimeStamp() {
		return this.timeStamp;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getUpdatedOn()
	 */
	@Override
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/**
	 * Sets the attach file name.
	 *
	 * @param attachFileName
	 *            the new attach file name
	 */
	public void setAttachFileName(final String attachFileName) {
		this.attachFileName = attachFileName;
	}

	/**
	 * Sets the attempts.
	 *
	 * @param attempts
	 *            the new attempts
	 */
	public void setAttempts(final Integer attempts) {
		this.attempts = attempts;
	}

	/**
	 * Sets the bcc recipients.
	 *
	 * @param bccRecipients
	 *            the new bcc recipients
	 */
	public void setBccRecipients(final String bccRecipients) {
		this.bccRecipients = bccRecipients;
	}

	/**
	 * Sets the cc recipients.
	 *
	 * @param ccRecipients
	 *            the new cc recipients
	 */
	public void setCcRecipients(final String ccRecipients) {
		this.ccRecipients = ccRecipients;
	}

	/**
	 * Sets the contents.
	 *
	 * @param contents
	 *            the new contents
	 */
	public void setContents(final String contents) {
		this.contents = contents;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setCreatedOn(java.util.Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Sets the errors.
	 *
	 * @param errors
	 *            the new errors
	 */
	public void setErrors(final String errors) {
		this.errors = errors;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Sets the recipients.
	 *
	 * @param recipients
	 *            the new recipients
	 */
	public void setRecipients(final String recipients) {
		this.recipients = recipients;
	}

	/**
	 * Sets the sent time.
	 *
	 * @param sentTime
	 *            the new sent time
	 */
	public void setSentTime(final Date sentTime) {
		this.sentTime = sentTime;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject
	 *            the new subject
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param timeStamp
	 *            the new time stamp
	 */
	public void setTimeStamp(final Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setUpdatedOn(java.util.Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * Gets the email of.
	 *
	 * @return the email of
	 */
	public String getEmailOf() {
		return this.emailOf;
	}

	/**
	 * Sets the email of.
	 *
	 * @param emailOf
	 *            the new email of
	 */
	public void setEmailOf(final String emailOf) {
		this.emailOf = emailOf;
	}

	/**
	 * Gets the actual file name.
	 *
	 * @return the actual file name
	 */
	public String getActualFileName() {
		return this.actualFileName;
	}

	/**
	 * Sets the actual file name.
	 *
	 * @param actualFileName
	 *            the new actual file name
	 */
	public void setActualFileName(final String actualFileName) {
		this.actualFileName = actualFileName;
	}

	/**
	 * Gets the attachment ID.
	 *
	 * @return the attachment ID
	 */
	public Integer getAttachmentID() {
		return this.attachmentID;
	}

	/**
	 * Sets the attachment ID.
	 *
	 * @param attachmentID
	 *            the new attachment ID
	 */
	public void setAttachmentID(final Integer attachmentID) {
		this.attachmentID = attachmentID;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return this.token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token
	 *            the new token
	 */
	public void setToken(final String token) {
		this.token = token;
	}

	/**
	 * Gets the upload attach directory path.
	 *
	 * @return the upload attach directory path
	 */
	public String getUploadAttachDirectoryPath() {
		return this.uploadAttachDirectoryPath;
	}

	/**
	 * Sets the upload attach directory path.
	 *
	 * @param uploadAttachDirectoryPath
	 *            the new upload attach directory path
	 */
	public void setUploadAttachDirectoryPath(
			final String uploadAttachDirectoryPath) {
		this.uploadAttachDirectoryPath = uploadAttachDirectoryPath;
	}

}
