/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.common;

import static javax.persistence.GenerationType.IDENTITY;

// Generated Oct 5, 2016 2:02:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.enums.PreferenceModule;
import com.job.portal.pojo.enums.PreferenceType;
import com.job.portal.pojo.enums.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class Preference.
 */

/**
 * @author shahhard
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "preference")
public class Preference extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8741415662347310366L;

	/** The Constant ADMIN_EMAIL. */
	public static final String ADMIN_EMAIL = "system.admin.email";

	/** The Constant SYSTEM_BASE_URL. */
	public static final String SYSTEM_BASE_URL = "system.base.url";

	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name";

	/** The Constant SYSTEM_HOME. */
	public static final String SYSTEM_HOME = "system.home";

	/** The Constant SYSTEM_TEMP. */
	public static final String SYSTEM_TEMP = "system.temp";

	/** The Constant SYSTEM_ATTACHMENTS. */
	public static final String SYSTEM_ATTACHMENTS = "system.attachments";

	/** The Constant UPLOADED_TO_SYSTEM. */
	public static final String UPLOADED_TO_SYSTEM = "system.uploadedToSystem";

	/** The Constant SYSTEM_IMPORT_DATA_HOME. */
	public static final String SYSTEM_IMPORT_DATA_HOME = "system.importDataHome";

	/** The Constant IMPORT_DATA_ATTACHMENTS_CUSTOM_COMPLETED. */
	public static final String IMPORT_DATA_ATTACHMENTS_CUSTOM_COMPLETED = "system.importDataAttachments.customCompleted";

	/** The Constant IMPORT_DATA_ATTACHMENTS_CUSTOM_UPLOADED. */
	public static final String IMPORT_DATA_ATTACHMENTS_CUSTOM_UPLOADED = "system.importDataAttachments.customUploaded";

	/** The Constant IMPORT_DATA_ATTACHMENTS_ORIGINAL_PROCESSED. */
	public static final String IMPORT_DATA_ATTACHMENTS_ORIGINAL_PROCESSED = "system.importDataAttachments.originalProcessed";

	/** The Constant IMPORT_DATA_ATTACHMENTS_ORIGINAL_DOWNLOADED. */
	public static final String IMPORT_DATA_ATTACHMENTS_ORIGINAL_DOWNLOADED = "system.importDataAttachments.originalDownloaded";

	/** The Constant SYSTEM_SFTP. */
	public static final String SYSTEM_SFTP = "system.sftp";

	/** The Constant SYSTEM_PROCESSEDFILE. */
	public static final String SYSTEM_PROCESSEDFILE = "system.processedfile";

	/** The Constant SYSTEM_ERRORS. */
	public static final String SYSTEM_ERRORS = "system.error";

	/** The Constant HILTON_IMPORT_DATA_ATTACHMENTS. */
	public static final String HILTON_IMPORT_DATA_ATTACHMENTS = "system.hilton.importDataAttachements";

	/** The Constant SYSTEM_INDUSTRY_TYPE. */
	public static final String SYSTEM_INDUSTRY_TYPE = "system.industry.type";

	/** The Constant INDUSTRY_TYPE_ANALYTICS_ID. */
	public static final String INDUSTRY_TYPE_ANALYTICS_ID = "industry.type.analytics.id";

	/** The Constant SYSTEM_AVATAR. */
	public static final String SYSTEM_AVATAR = "system.avatars";

	// IN-1227
	/** The Constant SYSTEM_AVATAR. */
	public static final String SYSTEM_LOGO = "system.logos";
	// END

	// TODO:changed for feed
	/** Feed Image folder which stores images uploaded by feed. */
	public static final String SYSTEM_FEEDIMAGE = "system.feedImages";

	/** Feed media folder which stores videos uploaded by feed. */
	public static final String SYSTEM_FEEDVIDEO = "system.feedVideo";

	/** The Constant SYSTEM_REPORT_TEMPLATES. */
	public static final String SYSTEM_REPORT_TEMPLATES = "system.report.templates";

	/** The Constant SYSTEM_UPLOAD_ATTACHMENTS. */
	public static final String SYSTEM_UPLOAD_ATTACHMENTS = "system.upload.attachments";

	/** The Constant SYSTEM_ADMIN_EMAIL. */
	public static final String SYSTEM_ADMIN_EMAIL = "system.admin.email";

	/** The Constant SYSTEM_FEEDBACK_EMAIL. */
	public static final String SYSTEM_FEEDBACK_EMAIL = "system.feedback.email";

	/** The Constant SYSTEM_FAILURE_EMAIL. */
	public static final String SYSTEM_FAILURE_EMAIL = "system.failure.email";

	/** The Constant MAIL_SMTP_HOST. */
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";

	/** The Constant MAIL_SMTP_PORT. */
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";

	/** The Constant MAIL_SENDER. */
	public static final String MAIL_SENDER = "mail.sender";

	/** The Constant MAIL_DISPLAY_NAME. */
	public static final String MAIL_DISPLAY_NAME = "mail.display.name";

	/** The Constant MAIL_USER_NAME. */
	public static final String MAIL_USER_NAME = "mail.user.name";

	/** The Constant MAIL_USER_PASS. */
	public static final String MAIL_USER_PASS = "mail.user.pwd";

	/** The Constant MAIL_BCC. */
	public static final String MAIL_BCC = "mail.bcc";

	/** The Constant MAIL_ENABLED. */
	public static final String MAIL_ENABLED = "mail.enabled";

	/** The Constant IS_EUROPECAR. */
	public static final String IS_EUROPECAR = "system.is.europecar";

	/** The Constant MAIL_TEST_SMTP_HOST. */
	public static final String MAIL_TEST_SMTP_HOST = "mail.test.smtp.host";

	/** The Constant MAIL_TEST_SMTP_PORT. */
	public static final String MAIL_TEST_SMTP_PORT = "mail.test.smtp.port";

	/** The Constant MAIL_TEST_SENDER. */
	public static final String MAIL_TEST_SENDER = "mail.test.sender";

	/** The Constant MAIL_TEST_USER_NAME. */
	public static final String MAIL_TEST_USER_NAME = "mail.test.user.name";

	/** The Constant MAIL_TEST_USER_PASS. */
	public static final String MAIL_TEST_USER_PASS = "mail.test.user.pwd";

	/** The Constant MAIL_TEST. */
	public static final String MAIL_TEST = "mail.test";

	/** The Constant VERSION. */
	public static final String VERSION = "version";

	/** The Constant BUILD. */
	public static final String BUILD = "build";

	/** The Constant UTILITY_MAIL_RECIPIENTS. */
	public static final String UTILITY_MAIL_RECIPIENTS = "system.utility.mailRecipients";
	// ?????????
	/** The Constant ANY_VERSION. */
	public static final String ANY_VERSION = "*";

	/** The Constant SYSTEM_PRODUCT_IMG. */
	public static final String SYSTEM_PRODUCT_IMG = "system.product.image.path";

	/** The Constant MAIL_ENABLED_PROMOCODE. */
	public static final String MAIL_ENABLED_PROMOCODE = "mail.procode";

	/** The Constant PROD_METRICS_CC_LIST. */
	public static final String PROD_METRICS_CC_LIST = "product.metrics.cc.list";

	/** The Constant PROD_METRICS_BCC_LIST. */
	public static final String PROD_METRICS_BCC_LIST = "product.metrics.bcc.list";

	/** The Constant FEEDBACK_EMAIL_LIST. */
	public static final String FEEDBACK_EMAIL_LIST = "feedback.email.list";

	/** The Constant EMAIL_SUBJECT_SUFFIX. */
	public static final String EMAIL_SUBJECT_SUFFIX = "email.subject.suffix";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_SENDER_ID. */
	public static final String SYSTEM_INTEGRATION_INTACCT_SENDER_ID = "system.integration.intacct.sender_id";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_SENDER_PASS_WORD. */
	public static final String SYSTEM_INTEGRATION_INTACCT_SENDER_PASS_WORD = "system.integration.intacct.sender_password";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_COMPANY_ID. */
	public static final String SYSTEM_INTEGRATION_INTACCT_COMPANY_ID = "system.integration.intacct.company_id";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_USER_ID. */
	public static final String SYSTEM_INTEGRATION_INTACCT_USER_ID = "system.integration.intacct.user_id";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_USER_PASS_WORD. */
	public static final String SYSTEM_INTEGRATION_INTACCT_USER_PASS_WORD = "system.integration.intacct.user_password";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_ENABLE_PROCESS. */
	public static final String SYSTEM_INTEGRATION_INTACCT_ENABLE_PROCESS = "system.integration.intacct.enableProcess";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_DISPLAY_DOCUMENT. */
	public static final String SYSTEM_INTEGRATION_INTACCT_DISPLAY_DOCUMENT = "system.integration.intacct.displayDocument";

	/** The Constant SYSTEM_INTEGRATION_INTACCT_SESSION_ID. */
	public static final String SYSTEM_INTEGRATION_INTACCT_SESSION_ID = "system.integration.intacct.sessionId";

	/** The Constant SYSTEM_INTEGRATION_ADAPTIVE_USER_ID. */
	public static final String SYSTEM_INTEGRATION_ADAPTIVE_USER_ID = "system.integration.adaptive.user_id";

	/** The Constant SYSTEM_INTEGRATION_ADAPTIVE_USER_PASS_WORD. */
	public static final String SYSTEM_INTEGRATION_ADAPTIVE_USER_PASS_WORD = "system.integration.adaptive.user_password";

	/** The Constant SYSTEM_INTEGRATION_ADAPTIVE_ENABLE_PROCESS. */
	public static final String SYSTEM_INTEGRATION_ADAPTIVE_ENABLE_PROCESS = "system.integration.adaptive.enableProcess";

	/** The Constant SYSTEM_REPORT_ACTIVATION_STATUS. */
	public static final String SYSTEM_REPORT_ACTIVATION_STATUS = "system.report.activation.status";

	/** The Constant SYSTEM_CLIENT_SIGNATURE. */
	public static final String SYSTEM_CLIENT_SIGNATURE = "system.client.signature.path";

	/** The Constant WISTIA_API_TOKEN. */
	public static final String WISTIA_API_TOKEN = "wistia.api.token";

	/** The Constant WISTIA_PROJECT_HASHEDID. */
	public static final String WISTIA_PROJECT_HASHEDID = "wistia.project.hashedid";

	/** The Constant WISTIA_PROJECT_HASHEDID FOR FEED VIDEOS. */
	public static final String FEEDVIDEOS_WISTIA_PROJECT_HASHEDID = "feedVideos.wistia.project.hashedid";

	/** The Constant SYSTEM_MAIL_ATTACHMENT. */
	public static final String SYSTEM_MAIL_ATTACHMENT = "system.attachments";

	/** The Constant OBSERVATION_COUNTERCOACHING_ONE2ONE_EXPECTED. */
	public static final String OBSERVATION_COUNTERCOACHING_ONE2ONE_EXPECTED = "observation.counterCoaching.one2one.expected";

	/** The Constant CERTIFICATE_PATH. */
	public static final String CERTIFICATE_PATH = "apns.cert.path";

	/** The Constant RUNTIME_ENV. */
	public static final String RUNTIME_ENV = "runtime.env";

	/** The Constant API_URL_FCM. */
	public static final String API_URL_FCM = "android.pn.api.url.fcm";

	/** The Constant AUTH_KEY_FCM. */
	public static final String AUTH_KEY_FCM = "android.pn.auth.key.fcm";

	/** The Constant METRICS_REC_LCK_CC_LIST. */
	public static final String METRICS_REC_LCK_CC_LIST = "metrics.rec.lck.cc.list";

	/** The Constant USER_REGISTER_MAIL_LIST. */
	public static final String USER_REGISTER_MAIL_LIST = "user.register.mail.list";

	/** The Constant STORED_PROCEDURE_VERIFICATION_ENABLE. */
	public static final String STORED_PROCEDURE_VERIFICATION_ENABLE = "stored.procedure.verification.enable";

	/** The Constant STORED_PROCEDURE_FAILURE_EMAIL_LIST. */
	public static final String STORED_PROCEDURE_FAILURE_EMAIL_LIST = "stored.procedure.failure.email.list";

	/** The Constant STORED_PROCEDURE_VERIFICATION_INTERVAL. */
	public static final String STORED_PROCEDURE_VERIFICATION_INTERVAL = "stored.procedure.verification.interval";

	/** The Constant DATA_UPLOAD_UTILITY_STATUS_FAILURE_EMAIL_LIST. */
	public static final String DATA_UPLOAD_UTILITY_STATUS_FAILURE_EMAIL_LIST = "data.upload.status.failure.email.list";

	/** The Constant DATA_UPLOAD_UTILITY_VERIFICATION_INTERVAL. */
	public static final String DATA_UPLOAD_UTILITY_VERIFICATION_INTERVAL = "data.upload.verification.interval";

	/** The Constant STORED_PROCEDURE_VERIFICATION_INTERVAL. */
	public static final String DEFAULT_DASHBOARD_OWNER = "default.dashboard.owner";

	/** The Constant METRICS_AUTO_CORRECT_RECORDS. */
	public static final String METRICS_AUTO_CORRECT_RECORDS = "metrics.auto.correct.records";

	/** The Constant DEFAULT_USER_RECORD. */
	public static final String DEFAULT_USER_EMPID_RECORD = "default.user.empid.record";

	/** The Constant ONBOARD_SYSTEM_ADMIN_EMAIL. */
	public static final String ONBOARD_SYSTEM_ADMIN_EMAIL = "onboard.system.admin.email";

	/** The version. */
	private String version;

	/** The name. */
	private String name;

	/** The value. */
	private String value;

	/** The description. */
	private String description;

	/** The preference type. */
	private PreferenceType preferenceType;

	/** The preference module. */
	private PreferenceModule preferenceModule;

	/** The lable tooltip. */
	private String tooltip;

	/**
	 * Instantiates a new preference.
	 */
	public Preference() {
	}

	/**
	 * Instantiates a new preference.
	 *
	 * @param industryId
	 *            the industry id
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @param description
	 *            the description
	 * @param createdBy
	 *            the created by
	 * @param updatedBy
	 *            the updated by
	 * @param createdOn
	 *            the created on
	 * @param updatedOn
	 *            the updated on
	 */
	public Preference(final String name, final String value,
			final String description, final User createdBy,
			final User updatedBy, final Date createdOn, final Date updatedOn) {
		this.name = name;
		this.value = value;
		this.description = description;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	@Column(name = "version", nullable = false, length = 20)
	public String getVersion() {
		return this.version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	@Column(name = "value", nullable = false, length = 800)
	public String getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value) {
		this.value = value;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	@Column(name = "description", nullable = false, length = 200)
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedBy()
	 */
	@Override
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATED_BY", nullable = false)
	public User getCreatedBy() {
		return this.createdBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedBy(java.lang.
	 * Integer)
	 */
	@Override
	public void setCreatedBy(final User createdBy) {
		this.createdBy = createdBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedBy()
	 */
	@Override
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UPDATED_BY", nullable = false)
	public User getUpdatedBy() {
		return this.updatedBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedBy(java.lang.
	 * Integer)
	 */
	@Override
	public void setUpdatedBy(final User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", nullable = false, length = 19)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getActiveStatus()
	 */
	@Override
	@Column(name = "active_status", nullable = false)
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setActiveStatus(com.fpg.
	 * pulse.pojo.enums.Status)
	 */
	@Override
	public void setActiveStatus(final Status activeStatus) {
		this.activeStatus = activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getJsonData()
	 */
	@Override
	@Column(name = "json_data")
	public String getJsonData() {
		return this.jsonData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setJsonData(java.lang.
	 * String)
	 */
	@Override
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

	/**
	 * Gets the preference type.
	 *
	 * @return the preference type
	 */
	@Column(name = "TYPE")
	public PreferenceType getPreferenceType() {
		return this.preferenceType;
	}

	/**
	 * Sets the preference type.
	 *
	 * @param preferenceType
	 *            the new preference type
	 */
	public void setPreferenceType(final PreferenceType preferenceType) {
		this.preferenceType = preferenceType;
	}

	/**
	 * Gets the preference module.
	 *
	 * @return the preference module
	 */
	@Column(name = "preferenceModule")
	public PreferenceModule getPreferenceModule() {
		return this.preferenceModule;
	}

	/**
	 * Sets the preference module.
	 *
	 * @param preferenceModule
	 *            the new preference module
	 */
	public void setPreferenceModule(final PreferenceModule preferenceModule) {
		this.preferenceModule = preferenceModule;
	}

	/**
	 * Gets the tooltip.
	 *
	 * @return the tooltip
	 */

	@Column(name = "tooltip")
	public String getTooltip() {
		return this.tooltip;
	}

	/**
	 * Sets the tooltip.
	 *
	 * @param tooltip
	 *            the tooltip to set
	 */
	public void setTooltip(final String tooltip) {
		this.tooltip = tooltip;
	}

}
