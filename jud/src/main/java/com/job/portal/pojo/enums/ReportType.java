/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc

/**
 * The Enum ReportType.
 */
public enum ReportType {

	/** The Training expectations. */
	TrainingExpectations(0),

	/** The Progress report. */
	ProgressReport(1),

	/** The Certifications. */
	Certifications(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the report type
	 */
	public static ReportType fromInteger(final Integer value) {
		if (value == TrainingExpectations.value) {
			return TrainingExpectations;
		} else if (value == ProgressReport.value) {
			return ProgressReport;
		} else if (value == Certifications.value) {
			return Certifications;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new report type.
	 *
	 * @param value
	 *            the value
	 */
	private ReportType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}