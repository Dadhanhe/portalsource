/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.University;
import com.job.portal.pojo.customer.dto.UniversityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserDTOMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IUniversityMapper
		extends IAbstractMapper<University, UniversityDTO> {

	/**
	 * To entity.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Override

	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	University toEntity(UniversityDTO dto);

	/**
	 * To dto.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO
	 */
	@Override

	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	UniversityDTO toDto(University entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final UniversityDTO dto,
			@MappingTarget final University entity) {
		checkCreatedByUpdatedBy(dto, entity);
	}

	/**
	 * Map plain.
	 *
	 * @param dto
	 *            the dto
	 * @return the user plain
	 */
	University toEntityPlain(UniversityDTO dto);

	/**
	 * Map 2.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	University toEntity2(UniversityDTO dto);

	/**
	 * Map 2.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO 2
	 */
	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	UniversityDTO toDto2(University entity);

}
