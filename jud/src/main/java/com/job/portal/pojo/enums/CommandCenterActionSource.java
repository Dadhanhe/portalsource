/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum CommandCenterActionSource.
 */
public enum CommandCenterActionSource {

	/** The Command center. */
	CommandCenter(0),

	/** The User. */
	User(1),

	/** The Counter coaching. */
	CounterCoaching(2),

	/** The Goal. */
	Goal(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the command center action source
	 */
	public static CommandCenterActionSource fromInteger(final Integer value) {
		if (value == CommandCenter.value) {
			return CommandCenter;
		} else if (value == User.value) {
			return User;
		} else if (value == CounterCoaching.value) {
			return CounterCoaching;
		} else if (value == Goal.value) {
			return Goal;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new command center action source.
	 *
	 * @param value
	 *            the value
	 */
	private CommandCenterActionSource(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == CommandCenter.value) {
			return "CommandCenter";
		} else if (value == User.value) {
			return "User";
		} else if (value == CounterCoaching.value) {
			return "CounterCoaching";
		} else if (value == Goal.value) {
			return "Goal";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static CommandCenterActionSource getEnum(final String status) {
		if (status != null) {
			if (status.equals("CommandCenter")) {
				return CommandCenterActionSource.CommandCenter;
			} else if (status.equals("User")) {
				return CommandCenterActionSource.User;
			} else if (status.equals("CounterCoaching")) {
				return CommandCenterActionSource.CounterCoaching;
			} else if (status.equals("Goal")) {
				return CommandCenterActionSource.Goal;
			}
		}
		return null;
	}
}
