/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.PulseDateUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class SynoTest.
 */
public class SynoTest {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(SynoTest.class);

	/**
	 * Instantiates a new syno test.
	 */
	public SynoTest() {

		final List<Date> dateList = PulseDateUtils.getLastMonthsDates(3);
		final Date startDate = dateList.get(0);
		final Date endDate = dateList.get(dateList.size() - 1);
		final String strError3 = " startDate :: " + startDate
				+ " ::  endDate - " + endDate;
		logger.debug(strError3);

		// ObjectUtils.logException(logger, null, "Test Message :: ");
		// final String str = null;
		// try {
		// str.length();
		// } catch (final Exception e) {
		// ObjectUtils.logException(logger, e, "Test Message :: ");
		// }

		// getLastMonthsDates(3);
		//
		// final Map<String, Object> jsonMap = new HashMap<>();
		// jsonMap.put("json466", 10);
		// jsonMap.put("json467", 20);
		// jsonMap.put("json468", 30);
		//
		// final ObjectMapper mapper = new ObjectMapper();
		// try {
		// System.out.println(mapper.writeValueAsString(jsonMap));
		// } catch (final Exception e) {
		// ObjectUtils.logError(logger, e, null);
		// }

		// final DateFormat format = new SimpleDateFormat("hh:mm:ss a");
		//
		// final String value = "07:00:00 AM";
		// try {
		// final Date date = format.parse(value);
		// System.out.println(new Time(date.getTime()));
		// } catch (final ParseException e) {
		// throw new JsonSyntaxException(e);
		// }

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args) {
		new SynoTest();

		System.out.println("isValid - dd/MM/yyyy with 20130925 = "
				+ isValidFormat("dd/MM/yyyy", "20130925", Locale.ENGLISH));
		System.out.println("isValid - dd/MM/yyyy with 25/09/2013 = "
				+ isValidFormat("dd/MM/yyyy", "25/09/2013", Locale.ENGLISH));
		System.out.println("isValid - dd/MM/yyyy with 25/09/2013 12:13:50 = "
				+ isValidFormat("dd/MM/yyyy", "25/09/2013  12:13:50",
						Locale.ENGLISH));
		System.out.println("isValid - yyyy-MM-dd with 2017-18--15 = "
				+ isValidFormat("yyyy-MM-dd", "20-18--15", Locale.ENGLISH));

		System.out.println("isValid - yyyy-MM-dd with 20-1822-15 = "
				+ isValidFormat("yyyy-MM-dd", "20-1822-15", Locale.ENGLISH));

		try {
			final String DATE_FORMAT = "yyyy-MM-dd";
			final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			final Date date = sdf.parse("20-1888-15");
			System.out.println("date :: " + date);
			System.out.println("date :: " + new Date());
		} catch (final ParseException e) {
			// TODO Auto-generated catch block
			ObjectUtils.logException(logger, e, "Failed to parse");
		}

		// System.out.println("isValid - dd/MM/yyyy with 20130925 = "
		// + isValidFormat("dd/MM/yyyy", "20130925"));
		// System.out.println("isValid - dd/MM/yyyy with 25/09/2013 = "
		// + isValidFormat("dd/MM/yyyy", "25/09/2013"));
		// System.out.println("isValid - dd/MM/yyyy with 25/09/2013 12:13:50 = "
		// + isValidFormat("dd/MM/yyyy", "25/09/2013 12:13:50"));
		// System.out.println("isValid - yyyy-MM-dd with 2017-18--15 = "
		// + isValidFormat("yyyy-MM-dd", "20-18--15"));
		System.out.println("isValid - yyyy-MM-dd with 20-1822-15 = "
				+ isValidFormat("yyyy-MM-dd", "20-1822-15"));

	}

	/**
	 * Checks if is valid format.
	 *
	 * @param format
	 *            the format
	 * @param value
	 *            the value
	 * @return true, if is valid format
	 */
	public static boolean isValidFormat(final String format,
			final String value) {
		Date date = null;
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (final ParseException ex) {
			ObjectUtils.logException(logger, ex,
					"Failed to export Tenant Locations.");
		}
		return date != null;
	}

	/**
	 * Checks if is valid format.
	 *
	 * @param format
	 *            the format
	 * @param value
	 *            the value
	 * @param locale
	 *            the locale
	 * @return true, if is valid format
	 */
	public static boolean isValidFormat(final String format, final String value,
			final Locale locale) {
		LocalDateTime ldt = null;
		final DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format,
				locale);

		try {
			ldt = LocalDateTime.parse(value, fomatter);
			final String result = ldt.format(fomatter);
			return result.equals(value);
		} catch (final DateTimeParseException e) {
			System.out.println("error...1");
			try {
				final LocalDate ld = LocalDate.parse(value, fomatter);
				final String result = ld.format(fomatter);
				return result.equals(value);
			} catch (final DateTimeParseException exp) {
				System.out.println("error...2");
				try {
					final LocalTime lt = LocalTime.parse(value, fomatter);
					final String result = lt.format(fomatter);
					return result.equals(value);
				} catch (final DateTimeParseException e2) {
					// Debugging purposes
					// e2.printStackTrace();
					System.out.println("error...3");
				}
			}
		}

		return false;
	}

	/**
	 * Gets the last months dates.
	 *
	 * @param howManyMonths
	 *            the how many months
	 * @return the last months dates
	 */
	public List<Date> getLastMonthsDates(final Integer howManyMonths) {
		final List<Date> dateList = new ArrayList<>();
		for (int ik = 0; ik < howManyMonths; ik++) {
			final Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.add(Calendar.MONTH, ik * (-1));
			dateList.add(calendar.getTime());
		}
		return dateList;
	}

}
