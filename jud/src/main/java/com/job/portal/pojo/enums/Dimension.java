/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Dimension.
 */
public enum Dimension {

	/** The Region. */
	Region(1),

	/** The Tenant location. */
	TenantLocation(2),

	/** The Location group. */
	LocationGroup(3),

	/** The Product. */
	Product(4),

	/** The User. */
	User(5),

	/** The Role. */
	Role(6),

	/** The Region type. */
	RegionType(7),

	/** The Custom goal filter. */
	CustomGoalFilter(8),

	/** The Custom hide date filter. */
	CustomHideDateFilter(9),

	/** The Is not default sort. */
	IsNotDefaultSort(10),

	/** The Custom show year filter. */
	CustomShowYearFilter(11),

	/** The Is counter coaching. */
	IsCounterCoaching(12),

	/** The Is observation. */
	IsObservation(13),

	/** The Is one 2 one. */
	IsOne2One(14)

	/** For My Contests Dashboard - Manual Contest DD Integration. */
	,CustomContestFilter(15)

	,
	/** The Market segment 1. */
	MarketSegment1(16)

	,
	/** The Market segment 2. */
	MarketSegment2(17),

	/** The Reservation type. */
	ReservationType(18),

	/** The Custom show month year filter. */
	CustomShowMonthYearFilter(19),

	/** This is for showing FPG user toggle. */
	IsFPGUserToggle(20),

	/** This is for showing platform filter. */
	showPlatformFilter(21),

	/** This is for showing masterLocationSwitch. */
	MasterLocationSwitch(22);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the dimension
	 */
	public static Dimension fromInteger(final Integer value) {
		if (value == Region.value) {
			return Region;
		} else if (value == TenantLocation.value) {
			return TenantLocation;
		} else if (value == LocationGroup.value) {
			return LocationGroup;
		} else if (value == Product.value) {
			return Product;
		} else if (value == User.value) {
			return User;
		} else if (value == Role.value) {
			return Role;
		} else if (value == RegionType.value) {
			return RegionType;
		} else if (value == CustomGoalFilter.value) {
			return CustomGoalFilter;
		} else if (value == CustomHideDateFilter.value) {
			return CustomHideDateFilter;
		} else if (value == IsNotDefaultSort.value) {
			return IsNotDefaultSort;
		} else if (value == CustomContestFilter.value) {
			return CustomContestFilter;
		} else if (value == CustomShowYearFilter.value) {
			return CustomShowYearFilter;
		} else if (value == IsCounterCoaching.value) {
			return IsCounterCoaching;
		} else if (value == IsObservation.value) {
			return IsObservation;
		} else if (value == IsOne2One.value) {
			return IsOne2One;
		} else if (value == MarketSegment1.value) {
			return MarketSegment1;
		} else if (value == MarketSegment2.value) {
			return MarketSegment2;
		} else if (value == ReservationType.value) {
			return ReservationType;
		} else if (value == CustomShowMonthYearFilter.value) {
			return CustomShowMonthYearFilter;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new dimension.
	 *
	 * @param value
	 *            the value
	 */
	private Dimension(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("industry.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Region.value) {
			return "Region";
		} else if (value == TenantLocation.value) {
			return "TenantLocation";
		} else if (value == LocationGroup.value) {
			return "LocationGroup";
		} else if (value == User.value) {
			return "User";
		} else if (value == Product.value) {
			return "Product";
		} else if (value == MarketSegment1.value) {
			return "MarketSegment1";
		} else if (value == MarketSegment2.value) {
			return "MarketSegment2";
		} else if (value == CustomShowMonthYearFilter.value) {
			return "CustomShowMonthYearFilter";
		}
		return null;
	}

	/**
	 * Gets the value by industry.
	 *
	 * @param code
	 *            the code
	 * @return the value by industry
	 */
	public static int getValueByIndustry(final String code) {
		for (int i = 1; i <= Dimension.values().length; i++) {
			if (code.equalsIgnoreCase(Dimension.values()[i].name())) {
				return Dimension.values()[i].value();
			}
		}
		return -1;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the dimension
	 */
	public static Dimension fromString(final String value) {
		if (value == Region.name()) {
			return Region;
		} else if (value.equals(TenantLocation.name())) {
			return TenantLocation;
		} else if (value.equals(LocationGroup.name())) {
			return LocationGroup;
		} else if (value == Product.name()) {
			return Product;
		} else if (value.equals(User.name())) {
			return User;
		} else if (value.equals(Role.name())) {
			return Role;
		} else if (value.equals(RegionType.name())) {
			return RegionType;
		} else if (value.equals(CustomGoalFilter.name())) {
			return CustomGoalFilter;
		} else if (value.equals(CustomHideDateFilter.name())) {
			return CustomHideDateFilter;
		} else if (value.equals(IsNotDefaultSort.name())) {
			return IsNotDefaultSort;
		} else if (value.equals(CustomContestFilter.name())) {
			return CustomContestFilter;
		} else if (value.equals(CustomShowYearFilter.name())) {
			return CustomShowYearFilter;
		} else if (value.equals(IsCounterCoaching.name())) {
			return IsCounterCoaching;
		} else if (value.equals(IsObservation.name())) {
			return IsObservation;
		} else if (value.equals(IsOne2One.name())) {
			return IsOne2One;
		} else if (value.equals(MarketSegment1.name())) {
			return MarketSegment1;
		} else if (value.equals(MarketSegment2.name())) {
			return MarketSegment2;
		} else if (value.equals(ReservationType.name())) {
			return ReservationType;
		} else if (value.equals(CustomShowMonthYearFilter.name())) {
			return CustomShowMonthYearFilter;
		}
		return null;
	}
}
