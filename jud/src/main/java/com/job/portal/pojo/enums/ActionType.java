/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc

/**
 * The Enum ActionType.
 */
public enum ActionType {

	/** The Counter coaching. */
	CounterCoaching(0),

	/** The One to one. */
	OneToOne(1),

	/** The Mail. */
	Mail(2),

	/** The Video. */
	Video(3),

	/** The Complete. */
	Complete(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the action type
	 */
	public static ActionType fromInteger(final Integer value) {
		if (value == CounterCoaching.value) {
			return CounterCoaching;
		} else if (value == OneToOne.value) {
			return OneToOne;
		} else if (value == Mail.value) {
			return Mail;
		} else if (value == Video.value) {
			return Video;
		} else if (value == Complete.value) {
			return Complete;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new action type.
	 *
	 * @param value
	 *            the value
	 */
	private ActionType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("actionType.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
