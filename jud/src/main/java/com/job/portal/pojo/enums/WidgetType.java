/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum WidgetType.
 */
public enum WidgetType {

	/** The Chart. */
	Chart(0),

	/** The Block. */
	Block(1),

	/** The Table. */
	Table(2),

	/** The Tile. */
	Tile(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the widget type
	 */
	public static WidgetType fromInteger(final Integer value) {
		if (value == Chart.value) {
			return Chart;
		} else if (value == Block.value) {
			return Block;
		} else if (value == Table.value) {
			return Table;
		} else if (value == Tile.value) {
			return Tile;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new widget type.
	 *
	 * @param value
	 *            the value
	 */
	private WidgetType(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Chart.value) {
			return "Chart";
		} else if (value == Block.value) {
			return "Block";
		} else if (value == Table.value) {
			return "Table";
		} else if (value == Tile.value) {
			return "Tile";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static WidgetType getEnum(final String status) {
		if (status != null) {
			if (status.equals("Chart")) {
				return WidgetType.Chart;
			} else if (status.equals("Block")) {
				return WidgetType.Block;
			} else if (status.equals("Table")) {
				return WidgetType.Table;
			} else if (status.equals("Tile")) {
				return WidgetType.Tile;
			}
		}
		return null;
	}
}
