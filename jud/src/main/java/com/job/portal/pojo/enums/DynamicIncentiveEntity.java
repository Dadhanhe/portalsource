/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum IncentiveEntity.
 */
public enum DynamicIncentiveEntity {

	/** The Region. */
	Region(0),

	/** The Tenant location. */
	TenantLocation(1),

	/** The Location group. */
	LocationGroup(2),

	/** The Role. */
	Role(3),

	/** The User. */
	User(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the incentive entity
	 */

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the dimension
	 */
	public static DynamicIncentiveEntity fromInteger(final Integer value) {
		if (value == Region.value) {
			return Region;
		} else if (value == TenantLocation.value) {
			return TenantLocation;
		} else if (value == LocationGroup.value) {
			return LocationGroup;
		} else if (value == User.value) {
			return User;
		} else if (value == Role.value) {
			return Role;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new incentive entity.
	 *
	 * @param value
	 *            the value
	 */
	private DynamicIncentiveEntity(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("industry.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Region.value) {
			return "Region";
		} else if (value == TenantLocation.value) {
			return "TenantLocation";
		} else if (value == LocationGroup.value) {
			return "LocationGroup";
		} else if (value == User.value) {
			return "User";
		} else if (value == Role.value) {
			return "Role";
		}
		return null;
	}

	/**
	 * Gets the value by industry.
	 *
	 * @param code
	 *            the code
	 * @return the value by industry
	 */
	public static int getValueByIndustry(final String code) {
		for (int i = 1; i <= DynamicIncentiveEntity.values().length; i++) {
			if (code.equalsIgnoreCase(
					DynamicIncentiveEntity.values()[i].name())) {
				return DynamicIncentiveEntity.values()[i].value();
			}
		}
		return -1;
	}
}