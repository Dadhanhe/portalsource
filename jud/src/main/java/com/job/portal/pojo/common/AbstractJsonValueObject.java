/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.solr.client.solrj.beans.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.job.portal.utils.ObjectUtils;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractJsonValueObject.
 */
/**
 * @author shahhard
 *
 */
@MappedSuperclass
public abstract class AbstractJsonValueObject extends AbstractValueObject {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(AbstractJsonValueObject.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The json data. */
	@Field
	protected String jsonData;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getJsonData()
	 */
	@Override
	@Column(name = "json_data")
	public String getJsonData() {
		return this.jsonData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#setJsonData(java.lang.
	 * String)
	 */
	@Override
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getJsonMap()
	 */
	@Override
	@Transient
	public Map<String, Object> getJsonMap() {
		final ObjectMapper mapper = new ObjectMapper();
		final String json = this.jsonData;
		Map<String, Object> map = new HashMap<String, Object>();
		if (!Objects.isEmpty(json)) {
			// convert JSON string to Map
			try {
				map = mapper.readValue(json,
						new TypeReference<Map<String, String>>() {
						});
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				ObjectUtils.logException(logger, e, null);
			}
		}
		return map;
	}
}
