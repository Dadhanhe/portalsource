/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Status;

/**
 * @author Vaibhav
 *
 */
@Entity
@Table(name = "COUNTRY")
public class Country extends AbstractValueObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "NAME")
	private String name;

	/** The district. */
	@Column(name = "CODE")
	private String code;

	// @ManyToOne(fetch = FetchType.EAGER, optional = true)
	// @JoinColumn(name = "STATUS_ID", nullable = true)
	// private Status status;

	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	@Override
	@Column(name = "active_status", nullable = false)
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/**
	 * Sets the active status.
	 *
	 * @param activeStatus
	 *            the new active status
	 */

	@Override
	public void setActiveStatus(final Status activeStatus) {
		this.activeStatus = activeStatus;
	}

}
