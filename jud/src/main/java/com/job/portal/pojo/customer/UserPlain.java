/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

// Generated Oct 5, 2016 2:02:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.job.portal.pojo.enums.Gender;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.JsonDateTimeDeserializer;
import com.job.portal.utils.JsonDateTimeSerializer;
import com.job.portal.utils.Objects;
import com.job.portal.utils.ValidationUtils;

import flexjson.JSON;

// TODO: Auto-generated Javadoc
/**
 * The Class UserPlain.
 */
@Entity
public class UserPlain {

	/** The id. */
	private Integer id;
	/** The c by identity. */
	private String cByIdentity;

	/** The u by identity. */
	private String uByIdentity;

	/** The active status. */
	private Status activeStatus;

	/** The created on. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date createdOn;

	/** The updated on. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date updatedOn;

	/** The json data. */
	private String jsonData;

	/** The email. */
	@Valid
	@Pattern(regexp = ValidationUtils.EmailRegex)
	@Size(max = 100)
	private String email;

	/** The password. */
	private String password;

	/** The access token. */
	private String accessToken;

	/** The token expiry. */
	private Date tokenExpiry;

	/** The salt. */
	private String salt;

	/** The signed in time. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date signedInTime;

	/** The default industry. */
	private Integer defaultIndustry;

	/** The last selected tenant. */
	private Integer lastSelectedTenant;

	/** The number. */
	@Valid
	@Pattern(regexp = "[\\w\\`\\'\\.]{1,20}")
	private String number;

	/** The first name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{1,50}")
	private String firstName;

	/** The middle name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{0,50}")
	private String middleName;

	/** The last name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{1,50}")
	private String lastName;

	/** The gender. */
	@NotNull
	private Gender gender;

	/** The phone number. */
	@Valid
	@Pattern(regexp = "[\\d\\s\\-\\(\\)]{0,20}")
	private String phoneNumber;

	/** The mobile number. */
	private String mobileNumber;

	/** The hired date. */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date hiredDate;

	/** The fpg level. */
	private String fpgLevel;

	/** The role name. */
	private String roleName;

	/** The is super admin. */
	private Boolean isSuperAdmin;

	/** The is performance manager. */
	private Boolean isPerformanceManager;

	/** The is default tenant role. */
	private Boolean isDefaultTenantRole;

	/** The job title. */
	@Size(min = 1, max = 50)
	private String jobTitle;

	/** The linkedin id. */
	private String linkedinId;

	/** The linkedin image. */
	private String linkedinImage;

	/** The birth month. */
	private Integer birthMonth;

	/** The birth day. */
	private Integer birthDay;

	/** The pulse id. */
	private String pulseId;

	/** The is fpg member. */
	private Boolean isFpgMember;

	/** The authy id. */
	private String authyId;

	/** The is authy attempted. */
	private Boolean isAuthyAttempted;

	/** The country code. */
	private String countryCode;

	/** The desired location. */
	private String desiredLocation;

	/** The ios version. */
	private String iosVersion;

	/** The android version. */
	private String androidVersion;

	/** The work age. */
	private String workAge;

	/** The is first login. */
	private Boolean isFirstLogin;

	/** The working type. */
	private String workingType;

	/** The block event. */
	private String blockEvent;

	/** The user val. */
	private String userVal;

	/** The last activity done on. */
	private Date lastActivityDoneOn;

	/** The assigned to tenant. */
	private String assignedToTenant;

	/** The role id. */
	private Integer roleId;

	/** The user location name. */
	private String userLocationName;

	/** The is beta tester. */
	private Boolean isBetaTester;

	/** The contact email. */
	private String contactEmail;

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	@Column(name = "roleName", nullable = true)
	public String getRoleName() {
		return this.roleName;
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName
	 *            the new role name
	 */
	public void setRoleName(final String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Gets the checks if is super admin.
	 *
	 * @return the checks if is super admin
	 */
	@Column(name = "isSuperAdmin", nullable = true)
	public Boolean getIsSuperAdmin() {
		return this.isSuperAdmin;
	}

	/**
	 * Sets the checks if is super admin.
	 *
	 * @param isSuperAdmin
	 *            the new checks if is super admin
	 */
	public void setIsSuperAdmin(final Boolean isSuperAdmin) {
		this.isSuperAdmin = isSuperAdmin;
	}

	/**
	 * Gets the checks if is performance manager.
	 *
	 * @return the checks if is performance manager
	 */
	@Column(name = "isPerformanceManager", nullable = true)
	public Boolean getIsPerformanceManager() {
		return this.isPerformanceManager;
	}

	/**
	 * Sets the checks if is performance manager.
	 *
	 * @param isPerformanceManager
	 *            the new checks if is performance manager
	 */
	public void setIsPerformanceManager(final Boolean isPerformanceManager) {
		this.isPerformanceManager = isPerformanceManager;
	}

	/**
	 * Gets the checks if is default tenant role.
	 *
	 * @return the checks if is default tenant role
	 */
	@Column(name = "isDefaultTenantRole", nullable = true)
	public Boolean getIsDefaultTenantRole() {
		return this.isDefaultTenantRole;
	}

	/**
	 * Sets the checks if is default tenant role.
	 *
	 * @param isDefaultTenantRole
	 *            the new checks if is default tenant role
	 */
	public void setIsDefaultTenantRole(final Boolean isDefaultTenantRole) {
		this.isDefaultTenantRole = isDefaultTenantRole;
	}

	/**
	 * Gets the role id.
	 *
	 * @return the role id
	 */
	@Column(name = "roleId", nullable = true)
	public Integer getRoleId() {
		return this.roleId;
	}

	/**
	 * Sets the role id.
	 *
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(final Integer roleId) {
		this.roleId = roleId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedBy()
	 */

	/**
	 * Gets the c by identity.
	 *
	 * @return the c by identity
	 */
	@Column(name = "cByIdentity")
	public String getCByIdentity() {
		return this.cByIdentity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedBy(java.lang.
	 * Integer)
	 */

	/**
	 * Sets the c by identity.
	 *
	 * @param cByIdentity
	 *            the new c by identity
	 */
	public void setCByIdentity(final String cByIdentity) {
		this.cByIdentity = cByIdentity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedBy()
	 */

	/**
	 * Gets the u by identity.
	 *
	 * @return the u by identity
	 */
	@Column(name = "uByIdentity")
	public String getUByIdentity() {
		return this.uByIdentity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedBy(java.lang.
	 * Integer)
	 */

	/**
	 * Sets the u by identity.
	 *
	 * @param uByIdentity
	 *            the new u by identity
	 */
	public void setUByIdentity(final String uByIdentity) {
		this.uByIdentity = uByIdentity;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getCreatedOn()
	 */

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */

	/**
	 * Sets the created on.
	 *
	 * @param createdOn
	 *            the new created on
	 */
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getUpdatedOn()
	 */

	/**
	 * Gets the updated on.
	 *
	 * @return the updated on
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON")
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */

	/**
	 * Sets the updated on.
	 *
	 * @param updatedOn
	 *            the new updated on
	 */
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getActiveStatus()
	 */

	/**
	 * Gets the active status.
	 *
	 * @return the active status
	 */
	@Column(name = "active_status")
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setActiveStatus(com.fpg.
	 * pulse.pojo.enums.Status)
	 */

	/**
	 * Sets the active status.
	 *
	 * @param activeStatus
	 *            the new active status
	 */
	public void setActiveStatus(final Status activeStatus) {
		this.activeStatus = activeStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getJsonData()
	 */

	/**
	 * Gets the json data.
	 *
	 * @return the json data
	 */
	@Column(name = "json_data")
	public String getJsonData() {
		return this.jsonData;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#setJsonData(java.lang.
	 * String)
	 */

	/**
	 * Sets the json data.
	 *
	 * @param jsonData
	 *            the new json data
	 */
	public void setJsonData(final String jsonData) {
		this.jsonData = jsonData;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	@Column(name = "EMAIL", unique = true, nullable = false, length = 100)
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	@Column(name = "PASSWORD", nullable = false, length = 100)
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	@Column(name = "ACCESS_TOKEN", length = 32)
	public String getAccessToken() {
		return this.accessToken;
	}

	/**
	 * Sets the access token.
	 *
	 * @param accessToken
	 *            the new access token
	 */
	public void setAccessToken(final String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * Gets the token expiry.
	 *
	 * @return the token expiry
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TOKEN_EXPIRY", length = 19)
	public Date getTokenExpiry() {
		return this.tokenExpiry;
	}

	/**
	 * Sets the token expiry.
	 *
	 * @param tokenExpiry
	 *            the new token expiry
	 */
	public void setTokenExpiry(final Date tokenExpiry) {
		this.tokenExpiry = tokenExpiry;
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	@Column(name = "SALT", nullable = false, length = 45)
	public String getSalt() {
		return this.salt;
	}

	/**
	 * Sets the salt.
	 *
	 * @param salt
	 *            the new salt
	 */
	public void setSalt(final String salt) {
		this.salt = salt;
	}

	/**
	 * Gets the signed in time.
	 *
	 * @return the signed in time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SIGNED_IN_TIME", length = 19)
	public Date getSignedInTime() {
		return this.signedInTime;
	}

	/**
	 * Sets the signed in time.
	 *
	 * @param signedInTime
	 *            the new signed in time
	 */
	public void setSignedInTime(final Date signedInTime) {
		this.signedInTime = signedInTime;
	}

	/**
	 * Gets the default industry.
	 *
	 * @return the default industry
	 */
	@Column(name = "DEFAULT_INDUSTRY")
	public Integer getDefaultIndustry() {
		return this.defaultIndustry;
	}

	/**
	 * Sets the default industry.
	 *
	 * @param defaultIndustry
	 *            the new default industry
	 */
	public void setDefaultIndustry(final Integer defaultIndustry) {
		this.defaultIndustry = defaultIndustry;
	}

	/**
	 * Gets the last selected tenant.
	 *
	 * @return the last selected tenant
	 */
	@Column(name = "last_selected_tenant")
	public Integer getLastSelectedTenant() {
		return this.lastSelectedTenant;
	}

	/**
	 * Sets the last selected tenant.
	 *
	 * @param lastSelectedTenant
	 *            the new last selected tenant
	 */
	public void setLastSelectedTenant(final Integer lastSelectedTenant) {
		this.lastSelectedTenant = lastSelectedTenant;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	@Column(name = "NUMBER", nullable = false, length = 20)
	public String getNumber() {
		return this.number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number
	 *            the new number
	 */
	public void setNumber(final String number) {
		this.number = number;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the middle name.
	 *
	 * @return the middle name
	 */
	@Column(name = "MIDDLE_NAME", length = 50)
	public String getMiddleName() {
		return this.middleName;
	}

	/**
	 * Sets the middle name.
	 *
	 * @param middleName
	 *            the new middle name
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	@Column(name = "LAST_NAME", nullable = false, length = 50)
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	@Column(name = "GENDER", nullable = false)
	public Gender getGender() {
		return this.gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the new gender
	 */
	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	@Column(name = "PHONE_NUMBER", length = 50)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber
	 *            the new phone number
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the mobile number.
	 *
	 * @return the mobile number
	 */
	@Column(name = "MOBILE_NUMBER", length = 45)
	public String getMobileNumber() {
		return this.mobileNumber;
	}

	/**
	 * Sets the mobile number.
	 *
	 * @param mobileNumber
	 *            the new mobile number
	 */
	public void setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the hired date.
	 *
	 * @return the hired date
	 */
	@Column(name = "HIRED_DATE", length = 19)
	public Date getHiredDate() {
		return this.hiredDate;
	}

	/**
	 * Sets the hired date.
	 *
	 * @param hiredDate
	 *            the new hired date
	 */
	public void setHiredDate(final Date hiredDate) {
		this.hiredDate = hiredDate;
	}

	/**
	 * Gets the fpg level.
	 *
	 * @return the fpg level
	 */
	@Column(name = "FPG_LEVEL", length = 200)
	public String getFpgLevel() {
		return this.fpgLevel;
	}

	/**
	 * Sets the fpg level.
	 *
	 * @param fpgLevel
	 *            the new fpg level
	 */
	public void setFpgLevel(final String fpgLevel) {
		this.fpgLevel = fpgLevel;
	}

	/**
	 * Gets the job title.
	 *
	 * @return the job title
	 */
	@Column(name = "JOB_TITLE", nullable = false, length = 100)
	public String getJobTitle() {
		return this.jobTitle;
	}

	/**
	 * Sets the job title.
	 *
	 * @param jobTitle
	 *            the new job title
	 */
	public void setJobTitle(final String jobTitle) {
		this.jobTitle = jobTitle;
	}

	/**
	 * Gets the linkedin id.
	 *
	 * @return the linkedin id
	 */
	@Column(name = "LINKEDIN_ID", length = 20)
	public String getLinkedinId() {
		return this.linkedinId;
	}

	/**
	 * Sets the linkedin id.
	 *
	 * @param linkedinId
	 *            the new linkedin id
	 */
	public void setLinkedinId(final String linkedinId) {
		this.linkedinId = linkedinId;
	}

	/**
	 * Gets the linkedin image.
	 *
	 * @return the linkedin image
	 */
	@Column(name = "LINKEDIN_IMAGE", length = 240)
	public String getLinkedinImage() {
		return this.linkedinImage;
	}

	/**
	 * Sets the linkedin image.
	 *
	 * @param linkedinImage
	 *            the new linkedin image
	 */
	public void setLinkedinImage(final String linkedinImage) {
		this.linkedinImage = linkedinImage;
	}

	/**
	 * Gets the birth month.
	 *
	 * @return the birth month
	 */
	@Column(name = "BIRTH_MONTH")
	public Integer getBirthMonth() {
		return this.birthMonth;
	}

	/**
	 * Sets the birth month.
	 *
	 * @param birthMonth
	 *            the new birth month
	 */
	public void setBirthMonth(final Integer birthMonth) {
		this.birthMonth = birthMonth;
	}

	/**
	 * Gets the birth day.
	 *
	 * @return the birth day
	 */
	@Column(name = "BIRTH_DAY")
	public Integer getBirthDay() {
		return this.birthDay;
	}

	/**
	 * Sets the birth day.
	 *
	 * @param birthDay
	 *            the new birth day
	 */
	public void setBirthDay(final Integer birthDay) {
		this.birthDay = birthDay;
	}

	/**
	 * Gets the pulse id.
	 *
	 * @return the pulse id
	 */
	@Column(name = "PULSE_ID", length = 100)
	public String getPulseId() {
		return this.pulseId;
	}

	/**
	 * Sets the pulse id.
	 *
	 * @param pulseId
	 *            the new pulse id
	 */
	public void setPulseId(final String pulseId) {
		this.pulseId = pulseId;
	}

	/**
	 * Gets the checks if is fpg member.
	 *
	 * @return the checks if is fpg member
	 */
	@Column(name = "IS_FPG_MEMBER")
	public Boolean getIsFpgMember() {
		return this.isFpgMember;
	}

	/**
	 * Sets the checks if is fpg member.
	 *
	 * @param isFpgMember
	 *            the new checks if is fpg member
	 */
	public void setIsFpgMember(final Boolean isFpgMember) {
		this.isFpgMember = isFpgMember;
	}

	/**
	 * Gets the authy id.
	 *
	 * @return the authy id
	 */
	@Column(name = "AUTHY_ID", length = 45)
	public String getAuthyId() {
		return this.authyId;
	}

	/**
	 * Sets the authy id.
	 *
	 * @param authyId
	 *            the new authy id
	 */
	public void setAuthyId(final String authyId) {
		this.authyId = authyId;
	}

	/**
	 * Gets the checks if is authy attempted.
	 *
	 * @return the checks if is authy attempted
	 */
	@Column(name = "IS_AUTHY_ATTEMPTED")
	public Boolean getIsAuthyAttempted() {
		return this.isAuthyAttempted;
	}

	/**
	 * Sets the checks if is authy attempted.
	 *
	 * @param isAuthyAttempted
	 *            the new checks if is authy attempted
	 */
	public void setIsAuthyAttempted(final Boolean isAuthyAttempted) {
		this.isAuthyAttempted = isAuthyAttempted;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	@Column(name = "COUNTRY_CODE", length = 5)
	public String getCountryCode() {
		return this.countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode
	 *            the new country code
	 */
	public void setCountryCode(final String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the desired location.
	 *
	 * @return the desired location
	 */
	@Column(name = "DESIRED_LOCATION", length = 200)
	public String getDesiredLocation() {
		return this.desiredLocation;
	}

	/**
	 * Sets the desired location.
	 *
	 * @param desiredLocation
	 *            the new desired location
	 */
	public void setDesiredLocation(final String desiredLocation) {
		this.desiredLocation = desiredLocation;
	}

	/**
	 * Gets the ios version.
	 *
	 * @return the ios version
	 */
	@Column(name = "IOS_VERSION", length = 45)
	public String getIosVersion() {
		return this.iosVersion;
	}

	/**
	 * Sets the ios version.
	 *
	 * @param iosVersion
	 *            the new ios version
	 */
	public void setIosVersion(final String iosVersion) {
		this.iosVersion = iosVersion;
	}

	/**
	 * Gets the android version.
	 *
	 * @return the android version
	 */
	@Column(name = "ANDROID_VERSION", length = 45)
	public String getAndroidVersion() {
		return this.androidVersion;
	}

	/**
	 * Sets the android version.
	 *
	 * @param androidVersion
	 *            the new android version
	 */
	public void setAndroidVersion(final String androidVersion) {
		this.androidVersion = androidVersion;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Transient
	@JSON(include = true)
	public String getName() {
		String name = "";
		if (!Objects.isEmpty(this.firstName)) {
			name += this.firstName + " ";
		}
		if (!Objects.isEmpty(this.lastName)) {
			name += this.lastName;
		}
		return name;
	}

	/**
	 * Gets the work age.
	 *
	 * @return the work age
	 */
	@Transient
	public String getWorkAge() {
		String ret = "";
		if (this.hiredDate != null) {
			long workAge = 0;
			final long timeSpan = new Date().getTime()
					- this.hiredDate.getTime();
			workAge = timeSpan / (1000 * 60 * 60 * 24) / 30;
			final long year = workAge / 12;
			final long month = workAge % 12;
			final String yearDesc = year > 1 ? "years" : "year";
			final String monthDesc = month > 1 ? "months" : "month";
			if (year > 0) {
				ret += String.format("%d %s", year, yearDesc);
			}
			if (month > 0) {
				if (ret.length() > 0) {
					ret += " ";
				}
				ret += String.format("%d %s", month, monthDesc);
			}
		}
		this.workAge = ret;
		return this.workAge;
	}

	/**
	 * Sets the work age.
	 *
	 * @param workAge
	 *            the new work age
	 */
	public void setWorkAge(final String workAge) {
		this.workAge = workAge;
	}

	/**
	 * Gets the checks if is first login.
	 *
	 * @return the checks if is first login
	 */
	@Column(name = "IS_FIRST_LOGIN")
	public Boolean getIsFirstLogin() {
		return this.isFirstLogin;
	}

	/**
	 * Sets the checks if is first login.
	 *
	 * @param isFirstLogin
	 *            the new checks if is first login
	 */
	public void setIsFirstLogin(final Boolean isFirstLogin) {
		this.isFirstLogin = isFirstLogin;
	}

	/**
	 * Gets the working type.
	 *
	 * @return the working type
	 */
	@Column(name = "WORKINGTYPE")
	public String getWorkingType() {
		return this.workingType;
	}

	/**
	 * Sets the working type.
	 *
	 * @param workingType
	 *            the new working type
	 */
	public void setWorkingType(final String workingType) {
		this.workingType = workingType;
	}

	/**
	 * Gets the block event.
	 *
	 * @return the block event
	 */
	@Column(name = "Block_EVENT")
	public String getBlockEvent() {
		return this.blockEvent;
	}

	/**
	 * Sets the block event.
	 *
	 * @param blockEvent
	 *            the new block event
	 */
	public void setBlockEvent(final String blockEvent) {
		this.blockEvent = blockEvent;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	@Transient
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	/**
	 * Gets the user val.
	 *
	 * @return the user val
	 */
	@Column(name = "userVal", nullable = false, length = 100)
	public String getUserVal() {
		return this.userVal;
	}

	/**
	 * Sets the user val.
	 *
	 * @param userVal
	 *            the new user val
	 */
	public void setUserVal(final String userVal) {
		this.userVal = userVal;
	}

	/**
	 * Gets the last activity done on.
	 *
	 * @return the last activity done on
	 */
	@JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)
	@Column(name = "LAST_ACTIVITY_DONE_ON")
	public Date getLastActivityDoneOn() {
		return this.lastActivityDoneOn;
	}

	/**
	 * Sets the last activity done on.
	 *
	 * @param lastActivityDoneOn
	 *            the new last activity done on
	 */
	public void setLastActivityDoneOn(final Date lastActivityDoneOn) {
		this.lastActivityDoneOn = lastActivityDoneOn;
	}

	/**
	 * Gets the assigned to tenant.
	 *
	 * @return the assigned to tenant
	 */
	@Column(name = "assignedToTenant")
	public String getAssignedToTenant() {
		return this.assignedToTenant;
	}

	/**
	 * Sets the assigned to tenant.
	 *
	 * @param assignedToTenant
	 *            the new assigned to tenant
	 */
	public void setAssignedToTenant(final String assignedToTenant) {
		this.assignedToTenant = assignedToTenant;
	}

	/**
	 * Gets the user location name.
	 *
	 * @return the user location name
	 */
	@Column(name = "userLocationName")
	public String getUserLocationName() {
		return this.userLocationName;
	}

	/**
	 * Sets the user location name.
	 *
	 * @param userLocationName
	 *            the new user location name
	 */
	public void setUserLocationName(final String userLocationName) {
		this.userLocationName = userLocationName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#clone()
	 */

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	/**
	 * Gets the checks if is beta tester.
	 *
	 * @return the checks if is beta tester
	 */
	@Column(name = "IS_BETA_TESTER")
	public Boolean getIsBetaTester() {
		return this.isBetaTester;
	}

	/**
	 * Sets the checks if is beta tester.
	 *
	 * @param isBetaTester
	 *            the new checks if is beta tester
	 */
	public void setIsBetaTester(final Boolean isBetaTester) {
		this.isBetaTester = isBetaTester;
	}

	/**
	 * Gets the contact email.
	 *
	 * @return the contact email
	 */
	@Column(name = "contact_email")
	public String getContactEmail() {
		return this.contactEmail;
	}

	/**
	 * Sets the contact email.
	 *
	 * @param contactEmail
	 *            the new contact email
	 */
	public void setContactEmail(final String contactEmail) {
		this.contactEmail = contactEmail;
	}

}
