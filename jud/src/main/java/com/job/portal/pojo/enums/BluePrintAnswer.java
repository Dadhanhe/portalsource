/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum BluePrintAnswer.
 */
public enum BluePrintAnswer {

	/** The No. */
	No(0),

	/** The Yes. */
	Yes(1),

	/** The na. */
	NA(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the blue print answer
	 */
	public static BluePrintAnswer fromInteger(final Integer value) {
		if (value == Yes.value) {
			return Yes;
		} else if (value == No.value) {
			return No;
		} else if (value == NA.value) {
			return NA;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new blue print answer.
	 *
	 * @param value
	 *            the value
	 */
	private BluePrintAnswer(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("bluePrintAnswer.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("bluePrintAnswer.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Yes.value) {
			return "Yes";
		} else if (value == No.value) {
			return "No";
		} else if (value == NA.value) {
			return "NA";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param bluePrintAnswer
	 *            the blue print answer
	 * @return the enum
	 */
	public static BluePrintAnswer getEnum(final String bluePrintAnswer) {
		if (bluePrintAnswer != null) {
			if (bluePrintAnswer.equals("Yes")) {
				return BluePrintAnswer.Yes;
			} else if (bluePrintAnswer.equals("No")) {
				return BluePrintAnswer.No;
			} else if (bluePrintAnswer.equals("NA")) {
				return BluePrintAnswer.NA;
			}
		}
		return null;
	}

}
