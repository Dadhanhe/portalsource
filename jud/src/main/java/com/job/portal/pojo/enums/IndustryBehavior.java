/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum IndustryBehavior.
 */
public enum IndustryBehavior {

	/** The hotel. */
	HOTEL(1),

	/** The carrental. */
	CARRENTAL(2),

	/** The callcenter. */
	CALLCENTER(3),

	/** The entertainment. */
	ENTERTAINMENT(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the industry behavior
	 */
	public static IndustryBehavior fromInteger(final Integer value) {
		if (value == HOTEL.value) {
			return HOTEL;
		} else if (value == CARRENTAL.value) {
			return CARRENTAL;
		} else if (value == CALLCENTER.value) {
			return CALLCENTER;
		} else if (value == ENTERTAINMENT.value) {
			return ENTERTAINMENT;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new industry behavior.
	 *
	 * @param value
	 *            the value
	 */
	private IndustryBehavior(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("industry.behavior.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == HOTEL.value) {
			return "HOTEL";
		} else if (value == CARRENTAL.value) {
			return "CARRENTAL";
		} else if (value == CALLCENTER.value) {
			return "CALLCENTER";
		} else if (value == ENTERTAINMENT.value) {
			return "ENTERTAINMENT";
		}
		return null;
	}

}
