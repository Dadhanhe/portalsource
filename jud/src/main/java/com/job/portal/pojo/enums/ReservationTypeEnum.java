/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum ReservationType.
 */
public enum ReservationTypeEnum {

	/** The All. */
	NA(-1),
	/** The Walk in. */
	WalkIn(1),
	/** The Reservation. */
	Reservation(0);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new reservation type.
	 *
	 * @param value
	 *            the value
	 */
	private ReservationTypeEnum(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the reservation type
	 */
	public static ReservationTypeEnum fromInteger(final Integer value) {
		if (value == NA.value) {
			return NA;
		} else if (value == WalkIn.value) {
			return WalkIn;
		} else if (value == Reservation.value) {
			return Reservation;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == NA.value) {
			return "NA";
		} else if (value == WalkIn.value) {
			return "WalkIn";
		} else if (value == Reservation.value) {
			return "Reservation";
		}
		return null;
	}
}
