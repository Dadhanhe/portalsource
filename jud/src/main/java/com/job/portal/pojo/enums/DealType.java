/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum DealType.
 */
public enum DealType implements Serializable {

	/** The onward. */
	ONWARD(0),

	/** The return. */
	RETURN(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new deal type.
	 *
	 * @param val
	 *            the val
	 */
	private DealType(int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}
}
