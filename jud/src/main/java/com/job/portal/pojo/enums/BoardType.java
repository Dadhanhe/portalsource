/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Status.
 */
public enum BoardType {

	/** The cbsc. */
	CBSC(0),

	/** The icsc. */
	ICSC(1),

	/** The ib. */
	IB(2),

	/** The ssc. */
	SSC(3),

	/** The hssc. */
	HSSC(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the status
	 */
	public static BoardType fromInteger(final Integer value) {
		if (value == CBSC.value) {
			return CBSC;
		} else if (value == ICSC.value) {
			return ICSC;
		} else if (value == IB.value) {
			return IB;
		} else if (value == SSC.value) {
			return SSC;
		} else if (value == HSSC.value) {
			return HSSC;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new status.
	 *
	 * @param value
	 *            the value
	 */
	private BoardType(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("BoardType.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("BoardType.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == CBSC.value) {
			return "CBSC";
		} else if (value == ICSC.value) {
			return "ICSC";
		} else if (value == IB.value) {
			return "IB";
		} else if (value == SSC.value) {
			return "SSC";
		} else if (value == HSSC.value) {
			return "HSSC";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param boardType
	 *            the board type
	 * @return the enum
	 */
	public static BoardType getEnum(final String boardType) {
		if (boardType != null) {
			if (boardType.equals("CBSC")) {
				return BoardType.CBSC;
			} else if (boardType.equals("ICSC")) {
				return BoardType.ICSC;
			} else if (boardType.equals("IB")) {
				return BoardType.IB;
			} else if (boardType.equals("SSC")) {
				return BoardType.SSC;
			} else if (boardType.equals("HSSC")) {
				return BoardType.HSSC;
			}
		}
		return null;
	}
}
