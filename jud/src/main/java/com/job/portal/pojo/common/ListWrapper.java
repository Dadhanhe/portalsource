/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.job.portal.pojo.common.dto.AbstractDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ListWrapper.
 *
 * @param <E>
 *            the element type
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListWrapper<E extends AbstractDTO> extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6698780728199107564L;

	/** The data. */
	private List<? extends AbstractDTO> data;

	/** The first. */
	private Integer first;

	/** The total in list. */
	private Integer totalInList;

	/** The total. */
	private Long total;

	/** The attachment directory path. */
	private String attachmentDirectoryPath;

	/** The upload attach directory path. */
	private String uploadAttachDirectoryPath;

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public List<? extends AbstractDTO> getData() {
		return this.data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *            the new data
	 */
	public void setData(final List<? extends AbstractDTO> data) {
		this.data = data;
	}

	/**
	 * Gets the first.
	 *
	 * @return the first
	 */
	public Integer getFirst() {
		return this.first;
	}

	/**
	 * Sets the first.
	 *
	 * @param first
	 *            the new first
	 */
	public void setFirst(final Integer first) {
		this.first = first;
	}

	/**
	 * Gets the total in list.
	 *
	 * @return the total in list
	 */
	public Integer getTotalInList() {
		return this.totalInList;
	}

	/**
	 * Sets the total in list.
	 *
	 * @param totalInList
	 *            the new total in list
	 */
	public void setTotalInList(final Integer totalInList) {
		this.totalInList = totalInList;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public Long getTotal() {
		return this.total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total
	 *            the new total
	 */
	public void setTotal(final Long total) {
		this.total = total;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getId()
	 */
	@Override
	public Integer getId() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {

	}

	/**
	 * Gets the attachment directory path.
	 *
	 * @return the attachment directory path
	 */
	public String getAttachmentDirectoryPath() {
		return this.attachmentDirectoryPath;
	}

	/**
	 * Sets the attachment directory path.
	 *
	 * @param attachmentDirectoryPath
	 *            the new attachment directory path
	 */
	public void setAttachmentDirectoryPath(
			final String attachmentDirectoryPath) {
		this.attachmentDirectoryPath = attachmentDirectoryPath;
	}

	/**
	 * Gets the upload attach directory path.
	 *
	 * @return the upload attach directory path
	 */
	public String getUploadAttachDirectoryPath() {
		return this.uploadAttachDirectoryPath;
	}

	/**
	 * Sets the upload attach directory path.
	 *
	 * @param uploadAttachDirectoryPath
	 *            the new upload attach directory path
	 */
	public void setUploadAttachDirectoryPath(
			final String uploadAttachDirectoryPath) {
		this.uploadAttachDirectoryPath = uploadAttachDirectoryPath;
	}

}
