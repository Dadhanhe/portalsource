/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// TODO: Auto-generated Javadoc
/**
 * The Class EventLogDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class EventLogDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3299219291447879473L;

	/** The action. */
	@Field
	private String action;

	/** The entity id. */
	@Field
	private Integer entityId;

	/** The entity type. */
	@Field
	private String entityType;

	/** The executor id. */
	@Field
	private Integer executorId;

	/** The display name. */
	@Field
	private String displayName;

	/** The entity name. */
	@Field
	private String entityName;

	/** The platform. */
	@Field
	private Integer platform;

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return this.action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action
	 *            the new action
	 */
	public void setAction(final String action) {
		this.action = action;
	}

	/**
	 * Gets the entity id.
	 *
	 * @return the entity id
	 */
	public Integer getEntityId() {
		return this.entityId;
	}

	/**
	 * Sets the entity id.
	 *
	 * @param entityId
	 *            the new entity id
	 */
	public void setEntityId(final Integer entityId) {
		this.entityId = entityId;
	}

	/**
	 * Gets the entity type.
	 *
	 * @return the entity type
	 */
	public String getEntityType() {
		return this.entityType;
	}

	/**
	 * Sets the entity type.
	 *
	 * @param entityType
	 *            the new entity type
	 */
	public void setEntityType(final String entityType) {
		this.entityType = entityType;
	}

	/**
	 * Gets the executor id.
	 *
	 * @return the executor id
	 */
	public Integer getExecutorId() {
		return this.executorId;
	}

	/**
	 * Sets the executor id.
	 *
	 * @param executorId
	 *            the new executor id
	 */
	public void setExecutorId(final Integer executorId) {
		this.executorId = executorId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.dto.AbstractDTO#getCreatedOn()
	 */
	@Override
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.dto.AbstractDTO#setCreatedOn(java.util.Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Sets the display name.
	 *
	 * @param displayName
	 *            the new display name
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Gets the entity name.
	 *
	 * @return the entity name
	 */
	public String getEntityName() {
		return this.entityName;
	}

	/**
	 * Sets the entity name.
	 *
	 * @param entityName
	 *            the new entity name
	 */
	public void setEntityName(final String entityName) {
		this.entityName = entityName;
	}

	/**
	 * Gets the entity name.
	 *
	 * @return the platform.
	 */
	public Integer getPlatform() {
		return this.platform;
	}

	/**
	 * Sets the entity name.
	 *
	 * @param platform
	 *            the new platform
	 */
	public void setPlatform(final Integer platform) {
		this.platform = platform;
	}

}
