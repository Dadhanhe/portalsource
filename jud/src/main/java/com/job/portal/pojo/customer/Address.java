/**
 *
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.job.portal.pojo.common.AbstractValueObject;

/**
 * @author Vaibhav
 *
 */
@Entity
@Table(name = "address")
public class Address extends AbstractValueObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "LANDMARK")
	private String landmark;

	/** The lat. */
	// @NotNull(groups = { Load.SaveGroup.class })
	@Column(name = "LATITUDE")
	private String lat;

	/** The lng. */
	// @NotNull(groups = { Load.SaveGroup.class })
	@Column(name = "LONGITUDE")
	private String lng;

	/** The address. */
	@Column(name = "ADDRESS")
	private String address;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "CITY_ID", nullable = true)
	private City city;

	@Column(name = "PINCODE")
	private String pincode;

	/** The district. */
	@Column(name = "DISTRICT")
	private String district;

	/** The google place id. */
	@Column(name = "GOOGLE_PLACE_ID")
	private String googlePlaceId;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "USER_ID", nullable = true)
	private User user;

	@Column(name = "DISTANCE")
	private Double distance;

	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the landmark
	 */
	public String getLandmark() {
		return this.landmark;
	}

	/**
	 * @param landmark
	 *            the landmark to set
	 */
	public void setLandmark(final String landmark) {
		this.landmark = landmark;
	}

	/**
	 * @return the lat
	 */
	public String getLat() {
		return this.lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(final String lat) {
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public String getLng() {
		return this.lng;
	}

	/**
	 * @param lng
	 *            the lng to set
	 */
	public void setLng(final String lng) {
		this.lng = lng;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return this.city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(final City city) {
		this.city = city;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return this.pincode;
	}

	/**
	 * @param pincode
	 *            the pincode to set
	 */
	public void setPincode(final String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return this.district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(final String district) {
		this.district = district;
	}

	/**
	 * @return the googlePlaceId
	 */
	public String getGooglePlaceId() {
		return this.googlePlaceId;
	}

	/**
	 * @param googlePlaceId
	 *            the googlePlaceId to set
	 */
	public void setGooglePlaceId(final String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(final User user) {
		this.user = user;
	}

	/**
	 * @return the distance
	 */
	public Double getDistance() {
		return this.distance;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(final Double distance) {
		this.distance = distance;
	}

}
