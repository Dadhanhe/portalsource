/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.enums.CommandCenterActionSource;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandCenterActionDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class CommandCenterActionDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The survey id. */
	private Integer surveyId;

	/** The survey survey status. */
	private String surveySurveyStatus;

	/** The survey surveyor name. */
	private String surveySurveyorName;

	/** The one to one user full name. */
	private String oneToOneUserFullName;

	/** The one to one id. */
	private Integer oneToOneId;

	/** The one to one oto type name. */
	private String oneToOneOtoTypeName;

	/** The one to one survey status. */
	private String oneToOneSurveyStatus;

	/** The video ID. */
	private Integer videoID;

	/** The agent id. */
	private Integer agentId;

	/** The agent full name. */
	private String agentFullName;

	/** The action type. */
	private String actionType;

	/** The notes. */
	private String notes;

	/** The command center action source. */
	private CommandCenterActionSource commandCenterActionSource;

	/**
	 * Gets the action type.
	 *
	 * @return the action type
	 */
	public String getActionType() {
		return this.actionType;
	}

	/**
	 * Sets the action type.
	 *
	 * @param actionType
	 *            the new action type
	 */
	public void setActionType(final String actionType) {
		this.actionType = actionType;
	}

	/**
	 * Gets the survey id.
	 *
	 * @return the survey id
	 */
	public Integer getSurveyId() {
		return this.surveyId;
	}

	/**
	 * Sets the survey id.
	 *
	 * @param surveyId
	 *            the new survey id
	 */
	public void setSurveyId(final Integer surveyId) {
		this.surveyId = surveyId;
	}

	/**
	 * Gets the survey survey status.
	 *
	 * @return the survey survey status
	 */
	public String getSurveySurveyStatus() {
		return this.surveySurveyStatus;
	}

	/**
	 * Sets the survey survey status.
	 *
	 * @param surveySurveyStatus
	 *            the new survey survey status
	 */
	public void setSurveySurveyStatus(final String surveySurveyStatus) {
		this.surveySurveyStatus = surveySurveyStatus;
	}

	/**
	 * Gets the one to one id.
	 *
	 * @return the one to one id
	 */
	public Integer getOneToOneId() {
		return this.oneToOneId;
	}

	/**
	 * Sets the one to one id.
	 *
	 * @param oneToOneId
	 *            the new one to one id
	 */
	public void setOneToOneId(final Integer oneToOneId) {
		this.oneToOneId = oneToOneId;
	}

	/**
	 * Gets the one to one oto type name.
	 *
	 * @return the one to one oto type name
	 */
	public String getOneToOneOtoTypeName() {
		return this.oneToOneOtoTypeName;
	}

	/**
	 * Sets the one to one oto type name.
	 *
	 * @param oneToOneOtoTypeName
	 *            the new one to one oto type name
	 */
	public void setOneToOneOtoTypeName(final String oneToOneOtoTypeName) {
		this.oneToOneOtoTypeName = oneToOneOtoTypeName;
	}

	/**
	 * Gets the one to one survey status.
	 *
	 * @return the one to one survey status
	 */
	public String getOneToOneSurveyStatus() {
		return this.oneToOneSurveyStatus;
	}

	/**
	 * Sets the one to one survey status.
	 *
	 * @param oneToOneSurveyStatus
	 *            the new one to one survey status
	 */
	public void setOneToOneSurveyStatus(final String oneToOneSurveyStatus) {
		this.oneToOneSurveyStatus = oneToOneSurveyStatus;
	}

	/**
	 * Gets the agent id.
	 *
	 * @return the agent id
	 */
	public Integer getAgentId() {
		return this.agentId;
	}

	/**
	 * Sets the agent id.
	 *
	 * @param agentId
	 *            the new agent id
	 */
	public void setAgentId(final Integer agentId) {
		this.agentId = agentId;
	}

	/**
	 * Gets the agent full name.
	 *
	 * @return the agent full name
	 */
	public String getAgentFullName() {
		return this.agentFullName;
	}

	/**
	 * Sets the agent full name.
	 *
	 * @param agentFullName
	 *            the new agent full name
	 */
	public void setAgentFullName(final String agentFullName) {
		this.agentFullName = agentFullName;
	}

	/**
	 * Gets the video ID.
	 *
	 * @return the video ID
	 */
	public Integer getVideoID() {
		return this.videoID;
	}

	/**
	 * Sets the video ID.
	 *
	 * @param videoID
	 *            the new video ID
	 */
	public void setVideoID(final Integer videoID) {
		this.videoID = videoID;
	}

	/**
	 * Gets the notes.
	 *
	 * @return the notes
	 */
	public String getNotes() {
		return this.notes;
	}

	/**
	 * Sets the notes.
	 *
	 * @param notes
	 *            the new notes
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Gets the survey surveyor name.
	 *
	 * @return the survey surveyor name
	 */
	public String getSurveySurveyorName() {
		return this.surveySurveyorName;
	}

	/**
	 * Sets the survey surveyor full name.
	 *
	 * @param surveySurveyorName
	 *            the new survey surveyor full name
	 */
	public void setSurveySurveyorFullName(final String surveySurveyorName) {
		this.surveySurveyorName = surveySurveyorName;
	}

	/**
	 * Gets the one to one user full name.
	 *
	 * @return the one to one user full name
	 */
	public String getOneToOneUserFullName() {
		return this.oneToOneUserFullName;
	}

	/**
	 * Sets the one to one user full name.
	 *
	 * @param oneToOneUserFullName
	 *            the new one to one user full name
	 */
	public void setOneToOneUserFullName(final String oneToOneUserFullName) {
		this.oneToOneUserFullName = oneToOneUserFullName;
	}

	/**
	 * Gets the command center action source.
	 *
	 * @return the command center action source
	 */
	public CommandCenterActionSource getCommandCenterActionSource() {
		return this.commandCenterActionSource;
	}

	/**
	 * Sets the command center action source.
	 *
	 * @param commandCenterActionSource
	 *            the new command center action source
	 */
	public void setCommandCenterActionSource(
			final CommandCenterActionSource commandCenterActionSource) {
		this.commandCenterActionSource = commandCenterActionSource;
	}

}
