/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import com.fasterxml.jackson.annotation.JsonValue;

// TODO: Auto-generated Javadoc
/**
 * The Enum CompanyType.
 */
public enum CompanyType {

	/** The normal. */
	NORMAL(0);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new company type.
	 *
	 * @param val
	 *            the val
	 */
	private CompanyType(int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	@JsonValue
	public int getValue() {
		return this.value;
	}
}
