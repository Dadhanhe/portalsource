/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

// TODO: Auto-generated Javadoc
/**
 * The Class PulseResponseBody.
 *
 * @param <T>
 *            the generic type
 */
public class PulseResponseBody<T> {

	/** The data. */
	T data;

	/** The status. */
	String status;

	/** The status code. */
	Integer statusCode;

	/** The message. */
	String message;

	/**
	 * Instantiates a new pulse response body.
	 *
	 * @param data
	 *            the data
	 * @param status
	 *            the status
	 */
	public PulseResponseBody(final T data, final String status) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.status = status;
	}

	/**
	 * Instantiates a new pulse response body.
	 *
	 * @param data
	 *            the data
	 * @param status
	 *            the status
	 * @param message
	 *            the message
	 */
	public PulseResponseBody(final T data, final String status,
			final String message) {
		// TODO Auto-generated constructor stub
		this.data = data;
		this.status = status;
		this.message = message;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public T getData() {
		return this.data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data
	 *            the new data
	 */
	public void setData(final T data) {
		this.data = data;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public Integer getStatusCode() {
		if ("success".equalsIgnoreCase(this.status)) {
			return 200;
		} else {
			return 500;
		}
	}

	/**
	 * Sets the status code.
	 *
	 * @param statusCode
	 *            the new status code
	 */
	public void setStatusCode(final Integer statusCode) {
		if ("success".equalsIgnoreCase(this.status)) {
			this.statusCode = 200;
		} else {
			this.statusCode = 500;
		}
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}
}
