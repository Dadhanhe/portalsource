/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum TrainingStatus.
 */
public enum TrainingStatus {

	/** The Completed. */
	Completed(0),

	/** The Pending. */
	Pending(1),

	/** The Incomplete. */
	Incomplete(2);

	/** The value. */
	private final int value;

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the training status
	 */
	public static TrainingStatus fromInteger(final Integer value) {
		if (value == Completed.value) {
			return Completed;
		} else if (value == Pending.value) {
			return Pending;
		} else if (value == Incomplete.value) {
			return Incomplete;
		}
		return null;
	}

	/**
	 * Instantiates a new training status.
	 *
	 * @param value
	 *            the value
	 */
	private TrainingStatus(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

}
