/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum EmailType.
 */
public enum EmailType {

	/** The Reset password. */
	ResetPassword(0),

	/** The Metrics reminder. */
	MetricsReminder(1),

	/** The Individual metrics reminder. */
	IndividualMetricsReminder(2),

	/** The Invitation. */
	Invitation(3),

	/** The Feedback. */
	Feedback(4),

	/** The User register. */
	UserRegister(5),

	/** The User imported. */
	UserImported(6),

	/** The Email report. */
	EmailReport(7),

	/** The Upload metric error report. */
	UploadMetricErrorReport(8),

	/** The Upload metric completed. */
	UploadMetricCompleted(9),

	/** The Task creation. */
	TaskCreation(10),

	/** The Task status update. */
	TaskStatusUpdate(11),

	/** The Task unassigned. */
	TaskUnassigned(12),

	/** The Task assigned. */
	TaskAssigned(13),

	/** The Counter coaching taken. */
	CounterCoachingTaken(14),

	/** The One to one scheduled. */
	OneToOneScheduled(15),

	/** The One to one taken. */
	OneToOneTaken(16),

	/** The One to one schedule cancel. */
	OneToOneScheduleCancel(17),

	/** The Send sessions mail. */
	SendSessionsMail(18),

	/** The One to one reminder. */
	OneToOneReminder(19),

	/** The Counter coaching schedule cancel. */
	CounterCoachingScheduleCancel(20),

	/** The Audit product denied. */
	AuditProductDenied(21),

	/** The Audit individual metric denied. */
	AuditIndividualMetricDenied(22),

	/** The Audit report scheduler. */
	AuditReportScheduler(23),

	/** The Financial report scheduler. */
	FinancialReportScheduler(24),

	/** The Counter coaching scheduled. */
	CounterCoachingScheduled(25),

	/** The Login reminder. */
	LoginReminder(26),

	/** The Observation taken. */
	ObservationTaken(27),

	/** The Observation schedule cancel. */
	ObservationScheduleCancel(28),

	/** The Observation scheduled. */
	ObservationScheduled(29),

	/** The User activation. */
	UserActivation(30),

	/** The Promo code. */
	PromoCode(31),

	/** The File not processing utility. */
	FileNotProcessingUtility(32),

	/** The Email not read utility. */
	EmailNotReadUtility(33),

	/** The Ecommerce order. */
	EcommerceOrder(34),

	/** The User media recommendation. */
	UserMediaRecommendation(35),

	/** The User media recommendation watched. */
	UserMediaRecommendationWatched(36),

	/** The Send meeting invite. */
	SendMeetingInvite(37),

	/** The Cancel meeting invite. */
	CancelMeetingInvite(38),

	/** The Update meeting invite. */
	UpdateMeetingInvite(39),

	/** The Custom report. */
	CustomReport(40),

	/** The Training expectations report. */
	TrainingExpectationsReport(41),

	/** The Progress report. */
	ProgressReport(42),

	/** The Certifications report. */
	CertificationsReport(43),

	/** The One to one remove user. */
	OneToOneRemoveUser(44),

	/** The Meeting remove user. */
	MeetingRemoveUser(45),

	/** The Goal cast. */
	GoalCast(46),

	/** The Goal update. */
	GoalUpdate(47),

	/** The VPL activity approval. */
	VPLActivityApproval(48),

	/** The Utility error report. */
	UtilityErrorReport(49),

	/** The Product metric data missing report. */
	ProductMetricDataMissingReport(50),

	/** The Training assign. */
	TrainingAssign(51),

	/** The Training complete. */
	TrainingComplete(52),

	/** The Metrics locking report. */
	MetricsLockingReport(53),

	/** The Report transformation failed. */
	ReportTransformationFailed(54),

	/** The Light table dashboard scheduler report. */
	LightTableDashboardSchedulerReport(55),

	/** The Product dashboard report. */
	ProductDashboardReport(56),

	/** The Location report scheduler. */
	LocationReportScheduler(57),

	/** The Meeting scheduled. */
	MeetingScheduled(58),

	/** The Meeting taken. */
	MeetingTaken(59),

	/** The Meeting schedule cancel. */
	MeetingScheduleCancel(60),

	/** The Meeting reminder. */
	MeetingReminder(61),

	/** The Goal setting. */
	GoalSetting(62),

	/** The Goal progress weekly update. */
	GoalProgressWeeklyUpdate(63),

	/** The Goal progress monthly update. */
	GoalProgressMonthlyUpdate(64),

	/** The Sent email process failed. */
	SentEmailProcessFailed(65),

	/** The Pulse ID changed. */
	PulseIDChanged(66),

	/** The Contest. */
	Contest(67),

	/** The User team assign via import. */
	UserTeamAssignViaImport(68),

	/** The User not assign to team via import. */
	UserNotAssignToTeamViaImport(69),

	/** The Product metrics status update. */
	ProductMetricsUpdateErrorRecords(70),

	/** The Goal acceptance reminder. */
	GoalAcceptanceReminder(71),

	/** The Goal accepted. */
	GoalAccepted(72),

	/** The ML one 2 one recommendations email. */
	MLOne2OneRecommendationsEmail(73),

	/** The ML video recommendations email. */
	MLVideoRecommendationsEmail(74),

	/** The Auth expired email. */
	AuthExpiredEmail(75),
	/** The Audit trail update. */
	AuditTrailUpdate(76),

	/** The Blue print score change. */
	BluePrintScoreChange(77),

	/** The Blue print not updated. */
	BluePrintNotUpdated(78),

	/** The Onboarding invitation. */
	OnboardingInvitation(79),

	/** The Data upload utility status. */
	DataUploadUtilityStatus(80),

	/** The Onboard. */
	Onboarding(81),

	/** The Comments feed. */
	CommentsFeed(82),

	/** The Beta tester. */
	BetaTester(83),

	/** The Feed. */
	Feed(84),

	/** The Import tenant product. */
	ImportTenantProduct(85),

/** The Bulk export. */

	BulkExport(86);
	


	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the email type
	 */
	public static EmailType fromInteger(final Integer value) {
		if (value == ResetPassword.value) {
			return ResetPassword;
		} else if (value == MetricsReminder.value) {
			return MetricsReminder;
		} else if (value == IndividualMetricsReminder.value) {
			return IndividualMetricsReminder;
		} else if (value == Invitation.value) {
			return Invitation;
		} else if (value == Feedback.value) {
			return Feedback;
		} else if (value == UserRegister.value) {
			return UserRegister;
		} else if (value == UserImported.value) {
			return UserImported;
		} else if (value == UploadMetricErrorReport.value) {
			return UploadMetricErrorReport;
		} else if (value == UploadMetricCompleted.value) {
			return UploadMetricCompleted;
		} else if (value == TaskCreation.value) {
			return TaskCreation;
		} else if (value == TaskStatusUpdate.value) {
			return TaskStatusUpdate;
		} else if (value == TaskUnassigned.value) {
			return TaskUnassigned;
		} else if (value == TaskAssigned.value) {
			return TaskAssigned;
		} else if (value == CounterCoachingTaken.value) {
			return CounterCoachingTaken;
		} else if (value == OneToOneScheduled.value) {
			return OneToOneScheduled;
		} else if (value == OneToOneTaken.value) {
			return OneToOneTaken;
		} else if (value == OneToOneScheduleCancel.value) {
			return OneToOneScheduleCancel;
		} else if (value == OneToOneReminder.value) {
			return OneToOneReminder;
		} else if (value == SendSessionsMail.value) {
			return SendSessionsMail;
		} else if (value == CounterCoachingScheduleCancel.value) {
			return CounterCoachingScheduleCancel;
		} else if (value == AuditProductDenied.value) {
			return AuditProductDenied;
		} else if (value == AuditIndividualMetricDenied.value) {
			return AuditIndividualMetricDenied;
		} else if (value == AuditReportScheduler.value) {
			return AuditReportScheduler;
		} else if (value == FinancialReportScheduler.value) {
			return FinancialReportScheduler;
		} else if (value == CounterCoachingScheduled.value) {
			return CounterCoachingScheduled;
		} else if (value == LoginReminder.value) {
			return LoginReminder;
		} else if (value == ObservationTaken.value) {
			return ObservationTaken;
		} else if (value == ObservationScheduleCancel.value) {
			return ObservationScheduleCancel;
		} else if (value == ObservationScheduled.value) {
			return ObservationScheduled;
		} else if (value == FileNotProcessingUtility.value) {
			return FileNotProcessingUtility;
		} else if (value == EmailNotReadUtility.value) {
			return EmailNotReadUtility;
		} else if (value == PromoCode.value) {
			return PromoCode;
		} else if (value == EcommerceOrder.value) {
			return EcommerceOrder;
		} else if (value == UserActivation.value) {
			return UserActivation;
		} else if (value == UserMediaRecommendation.value) {
			return UserMediaRecommendation;
		} else if (value == UserMediaRecommendationWatched.value) {
			return UserMediaRecommendationWatched;
		} else if (value == SendMeetingInvite.value) {
			return SendMeetingInvite;
		} else if (value == CancelMeetingInvite.value) {
			return CancelMeetingInvite;
		} else if (value == UpdateMeetingInvite.value) {
			return UpdateMeetingInvite;
		} else if (value == CustomReport.value) {
			return CustomReport;
		} else if (value == TrainingExpectationsReport.value) {
			return TrainingExpectationsReport;
		} else if (value == ProgressReport.value) {
			return ProgressReport;
		} else if (value == CertificationsReport.value) {
			return CertificationsReport;
		} else if (value == OneToOneRemoveUser.value) {
			return OneToOneRemoveUser;
		} else if (value == MeetingRemoveUser.value) {
			return MeetingRemoveUser;
		} else if (value == GoalCast.value) {
			return GoalCast;
		} else if (value == GoalUpdate.value) {
			return GoalUpdate;
		} else if (value == VPLActivityApproval.value) {
			return VPLActivityApproval;
		} else if (value == UtilityErrorReport.value) {
			return UtilityErrorReport;
		} else if (value == ProductMetricDataMissingReport.value) {
			return ProductMetricDataMissingReport;
		} else if (value == MetricsLockingReport.value) {
			return MetricsLockingReport;
		} else if (value == TrainingAssign.value) {
			return TrainingAssign;
		} else if (value == TrainingComplete.value) {
			return TrainingComplete;
		} else if (value == ReportTransformationFailed.value) {
			return ReportTransformationFailed;
		} else if (value == Contest.value) {
			return Contest;
		} else if (value == UserTeamAssignViaImport.value) {
			return UserTeamAssignViaImport;
		} else if (value == MLOne2OneRecommendationsEmail.value) {
			return MLOne2OneRecommendationsEmail;
		} else if (value == MLVideoRecommendationsEmail.value) {
			return MLVideoRecommendationsEmail;
		} else if (value == ProductMetricsUpdateErrorRecords.value) {
			return ProductMetricsUpdateErrorRecords;
		} else if (value == GoalAcceptanceReminder.value) {
			return GoalAcceptanceReminder;
		} else if (value == GoalAccepted.value) {
			return GoalAccepted;
		} else if (value == AuthExpiredEmail.value) {
			return AuthExpiredEmail;
		} else if (value == AuditTrailUpdate.value) {
			return AuditTrailUpdate;
		} else if (value == BluePrintScoreChange.value) {
			return BluePrintScoreChange;
		} else if (value == OnboardingInvitation.value) {
			return OnboardingInvitation;
		} else if (value == BluePrintNotUpdated.value) {
			return BluePrintNotUpdated;
		} else if (value == DataUploadUtilityStatus.value) {
			return DataUploadUtilityStatus;
		} else if (value == Onboarding.value) {
			return Onboarding;
		} else if (value == CommentsFeed.value) {
			return CommentsFeed;
		} else if (value == BetaTester.value) {
			return BetaTester;
		} else if (value == Feed.value) {
			return Feed;
		} else if (value == ImportTenantProduct.value) {
			return ImportTenantProduct;
		} else if (value == BulkExport.value) {
			return BulkExport;
		}

		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new email type.
	 *
	 * @param value
	 *            the value
	 */
	private EmailType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("email.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
