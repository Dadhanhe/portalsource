/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum PreferenceType.
 */
public enum PreferenceType {

	/** The Preference. */
	Preference(0),

	/** The Label. */
	Label(1);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the preference type
	 */
	public static PreferenceType fromInteger(final Integer value) {
		if (value == Preference.value) {
			return Preference;
		} else if (value == Label.value) {
			return Label;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new preference type.
	 *
	 * @param value
	 *            the value
	 */
	private PreferenceType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
