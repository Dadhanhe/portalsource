/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc

/**
 * The Enum SurveyEntityType.
 */
public enum SurveyEntityType {

	/** The Observation. */
	Observation(0),

	/** The Counter coaching. */
	CounterCoaching(1),

	/** The Contact report. */
	ContactReport(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the survey entity type
	 */
	public static SurveyEntityType fromInteger(final Integer value) {
		if (value == Observation.value) {
			return Observation;
		} else if (value == CounterCoaching.value) {
			return CounterCoaching;
		} else if (value == ContactReport.value) {
			return ContactReport;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new survey entity type.
	 *
	 * @param value
	 *            the value
	 */
	private SurveyEntityType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("surveyEntityType.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Observation.value) {
			return "Observation";
		} else if (value == CounterCoaching.value) {
			return "CounterCoaching";
		} else if (value == ContactReport.value) {
			return "ContactReport";
		}
		return null;
	}

}
