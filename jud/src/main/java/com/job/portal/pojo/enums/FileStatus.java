/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum FileStatus.
 */
public enum FileStatus {

	/** The Pending. */
	Pending(0),

	/** The Processed. */
	Processed(1),

	/** The Normal. */
	Normal(2),

	/** The Error. */
	Error(3),

	/** The Queued. */
	Queued(4),

	/** The Archive. */
	Archive(6),

	/** The In progress. */
	InProgress(7);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the file status
	 */
	public static FileStatus fromInteger(final Integer value) {
		if (value == Pending.value) {
			return Pending;
		} else if (value == Processed.value) {
			return Processed;
		} else if (value == Archive.value) {
			return Archive;
		} else if (value == Error.value) {
			return Error;
		} else if (value == Normal.value) {
			return Normal;
		} else if (value == Queued.value) {
			return Queued;
		} else if (value == InProgress.value) {
			return InProgress;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new file status.
	 *
	 * @param value
	 *            the value
	 */
	private FileStatus(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Pending.value) {
			return "Pending";
		} else if (value == Processed.value) {
			return "Processed";
		} else if (value == Normal.value) {
			return "Normal";
		} else if (value == Error.value) {
			return "Error";
		} else if (value == Queued.value) {
			return "Queued";
		} else if (value == Archive.value) {
			return "Archive";
		} else if (value == InProgress.value) {
			return "In-progress";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static FileStatus getEnum(final String status) {
		if (status != null) {
			if (status.equals("Pending")) {
				return FileStatus.Pending;
			} else if (status.equals("Processed")) {
				return FileStatus.Processed;
			} else if (status.equals("Normal")) {
				return FileStatus.Normal;
			} else if (status.equals("Error")) {
				return FileStatus.Error;
			} else if (status.equals("Queued")) {
				return FileStatus.Queued;
			} else if (status.equals("Archive")) {
				return FileStatus.Archive;
			} else if (status.equals("In-progress")) {
				return FileStatus.InProgress;
			}
		}
		return null;
	}
}
