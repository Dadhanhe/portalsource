/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum MachineLearningRecommendationType.
 */
public enum MachineLearningRecommendationType {

	/** The onetoone. */
	ONETOONE(0),
	/** The video. */
	VIDEO(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new machine learning recommendation type.
	 *
	 * @param val
	 *            the val
	 */
	private MachineLearningRecommendationType(final int val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}
}
