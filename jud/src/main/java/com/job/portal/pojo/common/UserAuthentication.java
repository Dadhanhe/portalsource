/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

// TODO: Auto-generated Javadoc
/**
 * The Class UserAuthentication.
 */
public class UserAuthentication {

	/** The client id. */
	private String client_id;

	/** The client secret. */
	private String client_secret;

	/** The grant type. */
	private String grant_type;

	/** The username. */
	private String username;

	/** The password. */
	private String password;

	/** The device id. */
	private String deviceId;

	/** The platform. */
	private Integer platform;

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClient_id() {
		return this.client_id;
	}

	/**
	 * Sets the client id.
	 *
	 * @param client_id
	 *            the new client id
	 */
	public void setClient_id(final String client_id) {
		this.client_id = client_id;
	}

	/**
	 * Gets the client secret.
	 *
	 * @return the client secret
	 */
	public String getClient_secret() {
		return this.client_secret;
	}

	/**
	 * Sets the client secret.
	 *
	 * @param client_secret
	 *            the new client secret
	 */
	public void setClient_secret(final String client_secret) {
		this.client_secret = client_secret;
	}

	/**
	 * Gets the grant type.
	 *
	 * @return the grant type
	 */
	public String getGrant_type() {
		return this.grant_type;
	}

	/**
	 * Sets the grant type.
	 *
	 * @param grant_type
	 *            the new grant type
	 */
	public void setGrant_type(final String grant_type) {
		this.grant_type = grant_type;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username
	 *            the new username
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Gets the device id.
	 *
	 * @return the device id
	 */
	public String getDeviceId() {
		return this.deviceId;
	}

	/**
	 * Sets the device id.
	 *
	 * @param deviceId
	 *            the new device id
	 */
	public void setDeviceId(final String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * Gets the platform.
	 *
	 * @return the platform
	 */
	public Integer getPlatform() {
		return this.platform;
	}

	/**
	 * Sets the platform.
	 *
	 * @param platform
	 *            the new platform
	 */
	public void setPlatform(final Integer platform) {
		this.platform = platform;
	}

}
