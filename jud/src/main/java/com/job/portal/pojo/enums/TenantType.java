/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Enum TenantType.
 */
public enum TenantType implements Serializable {

	/** The Normal. */
	Normal(0),

	/** The Virtual. */
	Virtual(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new tenant type.
	 *
	 * @param value
	 *            the value
	 */
	private TenantType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the tenant type
	 */
	public static TenantType fromInteger(final Integer value) {
		if (value == Normal.value) {
			return Normal;
		} else if (value == Virtual.value) {
			return Virtual;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Normal.value) {
			return "Normal";
		} else if (value == Virtual.value) {
			return "Virtual";
		}
		return null;
	}

}
