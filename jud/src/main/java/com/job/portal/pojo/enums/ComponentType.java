/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum ComponentType.
 */
public enum ComponentType {

	/** The Employee dashboard. */
	EmployeeDashboard(0),

	/** The Financial dashboard. */
	FinancialDashboard(1),

	/** The PCR leaderboard. */
	PCRLeaderboard(2),

	/** The all. */
	ALL(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the component type
	 */
	public static ComponentType fromInteger(final Integer value) {
		if (value == EmployeeDashboard.value) {
			return EmployeeDashboard;
		} else if (value == FinancialDashboard.value) {
			return FinancialDashboard;
		} else if (value == PCRLeaderboard.value) {
			return PCRLeaderboard;
		} else if (value == ALL.value) {
			return ALL;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new component type.
	 *
	 * @param value
	 *            the value
	 */
	private ComponentType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("component.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == EmployeeDashboard.value) {
			return "EmployeeDashboard";
		} else if (value == FinancialDashboard.value) {
			return "FinancialDashboard";
		} else if (value == PCRLeaderboard.value) {
			return "PCRLeaderboard";
		} else if (value == ALL.value) {
			return "ALL";
		}
		return null;
	}

	/**
	 * Gets the value by component.
	 *
	 * @param code
	 *            the code
	 * @return the value by component
	 */
	public static int getValueByComponent(final String code) {
		for (int i = 0; i < ComponentType.values().length; i++) {
			if (code.equalsIgnoreCase(ComponentType.values()[i].name())) {
				return ComponentType.values()[i].value();
			}
		}
		return -1;
	}
}
