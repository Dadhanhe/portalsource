/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum Platform.
 */
public enum Platform {

	/** The i OS. */
	iOS(0),

	/** The Android. */
	Android(1),

	/** The Web. */
	Web(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the platform
	 */
	public static Platform fromInteger(final Integer value) {
		if (value == iOS.value) {
			return iOS;
		} else if (value == Android.value) {
			return Android;
		} else if (value == Web.value) {
			return Web;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new platform.
	 *
	 * @param value
	 *            the value
	 */
	private Platform(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("platform.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
