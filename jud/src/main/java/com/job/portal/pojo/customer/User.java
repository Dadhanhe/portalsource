/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

// Generated Oct 5, 2016 2:02:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.enums.Gender;
import com.job.portal.pojo.enums.Status;
import com.job.portal.utils.ValidationUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "user")
public class User extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1527816557995047751L;

	/** The email. */
	@Valid
	@Pattern(regexp = ValidationUtils.EmailRegex)
	@Size(max = 100)
	private String email;

	/** The password. */
	private String password;

	/** The access token. */
	private String accessToken;

	/** The salt. */
	private String salt;

	/** The first name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{1,50}")
	private String firstName;

	/** The middle name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{0,50}")
	private String middleName;

	/** The last name. */
	@Valid
	@Pattern(regexp = "[\\w\\s\\`\\'\\.\\-\\u0080-\\u00FF]{1,50}")
	private String lastName;

	/** The gender. */
	@NotNull
	private Gender gender;

	/** The phone number. */
	@Valid
	@Pattern(regexp = "[\\d\\s\\-\\(\\)]{0,20}")
	private String phoneNumber;

	/** The authy id. */
	private String authyId;

	/** The is authy attempted. */
	private Boolean isAuthyAttempted;

	private String saluation;

	/** The birth month. */
	private Integer birthMonth;

	/** The birth day. */
	private Integer birthDay;

	private Integer nationality;

	private double currentSalary;

	private double expectedSalary;

	private Integer noticePeriod;

	private String affirmative;

	private String materialStatus;

	private String userUid;

	/**
	 * Instantiates a new user.
	 *
	 * @param id
	 *            the id
	 */

	/**
	 * Instantiates a new user.
	 */
	public User() {
		this.gender = Gender.Male;
		this.activeStatus = Status.Active;
		this.salt = DigestUtils.md5Hex(UUID.randomUUID().toString());
		this.firstName = "";
		this.lastName = "";
		this.middleName = "";
		this.phoneNumber = "";
		this.isAuthyAttempted = false;
	}

	/**
	 * Sets the default properties.
	 */
	@PostConstruct
	void setDefaultProperties() {
		this.gender = Gender.Male;
		this.activeStatus = Status.Active;
		this.salt = DigestUtils.md5Hex(UUID.randomUUID().toString());
		this.middleName = "";
		this.phoneNumber = "";
		this.isAuthyAttempted = false;

	}

	public User(final Integer id) {
		this.id = id;
		this.gender = Gender.Male;
		this.activeStatus = Status.Active;
		this.salt = DigestUtils.md5Hex(UUID.randomUUID().toString());
		this.middleName = "";
		this.phoneNumber = "";
		this.isAuthyAttempted = false;
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param email
	 *            the email
	 * @param password
	 *            the password
	 * @param status
	 *            the status
	 * @param salt
	 *            the salt
	 * @param createdBy
	 *            the created by
	 * @param updatedBy
	 *            the updated by
	 * @param createdOn
	 *            the created on
	 * @param updatedOn
	 *            the updated on
	 * @param dashboardUrl
	 *            the dashboard url
	 */
	public User(final String email, final String password, final Status status,
			final String salt, final User createdBy, final User updatedBy,
			final Date createdOn, final Date updatedOn,
			final String dashboardUrl) {
		this.email = email;
		this.password = password;
		this.activeStatus = status;
		this.salt = salt;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.gender = Gender.Male;
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param email
	 *            the email
	 * @param password
	 *            the password
	 * @param status
	 *            the status
	 * @param accessToken
	 *            the access token
	 * @param tokenExpiry
	 *            the token expiry
	 * @param salt
	 *            the salt
	 * @param signedInTime
	 *            the signed in time
	 * @param createdBy
	 *            the created by
	 * @param updatedBy
	 *            the updated by
	 * @param createdOn
	 *            the created on
	 * @param updatedOn
	 *            the updated on
	 * @param dashboardUrl
	 *            the dashboard url
	 */
	public User(final String email, final String password, final Status status,
			final String accessToken, final Date tokenExpiry, final String salt,
			final Date signedInTime, final User createdBy, final User updatedBy,
			final Date createdOn, final Date updatedOn,
			final String dashboardUrl) {
		this.email = email;
		this.password = password;
		this.activeStatus = status;
		this.accessToken = accessToken;
		this.salt = salt;

		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.gender = Gender.Male;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	@Column(name = "EMAIL", nullable = false, length = 100)
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	@Column(name = "PASSWORD", nullable = false, length = 100)
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Gets the active status.
	 *
	 * @return the active status
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getActiveStatus()
	 */
	@Override
	@Column(name = "active_status", nullable = false)
	public Status getActiveStatus() {
		return this.activeStatus;
	}

	/**
	 * Sets the active status.
	 *
	 * @param activeStatus
	 *            the new active status
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setActiveStatus(com.fpg.
	 * pulse.pojo.enums.Status)
	 */
	@Override
	public void setActiveStatus(final Status activeStatus) {
		this.activeStatus = activeStatus;
	}

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	@Column(name = "ACCESS_TOKEN", length = 32)
	public String getAccessToken() {
		return this.accessToken;
	}

	/**
	 * Sets the access token.
	 *
	 * @param accessToken
	 *            the new access token
	 */
	public void setAccessToken(final String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * Gets the salt.
	 *
	 * @return the salt
	 */
	@Column(name = "SALT", nullable = false, length = 45)
	public String getSalt() {
		return this.salt;
	}

	/**
	 * Sets the salt.
	 *
	 * @param salt
	 *            the new salt
	 */
	public void setSalt(final String salt) {
		this.salt = salt;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getCreatedBy()
	 */
	@Override
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATED_BY", nullable = false)
	public User getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy
	 *            the new created by
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setCreatedBy(java.lang.
	 * Integer)
	 */
	@Override
	public void setCreatedBy(final User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updated by.
	 *
	 * @return the updated by
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getUpdatedBy()
	 */
	@Override
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UPDATED_BY", nullable = false)
	public User getUpdatedBy() {
		return this.updatedBy;
	}

	/**
	 * Sets the updated by.
	 *
	 * @param updatedBy
	 *            the new updated by
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setUpdatedBy(java.lang.
	 * Integer)
	 */
	@Override
	public void setUpdatedBy(final User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getCreatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON", nullable = false, length = 19)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn
	 *            the new created on
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setCreatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setCreatedOn(final Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the updated on.
	 *
	 * @return the updated on
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see com.fpg.pulse.pojo.common.AbstractValueObject#getUpdatedOn()
	 */
	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	/**
	 * Sets the updated on.
	 *
	 * @param updatedOn
	 *            the new updated on
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.fpg.pulse.pojo.common.AbstractValueObject#setUpdatedOn(java.util.
	 * Date)
	 */
	@Override
	public void setUpdatedOn(final Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the middle name.
	 *
	 * @return the middle name
	 */
	@Column(name = "MIDDLE_NAME", length = 50)
	public String getMiddleName() {
		return this.middleName;
	}

	/**
	 * Sets the middle name.
	 *
	 * @param middleName
	 *            the new middle name
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	@Column(name = "LAST_NAME", nullable = false, length = 50)
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	@Column(name = "GENDER", nullable = false)
	public Gender getGender() {
		return this.gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender
	 *            the new gender
	 */
	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	@Column(name = "PHONE_NUMBER", length = 50)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber
	 *            the new phone number
	 */
	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the authy id.
	 *
	 * @return the authy id
	 */
	@Column(name = "AUTHY_ID", length = 45)
	public String getAuthyId() {
		return this.authyId;
	}

	/**
	 * Sets the authy id.
	 *
	 * @param authyId
	 *            the new authy id
	 */
	public void setAuthyId(final String authyId) {
		this.authyId = authyId;
	}

	/**
	 * Gets the checks if is authy attempted.
	 *
	 * @return the checks if is authy attempted
	 */
	@Column(name = "IS_AUTHY_ATTEMPTED")
	public Boolean getIsAuthyAttempted() {
		return this.isAuthyAttempted;
	}

	/**
	 * Sets the checks if is authy attempted.
	 *
	 * @param isAuthyAttempted
	 *            the new checks if is authy attempted
	 */
	public void setIsAuthyAttempted(final Boolean isAuthyAttempted) {
		this.isAuthyAttempted = isAuthyAttempted;
	}

	@Column(name = "SALUATION")
	public String getSaluation() {
		return this.saluation;
	}

	public void setSaluation(final String saluation) {
		this.saluation = saluation;
	}

	@Column(name = "birthMonth")
	public Integer getBirthMonth() {
		return this.birthMonth;
	}

	public void setBirthMonth(final Integer birthMonth) {
		this.birthMonth = birthMonth;
	}

	@Column(name = "birthDay")
	public Integer getBirthDay() {
		return this.birthDay;
	}

	public void setBirthDay(final Integer birthDay) {
		this.birthDay = birthDay;
	}

	@Column(name = "NATIONALITY")
	public Integer getNationality() {
		return this.nationality;
	}

	public void setNationality(final Integer nationality) {
		this.nationality = nationality;
	}

	@Column(name = "CURRENT_SALARY")
	public double getCurrentSalary() {
		return this.currentSalary;
	}

	public void setCurrentSalary(final double currentSalary) {
		this.currentSalary = currentSalary;
	}

	@Column(name = "EXPECTED_SALARY")
	public double getExpectedSalary() {
		return this.expectedSalary;
	}

	public void setExpectedSalary(final double expectedSalary) {
		this.expectedSalary = expectedSalary;
	}

	@Column(name = "NOTICE_PERIOD")
	public Integer getNoticePeriod() {
		return this.noticePeriod;
	}

	public void setNoticePeriod(final Integer noticePeriod) {
		this.noticePeriod = noticePeriod;
	}

	@Column(name = "AFFIRMATIVE")
	public String getAffirmative() {
		return this.affirmative;
	}

	public void setAffirmative(final String affirmative) {
		this.affirmative = affirmative;
	}

	@Column(name = "MATERIAL_STATUS")
	public String getMaterialStatus() {
		return this.materialStatus;
	}

	public void setMaterialStatus(final String materialStatus) {
		this.materialStatus = materialStatus;
	}

	@Column(name = "USERUID")
	public String getUserUid() {
		return this.userUid;
	}

	public void setUserUid(final String userUid) {
		this.userUid = userUid;
	}

}
