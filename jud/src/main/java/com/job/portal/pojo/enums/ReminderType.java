/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum ReminderType.
 */
public enum ReminderType {

	/** The Missing metrics. */
	MissingMetrics(0),

	/** The Missing individual metrics. */
	MissingIndividualMetrics(1),

	/** The Missing product metrics. */
	MissingProductMetrics(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the reminder type
	 */
	public static ReminderType fromInteger(Integer value) {
		if (value == MissingMetrics.value) {
			return MissingMetrics;
		} else if (value == MissingIndividualMetrics.value) {
			return MissingIndividualMetrics;
		} else if (value == MissingProductMetrics.value) {
			return MissingProductMetrics;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new reminder type.
	 *
	 * @param value
	 *            the value
	 */
	private ReminderType(int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("reminder.type.%d", value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return value;
	}
}
