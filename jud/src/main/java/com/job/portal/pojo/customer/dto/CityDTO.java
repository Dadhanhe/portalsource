/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;

/**
 * @author Vaibhav
 *
 */
@JsonDeserialize(using = DTOSerializer.class)
public class CityDTO extends AbstractDTO {

	private Integer stateId;

	private String stateName;

	private String name;

	private String code;

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return this.stateId;
	}

	/**
	 * @param stateId
	 *            the stateId to set
	 */
	public void setStateId(final Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * @param stateName
	 *            the stateName to set
	 */
	public void setStateName(final String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

}
