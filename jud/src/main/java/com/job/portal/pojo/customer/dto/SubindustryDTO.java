/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import com.job.portal.pojo.common.dto.AbstractDTO;

/**
 * The Class IndustryDTO.
 */
public class SubindustryDTO extends AbstractDTO {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The name. */
	String name;

	/** The industry id. */
	Integer industryId;

	/** The industry name. */
	String industryName;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the industry id.
	 *
	 * @return the industry id
	 */
	public Integer getIndustryId() {
		return this.industryId;
	}

	/**
	 * Sets the industry id.
	 *
	 * @param industryId
	 *            the new industry id
	 */
	public void setIndustryId(final Integer industryId) {
		this.industryId = industryId;
	}

	/**
	 * Gets the industry name.
	 *
	 * @return the industry name
	 */
	public String getIndustryName() {
		return this.industryName;
	}

	/**
	 * Sets the industry name.
	 *
	 * @param industryName
	 *            the new industry name
	 */
	public void setIndustryName(final String industryName) {
		this.industryName = industryName;
	}

}
