/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum GoalMetricType.
 */
public enum GoalMetricType {
	// Used For Recommended Goal only.
	/** The daily arrivals. */
	DAILY_ARRIVALS(0),

	/** The guest average stay. */
	GUEST_AVERAGE_STAY(1),

	/** The average upsells. */
	AVERAGE_UPSELLS(2),

	/** The average upsells revenue pd pa. */
	AVERAGE_UPSELLS_REVENUE_PD_PA(3),

	/** The upsell room revenue goal. */
	UPSELL_ROOM_REVENUE_GOAL(4),

	/** The other product goal. */
	OTHER_PRODUCT_GOAL(5);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new goal metric type.
	 *
	 * @param value
	 *            the value
	 */
	private GoalMetricType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the goal metric type
	 */
	public static GoalMetricType fromInteger(final Integer value) {
		if (value == DAILY_ARRIVALS.value) {
			return DAILY_ARRIVALS;
		} else if (value == GUEST_AVERAGE_STAY.value) {
			return GUEST_AVERAGE_STAY;
		} else if (value == AVERAGE_UPSELLS.value) {
			return AVERAGE_UPSELLS;
		} else if (value == AVERAGE_UPSELLS_REVENUE_PD_PA.value) {
			return AVERAGE_UPSELLS_REVENUE_PD_PA;
		} else if (value == UPSELL_ROOM_REVENUE_GOAL.value) {
			return UPSELL_ROOM_REVENUE_GOAL;
		} else if (value == OTHER_PRODUCT_GOAL.value) {
			return OTHER_PRODUCT_GOAL;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == DAILY_ARRIVALS.value) {
			return "No of Arrivals Per Month Per Agent";
		} else if (value == GUEST_AVERAGE_STAY.value) {
			return "Length of Stay";
		} else if (value == AVERAGE_UPSELLS.value) {
			return "No of Upsells";
		} else if (value == AVERAGE_UPSELLS_REVENUE_PD_PA.value) {
			return "Average Upsell Revenue";
		} else if (value == UPSELL_ROOM_REVENUE_GOAL.value) {
			return "Upsell Room Revenue Goal";
		} else if (value == OTHER_PRODUCT_GOAL.value) {
			return "Other Product Revenue Goal";
		}
		return null;
	}

}
