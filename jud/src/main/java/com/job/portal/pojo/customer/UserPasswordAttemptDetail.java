/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.job.portal.pojo.common.AbstractValueObject;

// TODO: Auto-generated Javadoc
/**
 * The Class UserPasswordAttemptDetail.
 */
@Entity
@Table(name = "user_password_attempt_detail")
public class UserPasswordAttemptDetail extends AbstractValueObject {

	/** The user id. */
	Integer userId;

	/** The is locked. */
	private Boolean isLocked;

	/** The login failed count. */
	private Integer loginFailedCount;

	/** The last attempt. */
	private Date lastAttempt;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;

	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name = "USER_ID", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */

	public void setUserId(final Integer userId) {
		this.userId = userId;
	}

	/**
	 * Gets the checks if is locked.
	 *
	 * @return the checks if is locked
	 */
	@Column(name = "isLocked")
	public Boolean getIsLocked() {
		return this.isLocked;
	}

	/**
	 * Sets the checks if is locked.
	 *
	 * @param isLocked
	 *            the new checks if is locked
	 */
	public void setIsLocked(final Boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * Gets the login failed count.
	 *
	 * @return the login failed count
	 */
	@Column(name = "loginFailedCount")
	public Integer getLoginFailedCount() {
		return this.loginFailedCount;
	}

	/**
	 * Sets the login failed count.
	 *
	 * @param loginFailedCount
	 *            the new login failed count
	 */
	public void setLoginFailedCount(final Integer loginFailedCount) {
		this.loginFailedCount = loginFailedCount;
	}

	/**
	 * Gets the last attempt.
	 *
	 * @return the last attempt
	 */
	@Column(name = "lastAttempt")
	public Date getLastAttempt() {
		return this.lastAttempt;
	}

	/**
	 * Sets the last attempt.
	 *
	 * @param lastAttempt
	 *            the new last attempt
	 */
	public void setLastAttempt(final Date lastAttempt) {
		this.lastAttempt = lastAttempt;
	}

}
