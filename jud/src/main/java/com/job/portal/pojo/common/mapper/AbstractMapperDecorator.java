/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.mapper;

import java.lang.reflect.Field;

import com.job.portal.pojo.common.AbstractValueObject;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractMapperDecorator.
 */
public abstract class AbstractMapperDecorator implements IAbstractMapper {

	/** The delegate. */
	private final IAbstractMapper delegate;

	/**
	 * Instantiates a new abstract mapper decorator.
	 *
	 * @param delegate
	 *            the delegate
	 */
	public AbstractMapperDecorator(final IAbstractMapper delegate) {
		this.delegate = delegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.job.portal.pojo.common.mapper.IAbstractMapper#toEntity(com.job.portal.
	 * pojo.common.dto.AbstractDTO)
	 */
	@Override
	public AbstractValueObject toEntity(final AbstractDTO dto) {
		try {
			final AbstractValueObject vo = this.delegate.toEntity(dto);
			final Field[] fields = dto.getClass().getDeclaredFields();
			for (final Field f : fields) {
				if (!Objects.isEmpty(f.get(dto))) {
					return vo;
				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
