/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum SurveyStatus.
 */
public enum SurveyStatus {

	/** The Completed. */
	Completed(0),

	/** The Pending. */
	Pending(1),

	/** The Cancelled. */
	Cancelled(2),

	/** The Launched. */
	Launched(3);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the survey status
	 */
	public static SurveyStatus fromInteger(final Integer value) {
		if (value == Completed.value) {
			return Completed;
		} else if (value == Pending.value) {
			return Pending;
		} else if (value == Cancelled.value) {
			return Cancelled;
		} else if (value == Launched.value) {
			return Launched;
		}

		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new survey status.
	 *
	 * @param value
	 *            the value
	 */
	private SurveyStatus(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("surveyStatus.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("surveyStatus.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Completed.value) {
			return "Completed";
		} else if (value == Pending.value) {
			return "Pending";
		} else if (value == Cancelled.value) {
			return "Cancelled";
		} else if (value == Launched.value) {
			return "Launched";
		}
		return null;
	}
}
