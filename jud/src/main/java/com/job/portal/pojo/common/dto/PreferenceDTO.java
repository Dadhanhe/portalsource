/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import org.apache.solr.client.solrj.beans.Field;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// TODO: Auto-generated Javadoc
/**
 * The Class PreferenceDTO.
 */

@JsonDeserialize(using = DTOSerializer.class)
public class PreferenceDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7385135371154360356L;

	/** The description. */
	@Field
	private String description;

	/** The tooltip. */
	@Field
	private String tooltip;

	/** The name. */
	@Field
	private String name;

	/** The value. */
	@Field
	private String value;

	/** The version. */
	@Field
	private String version;

	/** The preference type. */
	@Field
	private String preferenceType;

	/** The preference module. */
	@Field
	private String preferenceModule;

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return this.version;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value) {
		this.value = value;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	/**
	 * Gets the preference type.
	 *
	 * @return the preference type
	 */
	public String getPreferenceType() {
		return this.preferenceType;
	}

	/**
	 * Sets the preference type.
	 *
	 * @param preferenceType
	 *            the new preference type
	 */
	public void setPreferenceType(final String preferenceType) {
		this.preferenceType = preferenceType;
	}

	/**
	 * Gets the preference module.
	 *
	 * @return the preference module
	 */
	public String getPreferenceModule() {
		return this.preferenceModule;
	}

	/**
	 * Sets the preference module.
	 *
	 * @param preferenceModule
	 *            the new preference module
	 */
	public void setPreferenceModule(final String preferenceModule) {
		this.preferenceModule = preferenceModule;
	}

	/**
	 * Gets the tooltip.
	 *
	 * @return the tooltip
	 */
	public String getTooltip() {
		return this.tooltip;
	}

	/**
	 * Sets the tooltip.
	 *
	 * @param tooltip
	 *            the tooltip to set
	 */
	public void setTooltip(final String tooltip) {
		this.tooltip = tooltip;
	}

}
