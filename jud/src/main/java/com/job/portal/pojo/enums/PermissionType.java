/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum PermissionType.
 */
public enum PermissionType {

	/** The user. */
	USER(0),

	/** The role. */
	ROLE(1);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new permission type.
	 *
	 * @param value
	 *            the value
	 */
	private PermissionType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the permission type
	 */
	public static PermissionType fromInteger(final Integer value) {
		if (value == USER.value) {
			return USER;
		} else if (value == ROLE.value) {
			return ROLE;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == USER.value) {
			return "Approved";
		} else if (value == ROLE.value) {
			return "Pending";
		}
		return null;
	}

}
