/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum OptionType.
 */
public enum OptionType {

	/** The True false. */
	TrueFalse(0),

	/** The mcq. */
	MCQ(1),

	/** The mcq4. */
	MCQ4(2);

	/** The value. */
	private final int value;

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the option type
	 */
	public static OptionType fromInteger(final Integer value) {
		if (value == TrueFalse.value) {
			return TrueFalse;
		} else if (value == MCQ.value) {
			return MCQ;
		} else if (value == MCQ4.value) {
			return MCQ4;
		}
		return null;
	}

	/**
	 * Instantiates a new option type.
	 *
	 * @param value
	 *            the value
	 */
	private OptionType(final int value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == TrueFalse.value) {
			return "TrueFalse";
		} else if (value == MCQ.value) {
			return "MCQ";
		} else if (value == MCQ4.value) {
			return "MCQ4";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param option
	 *            the option
	 * @return the enum
	 */
	public static OptionType getEnum(final String option) {
		if (option != null) {
			if (option.equals("TrueFalse")) {
				return OptionType.TrueFalse;
			} else if (option.equals("MCQ")) {
				return OptionType.MCQ;
			} else if (option.equals("MCQ4")) {
				return OptionType.MCQ4;
			}
		}
		return null;
	}

}
