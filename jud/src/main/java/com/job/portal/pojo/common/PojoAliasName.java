/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class PojoAliasName.
 */
@Entity
@Table(name = "pojo_alias_name")
public class PojoAliasName extends AbstractValueObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The pojo name. */
	private String pojoName;

	/** The display name. */
	private String displayName;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.job.portal.pojo.common.AbstractValueObject#getId()
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", length = 11)
	public Integer getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.job.portal.pojo.common.AbstractValueObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Gets the pojo name.
	 *
	 * @return the pojo name
	 */
	@Column(name = "POJO_NAME", nullable = false, length = 11)
	public String getPojoName() {
		return this.pojoName;
	}

	/**
	 * Sets the pojo name.
	 *
	 * @param pojoName
	 *            the new pojo name
	 */
	public void setPojoName(final String pojoName) {
		this.pojoName = pojoName;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	@Column(name = "DISPLAY_NAME", nullable = false, length = 11)
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Sets the display name.
	 *
	 * @param displayName
	 *            the new display name
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

}
