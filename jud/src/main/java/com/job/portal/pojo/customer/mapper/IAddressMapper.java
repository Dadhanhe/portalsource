/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.Address;
import com.job.portal.pojo.customer.dto.AddressDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserDTOMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IAddressMapper extends IAbstractMapper<Address, AddressDTO> {

	/**
	 * To entity.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Override

	Address toEntity(AddressDTO dto);

	/**
	 * To dto.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO
	 */
	@Override

	AddressDTO toDto(Address entity);

	/**
	 * Map plain.
	 *
	 * @param dto
	 *            the dto
	 * @return the user plain
	 */
	Address toEntityPlain(AddressDTO dto);

	/**
	 * Map 2.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	Address toEntity2(AddressDTO dto);

	/**
	 * Map 2.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO 2
	 */
	AddressDTO toDto2(Address entity);
}
