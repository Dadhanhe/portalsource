/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum MetricAuditStatus.
 */
public enum MetricAuditStatus {

	/** The approved. */
	APPROVED(0),

	/** The pending. */
	PENDING(1),

	/** The denied. */
	DENIED(2),

	/** The locked. */
	LOCKED(3),

	/** The error. */
	ERROR(4),

	/** The purged. */
	PURGED(5),

	/** The reviewed. */
	REVIEWED(6),

	/** The pending approval. */
	PENDING_APPROVAL(7);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new metric audit status.
	 *
	 * @param value
	 *            the value
	 */
	private MetricAuditStatus(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the metric audit status
	 */
	public static MetricAuditStatus fromInteger(final Integer value) {
		if (value == APPROVED.value) {
			return APPROVED;
		} else if (value == PENDING.value) {
			return PENDING;
		} else if (value == DENIED.value) {
			return DENIED;
		} else if (value == LOCKED.value) {
			return LOCKED;
		} else if (value == ERROR.value) {
			return ERROR;
		} else if (value == PURGED.value) {
			return PURGED;
		} else if (value == REVIEWED.value) {
			return REVIEWED;
		} else if (value == PENDING_APPROVAL.value) {
			return PENDING_APPROVAL;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == APPROVED.value) {
			return "Approved";
		} else if (value == PENDING.value) {
			return "Pending";
		} else if (value == DENIED.value) {
			return "Denied";
		} else if (value == LOCKED.value) {
			return "Locked";
		} else if (value == ERROR.value) {
			return "Error";
		} else if (value == PURGED.value) {
			return "Purged";
		} else if (value == REVIEWED.value) {
			return "Reviewed";
		} else if (value == PENDING_APPROVAL.value) {
			return "Pending_Approval";
		}
		return null;
	}

}
