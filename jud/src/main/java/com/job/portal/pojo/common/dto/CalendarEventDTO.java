/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import org.apache.solr.client.solrj.beans.Field;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarEventDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class CalendarEventDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8334691208281578117L;

	/** The calendar event type. */
	@Field
	private String calendarEventType;

	/** The background color. */
	@Field
	private String backgroundColor;

	/** The name. */
	@Field
	private String name;

	/** The short name. */
	@Field
	private String shortName;

	/** The have start date without time. */
	@Field
	private Boolean haveStartDateWithoutTime;

	/** The have start date with time. */
	@Field
	private Boolean haveStartDateWithTime;

	/** The have end date without time. */
	@Field
	private Boolean haveEndDateWithoutTime;

	/** The have end date with time. */
	@Field
	private Boolean haveEndDateWithTime;

	/** The have location. */
	@Field
	private Boolean haveLocation;

	/** The have performance manager. */
	@Field
	private Boolean havePerformanceManager;

	/** The have time zone. */
	@Field
	private Boolean haveTimeZone;

	/** The able to invite single user. */
	@Field
	private Boolean ableToInviteSingleUser;

	/** The able to invite multiple user. */
	@Field
	private Boolean ableToInviteMultipleUser;

	/** The able to pulse push notification. */
	@Field
	private Boolean ableToPulsePushNotification;

	/** The able to email push notification. */
	@Field
	private Boolean ableToEmailPushNotification;

	/** The have description. */
	@Field
	private Boolean haveDescription;

	/** The rank. */
	@Field
	private Integer rank;

	/** The is contributor. */
	@Field
	private Boolean isContributor;

	/** The is operator. */
	@Field
	private Boolean isOperator;

	/** The associate route. */
	@Field
	private String associateRoute;

	/**
	 * Gets the calendar event type.
	 *
	 * @return the calendar event type
	 */
	public String getCalendarEventType() {
		return this.calendarEventType;
	}

	/**
	 * Sets the calendar event type.
	 *
	 * @param calendarEventType
	 *            the new calendar event type
	 */
	public void setCalendarEventType(String calendarEventType) {
		this.calendarEventType = calendarEventType;
	}

	/**
	 * Gets the background color.
	 *
	 * @return the background color
	 */
	public String getBackgroundColor() {
		return this.backgroundColor;
	}

	/**
	 * Sets the background color.
	 *
	 * @param backgroundColor
	 *            the new background color
	 */
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the short name.
	 *
	 * @return the short name
	 */
	public String getShortName() {
		return this.shortName;
	}

	/**
	 * Sets the short name.
	 *
	 * @param shortName
	 *            the new short name
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Gets the have start date without time.
	 *
	 * @return the have start date without time
	 */
	public Boolean getHaveStartDateWithoutTime() {
		return this.haveStartDateWithoutTime;
	}

	/**
	 * Sets the have start date without time.
	 *
	 * @param haveStartDateWithoutTime
	 *            the new have start date without time
	 */
	public void setHaveStartDateWithoutTime(Boolean haveStartDateWithoutTime) {
		this.haveStartDateWithoutTime = haveStartDateWithoutTime;
	}

	/**
	 * Gets the have start date with time.
	 *
	 * @return the have start date with time
	 */
	public Boolean getHaveStartDateWithTime() {
		return this.haveStartDateWithTime;
	}

	/**
	 * Sets the have start date with time.
	 *
	 * @param haveStartDateWithTime
	 *            the new have start date with time
	 */
	public void setHaveStartDateWithTime(Boolean haveStartDateWithTime) {
		this.haveStartDateWithTime = haveStartDateWithTime;
	}

	/**
	 * Gets the have end date without time.
	 *
	 * @return the have end date without time
	 */
	public Boolean getHaveEndDateWithoutTime() {
		return this.haveEndDateWithoutTime;
	}

	/**
	 * Sets the have end date without time.
	 *
	 * @param haveEndDateWithoutTime
	 *            the new have end date without time
	 */
	public void setHaveEndDateWithoutTime(Boolean haveEndDateWithoutTime) {
		this.haveEndDateWithoutTime = haveEndDateWithoutTime;
	}

	/**
	 * Gets the have end date with time.
	 *
	 * @return the have end date with time
	 */
	public Boolean getHaveEndDateWithTime() {
		return this.haveEndDateWithTime;
	}

	/**
	 * Sets the have end date with time.
	 *
	 * @param haveEndDateWithTime
	 *            the new have end date with time
	 */
	public void setHaveEndDateWithTime(Boolean haveEndDateWithTime) {
		this.haveEndDateWithTime = haveEndDateWithTime;
	}

	/**
	 * Gets the have location.
	 *
	 * @return the have location
	 */
	public Boolean getHaveLocation() {
		return this.haveLocation;
	}

	/**
	 * Sets the have location.
	 *
	 * @param haveLocation
	 *            the new have location
	 */
	public void setHaveLocation(Boolean haveLocation) {
		this.haveLocation = haveLocation;
	}

	/**
	 * Gets the have performance manager.
	 *
	 * @return the have performance manager
	 */
	public Boolean getHavePerformanceManager() {
		return this.havePerformanceManager;
	}

	/**
	 * Sets the have performance manager.
	 *
	 * @param havePerformanceManager
	 *            the new have performance manager
	 */
	public void setHavePerformanceManager(Boolean havePerformanceManager) {
		this.havePerformanceManager = havePerformanceManager;
	}

	/**
	 * Gets the have time zone.
	 *
	 * @return the have time zone
	 */
	public Boolean getHaveTimeZone() {
		return this.haveTimeZone;
	}

	/**
	 * Sets the have time zone.
	 *
	 * @param haveTimeZone
	 *            the new have time zone
	 */
	public void setHaveTimeZone(Boolean haveTimeZone) {
		this.haveTimeZone = haveTimeZone;
	}

	/**
	 * Gets the able to invite single user.
	 *
	 * @return the able to invite single user
	 */
	public Boolean getAbleToInviteSingleUser() {
		return this.ableToInviteSingleUser;
	}

	/**
	 * Sets the able to invite single user.
	 *
	 * @param ableToInviteSingleUser
	 *            the new able to invite single user
	 */
	public void setAbleToInviteSingleUser(Boolean ableToInviteSingleUser) {
		this.ableToInviteSingleUser = ableToInviteSingleUser;
	}

	/**
	 * Gets the able to invite multiple user.
	 *
	 * @return the able to invite multiple user
	 */
	public Boolean getAbleToInviteMultipleUser() {
		return this.ableToInviteMultipleUser;
	}

	/**
	 * Sets the able to invite multiple user.
	 *
	 * @param ableToInviteMultipleUser
	 *            the new able to invite multiple user
	 */
	public void setAbleToInviteMultipleUser(Boolean ableToInviteMultipleUser) {
		this.ableToInviteMultipleUser = ableToInviteMultipleUser;
	}

	/**
	 * Gets the able to pulse push notification.
	 *
	 * @return the able to pulse push notification
	 */
	public Boolean getAbleToPulsePushNotification() {
		return this.ableToPulsePushNotification;
	}

	/**
	 * Sets the able to pulse push notification.
	 *
	 * @param ableToPulsePushNotification
	 *            the new able to pulse push notification
	 */
	public void setAbleToPulsePushNotification(
			Boolean ableToPulsePushNotification) {
		this.ableToPulsePushNotification = ableToPulsePushNotification;
	}

	/**
	 * Gets the able to email push notification.
	 *
	 * @return the able to email push notification
	 */
	public Boolean getAbleToEmailPushNotification() {
		return this.ableToEmailPushNotification;
	}

	/**
	 * Sets the able to email push notification.
	 *
	 * @param ableToEmailPushNotification
	 *            the new able to email push notification
	 */
	public void setAbleToEmailPushNotification(
			Boolean ableToEmailPushNotification) {
		this.ableToEmailPushNotification = ableToEmailPushNotification;
	}

	/**
	 * Gets the have description.
	 *
	 * @return the have description
	 */
	public Boolean getHaveDescription() {
		return this.haveDescription;
	}

	/**
	 * Sets the have description.
	 *
	 * @param haveDescription
	 *            the new have description
	 */
	public void setHaveDescription(Boolean haveDescription) {
		this.haveDescription = haveDescription;
	}

	/**
	 * Gets the rank.
	 *
	 * @return the rank
	 */
	public Integer getRank() {
		return this.rank;
	}

	/**
	 * Sets the rank.
	 *
	 * @param rank
	 *            the new rank
	 */
	public void setRank(Integer rank) {
		this.rank = rank;
	}

	/**
	 * Gets the checks if is contributor.
	 *
	 * @return the checks if is contributor
	 */
	public Boolean getIsContributor() {
		return this.isContributor;
	}

	/**
	 * Sets the checks if is contributor.
	 *
	 * @param isContributor
	 *            the new checks if is contributor
	 */
	public void setIsContributor(Boolean isContributor) {
		this.isContributor = isContributor;
	}

	/**
	 * Gets the checks if is operator.
	 *
	 * @return the checks if is operator
	 */
	public Boolean getIsOperator() {
		return this.isOperator;
	}

	/**
	 * Sets the checks if is operator.
	 *
	 * @param isOperator
	 *            the new checks if is operator
	 */
	public void setIsOperator(Boolean isOperator) {
		this.isOperator = isOperator;
	}

	/**
	 * Gets the associate route.
	 *
	 * @return the associate route
	 */
	public String getAssociateRoute() {
		return this.associateRoute;
	}

	/**
	 * Sets the associate route.
	 *
	 * @param associateRoute
	 *            the new associate route
	 */
	public void setAssociateRoute(String associateRoute) {
		this.associateRoute = associateRoute;
	}
}
