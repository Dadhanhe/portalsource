/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.common.dto;

import org.apache.solr.client.solrj.beans.Field;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

// TODO: Auto-generated Javadoc
/**
 * The Class FieldSchemeDTO.
 */
@JsonDeserialize(using = DTOSerializer.class)
public class FieldSchemeDTO extends AbstractDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8580842968084115283L;

	/** The allow future date. */
	@Field
	private Boolean allowFutureDate;

	/** The column name. */
	@Field
	private String columnName;

	/** The column width. */
	@Field
	private Integer columnWidth;

	/** The description. */
	@Field
	private String description;

	/** The disable past date. */
	@Field
	private Boolean disablePastDate;

	/** The displayed on search page. */
	@Field
	private Boolean displayedOnSearchPage;

	/** The display name. */
	@Field
	private String displayName;

	/** The entity type. */
	@Field
	private String entityType;

	/** The front regex. */
	@Field
	private String frontRegex;

	/** The group hint. */
	@Field
	private String groupHint;

	/** The group title. */
	@Field
	private String groupTitle;

	/** The input hint. */
	@Field
	private String inputHint;

	/** The input width. */
	@Field
	private Integer inputWidth;

	/** The is import field. */
	@Field
	private Boolean isImportField;

	/** The is required. */
	@Field
	private Boolean isRequired;

	/** The is retail field scheme. */
	@Field
	private Boolean isRetailFieldScheme;

	/** The is isStackHolderFieldScheme . */
	@Field
	private String isStakeHolderFieldScheme;

	/** The is unique. */
	@Field
	private Boolean isUnique;

	/** The max length. */
	@Field
	private Integer maxLength;

	/** The placeholder. */
	@Field
	private String placeholder;

	/** The rank. */
	@Field
	private Integer rank;

	/** The readonly. */
	@Field
	private Boolean readonly;

	/** The regex. */
	@Field
	private String regex;

	/** The rental car type. */
	@Field
	private String rentalCarType;

	/** The required to update. */
	@Field
	private Boolean requiredToUpdate;

	/** The retail field scheme name. */
	@Field
	private String retailFieldSchemeName;

	/** The tag. */
	@Field
	private String tag;

	/** The field typee. */
	@Field
	private String fieldTypee;

	/** The is money. */
	private Boolean isMoney;

	/** The enumeration. */
	private String enumeration;

	/** The data source. */
	private String dataSource;

	/** The default value. */
	private String defaultValue;

	/** The industry name. */
	@Field
	private String industryName;

	/** The predefined. */
	private Boolean predefined;

	/** The is meeting and events field scheme. */
	@Field
	private Boolean isMeetingAndEventsFieldScheme;

	/** The is fn B field scheme. */
	@Field
	private Boolean isFnBFieldScheme;

	/** The fnb field scheme name. */
	@Field
	private String fnbFieldSchemeName;

	/** The is common field scheme. */
	@Field
	private Boolean isCommonFieldScheme;

	/** The is goal field scheme. */
	@Field
	private Boolean isGoalFieldScheme;

	/** The displayed on profile page. */
	@Field
	private Boolean displayedOnProfilePage;

	/** The stake holder type. */
	@Field
	private Integer stakeHolderType;

	/** The is onboarding field scheme. */
	@Field
	private Boolean isOnboardingFieldScheme;

	/**
	 * Gets the allow future date.
	 *
	 * @return the allow future date
	 */
	public Boolean getAllowFutureDate() {
		return this.allowFutureDate;
	}

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return this.columnName;
	}

	/**
	 * Gets the column width.
	 *
	 * @return the column width
	 */
	public Integer getColumnWidth() {
		return this.columnWidth;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Gets the disable past date.
	 *
	 * @return the disable past date
	 */
	public Boolean getDisablePastDate() {
		return this.disablePastDate;
	}

	/**
	 * Gets the displayed on search page.
	 *
	 * @return the displayed on search page
	 */
	public Boolean getDisplayedOnSearchPage() {
		return this.displayedOnSearchPage;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Gets the entity type.
	 *
	 * @return the entity type
	 */
	public String getEntityType() {
		return this.entityType;
	}

	/**
	 * Gets the front regex.
	 *
	 * @return the front regex
	 */
	public String getFrontRegex() {
		return this.frontRegex;
	}

	/**
	 * Gets the group hint.
	 *
	 * @return the group hint
	 */
	public String getGroupHint() {
		return this.groupHint;
	}

	/**
	 * Gets the industry name.
	 *
	 * @return the industry name
	 */
	public String getIndustryName() {
		return this.industryName;
	}

	/**
	 * Gets the group title.
	 *
	 * @return the group title
	 */
	public String getGroupTitle() {
		return this.groupTitle;
	}

	/**
	 * Gets the input hint.
	 *
	 * @return the input hint
	 */
	public String getInputHint() {
		return this.inputHint;
	}

	/**
	 * Gets the input width.
	 *
	 * @return the input width
	 */
	public Integer getInputWidth() {
		return this.inputWidth;
	}

	/**
	 * Gets the checks if is import field.
	 *
	 * @return the checks if is import field
	 */
	public Boolean getIsImportField() {
		return this.isImportField;
	}

	/**
	 * Gets the checks if is required.
	 *
	 * @return the checks if is required
	 */
	public Boolean getIsRequired() {
		return this.isRequired;
	}

	/**
	 * Gets the checks if is retail field scheme.
	 *
	 * @return the checks if is retail field scheme
	 */
	public Boolean getIsRetailFieldScheme() {
		return this.isRetailFieldScheme;
	}

	/**
	 * Gets the checks if is unique.
	 *
	 * @return the checks if is unique
	 */
	public Boolean getIsUnique() {
		return this.isUnique;
	}

	/**
	 * Gets the max length.
	 *
	 * @return the max length
	 */
	public Integer getMaxLength() {
		return this.maxLength;
	}

	/**
	 * Gets the placeholder.
	 *
	 * @return the placeholder
	 */
	public String getPlaceholder() {
		return this.placeholder;
	}

	/**
	 * Gets the rank.
	 *
	 * @return the rank
	 */
	public Integer getRank() {
		return this.rank;
	}

	/**
	 * Gets the readonly.
	 *
	 * @return the readonly
	 */
	public Boolean getReadonly() {
		return this.readonly;
	}

	/**
	 * Gets the regex.
	 *
	 * @return the regex
	 */
	public String getRegex() {
		return this.regex;
	}

	/**
	 * Gets the rental car type.
	 *
	 * @return the rental car type
	 */
	public String getRentalCarType() {
		return this.rentalCarType;
	}

	/**
	 * Gets the required to update.
	 *
	 * @return the required to update
	 */
	public Boolean getRequiredToUpdate() {
		return this.requiredToUpdate;
	}

	/**
	 * Gets the retail field scheme name.
	 *
	 * @return the retail field scheme name
	 */
	public String getRetailFieldSchemeName() {
		return this.retailFieldSchemeName;
	}

	/**
	 * Gets the tag.
	 *
	 * @return the tag
	 */
	public String getTag() {
		return this.tag;
	}

	/**
	 * Sets the allow future date.
	 *
	 * @param allowFutureDate
	 *            the new allow future date
	 */
	public void setAllowFutureDate(final Boolean allowFutureDate) {
		this.allowFutureDate = allowFutureDate;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName
	 *            the new column name
	 */
	public void setColumnName(final String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Sets the column width.
	 *
	 * @param columnWidth
	 *            the new column width
	 */
	public void setColumnWidth(final Integer columnWidth) {
		this.columnWidth = columnWidth;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Sets the disable past date.
	 *
	 * @param disablePastDate
	 *            the new disable past date
	 */
	public void setDisablePastDate(final Boolean disablePastDate) {
		this.disablePastDate = disablePastDate;
	}

	/**
	 * Sets the displayed on search page.
	 *
	 * @param displayedOnSearchPage
	 *            the new displayed on search page
	 */
	public void setDisplayedOnSearchPage(final Boolean displayedOnSearchPage) {
		this.displayedOnSearchPage = displayedOnSearchPage;
	}

	/**
	 * Sets the display name.
	 *
	 * @param displayName
	 *            the new display name
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Sets the entity type.
	 *
	 * @param entityType
	 *            the new entity type
	 */
	public void setEntityType(final String entityType) {
		this.entityType = entityType;
	}

	/**
	 * Sets the front regex.
	 *
	 * @param frontRegex
	 *            the new front regex
	 */
	public void setFrontRegex(final String frontRegex) {
		this.frontRegex = frontRegex;
	}

	/**
	 * Sets the group hint.
	 *
	 * @param groupHint
	 *            the new group hint
	 */
	public void setGroupHint(final String groupHint) {
		this.groupHint = groupHint;
	}

	/**
	 * Sets the group title.
	 *
	 * @param groupTitle
	 *            the new group title
	 */
	public void setGroupTitle(final String groupTitle) {
		this.groupTitle = groupTitle;
	}

	/**
	 * Sets the input hint.
	 *
	 * @param inputHint
	 *            the new input hint
	 */
	public void setInputHint(final String inputHint) {
		this.inputHint = inputHint;
	}

	/**
	 * Sets the input width.
	 *
	 * @param inputWidth
	 *            the new input width
	 */
	public void setInputWidth(final Integer inputWidth) {
		this.inputWidth = inputWidth;
	}

	/**
	 * Sets the checks if is import field.
	 *
	 * @param isImportField
	 *            the new checks if is import field
	 */
	public void setIsImportField(final Boolean isImportField) {
		this.isImportField = isImportField;
	}

	/**
	 * Sets the checks if is required.
	 *
	 * @param isRequired
	 *            the new checks if is required
	 */
	public void setIsRequired(final Boolean isRequired) {
		this.isRequired = isRequired;
	}

	/**
	 * Sets the checks if is retail field scheme.
	 *
	 * @param isRetailFieldScheme
	 *            the new checks if is retail field scheme
	 */
	public void setIsRetailFieldScheme(final Boolean isRetailFieldScheme) {
		this.isRetailFieldScheme = isRetailFieldScheme;
	}

	/**
	 * Sets the checks if is unique.
	 *
	 * @param isUnique
	 *            the new checks if is unique
	 */
	public void setIsUnique(final Boolean isUnique) {
		this.isUnique = isUnique;
	}

	/**
	 * Sets the max length.
	 *
	 * @param maxLength
	 *            the new max length
	 */
	public void setMaxLength(final Integer maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * Sets the placeholder.
	 *
	 * @param placeholder
	 *            the new placeholder
	 */
	public void setPlaceholder(final String placeholder) {
		this.placeholder = placeholder;
	}

	/**
	 * Sets the rank.
	 *
	 * @param rank
	 *            the new rank
	 */
	public void setRank(final Integer rank) {
		this.rank = rank;
	}

	/**
	 * Sets the readonly.
	 *
	 * @param readonly
	 *            the new readonly
	 */
	public void setReadonly(final Boolean readonly) {
		this.readonly = readonly;
	}

	/**
	 * Sets the regex.
	 *
	 * @param regex
	 *            the new regex
	 */
	public void setRegex(final String regex) {
		this.regex = regex;
	}

	/**
	 * Sets the rental car type.
	 *
	 * @param rentalCarType
	 *            the new rental car type
	 */
	public void setRentalCarType(final String rentalCarType) {
		this.rentalCarType = rentalCarType;
	}

	/**
	 * Sets the required to update.
	 *
	 * @param requiredToUpdate
	 *            the new required to update
	 */
	public void setRequiredToUpdate(final Boolean requiredToUpdate) {
		this.requiredToUpdate = requiredToUpdate;
	}

	/**
	 * Sets the retail field scheme name.
	 *
	 * @param retailFieldSchemeName
	 *            the new retail field scheme name
	 */
	public void setRetailFieldSchemeName(final String retailFieldSchemeName) {
		this.retailFieldSchemeName = retailFieldSchemeName;
	}

	/**
	 * Sets the tag.
	 *
	 * @param tag
	 *            the new tag
	 */
	public void setTag(final String tag) {
		this.tag = tag;
	}

	/**
	 * Gets the field typee.
	 *
	 * @return the field typee
	 */
	public String getFieldTypee() {
		return this.fieldTypee;
	}

	/**
	 * Sets the field typee.
	 *
	 * @param fieldType
	 *            the new field typee
	 */
	public void setFieldTypee(final String fieldType) {
		this.fieldTypee = fieldType;
	}

	/**
	 * Gets the checks if is money.
	 *
	 * @return the checks if is money
	 */
	public Boolean getIsMoney() {
		return this.isMoney;
	}

	/**
	 * Sets the checks if is money.
	 *
	 * @param isMoney
	 *            the new checks if is money
	 */
	public void setIsMoney(final Boolean isMoney) {
		this.isMoney = isMoney;
	}

	/**
	 * Gets the enumeration.
	 *
	 * @return the enumeration
	 */
	public String getEnumeration() {
		return this.enumeration;
	}

	/**
	 * Sets the enumeration.
	 *
	 * @param enumeration
	 *            the new enumeration
	 */
	public void setEnumeration(final String enumeration) {
		this.enumeration = enumeration;
	}

	/**
	 * Gets the data source.
	 *
	 * @return the data source
	 */
	public String getDataSource() {
		return this.dataSource;
	}

	/**
	 * Sets the data source.
	 *
	 * @param dataSource
	 *            the new data source
	 */
	public void setDataSource(final String dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Gets the default value.
	 *
	 * @return the default value
	 */
	public String getDefaultValue() {
		return this.defaultValue;
	}

	/**
	 * Sets the default value.
	 *
	 * @param defaultValue
	 *            the new default value
	 */
	public void setDefaultValue(final String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Sets the industry name.
	 *
	 * @param industryName
	 *            the new industry name
	 */
	public void setIndustryName(final String industryName) {
		this.industryName = industryName;
	}

	/**
	 * Gets the predefined.
	 *
	 * @return the predefined
	 */
	public Boolean getPredefined() {
		return this.predefined;
	}

	/**
	 * Sets the predefined.
	 *
	 * @param predefined
	 *            the new predefined
	 */
	public void setPredefined(final Boolean predefined) {
		this.predefined = predefined;
	}

	/**
	 * Gets the checks if is meeting and events field scheme.
	 *
	 * @return the checks if is meeting and events field scheme
	 */
	public Boolean getIsMeetingAndEventsFieldScheme() {
		return this.isMeetingAndEventsFieldScheme;
	}

	/**
	 * Sets the checks if is meeting and events field scheme.
	 *
	 * @param isMeetingAndEventsFieldScheme
	 *            the new checks if is meeting and events field scheme
	 */
	public void setIsMeetingAndEventsFieldScheme(
			final Boolean isMeetingAndEventsFieldScheme) {
		this.isMeetingAndEventsFieldScheme = isMeetingAndEventsFieldScheme;
	}

	/**
	 * Gets the checks if is fn B field scheme.
	 *
	 * @return the checks if is fn B field scheme
	 */
	public Boolean getIsFnBFieldScheme() {
		return this.isFnBFieldScheme;
	}

	/**
	 * Sets the checks if is fn B field scheme.
	 *
	 * @param isFnBFieldScheme
	 *            the new checks if is fn B field scheme
	 */
	public void setIsFnBFieldScheme(final Boolean isFnBFieldScheme) {
		this.isFnBFieldScheme = isFnBFieldScheme;
	}

	/**
	 * Gets the fnb field scheme name.
	 *
	 * @return the fnb field scheme name
	 */
	public String getFnbFieldSchemeName() {
		return this.fnbFieldSchemeName;
	}

	/**
	 * Sets the fnb field scheme name.
	 *
	 * @param fnbFieldSchemeName
	 *            the new fnb field scheme name
	 */
	public void setFnbFieldSchemeName(final String fnbFieldSchemeName) {
		this.fnbFieldSchemeName = fnbFieldSchemeName;
	}

	/**
	 * Gets the checks if is common field scheme.
	 *
	 * @return the checks if is common field scheme
	 */
	public Boolean getIsCommonFieldScheme() {
		return this.isCommonFieldScheme;
	}

	/**
	 * Sets the checks if is common field scheme.
	 *
	 * @param isCommonFieldScheme
	 *            the new checks if is common field scheme
	 */
	public void setIsCommonFieldScheme(final Boolean isCommonFieldScheme) {
		this.isCommonFieldScheme = isCommonFieldScheme;
	}

	/**
	 * Gets the checks if is goal field scheme.
	 *
	 * @return the checks if is goal field scheme
	 */
	public Boolean getIsGoalFieldScheme() {
		return this.isGoalFieldScheme;
	}

	/**
	 * Sets the checks if is goal field scheme.
	 *
	 * @param isGoalFieldScheme
	 *            the new checks if is goal field scheme
	 */
	public void setIsGoalFieldScheme(final Boolean isGoalFieldScheme) {
		this.isGoalFieldScheme = isGoalFieldScheme;
	}

	/**
	 * Gets the displayed on profile page.
	 *
	 * @return the displayed on profile page
	 */
	public Boolean getDisplayedOnProfilePage() {
		return this.displayedOnProfilePage;
	}

	/**
	 * Sets the displayed on profile page.
	 *
	 * @param displayedOnProfilePage
	 *            the new displayed on profile page
	 */
	public void setDisplayedOnProfilePage(
			final Boolean displayedOnProfilePage) {
		this.displayedOnProfilePage = displayedOnProfilePage;
	}

	/**
	 * Gets the checks if is stake holder field scheme.
	 *
	 * @return the isStakeHolderFieldScheme
	 */
	public String getIsStakeHolderFieldScheme() {
		return this.isStakeHolderFieldScheme;
	}

	/**
	 * Sets the checks if is stake holder field scheme.
	 *
	 * @param isStakeHolderFieldScheme
	 *            the isStakeHolderFieldScheme to set
	 */
	public void setIsStakeHolderFieldScheme(
			final String isStakeHolderFieldScheme) {
		this.isStakeHolderFieldScheme = isStakeHolderFieldScheme;
	}

	/**
	 * Gets the stake holder type.
	 *
	 * @return the stake holder type
	 */
	public Integer getStakeHolderType() {
		return this.stakeHolderType;
	}

	/**
	 * Sets the stake holder type.
	 *
	 * @param stakeHolderType
	 *            the new stake holder type
	 */
	public void setStakeHolderType(final Integer stakeHolderType) {
		this.stakeHolderType = stakeHolderType;
	}

	/**
	 * Gets the checks if is onboarding field scheme.
	 *
	 * @return the checks if is onboarding field scheme
	 */
	public Boolean getIsOnboardingFieldScheme() {
		return this.isOnboardingFieldScheme;
	}

	/**
	 * Sets the checks if is onboarding field scheme.
	 *
	 * @param isOnboardingFieldScheme
	 *            the new checks if is onboarding field scheme
	 */
	public void setIsOnboardingFieldScheme(
			final Boolean isOnboardingFieldScheme) {
		this.isOnboardingFieldScheme = isOnboardingFieldScheme;
	}
}
