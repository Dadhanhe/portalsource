/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum FieldType.
 */
public enum FieldType implements Serializable {

	/** The Text. */
	Text(0),

	/** The Date. */
	Date(1),

	/** The Date time. */
	DateTime(2),

	/** The Numeric. */
	Numeric(3),

	/** The File. */
	File(4),

	/** The Label. */
	Label(5),

	/** The Yes no. */
	YesNo(6),

	/** The Drop down. */
	DropDown(7);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the field type
	 */
	public static FieldType fromInteger(final Integer value) {
		if (value == Text.value) {
			return Text;
		} else if (value == Date.value) {
			return Date;
		} else if (value == DateTime.value) {
			return DateTime;
		} else if (value == Numeric.value) {
			return Numeric;
		} else if (value == File.value) {
			return File;
		} else if (value == Label.value) {
			return Label;
		} else if (value == YesNo.value) {
			return YesNo;
		} else if (value == DropDown.value) {
			return DropDown;
		}
		return null;
	}

	/**
	 * From string.
	 *
	 * @param value
	 *            the value
	 * @return the field type
	 */
	public static FieldType fromString(final String value) {
		if (value == "Text") {
			return Text;
		} else if (value == "Date") {
			return Date;
		} else if (value == "DateTime") {
			return DateTime;
		} else if (value == "Numeric") {
			return Numeric;
		} else if (value == "File") {
			return File;
		} else if (value == "Label") {
			return Label;
		} else if (value == "YesNo") {
			return YesNo;
		} else if (value == "DropDown") {
			return DropDown;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new field type.
	 *
	 * @param value
	 *            the value
	 */
	private FieldType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("field.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public int getValue() {
		return this.value;
	}

}
