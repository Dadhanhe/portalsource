/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum PreferenceModule.
 */
public enum PreferenceModule {

	/** The System. */
	System(0),

	/** The Agent profile. */
	AgentProfile(1),

	/** The Incentive. */
	Incentive(2),

	/** The Tenant observation. */
	Observation(3),

	/** The Tenant coaching. */
	Coaching(4),

	/** The Blueprint. */
	Blueprint(5),

	// Tfs Issue - 185497609 - EktaN

	/** The One to one. */
	OneToOne(6),

	/** The Command center. */
	CommandCenter(7);
	// End

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the preference module
	 */
	public static PreferenceModule fromInteger(final Integer value) {
		if (value == System.value) {
			return System;
		} else if (value == AgentProfile.value) {
			return AgentProfile;
		} else if (value == Incentive.value) {
			return Incentive;
		} else if (value == Observation.value) {
			return Observation;
		} else if (value == Coaching.value) {
			return Coaching;
		} else if (value == Blueprint.value) {
			return Blueprint;
		} else if (value == OneToOne.value) {
			return OneToOne;
		} else if (value == CommandCenter.value) {
			return CommandCenter;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new preference module.
	 *
	 * @param value
	 *            the value
	 */
	private PreferenceModule(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
