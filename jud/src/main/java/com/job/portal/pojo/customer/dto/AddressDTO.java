/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 *
 *
 *
 *
 *
 *
 *
 */
package com.job.portal.pojo.customer.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.job.portal.pojo.common.dto.AbstractDTO;
import com.job.portal.pojo.common.dto.DTOSerializer;

/**
 * @author Vaibhav
 *
 */
@JsonDeserialize(using = DTOSerializer.class)
public class AddressDTO extends AbstractDTO {

	private String lat;

	private String lng;

	private String address;

	private Integer cityID;

	private String cityName;

	private String pincode;

	private String district;

	private String googlePlaceId;

	private Double distance;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the lat
	 */
	public String getLat() {
		return this.lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(final String lat) {
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public String getLng() {
		return this.lng;
	}

	/**
	 * @param lng
	 *            the lng to set
	 */
	public void setLng(final String lng) {
		this.lng = lng;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * @return the cityID
	 */
	public Integer getCityID() {
		return this.cityID;
	}

	/**
	 * @param cityID
	 *            the cityID to set
	 */
	public void setCityID(final Integer cityID) {
		this.cityID = cityID;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return this.cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(final String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return this.pincode;
	}

	/**
	 * @param pincode
	 *            the pincode to set
	 */
	public void setPincode(final String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return this.district;
	}

	/**
	 * @param district
	 *            the district to set
	 */
	public void setDistrict(final String district) {
		this.district = district;
	}

	/**
	 * @return the googlePlaceId
	 */
	public String getGooglePlaceId() {
		return this.googlePlaceId;
	}

	/**
	 * @param googlePlaceId
	 *            the googlePlaceId to set
	 */
	public void setGooglePlaceId(final String googlePlaceId) {
		this.googlePlaceId = googlePlaceId;
	}

	/**
	 * @return the distance
	 */
	public Double getDistance() {
		return this.distance;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(final Double distance) {
		this.distance = distance;
	}

}
