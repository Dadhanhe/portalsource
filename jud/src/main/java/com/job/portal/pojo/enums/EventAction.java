/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum EventAction.
 */
public enum EventAction {

	/** The Created. */
	Created(0),

	/** The Updated. */
	Updated(1),

	/** The Deleted. */
	Deleted(2),

	/** The Login. */
	Login(3),

	/** The Logout. */
	Logout(4),

	/** The Signin. */
	Signin(5),

	/** The Signout. */
	Signout(6),

	/** The Change password. */
	ChangePassword(7);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the event action
	 */
	public static EventAction fromInteger(final Integer value) {
		if (value == Created.value) {
			return Created;
		} else if (value == Updated.value) {
			return Updated;
		} else if (value == Deleted.value) {
			return Deleted;
		} else if (value == Login.value) {
			return Login;
		} else if (value == Logout.value) {
			return Logout;
		} else if (value == Signin.value) {
			return Signin;
		} else if (value == Signout.value) {
			return Signout;
		} else if (value == ChangePassword.value) {
			return ChangePassword;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new event action.
	 *
	 * @param value
	 *            the value
	 */
	private EventAction(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Created.value) {
			return "Create";
		} else if (value == Updated.value) {
			return "Update";
		} else if (value == Deleted.value) {
			return "Delete";
		} else if (value == Login.value) {
			return "Login";
		} else if (value == Logout.value) {
			return "Logout";
		} else if (value == Signin.value) {
			return "Signin";
		} else if (value == Signout.value) {
			return "Signout";
		} else if (value == ChangePassword.value) {
			return "ChangePassword";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static EventAction getEnum(final String status) {
		if (status != null) {
			if (status.equals("Create")) {
				return EventAction.Created;
			} else if (status.equals("Update")) {
				return EventAction.Updated;
			} else if (status.equals("Delete")) {
				return EventAction.Deleted;
			} else if (status.equals("Login")) {
				return EventAction.Login;
			} else if (status.equals("Logout")) {
				return EventAction.Logout;
			} else if (status.equals("Signin")) {
				return EventAction.Signin;
			} else if (status.equals("Signout")) {
				return EventAction.Signout;
			} else if (status.equals("ChangePassword")) {
				return EventAction.ChangePassword;
			}
		}
		return null;
	}
}
