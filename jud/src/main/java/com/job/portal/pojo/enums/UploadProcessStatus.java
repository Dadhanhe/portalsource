/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum UploadProcessStatus.
 */
public enum UploadProcessStatus {

	/** The Pending. */
	Pending(0),

	/** The Queued. */
	Queued(1),

	/** The Processed. */
	Processed(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the upload process status
	 */
	public static UploadProcessStatus fromInteger(final Integer value) {
		if (value == Pending.value) {
			return Pending;
		} else if (value == Queued.value) {
			return Queued;
		} else if (value == Processed.value) {
			return Processed;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new upload process status.
	 *
	 * @param value
	 *            the value
	 */
	private UploadProcessStatus(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Pending.value) {
			return "Pending";
		} else if (value == Queued.value) {
			return "Queued";
		} else if (value == Processed.value) {
			return "Processed";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static UploadProcessStatus getEnum(final String status) {
		if (status != null) {
			if (status.equals("Pending")) {
				return UploadProcessStatus.Pending;
			} else if (status.equals("Queued")) {
				return UploadProcessStatus.Queued;
			} else if (status.equals("Processed")) {
				return UploadProcessStatus.Processed;
			}
		}
		return null;
	}
}
