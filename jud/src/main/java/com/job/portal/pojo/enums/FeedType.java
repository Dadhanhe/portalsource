/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum FeedType.
 */
public enum FeedType {

	/** The Take blueprint. */
	TakeBlueprint(2);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the widget type
	 */
	public static FeedType fromInteger(final Integer value) {
		if (value == TakeBlueprint.value) {
			return TakeBlueprint;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new widget type.
	 *
	 * @param value
	 *            the value
	 */
	private FeedType(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == TakeBlueprint.value) {
			return "TakeBlueprint";
		}
		return null;
	}

	/**
	 * Gets the enum.
	 *
	 * @param status
	 *            the status
	 * @return the enum
	 */
	public static FeedType getEnum(final String status) {
		if (status != null) {
			if (status.equals("TakeBlueprint")) {
				return FeedType.TakeBlueprint;
			}
		}
		return null;
	}
}
