/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum MethodType.
 */
public enum MethodType {

	/** The create. */
	CREATE(0),

	/** The update. */
	UPDATE(1),

	/** The delete. */
	DELETE(2),

	/** The update delete. */
	UPDATE_DELETE(3);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new method type.
	 *
	 * @param value
	 *            the value
	 */
	private MethodType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the method type
	 */
	public static MethodType fromInteger(final Integer value) {
		if (value == CREATE.value) {
			return CREATE;
		} else if (value == UPDATE.value) {
			return UPDATE;
		} else if (value == DELETE.value) {
			return DELETE;
		} else if (value == UPDATE_DELETE.value) {
			return UPDATE_DELETE;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == CREATE.value) {
			return "Create";
		} else if (value == UPDATE.value) {
			return "Update";
		} else if (value == DELETE.value) {
			return "Delete";
		} else if (value == UPDATE_DELETE.value) {
			return "Update_Delete";
		}
		return null;
	}

}
