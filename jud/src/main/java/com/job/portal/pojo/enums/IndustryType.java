/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum IndustryType.
 */
public enum IndustryType {

	/** The Hotel. */
	Hotel(1),

	/** The Car rental. */
	CarRental(2),

	/** The Theme park CC. */
	ThemeParkCC(3),

	/** The Car dealer. */
	CarDealer(4),

	/** The Entertainment. */
	Entertainment(5),

	/** The Call center HRCC. */
	CallCenterHRCC(6),

	/** The Nissan sales. */
	NissanSales(7),

	/** The Insurance. */
	Insurance(8),

	/** The Universal. */
	Universal(9),

	/** The Marriott. */
	Marriott(10);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the industry type
	 */
	public static IndustryType fromInteger(final Integer value) {
		if (value == Hotel.value) {
			return Hotel;
		} else if (value == CarRental.value) {
			return CarRental;
		} else if (value == ThemeParkCC.value) {
			return ThemeParkCC;
		} else if (value == CarDealer.value) {
			return CarDealer;
		} else if (value == Entertainment.value) {
			return Entertainment;
		} else if (value == CallCenterHRCC.value) {
			return CallCenterHRCC;
		} else if (value == NissanSales.value) {
			return NissanSales;
		} else if (value == Insurance.value) {
			return Insurance;
		} else if (value == Universal.value) {
			return Universal;
		} else if (value == Marriott.value) {
			return Marriott;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new industry type.
	 *
	 * @param value
	 *            the value
	 */
	private IndustryType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("industry.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Hotel.value) {
			return "Hotel";
		} else if (value == CarRental.value) {
			return "CarRental";
		} else if (value == ThemeParkCC.value) {
			return "ThemePark";
		} else if (value == CarDealer.value) {
			return "CarDealer";
		} else if (value == Entertainment.value) {
			return "Entertainment";
		} else if (value == CallCenterHRCC.value) {
			return "CallCenterHRCC";
		} else if (value == NissanSales.value) {
			return "NissanSales";
		} else if (value == Insurance.value) {
			return "Insurance";
		} else if (value == Universal.value) {
			return "Universal";
		} else if (value == Marriott.value) {
			return "Marriott";
		}
		return null;
	}

	/**
	 * Gets the value by industry.
	 *
	 * @param code
	 *            the code
	 * @return the value by industry
	 */
	public static int getValueByIndustry(final String code) {
		for (int i = 0; i < IndustryType.values().length; i++) {
			if (code.equalsIgnoreCase(IndustryType.values()[i].name())) {
				return IndustryType.values()[i].value();
			}
		}
		return -1;
	}
}
