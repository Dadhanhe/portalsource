/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.customer.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import com.job.portal.pojo.common.mapper.IAbstractMapper;
import com.job.portal.pojo.customer.User;
import com.job.portal.pojo.customer.UserPlain;
import com.job.portal.pojo.customer.dto.UserDTO;
import com.job.portal.pojo.customer.dto.UserDTO2;
import com.job.portal.utils.Objects;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserDTOMapper.
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IUserMapper extends IAbstractMapper<User, UserDTO> {

	/**
	 * To entity.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Override

	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	User toEntity(UserDTO dto);

	/**
	 * To dto.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO
	 */
	@Override

	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	UserDTO toDto(User entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final UserDTO dto,
			@MappingTarget final User entity) {
		checkCreatedByUpdatedBy(dto, entity);
	}

	/**
	 * Map plain.
	 *
	 * @param dto
	 *            the dto
	 * @return the user plain
	 */
	UserPlain toEntityPlain(UserDTO dto);

	/**
	 * Map plain.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO
	 */
	@Mapping(target = "userFullName", source = "entity.fullName")
	@Mapping(target = "cByIdentity", source = "entity", qualifiedByName = "createdByUser")
	@Mapping(target = "uByIdentity", source = "entity", qualifiedByName = "updatedByUser")
	UserDTO toDtoPlain(UserPlain entity);

	/**
	 * Map 2.
	 *
	 * @param dto
	 *            the dto
	 * @return the user
	 */
	@Mapping(target = "createdBy.id", source = "dto.createdById")
	@Mapping(target = "updatedBy.id", source = "dto.updatedById")
	User toEntity2(UserDTO2 dto);

	/**
	 * Map 2.
	 *
	 * @param entity
	 *            the entity
	 * @return the user DTO 2
	 */
	// @Mapping(target = "createdById", source = "entity.createdBy.id")
	// @Mapping(target = "updatedById", source = "entity.updatedBy.id")
	UserDTO2 toDto2(User entity);

	/**
	 * After mapping.
	 *
	 * @param dto
	 *            the dto
	 * @param entity
	 *            the entity
	 */
	@AfterMapping
	default void afterMapping(final UserDTO2 dto,
			@MappingTarget final User entity) {
		if (Objects.isEmpty(dto.getCreatedById())) {
			entity.setCreatedBy(null);
		}

		if (Objects.isEmpty(dto.getUpdatedById())) {
			entity.setUpdatedBy(null);
		}
	}

	/**
	 * Gets the created updated.
	 *
	 * @param user
	 *            the user
	 * @return the created updated
	 */
	@Named("createdByUser")
	default String getCreatedByUser(final UserPlain user) {
		return user.getCByIdentity();
	}

	/**
	 * Gets the updated by user.
	 *
	 * @param user
	 *            the user
	 * @return the updated by user
	 */
	@Named("updatedByUser")
	default String getUpdatedByUser(final UserPlain user) {
		return user.getUByIdentity();
	}
}
