/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
 * The Enum CycleType.
 */
public enum CycleType {

	/** The Once. */
	Once(0),

	/** The Daily. */
	Daily(1),

	/** The Weekly. */
	Weekly(2),

	/** The Monthly. */
	Monthly(3),

	/** The None. */
	None(4);

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the cycle type
	 */
	public static CycleType fromInteger(final Integer value) {
		if (value == Once.value) {
			return Once;
		} else if (value == Daily.value) {
			return Daily;
		} else if (value == Weekly.value) {
			return Weekly;
		} else if (value == Monthly.value) {
			return Monthly;
		} else if (value == None.value) {
			return None;
		}
		return null;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == Once.value) {
			return "Once";
		} else if (value == Daily.value) {
			return "Daily";
		} else if (value == Weekly.value) {
			return "Weekly";
		} else if (value == Monthly.value) {
			return "Monthly";
		} else if (value == None.value) {
			return "None";
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new cycle type.
	 *
	 * @param value
	 *            the value
	 */
	private CycleType(final int value) {
		this.value = value;
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}