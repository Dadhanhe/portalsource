/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum GoalType.
 */
public enum GoalType {

	/** The incremental revenue. */
	INCREMENTAL_REVENUE(0), // Used for revenue Goal

	/** The arpd. */
	ARPD(1), // Used for CR revenue goal
	/** The ir. */
	IR(2); // This is for recommended goal
	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the goal type
	 */
	public static GoalType fromInteger(final Integer value) {
		if (value == INCREMENTAL_REVENUE.value) {
			return INCREMENTAL_REVENUE;
		} else if (value == ARPD.value) {
			return ARPD;
		} else if (value == IR.value) {
			return IR;
		}
		return null;
	}

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new goal type.
	 *
	 * @param value
	 *            the value
	 */
	private GoalType(final int value) {
		this.value = value;
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("goal.type.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}

	/**
	 * Gets the enum name.
	 *
	 * @param value
	 *            the value
	 * @return the enum name
	 */
	public static String getEnumName(final Integer value) {
		if (value == INCREMENTAL_REVENUE.value) {
			return "Hotel";
		} else if (value == ARPD.value) {
			return "CarRental";
		} else if (value == IR.value) {
			return "NewHotelGoal";
		}
		return null;
	}

	/**
	 * Gets the value.
	 *
	 * @param code
	 *            the code
	 * @return the value
	 */
	public static int getValue(final String code) {
		for (int i = 0; i < GoalType.values().length; i++) {
			if (code.equalsIgnoreCase(GoalType.values()[i].name())) {
				return GoalType.values()[i].value();
			}
		}
		return -1;
	}
}