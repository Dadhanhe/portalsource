/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum AvgPer.
 */
public enum AvgPer {

	/** The day. */
	DAY(0),

	/** The week. */
	WEEK(1),

	/** The month. */
	MONTH(2),

	/** The quarter. */
	QUARTER(3),

	/** The year. */
	YEAR(4);

	/** The value. */
	private final int value;

	/**
	 * Instantiates a new avg per.
	 *
	 * @param value
	 *            the value
	 */
	private AvgPer(final int value) {
		this.value = value;
	}

	/**
	 * Html style.
	 *
	 * @return the string
	 */
	public String htmlStyle() {
		return String.format("status.%d.style", this.value);
	}

	/**
	 * I 18 n key.
	 *
	 * @return the string
	 */
	public String i18nKey() {
		return String.format("status.%d", this.value);
	}

	/**
	 * Value.
	 *
	 * @return the int
	 */
	public int value() {
		return this.value;
	}
}
