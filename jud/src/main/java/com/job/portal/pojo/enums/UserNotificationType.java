/*
 * Copyright 2018  JUD     All Rights Reserved.
 *
 * 
 * 
 *  
 * 
 * 
 * 
 * 
 * 
 */
package com.job.portal.pojo.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum UserNotificationType.
 */
public enum UserNotificationType {

	/** The Goal cast. */
	GoalCast(0),

	/** The Goal update. */
	GoalUpdate(1),

	/** The Goal achieve. */
	GoalAchieve(2),

	/** The Fpg goal update. */
	FpgGoalUpdate(3),

	/** The Weekly monthly goal. */
	WeeklyMonthlyGoal(4),

	/** The Location target create. */
	LocationTargetCreate(5),

	/** The Location target update. */
	LocationTargetUpdate(6),

	/** The Social interaction like. */
	SocialInteractionLike(7),

	/** The Social interaction comment. */
	SocialInteractionComment(8),

	/** The Training assign. */
	TrainingAssign(9),

	/** The Training complete. */
	TrainingComplete(10),

	/** The Feed social interaction like. */
	FeedSocialInteractionLike(11),

	/** The Feed social interaction comment. */
	FeedSocialInteractionComment(12),

	/** The Observation taken. */
	ObservationTaken(13),

	/** The Observation update schedule. */
	ObservationUpdateSchedule(14),

	/** The Observation cancel. */
	ObservationCancel(15),

	/** The Observation schedule. */
	ObservationSchedule(16),

	/** The Coaching taken. */
	CoachingTaken(17),

	/** The Coaching update schedule. */
	CoachingUpdateSchedule(18),

	/** The Coaching cancel. */
	CoachingCancel(19),

	/** The Coaching schedule. */
	CoachingSchedule(20),

	/** The One to one schedule. */
	OneToOneSchedule(21),

	/** The One to one schedule update. */
	OneToOneScheduleUpdate(22),

	/** The One to one taken. */
	OneToOneTaken(23),

	/** The One to one cancel. */
	OneToOneCancel(24),

	/** The One to one remove. */
	OneToOneRemove(25),

	/** The Meeting schedule. */
	MeetingSchedule(26),

	/** The Meeting schedule update. */
	MeetingScheduleUpdate(27),

	/** The Meeting cancel. */
	MeetingCancel(28),

	/** The Meeting remove. */
	MeetingRemove(29),

	/** The Contest report owner assign. */
	ContestReportOwnerAssign(30),

	/** The Contest report owner remove. */
	ContestReportOwnerRemove(31),

	/** The Contest report user assign. */
	ContestReportUserAssign(32),

	/** The Contest report user remove. */
	ContestReportUserRemove(33),

	/** The Goal acceptance reminder. */
	GoalAcceptanceReminder(34),

	/** The Goal accepted. */
	GoalAccepted(35),

	/** The Machine learning recommendations video. */
	MachineLearningRecommendationsVideo(36),

	/** The Machine learning recommendations one 2 one. */
	MachineLearningRecommendationsOne2One(37),

	/** The Auth expire reminder. */
	AuthExpireReminder(38),

	/** The Audit trail update. */
	AuditTrailUpdate(39),

	/** The Lock record reminder. */
	LockRecordReminder(40),

	/** The Blue print score change. */
	BlueprintScoreChange(41),

	/** The Blue print not updated. */
	BlueprintNotUpdated(42),

	/** The video Library Recommend. */
	VideoLibraryRecommend(43),

	/** The Comments feed. */
	CommentsFeed(44),
	/** The Import tenant product. */
	ImportTenantProduct(46),

	/** The Feed. */
	Feed(45);

	/** The value. */
	private final int value;

	/**
	 * From integer.
	 *
	 * @param value
	 *            the value
	 * @return the user notification type
	 */
	public static UserNotificationType fromInteger(final Integer value) {
		if (value == GoalCast.value) {
			return GoalCast;
		} else if (value == GoalUpdate.value) {
			return GoalUpdate;
		} else if (value == GoalAchieve.value) {
			return GoalAchieve;
		} else if (value == FpgGoalUpdate.value) {
			return FpgGoalUpdate;
		} else if (value == WeeklyMonthlyGoal.value) {
			return WeeklyMonthlyGoal;
		} else if (value == LocationTargetCreate.value) {
			return LocationTargetCreate;
		} else if (value == LocationTargetUpdate.value) {
			return LocationTargetUpdate;
		} else if (value == SocialInteractionLike.value) {
			return SocialInteractionLike;
		} else if (value == SocialInteractionComment.value) {
			return SocialInteractionComment;
		} else if (value == TrainingAssign.value) {
			return TrainingAssign;
		} else if (value == TrainingComplete.value) {
			return TrainingComplete;
		} else if (value == FeedSocialInteractionLike.value) {
			return FeedSocialInteractionLike;
		} else if (value == FeedSocialInteractionComment.value) {
			return FeedSocialInteractionComment;
		} else if (value == ObservationTaken.value) {
			return ObservationTaken;
		} else if (value == ObservationUpdateSchedule.value) {
			return ObservationUpdateSchedule;
		} else if (value == ObservationCancel.value) {
			return ObservationCancel;
		} else if (value == ObservationSchedule.value) {
			return ObservationSchedule;
		} else if (value == CoachingTaken.value) {
			return CoachingTaken;
		} else if (value == CoachingUpdateSchedule.value) {
			return ObservationUpdateSchedule;
		} else if (value == CoachingCancel.value) {
			return CoachingCancel;
		} else if (value == CoachingSchedule.value) {
			return CoachingSchedule;
		} else if (value == OneToOneSchedule.value) {
			return OneToOneSchedule;
		} else if (value == OneToOneScheduleUpdate.value) {
			return OneToOneScheduleUpdate;
		} else if (value == OneToOneTaken.value) {
			return OneToOneTaken;
		} else if (value == OneToOneCancel.value) {
			return OneToOneCancel;
		} else if (value == OneToOneRemove.value) {
			return OneToOneRemove;
		} else if (value == MeetingSchedule.value) {
			return MeetingSchedule;
		} else if (value == MeetingScheduleUpdate.value) {
			return MeetingScheduleUpdate;
		} else if (value == MeetingCancel.value) {
			return MeetingCancel;
		} else if (value == MeetingRemove.value) {
			return MeetingRemove;
		} else if (value == MachineLearningRecommendationsVideo.value) {
			return MachineLearningRecommendationsVideo;
		} else if (value == MachineLearningRecommendationsOne2One.value) {
			return MachineLearningRecommendationsOne2One;
		} else if (value == ContestReportOwnerAssign.value) {
			return ContestReportOwnerAssign;
		} else if (value == ContestReportOwnerRemove.value) {
			return ContestReportOwnerRemove;
		} else if (value == ContestReportUserAssign.value) {
			return ContestReportUserAssign;
		} else if (value == ContestReportUserRemove.value) {
			return ContestReportUserRemove;
		} else if (value == GoalAcceptanceReminder.value) {
			return GoalAcceptanceReminder;
		} else if (value == GoalAccepted.value) {
			return GoalAccepted;
		} else if (value == AuthExpireReminder.value) {
			return AuthExpireReminder;
		} else if (value == AuditTrailUpdate.value) {
			return AuditTrailUpdate;
		} else if (value == LockRecordReminder.value) {
			return LockRecordReminder;
		} else if (value == BlueprintScoreChange.value) {
			return BlueprintScoreChange;
		} else if (value == BlueprintNotUpdated.value) {
			return BlueprintNotUpdated;
		} else if (value == CommentsFeed.value) {
			return CommentsFeed;
		} else if (value == Feed.value) {
			return Feed;
		} else if (value == ImportTenantProduct.value) {
			return ImportTenantProduct;
		}
		return null;
	}

	/**
	 * Instantiates a new user notification type.
	 *
	 * @param value
	 *            the value
	 */
	private UserNotificationType(final int value) {
		this.value = value;
	}
}
