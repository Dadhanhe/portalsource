/**
 * jquery.elastislide.js v1.1.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */
!function(t,i,s){"use strict"
var e,n,o=t.event
e=o.special.debouncedresize={setup:function(){t(this).on("resize",e.handler)},teardown:function(){t(this).off("resize",e.handler)},handler:function(t,i){var s=this,a=arguments,h=function(){t.type="debouncedresize",o.dispatch.apply(s,a)}
n&&clearTimeout(n),i?h():n=setTimeout(h,e.threshold)},threshold:150}
var a="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="
t.fn.imagesLoaded=function(i){function s(){var s=t(d),e=t(u)
o&&(u.length?o.reject(r,s,e):o.resolve(r)),t.isFunction(i)&&i.call(n,r,s,e)}function e(i,e){i.src!==a&&-1===t.inArray(i,l)&&(l.push(i),e?u.push(i):d.push(i),t.data(i,"imagesLoaded",{isBroken:e,src:i.src}),h&&o.notifyWith(t(i),[e,r,t(d),t(u)]),r.length===l.length&&(setTimeout(s),r.unbind(".imagesLoaded")))}var n=this,o=t.isFunction(t.Deferred)?t.Deferred():0,h=t.isFunction(o.notify),r=n.find("img").add(n.filter("img")),l=[],d=[],u=[]
return t.isPlainObject(i)&&t.each(i,function(t,s){"callback"===t?i=s:o&&o[t](s)}),r.length?r.bind("load.imagesLoaded error.imagesLoaded",function(t){e(t.target,"error"===t.type)}).each(function(i,s){var n=s.src,o=t.data(s,"imagesLoaded")
return o&&o.src===n?void e(s,o.isBroken):s.complete&&void 0!==s.naturalWidth?void e(s,0===s.naturalWidth||0===s.naturalHeight):void((s.readyState||s.complete)&&(s.src=a,s.src=n))}):s(),o?o.promise(n):n}
var h=t(i),r=i.Modernizr
t.Elastislide=function(i,s){this.$el=t(s),this._init(i)},t.Elastislide.defaults={orientation:"horizontal",speed:500,easing:"ease-in-out",minItems:3,start:0,onClick:function(t,i,s){return!1},onReady:function(){return!1},onBeforeSlide:function(){return!1},onAfterSlide:function(){return!1}},t.Elastislide.prototype={_init:function(i){this.options=t.extend(!0,{},t.Elastislide.defaults,i)
var s=this,e={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",msTransition:"MSTransitionEnd",transition:"transitionend"}
if(this.transEndEventName=e[r.prefixed("transition")],this.support=r.csstransitions&&r.csstransforms,this.current=this.options.start,this.isSliding=!1,this.$items=this.$el.children("li"),this.itemsCount=this.$items.length,0===this.itemsCount)return!1
this._validate(),this.$items.detach(),this.$el.empty(),this.$el.append(this.$items),this.$el.wrap('<div class="elastislide-wrapper elastislide-loading elastislide-'+this.options.orientation+'"></div>'),this.hasTransition=!1,this.hasTransitionTimeout=setTimeout(function(){s._addTransition()},100),this.$el.imagesLoaded(function(){s.$el.show(),s._layout(),s._configure(),s.hasTransition?(s._removeTransition(),s._slideToItem(s.current),s.$el.on(s.transEndEventName,function(){s.$el.off(s.transEndEventName),s._setWrapperSize(),s._addTransition(),s._initEvents()})):(clearTimeout(s.hasTransitionTimeout),s._setWrapperSize(),s._initEvents(),s._slideToItem(s.current),setTimeout(function(){s._addTransition()},25)),s.options.onReady()})},_validate:function(){this.options.speed<0&&(this.options.speed=500),(this.options.minItems<1||this.options.minItems>this.itemsCount)&&(this.options.minItems=1),(this.options.start<0||this.options.start>this.itemsCount-1)&&(this.options.start=0),"horizontal"!=this.options.orientation&&"vertical"!=this.options.orientation&&(this.options.orientation="horizontal")},_layout:function(){this.$el.wrap('<div class="elastislide-carousel"></div>'),this.$carousel=this.$el.parent(),this.$wrapper=this.$carousel.parent().removeClass("elastislide-loading")
var i=this.$items.find("img:first")
this.imgSize={width:i.outerWidth(!0),height:i.outerHeight(!0)},this.highestContainerHeight=this.$items.first().height()
var s=0
t.each(this.$items,function(i,e){t(e).height()>s&&(s=t(e).height())}),this.highestContainerHeight=s,this._setItemsSize(),"horizontal"===this.options.orientation?this.$el.css("max-height",this.highestContainerHeight):this.$el.css("height",this.options.minItems*this.highestContainerHeight),this._addControls()},_addTransition:function(){this.support&&this.$el.css("transition","all "+this.options.speed+"ms "+this.options.easing),this.hasTransition=!0},_removeTransition:function(){this.support&&this.$el.css("transition","all 0s"),this.hasTransition=!1},_addControls:function(){var i=this
this.$navigation=t('<nav><span class="elastislide-prev">Previous</span><span class="elastislide-next">Next</span></nav>').appendTo(this.$wrapper),this.$navPrev=this.$navigation.find("span.elastislide-prev").on("mousedown.elastislide",function(t){return i._slide("prev"),!1}),this.$navNext=this.$navigation.find("span.elastislide-next").on("mousedown.elastislide",function(t){return i._slide("next"),!1})},_setItemsSize:function(){var t="horizontal"===this.options.orientation?100*Math.floor(this.$carousel.width()/this.options.minItems)/this.$carousel.width():100
this.$items.css({width:t+"%","max-width":this.imgSize.width,"max-height":(this.options.orientation,this.highestContainerHeight)}),"vertical"===this.options.orientation&&(this.$wrapper.css("max-width",this.imgSize.width+parseInt(this.$wrapper.css("padding-left"))+parseInt(this.$wrapper.css("padding-right"))),this.$items.css({height:this.highestContainerHeight}))},_setWrapperSize:function(){"vertical"===this.options.orientation&&this.$wrapper.css({})},_configure:function(){this.fitCount="horizontal"===this.options.orientation?this.$carousel.width()<this.options.minItems*this.imgSize.width?this.options.minItems:Math.floor(this.$carousel.width()/this.imgSize.width):this.$carousel.height()<this.options.minItems*this.highestContainerHeight?this.options.minItems:Math.floor(this.$carousel.height()/this.highestContainerHeight)},_initEvents:function(){var i=this
h.on("debouncedresize.elastislide",function(){i._setItemsSize(),i._configure(),i._slideToItem(i.current)}),this.$el.on(this.transEndEventName,function(){i._onEndTransition()}),"horizontal"===this.options.orientation?this.$el.on({swipeleft:function(){i._slide("next")},swiperight:function(){i._slide("prev")}}):this.$el.on({swipeup:function(){i._slide("next")},swipedown:function(){i._slide("prev")}}),this.$el.on("click.elastislide","li",function(s){var e=t(this)
i.options.onClick(e,e.index(),s)})},_destroy:function(t){this.$el.off(this.transEndEventName).off("swipeleft swiperight swipeup swipedown .elastislide"),h.off(".elastislide"),this.$el.css({"max-height":"none",transition:"none"}).unwrap(this.$carousel).unwrap(this.$wrapper),this.$items.css({width:"auto","max-width":"none","max-height":"none"}),this.$navigation.remove(),this.$wrapper.remove(),t&&t.call()},_toggleControls:function(t,i){i?"next"===t?this.$navNext.show():this.$navPrev.show():"next"===t?this.$navNext.hide():this.$navPrev.hide()},_slide:function(i,s){if(this.isSliding)return!1
this.options.onBeforeSlide(),this.isSliding=!0
var e=this,n=this.translation||0,o="horizontal"===this.options.orientation?this.$items.outerWidth(!0):this.$items.outerHeight(!0),a=this.itemsCount*o,h="horizontal"===this.options.orientation?this.$carousel.width():this.$carousel.height()
if(void 0===s){var r=this.fitCount*o
if(r<0)return!1
if("next"===i&&a-(Math.abs(n)+r)<h)r=a-(Math.abs(n)+h),this._toggleControls("next",!1),this._toggleControls("prev",!0)
else if("prev"===i&&Math.abs(n)-r<0)r=Math.abs(n),this._toggleControls("next",!0),this._toggleControls("prev",!1)
else{var l="next"===i?Math.abs(n)+Math.abs(r):Math.abs(n)-Math.abs(r)
l>0?this._toggleControls("prev",!0):this._toggleControls("prev",!1),l<a-h?this._toggleControls("next",!0):this._toggleControls("next",!1)}s="next"===i?n-r:n+r}else{var r=Math.abs(s)
Math.max(a,h)-r<h&&(s=-(Math.max(a,h)-h)),r>0?this._toggleControls("prev",!0):this._toggleControls("prev",!1),Math.max(a,h)-h>r?this._toggleControls("next",!0):this._toggleControls("next",!1)}if(this.translation=s,n===s)return this._onEndTransition(),!1
if(this.support)"horizontal"===this.options.orientation?this.$el.css("transform","translateX("+s+"px)"):this.$el.css("transform","translateY("+s+"px)")
else{t.fn.applyStyle=this.hasTransition?t.fn.animate:t.fn.css
var d="horizontal"===this.options.orientation?{left:s}:{top:s}
this.$el.stop().applyStyle(d,t.extend(!0,[],{duration:this.options.speed,complete:function(){e._onEndTransition()}}))}this.hasTransition||this._onEndTransition()},_onEndTransition:function(){this.isSliding=!1,this.options.onAfterSlide()},_slideTo:function(t){var t=t||this.current,i=Math.abs(this.translation)||0,s="horizontal"===this.options.orientation?this.$items.outerWidth(!0):this.$items.outerHeight(!0),e=i+this.$carousel.width(),n=Math.abs(t*s);(n+s>e||n<i)&&this._slideToItem(t)},_slideToItem:function(t){var i="horizontal"===this.options.orientation?t*this.$items.outerWidth(!0):t*this.$items.outerHeight(!0)
this._slide("",-i)},add:function(t){var i=this,s=this.current,e=this.$items.eq(this.current)
this.$items=this.$el.children("li"),this.itemsCount=this.$items.length,this.current=e.index(),this._setItemsSize(),this._configure(),this._removeTransition(),s<this.current?this._slideToItem(this.current):this._slide("next",this.translation),setTimeout(function(){i._addTransition()},25),t&&t.call()},setCurrent:function(t,i){this.current=t,this._slideTo(),i&&i.call()},next:function(){self._slide("next")},previous:function(){self._slide("prev")},slideStart:function(){this._slideTo(0)},slideEnd:function(){this._slideTo(this.itemsCount-1)},destroy:function(t){this._destroy(t)}}
var l=function(t){i.console&&i.console.error(t)}
t.fn.elastislide=function(i){var s=t.data(this,"elastislide")
if("string"==typeof i){var e=Array.prototype.slice.call(arguments,1)
this.each(function(){return s?t.isFunction(s[i])&&"_"!==i.charAt(0)?void s[i].apply(s,e):void l("no such method '"+i+"' for elastislide self"):void l("cannot call methods on elastislide prior to initialization; attempted to call method '"+i+"'")})}else this.each(function(){s?s._init():s=t.data(this,"elastislide",new t.Elastislide(i,this))})
return s}}(jQuery,window)
