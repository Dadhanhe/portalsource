import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationBoardComponent } from './educationBoard.component';
import { EducationBoardRoutingModule } from './educationBoard-routing.module';



@NgModule({
    imports: [
        CommonModule,
        EducationBoardRoutingModule
    ],
    declarations: [EducationBoardComponent],

})
export class EducationBoardModule { }
