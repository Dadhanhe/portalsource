import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;
@Component({
    selector: 'app-educationBoard',
    templateUrl: "./educationBoard.component.html",
    animations: []
})


export class EducationBoardComponent implements OnInit, AfterViewInit {
    closeResult: string;
    constructor(private modalService: NgbModal) { }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    addEducationBoard() {
        console.log("test");
        $('#savePopup').modal('show');
    }
    oaddEducationBoardpen(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {

        });
    }
}
