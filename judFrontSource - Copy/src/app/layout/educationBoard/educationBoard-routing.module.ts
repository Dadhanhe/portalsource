import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EducationBoardComponent } from './educationBoard.component';

const routes: Routes = [
    {
        path: '', component: EducationBoardComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EducationBoardRoutingModule {
}
