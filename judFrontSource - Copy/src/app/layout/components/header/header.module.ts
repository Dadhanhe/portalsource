import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';

@NgModule({
    imports: [RouterModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [HeaderComponent],
    exports: [HeaderComponent],
})

export class HeaderModule { }
