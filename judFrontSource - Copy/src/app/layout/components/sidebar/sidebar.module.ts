import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';

@NgModule({
    imports: [RouterModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [SidebarComponent],
    exports: [SidebarComponent],
})

export class SidebarModule { }
