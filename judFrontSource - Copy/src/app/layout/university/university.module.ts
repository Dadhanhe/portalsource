import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UniversitySearchComponent } from './university-serach.component';
import { UniversityRoutingModule } from './university.routes';


@NgModule({
  imports: [
    CommonModule,
    UniversityRoutingModule
  ],
  declarations: [UniversitySearchComponent],
  
})
export class universityModule { }
