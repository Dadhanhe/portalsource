import { Component, OnInit, Input, Output, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';


@Component({
    selector: 'app-university',
    templateUrl: "./university-search.component.html",
    animations: [routerTransition()]
})
export class UniversitySearchComponent {
    
}
