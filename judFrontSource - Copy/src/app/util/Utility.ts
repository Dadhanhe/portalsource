export class Utility {
    public static isEnableWaitIndicator = true;
public static blank(val) {
    return val == undefined || val == null
        || (typeof val == "string" && val.trim() == "") || (val instanceof Array && val.length < 1) ? true : false;
    }
    static notBlank(val) {
        return val == undefined || val == null
            || (typeof val == "string" && val.trim() == "") || (val instanceof Array && val.length < 1) ? false : true;
    }
    static printStackTrace(response, isSingleField) {
        if (isSingleField) {
            console.log('resource: ' + response.resource);
            console.log('field: ' + response.field);
            console.log('code:' + response.code);
            console.log('message: ' + response.message);
        } else {
            if (response.data) {
                let errorFields = response.data;
                errorFields.forEach(element => {
                    console.log('resource: ' + element.resource);
                    console.log('field: ' + element.field);
                    console.log('code:' + element.code);
                    console.log('message: ' + element.message);
                });
            }
        }
    }
}   