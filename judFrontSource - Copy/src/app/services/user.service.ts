import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Router } from '@angular/router';
import { AbstractService } from './abstract.service';
import { Utility } from '../util/Utility';
declare var $: any;
declare var JSOG: any;
declare var moment: any;
@Injectable()
export class UserService extends AbstractService {
    authenticate(email, password) {
        //let formBody = "client_id=restapp&client_secret=restapp&grant_type=password&username=" + email + "&password=" + $.md5(password);
        let str = { client_id: 'restapp', client_secret: 'restapp', grant_type: 'password', username: email, password: password, deviceId: null, platform: 2 };
        return this.http.post(this.globalService.getBase() + "/api/user/authenticateUserUsingLDAP", str);
    }
    getUser(auth) {
        let params: HttpParams = new HttpParams();
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Authorization', 'bearer ' + (auth ? auth.access_token : this.globalService.getToken()));
        headers = headers.append('IndustryId', '1');
        return this.http.get(this.globalService.getBase() + "/api/user/secure/getByToken", { headers: headers, params: params })
            ;
    }
}
