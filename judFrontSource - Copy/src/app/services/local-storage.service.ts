import { Injectable } from '@angular/core';
declare var moment: any;

@Injectable()
export class LocalStorageService {
    get(key: any): any {
        var storageVal: any = /*this.getLocal('remember') ?*/ localStorage[key] /*: sessionStorage[key]*/;
        if (!storageVal) {
            return;
        }
        var parsedVal = JSON.parse(storageVal);
        if (parsedVal.expiry && (moment(parsedVal.expiry).valueOf()) > (moment().valueOf())) {
            this.delete(key);
            return;
        }
        return parsedVal.value;
    }

    set(key: string, value: any, expiry: Date = null) {
        var saveObject: any = { value: value };
        if (expiry) {
            saveObject.expiry = expiry;
        }
       /* if (this.getLocal('remember')) {*/
            localStorage[key] = JSON.stringify(saveObject);
       /* } else {
            sessionStorage[key] = JSON.stringify(saveObject);
        }*/
    }

    delete(key: string) {
        /*if (this.getLocal('remember')) {*/
            localStorage.removeItem(key);
        /*} else {
            sessionStorage.removeItem(key);
        }*/
    }

    deleteAll() {
        localStorage.clear();
        sessionStorage.clear();
    }
    deleteAllEndsWith(endsWith) {
        /*if (this.getLocal('remember')) {*/
            this.deleteFromAllEndsWith(endsWith, localStorage);
       /* } else {
            this.deleteFromAllEndsWith(endsWith, sessionStorage);
        }*/
    }
    deleteFromAllEndsWith(endsWith, storage) {
        var arr = []; // Array to hold the keys
        // Iterate over localStorage and insert the keys that meet the condition into arr
        for (var i = 0; i < storage.length; i++) {
            if (storage.key(i).endsWith(endsWith)) {
                arr.push(storage.key(i));
            }
        }
        // Iterate over arr and remove the items by key
        for (var i = 0; i < arr.length; i++) {
            storage.removeItem(arr[i]);
        }
    }
    getLocal(key: any): any {
        var storageVal: any = localStorage[key];
        if (!storageVal) {
            return;
        }
        var parsedVal = JSON.parse(storageVal);
        if (parsedVal.expiry && ((moment(parsedVal.expiry).valueOf())) > (moment().valueOf())) {
            this.delete(key);
            return;
        }
        return parsedVal.value;
    }

    setLocal(key: string, value: any, expiry: Date = null) {
        var saveObject: any = { value: value };
        if (expiry) {
            saveObject.expiry = expiry;
        }
        localStorage[key] = JSON.stringify(saveObject);
    }
}