import { UserService } from './services/user.service';
import { LocalStorageService } from './services/local-storage.service';
import { GlobalService } from './services/global.service';

export const ApiServices = [GlobalService, LocalStorageService,
    UserService];