import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Utility } from '../util/Utility';
import { UserService } from '../services/user.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GlobalService } from '../services/global.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../validation/validation.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    showValidationMsg: boolean = false;
    signInForm: any;
    userId: any;
    @BlockUI() blockUI: NgBlockUI;
    errorMessage: boolean = false;
    constructor(public router: Router, private userService: UserService, public globalService: GlobalService,private formBuilder: FormBuilder) {
        this.signInForm = this.formBuilder.group({
            'email': ['', [ValidationService.required, ValidationService.emailValidator]],
            // 'password': ['', [ValidationService.required, ValidationService.passwordValidator]]
            'password': ['', [ValidationService.required]]
        });
        this.signInForm.valueChanges.subscribe(data => {
            this.errorMessage = false;
        });
    }

    ngOnInit() {}

   

    doLogin(token?: any) {
        // show validation only if form submitted once.
        if (!this.showValidationMsg) {
            this.showValidationMsg = true;
        }
        if (Utility.blank(token) && (this.signInForm.dirty && this.signInForm.valid)) {
            // get credentials
            // this.signInForm.value.email = this.signInForm.value.email.trim();
            var email = this.signInForm.value.email;
            if (Utility.notBlank(email)) {
                email = email.trim();
            }
            var password = this.signInForm.value.password;
            if (Utility.isEnableWaitIndicator) { this.blockUI.start(); }
            this.userService.authenticate(email, password).subscribe((response: any) => {
                if (response && response.status == "SUCCESS") {
                    this.userDetail(response.data);
                } else {
                    if (Utility.isEnableWaitIndicator) {
                        this.blockUI.stop();
                    } else {
                        console.log("BlockUI stop on position 2..");
                    }
                    if (response.data) {
                        let errorFields = response.data[0];
                        if (this.signInForm.controls[errorFields.field]) {
                            this.signInForm.controls[errorFields.field].markAsTouched();
                            this.signInForm.controls[errorFields.field].setErrors([errorFields.message]);
                        } else {
                            Utility.printStackTrace(errorFields, true);
                        }
                    }
                }
            });
        } else if (Utility.notBlank(token)) {
            this.userDetail({ access_token: token });
        }
        return false;
    }
    userDetail(auth) {
        let thiss = this;
        this.userService.getUser(auth).subscribe((res: any) => {
            thiss.globalService.setUser(res.data);
            thiss.userId = thiss.globalService.getUser().id;
        });
        localStorage.setItem('isLoggedin', 'true');
        thiss.router.navigate(['/dashboard']);
    }
}
