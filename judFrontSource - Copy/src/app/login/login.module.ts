import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { ValidationModule } from '../validation/validation.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    imports: [CommonModule, LoginRoutingModule,CommonModule,
        FormsModule,
        ReactiveFormsModule,
     ValidationModule],
    declarations: [LoginComponent]
})
export class LoginModule {}
