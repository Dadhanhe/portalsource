import { ValidatorFn, FormControl } from '@angular/forms';
import { Utility } from '../util/Utility';

export class ValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'This field is *Required*',
            'pattern': 'Invalid format',
            'invalidEmailAddress': 'Invalid email address. Please verify email address.',
            'invalidPassword': 'Password must be between 6 and 20 characters long.',
            // 'invalidPassword': validatorValue.validationMessage,
            'invalidContact': 'Invalid contact. Please provide proper contact number.',
            'invalidNumber': 'This field requires digits.',
            'invalidPositiveNumber': 'This field requires positive digits only.',
            'minlength': `Minimum ${validatorValue.requiredLength} characters required`,
            'invalidAlphaNumUnderscorePatter': 'This field requires alphabets, digits or underscore',
            'invalidNumberRange1To100': 'must be between 1 ~ 100 numbers',
            'invalidNumberRange1To10': 'must be between 1 ~ 10 numbers',
            'invalidNumberRange0To100': 'must be between 0 ~ 100 numbers',
            'invalidCharacterRange1To50': 'must be 1 ~ 50 characters',
            'invalidCharacterRange1To12': 'must be 1 ~ 12 characters',
            'invalidCharacterRange1To30': 'must be 1 ~ 30 characters',
            'youTubleURLValidator': 'must be 1 ~ 800 characters should have format like "http://www.youtube.com/watch?v=Lp7eHq4Sxxc"',
            'maxlength': `More than ${validatorValue.requiredLength} characters not allowed`,
            'importError': validatorValue.errorMessage,
            'USER_NOT_CREATED': 'User not created',
            'PRODUCT_NOT_FOUND': 'Product not found',
            'invalidInputValue': 'Invalid Value',
            'invalidPhone': 'Invalid Phone Number',
            'twoDecimalDigit': 'This field allow a number upto 2 decimal places.',
            'max': `This field can not be more than ${validatorValue.max}`,
            'min': `This field can not be less than ${validatorValue.min}`,
            'uniqueTierAlias': 'Please enter unique tier name',
            'invalidName': 'Please input alphabet characters only',
            'invalidNumberRange1To999': 'must be between 1 ~ 999 numbers',
        };
        return config[validatorName];
    }
    static required(control) {
        if (control.value == null || typeof control.value === 'string' && control.value.trim().length === 0) {
            return { 'required': true }
        } else {
            return null;
        }
    }
    static validateMultiSelectRequired(control) {
        if (control.value == undefined || control.value.length == 0) {
            return { 'required': true };
        } else {
            return null;
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^[_A-Za-z0-9-\'\+]+(\.[_A-Za-z0-9-\'\+]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/)) {
                return null;
            } else {
                return { 'invalidEmailAddress': true };
            }
        } else {
            return null;
        }
    }
    // This method is used to validate email address entered on login page
    static loginEmailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().trim().match(/^[_A-Za-z0-9-\'\+]+(\.[_A-Za-z0-9-\'\+]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/)) {
                return null;
            } else {
                return { 'invalidEmailAddress': true };
            }
        } else {
            return null;
        }
    }
    static multiEmailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            let emails = control.value.split(',');
            let invalid = false;
            emails.forEach(element => {
                if (element != undefined && element.toString().trim() != "") {
                    if (element.toString().match(/^[_A-Za-z0-9-\'\+]+(\.[_A-Za-z0-9-\'\+]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/)) {

                    } else {
                        invalid = true;
                    }
                }
            });
            if (invalid) {
                return { 'invalidEmailAddress': true };
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    static inputValidator(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^[\w&.'\-\s]+$/)) {
                return null;
            } else {
                return { 'invalidInputValue': true };
            }
        } else {
            return null;
        }
    }
    static menuDisplayNameValidator(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^[a-zA-Z0-9&_'( )-]*[a-zA-Z][a-zA-Z0-9_&'( )-]*$/)) {
                return null;
            } else {
                return { 'invalidInputValue': true };
            }
        } else {
            return null;
        }
    }
    static empNumber(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^\w{1,100}$/)) {
                return null;
            } else {
                return { 'invalidAlphaNumUnderscorePatter': true };
            }
        } else {
            return null;
        }
    }

    static passwordValidator(control) {
        // {6,20}           - Assert password is between 6 and 20 characters
        if (control.value.toString().match(/^.{6,20}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }
    // static passwordValidator(validationMessage): ValidatorFn {
    //     return (control: any) => {
    //         {
    //             return { 'invalidPassword': { 'validationMessage': validationMessage } };
    //         }
    //     }
    // }

    static numberOnly(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^[0-9]{1,3}$/)) {
                return null;
            } else {
                return { 'invalidNumber': true };
            }
        } else {
            return null;
        }
    }
    static digitOnly(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/^\d+(\.\d{1,2})?$/)) {
                return null;
            } else {
                return { 'invalidNumber': true };
            }
        } else {
            return null;
        }
    }
    static decimalNumberUpTo100(control) {
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (parseInt(control.value) > 100) {
                return { 'invalidNumberRange1To100': true };
            }
            // ^(([1-9]{1,2})|100)(([^])\.\d{1,2})?$
            if (control.value.toString().match(/^0*(?:[1-9][0-9]?|100)(([^])\.\d{1,2})?$/)) {
                return null;
            } else {
                return { 'invalidNumberRange1To100': true };
            }
        } else {
            return null;
        }
    }
    static numberRange_1_To_100(control) {
        if (control.value != undefined && control.value.toString().match(/^0*(?:[1-9][0-9]?|100)$/)) {
            return null;
        } else {
            return { 'invalidNumberRange1To100': true };
        }
    }
    static numberRange_1_To_10(control) {
        if (control.value != undefined && control.value.toString().match(/^0*(?:[1-9][0-9]?|10)$/)) {
            return null;
        } else {
            return { 'invalidNumberRange1To10': true };
        }
    }

    static numberRange_0_To_100(control) {
        if (control.value != undefined && control.value.toString().match(/^(?:[0-9][0-9]?|100)$/)) {
            return null;
        } else {
            return { 'invalidNumberRange0To100': true };
        }
    }

    static characterRange_1_To_12(control) {
        if (control.value != undefined && control.value.toString().match(/^.{1,12}$/)) {
            return null;
        } else {
            return { 'invalidCharacterRange1To12': true };
        }
    }
    static characterRange_1_To_30(control) {
        if (control.value != undefined && control.value.toString().match(/^.{1,30}$/)) {
            return null;
        } else {
            return { 'invalidCharacterRange1To30': true };
        }
    }
    static twoDecimalDeigitValidator(control) {
        if (control.value != undefined && control.value.toString().match(/^[0-9]+(\.{0,1}[0-9]{1,2})?$/)) {
            return null;
        } else {
            return { 'twoDecimalDigit': true };
        }
    }

    static characterRange_1_To_50(control) {
        if (control.value != undefined && control.value.toString().match(/^.{1,50}$/)) {
            return null;
        } else {
            return { 'invalidCharacterRange1To50': true };
        }
    }
    static youTubeURLMatching(control) {
        if (control.value != undefined && control.value.toString().match(/^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/)) {
            return null;
        } else {
            return { 'youTubleURLValidator': true };
        }
    }

    /*  static invalidPhone(control) {
          if (control.value == undefined || (Utility.notBlank(control.value) && control.value.toString().match(/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/))) {
              return null;
          } else {
              return { 'invalidPhone': true };
          }
      }*/
    static invalidPhone(control) {
        if (control.value == null || control.value.toString().match(/^[0-9]*$/)) {
            return null;
        } else {
            return { 'invalidPhone': true };
        }
    }
    static invalidName(control) {
        if (control.value == null || control.value.toString().match(/^[A-Za-z]+$/)) {
            return null;
        } else {
            return { 'invalidName': true };
        }
    }
    static importError(errorMessage): ValidatorFn {
        return (control: any) => {
            if (Utility.blank(control.importErrorValidationCount) || control.importErrorValidationCount > 1) {
                //control.importErrorValidationCount = control.importErrorValidationCount+1
                return null;
            }
            //control.importErrorValidationCount = control.importErrorValidationCount+1;
            control.markAsTouched();
            return { 'importError': { 'errorMessage': errorMessage } };
        };
    }

    static checkNumber(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (isNaN(control.value.toString())) {
                return { 'invalidNumber': true };
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    static nonZeroNumberOnly(control) {
        // RFC 2822 compliant regex
        if (control.value != undefined && control.value.toString().trim() != "") {
            if (control.value.toString().match(/[1-9][0-9]*/) && control.value.toString().indexOf(".") < 0) {
                return null;
            } else {
                return { 'invalidNumberRange1To999': true };
            }
        } else {
            return null;
        }
    }



    static checkPositiveNumber(): ValidatorFn {
        return (control: FormControl): { [key: string]: any } | null => {
            // RFC 2822 compliant regex
            if (control.value != null) {
                let result = ValidationService.checkNumber(control);
                if (result == null) {
                    // to avoid data like +96 
                    // 185489972 : 2.17.1.30 = IN-525 = User is able to enter negative amount in gift card field
                    if (Number(control.value.toString()) < 0 || control.value.toString().indexOf('+') != -1) {
                        return { 'invalidPositiveNumber': true };
                    }
                    else {
                        null;
                    }
                }
                else {
                    return result;
                }
            }
            else {
                null;
            }
        }
    }
    static max(max: number): ValidatorFn {
        return (control: FormControl): { [key: string]: any } | null => {

            let val: number = control.value;

            if (val == undefined || val.toString().trim() == "") {
                return null;
            }
            if (val <= max) {
                return null;
            }
            return { 'max': { 'max': max } };
        }
    }

    static min(min: number): ValidatorFn {
        return (control: FormControl): { [key: string]: any } | null => {

            let val: number = control.value;

            if (val == undefined || val.toString().trim() == "") {
                return null;
            }
            if (val >= min) {
                return null;
            }
            return { 'min': { 'min': min } };
        }
    }
    static isUniqueTierAlias(value1, value2, value3, value4): ValidatorFn {
        return (control: FormControl): { [key: string]: any } | null => {
            // RFC 2822 compliant regex
            if (Utility.notBlank(control.value)) {
                if (Utility.notBlank(value1) && control.value.trim().toUpperCase() == value1.trim().toUpperCase()) {
                    return { 'uniqueTierAlias': true };
                } else if (Utility.notBlank(value2) && control.value.trim().toUpperCase() == value2.trim().toUpperCase()) {
                    return { 'uniqueTierAlias': true };
                }
                else if (Utility.notBlank(value3) && control.value.trim().toUpperCase() == value3.trim().toUpperCase()) {
                    return { 'uniqueTierAlias': true };
                }
                else if (Utility.notBlank(value4) && control.value.trim().toUpperCase() == value4.trim().toUpperCase()) {
                    return { 'uniqueTierAlias': true };
                }
                return null;
            }
        }
    }
}