ALTER TABLE `jud`.`user` 
DROP FOREIGN KEY `FK_USERTBL_MODIFIED_BY_ID`;
ALTER TABLE `jud`.`user` 
CHANGE COLUMN `MODIFIED_ON` `UPDATED_ON` DATETIME NOT NULL ,
CHANGE COLUMN `MODIFIED_BY` `UPDATED_BY` INT(11) NOT NULL ;
ALTER TABLE `jud`.`user` 
ADD CONSTRAINT `FK_USERTBL_MODIFIED_BY_ID`
  FOREIGN KEY (`UPDATED_BY`)
  REFERENCES `jud`.`user` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


ALTER TABLE `jud`.`user` 
ADD COLUMN `accessToken` VARCHAR(45) NULL AFTER `STATUS_ID`,
ADD COLUMN `salt` VARCHAR(45) NULL AFTER `accessToken`;


ALTER TABLE `jud`.`user` 
CHANGE COLUMN `MOBILE` `MOBILE` VARCHAR(45) NOT NULL ;


ALTER TABLE `jud`.`user` 
ADD COLUMN `birthDay` INT(11) NULL AFTER `salt`,
CHANGE COLUMN `BIRTHDATE` `birthMonth` INT(11) NULL DEFAULT NULL ;


ALTER TABLE `jud`.`user` 
ADD COLUMN `IS_AUTHY_ATTEMPTED` TINYINT(1) NULL AFTER `birthDay`,
ADD COLUMN `AUTHY_ID` VARCHAR(45) NULL AFTER `IS_AUTHY_ATTEMPTED`;


ALTER TABLE `jud`.`user` 
CHANGE COLUMN `LAST_NME` `LAST_NAME` VARCHAR(45) NOT NULL ;


CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) NOT NULL,
  `authentication` longblob,
  `authentication_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `token` longblob,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `oauth_client_details` (
  `client_id` varchar(128) NOT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(128) DEFAULT NULL,
  `authorities` varchar(128) DEFAULT NULL,
  `authorized_grant_types` varchar(128) DEFAULT NULL,
  `client_secret` varchar(128) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `resource_ids` varchar(128) DEFAULT NULL,
  `scope` varchar(128) DEFAULT NULL,
  `web_server_redirect_uri` varchar(128) DEFAULT NULL,
  `autoapprove` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(255) NOT NULL,
  `authentication` longblob,
  `token` longblob,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*
-- Query: SELECT * FROM uo_31102018.oauth_client_details
LIMIT 0, 1000

-- Date: 2018-11-29 10:47
*/
INSERT INTO `oauth_client_details` (`client_id`,`access_token_validity`,`additional_information`,`authorities`,`authorized_grant_types`,`client_secret`,`refresh_token_validity`,`resource_ids`,`scope`,`web_server_redirect_uri`,`autoapprove`) VALUES ('restapp',NULL,NULL,'ROLE_ADMIN','password,authorization_code,refresh_token,implicit','restapp',NULL,NULL,'read,write,trust',NULL,NULL);



CREATE TABLE `app_preference` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '''Unique by Industry''',
  `value` varchar(800) NOT NULL,
  `version` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `ACTIVE_STATUS` tinyint(4) NOT NULL DEFAULT '0',
  `CREATED_BY` int(11) NOT NULL,
  `UPDATED_BY` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `UPDATED_ON` datetime NOT NULL,
  `json_data` json DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX_NAME` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;


INSERT INTO `jud`.`role` (`NAME`) VALUES ('Super_admin');
INSERT INTO `jud`.`role` (`NAME`) VALUES ('Operational_Admin');
INSERT INTO `jud`.`role` (`NAME`) VALUES ('Field_Admin');


-------------  01122018 --------

----- Hemali ----
/*
-- Query: SELECT * FROM jud.user
LIMIT 0, 1000

-- Date: 2018-12-01 01:07
*/
INSERT INTO `user` (`ID`,`SALUATION`,`FIRST_NAME`,`LAST_NAME`,`MIDDLE_NAME`,`EMAIL`,`PHONE_NUMBER`,`GENDER`,`PASSWORD`,`birthMonth`,`CREATED_ON`,`CREATED_BY`,`UPDATED_ON`,`UPDATED_BY`,`NATIONALITY`,`CURRENT_SALARY`,`EXPECTED_SALARY`,`NOTICE_PERIOD`,`AFFIRMATIVE`,`MATERIAL_STATUS`,`USERUID`,`ACTIVE_STATUS`,`accessToken`,`salt`,`birthDay`,`IS_AUTHY_ATTEMPTED`,`AUTHY_ID`) VALUES (1,'MRS','Hemali','Dadhaniya','Girishbhai','hemali.dadhaniya@gmail.com','8980896398',1,'a8b1d597bc53dcd11dc3cdc5558a8ca0',6,'2015-04-24 13:20:45',2,'2015-04-24 13:20:45',2,0,420000,500000,'4',NULL,'single','123456',0,NULL,'53cf87290c90a8cdfdd97ae8ad589f41',NULL,NULL,NULL);
INSERT INTO `user` (`ID`,`SALUATION`,`FIRST_NAME`,`LAST_NAME`,`MIDDLE_NAME`,`EMAIL`,`PHONE_NUMBER`,`GENDER`,`PASSWORD`,`birthMonth`,`CREATED_ON`,`CREATED_BY`,`UPDATED_ON`,`UPDATED_BY`,`NATIONALITY`,`CURRENT_SALARY`,`EXPECTED_SALARY`,`NOTICE_PERIOD`,`AFFIRMATIVE`,`MATERIAL_STATUS`,`USERUID`,`ACTIVE_STATUS`,`accessToken`,`salt`,`birthDay`,`IS_AUTHY_ATTEMPTED`,`AUTHY_ID`) VALUES (2,'MRS','Hemali1','Dadhaniya','Girishbhai','hemali1.dadhaniya@gmail.com','89808963981',1,'a8b1d597bc53dcd11dc3cdc5558a8ca0',6,'2015-04-24 13:20:45',1,'2015-04-24 13:20:45',1,0,420000,500000,'4',NULL,'single','1245',0,NULL,'53cf87290c90a8cdfdd97ae8ad589f41',NULL,NULL,NULL);

----- Hemali ----
CREATE TABLE `education_board` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `boardType` TINYINT(4) NOT NULL,
  `CREATED_BY` INT(11) NOT NULL,
  `UPDATED_BY` INT(11) NOT NULL,
  `CREATED_ON` DATETIME NOT NULL,
  `UPDATED_ON` DATETIME NOT NULL,
  `ACTIVE_STATUS` TINYINT(4) NOT NULL,
  PRIMARY KEY (`ID`));


ALTER TABLE `jud`.`education_board` 
DROP COLUMN `boardType`;


ALTER TABLE `jud`.`university` 
DROP FOREIGN KEY `FK_UNIVERSITY_ADDRESS_ID`;
ALTER TABLE `jud`.`university` 
DROP COLUMN `ADDRESS_ID`,
DROP COLUMN `DESCRIPTION`,
ADD COLUMN `CREATED_BY` INT(11) NOT NULL AFTER `NAME`,
ADD COLUMN `UPDATED_BY` INT(11) NOT NULL AFTER `CREATED_BY`,
ADD COLUMN `CREATED_ON` DATETIME NOT NULL AFTER `UPDATED_BY`,
ADD COLUMN `UPDATED_ON` DATETIME NOT NULL AFTER `CREATED_ON`,
ADD COLUMN `ACTIVE_STATUS` TINYINT(4) NOT NULL AFTER `UPDATED_ON`,
DROP INDEX `FK_UNIVERSITY_ADDRESS_ID_idx` ;


CREATE TABLE `disability` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `UPDATED_BY` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `UPDATED_ON` datetime NOT NULL,
  `ACTIVE_STATUS` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `jud`.`country` 
DROP FOREIGN KEY `FK_STATUS_ID`;
ALTER TABLE `jud`.`country` 
DROP COLUMN `STATUS_ID`,
DROP INDEX `FK_STATUS_ID_idx` ;


CREATE TABLE `industry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `UPDATED_BY` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `UPDATED_ON` datetime NOT NULL,
  `ACTIVE_STATUS` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `subindustry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `INDUSTRY_ID` int(11) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `UPDATED_BY` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `UPDATED_ON` datetime NOT NULL,
  `ACTIVE_STATUS` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `jud`.`subindustry` 
ADD INDEX `fk_industry_id_idx` (`INDUSTRY_ID` ASC);
;
ALTER TABLE `jud`.`subindustry` 
ADD CONSTRAINT `fk_industry_id`
  FOREIGN KEY (`INDUSTRY_ID`)
  REFERENCES `jud`.`industry` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
